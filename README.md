My doctoral dissertation
=========================

This repository contains my dissertation, written at the University of Tampere in 2015--2018.


Setting up
----------

## R packages

The (numeric) data and essential data analysis scripts are included in the package `phdR2`

The bayesian models used in the analysis are created with the helper package `phdBayes`

## Custom pandoc filters

### Pangloss

Install with `pip3 install git+https://github.com/hrmJ/pangloss_linguex`

### Pandoc_avm

Install with `pip3 install git+https://github.com/hrmJ/pandoc_avm`


## Fonts etc.

    sudo apt install fonts-texgyre


## Before building the book


Make sure you've got the right options set up in the `.Rprofile` file:

```


options(modeldatapath = "/home/juho/phd_data/data/modeldata/")

```
