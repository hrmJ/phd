#!/usr/bin/python3
import sys
import panflute as pf


def bibfilter(elem, doc):
    """
    Surround the references with a custom environment
    remember to chmod +x this!
    """
    if isinstance(elem, pf.Div):
        if elem.identifier == "refs":
            return [pf.RawBlock("\\begin{mybibliography}\\begin{singlespace}  \\widowpenalties 1 10000 \n \\raggedbottom ", "latex"), elem, pf.RawBlock("\\end{singlespace}\\end{mybibliography}", "latex")]


def main(doc=None):
    return pf.run_filter(bibfilter, doc=doc) 


if __name__ == '__main__':
    main()

