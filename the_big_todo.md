The big ToDo, part 2
=====================

Suuret linjat
-------------

Älä muuta metodia tai aineistoa (jälkimmäisessä pienet säädöt ok): niitä 
ei kritisoitu, vaan ennen kaikkea 7-8-lukujen tulkintaa ja 
CXg- / lingvististä osaa.

TODELLAKIN. Suunnitelma on:

- Ota huomioon ennen kaikkea Nikunlassin kommentit
- Leisiön kommenteista kaikki, mitkä on helposti ja järkevästi toteutettavissa,
  mutta vähempi stressi epämääräisemmistä
- Muista, että Leisiökin päättää lausuntosta seuraavasti: "**Pienin** muutoksin työ
tulee vaikuttamaan alaansa."
- lukuihin 7 - 9 tarvitaan niihinkin vähintään sen verran muutoksia, että
voi sanoa niihin tehdyn muutoksia. Lue aina yhden Leisiön 7-9-kritiikin perään
yksi Nikunlassin positiivinen arvio niistä.

- [X] Puhu taksonomioista "näkökulmina jaotteluun" ja korosta (vaikka kuviolla)
  näiden jaotteluiden johdannossa, että ovat nimenomaan eri kulmia, joista
  käsin ilmiötä voi (ja pitää) valottaa ja  
- [X] Eroon potentiaalisesti kalendaarinen / ei-kalendaarinen-jaottelusta
    - [ ] tässä tosin ysiluvun fokusjutuissa pitää olla tarkkana
    - [ ] Korosta kuitenkin kutosluvussa ja ehkä aiemminkin, että
      morfrak-muuttuja ei ole yksi yhteen taksonomia 1 eikä myöskään nimestä
      huolimatta pelkkä morf.rak. Nimeä niin, että nimessä on
      kauttaviiva/viivoja ja useampi osa? esim. morf/ref
    - Nikunlassin huomiot NP--PP-konfuusiosta. Parempi ehkä puhua
      ei-adverbisistä tai NP/PP-nimellä tms.
    - [ ] Ehkä parempi olla puhumatta sijainnista neljäntenä taksonomiana.
- [X] Referentiaalisuus jatkumona kaipaa tarkennusta
- [X] Yritä ottaa Nikunlassin ehdotuksen mukaisesti konstruktiokieliopin
  näkökulma myös kakkoslukuun.
    - = *ajanilmaukset konstruktioina*, eikä vain konstruktioiden osina
    - Tämä voisi tapahtua siten, että ennen näkökulmia (aka taksonomiat) lyhyt
      johdatus cxg:n perusajatuksiin
- [X] Kasvata cxg:n roolia ajan käsitteen kuvauksessa, ts. luvussa 2
    - Ajattele, jos ajanilmaukset itse tulkittaisiin konstruktioiksi -
      todellakin harkinnan arvoista!
    - inheritance-käsitteistön tarkempi soveltaminen voisi todellakin olla
      paikallaan.
    - Leisiö kritisoi notaation käyytöä, mutta Nikunlassilta saatavissa suora
      lainaus siitä, että juuri notaatio on hyödyllinen.
- [ ] Käytä KYMPPILUKUA siihen, että luonnostelet konstruktioiden välille
  linkkien ohjaamaa hierarkiaa.
    - [ ] ks. etenkin goldbergin kuviota sivulla 109, joka kuvaa
      konstruktioitten välisiä linkkejä verkostona
- [ ] Missio: tekstistä kevyempi .... Kysy kommentteja vaikka Kirsiltä ja
  muilta fennisteiltä? Ehkä ammattiapua (ks. esim. http://kativastamaki.fi/)?
  Ehkäpä juuri kolmoslukua voisi näytättää jollain kielentarkastajalla? Tai
  ainakin siihen voisi keskittyä erityisellä huolella...


### Muuta 

- [ ] Nimi: Ajanilmausten sijainnit suomessa ja venäjässä: kvantitatiivinen näkökulma.?
- [ ] Segmentoinnin käsite ja alatopiikki vs. vanhemmat tavat lähestyä tätä
- [ ] Kaikki Nikunlassin miscellancea


Lähempänä pintatasoa
--------------------

- [ ] Nikunlassilla erinomaisia ja spesifejä lähde-ehdotuksia! (alaviite 1)
- [ ] Lisää myös kommentti tiivistetty hist.
- [X] Määritelmät pitää muotoilla paremmin, katso Nikunlassin huomautukset (1. sivu, alaviite 2)
- [ ] Tulostettava "key" aineistoryhmistä githubiin ja liitteeksi
    - samalla kappaleen verran lukuohjetta ennen kuin aineitoryhmiin sukelletaan
    kunnolla luvuissa 7-10
- [ ] Yritä puhua substantiibeista nominien sijaan
- [ ] katso, että taivutusmuotoina adverbeissa olisi takavokaali
- [ ] puhu määrite--arvo-matriiseista
- [ ] muista määritellä affektiivisuus tarpeaksi laimeasti
- [/] teelinen ekstensio on terminä mennyt sekaisin
- [ ] lisää lähteisiin viittaus uskonnollista kieltä käsittelevään SKS:n kirjaan 
 	Sanaa tutkimassa - näkökulmia uskonnolliseen kieleen ja sen käyttöön
    https://sks.kauppakv.fi/sivu/kielitiede/?action=search&search=&filter-author%5B%5D=Nissi%2C+Riikka
    - ks. myös  https://tampub.uta.fi/bitstream/handle/10024/65720/951-44-6622-5.pdf?..#page=253
- [ ] Tarkista lause- ja lausetaso-sanojen käyttö kolmosluvussa.
- [ ] Korvaa kehyksinen ajanilmaus -termi.
    - mahdollisesti "jatkuva resultatiivinen ekstensio"
- [ ] Muistaa sisällyttää paremmin ajatus perinteisistä fennistiikan lausetyypeistä.
- [ ] Pidä huolta, että kehysadverbiaali-käsite tulee mainituksi (tulikohan se jo poistettua vahingossa?)
- [ ] Harkitse yhdenmukaista konstruktion nimi -notaatiota (all caps;vrt. Goldberg ja Leino)
- [ ] Tekstinsisäisistä viittauksista vähemmän raskaita
    - [ ]  ? Jokaisen lyhenteen perässä väh. yksi esimerkki: tee tästä konventio,
      jonka selostat jo vitos(tms.)luvussa.
    - [ ] Oleellisempi: kun viittailet aikaisempiin esimerkkeihin, anna aina esimerkistä
    pieni (olennaisin) pätkä, niin ettei tarvitse kääntyä takaisin kurkkimaan.
- [ ] Katso, voisiko jotenkin pehmentää sitä, että luvussa 7 viitataan luvun 9 päätelmiin
- [ ] AIK-merkintä konstruktioissa gf=adv-merkinnän sijaan.
- [ ] res.keh kaikissa kuvioissa (ym.) muotoon res.jatk. Myös lyhenneliitteestä!
- [ ] taksonomia-sanan siistiminen GetGroupMetaTab-funktiosta
- [ ] kokeile ehkä datapisteistä puhumista toiston välttämiseksi?
- [ ] verbatim-fontin käyttö kursiivin sijaan konstruktioiden yksittäisiä arvoja kuvattaessa
- [ ] "aikaa ilmaiseva konstruktio" -termiä pitää saada viljeltyä sopivasti myös 7--9-lukuihin
- [ ] ilmoita jossain ekassa matriisissa tai muualla, että muuttujien arvot kursiivilla.
    - tai mahdollisesti johdannon lopuksi?
- [ ] uudet konstruktionimet satunnaisotantoihin.
- [ ] mieti ehkä sitä, että kun annetaan viitattujen esimerkkien tiivistelmät, annettaisiin myös sivunumero 
- [ ] tarkkana alatopiikkikonstruktiosta puhumisen kanssa nyt,  kun käytössä niin selkeästi määritelty segmentoitu ajanjakso -konstruktio.
- [ ] affektiivisen konstruktion alalajien merkitseminen tai ainakin huomautus esim. l5c-satunnaisotantataulukossa
- [ ] keksi parempi termi TT:lle kuin ajallinen topiikki. (TT-aika?)
- [ ] venäjänkielisen S3-aineiston "laittomalle" esimerkille 352 on lisättävä selitykseksi, että oikeastaan kyseessä on annotointivirhe
- [ ] kvantitatiivista dataa virkkeiden lyhenemisestä?
- [ ] ehkä tiivistelmiä argumenteista 7--9-lukujen loppuun?
- [ ] Kutosluku on osittain käsittelemättä: morf/ref-siivousta lähinnä
- [ ] telic-merkintä kuvioissa?
- [ ] käsittele ajatusta siitä, että kielenoppijat tallentavat frekvenssejä osana informaatiota
    - ks. hilpert 143
- [X] HUOMHUOM!! Kumoa muutos, joka koskee S2- ja S3-mallien morph-muuttujaa.
- [ ] morph-kirjoitusasu F:llä kuvioihin(?)
- [ ] affektin merkitseminen matriiseihin aff
- [ ] määrää painottava pois affektiivisista?
- [ ] tapahtumasta raportointi syytä mainita itse asiassa kolmosluvussa
- [ ] fok ja foc matriiseissa
- [ ] ident+/anch+ ei--fokaalisessa?
- [ ] viittausten kirjoitusasu: taks1 vai taksn1


