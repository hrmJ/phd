# Konstruktiokieliopin tuominen ilmausten tasolle

- Esipuhe tms, jossa 
    - kuvataan lyhyesti cxg:n perusidea
    - esitellään ajatus konstruktion käsitteen relevanttiudesta
- Koko kakkosluvun muokkaus niin, että ajatus ajanilmauksista konstruktioina tulisi paremmin näkyviin
    - *ajanilmaus + ajanilmauskonstruktio: linkittyneet instance-linkillä*
- Korjaa adverbiaali-sanan käyttöä englanninkielisessä abstraktissa

## Perintä

    - *ajanilmaus + ajanilmauskonstruktio: linkittyneet instance-linkillä*
    - Nikunlassin huomio sivulta 28: ajanilmauskonnstruktiot ovat linkittyneet perinteisiinlausetyyppeihin perintälinkkien kautta.
    - Muista myös perimiskäsitteistön laajempi hyödyntäminen ja käyttöönotto luvussa 10
    - Perintälinkit myös ratkaisuna leisiötä ärsyttäneeseen affektiiviseen konstruktioon ja sen suhteeseen fokaalisen konstruktion kanssa

# Määritelmät

- [ ] Objektin käsite suhteessa määritelmään 1
- [ ] Sivulla 19 duratiivinen kesto + objektit
- [ ] Määritelmä 3 ei täytä määritelmien kriteerejä
- [ ] Määritelmä 4 ei täytä määritelmien kriteerejä

# Näkökulma 1

- [ ] NP ja PP suhde
