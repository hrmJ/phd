\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\hypersetup{unicode=true,
            pdftitle={Ajanilmausten sijainnit suomessa ja venäjässä: konstruktiotason kontrastiivinen näkökulma},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\newcommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{Ajanilmausten sijainnit suomessa ja venäjässä: konstruktiotason
kontrastiivinen näkökulma}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{}
    \preauthor{}\postauthor{}
    \date{}
    \predate{}\postdate{}
  

\begin{document}
\maketitle

On the locations of time adverbials in Finnish and Russian: a
construction-based contrastive approach.

250 pages, in Finnish

\textbf{Keywords} Contrastive linguistics, corpora, word order,
construction grammar, time adverbials

\section{Abstract}\label{abstract}

\subsection{Motivation and theoretical
background}\label{motivation-and-theoretical-background}

Finnish and Russian are languages traditionally considered to have a
free or -- in generativistic terms -- discourse configurational word
order. In both languages the positioning of adverbials in general and
time adverbials in particular is especially flexible. This flexibility
poses a challenge both from an L2 learner's and a translator's
perspective: coming from a Finnish background it is often difficult to
use time adverbials idiomatically in Russian and vice versa. The
challenge is made greater by the fact that there is very little learning
material or teacher's guides available on the subject. This study takes
a construction grammar based contrastive approach on the issue, aiming
to provide translators, language learners and teachers with concrete
examples on what the essential differences between Finnish and Russian
are with regard to the placement of time adverbials.

\subsection{Research data}\label{research-data}

The study examines 50 different groups of Finnish and Russian time
adverbials, the selection of which was based on three qualities: the
etymology, semantic function and referential status of the adverbial.
Syntactically, the focus is on Finnish and Russian affirmative SVO
sentences, in the context of which the possible locations of the
adverbial were divided into four distinct categories: L1 (before the
subject, the verb and the object), L2 (between the subject and the
verb), L3 (between the verb and the object) and L4 (after the subject,
the verb and the object). The data of the study consists of two pairs of
large comparable corpora: A Finnish and a Russian newspaper corpus and a
Finnish and a Russian corpus of internet texts. The corpora were queried
for the expressions included in the 50 groups and the concordances
retrieved by the queries were further annotated using dependency
parsers. As a result, a data set of 81 663 Finnish and 70 106 Russian
sentences was formed.

\subsection{Reseach methods and main
findings}\label{reseach-methods-and-main-findings}

The collected data shows, first of all, that there really is a notable
difference between Finnish and Russian in the frequencies of the four
locations defined. Besides the grammatically expected difference in the
usage of L2 and L3 (the former is rare in Finnish, the latter in
Russian), the data indicates that the sentence-initial position is more
frequent in Russian (36,04 \% of all the cases compared to 11,17 \% in
Finnish) and, conversely, the sentence-final more frequent in Finnish
(35,31 \% of all the cases compared to 9,17 \% in Russian). To account
for the reasons behind these observations, a (bayesian) categorical
regression model was built with the location of the adverbial as the
dependent variable. The model was used to identify the use cases of a
time adverbial that are, on the other hand, common for both of the
studied languages, and, on the other hand, more typical for Russian than
for Finnish and vice versa. The properties of these use cases -- most
importantly, the roles of the constituents in the examples from the
point of view of information structure -- were summarized as
constructions.

The final results of the study are presented as a network of
constructions, indicating which Finnish and Russian use cases of time
adverbials are similar and to what extent. In many cases there is a
correspondence between a Russian construction with a sentence-initial
and a Finnish construction with a sentence-final or L3-located
adverbial. One clear example of these are sentences classified in the
study as introductory time adverbial constructions, which are
constructions usually used at the beginning of a text to introduce, for
instance, the central event a news item is reporting about.

\subsection{Conclusions}\label{conclusions}

By analyzing time adverbials as parts of constructions rather than as an
independent syntactic category and by using a data-driven, explicitly
contrastive approach the study intends to raise discussion on the role
and methods of teaching grammar for students in translation studies. It
is suggested that a corpus-based methodology and a theoretical framework
based on construction grammar would provide a fruitful combination in
university-level second language teaching aimed specifically at future
translators and interpreters working within a specific language pair.
Although the study operates mainly on a rather practical and applied
level of contrastive linguistics, the findings give grounds for further
research to be conducted from a more theoretical, typological
perspective.


\end{document}
