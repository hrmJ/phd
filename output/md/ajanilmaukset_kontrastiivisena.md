---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
fontsize: 11.5pt
geometry: twoside, b5paper, layout=b5paper, left=2cm, right=2cm, top=2cm, bottom=3cm, bindingoffset=0.5cm
indent: true
subparagraph: yes
output:
    html_document:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
    pdf_document: 
      latex_engine: xelatex
      toc: true
      toc_depth: 6
      number_sections: true
      keep_tex: true
      includes:
          in_header: preamble.tex
---


Suomen ja venäjän ajanilmauskonstruktiot kontrastiivisena tutkimuskohteena {#kontr}
===========================================================================


Edellä osiossa \ref{vsvtk} esiteltiin *ajanilmauskonstruktion* käsite  ja
todettiin sen olevan nyt käsillä olevan tutkimuksen varsinainen kohde. Tämän
luvun tehtävänä on eritellä tarkemmin, minkälaiset ajanilmauskonstruktiot
ovat suomessa ja venäjässä tutkimuskirjallisuuden perusteella mahdollisia ja
miten yleisiä minkäkin tyyppiset konstruktiot ovat. Ennen siirtymistä
suomen ja venäjän konkreettisiin ominaisuuksiin tarkastelen kuitenkin sitä, 
minkälainen kieltenvälinen vertailu on tämänkaltaisen tutkimuksen kannalta
mielekästä.

Ajanilmauskonstruktiot ja kontrastiivinen (funktionaalinen) analyysi
----------------------------------------------------------------------

Andrew Chesterman [-@chesterman1998contrastive, 53] esittää, että kieltenvälisen
vertailun -- kontrastiivisen analyysin -- tavoitteena on tuottaa ja testata
falsifioitavissa olevia hypoteeseja *jonkin ilmiön samanlaisuudesta* kahden kielen
välillä. Tässä tutkimuksessa tutkittavana ilmiönä ovat ajanilmauskonstruktiot ja
tutkimuksen tavoitteena sen tarkasteleminen, kuinka samanlaisia tietyt
ajanilmauksen sisältävät rakenteet ovat suomessa ja venäjässä.

Mielenkiintoista kyllä, konstruktiokieliopin piirissä kontrastiivinen tutkimus
on esimerkiksi Hans Boasin [-@boas2010, 2] mukaan ollut verrattain vähäistä. Kuten Boas
argumentoi, tämä ei kuitenkaan johdu siitä, etteikö konstruktionistinen teoria
sopisi hyvin kontrastiivisen vertailun lähtökohdaksi. Päinvastoin, juuri
ajanilmauskonstruktioita ajatellen konstruktiokielioppi tarjoaa erityisen hedelmällisen 
teoreettisen lähtökohdan siitä syystä, että konstruktion käsite antaa
mahdollisuuden vertailla kieliä kaikilla tasoilla -- ei vain leksikaalisella tai
syntaktisella vaan myös esimerkiksi informaatiorakenteen tasolla (mts. 15).

On korostettava, että nyt käsillä olevassa tutkimuksessa konstruktion käsite on
ennen muuta työkalu, käsitteellinen tila, jonka sisällä suomea ja venäjää
voidaan vertailla. Lähtökohtani on ennemmin kontrastiivinen (alhaalta ylös) kuin
typologinen (ylhäältä alas) [vrt. @boas2010, 7]: en lähde ajatuksesta, jonka
mukaan suomen ja venäjän ajanilmauskonstruktiot heijastaisivat jotakin
universaalia konstruktiomallia -- ennemminkin lähestyn konstruktioita itsenäisinä,
kielispesifeinä ilmiöinä, joiden keskinäisestä samanlaisuudesta voidaan tehdä
päätelmiä. Aivan kuten Leino [-@leino2010, 125], katson siis, ettei ole
mielekästä puhua yhden ja saman konstruktion realisoitumisesta kielessä A ja
kielessä B, vaan pikemminkin kahdesta erillisestä konstruktiosta, joiden
samanlaisuutta (Leinolla *correspondence*) voidaan arvioida.

\todo[inline]{katso kielispesifisyydestä Croft 2001 (lk.8-9)}

Samanlaisuus sinänsä on pintapuolisesta yksinkertaisuudestaan ja
arkipäiväisyydestään huolimatta melko monitahoinen  käsite, johon liittyy paljon
määrittelyongelmia [@chesterman1998contrastive, 6]. Chesterman mainitsee
samanlaisuuden määrittelyn kannalta merkittävinä tutkijoina muun muassa Tverskyn
[-@tversky77], Medinin ja Goldstonen [-@medingoldstone1995] ja Sovranin
[-@sovran1992]. 

Yksi keskeisimmistä samanlaisuuteen liittyvistä käsitteistä ovat *piirteet* ja
*piirrejoukot*: Chestermanin (mts., 7) mukaan yksinkertaisimmillaan voidaan
ajatella, että jos kahdella entiteetillä on jokin yhteinen piirre, ovat ne
ainakin jossain määrin samanlaisia ja jos niillä on pelkästään yhteisiä
piirteitä, ne ovat identtisiä. Olennaista kuitenkin on erottaa ne piirteet,
jotka kulloisenkin vertailun kannalta ovat relevantteja tai keskeisiä ja
ymmärtää, että samanlaisuuden asteeseen vaikuttavat aina myös olosuhteet, joissa
vertailu suoritetaan (mts. 9). Lisäksi, kuten Leino [-@leino2010, 132] painottaa,
on huomattava, että samanlaisuudessa on aina kyse asteittaisesta ominaisuudesta.
Konstruktioiden tapauksessa on Leinon mukaan (mp.) ajateltava paitsi
merkityksen, myös muodon tason samanlaisuutta.

Kieltenvälisestä vertailusta puhuttaessa ei samanlaisuuden käsitteen lisäksi voi
välttää termiä *ekvivalenssi*. Chestermanin [-@chesterman1998contrastive, 20-25]
mukaan termi on kulkenut käännöstieteissä pitkän matkan: alun perin kyse on
ollut oletuksesta lähde- ja kohdetekstin yhtäläisyydestä, mistä sittemmin
on siirrytty jaottelemaan ekvivalenssia eri alalajeihin (kuten Nidan formaalinen
ja dynaaminen ekvivalenssi) ja lopulta relativistiseen näkemykseen, jonka mukaan
lähde- ja kohdeteksteistä ei ylipäätään ole mielekästä pyrkiä määrittelemään,
ovatko ne millään tasolla samoja -- järkevämmäksi on  nähty se, että tarkastellaan nimenomaan
samanlaisuutta ja pohditaan esimerkiksi sitä, onko lähdetekstin suhde johonkin
tiettyyn kohdetekstiin tarkoituksenmukainen. Vaikka lähtökohtaisesti voisi
ajatella, että se, mitä kontrastiivisessa kielitieteessä ymmärretään
ekvivalenssilla eroaisi siitä, miten käsite
ymmärretään käännöstieteessä, Chesterman (mts. 39) esittää, että myös
kontrastiivisessa tutkimuksessa ajatus kääntämisestä on yleensä vähintään
implisiittisesti läsnä: kieliä vertailevat tutkijat arvioivat loppujen lopuksi
sitä, voisiko jokin kielen A esiintymä olla käännös jollekin kielen B
esiintymälle.[^leinotrekvo] 

[^leinotrekvo]: esimerkiksi Leino käyttääkin spesifisti termiä *translation
equivalence* ekvivalenssista puhuessaan. 

Nyt käsillä olevan tutkimuksen kannalta ekvivalenssi ei kuitenkaan ole erityisen
keskeinen käsite. Allekirjoitan Chestermanin [-@chesterman1998contrastive, 28]
mainitseman Touryn [-@toury1981, 256] oletuksen siitä, että kontrastiivisen ja
käännöstieteellisen tutkimuksen välillä voidaan nähdä työnjako: siinä missä
edellisen tehtävänä on selvittää, mitä samanlaisuuksia kielten välillä on,
pyrkii jälkimmäinen selittämään, miksi kääntäjä on valinnut juuri tietyn
samanlaisuuden käännösratkaisunsa pohjaksi. Näen oman tutkimukseni selkeämmin
osana kontrastiivista kenttää, enkä pyri niinkään arvioimaan
ajanilmauskonstruktioihin liittyviä käännösratkaisuja kuin tarjoamaan tällaisten
arvioiden tekijöille materiaalia arvioinnin pohjaksi. Tässä mielessä omalta
kannaltani on oleellisempaa puhua ennemmin yksinkertaisesti samanlaisuudesta
kuin ekvivalenssista -- olkoonkin, että määritellessäni jotkin konstruktiot
samanlaiseksi tulen samalla tehneeksi jonkinlaisen esityksen niiden
käännettävyydestä.

\todo[inline]{Leinon (2010) "vastaavuus"-käsite (correspondence)}

Metodologiselta kannalta ajateltuna noudattelen
samanlaisuuden määrittelemisessä Chestermanin funktionaaliseksi
kontrastiiviseksi analyysiksi nimittämää strategiaa, joka  etenee siten, että
aluksi tutkijalla on jokin havainto, jonka perusteella hän katsoo kielen A
ilmauksen X olevan mahdollisesti samanlainen kuin kielen B ilmauksen Y. Kussakin
konkreettisessa vertailussa on erikseen määriteltävä, mitä ovat ne ilmaukseen ja
sen käyttöön liittyvät piirteet, joiden samanlaisuus on vertailun kannalta
relevanttia [@chesterman1998contrastive, 56]. Kun tutkija on määritellyt
kriteerit, joiden perusteella samanlaisuutta arvioidaan, hän esittää
lähtökohtaiseksi hypoteesikseen, että ilmaukset X ja Y ovat identtisiä (mts.
57). Tämän jälkeen hypoteesia testataan ja mikäli ilmaukset eivät ole
identtisiä, muodostetaan uusi hypoteesi, jossa X:n ja Y:n välistä suhdetta
kuvaillaan joksikin muuksi kuin identtiseksi ja esitetään, millä tavoin X ja Y
eroavat (mts. 58). Tätä muokattua hypoteesia testataan ja muokataan edelleen.

Palaan ajanilmauskonstruktioiden samanlaisuutta mittaaviin kriteereihin
tarkemmin luvussa \ref{kokoava-kontrastiivinen-analyysi}, jonka tavoitteena on
etsiä konkreettisia konstruktioryppäitä ja esittää arvioita niiden
samanlaisuudesta suomessa ja venäjässä. Jaan itse vertailtavat konstruktiot
ajanilmauksen sijainnin mukaan neljään pääryhmään, jotka määritellään lähemmin
seuraavassa.

\todo[inline]{Jotain ehkä vielä tekstistrategiasta: konstruktiotasoa isommat kokonaisuudet
ja (ehkä samankaltaisten) konstruktioiden *käyttö* vs. rakenne}

Neljäs taksonomia: sijainti {#taks4}
---------------------------

Tässä yhteydessä on tarpeen esitellä eräs oleellinen tässä tutkimuksessa
analysoitavaa tutkimusaineistoa määrittelevä rajoite. Koska tarkoituksena on
tutkia nimenomaan *ajanilmauksen* sijaintia eikä esimerkiksi sanajärjestystä
yleensä, pyrin rajoittamaan tarkasteltavien lauseiden joukkoa vakioimalla sen
syntaktisen ympäristön, jossa ajanilmauksia tarkastellaan. Tällaisena
vakioituna syntaktisena ympäristönä toimivat *myönteiset SVO-lauseet* eli
lauseet

- jotka eivät ole kieltolauseita
- joissa on nominatiivimuotoinen subjekti, finiittiverbi ja objekti (tai
  vastaava valenssin kannalta olennainen verbin argumentti, ks. osio \ref{tutkimusaineistot})
- joissa mainitut kolme elementtiä esiintyvät nimenomaan järjestyksessä
  subjekti--verbi--objekti.

Valittu rajaus ei välttämättä kuvasta kaikkein yleisintä syntaktista ympäristöä
sen paremmin suomessa kuin venäjässä, eikä se ole täysin tasapuolinen kielten
välillä. Oman näkemykseni mukaan tärkeimmät rajaukseen liittyvät ongelmakohdat
voidaan tiivistää seuraavaan listaan:

1. Kummassakin kielessä on hyvin tavallista, että subjekti ilmaistaan osana
   taivutuspäätettä tai jätetään kokonaan ilmaisematta.
2. Kummassakin kielessä on paljon erilaisia subjektittomia lausetyyppejä [ks. @mlein]
3. Venäjässä SVO-järjestys ei ole yhtä tavallinen kuin suomessa[^kinghuom]

[^kinghuom]: Vaikka venäjää on perinteisesti pidetty SVO-kielenä, 
on esimerkiksi esimerkiksi King [-@king1995] esittänyt perusteita
jopa VSO-järjestyksen ensisijaisuudelle. 
\todo[inline]{Intransitiivilauseista...}

Yhtä kaikki, on myös selvää, että SVO-lauseet ovat sellainen syntaktinen
ympäristö, joka kummassakin kielessä on kaikesta huolimatta tavallinen. Se, mitä
ajanilmausten sijainnista selviää myönteisiä SVO-lauseita tarkastelemalla, on
luultavasti relevanttia ja yleistettävissä laajemmallekin. Ehdottomasti tärkein
syy juuri tämän rajauksen valitsemiselle on kuitenkin tekninen: SVO-lauseet on
kaikkein helpoin erottaa koneellisen annotoinnin perusteella ja toisaalta niiden
osalta koneellisen annotoinnin tarkkuus myös on oletettavasti hyvä [@tobecited].

\todo[inline]{huomauta subjektittomista}

SVO-lauseiden valinta syntaktiseksi rajaukseksi saattaa ensi silmäyksellä  olla
epätyypillinen myös konstruktiokieliopin näkökulmasta -- onhan
konstruktiokieliopissa nähty ensiarvoisen tärkeäksi nimenomaan perifeeristen
rakenteiden tutkiminen ja vastustettu filosofiaa, jossa lähdetään liikkeelle
yksinkertaisista tapauksista ja siirrytään asteittain kompleksisempiin
[@ostman2004, 15]. Tässäkin yhteydessä on kuitenkin korostettava uudestaan,
että syyt juuri SVO-lauseiden valintaan ovat ennen kaikkea tekniset. Koska
konstruktiokieliopin näkökulmasta tavoitteena on lopulta kattaa *kaikki
mahdolliset* kielen ilmiöt -- yksinkertaiset ja kompleksiset -- ei
SVO-lauseiden valitseminen syntaktiseksi rajaukseksi ainakaan mitenkään sodi
tätä ajatusta vastaan, vaan ne ovat rakenteita siinä missä muutkin.
Kielteisistä lauseista on vielä todettava, että pääsyy niiden sulkemiseen
aineiston ulkopuolelle on se, että suomessa kieltorakenteet muodostetaan
erityistä kieltoverbiä käyttäen tavalla, joka eroaa selvästi venäjän
kieltorakenteiden muodostamisesta [suomen kieltoverbistä ks. esim
@miestamo2004suomen; venäjän kieltorakenteista esim. @paduceva2013].
Koneelliseen analyysiin liittyviin ongelmiin sekä rajauksen tekniseen
toteutukseen pureudutaan tarkemmin osiossa
\ref{tutkittavien-ajanilmaustapausten-erottaminen-verrannollisista-korpuksista}.

SVO-järjestyksessä pitäytymisellä on myös se etu, että se tekee ajanilmauksen
sijainnin määrittelemisen melko selkeäksi. Esitänkin seuraavassa edellä 
käsiteltyjen kolmen ensimmäisen ajanilmausten taksonomian lisäksi neljännen, 
sijainnin mukaan tehtävän luokittelun. Tämän luokittelun mukaan
ajanilmauksen sijainti voidaan jakaa neljään kategoriaan, joita nimitän
lyhenteillä S1--S4. Sijainnit ovat:

1. Sijainti ennen sekä subjektia että verbiä (S1)
2. Sijainti juuri ennen verbiä, mutta subjektin jäljessä. (S2)
3. Sijainti juuri verbin jälkeen, ennen objektia (S3)
4. Sijainti sekä verbin että sen (muiden) täydennysten jälkeen (S4)

Tarkemmalla syntaktisella tasolla on huomautettava, että mikäli lauseen
pääverbinä on etenkin suomessa tavallinen liittomuoto (kuten lauseessa *Aatos
on pitkään harrastanut jalkapalloa*), katsotaan ajanilmauksen sijainniksi S3,
jos se sijaitsee verbin persoonamuotoisen osan jälkeen -- suluissa annetun
lauseen sijainniksi määriteltäisiin siis S3.

Tutkimuksen rajauksesta SVO-tapauksiin seuraa se, että vaikka tutkimuksen 
tuloksina esitettävät konstruktiot ovat luultavasti useimmiten yleistettävissä
moniin erilaisiin lausetyyppeihin ja syntaktisiin rakenteisiin, esitän
ne tässä tutkimuksessa aina käyttämällä rakenteita, joissa  edellä esitetyn
matriisin 1 tavoin on läsnä subjekti, verbi, objekti tai
muu täydennys sekä adverbiaali. Jos kulloinkin käsiteltävän konstruktion 
kannalta ajanilmauksen sijainti on olennainen -- toisin sanoen, jos
ajanilmauksen sijaitseminen esimerkiksi  S3-asemassa S1-aseman sijaan tuo
mukanaan jotain olennaista eroa konstruktion merkitykseen tai käyttöön --
käytän konstruktioista nimityksiä *S1-konstruktio*, *S2-konstruktio* ja niin edelleen.
Sitä, miten usein ajanilmaukset tutkimuskirjallisuuden perusteella sijaitsevat
missäkin edellä määritellyistä asemissa, käsitellään tarkemmin 
seuraavissa kahdessa alaluvussa.

Ajanilmauksen tyypilliset sijainnit venäjässä {#konstr-ven}
---------------------------------

Kuten osiossa \ref{jotain} mainittiin, venäjän sanajärjestystutkimuksen
perusteos on Kovtunova [-@kovtunova1976], jonka perusteella kirjoitetussa
kielessä esiintyvälle ajanilmaukselle voidaan määrittää kaksi oletusasemaa. 

Ensinnäkin, mikäli ajanilmaus on tavallinen adverbi, oletussijainti on
kiinteästi aivan verbin edessä (mts. 166). Esimerkkinä tästä voidaan
tarkastella lauseita tutkimusaineiston ryhmistä L8a ja
E6a:




\ex.\label{ee_rulc8_oletus} \small Кто-то из нас скоро умрет (press\_ru)






\ex.\label{ee_ruex6_oletus} \small Я долго искал правильного партнера (press\_ru)




Toiseksi, on tavallista, että ajanilmaus sijaitsee koko lauseen ensimmäisenä.
Tämä on tilanne erityisesti siinä tapauksessa, että ajanilmaus toimii Kovtunovan
termein *determinanttina*. Determinantin käsite liittyy lauseen jakamiseen
*predikatiivilausekkeeksi* (предикативная группа) ja predikatiivilausekkeen ulkopuolisiksi
elementeiksi. Predikaattilausekkeen muodostavat subjekti ja finiittiverbi, ja
näiden muodostamaa kokonaisuutta määrittävät elementit[^tark] ovat determinantteja (mts: 61).
Määritelmä muistuttaa pitkälti edellä mainittua kehysadverbiaalin
käsitettä (ks. osio \ref{topiikin-ja-fokuksen-tarkempi-erittely}). Kovtunova
itse antaa seuraavan esimerkin determinanttina lauseen alussa toimivasta
ajanilmauksesta (ajanilmaus kursivoitu):




\ex.\label{ee_kovtunova_191} \small \emph{В раннем детстве} меня посещала странная грёза



[^tark]: tarkenna määritelmää myös subjektittomiin lauseisiin ym.

Kahdesta oletussijainnista poikkeavat lauseasemat ovat Kovtunovan mukaan aina
seurausta joistakin lauseen informaatiorakenteessa tai Prahan koulukunnan termein
*funktionaalisessa lauseperspektiivissä* [ks. esim. @tobecited] tapahtuvista
muutoksista. Esimerkiksi sijainti lauseen lopussa on mahdollinen, jos
ajanilmaus täyttää reeman funktion [@kovtunova1976, 68] tai jos lause on jollain tapaa
tyylillisesti värittynyt ja korostaa determinanttia (mts. 106).

Sijainti heti finiittiverbin jälkeen mutta ennen objektia on venäjässä
mahdollinen, mutta rajoittunut. Esimerkiksi Kallestinova & Slabakova 
[-@kallestinova08, 200] toteavat, ettei verbin ja objektin välinen sijainti ole täysin kieliopin
vastainen, mutta yhtä kaikki huomattavasti epätyypillisempi kuin verbinetinen
sijainti. Tähän huomioon palataan tarkemmin luvussa \ref{kesk-sis}.


<!-- Tähän lisää esim. Kingiltä? Ja Zimmerlingiltä? -->

Ajanilmauksen tyypilliset sijainnit suomessa
--------------------------------------------

\todo[inline]{Karlsson 1999?}

Maria Vilkunan [-@vilkuna1989; -@vilkuna1995] sanajärjestysesityksillä ei
välttämättä ole aivan yhtä käytettyä statusta suomen sanajärjestystutkimuksen
historiassa kuin Kovtunovalla venäjän osalta. Yhtä kaikki Vilkunan esitykset ovat
laajimpia ja kaiken kaikkiaan käyttökelpoisia lähtökohtia, kun mietitään
ajanilmauksille tavallisia sijainteja suomessa.

Vilkunan sanajärjestysteorian kulmakivi on lauseen jaottelu
diskurssifunktioihin (huomaa ero termiin niin kuin se on käytössä tässä
tutkimuksessa) eli K-, T- ja V-kenttiin [-@vilkuna1995, 244]. K-kenttään
sijoittuu usein (muttei aina) kontrastiivista aineista, T-kenttä kattaa lauseen
topikaalisen ytimen ja T:n jälkeinen aines kuuluu V-kenttään [@vilkuna1989, 38-39].
Vilkunan kuvaus on tarkoituksellisen lineaarinen, niin että diskurssifunktiot
ovat staattisia kenttiä, eivätkä mahdollisesti eri lauseasemin toteuttavia
informaatiorakenteellisia funktioita. Sekaannuksen välttämiseksi käytän
Vilkunan diskurssifunktioista vastedes erillistä termiä *diskurssikenttä*.
Huomattakoon, että Vilkunan kenttä-käsitteet ovat pohjana myös Ison suomen kieliopin 
tavalle kuvata sanajärjestystä (ks. esim. § 1369).

Diskurssikenttämallin hankaluus nyt käsillä olevassa tutkimusasetelmassa on,
ettei juuri adverbiaalien sijoittaminen malliin ole ongelmatonta, vaan usein --
joskaan ei aina --  esimerkiksi lauseenalkuinen ajanilmaus vaikuttaisi vaativan
kokonaan oman diskurssikenttänsä esitettyjen kolmen lisäksi [ks. alaviite
@vilkuna1989, 58]. Verrattuna venäjään  lauseenalkuinen ajanilmaus on suomessa
vaikeammin määriteltävä: se on myös suomessa melko tavallinen, mutta sen
funktioiden erottelu on usein monimutkaista. Shore [-@shore2008, 45] toteaakin
tämän tutkimuksen kannalta kiinnostavasti:

\begin{quote}%
-\/- näiden konstituenttien {[}lauseenalkuisten adverbiaalien, objektien
ym.{]} tekstuaalisten tehtävien perusteelliseksi ymmärtämiseksi
tarvittaisiin laajaan aineistoon ja eri tekstilajeihin perustuvaa
tutkimusta.
%
\end{quote}


Tutkimuksen luvut \ref{ajanilmaukset-ja-alkusijainti} ja \ref{kokoava-kontrastiivinen-analyysi}
pyrkivät ajanilmausten osalta vastaamaan tähän haasteeseen. Tavoitteena
on esittää lauseenalkuiset ajanilmaustapaukset niiden tekstuaalisten  -- tai paremminkin
ylipäätään *viestinnällisten* -- tehtävien ymmärtämiseksi osina erilaisia
S1-sijaintia hyödyntäviä konstruktioita, joilla kaikilla on oma käyttötarkoituksensa
ja -tilanteensa. Kuten luvussa \ref{ajanilmaukset-ja-alkusijainti} käy ilmi, suomessa
nämä käyttötilanteet ovat lopulta yllättävänkin rajattuja. Esimerkkinä voidaan
esittää virke \ref{ee_fi_lc6_alku}




\ex.\label{ee_fi_lc6_alku} \small Kosteikkovahvero on paljolti suppilovahveron näköinen ja myös
erinomainen ruokasieni, mutta joskus rustonupikka saattaa harhauttaa
kerääjää. (Araneum Finnicum: sienikoto.fi)



Esimerkin @ee_fi0a_alku *joskus*-ajanilmauksen voi Vilkunan mallissa katsoa kuuluvan
K-kenttään -- tämän tutkimuksen kehyksessä ajanilmaus on kuvailtavissa
diskurssifunktioltaan kontrastiiviseksi. Vastaavat tapaukset,
joissa rinnastetaan esimerkiksi jokin tavallisesti voimassa oleva asiaintila
ja siihen tietyissä tilanteissa liittyvät poikkeukset, tulkitaan
luvussa \ref{ei--deiktiset-adverbit} *kontrastiivinen konstruktio* -nimisen
rakenteen edustajiksi.


\todo[inline]{katso vertailun vuoksi tähän sulkala ja manninen SVOA:sta, ehkä myös hkv}

Tavallisin adverbiaalin ja sitä kautta ajanilmausten sijainti on
Vilkunan mukaan V-kentän sisällä: suoraan finiittiverbin jäljessä ennen verbin
muita täydennyksiä, ennen kaikkea objektia [-@vilkuna1989, 25, 40]. Tästä
esimerkkinä on seuraavassa esitetty lauseet \ref{ee_filc1_oletus} ja
\ref{ee_fiex1_oletus} tutkimusaineiston ryhmistä L5a ja E1b.




\ex.\label{ee_filc1_oletus} \small Jäsenmaksuja kertyi vuonna 1992 yhteensä 22700 mk. (Araneum Finnicum:
gamma.nic.fi)






\ex.\label{ee_fiex1_oletus} \small Albumi myi \emph{jo kolmessa päivässä} tuplaplatinaa. (Araneum Finnicum:
himosareena.fi)



Vaikka edellä on todettu sijainnin subjektin ja verbin välissä (T-kentän
lopussa ennen V-kenttää) olevan suomessa rajoitettu, sekin on mahdollinen
ja jopa tavallinen etenkin sivulauseissa [@vilkuna1989, 59]. Näin on myös
tutkimusaineiston ryhmää L6a edustavassa esimerkissä \ref{ee_filc5_sivul}:




\ex.\label{ee_filc5_sivul} \small Vapaa-aika nyt on jokaisen oma asia ja vaikka tuohon onkin kerätty ehkä
kaikkein tyrkyimmät tyypit, niin voisin kuvitella, että hekin
\emph{kymmenen vuoden päästä} häpeävät omaa käytöstään ja olemustaan
(Araneum Finnicum: forum.hevostalli.net)



Paitsi lauseenalkuinen, myös lauseenloppuinen sijainti vaikuttaa
lähtökohtaisesti olevan suomessa paikoin venäjää monitulkintaisempi. Samoin
kuin venäjässä, loppusijainti liittyy luonnollisesti tilanteisiin, joissa
ajanilmaus on fokuksessa. Kuitenkin suomessa on myös käyttötilanteita,
joissa lauseen lopussa sijaitseva ajanilmaus ei merkitse mitenkään erityisellä tavalla
uutta informaatiota [vrt. @vilkuna1989, 89, esimerkki 31] . Vastaavat lauseet, kuten esimerkki
\ref{ee_fi_lauseenlop} tutkimusaineiston ryhmästä L1b, ovat myös erityishuomion
kohteena tutkimuksen osiossa  \ref{deiktiset-adverbit-ja-positionaalisuus}.




\ex.\label{ee_fi_lauseenlop} \small ...ja teki oikein kovasti mieli listiä se vanha äijä, joka kopeloi minua
\emph{tänään.} (araneum\_fi)


