---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
fontsize: 11.5pt
geometry: twoside, b5paper, layout=b5paper, left=2cm, right=2cm, top=2cm, bottom=3cm, bindingoffset=0.5cm
indent: true
subparagraph: yes
output:
    html_document:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
    pdf_document: 
      latex_engine: xelatex
      toc: true
      toc_depth: 6
      number_sections: true
      keep_tex: true
      includes:
          in_header: preamble.tex
---


Tilastollinen malli  {#tilastollinenmalli}
============================================


## Bayesiläinen päättely tilastollisen analyysin lähtökohtana

Kvantitatiivisten tutkimustulosten tulkinnassa ovat viime vuosikymmeninä
voimakkaasti yleistyneet niin kutsutut bayesiläiset tilastomenetelmät [@andrews2013, 2]. 
Bayesiläinen tulokulma on tavallisesti nähty vastakohtana perinteiselle
eli frekventistiselle tilastolliselle päättelylle, joka oli hallitseva
lähestymistapa koko 1900-luvun ajan (mts. 5). 

Frekventistisessä päättelyssä keskeisellä sijalla on sen toteaminen,
voidaanko niin kutsuttu nollahypoteesi hylätä vai ei. Yhden käytetyimmistä bayesiläisen päättelyn oppaista
kirjoittanut John Kruschke[^kruschkeopas] [-@kruschke2014doing, 298] käyttää tämän
vuoksi frekventistisestä metodiikasta termiä Null hypothesis significance
testing (NHST). 

[^kruschkeopas]: Onko hyvä heittää tällaista määrettä? Saisiko jostain lähdettä
Kruschken käytettyydelle?

Nollahypoteesimenetelmien (jatkossa NHST-menetelmät) olennainen rajoitus on,
että ne todella antavat mahdollisuuden ainoastaan hypoteesin kumoamiseen, eivät
vahvistamiseen. Andrewsin  ja Baguleyn [-@andrews2013, 3] mukaan juuri ajatus
*käänteisen todennäköisyyden* mittaamisesta oli 1700-luvulla eläneen Thomas
Bayesin ja 1800-luvulla eläneen Pierre-Simon Laplacen tärkein saavutus
tilastotieteen edistymisen kannalta. Ajatus käänteisestä todennäköisyydestä on
pohjimmiltaan yksinkertainen: jos NHST-menetelmien mukaisen merkitsevyystestin
perusteella voidaan osoittaa viiden prosentin todennäköisyys sille, että
kymmenen kertaa heitetty kolikko on epäreilu (tuottaa esimerkiksi kruunia
useammin kuin klaavoja), tutkija ei silti voi todeta, että todennäköisyys
kolikon reiluudelle olisi 95%. Bayesiläisissä menetelmissä etuna on se, että
tutkija pystyy suoraan arvioimaan sitä, kuinka varma hän on kolikon reiluudesta
-- ei vain tietyissä tapauksissa hylkäämään oletusta epäreiluudesta.

Bayesiläisen päättelyn ytimessä on ennakko-oletusten määritteleminen. Kruschke
demonstroi ennakko-oletusten mallintamisen tärkeyttä seuraavasti. Kuvitellaan,
että tutkijan olisi tehtävä päätelmä siitä, kannattaisiko jalkapallo-ottelun
aloittaja valita heittämällä kolikon sijasta *naulaa* 
[@kruschke2014doing, 315]. Naulaa heitettäessä kaksi mahdollista heittotulosta
ovat a) se, että naula putoaa kyljelleen sekä b) se, että naula jää seisomaan
kannalleen. Etenkin pienellä aineistolla (ainoastaan muutama heitto) mitattuna
NHST-menetelmin toimiva tutkija ei voisi perustellusti hylätä ajatusta siitä,
että naula saattaisi olla tasapuolinen väline aloittajan arpomiseksi.
Bayesiläinen tutkija taas pystyy sisällyttämään tilastolliseen malliinsa
perustellun ennakko-oletuksensa: sen, että naulan putoaminen kannalleen on
fysikaalisesti jollei mahdotonta niin ainakin vaikeaa ja siten äärimmäisen
harvinaista. 

Bayesiläinen päättely lähteekin siitä, että tutkija määrittelee
ennakko-oletuksensa jonkin tapahtuman todennäköisyydestä. Nämä ennakko-oletukset
ilmaistaan tilastollisten jakaumien (*priorijakauma*) muodossa. Tämän jälkeen
mallia *päivitetään*, eli katsotaan, missä määrin käsitys jonkin ilmiön
todennäköisyydestä muuttuu, kun otetaan huomioon ilmiötä kuvaavaa dataa. Myös 
muuttuneet käsitykset ilmaistaan jakaumana (*posteriorijakauma*).

## Tutkimuksessa käytettävä tilastollinen malli {#tutkimuksen-malli}


Nyt käsillä olevan tutkimuksen tärkeimpiä tavoitteita on  vastata kysymykseen
siitä, mitkä tekijät vaikuttavat ajanilmauksen sijoittumiseen suomessa ja
venäjässä -- toisin sanoen, mitkä piirteet ovat tyypillisiä suomen, mitkä taas
venäjän S1-, S2-, S3- ja S4-konstruktioille. Mikko Ketokivi [-@ketokivi2015, 131]
tähdentää omassa tilastollisen päättelyn johdantoteoksessaan, että
tämänkaltaiset tutkimuskysymykset ovat kysymyksiä, joissa yhden muuttujan
arvojen vaihtelua eli varianssia selitetään toisten muuttujien varianssilla.
Muuttujaa, jonka varianssia pyritään selittämään, on tavallisesti kutsuttu
esimerkiksi *riippuvaksi* muuttujaksi (dependent variable) tai y-muuttujaksi.
Muuttujat, joiden avulla yhden muuttujan varianssia pyritään selittämään, ovat
usein nimeltään *riippumattomia muuttujia* (independent variable). Käytän tässä
Ketokiven esittelemiä, mielestäni kuvaavia suomenkielisiä termejä *selitettävä*
ja *selittävä* muuttuja, jotka vertautuvat hyvin englanninkielisessä
kirjallisuudessa esiintyviin ja esimerkiksi Rolf Baayenin [-@baayen2012, 14]
suosittelemiin nimityksiin *predicted variable* ja *predictor variable*.




Selitettävänä muuttujana tässä tutkimuksessa on ajanilmauksen sijainti.
Kyseessä on kategorinen (nominaalinen)[^ordinaalimuut] muuttuja, jolle 
oli alun perin tarkoitus antaa neljä mahdollista arvoa: S1, S2, S3 ja S4. Tämä jaottelu
olisi kuitenkin heikentänyt mallin selitysvoimaa.

Jos katsotaan edellisessä luvussa esitettyjä yleishavaintoja eri sijaintien osuuksista
tarkastelluissa kielissä, voidaan todeta -- niin kuin tutkimuskirjallisuuden perusteellakin 
saatettiin olettaa -- että suomen ja venäjän välillä on lähtökohtainen, osin kieliopillinen
ero S2- ja S3-sijaintien käytössä. Suomi ei juurikaan hyödynnä S2-sijaintia (siihen 
osuu ainoastaan 5,57 % tapauksista); vastaavasti venäjä ei juurikaan hyödynnä
S3-sijaintia (johon osuu 4,89 % tapauksista). Toisin päin katsottuna S2 on venäjän
yleisin sijainti (49,13 %) ja S3 suomen yleisin sijainti
(47,60 %).
Tästä seuraa, että olisi melko hedelmätöntä vertailla keskenään suomen ja venäjän S3-sijainteja
tai venäjän ja suomen S2-sijainteja. Mielekkäämpien vertailujen mahdollistamiseksi 
sijainnit S2 ja S3 onkin tilastollisessa mallissa yhdistetty, niin että selitettävällä
muuttujalla on kolme arvoa: S1, S2/S3 ja S4. Nimitän näitä jatkossa myös *alku-*, *keski-*
ja *loppusijainneiksi.*


Jos käytettävässä tilastollisessa mallissa ei olisi yhtään selittävää
muuttujaa, malli vastaisi kysymykseen *mikä on minkäkin sijainnin
todennäköisyys*. Esitän alla, miten merkitsen näitä todennäköisyyksiä jatkossa
ja havainnollisuuden vuoksi liitän karkeimpana mahdollisena arviona kustakin
todennäköisyydestä tiedon siitä, kuinka suuri osuus aineistossa kuhunkin sijaintiin
sijoittuu:

- Alkusijainnin todennäköisyys:  P(S1) ~ 22.98 %
- Keskisijainnin todennäköisyys:  P(S2/S3) ~ 53.56 %
- Loppusijainnin todennäköisyys:  P(S4) ~ 23.46 %

Ymmärrettävästi tällaisenaan malli olisi koko lailla epäinformatiivinen. Sen
avulla voisi ainoastaan tehdä päätelmiä siitä, millä todennäköisyydellä
satunnaisesti valittu suomen- tai venäjänkielinen positiivinen SVO-lause sisältää S1-, S2-,
S3- tai S4-sijoittuneen ajanilmauksen. Olennaisin selittävä muuttuja onkin
kunkin tapauksen kieli, jolloin tutkimuskysymys muuttuu muotoon *Mikä on
sijainnin Y todennäköisyys, jos tiedossa on, että kyseessä on kieltä X edustava
lause?* Tällaiset ehdolliset todennäköisyydet merkitsen pystyviivan avulla, niin
että S1-sijainnin todennäköisyys suomenkieliselle lauseelle merkittäisiin
$P(S1|lang=fi)$, S3-todennäköisyys venäjänkieliselle lauseelle $P(S3|lang=ru)$ ja
niin edelleen. 

[^ordinaalimuut]: Voisi esittää, että tässä käytetyt neljä sijaintimuuttujan
arvoa tekevät muuttujasta itse asiassa ordinaalimuuttujan eli muuttujan, jossa
eri arvojen järjestyksellä on merkitystä. Tätä lähestymistapaa en kuitenkaan
noudata, sillä järjestyksen selitysvoima sinänsä ei ole niin suuri, että sitä
olisi mieltä erikseen mallintaa. Olennaista on kysymys siitä, mikä neljästä
sijaintikategoriasta jossakin datapisteessä havaitaan.



\todo[inline]{Perustele lausetyyppiä ja sitä, että ei a-konjunktiota mukana}

\todo[inline]{Käsittele myös referentiaalisuuden typistetty esittäminen}


\todo[inline]{OLISI HALUTTU: referentiaalisuus, mutta sitä ei ollut mahdollista operationalisoida
järkevästi. Ei olisi myöskään ollut järkevää ottaa yhtä muuttujaa,jossa on 52 eri arvoa.
Näin ollen päädyttiin ottamaan selkeimmin aineistoa ryhmitteleväksi muuttujaksi semanttinen funktio,
joka on jossain määrin kompromissi luvusta 1. Sitä pidetään suuntaviivana, ja esimerkiksi referentiaalisuutta
tutkitaan ja tarkastellaan selittävänä tekijänä tämän muuttujan *sisällä*.
}


\todo[inline]{

SELOSTA myös..

}

### Selittävät muuttujat

\todo[inline]{AJATUS: OTA varsinaiseen isoon malliin mukaan vain morph + funct + pos... 
Mainitse jo tässä ja huomioi myöhemmin, että yksittäisiä kohtia voidaan selittää
myös clausestatus, corpus tms. avulla.
}

Tutkimuksen johdannossa esitettiin yhdeksi tärkeimmistä tavoitteista selittävien
tekijöiden löytäminen ajanilmauksen sijainnille. Luvussa  \ref{aimm} pyrittiin
määrittelemään mahdollisimman tarkasti, mitä *ajanilmauksella* tässä
tutkimuksessa tarkoitetaan ja minkälaisia erilaisia ajanilmauksia voidaan
erottaa. Nämä erilaisten ajanilmausten erot olivat tärkein lähtökohta myös
määriteltäessä niitä selittäviä muuttujia, joita tutkimuksessa käytetään.

Mahdollisiksi selittäviksi muuttujiksi voidaan taksonomioiden 1, 2 ja 3 pohjalta (ks. alaluvut \ref{taksn1}, \ref{taksn2} ja \ref{taksn3})
erotella *muodostustapa*, *semanttinen funktio* ja *referentiaalisuus.* Lisäksi yksi
vaihtoehto olisi ollut tarkastella aineistoryhmää yhtenä ainoana muuttujana, jolla tässä tapauksessa 
olisi ollut 50 eri arvoa ja joka ei olisi säilyttänyt
edellä muodostettujen taksonomioiden kautta saatua informaatiota erilaisten ajanilmausten luonteesta.
Muita oletettavasti sijaintiin vaikuttavia ja aineistosta mitattavissa olevia tekijöitä ovat lausetyyppi  eli
se, onko kyseessä esimerkiksi alisteinen sivulause, relatiivilause tai kysymyslause sekä 
mahdollisesti korpus, josta lause on peräisin.




Ajanilmauksen muodostustapa operationalisoitiin lopullisessa tilastollisessa
mallissa siten, että siitä erotettiin kaksi mitattavaa ominaisuutta:
ajanilmauksen morfologinen rakenne  ja ajanilmauksen positionaalisuus.

Morfologisella rakenteella  tarkoitetaan tässä yhteydessä lähtökohtaisesti sitä, ovatko
ajanilmaukset nominaalisia kuten ilmauksessa *viime vuonna* vai adverbisiä
kuten ilmauksessa *kauan*. Adverbisistä ajanilmauksista eroteltiin kuitenkin omaksi joukokseen
*deiktiset adverbit*, joihin laskettiin aineistoryhmät L1a--L1c,L5c ja L8a--L8c
(ks. luvut \ref{l1a-l1b-l1c}, \ref{l5a-l5b} ja \ref{l8a-l8b-l8c} edellä). Muita adverbisiä aineistoryhmiä
ovat E5b, E6a--E6b, F2b, F3a--F3e sekä P1--P2 (luvut \ref{e5a-e5b}--\ref{e7},\ref{f2a-f2b}--\ref{f4},  \ref{l7a-l7b-l7c-l7d} ja \ref{p1-p2}).
Morfologinen rakenne -muuttujan eri arvot sekä niiden suhteelliset kielikohtaiset osuudet on esitetty
seuraavassa kuviossa:


![**Kuvio 2: ** Morfologinen rakenne -muuttujan arvot ja kielikohtainen jakautuminen](figure/morph.counts-1.pdf)

\todo[inline]{TODO: mitkä adverbisistä oikeasti pronominaalisia, mainitse erikseen}

Kuvio 2 osoittaa, että aineistoryhmien
kokoeroista johtuen morfologisen rakenteen jakautuminen muuttujan eri arvojen välillä 
ei ole täysin samanlainen suomessa ja venäjässä. Kummassakin kielessä deiktiset adverbit muodostavat
kuitenkin pienimmän joukon.

Positionaalisuutta mitattiin tutkimusaineistossa kahdella arvolla: 0 (ei
positionaalinen) ja 1 (positionaalinen). Positionaalisia aineistoryhmiä
tutkimusaineistossa edustavat ryhmät L2a-L2b (luku \ref{l2a-l2b}), L4a
(\ref{l4a-l4b} ) ja F1b (\ref{f1a-f1b}), mikä tarkoittaa, että suurin osa
kaikista tutkimusaineiston tapauksista on ei-positionaalisia. Suomessa
positionaalisten tapausten osuus kaikista on
11,71
ja venäjässä 
6,44 prosenttia.

Morfologisen rakenteen ja positionaalisuuden lisäksi tilastollisessa mallissa
otettiin kolmantena selittävänä muuttujana huomioon ajanilmauksen semanttinen funktio.
Ajanilmaukset ryhmiteltiin eri semanttisiin funktioihin luvussa \ref{taksn2} esitellyn
toisen ajanilmaustaksonomian perusteella siten, että tuloksena saatiin kuvion
 3 mukainen jakauma, joka kattaa kaiken kaikkiaan
11 eri kategoriaa:[^katsel]


[^katsel]: Eri kategorioista käytettyjen lyhenteiden selitykset on annettu luvun \ref{aineistoryhmuxe4t} alussa

![**Kuvio 3: ** Semanttinen funktio -muuttujan arvot ja kielikohtaiset suhteelliset osuudet](figure/semf.tab.counts-1.pdf)

Kuvio 3 osoittaa, että niin suomessa kuin venäjässäkin tavallisimman 
semanttinen funktio -muuttujan kategorian muodostavat simultaanista lokalisoivaa funktiota edustavat
tapaukset, joita kummassakin kielessä on liki puolet kaikista tapauksista. Vähiten on likimääräistä
funktiota edustavia tapauksia. 

Edellä lueteltujen kolmen selittävän muuttujan lisäksi luvussa \ref{taksn3}
esitetyn kolmannen, referentiaalisuuteen liittyvän taksonomian perusteella oli
alun perin tarkoitus rakentaa vielä yksi selittävä muuttuja. Kävi
kuitenkin ilmi, ettei referentiaalisuuden kuvaaminen
omana erillisenä muuttujanaan tarkentanut tilastollista mallia, vaan se informaatio,
joka erillisellä referentiaalisuus-muuttujalla aineistosta olisi voitu saada,
saatiin selville myös semanttista funktiota tarkastelemalla. Lisäksi malliin 
kokeiltiin liittää selittävinä muuttujina edellä mainittuja
lähdekorpusta ja lausetyyppiä. Näiden selitysvoima koko aineiston
kannalta havaittiin kuitenkin olemattoman pieneksi, minkä seurauksena nekin suljettiin
pois varsinaisesta, koko aineistoon sovellettavasta mallista. Niin lähdekorpusta kuin sovelletusti
myös lausetyyppiä käytettiin kuitenkin vain tiettyjä aineistoryhmiä tarkastelevissa
pienemmissä tilastollisissa malleissa (ks. esim. alaluvut \ref{tilastollinen-malli-johdantokonstruktion-sijaintien-kartoittamiseksi}
ja  \ref{deiktiset-adverbit-ja-positionaalisuus}). 

Tutkimusta lähdettiin tekemään edellä kuvatulla mallilla, jossa kielen lisäksi 
on kolme selittävää muuttujaa. Tutkimuksen kuluessa
 havaittiin kuitenkin, että malliin olisi mielekästä sisällyttää informaatiota myös
siitä, onko ajanilmauksen sisältämässä lauseessa läsnä sanoja, jotka on automaattisessa
morfologisessa analyysissa merkitty *numeraaleiksi*. Näin ollen lopullinen malli
sisälsi myös viidennen selittävän muuttujan, *numeraalin läsnäolon*. Tämä muuttuja 
ja perustelut sen käyttämiseksi on esitetty osiossa \ref{kehres-s1}.


\todo[inline]{Kysy ehkä Michaelilta tarkemmin, miten kuvata mallin eri vaihtoehtojen kokeilemista}

### Mallin kuvaus kootusti


Tilastollinen malli, joka edellä kuvattujen selitettävän ja selittävien
muuttujien perusteella rakennettiin, on luonteeltaan regressiomalli
[@ketokivi2015, 134]. Kenties tavallisin regressiomallin muoto on
lineaarinen regressiomalli, jossa mitataan jatkuvien muuttujien, esimerkiksi
ihmisen painon ja viikoittaisen liikuntaan käytetyn ajan, yhteyttä. Kun
selitettävä muuttuja on, niin kuin tässä, kategorinen, käytetään tavallisesti
*logistista regressiomallia*. Nyt käsillä olevassa tapauksessa, kun
selitettävällä kategorisella muuttujalla on enemmän kuin kaksi mahdollista
arvoa, kyseessä on malli, jota Ketokivi (mts. 135) nimittää *kategoriseksi
regressioksi* ja esimerkiksi Kruschke [-@kruschke2014doing, 668] *ehdolliseksi
logistiseksi regressioksi* (*conditional logistic regression*).

Tarkastellaan tässä käytettävää kategorista regressiomallia lähemmin mallin
rakennetta havainnollistavan kuvion X avulla.

(kuvio puuttuu vielä)

Kuviosta nähdään, että ennakkokäsitykset kunkin muuttujan ja niiden välisten
interaktioiden välillä on ilmaistu puoli-Cauchy-jakaumilla (Gelman-viite). 
(tämä osio siis vielä kirjoittamatta, koska tilastollisen mallin tarkka muoto
vielä osin työn alla).
Koko malli rakennettiin hyödyntämällä R-ohjelman rjags-kirjastoa [@rjags]. Itse mallin kuvaus
kirjoitettiin bugs-muodossa ja se on nähtävillä Bitbucket-palvelussa osoitteessa
https://tinyurl.com/y9uagcfr (tarkistettu 15.3.2018).


\todo[inline]{Mainitse ROPE ja HDI.. Mainitse myös @mustanoja2011idiolekti, 106--109}


## Mallissa havaittavat vaikutukset yleisellä tasolla {#ylvaik}


\todo[inline]{ks. onko esim. mustanojalla viitettä keskihajontojen käyttämiseen}

Tarkastelen seuraavaksi
yleisellä tasolla, miten hyvin edellä luetellut muuttujat tässä esitellyn
tilastollisen mallin perusteella selittävät aineistossa havaittavaa
sijaintimuuttujan varianssia. Arvioitaessa eri muuttujien vaikuttavuutta yksi tärkeimmistä
mittareista ovat muuttujien keskihajonnat: mitä lähempänä nollaa keskihajonta
on, sitä pienempi selitysvoima muuttujalla voidaan nähdä. Tässä
käytettyjen selittävien muuttujien keskihajonnat käyvät ilmi kuviosta 
4:


![**Kuvio 4: ** Selittävien muuttujien keskihajonnat](figure/std.all-1.pdf)

Kuvion 4 kaltaisia kuvioita kutsutaan tavallisesti termillä
*caterpillar plot*. Tässä käytetyt caterpillar-kuviot on luotu R-ohjelman
*ggmcmc*-paketilla [@ggmcmc], ja koska ne ovat tutkimuksen kannalta olennainen
keino kuvata tilastollisia havaintoja, annan tässä kohtaa muutamia suuntaviivoja
niiden tulkitsemiseksi.

Caterpillar-kuvioiden y-akselilla ovat tarkkailtavat muuttujat, kuvion 
4 tapauksessa kielen (std.lang), morfologisen rakenteen
(std.morph), semanttisen funktion (std.funct) ja positionaalisuuden
(std.pos) keskihajonnat. X-akseli puolestaan kuvaa arviota
kyseisen muuttujan vaikutuksen voimakkuudesta tai tarkemmin sanottuna siitä, kuinka paljon
varianssia muuttuja selittää. Kuviossa 4 kyse on keskihajonnoista,
joten x-akseli lähtee nollasta (keskihajonta ei voi olla negatiivinen) eikä
sillä ole varsinaista ylärajaa. Kuten edellä selitettiin, tässä luvussa käytetyn
tilastollisen mallin linkkifunktio on logaritminen, minkä tähden useimmissa
luvun kuvioissa X-akselin arvot voivat olla myös negatiivisia. Kuvioissa
esitettyjä lukuja ei ole järkevää yrittää tulkita absoluuttisesti, vaan eri
muuttujien arvoja on verrattava toisiinsa. Nyrkkisääntönä voidaan ajatella, että
mitä kauempana nollasta X-akselin "toukat" ovat, sitä suurempi on kyseisen
muuttujan vaikutus. 

Kuviosta 4 nähdään, että std.lang-muuttujan kohdalla
x-akselille on piirretty kolmeosainen kuvio: ohut viiva, paksumpi viiva sekä
ympyrä. Viivat kuvastavat sitä, kuinka paljon tilastollinen malli on muuttanut
ennakkokäsitystä kielen vaikutuksesta eri sijainteihin -- toisin sanoen viivat
esittävät posteriorijakauman kielen keskihajonnalle. Ympyrä osoittaa jakauman
mediaanin: sen, mikä on oletettavasti paras yksittäinen arvio vaikutuksen
voimakkuudesta. Paksumpi viiva osoittaa, mille välille 65% \todo[inline]{TARKISTA} arvioista 
osuu ja ohuin viiva sen, mille välille osuu 95% kaikista arvioista (ks. *HDI*
edellä). Kuviossa 4 osa arvioista 
(morfologisen rakenteen ja positionaalisuuden keskihajonnat)
on niin tarkkoja, että ohuita
ja paksuja viivoja ei kuvasta juuri voi erottaa. Kokoavasti
voidaan sanoa, että semanttisen funktion vaikutus näyttäisi ensi katsomalta kaikkein
suurimmalta -- tähän arvioon samoin kuin toisiksi voimakkaampana näyttäytyvään 
kieli-muttujaan liittyy kuitenkin sen verran epävarmuutta, että näiden muuttujien
keskinäiinen järjestys voisi olla myös päinastainen. Yhtä kaikki, nämä kaksi
muuttujaa erottuvat selvästi muista.

\todo[inline]{Käytä Kruschken kahviesimerkkiä edellä logaritmisuuden selittämisessä}

Caterplot-kuvioiden lisäksi luvussa 5 tarkastellaan paikoitellen myös
varsinaisia posterijakaumia. Tällöin kyseessä voi olla joko yhden tietyn
muuttujan jakauma tietylle arvolle tai tavallisemmin kahden muuttujan tai arvon
välinen vertailu. Kuvio 5 esittää tarkemmin sen,
minkä arvion käytetty tilastollinen malli antaa semanttisen funktion
vaikutuksesta. Vastaavat jakaumat on piirretty edellä mainitun John Kruschken
[-@kruschke2014doing] teoksen tarjoamien R-funktioiden avulla.


![**Kuvio 5: ** Semanttisen funktion vaikutuksen posteriorijakauma](figure/morphsdpost-1.pdf)

Kuviosta 5 havaitaan, että arvio semanttisen 
funktion vaikutuksesta todella on sekin lopulta kohtalaisen tarkka. Kuviossa 4
näkyvä mediaani on noin 0,660, ja  95% mahdollisista arvoista osuu
välille 0,627 -- 0,696.


Kuvio 4 on merkittävä muutenkin kuin vain
keinona esitellä caterplot-kuvioiden tulkintaa. Se, että kielellä todella on
paljon -- mahdollisesti jopa eniten -- vaikutusta selitettävän muuttujan
saamiin arvoihin, vahvistaa edellä puhtaiden prosenttiosuuksien perusteella
tehdyn päätelmän siitä, että suomen ja venäjän välillä todella on
systemaattinen ero siinä, miten ajanilmaukset jakautuvat eri sijainteihin.

Jos kielen vaikutusta tutkitaan tilastollisen mallin valossa tarkemmin, saadaan
itse asiassa samankaltainen vaikutelma kuin edellä puhtaita prosenttiosuuksia
tarkasteltaessa. Tämä näkyy kuviosta 6, joka
esittää, miten todennäköinen kukin tarkastelluista neljästä sijainnista on
*suomessa verrattuna venäjään*. Juuri tämä *suomi verrattuna venäjään*
-näkökulma on luvuissa \ref{ajanilmaukset-ja-alkusijainti} --
\ref{ajanilmaukset-ja-loppusijainti} käytäntönä, kun vastaavia kuvioita
tarkastellaan enemmän. Kuten todettu, bayesiläisittäin rakennettu tilastollinen
malli toimii siten, että ensin kullekin sijainnille määritellään jokin
ennakkoarvio sen todennäköisyydestä. 
Kysyttäessä todennäköisyyttä $P(S1|fi)$[^propnot] kysytään lopulta sitä,
minkä verran ja mihin suuntaan arvio S1-sijainnin todennäköisyydestä muuttuu,
jos lang-muuttujan kahdesta mahdollisesta arvosta valitaan *fi* (suomi). Jos
tarkasteltavalla selittävällä muuttujalla on kaksi arvoa, kuten kielen
tapauksessa, on eri arvojen (*fi* ja *ru*) vaikutus täsmälleen toistensa
peilikuva: jos *fi*-arvo aiheuttaa oletusarvoon poikkeaman -0.5, on vastaava
*ru*-arvon aiheuttama poikkeama tällöin +0.5. Tämän vuoksi jatkossa
vertailtaessa suomea ja venäjää kuvioissa näytetään ainoastaan fi-arvoa
koskevat posteriorijakaumat -- ru-arvon jakaumat ovat aina näiden
käänteislukuja. 

[^propnot]: Koska kulloinkin käsiteltävät selittävät muuttujat käyvät aina ilmi
itse käsittelyn kontekstista, jätän todennäköisyyttä kuvaavissa merkinnöissä jatkossa muuttujan
nimen ilmaisematta, niin että merkintä *fi* tarkoittaa 'kieli-muuttuja arvolla fi'.

![**Kuvio 6: ** Kielen vaikutus eri sijaintien todennäköisyyteen](figure/justlang-1.pdf)


Kuvio 6 havainnollistaa hyvin suomen ja venäjän
välisiä eroja eri sijaintien käytössä. Kuviosta nähdään, että jos kielen tiedetään  olevan suomi, 
on S4-sijainti selvästi oletusarvoa todennäköisempi ja S1-sijainti vastaavasti
epätodennäköisempi. S2/S3-sijainti taas on mitä luultavimmin jotakuinkin yhtä tavallinen 
kielestä riippumatta, joskin on täysin mahdollista, että suomessa S2/S3 on jonkin verran
venäjää epätodennäköisempi. Kuvio 6 toimii
mallina siitä, miten vastaavia logaritmiselle asteikolle sijoittuvia kuvioita tulisi
tämän tutkimuksen yhteydessä tulkita: positiiviset arvot tarkoittavat, että
jokin selitettävän muuttujan arvo eli ajanilmauksen sijainti on
oletustodennäköisyyttä
(sijaintimuuttujan arvoa koskeva priori) todennäköisempi ja negatiiviset puolestaan sitä, että
jokin sijainti on oletustodennäköisyyttä epätodennäköisempi. Kuvion 6 
kaltaisissa suomea ja venäjää vertaavissa kuvioissa on lisäksi täydennettävä, että
"oletustodennäköisyyttä todennäköisempi" ja "oletustodennäköisyyttä epätodennäköisempi"
tulee ymmärtää tarkasteltuna suomessa verrattuna venäjään.

\todo[inline]{tarkenna edellistä niin, että puhutaan vetosuhteesta.}

Palataan vielä keskihajontoja käsittelevään kuvioon 4 .
Edellä jo todettiin, että semanttisella funktiolla näyttäisi
olevan lähes yhtä suuri merkitys ajanilmauksen sijainnin kannalta 
kuin kielellä. Myös morfologisella rakenteella, positionaalisuudella ja
numeraalin läsnäololla
keskihajonnat erottuvat nollasta, mikä tarkoittaa, että kaikki
mainitut muuttujat todella selittävät ainakin jonkin verran 
sijaintimuuttujan arvojen vaihtelua.
Tämän tutkimuksen kannalta olennaisimmat havainnot eivät kuitenkaan liity pelkästään
kieliin tai pelkästään muihin selittäviin muuttujiin, vaan ennen muuta siihen,
miltä osin semanttinen funktio, morfologinen rakenne, positionaalisuus ja
numeraalin läsnäolo vaikuttavat
*kielestä riippuen* eli interaktiossa kielimuuttujan kanssa. Voidaanko
esimerkiksi sanoa sekä 1) *jos ajanilmaus on morfologiselta rakenteeltaan
nominaalinen, sillä on yleisesti
ottaen adverbistä ajanilmausta suurempi todennäköisyys sijoittua lauseen
loppuun*  että 2) *jos kyseessä on venäjänkielinen ajanilmaus, havaittu
vaikutus on suurempi verrattuna suomenkieliseen ajanilmaukseen*
(jälkimmäistä todennäköisyyttä voitaisiin muodollisemmin merkitä
$P(S4|ru,NP)$)?.

Kuvio 7 esittää, minkä verran kielen ja muiden
selittävien muuttujien interaktioilla on mallin mukaan vaikutusta. Vaikutukset
ovat pienempiä ja osin epävarmempia kuin muuttujilla ilman interaktion huomioimista,
mutta eroja on yhtä kaikki havaittavissa.

![**Kuvio 7: ** Kielen ja muiden selittävien muuttujien välisten interaktioiden keskihajonnat](figure/sd.interact-1.pdf)

Kuviossa 4 havaittu semanttisen funktion voimakas vaikutus näkyy
kuvion 7  mukaan myös yhdistettynä kieleen, niin 
että juuri kieli + semanttinen funktio -yhdistelmä erottuu selitysvoimaltaan 
selvästi muista selittävistä muuttujista. Positionaalisuuden ja numeraalin 
läsnäolon kielisidonnaiset vaikutukset ovat lähellä toisiaan, ja morfologinen
rakenne sijoittuu vaikutukseltaan näiden kahden ja semanttisen funktion välimaastoon.



## Analyysin rakenne 

Kontrastiivista tutkimusta tehdessä asioiden esittämisjärjestyksen
määrittäminen on välillä ongelmallista. Koska tutkittavaa ilmiötä on
vertailtava monesta näkökulmasta, mikä tahansa valittava käsittelyjärjestys
kärsii jonkin verran siitä, että tiettyjä asioita on jo vertailtu aiemmin ja
toisaalta siitä, että tiettyjen asioiden vertailu tapahtuu vasta myöhemmin.
Olen tässä tutkimuksessa jakanut tilastollisen mallin tulkinnan ja aineiston varsinaisen analyysin
siten, että käsittelen kutakin kolmesta selitettävän muuttujan arvosta omana lukunaan.
Luvussa  \ref{ajanilmaukset-ja-alkusijainti} keskitytään siis alkusijaintiin
eli S1-konstruktioihin, luvussa \ref{ajanilmaukset-ja-keskisijainti} taas keskisijaintiin
eli S2- ja S3-konstruktioihin ja lopulta luvussa
\ref{ajanilmaukset-ja-loppusijainti} loppusijaintiin ja S4-konstruktioihin.

Sijaintikohtaisilla käsittelyluvuilla on karkeasti katsottuna kaksi päämäärää.
Ensinnäkin tarkoituksena on kartoittaa, minkälaisten konstruktioiden osina
ajanilmaukset kussakin sijainnissa kummassakin kielessä toimivat. Toiseksi
tavoitteena on löytää niitä eroja, joita kuhunkin sijaintiin ja sitä
hyödyntäviin konstruktioihin suomen ja venäjän välillä liittyy.
Sijaintikohtaisten lukujen jälkeisessä kokoavassa luvussa
\ref{kokoava-kontrastiivinen-analyysi} suoritetaan varsinainen kontrastiivisen
funktionaalisen analyysin menetelmää mukaileva samanlaisuushypoteesien
arviointi: verrataan luvuissa \ref{ajanilmaukset-ja-alkusijainti} --
\ref{ajanilmaukset-ja-loppusijainti} tehtyjä havaintoja ja määritellään, mitä
suomen ajanilmauskonstruktioita millekin venäjän ajanilmauskonstruktioille
voidaan pitää vastineina ja minkälaisissa tilanteissa.