---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
fontsize: 11.5pt
geometry: twoside, b5paper, layout=b5paper, left=2cm, right=2cm, top=2cm, bottom=3cm, bindingoffset=0.5cm
indent: true
subparagraph: yes
output:
    html_document:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
    pdf_document: 
      latex_engine: xelatex
      toc: true
      toc_depth: 6
      number_sections: true
      keep_tex: true
      includes:
          in_header: preamble.tex
---


Ajan ilmaisemisen monet muodot {#aimm}
===============================

Ajanilmauksen käsite {#ajanilm-kasite}
--------------------

Aikaa ilmaistaan luonnollisissa kielissä monin tavoin. Ajan
ilmaisumuotoja eriteltäessä on perinteisesti lueteltu ainakin verbien
aikamuodot ja aspekti, ajan adverbiaalit sekä esimerkiksi kiinassa
esiintyvät temporaalipartikkelit [@klein1999, 143]. Näistä kategorioista
tämän tutkimuksen fokusta kuvaa parhaiten nimenomaan aikaa
ilmaisevien adverbiaalien ryhmä.

Adverbiaali on tavallisesti ollut suhteellisen hämärärajainen
määriteltävä.  Esimerkiksi Iso suomen kielioppi luokittelee adverbiaalin
yleisnimitykseksi sellaisten lausekkeiden lauseenjäsentehtävälle, jotka
ovat muussa muodossa kuin kieliopillisessa sijassa [@visk, määritelmät].
Määritelmä on kaikkea muuta kuin täsmällinen ja kuvastaa sitä, että
adverbiaali on yleensäkin tavattu määritellä negaation kautta:
adverbiaaleja ovat ne elementit, jotka eivät ole esimerkiksi subjekteja tai
objekteja [@huumo1997, 25].

Vaikka eri ajan ilmaisukeinoista ajan adverbiaalin käsite siis osuukin
lähimmäs tässä tutkimuksessa tarkasteltavia ilmiöitä, ei se sellaisenaan ole
käyttökelpoinen. Negaatiomenetelmää edelleen mukaillen määritänkin
ajanilmauksen käsitteen tätä tutkimusta varten seuraavasti:\linebreak

\noindent 
\noindent \headingfont \small \textbf{Määritelmä 1}: Ajanilmauksia ovat *sanat,
lausekkeet  tai lauseet joiden avulla lauseessa tai
virkkeessä ilmaistaan aikaa mutta jotka eivät ole verbejä
vaan nomineista tai adverbeista muodostuvia verbien muita
kuin subjektiksi tai predikatiiveiksi luokiteltavia
täydennyksiä tai määritteitä*. \normalfont \vspace{0.6cm}
 \linebreak 



Vaikka esimerkiksi edellä kuvattuun ISKin määritelmään on
lisätty erikseen myös fennistiikassa *osmaksi* nimitetyt
elementit (kuten esimerkin \ref{ee_osma} maanantai), ovat nämä
koneellisessa analyysissä kuitenkin objekteja [@tobecited],
minkä vuoksi objekteja ei voi jättää määritelmän
ulkopuolelle.  Subjektin funktio sen sijaan aiheuttaa
potentiaalisesti aikaa ilmaisevan sanan jäämisen tämän
tutkimuksen ulkopuolelle, sillä vaikka esimerkin \ref{ee_eisubj}
*maanantain* periaatteessa voi tulkita aikaa ilmaisevaksi, on
ilmaisun ajallisuuden luonne jo hyvin toisenlainen ja paljon
implisiittisempi kuin esimerkiksi juuri lauseessa \ref{ee_osma}.




\ex.\label{ee_osma} \exfont \small Matias nukkui koko maanantain





\vspace{0.4cm} 

\noindent



\ex.\label{ee_eisubj} \exfont \small Maanantai oli mukava päivä.





\vspace{0.4cm} 

\noindent

Määritelmä 1 on luonteeltaan puhtaan syntaktinen. On
kuitenkin mielekästä kysyä myös, mitä tarkoittaa lause *joiden avulla lauseessa
tai virkkeessä ilmaistaan aikaa* -- toisin sanoen  annettava myös jonkinlainen
semanttinen määritelmä ajan ilmaisemiselle. Tämä on lopulta kauas käsillä olevan
tutkimuksen ulkopuolelle ulottuva filosofinen kysymys, eikä tässä pyritäkään
semanttisen määritelmän osalta vastaavaan  tarkkuuteen kuin määritelmässä 
1. Joitain suuntaviivoja voidaan kuitenkin antaa.

Lähtökohtaisesti voidaan todeta, että ajan ja temporaalisuuden käsite ovat
perustavanlaatuinen osa jokaisen ihmisen kokemusmaailmaa  [@jaszczcolt2012, 1].
Tästä huolimatta temporaalisuuden määritteleminen ei ole yksiselitteistä tai
suoraviivaista -- ajan käsitteen ympärillä tuntuu vallitsevan tiettyä
mystisyyttä ja kuvauksen saavuttamattomuutta, niin että esimerkiksi paikan
käsitteisiin verrattuna ajan käsitteellinen vangitseminen on selvästi vaikeampaa
[@langacker2012, 203]. Tunnettu ja myös empiiristä tukea saanut hypoteesi onkin,
että aikaa ilmaistaan kielessä käsitteillä, joilla alun perin on ilmaistu juuri
paikkaa [@haspelmath1997space, 140; @evans2003structure, 13]. 

Aikaa itseään on pyritty määrittelemään muun muassa tapahtumien käsitteen
avulla: esimerkiksi Sinhan ja Gärdenforsin [-@sinha14,1] mukaan ajan käsite
koostuu *tapahtumien ja tapahtumien välisten suhteiden kognitiivisista ja
kielellisistä representaatioista*. Ajatus ajasta puhtaasti kognitiivisena,
ainoastaan ihmismielessä sijaitsevana konseptina ei kuitenkaan ole ainoa eikä
edes tavallisin käsitys: aikaa voidaan lähestyä myös fysikaalisena ilmiönä,
meitä ympäröivän maailman objektiivisesti olemassa olevana komponenttina
[@evans2003structure, 4].  Sillä, onko aika todellinen osa fyysistä maailmaa vai
*ainoastaan* ihmismielen luoma konsepti, ei kuitenkaan ole tämän tutkimuksen kannalta
merkitystä.  Koska tutkimuksen tarkoitus on nimenomaan ymmärtää sitä, miten
kahdessa eri kielessä heijastuu ihmisen kognition tuottama käsitys ajasta,
lähdetään tässä liikkeelle ajasta inhimillisenä kokemuksena, joka vaatii ilmaisukeinonsa.
Tästä lähtökohdasta käsin voidaan antaa seuraava tarkoituksellisen summittainen
semanttinen määritelmä ajan ilmaisemiselle: \linebreak


\noindent 
\noindent \headingfont \small \textbf{Määritelmä 2}: Ajan ilmaiseminen tarkoittaa
 sitä, miten kieli ilmentää kognition kuvaa tapahtumien suhteista,
 sijainneista ja ekstensiosta aika-akselilla. \normalfont \vspace{0.6cm}
 \linebreak 



Aika-akselin käsitteessä näkyy ajan hahmottaminen paikan kategorian kautta.
Siinä missä paikkaa ilmaistaan kolmella akselilla (ylös-alas, sivuille,
eteen-taakse), aika ymmärretään tavallisesti yhdeksi jatkuvaksi akseliksi, joka
tavallisesti vertautuu juuri eteen-taakse-akseliin [@haspelmath1997space, 21].

Ajanilmausten kolme taksonomiaa
-----------------------------------

Ajanilmauksia voidaan luokitella monista eri lähtökohdista. Tässä käyttämäni
ajanilmausten jaottelu koostuu kolmesta eri tason  taksonomiasta. Jossain määrin vastaavan
jaottelun voi löytää esimerkiksi M.V. Vsevolodovan teoksesta Способы выражения
временных отношений в русском языке [-@vsevolodova1975], ja yhtymäkohdat
Vsevolodovan jaotteluun pyritäänkin tekemään eksplisiittisiksi. Kolme tässä
tarkasteltavaa osa-aluetta ovat

-  Taksonomia 1: Mistä kielellisistä aineksista ajanilmauksia muodostetaan
    [vrt. Vsevolodovan "sanojen luokat", -@vsevolodova1975,
    29]? 

- Taksonomia 2: Mitä semanttisia funktioita ajanilmauksilla on [vrt.
   Vsevolodovan ajallisten suhteiden tyypit, -@vsevolodova1975,
   18].

- Taksonomia 3: Millainen on se kiintopiste aikajanalla, johon ajanilmaus
   suhteutetaan?

### Taksonomia 1: ajanilmausten muodostustavat ja alkuperä {#taksn1}

#### Potentiaalisesti kalendaariset ajanilmaukset {#pot-kal}

Yksiköt, joilla aikaa ilmaistaan, eivät ole identtisiä kielestä ja kulttuurista
toiseen. Kahden hyvin läheisenkin kieliyhteisön välillä voi olla eroja
esimerkiksi siinä, miten aika tyypillisen valveillaolon ja nukkumisen  välillä
jaksotetaan eri osiin.

On kuitenkin olemassa joukko peruskäsitteitä, jotka ovat enemmän tai vähemmän
universaaleja. Näiden käsitteiden voi ainakin välillisesti nähdä rakentuvan
luonnossa havaittavien säännöllisesti toistuvien tapahtumien ympärille.
Tällaisia tapahtumia ovat valoisan ja hämärän ajan vaihtelu, kuun näyttäytyminen
maasta katsovalle havainnoijalle ja auringon kierrosta aiheutuva olosuhteiden
vaihtelu [@fillmore1975santa, 249]. Vaikka etenkin länsimaisessa
nyky-yhteiskunnassa aikakäsityksen on esitetty muuttuneen luonnon syklisten
tapahtumien muovaamasta lineaariseksi, muodostavat näihin luonnon tarjoamiin
perusyksiköihin suoraan tai välillisesti pohjautuvat ajanyksiköt yhtä kaikki
selkeän ilmausjoukon[^syklVSlin]. Nimitän tätä joukkoa *potentiaalisesti
kalendaarisiksi* ajanilmauksiksi. 

[^syklVSlin]: Tähän vähän syklinen vs lineaarinen. -polemiikkia ja
Viimarannan / Leinon toteamukset siitä, kuinka totta tämä itse
asiassa on...


Luonnossa havaittaviin syklisiin tapahtumiin ovat oletettavasti alun perin
perustuneet lähinnä vuoden, kuukauden ja päivän(vuorokauden) käsitteet
[@nocite]. Useimmat nykyiset ihmisyhteisöt käyttävät lisäksi näistä johdettuja
ajanyksiköitä: vuorokausi jaetaan 24 tuntiin, nämä edelleen 60 minuuttiin, nämä
60 sekuntiin. Päivien perusteella on myös johdettu viikon käsite [historiasta
ks. @zerubavel1989seven]. Erotuksena näistä tarkkarajaisista yksiköistä voidaan
omaksi ryhmäkseen katsoa päivän osat ja vuodenajat, joiden alku- ja loppurajat
ovat häilyviä ja jossain määrin subjektiivisia[^kultkoht] [@fillmore1975santa,
251].

[^kultkoht]: Lisäksi on kiinnostava kysymys, missä määrin
nämä sumearajaisemmat ajanyksiköt ovat kulttuuri- tai
kieliyhteisökohtaisia [vrt. @harme17].


Fillmore [-@fillmore1975santa, 251] jaottelee omaksi ryhmäkseen sellaiset
toistuvat sekvenssit, joiden yksittäiset komponentit on nimetty. Tähän ryhmään
luetaan jo mainitut päivän osat sekä kuukausien ja viikonpäivien nimet
(tammikuu, helmikuu... maanantai, tiistai...). Näitä syklin yksittäisten
komponenttien nimiä Fillmore kutsuu *positionaalisiksi*
ajanyksiköiksi[^vsevolodvapos]. Positionaalisiin ajanyksiköihin voitaisiin lukea 
myös esimerkiksi kuukauden numeroidut päivät (helmikuun 1./2./3./...).

[^vsevolodvapos]: Vsevolodova [-@vsevolodova1975, 29]
käyttää käsitettä *suljetut ajanilmausten joukot*,
jollaisia ovat kaikki tässä positionaalisiksi
määritellyt, mutta myös ylipäätään esimerkiksi kaikki
vuorokauden johdannaisyksiköt (tunnit, minuutit,
sekunnit). Avoimia joukkoja ovat tapaukset, jossa
ajanjaksoja ikään kuin tilannekohtaisesti ryhmitellään
laajemmiksi joukoiksi, sellaiset ilmaukset kuin vuosisata
tai "viisiminuuttinen".

Vsevolodova [-@vsevolodova1975, 31] huomauttaa vielä, että myös ruoka-aikojen
nimitykset voivat toimia päivän osien kanssa limittyvänä ja niitä täydentävänä
positionaalisten ilmausten joukkona. Tällöin päivän voisi katsoa jakautuvan
esimerkiksi osiin *ennen lounasta*, *päiväkahvin aikaan*, *ennen illallista* ja niin
edelleen. Toinen mahdollinen täydentävä päivän jaottelu perustuu Vsevolodovan
(mp.) mukaan auringon asentoihin ja pitää sisällään sellaisia ilmauksia kuin *в
сумерках*, *после восхода* jne. 

Erityisiä nimityksiä yksittäisille ajanjaksoille ovat myös ilmaukset *tänään*,
*huomenna* ja *eilen*, jotka ovat ikään kuin lyhennettyjä versioita ilmauksista
*tänä päivänä*, *tätä seuraavana päivänä* ja *tätä edeltäneenä päivänä*. Näitä
termi "positionaalinen" ei kuitenkaan koske.

Nimitän kaikkia edellä lueteltuja ajanilmauksia *potentiaalisesti
kalendaarisiksi ajanilmauksiksi*. Määrite *potentiaalinen* on termissä mukana
siksi, että lueteltuja ajanilmauksia voidaan käyttää kahdella tavalla,
kalendaarisesti tai ei-kalendaarisesti. 

Ei-kalendaarinen käyttö tarkoittaa, että ajanilmausta käytetään puhtaasti
ajallisen etäisyyden tai laajuuden mittayksikkönä. Esimerkiksi ilmaus *viikon
kuluttua hautajaisista* kertoo, että tapahtumien E~1~ ja E~2~ etäisyys on viikon
mittainen. Kalendaarinen käyttö puolestaan tarkoittaa, että tapahtumat
sijoitetaan jollekin käytettävän ajanyksikön identifioimalle ajanjaksolle, jolla
on määrätty alku- ja loppupiste [@fillmore1975santa, 250]. Ilmaukset
*hautajaisten jälkeisellä viikolla*, *koko viime vuoden*  ja *tulevana
maanantaina* ovat näin ollen kalendaarisia. On huomattava, että alun perin
luonnon sykleihin perustuneista ajanilmauksista edelleen johdettuja yksiköitä
kuten minuutti ja sekunti käytetään kalendaarisesti vain hyvin harvoin (*sillä
sekunnilla*). 

Luonnon tarjoamiin sykleihin palautuvien käsitteiden lisäksi potentiaalisesti
kalendaarisiin ilmauksiin voidaan vielä lukea erilaisiin juhlapäiviin perustuvat
ajanilmaukset, kuten *jouluna*. Vsevolodova [-@vsevolodova1975, 30] esittää
myös, että vuoden voi tietyissä konteksteissa ajatella jakautuvan *kausiin*,
kuten erityisesti venäläisille tärkeä *дачный сезон* ('mökkikausi'). Eräässä mielessä nämä voi
jopa tulkita kuukausien nimien kaltaisiksi vuotta jaksottaviksi
positionaalisiksi yksiköiksi, jotka esimerkiksi suomessa muodostavat
summittaisia syklejä kuten *pääsiäinen--juhannus--joulu*[^lisaajuhlista].
Tämänkaltainen vuoden jakaminen on altis muutoksille ja oletettavasti
esimerkiksi juhlapäivillä on aikaisemmin historiassa ollut huomattavasti
suurempi rooli ajan jaksottamisessa [@tobecited]. 

[^lisaajuhlista]:  Positionaalisesti käyttäytyvien juhlapäivien ja vuosittain
toistuvien kausien lisäksi potentiaalisesti kalendaarisiin ilmaisuihin voidaan
laskea kertaluontoiset, mutta silti erikseen nimetyt kaudet historiassa, kuten
ilmauksissa *jääkaudella/liitukaudella*. Kieltenvälisestä vertailusta tässä
suhteessa ks. luku x.


Niin juhlapäiville kuin muillekin positionaalisille ajanyksiköille on ominaista,
että niitä voidaan tavallisesti käyttää *ainoastaan kalendaarisesti*.
Suurimmassa osassa tapauksia ei-kalendaarinen käyttö mittayksikkönä tuottaisi
ainakin lähtökohtaisesti kyseenalaisia rakenteita, kuten *kaksi
joulua/maanantaita/aamua sitten* [vrt. @haspelmath1997space, 27]. 

#### Ei-kalendaariset ajanilmaukset

Potentiaalisesti kalendaaristen ajanilmausten lisäksi luonnolliset kielet
sisältävät koko joukon muita aikaa ilmaisevia sanoja. Kieltenvälistä vertailua
ajatellen on syytä korostaa, että kalendaariset ilmaukset muodostavat joukon,
joka ydinosiltaan on lähes identtinen suomessa ja venäjässä. Ei-kalendaaristen
ilmausten osalta vertailu hankaloituu, ja käännösvastineiden määrittelystä tulee
monimutkaisempaa.  Ero johtuu ennen muuta siitä, että
kalendaarisille ajanilmauksille voidaan (useimmissa tapauksissa) erottaa
kielestä riippumattomat, eksaktit referentit puhujien tajunnassa, kun taas
ei-kalendaaristen ilmausten viittauskohteet ja merkitys ovat hämärärajaisempia
ja subjektiivisempia. 

Hyvin lähellä kalendaarisia ajanilmauksia ovat *indefiniittisesti aikaa
kvantifioivat* ilmaukset. Tällä ryhmällä tarkoitan sanoja ja lausekkeita, jotka
viittaavat johonkin ajanjaksoon tai ajalliseen ekstensioon, mutta eivät käytä
mitään selkeästi määriteltyä yksikköä. Tavallisimpia tämän ryhmän edustajia ovat
ilmaukset, joissa on mukana suomen *aika-* tai venäjän *время*- tai
*пора*-sana[^vremja700], kuten *некоторое время назад*, *vähän aikaa*, *в те
времена*, *nuijasodan aikaan* ja niin edelleen. Näissä ilmauksissa sanat
aika/время tai suomessa usein myös *hetki* (venäjän момент/миг/мгновение
rajoitetummin) esiintyvät ikään kuin kalendaarisen ajanilmauksen paikalla, mutta
epämääräisempänä, tarkkaan rajaamattomana mittayksikkönä.  Toisaalta etenkin
venäjässä etymologialtaan kalendaariset ilmaukset kuten *минута* voivat
käyttäytyä myös aikaa kvantifioivasti [vrt. esim. @jakovleva2013].
Aikaa indefiniittisesti kvantifioivana voidaan nähdä myös suomen
*hiljattain/äskettäin* ja venäjän *недавно*, jotka ovat ikään kuin lyhenteitä ilmauksille 
*vähän aikaa sitten* ja *недавно*.


[^vremja700]: Viimaranta [-@viimaranta2006talking, 29] huomauttaa, että vaikka
время-sana on reilusti käytetympi, myös пора-sanan voi perustellusti katsoa
kuuluvan venäjän aikaa merkitseviin sanoihin.


Indefiniittisesti aikaa kvantifioivia ilmauksia paljon suurempia
ei-kalendaaristen ajanilmausten ryhmiä ovat lähinnä syntaktisten ja
morfologisten ominaisuuksien perusteella eroteltavat *aikaa ilmaisevat
ei-kalendaariset nominilausekkeet*, *aikaa ilmaisevat adverbit ja pronominit*
sekä *aikaa ilmaisevat sivulauseet ja nominaalirakenteet* [vrt. @klein1999,
147-148].

Aikaa ilmaisevat nominilausekkeet voidaan jakaa ilmauksiin, joissa aika
suhteutetaan johonkin yleisesti tunnettuun (objektiiviseen) historialliseen
kiintopisteeseen (*toisen maailmansodan alussa*, *после распада СССР*) sekä
ilmauksiin, joissa kiintopiste on subjektiivinen (*lapsuudessa*, *näytösten
välillä*, *ennen kaatumistaan*)[^overlap]. Aikaa ilmaisevien adverbien ja
pronominien heterogeeniseen ryhmään kuuluvat paitsi itsenäisinä konstituentteina
esiintyvät ilmaukset kuten *теперь*, *сейчас*, *kauan*, *тогда*, *vastikään*,
*часто*, *lopulta* ja  *yleensä*, myös usein vain toisen ajanilmauksen
määritteenä esiintyvät *jo*, *vielä*, *только* ja eräät muut. Näiden ilmausten
tarkempi ryhmittely tapahtuu jäljempänä taksonomioiden 2 ja 3 määrittelyn
yhteydessä. 

[^overlap]: Ajanilmaisujen luokittelussa on väistämättä enemmän tai vähemmän
ristikkäisyyksiä: esimerkiksi aikaan-sanan käytön postpositiona voisi siirtää
myös indefiniittisesti aikaa kvantifioivien ilmausten alta adpositioilmauksiin.
Olen kuitenkin halunnut pitää indefiniittisesti aikaa ilmaisevien luokan
erillisenä luokkanaan, koska se rinnastuu erityisellä tavalla varsinaisiin
potentiaalisesti kalendaarisiin ajanilmauksiin.


Kolmas suuri ei-kalendaaristen ajanilmaisujen ryhmä ovat suoraan verbin
täydennyksinä toimivat[^prondisclaimer] aikaa ilmaisevat sivulauseet ja
nominaalirakenteet. Suomessa tämän ryhmän ilmaisuja muodostetaan *kun-* ja
*kunnes*-konjunktioilla sekä temporaalirakenteella (*syötyämme illallista
menimme ulos*), venäjässä когда- ja пока-konjunktioilla sekä erilaisilla
gerundirakenteilla (*поужинав, мы вышли на улицу*).


[^prondisclaimer]: määritteellä *suoraan verbin
täydennyksinä toimivat* haluan sulkea pois aikaa
ilmaisevat relatiivilauseet, jotka ovat aina jonkin verbistä
riippuvan sanan määritteitä. 

Taksonomia 1 on esitetty kootusti seuraavassa kuviossa:


\rotatebox{90}{
\begin{forest}
  for tree={
    child anchor=west,
    parent anchor=east,
    grow'=east,
  %minimum size=1cm,%new possibility
  %text width=4cm,%
    %draw,
    anchor=west,
    %edge path={
    %  \noexpand\path[\forestoption{edge}]
    %    (.child anchor) -| +(-5pt,0) -- +(-5pt,0) |-
    %    (!u.parent anchor)\forestoption{edge label};
    %},
  }
[Ajanilmaukset
    [Potentiaalisesti kalendaariset, text width=3cm
        [Jako positionaalisuuden mukaan, text width=4cm
            [Positionaaliset
                [\small{\emph{Maanantaina, koko huhtikuun, aamulla}}, text width=4cm,  edge=dotted, l=4cm]
                ]
            [Ei-positionaaliset
                [\small{\emph{Sinä vuonna, kaksi tuntia}}, text width=4cm,edge=dotted, l=4cm]
                ]
            ]
%        [Jako definiittisyyden mukaan, text width=4cm
%            [Definiittiset
%                [\small{\emph{minuutin, maaliskuussa}}, text width=4cm,edge=dotted, l=4cm]
%            ]
%            [indefiniittiset
%                [\small{\emph{aamulla, koko syksyn}}, text width=4cm, edge=dotted, l=4cm]
%                ]
%            ]
        [Jako semanttisiin ryhmiin
            [Alun perin luonnon sykleihin perustuneet, text width=3.4cm
                [\small{\emph{vuosi, kuukausi, päivä}}, text width=4cm, edge=dotted, l=0.5cm]
                ]
            [Edelleen johdetut
                [\small{\emph{minuutissa, viikon kuluttua}}, text width=4cm, edge=dotted, l=4cm]
                ]
            [Juhlapäivät
                [\small{\emph{jouluna, juhannuksen jälkeen}}, text width=4cm, edge=dotted, l=4cm]
                ]
            ]
            [Puhehetkeä lähellä olevat päivät 
                [\small{\emph{eilen}}, text width=4cm,edge=dotted, l=4cm] 
                [\small{\emph{tänään}}, text width=4cm,edge=dotted, l=4cm] 
                [\small{\emph{huomenna}}, text width=4cm,edge=dotted, l=4cm] 
                ]
        ]
    [Ei-kalendaariset, text width=3cm
        [Indefiniittisesti aikaa kvantifioivat
                [\small{\emph{siihen aikaan, hetki sitten}}, text width=4cm, edge=dotted, l=8.6cm]
        ]
        [Adpositioilmaukset,
            [Objektiivinen kiintopiste
                [\small{\emph{ennen toista maailmansotaa}}, text width=4cm,edge=dotted, l=5cm]
                ]
            [Subjektiivinen kiintopiste
                [\small{\emph{ruoan jälkeen}}, text width=4cm, edge=dotted, l=5cm]
            ]
            ]
        [Adverbi- ja pronomini-ilmaukset
            %Itsenäiset & epäitsenäiset erikseen?
                [\small{\emph{joskus, silloin, jo, kauan}}, text width=4cm, edge=dotted, l=8.6cm]
        ]
        [Temporaaliset sivulauseet ja nominaalirakenteet, text width=4cm
                [\small{\emph{syötyämme menimme ulos, valvoin, kunnes silmissä sumeni}}, text width=4cm, edge=dotted, l=8.6cm]
        ]
        ]
]
\end{forest}
}


\normalsize
 
\noindent \headingfont \small \textbf{Kuvio 1}: Ajanilmausten muodostustapaan perustuva taksonomia \normalfont 


### Taksonomia 2: ajanilmausten semanttiset funktiot {#taksn2}


Edellä ajanilmauksia  luokiteltiin lähinnä sen perusteella, miten ne on
muodostettu (mikä on niiden alkuperä) ja mitä syntaktisia ominaisuuksia niillä
on. Nyt siirrytään tarkastelemaan, mitä eri ajallisia merkityksiä näillä
ilmauksilla haluaan välittää. Nimitän näitä merkityksiä Haspelmathin
[-@haspelmath1997space] terminologiaa soveltaen ajanilmausten *semanttisiksi
funktioiksi*.

Johanna Viimaranta [-@viimaranta2006talking, 319] erottaa yksityiskohtaisessa
katsauksessaan suomen ja venäjän ajanilmauksiin 56 eri merkitystä, joita puhujat
ajasta haluavat ilmaista. Viimarannan aineistosta selkeimmin esille nouseva
johtopäätös on, että pohjimmiltaan niin suomessa kuin venäjässä puhujat pyrkivät
ilmaisemaan ajasta samoja asioita -- esimerkiksi sitä, kuinka pitkään tapahtumat
kestävät, kuinka usein tapahtumat toistuvat ja tapahtuuko jokin tapahtuma
sopivaan tai oikeaan aikaan [-@viimaranta2006talking, 321]. Voidaan hyvin
olettaa, että monet näistä kategorioista ovat enemmän tai vähemmän
universaaleja. Suppeampia ja yleistettävyyteen pyrkiviä jaotteluja ovatkin
aikojen saatossa esittäneet muun muassa @kucera1975time, @klein1999,
@jacobson1978use, @haspelmath1997space sekä monet muut.

Otan tässä semanttisten funktioiden luokittelun pohjaksi Kuceran ja Trnkan
[-@kucera1975time] esittämän kolmijaon *T-adverbiaaleihin* (ajallista lokaatiota
ilmaiseviin), *D-adverbiaaleihin* (ajallista kestoa ilmaiseviin) ja
*F-adverbiaaleihin* (taajuutta ilmaiseviin). Tätä lähtökohtaa on käyttänyt myös
Sulkala [-@sulkala1981] analysoidessaan ajan adverbeja[^atermit] suomessa [vrt. myös esim. @quirk85, 230 ja englanti].
Lisäksi muun muassa Wolfgang Klein [-@klein1999, 149] lähtee vastaavasta kolmesta
kategoriasta, joita hän nimittää *positionaalisiksi*, *taajuutta ilmaiseviksi*
ja *kestoa ilmaiseviksi*. Oma jaotteluni on synteesi Kuceran ja Trnkan kolmesta
perusluokasta sekä Haspelmathin semanttisista funktioista, jotka puolestaan jakautuvat
ainoastaan kahteen pääluokkaan: ajalliseen lokaatioon ja ajalliseen ekstensioon
[-@haspelmath1997space, 8]. Varsinaisten *semanttisten ydinfunktioiden* lisäksi
erottelen *marginaalisempien semanttisten funktioiden* joukon.

[^atermit]: Sulkala todella keskittyy analyysissaan ainoastaan *adverbeihin*
sanaluokkana eikä adverbiaaleihin syntaktisena kategoriana.

#### Lokalisoiva semanttinen funktio


T-adverbiaalien kategorialla Kucera ja Trnka [-@kucera1975time, 12] tarkoittavat
yleisimmällä tasolla sitä, että ilmaus lokalisoi jonkin tapahtuman aikaan
vastaamalla kysymykseen *Milloin?*. Käytän tästä semanttisesta funktiosta
nimitystä *lokalisoiva funktio* eli *L-funktio*.  Virkkeet
\ref{ee_lokf} -- \ref{ee_lokf_piste} ovat esimerkkejä L-funktiosta:




\ex.\label{ee_lokf} \exfont \small Pirkko remontoi viime viikolla keittiötä.






\ex.\label{ee_lokf_piste} \exfont \small В шесть часов я еще спал.





\vspace{0.4cm} 

\noindent
Huomaa, että esimerkin \ref{ee_lokf} ajanilmaus ei viittaa vain yhteen hetkeen, vaan
tiettyyn ajanjaksoon, jonka pituutta ei kerrota eli jonka ekstensiota ei
määritellä. Sen sijaan virkkeen \ref{ee_lokf_piste} ajanilmaus osoittaa tarkan
pisteen, johon lauseessa esitetty propositio lokalisoidaan.

Virkkeet \ref{ee_lokf} -- \ref{ee_lokf_piste} ovat Haspelmathin mallissa esimerkkejä
*simultaanista lokaatiosta*: kummankin virkkeen ajanilmaukset identifioivat
jonkin viitepisteen (tai -jakson) ajassa, ja virkkeessä kuvattavasta
tapahtumasta annetaan informaatiota sikäli, kuin se koskettaa tätä
viitepistettä[^viitepm] [@haspelmath1997space, 29]. Kuvattavan tapahtuma siis
tapahtuu viitejakson sisällä tai viitepisteessä. Nimitän viitejakson sisällä
tapahtuvaa lokalisointia *kehyksiseksi* ja viitepisteessä tapahtuvaa
*punktuaaliseksi lokalisoinniksi.*

[^viitepm]: Viitepisteen käsitteen tarkemmasta määrittelystä ks. alaluku \ref{taksn3}.


Simultaanisen lokaation lisäksi Haspelmath [-@haspelmath1997space, 32,35] erottaa
sekventiaalisen lokaation eli lokalisoitavan tapahtuman sijoittumisen joko ennen
tai jälkeen viitepistettä (Pirkko remontoi keittiön ennen lamaa) sekä lokaation
mitattavana etäisyytenä (Pirkko aloitti keittiöremontin viikon kuluttua
lottovoitosta). Sekventiaalisen lokaation erikoistapauksena voidaan pitää
sellaisia ilmaisuja kuin *ensiksi*, *поначалу* ja *lopuksi* [vrt.
@viimaranta2006talking, 115] jotka ilmaisevat jonkin tapahtumajoukon sisäistä
järjestystä. Ajallinen etäisyys puolestaan voi olla definiittistä (*viikon
kuluttua*) tai indefiniittistä (*myöhemmin*).

Niin simultaani, sekventiaalinen kuin ajalliseen etäisyyteen perustuva lokaatio
ovat ajallista lokalisoimista myös Kuceran ja Trnkan mallissa [-@kucera1975time,
13-14]. Haspelmath [-@haspelmath1997space, 33] erottaa kuitenkin vielä
*sekventiaalis-duratiivisen* ajallisen lokalisoinnin: tapahtuman sijoittamisen
ennen tai jälkeen viitepistettä niin, että viitepiste sisällytetään kuuluvaksi
tapahtuman ajalliseen ekstensioon (Pirkko on remontoinut keittiötään
maaliskuusta asti). Niin kuin nimitys *sekventiaalis-duratiivinen* antaa
ymmärtää, tässä tapauksessa kyse on tietyssä mielessä keston ilmaisemisesta. On
kuitenkin olennaista huomata, ettei ilmaus *maaliskuusta asti* ainoastaan kerro
viestin vastaanottajalle että jokin tila on voimassa tietyn ajan, vaan myös lokalisoi
tapahtuman [keittiötä remontoidaan] aika-akselille toteuttaen lokalisoivaa
funktiota. Tämä käy selväksi, jos vertaa ilmausta *maaliskuusta asti* ilmaukseen
*kaksi kuukautta*, joka ilmaisee ainoastaan kestoa. Tästä havainnosta voidaan
johtaa seuraava semanttisia funktioita koskeva määritelmä:\linebreak

\noindent 
\noindent \headingfont \small \textbf{Määritelmä 3}: Yksi ja sama ajanilmaus voi toteuttaa
        useampaa semanttista funktiota\linebreak \normalfont \vspace{0.6cm}

[^tarkekst]: ks. seuraava kappale


L-funktiota voidaan pitää laajimpana tässä esitettävistä kolmesta funktiosta.
Sen ilmaisemiseksi on sekä suomessa että venäjässä käytettävissä lukuisia eri
keinoja ja niin kalendaariset kuin ei-kalendaariset ilmaukset toteuttavat tätä
funktiota usein. L-funktion realisoitumisen voi tiivistää seuraaviin kolmeen
kohtaan.

1. Kalendaaristen ilmausten osalta tavat ilmaista L-funktiota ovat monipuolisia.
   Melkein kaikki suomen paikallissijat voivat toimia L-funktion morfologisena
   ilmaisimena, samoin nominatiivi. Venäjässä rakenne в + akkusatiivi on
   yleinen, niin kuin myös на + akkusatiivi. Lisäksi tiettyjen substantiivien
   (месяц, год) yhteydessä näitä prepositioita seuraa prepositionaali
   [@vsevolodova1975, 28]. Myös instrumentaalimuoto ilman prepositiota on
   tavallinen. Muotoja утром, вечером, зимой, летом jne. voidaan itse asiassa
   pitää ennemmin taipumattomina adverbeinä kuin instrumentaalimuotoisina
   substantiiveinä [@haspelmath1997space, 112].

2. Aikaa kvantifioivat ilmaukset esiintyvät myös hyvin tavallisesti
   L-funktiossa. Esimerkkejä ovat *в те времена*, *sillä hetkellä* yms.

3. Ei-kalendaarisista (ei-nominaalisista) ajanilmauksista L-funktiota
   toteuttavat muun muassa *silloin*, *nyt*, tuolloin, тогда, потом, скоро ja
   monet muut.

#### Taajuutta ilmaiseva semanttinen funktio


Käsittelen toisena semanttisena funktiona Kuceran ja Trnkan F-adverbiaaleiksi
nimittämää luokkaa, koska tällä funktiolla voi nähdä hyvin läheisen suhteen
L-funktioon. F-adverbiaalit ilmaisevat Kuceran ja Trnkan [-@kucera1975time, 67]
mukaan tapahtuman toteutumista useamman kuin kerran, toisin sanoen taajuutta [ks.
myös @sulkala1981, 126]. Tässä suhteessa mahdollisuus määritelmän 3 
mukaiseen usean semanttisen funktion toteutumiseen
yhtä aikaa vaikuttaa todennäköiseltä, sillä usein ilmaistaessa jonkin tapahtuman
tai asiaintilan toistumista ilmaistaan myös, miten tapahtuma lokalisoituu
aika-akselille. Nimitän taajuutta ilmaisevaa semanttista funktiota F-funktioksi.
F-funktiota ilmaistaan suomessa ja venäjässä  karkeasti ottaen neljällä eri
tavalla[^vrtkuc].

[^vrtkuc]: Jaotteluni on eri kuin Kuceralla ja Trnkalla [-@kucera1975time,
67--68], jotka jakavat F-adverbiaalit a) sen perusteella, onko niillä
ankkurointipiste jossain muussa ajanjaksossa vai ei ja b) sen perusteella,
ilmaisevatko ne definiittistä vai indefiniittistä toistomäärää.

\subparagraph*{1. joka / kazhdyj + (kvantifioija) + substantiivi}

Ensimmäinen vaihtoehto on kalendaarisen ajanilmauksen yhdistäminen toistoa
ilmaiseviin *joka*- ja *каждый*-sanoihin. Varsinaisesti kalendaaristen
ilmausten, kuten *joka päivä* ja *joka vuosi* lisäksi myös sanat *hetki*,
*момент*, *мгновение* sekä *kerta/раз* voivat esiintyä tässä funktiossa. Tätä
toistoa voidaan myös kvantifioida lisäämällä järjestysluku substantiivin ja
määritteen väliin (joka *toinen* päivä). Käyttö on sikäli rajoitettua, että
vaikka positionaaliset *maanantai* ja *aamu* toteuttavat usein tämän tyyppistä
toistoa, on ilmaus *joka helmikuu* ainakin oman kielitajuni mukaan vähintäänkin
harvinainen. Venäjästä voidaan tähän ryhmään lukea lisäksi *еже*-alkuiset
kalendaarisista sanoista johdetut adverbit, kuten *ежемесячно*, *ежеминутно* ym.


\subparagraph*{2. Positionaalinen ajanilmaus + instr / po  + dat}


Kucera ja Trnka eivät erittele tätä ryhmää lainkaan F-adverbiaaleihin
kuuluvaksi, vaan analysoivat siihen liittyviä sanoja T-adverbiaalien yhteydessä
[vrt.  @kucera1975time, 12-13]. Myös Haspelmath [-@haspelmath1997space] pitää
vastaavia ilmauksia ainoastaan ajallisen lokaation määrittävinä. Selkeästi mukana
on kuitenkin myös tapahtumien useutta: itse asiassa monessa mielessä tämän
ryhmän ilmaukset (maanantaisin, по вечерам,) ovat synonyymisia edellisen ryhmän
ilmausten kanssa (vrt. kuitenkin \*joka aamupäivä). Määritelmän 3 mukainen taipumus toteuttaa useaa semanttista
funktiota vaikuttaisi erityisen ilmeiseltä juuri tämän ryhmän ilmauksilla.
Esimerkiksi *iltaisin* ei toki sijoita tapahtumaa absoluuttiselle aika-akselille
[vrt. @fillmore1975santa, 252], mutta kylläkin siihen sykliin, jonka osa
positionaalinen termi on.


\subparagraph*{3. Kerta/raz-sanat ja adverbijohdokset}

Toiston numeerisen määrän ilmaiseminen tapahtuu suomen *kerta*- ja venäjän
*раз*-sanojen avulla. Lisäksi kummassakin kielessä on lukusanojen pohjalta
muodostettavia adverbeja,[^kertaadv] joita voidaan usein käyttää synonyymisesti
ilmaisujen *kaksi kertaa* / *два раза* kanssa. Erityistä huomiota
on kiinnitettävä yhtä kertaa tarkoittaviin ilmaisuihin, joiden semanttinen
funktio on useimmiten tulkittava lokalisoivaksi ("kerran Pirkko päätti,
että...", "как-то раз/однажды я увидел на дороге..."). Määrälliselle toistolle
voidaan määrittää jakso, jonka sisällä toisto tapahtuu (esim. kaksi kertaa
*vuodessa*). Lisäksi kardinaalisesta toistosta voidaan erottaa ordinaalinen
toisto, esimerkiksi ilmaus *seitsemännen kerran* tai venäjän *впервые*. [ks.
@hakulinenkarlsson1979, 210].

[^kertaadv]: Suomessa *kahdesti*, *kolmesti* jne., venäjässä дважды, трижды,
четырежды sekä produktiivisemmat mutta vähemmän käytetyt кратно-johdokset
(пятикратно, шестикратно jne.)

\subparagraph*{4. Ei-numeerista taajuutta ilmaisevat adverbit ja aikaa kvantifioivat ilmaukset}

Erotan omaksi joukokseen muut kuin edellä mainitut lukusanoista johdetut
adverbit, jotka ilmaisevat taajuutta. Nämä jaetaan edelleen seuraaviin kolmeen
alaryhmään. Myös eräät aikaa kvantifioivat ilmaukset voi laskea osaksi näitä
ryhmiä.

\subparagraph*{4.a taajuutta korostavat adverbit}

Taajuutta korostavat adverbit kuvaavat tapahtumakertojen määrän puhujan
näkökulmasta suurena tai maksimaalisen suurena. Ensimmäiseen kategoriaan
kuuluvat mm. *usein*, *часто*, *постоянно*, *koko ajan*, *alituisesti*,
*беспрестанно*, jälkimmäiseen puolestaan lähinnä *aina* ja *всегда*.

\subparagraph*{4.b harvuutta korostavat ja neutraalit adverbit}

Tähän ryhmään kuuluu paitsi adverbeja, myös aikaa kvantifioivia ilmauksia kuten
*время от времени* ja *ajoittain*. Tyypillisiä harvuutta korostavia ovat
*редко*, *никогда*, *koskaan* ja joko neutraaleja tai harvuutta korostavia
*joskus*, *silloin tällöin*, *порой* ym.

\subparagraph*{4.c epämääräistä taajuutta ilmaisevat}

Tiettyjen ilmaisujen kohdalla käy hämäräksi, ovatko ne varsinaisesti ajallisia
ja ilmaisevatko ne varsinaisesti taajuutta.  Näiden ilmaisujen kuvaama
tapahtumien toistuvuus on epämääräistä ja useimmiten taajuutta korostavaa.
Luokittelen tähän sellaiset adverbit kuin *yleensä*, *обычно* ja *tavallisesti*.

#### Ekstensiota ilmaiseva semanttinen funktio

Semanttisista ydinfunktioista L-funktiota voi pitää laajimpana, mutta selkeästi
homogeenisempana joukkona kuin F-funktiota, joka on hajanainen ja myös
harvinaisempi[^taajuusgradu]. Ajallista ekstensiota ilmaiseva funktio
(E-funktio) on luultavasti harvinaisempi kuin L-funktio mutta yleisempi kuin
F-funktio[^kyleisyys], ja myös sille on ominaista tietty hajanaisuus, niin että
joitakin hämärärajaisia tapauksia [vrt. @kucera1975time, 56] luokitellaan tässä
varsinaisen *E-funktion* ulkopuolelle.

[^taajuusgradu]: Vertaa esimerkiksi pro gradu -tutkielmassani [-@gradulyhyt, ??]
tehty manuaalinen luokittelu ajankohtaan, taajuuteen ja kestoon, jossa taajuuden
luokka osoittautui huomattavasti muita suppeammaksi.

[^kyleisyys]: Suuntaa antavan käsityksen yleisyydestä voi myös tässä suhteessa
saada pro gradu -tutkielmassani esitettyjen lukumäärien perusteella.

E-funktion määrittäminen ajallisen ekstension käsitteen kautta on yritys välttää
*kesto-käsitteeseen* [vrt. @sulkala1981, 56] liittyvää hajanaisuutta. Samasta
syystä erotan omaksi ryhmäkseen *varsinaisesti ajallista ekstensiota ilmaisevat*
ja *tulkinnanvaraisesti ajallista ekstensiota ilmaisevat tapaukset*. Jaan
varsinaisesti ekstensiota ilmaisevan kategorian kahteen alaryhmään [vrt.
@haspelmath1997space, 38-39], joista ensimmäinen ilmaisee, kuinka kauan jotakin
toimintaa suoritetaan (Pirkko remontoi keittiötä kolme kuukautta) ja toinen,
missä ajassa toiminta suoritetaan (Pirkko remontoi keittiön kolmessa
kuukaudessa). Nimitän ensimmäistä merkitystä *duratiiviseksi ekstensioksi* ja
toista *resultatiiviseksi ekstensioksi*[^nimitystausta].

Duratiivisesta ekstensiosta on kyse myös silloin, kun asetetaan ajallinen
laajuus jonkin toiminnan tapahtumattomuudelle. Hakulisen ja Karlssonin
[-@hakulinenkarlsson1979, 210] nimittävät näitä esimerkkien \ref{ee_negekstfi} ja \ref{ee_negekstru}
kaltaisia tapauksia *kieltoduratiiviksi*.




\ex.\label{ee_negekstfi} \exfont \small En ole nähnyt häntä kahteen vuoteen.






\ex.\label{ee_negekstru} \exfont \small Уже два года, как я ее не вижу





\vspace{0.4cm} 

\noindent
Varsinainen resultatiivinen ekstensio on (niin venäjässä kuin suomessa)
sidoksissa aspektiltaan loppuun suoritettua toimintaa kuvaaviin verbeihin
[@sulkala1981, 38; @tobecited]. Tässä suhteessa sellaiset rakenteet kuin venäjän
*в течение + genetiivi* tai suomen genetiivi + mittaan/aikana/kuluessa eroavat
prototyyppisistä resultatiivisen ekstension ilmaisuista. Katson, että nämä ja
muut vastaavat ilmaukset muodostavat resultatiivisen ekstension alalajin,
*kehyksisen ekstension*, jonka tarkoituksena on ilmaista tietty määrä
resultatiivisuuden suhteen määrittelemättömiä toimintoja tietyn ajan sisällä
(kuten *olen tämän vuoden aikana pelannut paljon tietokonetta*). Samaan tapaan
kuin sekventiaalis-duratiivinen merkitys, myös kehyksisen ekstension merkityksen
voi joissakin tapauksissa nähdä ilmaisevan oikeastaan ajallista lokaatiota (vrt.
в течение прошлого лета / в прошлое лето). On syytä panna merkille, että
kehyksistä ekstensiota ilmaistaan usein myös sivulauseilla (*пока Анна и Ира
делали покупки, мы сидели в машине*).

[^nimitystausta]: Haspelmath käyttää näistä luokista termejä *atelic extent* ja
*telic extent*, Hakulisen ja Karlssonin suomenkielisessä terminologiassa ovat
käytössä käsitteet *duratiivinen* ja *punktuaalinen* kesto [nimityksistä
tarkemmin ks. @sulkala1981, 38].

Ajallinen ekstensio voidaan ilmaista joko implisiittisesti tai
eksplisiittisesti. Kaikki edellä luetellut tapaukset ovat esimerkkejä
implisiittisestä ekstension ilmaisemisesta. Eksplisiittistä ekstensiota
ilmaistaessa E-funktiota toteuttavat paitsi ajanilmaukset, myös ekstensiota
ilmaisevat verbit kuten *kulua*, *jatkua* ja *kestää*. Näissä tapauksissa
ajanilmaus on usein (muttei aina) syntaktisesti määriteltävissä subjektiksi
(keittiön remontoimiseen kului vuosi/kauan/viikkoja).

Seuraavassa on kuvattu tapauksia, joissa tarkasteltavien ilmausten funktio
ajallisen ekstension ilmaisimena tai ajallisuus ylipäätään eivät ole yhtä
selkeästi määriteltävissä kuin edellä esitettyjen esimerkkien kohdalla.




\ex.\label{ee_tulk_heti} \exfont \small He lähtivät heti/pian.






\ex.\label{ee_tulk_nop} \exfont \small Luin kirjan nopeasti.






\ex.\label{ee_tulk_siirto} \exfont \small Ilmoittautumisaikaa jatkettiin keskiviikkoon asti.





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_tulk_heti} voi nähdä ilmaisevan ajallista ekstensiota siinä
mielessä, että niissä kerrotaan, mikä on sen ajanjakson pituus, joka kului
puhehetkestä tapahtuman alkuun, tässä tapauksessa lähtemiseen. Voisi kuitenkin
myös sanoa, että kyseessä on ennemminkin tapahtuman lokalisointi
lähitulevaisuuteen. Esimerkki \ref{ee_tulk_nop} kertoo puolestaan jotain lukemiseen
käytetystä ajasta, mutta on luonteeltaan yhtä lailla myös *tapaa* ilmaiseva.
Esimerkissä \ref{ee_tulk_siirto} kyse on taas ikään kuin metatason ajallisesta
ekstensiosta: ajanilmauksen osoittama ekstensio koskee ennemmin yksittäistä
substantiivia kuin itse tapahtumaa. Esimerkkejä \ref{ee_tulk_heti} -- \ref{ee_tulk_siirto}
lähempänä varsinaista E-funktiota ovat esimerkin \ref{ee_tulk1} kaltaiset tapaukset:




\ex.\label{ee_tulk1} \exfont \small Он приехал сюда на год





\vspace{0.4cm} 

\noindent
Kucera ja Trnka [-@kucera1975time, 57] huomauttavat, että esimerkin \ref{ee_tulk1}
ajanilmaus *на год* ei kuitenkaan kerro lauseen varsinaisen tapahtuman [hän tuli
tänne] ekstensiosta mitään. Sen sijaan lause ikään kuin olettaa toisen
tapahtuman, esimerkiksi [hän viipyi täällä], ja *на год* määrittää juuri tämän
kestoa [vrt. myös @haspelmath1997space, 49]. Hakulinen ja Karlsson käyttävät
suomen kielen vastaavista rakenteista nimitystä "futuurinen duratiivi"
[-@hakulinenkarlsson1979, 210]. 


Esimerkkien \ref{ee_tulk_heti} -- \ref{ee_tulk1} kuvaamien merkitysten luokittelu
*tulkinnanvaraisesti ekstensiota ilmaiseviksi* erotuksena *varsinaisesti
ekstensiota ilmaisevista* on ennen muuta tarkoitettu rajaamaan tutkimuksen
fokusta, niin että huomio kiinnitetään pääasiassa varsinaisesti kestoa
ilmaiseviksi määriteltyihin tapauksiin.


Kummassakin tarkastellussa kielessä resultatiivisen ekstension ilmaisemiseen
käytettävät kielelliset keinot ovat melko selkeitä ja rajattuja. Suomessa
resultatiivista ekstensiota ilmaistaan lähinnä inessiivillä, venäjässä
ilmaisimena taas on rakenne *за* + akkusatiivi. Duratiivista kestoa ilmaisevat
sanat puolestaan ovat kummassakin kielessä syntaktisesti katsottuna
objekteja[^osma2] ja käyttäytyvät myös morfologisesti, kuten objektit, niin että
kummastakin kielestä itse asiassa puuttuu selkeä morfologinen duratiivisen
ekstension indikaattori. Tämä duratiivisen ekstension tunnuksettomuus vaikuttaa
itse asiassa Haspelmathin havaintojen perusteella [-@haspelmath1997space, 120]
melko universaalilta tendenssiltä.

Suomessa duratiivista ekstensiota ilmaisevat konstituentit ovat
totaaliobjekteja, joten ne esiintyvät aina joko genetiivissä tai nominatiivissa
[vrt. @visk, 925]. Venäjässä duratiivisen ekstension ajanilmaukset käyttäytyvät
kuten mitkä tahansa elottomat substantiivit akkusatiivissa [@vsevolodova1975,
29]. Kummassakin kielessä kiellon vaikutus objektiin realisoituu myös
ajanilmauksilla tuottaen suomessa partitiiviobjektin (en pelannut kahta
tuntiakaan) ja venäjässä genetiivimuotoisen objektin (я и двух часов не играл).
Adverbit esiintyvät luonnollisesti sellaisenaan, joskin esimerkiksi suomen
*kauan* saattaa kieltoduratiivissa saada partitiivitaivutuksen [@tobecited].

Varsinaisten E-funktioiden alalajeista voidaan todeta, että kieltoduratiivi
muodostetaan suomessa illatiivilla. Venäjässä kielteisyys ei läheskään aina näy
ajanilmauksessa morfologisesti, mutta se voidaan ilmaista rakenteella (уже) +
ajanilmaus + как -- kuten esimerkissä \ref{ee_negekstru}. Kehyksisen ekstension
ilmaisutavat käytiin läpi edellä luokan esittelyn yhteydessä.


[^osma2]: vrt. OSMAn käsite edellä.

Ajanilmausten muodostusta koskevaa taksonomiaa rakennettaessa tässä käytetty
malli sai monilta osin tukea Vsevolodovan [-@vsevolodova1975]
yksityiskohtaisesta jaottelusta. Semanttisten funktioiden osalta Vsevolodovan
analyysi [-@vsevolodova1975, 19-22] on jossain määrin erisuuntainen kuin omani.
Vsevolodovan mallissa ajanilmaukset jaetaan kolmeen tärkeimpään binääriseen
oppositioon. Ensimmäinen oppositio perustuu siihen, onko lausumassa kuvattu
toiminta samanaikaista kuin ajanilmauksen osoittama jakso vai ei (vrt ilmauksia
*koko yön* -- samanaikaista -- ja *ennen yötä* -- eriaikaista). Toinen oppositio
jaottelee ilmaukset sen perusteella, kuinka totaalisesti kuvattu toiminta
täyttää ilmausten osoittaman ajan (vrt. ilmauksia *прошлой зимой* -- vain
osittain -- ja *с июля по сентябрь* -- totaalisesti).  Kolmas oppositio vastaa
melko hyvin tässä esitettyä F-funktiota ja ilmaisee, onko kyse yksittäisestä vai
toimintakertoihin jaettavasta toiminnasta.

#### Muut semanttiset funktiot

L-, F-, ja E-funktiot muodostavat ajanilmausten semanttiset ydinfunktiot. Kuten
esimerkiksi Viimarannan 56-osaisen luokittelun perusteella voi todeta, näiden
lisäksi voidaan määritellä koko joukko muita, enemmän tai vähemmän
ydinfunktioiden kanssa limittyviä *marginaalisia semanttisia funktiota*.
Viimarannan jaottelussa yksittäiset semanttiset funktiot ryhmitellään kymmeneksi
laajemmaksi ryhmäksi, joista tässä määriteltyjen ydinfunktioiden ulkopuolelle
osuvat ryhmät *sopiva ja oikea aika*, *elämä aikana*, *ajan rajallisuus*, *ajan
kuluminen* ja *ajan likimääräisyys*. Viimarannan jaottelun lähtökohdat ovat
erilaiset kuin tässä tutkimuksessa ja jaottelu liikkuu paljon tarkemmalla
semanttisten nyanssien ja aikaan liittyvien metaforien tasolla. Tämän
tutkimuksen kannalta relevantti on lähinnä *ajan likimääräisyyden*
[@viimaranta2006talking, 148] luokka. Lisäksi tarkastelen ajanilmauksen suhdetta
viestijöiden odotuksiin (presuppositionaalinen funktio).

[presuppositionaalinen funktio tarkemmin kesken]

Haspelmath [-@haspelmath1997space, 48] huomauttaa, että monissa kielissä on oma
adpositionsa, jonka tehtävä on ilmaista ajanmääreen likimääräisyyttä.  Venäjässä
tätä funktiota toteuttaa *около-prepositio*, suomessa *maissa*-postpositio.
Lisäksi venäjässä voidaan käyttää lukusanojen yhteydessä rakennetta, joissa
lukusanan genetiivimuotoinen dependentti siirretään sen eteen (*часов в пять*).
Lisäksi likimääräisyyttä voidaan ilmaista delimitatiiveilla. Venäjässä tämä on
luontevampaa (*часок я постоял в магазине*) kuin suomessa (?*siinä kuluu ehkä
tunteroinen*).  Likimääräisyys ei kuitenkaan ole itsenäinen semanttinen funktio,
vaan likimääräisyyttä viestivä ajanilmaus toteuttaa aina myös joko lokalisoivaa
(tulimme takaisin *viiden maissa*) tai ekstension ilmaisevaa funktiota (*olimme
siellä päivän-pari*).

Taksonomia 2 on tiivistetty seuraavaan kaavioon:


\begin{landscape}

\scriptsize

\begin{forest}
  for tree={
    child anchor=west,
    parent anchor=east,
    grow'=east,
  %minimum size=1cm,%new possibility
  %text width=8cm,%
    %draw,
    anchor=west,
    %edge path={
    %  \noexpand\path[\forestoption{edge}]
    %    (.child anchor) -| +(-5pt,0) -- +(-5pt,0) |-
    %    (!u.parent anchor)\forestoption{edge label};
    %},
  }
[Ajanilmausten semanttiset funktiot, text width=1.5cm
[Ydinfunktiot
    [Lokalisoiva, text width=2cm  
        [simultaani, name=sim
            [kehyksinen
                [\scriptsize{\emph{Pirkko remontoi viime viikolla keittiötä}}, text width=8cm, edge=dotted, l=7.65cm]
            ]
            [punktuaalinen
                [\scriptsize{\emph{Heräsin kello kuusi}}, text width=8cm, edge=dotted, l=7.65cm]
            ]
        ]
        [sekventiaalinen
            [\scriptsize{\emph{Pirkko remontoi keittiön ennen lamaa}}, text width=8cm, edge=dotted, l=9.4cm]
        ]
        [sekventiaalis-duratiivinen, name=sekvd
            [\scriptsize{\emph{Pirkko remontoi maaliskuuhun asti}}, text width=8cm, edge=dotted, l=9.4cm]
        ]
        [ajallinen etäisyys
            [\scriptsize{\emph{Remontti alkaa viikon päästä}}, text width=8cm, edge=dotted, l=9.4cm]
        ]
    ]
    [Taajuutta ilmaiseva., text width=2cm
        [joka/каждый + N
            [\scriptsize{\emph{joka hetki, joka vuosi}}, text width=8cm, edge=dotted, l=9.4cm] 
        ] 
        [positionaalinen ilmaus + sija/prepositio
            [\scriptsize{\emph{maanantaisin, iltaisin}}, text width=8cm, edge=dotted, l=9.4cm] 
        ] 
        [kerta/раз + johdokset
            [\scriptsize{\emph{kaksi kertaa vuodessa, kolme kertaa, neljästi}}, text width=8cm, edge=dotted, l=9.4cm] 
        ] 
        [adverbiset ilmaukset
            [taajuutta korostavat
                [suuri taajuus
                    [\scriptsize{\emph{usein, alituisesti}}, text width=8cm, edge=dotted, l=3.5cm]
                ]
                [maksimaalinen taajuus 
                    [\scriptsize{\emph{aina}}, l=3.5cm, edge=dotted]
                ]
            ]
            [harvuutta korostavat/neutr.
                [korostavat
                    [\scriptsize{\emph{harvoin, koskaan}}, text width=8cm, edge=dotted, l=2.65cm]
                ] 
                [neutraalit
                    [\scriptsize{\emph{joskus, silloin tällöin}}, text width=8cm, edge=dotted, l=2.65cm] 
                ]
            ] 
            [epämääräinen taajuus
                [\scriptsize{\emph{yleensä, tavallisesti}}, text width=8cm, edge=dotted, l=6.2cm]
            ] 
        ] 
    ]
    [Ekstensiota ilmaiseva., text width=2cm, name=ekstf
       [Varsinaiset
           [duratiivinen ekstensio
               [myöntöduratiivi
                   [\scriptsize{\emph{Pirkko remontoi keittiötä kolme kuukautta}}, text width=8cm, edge=dotted, l=4.4cm]
               ]
               [kieltoduratiivi
                   [\scriptsize{\emph{En ole nähnyt häntä kahteen vuoteen}}, text width=8cm, edge=dotted, l=4.4cm]
               ]
           ]
           [resultatiivinen ekstensio
               [varsinainen res.ekst.
                   [\scriptsize{\emph{Pirkko remontoi keittiön kolmessa kuukaudessa}}, text width=8cm, edge=dotted, l=4.1cm]
               ]
               [kehyksinen ekstensio
                   [\scriptsize{\emph{Olen tämän vuoden aikana pelannut paljon tietokonetta}}, text width=8cm, edge=dotted, l=4.1cm]
               ]
           ]
       ] 
       [Tulkinnanvaraiset
           [teeliset
               [\scriptsize{\emph{Hän saapui kolmeksi kuukaudeksi}}, text width=8cm, edge=dotted, l=6.5cm]
           ]
           [aikaa ja tapaa ilmaisevat
               [\scriptsize{\emph{Remontti tehtiin nopeasti}}, text width=8cm, edge=dotted, l=6.5cm]
           ]
           [metatason ekstensio
               [\scriptsize{\emph{Tapahtuma siirrettiin loppuvuoteen}}, text width=8cm, edge=dotted, l=6.43cm]
           ]
       ] 
    ]
]
[Muut funktiot
[Presuppositionaalinen funktio, text width=4cm
  [\scriptsize{\emph{jo, vielä, vasta}}, text width=4cm, edge=dotted, l=11.34cm]
]
[Likimääräisyyttä ilmaiseva,text width=4cm, name=likim
  [\scriptsize{\emph{maissa, aikoihin}}, text width=4cm, edge=dotted, l=11.34cm]
]
]
]
\draw[->,dotted] (sekvd) to[out=west, in=north] (ekstf);
\draw[->,dotted] (likim) to[out=west, in=south] (sim);
\end{forest}

\normalsize

\end{landscape}
 
\noindent \headingfont \small \textbf{Kuvio 2}: Ajanilmausten semanttisiin funktioihin perustuva taksonomia \normalfont 



### Taksonomia 3: ajanilmausten viitepisteet {#taksn3}

Ajanilmausten eri jaottelutavoista on otettava esille vielä yksi ominaisuus,
joka osittain limittyy niin edellä määriteltyjen semanttisten funktioiden kuin
toisaalta ajanilmausten muodostustapojenkin kanssa. Tämä ominaisuus jakaa
ajanilmaukset sen perusteella, onko niillä jokin viittauskohde eli referentti
vai ei.

Tarkastellaan esimerkkiä \ref{ee_paikref}:




\ex.\label{ee_paikref} \exfont \small Tavataan kello 14 Mikon työhuoneessa





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_paikref} ilmauksilla *kello 14*, *Mikon* ja *Mikon työhuoneessa*
on kaikilla jokin viittauskohde, jonka kumpikin viestijä (puhuja ja viestin
vastaanottaja) oletettavasti kykenee identifioimaan. *Mikko* viittaa tiettyyn,
kummankin viestijän tunnistamaan henkilöön ja *Mikon työhuoneessa* vastaavasti
kummankin viestijän tunnistamaan paikkaan. Sekä *työhuone* että Mikko ovat
konkreettisia objekteja viestijöiden mielen ulkoisessa maailmassa. Ilmauksen
*kello 14* määrittelemisen konkreettiseksi osaksi fyysistä todellisuutta voi
nähdä riippuvan määrittelijän aikakäsityksestä, mutta oleellista on se, että
puheessa myös siihen suhtaudutaan samalla tavalla konkreettisena objektina kuin
paikanilmauksen *Mikon työhuoneessa* viittauskohteeseen: kognition tasolla
ilmauksen *kello 14* referentti on aivan yhtä todellinen kuin ilmausten
*työhuone* ja *Mikko* referentit.


Ajanilmausten viittauskohteet voidaan tulkita pisteiksi aika-akselilla aivan
samaan tapaan kuin paikanilmausten viittauskohteet ovat pisteitä jollakin
kolmesta spatiaalisesta akselista [@haspelmath1997space, 21]. Samalla tavoin
kuin ilmaus *siellä* viittaa kohtaan maapallolla, jonka ekstensio voi olla suuri
(esimerkiksi valtion alue) tai pieni (esimerkiksi talon piha), ilmaus *silloin*
voi viitata ekstensioltaan suureen (vuosisatoja) tai pieneen (sekunteja) jaksoon
aika-akselilla. Lisäksi ilmaus *sillä hetkellä* tarkoittaa yhtä konkreettista
pistettä aika-akselilla samalla tavoin kuin ilmaus *sillä kohtaa* voi viitata
yhteen konkreettiseen pisteeseen jollakin spatiaalisella akselilla. 

Ajanilmaukset voivat siis kantaa mukanaan referentiaalista tietoa eli olettaa
jonkin viittauspisteen. Näin ei kuitenkaan aina ole, vaan on olemassa myös
ei-referentiaalisia ajanilmauksia. Ei kuitenkaan ole yksiselitteistä
määritellä, onko ilmauksella referentti vai ei, vaan referentiaalisuus on
nähtävä binäärisen opposition sijaan jatkumona, jonka toisessa päässä ovat
kiistatta referentiaaliset, toisessa päässä kiistatta ei-referentiaaliset.
Tarkastellaan seuraavaksi, minkälaiset ilmaukset selkeimmin akselin kumpaankin
ääripäähän sijoittuvat.

#### Referentiaalisuuden indikaattoreita


Intuitiivisesti vaikuttaisi siltä, että jako kalendaarisesti ja
ei-kalendaarisesti käytettyihin ilmauksiin korreloi melko suoraan
referentiaalisuuden kanssa. Kalendaarisilla ilmauksilla, kuten *ensi
huhtikuussa*, *в два часа" on selkeät viittauskohteet aikajanalla, kun taas
*olimme siellä minuutin* ei oletettavasti identifioi mitään tiettyä aikajanan
minuuttia. Kuitenkin on todettava, että myös kaikissa taksonomian 1
ei-kalendaarisissa ryhmissä on ilmauksia, jotka voivat esiintyä
referentiaalisesti. Näitä ovat esimerkiksi indefiniittisesti aikaa kvantifioiva
*siihen aikaan*, ei-kalendaarinen nominilauseke *buurisodan jälkeen*, adverbi
*тогда* ja niin edespäin.

Vaikuttaisikin siltä, että kalendaarisuus on riittävä, joskaan ei välttämätön
ehto ilmauksen referentiaalisuudelle. Tämä yleistys ei kuitenkaan ole täysin
pätevä, sillä on myös löydettävissä kalendaarisia ilmauksia, jotka *eivät* ole
referentiaalisia. Esimerkiksi lauseen *sellaisina päivinä ei jaksaisi edes
hampaitaan harjata* ajanilmaus on kalendaarinen, muttei sisällä mitään
varsinaisesti identifioitavissa olevaa[^ident_seur] viitepistettä aikajanalta.
Kucera ja Trnka [-@kucera1975time, 13] mainitsevat lisäksi sellaiset ilmaukset
kuin *ночью* ja *в апреле*, jotka eivät myöskään välttämättä viittaa yhteen
identifioitavissa olevaan pisteeseen. Tällöin kyse on lähinnä geneerisistä
lauseista, kuten *в апреле обычно снега уже мало* tai *ночью мне всегда
холодно*.


Toisena referentiaalisuuden todennäköisenä indikaattorina voidaan pitää
lokalisoivaa semanttista funktiota. Verrattuna muihin semanttisiin funktioihin
L-funktio tuntuu varmimmin ennustavan jonkin ilmauksen referentiaalisuutta:
edellä esitetyistä esimerkeistä ei-referentiaalinen *olimme siellä minuutin*
toimii E-funktiossa, mutta *ensi huhtikuussa* ja *два часа* L-funktiossa.
E-funktiokaan ei kuitenkaan sulje pois referentiaalisuutta -- esimerkiksi
duratiivista ekstensiota ilmaiseva ajanilmaus lauseessa *pelasin korttia koko
yön* on selkeästi referentiaalinen. Jälleen kerran vaikuttaisi siltä, että
ajallinen lokalisoitavuus on riittävä, joskaan ei välttämätön ehto
referentiaalisuudelle. Kalendaarisuuden riittävyyden kyseenalaistaneet esimerkit
osoittavat kuitenkin, ettei L-funktiokaan ole vielä tae referentiaalisuudesta,
sillä lauseen *в апреле обычно снега уже мало* ajanilmauksella on nimenomaan
lokalisoiva funktio -- lokalisointi vain ei tapahdu absoluuttisella aikajanalla,
vaan pikemminkin *suhteessa positionaalisen ajanilmauksen implikoimaan sykliin*.
Vastaavien ajanilmausten roolia informaation välittämisessä tutkitaan tarkemmin
seuraavassa alaluvussa.

[^ident_seur]: Tarkemmin identifioitavuuden käsitteestä ks. seuraava alaluku.

Vastaesimerkeistä huolimatta voidaan todeta, että kalendaariset ja L-funktiossa
toimivat ajanilmaukset ovat selkeimmin referentiaalisia. E-funktiossa toimivat
ei-kalendaariset ilmaukset puolestaan ovat lähtökohtaisesti myös
ei-referentiaalisia, ja E-funktiossa toimivilla kalendaarisillakin ilmauksilla
viittauskohde on vain harvoin. Lisäksi ei-numeerista taajuutta ilmaisevat sanat
kuten *joskus* ja *tavallisesti* samoin kuin numeerista taajuutta ilmaisevat
*пять раз* ja *трижды* ovat referentiaalisuusjatkumon ei-referentiaalisessa
ääripäässä. F-funktion alaluokkien 1 ja 2 osalta määritteleminen on jossain
määrin haastavampaa: onko esimerkiksi ilmauksella *maanantaisin* viittauskohde?

Luonnollisesti myös ajanilmauksen itsenäisyys ja syntaktinen luonne ovat
tekijöitä, joilla on vaikutusta referentiaalisuuteen. Esimerkiksi
presuppositionaalisessa funktiossa käytettävillä  *vielä-*, *jo-* tai
*уже-*adverbeilla ei tavallisesti ole omaa viittauskohdettaan. Kuitenkin myös
nämä sanat voivat tietyissä konteksteissa viitata *puhehetkeen*: ilmauksissa
*почему он еще не пришел?* tai  *Lähdetään jo!* sanoilla *еще* ja *jo* on
presuppositionaalisen funktion lisäksi myös lokalisoiva funktio, ja ne
ankkuroivat *tulla-* ja *lähteä*-tyyppisten verbien kuvaaman toiminnan
aikajanalle.[^ankkurihuom] Toisaalta näissä ja monissa muissa tapauksissa myös ajanilmauksen
ajallisuus sinänsä alkaa horjua, ja sanojen funktio kallistuu kohti
diskurssipartikkelin roolia. Tällaisilla temporaalisuutensa menettäneillä
ilmaisuilla, kuten usein sanojen nyt, sitten ja *потом* kanssa, ei
luonnollisesti ole myöskään referenttiä.

[^ankkurihuom]: Käytän referentiaalisuudesta puhuttaessa termiä *ankkuroida*
tarkoittamaan kiinnekohdan löytämistä aikajanalta. Ankkuroimista informaatiorakenteen
kannalta käsitellään luvussa x.x [ks. Prince 81:237]

#### Viittauskohteen tarkkuus

Referentiaalisten ajanilmausten viittauskohteet voivat olla tarkempia tai
epämääräisempiä. Punktuaalista lokalisoivaa funktiota edustavilla
ajanilmauksilla viittauskohde on yksittäinen piste, kehyksistä lokalisoivaa
funktiota edustavilla puolestaan lyhyempi tai pidempi jakso. Kummassakin
funktiossa esiintyvillä sanoilla voi olla joko eksakteja tai epämääräisiä
referenttejä. Esimerkiksi virkkeen \ref{ee_pallootsa} ajanilmauksen referentti on
punktuaalinen (pallon iskeytyminen otsaan viittaa yhteen jakamattomaan hetkeen)
mutta myös epämääräinen (viitepisteen tarkkaa sijaintia aikajanalla ei
ilmaista). Virkkeessä \ref{ee_myoharem} sen sijaan ajanilmaus toteuttaa kehyksistä
lokalisoivaa funktiota, ja referentti on samalla tavoin epämääräinen. 




\ex.\label{ee_pallootsa} \exfont \small Sain hiljattain pallon otsaani






\ex.\label{ee_myoharem} \exfont \small Remontti tehdään sitten myöhemmin.





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_pallootsa} ja \ref{ee_myoharem} eroavat lisäksi siinä, että edellisessä
viittaushetki on puhujan tiedossa ja sille voidaan määrittää tarkka aikapiste
puhujan (muttei vastaanottajan) subjektiivisella aikajanalla, kun taas
esimerkissä \ref{ee_myoharem} viittauspisteen tarkka sijainti ei ole edes puhujan
tiedossa.

\todo[inline]{ Lisää settiä ajatellen elimme silloin vs. silloin hän..}

#### Deiktiset ajanilmaukset

Referentiaalisista ajanilmauksista voidaan erottaa omaksi ryhmäkseen ilmaukset,
joiden viittauskohde määritellään suhteessa puhehetkeen. Näitä ajanilmauksia
on perinteisesti nimitetty *deiktisiksi* [vrt. deiktisyyden määritelmät Fillmorella
-@fillmore1975santa, 256].[^shoredeikt] Deiktisiä ajanilmauksia ovat

1. monet puhehetkeen liittyvät adverbit kuten *nyt, kohta,
äsken, недавно, скоро*

2. puhehetkeä lähellä olevia päiviä tarkoittavat *eilen,
tänään, huomenna, toissapäivänä, ylihuomenna.*

3. päivän osien ja viikonpäivien viittauskohteet, jotka ovat
deiktisiä usein, mutteivät aina.  Esimerkiksi lauseessa
*aamulla minulla oli huono olo* ainoastaan
suhteuttaminen puhehetkeen tekee mahdolliseksi
aamulla-sanan referentin identifioimisen juuri tietyksi
(puhehetkellä käynnissä olevan päivän) aamuksi.

4. muut puhehetkeen suhteutetut kalendaariset tai aikaa
kvantifioivat ilmaukset kuten *через два дня*, *kaksi
vuotta sitten*, *в прошлое воскресенье* sekä *hetki
sitten*.

[^shoredeikt]: Deiktiset ilmaukset kattavat paitsi ajan, myös paikan ilmauksia
[ks. esim. @tobecited], ja tämän vuoksi myös adjektiivi *aikadeiktinen*
esiintyy tutkimuskirjallisuudessa [ks. ainakin @shore2008, 31].



#### Ei-deiktiset referentiaaliset ajanilmaukset

Jos referentiaalinen ilmaus ei ole deiktinen, se viittaa, kuten edellä
määriteltiin, joko johonkin yleisesti tunnistettavaan historialliseen
tapahtumaan (*Rooman palon aikana*) tai johonkin subjektiivisen aikajanan
pisteeseen (*в молодости*, *herättyäni*). Viittauskohteet voivat lisäksi olla
tavalla tai toisella edeltävässä diskurssissa muodostettuja, 
kuten esimerkissä (\ref{ee_refTT}):




\ex.\label{ee_refTT} \exfont \small Muistan sen päivän hyvin. Aamulla minulla oli huono olo... Sitten se
onneksi helpotti.





\vspace{0.4cm} 

\noindent
Lause *muistan sen päivän hyvin* asettaa ajan, josta puheenvuoron seuraavat
lauseet kertovat. Wolfgang Kleinin termein [-@klein1999, 5, 39] tätä aikaa
voitaisiin nimittää "ajalliseksi topiikiksi" (TT, topic time)[^bondhuom]. Puhehetken
(jäljempänä myös Kleinia mukaillen UT, utterance time)ja
erilaisten eksplisiittisesti nimettyjen subjektiivisen tai objektiivisen
aikajanan pisteiden lisäksi TT tarjoaa kolmannen tavan ankkuroida
ajanilmaukselle referentti. Sellaisissa ilmauksissa kuin *toisen maailmansodan
jälkeen* tai *äsken* TT asetetaan ikään kuin automaattisesti, mutta -- kuten
esimerkistä \ref{ee_refTT} havaitaan -- se voi syntyä myös implisiittisesti edeltävän
diskurssin tuloksena. Näin tapahtuu erityisen usein juuri positionaalisten
ajanilmausten yhteydessä.

[^bondhuom]: Ks. mysö Bondarko 2005:260

Ajallisen topiikin käsite auttaa myös määrittämään tiettyjen F-funktiossa
toimivien ajanilmausten sijaintia aikajanalla. Tätä valottaa seuraava esimerkki:




\ex.\label{ee_mikkeli} \exfont \small Toisen maailmansodan jälkeen isoisä muutti Kuopiosta Mikkeliin. Siellä
hänen elämänsä asettui raiteilleen. \emph{Aamuisin} hän käveli
kirjastoon lukemaan lehtiä. \emph{Joka sunnuntai} hän kävi
tuomiokirkossa jumalanpalveluksessa.





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_mikkeli} ajanilmausten *aamuisin* ja *joka sunnuntai* voi nähdä
viittaavan tiettyyn joukkoon aamuja ja sunnuntaita. Joukko on selvästi rajattu:
esitetyt väitteet koskevat vain sitä ajanjaksoa, jonka isoisä vietti Mikkelissä,
mahdollisesti vielä tarkemmin tuon ajanjakson alkuvaiheita. Tämä ajanjakso on
esimerkin \ref{ee_mikkeli} jälkimmäisten virkkeiden TT[^histvert]. Tässä mielessä
ajanilmaukset *aamuisin* ja *joka sunnuntai* ovat myös referentiaalisia. 

[^histvert]: Vertaa myös @le2003time historiallisten tekstien ajallisten
topiikkien rakentumisesta (tarkemmin luvussa x)

TT:n käsitteeseen läheisesti liittyvä muttei identtinen ilmiö on referenttien
anaforisuus. Tyypillisesti esimerkiksi sellaisten ajanilmausten kuin *sitten*,
*sittemmin*, *после того*, *тогда* referenttinä on piste tai jakso aikajanalla,
joka ankkuroidaan suhteessa jonkin edellä mainitun ajanilmauksen referenttiin.
Fillmore [-@fillmore1975santa, 289] käyttää tähän liittyen termiä
*diskurssideixis* ja soveltaa sitä myös kirjoitettuun tekstiin kuvaamaan
esimerkiksi sanojen *aiemmin* ja *edellisessä kappaleessa* referenttejä.


Referentiaaliset ajanilmaukset voidaan siis jakaa viiteen kategoriaan sen
perusteella, miten niiden viitepiste on ankkuroitu viestijöiden kognitiossa:

1. Ankkurointi suhteessa puhehetkeen (UT)
2. Ankkurointi absoluuttiselle aikajanalle 
3. Ankkurointi subjektiiviselle aikajanalle 
4. Ankkurointi suhteessa ajalliseen topiikkiin (TT)
5. Anaforinen ankkurointi 