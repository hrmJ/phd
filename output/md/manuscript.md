---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
fontsize: 11.5pt
geometry: twoside, b5paper, layout=b5paper, left=2cm, right=2cm, top=2cm, bottom=3cm, bindingoffset=0.5cm
indent: true
subparagraph: yes
output:
    html_document:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
    pdf_document: 
      latex_engine: xelatex
      toc: true
      toc_depth: 6
      number_sections: true
      keep_tex: true
      includes:
          in_header: preamble.tex
---
\todo[inline]{TODO: johdanto}

Johdanto
========



Odottaa...


Ajan ilmaisemisen monet muodot {#aimm}
===============================


Ajanilmauksen käsite {#ajanilm-kasite}
--------------------

Aikaa ilmaistaan luonnollisissa kielissä monin tavoin. Ajan
ilmaisumuotoja eriteltäessä on perinteisesti lueteltu ainakin verbien
aikamuodot ja aspekti, ajan adverbiaalit sekä esimerkiksi kiinassa
esiintyvät temporaalipartikkelit [@klein1999, 143]. Näistä kategorioista
tämän tutkimuksen fokusta kuvaa parhaiten nimenomaan aikaa
ilmaisevien adverbiaalien ryhmä.

Adverbiaali on tavallisesti ollut suhteellisen hämärärajainen
määriteltävä.  Esimerkiksi Iso suomen kielioppi luokittelee adverbiaalin
yleisnimitykseksi sellaisten lausekkeiden lauseenjäsentehtävälle, jotka
ovat muussa muodossa kuin kieliopillisessa sijassa [@visk, määritelmät].
Määritelmä on kaikkea muuta kuin täsmällinen ja kuvastaa sitä, että
adverbiaali on yleensäkin tavattu määritellä negaation kautta:
adverbiaaleja ovat ne elementit, jotka eivät ole esimerkiksi subjekteja tai
objekteja [@huumo1997, 25].

Vaikka eri ajan ilmaisukeinoista ajan adverbiaalin käsite siis osuukin
lähimmäs tässä tutkimuksessa tarkasteltavia ilmiöitä, ei se sellaisenaan ole
käyttökelpoinen. Negaatiomenetelmää edelleen mukaillen määritänkin
ajanilmauksen käsitteen tätä tutkimusta varten seuraavasti:\linebreak

\noindent 
\noindent \headingfont \small \textbf{Määritelmä 1}: Ajanilmauksia ovat *sanat,
lausekkeet  tai lauseet joiden avulla lauseessa tai
virkkeessä ilmaistaan aikaa mutta jotka eivät ole verbejä
vaan nomineista tai adverbeista muodostuvia verbien muita
kuin subjektiksi tai predikatiiveiksi luokiteltavia
täydennyksiä tai määritteitä*. \normalfont \vspace{0.6cm}
 \linebreak 



Vaikka esimerkiksi edellä kuvattuun ISKin määritelmään on
lisätty erikseen myös fennistiikassa *osmaksi* nimitetyt
elementit (kuten esimerkin \ref{ee_osma} maanantai), ovat nämä
koneellisessa analyysissä kuitenkin objekteja [@tobecited],
minkä vuoksi objekteja ei voi jättää määritelmän
ulkopuolelle.  Subjektin funktio sen sijaan aiheuttaa
potentiaalisesti aikaa ilmaisevan sanan jäämisen tämän
tutkimuksen ulkopuolelle, sillä vaikka esimerkin \ref{ee_eisubj}
*maanantain* periaatteessa voi tulkita aikaa ilmaisevaksi, on
ilmaisun ajallisuuden luonne jo hyvin toisenlainen ja paljon
implisiittisempi kuin esimerkiksi juuri lauseessa \ref{ee_osma}.




\ex.\label{ee_osma} \exfont \small Matias nukkui koko maanantain





\vspace{0.4cm} 

\noindent



\ex.\label{ee_eisubj} \exfont \small Maanantai oli mukava päivä.





\vspace{0.4cm} 

\noindent

Määritelmä 1 on luonteeltaan puhtaan syntaktinen. On
kuitenkin mielekästä kysyä myös, mitä tarkoittaa lause *joiden avulla lauseessa
tai virkkeessä ilmaistaan aikaa* -- toisin sanoen  annettava myös jonkinlainen
semanttinen määritelmä ajan ilmaisemiselle. Tämä on lopulta kauas käsillä olevan
tutkimuksen ulkopuolelle ulottuva filosofinen kysymys, eikä tässä pyritäkään
semanttisen määritelmän osalta vastaavaan  tarkkuuteen kuin määritelmässä 
1. Joitain suuntaviivoja voidaan kuitenkin antaa.

Lähtökohtaisesti voidaan todeta, että ajan ja temporaalisuuden käsite ovat
perustavanlaatuinen osa jokaisen ihmisen kokemusmaailmaa  [@jaszczcolt2012, 1].
Tästä huolimatta temporaalisuuden määritteleminen ei ole yksiselitteistä tai
suoraviivaista -- ajan käsitteen ympärillä tuntuu vallitsevan tiettyä
mystisyyttä ja kuvauksen saavuttamattomuutta, niin että esimerkiksi paikan
käsitteisiin verrattuna ajan käsitteellinen vangitseminen on selvästi vaikeampaa
[@langacker2012, 203]. Tunnettu ja myös empiiristä tukea saanut hypoteesi onkin,
että aikaa ilmaistaan kielessä käsitteillä, joilla alun perin on ilmaistu juuri
paikkaa [@haspelmath1997space, 140; @evans2003structure, 13]. 

Aikaa itseään on pyritty määrittelemään muun muassa tapahtumien käsitteen
avulla: esimerkiksi Sinhan ja Gärdenforsin [-@sinha14,1] mukaan ajan käsite
koostuu *tapahtumien ja tapahtumien välisten suhteiden kognitiivisista ja
kielellisistä representaatioista*. Ajatus ajasta puhtaasti kognitiivisena,
ainoastaan ihmismielessä sijaitsevana konseptina ei kuitenkaan ole ainoa eikä
edes tavallisin käsitys: aikaa voidaan lähestyä myös fysikaalisena ilmiönä,
meitä ympäröivän maailman objektiivisesti olemassa olevana komponenttina
[@evans2003structure, 4].  Sillä, onko aika todellinen osa fyysistä maailmaa vai
*ainoastaan* ihmismielen luoma konsepti, ei kuitenkaan ole tämän tutkimuksen kannalta
merkitystä.  Koska tutkimuksen tarkoitus on nimenomaan ymmärtää sitä, miten
kahdessa eri kielessä heijastuu ihmisen kognition tuottama käsitys ajasta,
lähdetään tässä liikkeelle ajasta inhimillisenä kokemuksena, joka vaatii ilmaisukeinonsa.
Tästä lähtökohdasta käsin voidaan antaa seuraava tarkoituksellisen summittainen
semanttinen määritelmä ajan ilmaisemiselle: \linebreak


\noindent 
\noindent \headingfont \small \textbf{Määritelmä 2}: Ajan ilmaiseminen tarkoittaa
 sitä, miten kieli ilmentää kognition kuvaa tapahtumien suhteista,
 sijainneista ja ekstensiosta aika-akselilla. \normalfont \vspace{0.6cm}
 \linebreak 



Aika-akselin käsitteessä näkyy ajan hahmottaminen paikan kategorian kautta.
Siinä missä paikkaa ilmaistaan kolmella akselilla (ylös-alas, sivuille,
eteen-taakse), aika ymmärretään tavallisesti yhdeksi jatkuvaksi akseliksi, joka
tavallisesti vertautuu juuri eteen-taakse-akseliin [@haspelmath1997space, 21].

Ajanilmausten kolme taksonomiaa
-----------------------------------

Ajanilmauksia voidaan luokitella monista eri lähtökohdista. Tässä käyttämäni
ajanilmausten jaottelu koostuu kolmesta eri tason  taksonomiasta. Jossain määrin vastaavan
jaottelun voi löytää esimerkiksi M.V. Vsevolodovan teoksesta Способы выражения
временных отношений в русском языке [-@vsevolodova1975], ja yhtymäkohdat
Vsevolodovan jaotteluun pyritäänkin tekemään eksplisiittisiksi. Kolme tässä
tarkasteltavaa osa-aluetta ovat

-  Taksonomia 1: Mistä kielellisistä aineksista ajanilmauksia muodostetaan
    [vrt. Vsevolodovan "sanojen luokat", -@vsevolodova1975,
    29]? 

- Taksonomia 2: Mitä semanttisia funktioita ajanilmauksilla on [vrt.
   Vsevolodovan ajallisten suhteiden tyypit, -@vsevolodova1975,
   18].

- Taksonomia 3: Millainen on se kiintopiste aikajanalla, johon ajanilmaus
   suhteutetaan?

### Taksonomia 1: ajanilmausten muodostustavat ja alkuperä {#taksn1}

#### Potentiaalisesti kalendaariset ajanilmaukset {#pot-kal}

Yksiköt, joilla aikaa ilmaistaan, eivät ole identtisiä kielestä ja kulttuurista
toiseen. Kahden hyvin läheisenkin kieliyhteisön välillä voi olla eroja
esimerkiksi siinä, miten aika tyypillisen valveillaolon ja nukkumisen  välillä
jaksotetaan eri osiin.

On kuitenkin olemassa joukko peruskäsitteitä, jotka ovat enemmän tai vähemmän
universaaleja. Näiden käsitteiden voi ainakin välillisesti nähdä rakentuvan
luonnossa havaittavien säännöllisesti toistuvien tapahtumien ympärille.
Tällaisia tapahtumia ovat valoisan ja hämärän ajan vaihtelu, kuun näyttäytyminen
maasta katsovalle havainnoijalle ja auringon kierrosta aiheutuva olosuhteiden
vaihtelu [@fillmore1975santa, 249]. Vaikka etenkin länsimaisessa
nyky-yhteiskunnassa aikakäsityksen on esitetty muuttuneen luonnon syklisten
tapahtumien muovaamasta lineaariseksi, muodostavat näihin luonnon tarjoamiin
perusyksiköihin suoraan tai välillisesti pohjautuvat ajanyksiköt yhtä kaikki
selkeän ilmausjoukon[^syklVSlin]. Nimitän tätä joukkoa *potentiaalisesti
kalendaarisiksi* ajanilmauksiksi. 

[^syklVSlin]: Tähän vähän syklinen vs lineaarinen. -polemiikkia ja
Viimarannan / Leinon toteamukset siitä, kuinka totta tämä itse
asiassa on...


Luonnossa havaittaviin syklisiin tapahtumiin ovat oletettavasti alun perin
perustuneet lähinnä vuoden, kuukauden ja päivän(vuorokauden) käsitteet
[@nocite]. Useimmat nykyiset ihmisyhteisöt käyttävät lisäksi näistä johdettuja
ajanyksiköitä: vuorokausi jaetaan 24 tuntiin, nämä edelleen 60 minuuttiin, nämä
60 sekuntiin. Päivien perusteella on myös johdettu viikon käsite [historiasta
ks. @zerubavel1989seven]. Erotuksena näistä tarkkarajaisista yksiköistä voidaan
omaksi ryhmäkseen katsoa päivän osat ja vuodenajat, joiden alku- ja loppurajat
ovat häilyviä ja jossain määrin subjektiivisia[^kultkoht] [@fillmore1975santa,
251].

[^kultkoht]: Lisäksi on kiinnostava kysymys, missä määrin
nämä sumearajaisemmat ajanyksiköt ovat kulttuuri- tai
kieliyhteisökohtaisia [vrt. @harme17].


Fillmore [-@fillmore1975santa, 251] jaottelee omaksi ryhmäkseen sellaiset
toistuvat sekvenssit, joiden yksittäiset komponentit on nimetty. Tähän ryhmään
luetaan jo mainitut päivän osat sekä kuukausien ja viikonpäivien nimet
(tammikuu, helmikuu... maanantai, tiistai...). Näitä syklin yksittäisten
komponenttien nimiä Fillmore kutsuu *positionaalisiksi*
ajanyksiköiksi[^vsevolodvapos]. Positionaalisiin ajanyksiköihin voitaisiin lukea 
myös esimerkiksi kuukauden numeroidut päivät (helmikuun 1./2./3./...).

[^vsevolodvapos]: Vsevolodova [-@vsevolodova1975, 29]
käyttää käsitettä *suljetut ajanilmausten joukot*,
jollaisia ovat kaikki tässä positionaalisiksi
määritellyt, mutta myös ylipäätään esimerkiksi kaikki
vuorokauden johdannaisyksiköt (tunnit, minuutit,
sekunnit). Avoimia joukkoja ovat tapaukset, jossa
ajanjaksoja ikään kuin tilannekohtaisesti ryhmitellään
laajemmiksi joukoiksi, sellaiset ilmaukset kuin vuosisata
tai "viisiminuuttinen".

Vsevolodova [-@vsevolodova1975, 31] huomauttaa vielä, että myös ruoka-aikojen
nimitykset voivat toimia päivän osien kanssa limittyvänä ja niitä täydentävänä
positionaalisten ilmausten joukkona. Tällöin päivän voisi katsoa jakautuvan
esimerkiksi osiin *ennen lounasta*, *päiväkahvin aikaan*, *ennen illallista* ja niin
edelleen. Toinen mahdollinen täydentävä päivän jaottelu perustuu Vsevolodovan
(mp.) mukaan auringon asentoihin ja pitää sisällään sellaisia ilmauksia kuin *в
сумерках*, *после восхода* jne. 

Erityisiä nimityksiä yksittäisille ajanjaksoille ovat myös ilmaukset *tänään*,
*huomenna* ja *eilen*, jotka ovat ikään kuin lyhennettyjä versioita ilmauksista
*tänä päivänä*, *tätä seuraavana päivänä* ja *tätä edeltäneenä päivänä*. Näitä
termi "positionaalinen" ei kuitenkaan koske.

Nimitän kaikkia edellä lueteltuja ajanilmauksia *potentiaalisesti
kalendaarisiksi ajanilmauksiksi*. Määrite *potentiaalinen* on termissä mukana
siksi, että lueteltuja ajanilmauksia voidaan käyttää kahdella tavalla,
kalendaarisesti tai ei-kalendaarisesti. 

Ei-kalendaarinen käyttö tarkoittaa, että ajanilmausta käytetään puhtaasti
ajallisen etäisyyden tai laajuuden mittayksikkönä. Esimerkiksi ilmaus *viikon
kuluttua hautajaisista* kertoo, että tapahtumien E~1~ ja E~2~ etäisyys on viikon
mittainen. Kalendaarinen käyttö puolestaan tarkoittaa, että tapahtumat
sijoitetaan jollekin käytettävän ajanyksikön identifioimalle ajanjaksolle, jolla
on määrätty alku- ja loppupiste [@fillmore1975santa, 250]. Ilmaukset
*hautajaisten jälkeisellä viikolla*, *koko viime vuoden*  ja *tulevana
maanantaina* ovat näin ollen kalendaarisia. On huomattava, että alun perin
luonnon sykleihin perustuneista ajanilmauksista edelleen johdettuja yksiköitä
kuten minuutti ja sekunti käytetään kalendaarisesti vain hyvin harvoin (*sillä
sekunnilla*). 

Luonnon tarjoamiin sykleihin palautuvien käsitteiden lisäksi potentiaalisesti
kalendaarisiin ilmauksiin voidaan vielä lukea erilaisiin juhlapäiviin perustuvat
ajanilmaukset, kuten *jouluna*. Vsevolodova [-@vsevolodova1975, 30] esittää
myös, että vuoden voi tietyissä konteksteissa ajatella jakautuvan *kausiin*,
kuten erityisesti venäläisille tärkeä *дачный сезон* ('mökkikausi'). Eräässä mielessä nämä voi
jopa tulkita kuukausien nimien kaltaisiksi vuotta jaksottaviksi
positionaalisiksi yksiköiksi, jotka esimerkiksi suomessa muodostavat
summittaisia syklejä kuten *pääsiäinen--juhannus--joulu*[^lisaajuhlista].
Tämänkaltainen vuoden jakaminen on altis muutoksille ja oletettavasti
esimerkiksi juhlapäivillä on aikaisemmin historiassa ollut huomattavasti
suurempi rooli ajan jaksottamisessa [@tobecited]. 

[^lisaajuhlista]:  Positionaalisesti käyttäytyvien juhlapäivien ja vuosittain
toistuvien kausien lisäksi potentiaalisesti kalendaarisiin ilmaisuihin voidaan
laskea kertaluontoiset, mutta silti erikseen nimetyt kaudet historiassa, kuten
ilmauksissa *jääkaudella/liitukaudella*. Kieltenvälisestä vertailusta tässä
suhteessa ks. luku x.


Niin juhlapäiville kuin muillekin positionaalisille ajanyksiköille on ominaista,
että niitä voidaan tavallisesti käyttää *ainoastaan kalendaarisesti*.
Suurimmassa osassa tapauksia ei-kalendaarinen käyttö mittayksikkönä tuottaisi
ainakin lähtökohtaisesti kyseenalaisia rakenteita, kuten *kaksi
joulua/maanantaita/aamua sitten* [vrt. @haspelmath1997space, 27]. 

#### Ei-kalendaariset ajanilmaukset

Potentiaalisesti kalendaaristen ajanilmausten lisäksi luonnolliset kielet
sisältävät koko joukon muita aikaa ilmaisevia sanoja. Kieltenvälistä vertailua
ajatellen on syytä korostaa, että kalendaariset ilmaukset muodostavat joukon,
joka ydinosiltaan on lähes identtinen suomessa ja venäjässä. Ei-kalendaaristen
ilmausten osalta vertailu hankaloituu, ja käännösvastineiden määrittelystä tulee
monimutkaisempaa.  Ero johtuu ennen muuta siitä, että
kalendaarisille ajanilmauksille voidaan (useimmissa tapauksissa) erottaa
kielestä riippumattomat, eksaktit referentit puhujien tajunnassa, kun taas
ei-kalendaaristen ilmausten viittauskohteet ja merkitys ovat hämärärajaisempia
ja subjektiivisempia. 

Hyvin lähellä kalendaarisia ajanilmauksia ovat *indefiniittisesti aikaa
kvantifioivat* ilmaukset. Tällä ryhmällä tarkoitan sanoja ja lausekkeita, jotka
viittaavat johonkin ajanjaksoon tai ajalliseen ekstensioon, mutta eivät käytä
mitään selkeästi määriteltyä yksikköä. Tavallisimpia tämän ryhmän edustajia ovat
ilmaukset, joissa on mukana suomen *aika-* tai venäjän *время*- tai
*пора*-sana[^vremja700], kuten *некоторое время назад*, *vähän aikaa*, *в те
времена*, *nuijasodan aikaan* ja niin edelleen. Näissä ilmauksissa sanat
aika/время tai suomessa usein myös *hetki* (venäjän момент/миг/мгновение
rajoitetummin) esiintyvät ikään kuin kalendaarisen ajanilmauksen paikalla, mutta
epämääräisempänä, tarkkaan rajaamattomana mittayksikkönä.  Toisaalta etenkin
venäjässä etymologialtaan kalendaariset ilmaukset kuten *минута* voivat
käyttäytyä ainoastaan aikaa kvantifioivasti [vrt. esim. @jakovleva2013].


[^vremja700]: Viimaranta [-@viimaranta2006talking, 29] huomauttaa, että vaikka
время-sana on reilusti käytetympi, myös пора-sanan voi perustellusti katsoa
kuuluvan venäjän aikaa merkitseviin sanoihin.


Indefiniittisesti aikaa kvantifioivia ilmauksia paljon suurempia
ei-kalendaaristen ajanilmausten ryhmiä ovat lähinnä syntaktisten ja
morfologisten ominaisuuksien perusteella eroteltavat *aikaa ilmaisevat
ei-kalendaariset nominilausekkeet*, *aikaa ilmaisevat adverbit ja pronominit*
sekä *aikaa ilmaisevat sivulauseet ja nominaalirakenteet* [vrt. @klein1999,
147-148].

Aikaa ilmaisevat nominilausekkeet voidaan jakaa ilmauksiin, joissa aika
suhteutetaan johonkin yleisesti tunnettuun (objektiiviseen) historialliseen
kiintopisteeseen (*toisen maailmansodan alussa*, *после расспада СССР*) sekä
ilmauksiin, joissa kiintopiste on subjektiivinen (*lapsuudessa*, *näytösten
välillä*, *ennen kaatumistaan*)[^overlap]. Aikaa ilmaisevien adverbien ja
pronominien heterogeeniseen ryhmään kuuluvat paitsi itsenäisinä konstituentteina
esiintyvät ilmaukset kuten *теперь*, *сейчас*, *kauan*, *тогда*, *vastikään*,
*часто*, *lopulta* ja  *yleensä*, myös usein vain toisen ajanilmauksen
määritteenä esiintyvät *jo*, *vielä*, *только* ja eräät muut. Näiden ilmausten
tarkempi ryhmittely tapahtuu jäljempänä taksonomioiden 2 ja 3 määrittelyn
yhteydessä. 

[^overlap]: Ajanilmaisujen luokittelussa on väistämättä enemmän tai vähemmän
ristikkäisyyksiä: esimerkiksi aikaan-sanan käytön postpositiona voisi siirtää
myös indefiniittisesti aikaa kvantifioivien ilmausten alta adpositioilmauksiin.
Olen kuitenkin halunnut pitää indefiniittisesti aikaa ilmaisevien luokan
erillisenä luokkanaan, koska se rinnastuu erityisellä tavalla varsinaisiin
potentiaalisesti kalendaarisiin ajanilmauksiin.


Kolmas suuri ei-kalendaaristen ajanilmaisujen ryhmä ovat suoraan verbin
täydennyksinä toimivat[^prondisclaimer] aikaa ilmaisevat sivulauseet ja
nominaalirakenteet. Suomessa tämän ryhmän ilmaisuja muodostetaan *kun-* ja
*kunnes*-konjunktioilla sekä temporaalirakenteella (*syötyämme illallista
menimme ulos*), venäjässä когда- ja пока-konjunktioilla sekä erilaisilla
gerundirakenteilla (*поужинав, мы вышли на улицу*).


[^prondisclaimer]: määritteellä *suoraan verbin
täydennyksinä toimivat* haluan sulkea pois aikaa
ilmaisevat relatiivilauseet, jotka ovat aina jonkin verbistä
riippuvan sanan dependenttejä. 

Taksonomia 1 on esitetty kootusti seuraavassa kuviossa:


\rotatebox{90}{
\begin{forest}
  for tree={
    child anchor=west,
    parent anchor=east,
    grow'=east,
  %minimum size=1cm,%new possibility
  %text width=4cm,%
    %draw,
    anchor=west,
    %edge path={
    %  \noexpand\path[\forestoption{edge}]
    %    (.child anchor) -| +(-5pt,0) -- +(-5pt,0) |-
    %    (!u.parent anchor)\forestoption{edge label};
    %},
  }
[Ajanilmaukset
    [Potentiaalisesti kalendaariset, text width=3cm
        [Jako positionaalisuuden mukaan, text width=4cm
            [Positionaaliset
                [\small{\emph{Maanantaina, koko huhtikuun, kello viisi}}, text width=4cm,  edge=dotted, l=4cm]
                ]
            [Ei-positionaaliset
                [\small{\emph{Sinä vuonna, kaksi tuntia}}, text width=4cm,edge=dotted, l=4cm]
                ]
            ]
        [Jako definiittisyyden mukaan, text width=4cm
            [Definiittiset
                [\small{\emph{minuutin, maaliskuussa}}, text width=4cm,edge=dotted, l=4cm]
            ]
            [indefiniittiset
                [\small{\emph{aamulla, koko syksyn}}, text width=4cm, edge=dotted, l=4cm]
                ]
            ]
        [Jako semanttisiin ryhmiin
            [Alun perin luonnon sykleihin perustuneet, text width=3.4cm
                [\small{\emph{vuosi, kuukausi, päivä}}, text width=4cm, edge=dotted, l=0.5cm]
                ]
            [Edelleen johdetut
                [\small{\emph{minuutissa, viikon kuluttua}}, text width=4cm, edge=dotted, l=4cm]
                ]
            [Juhlapäivät
                [\small{\emph{jouluna, juhannuksen jälkeen}}, text width=4cm, edge=dotted, l=4cm]
                ]
            ]
        ]
    [Ei-kalendaariset, text width=3cm
        [Indefiniittisesti aikaa kvantifioivat
                [\small{\emph{siihen aikaan, hetki sitten}}, text width=4cm, edge=dotted, l=8.6cm]
        ]
        [Adpositioilmaukset,
            [Objektiivinen kiintopiste
                [\small{\emph{ennen toista maailmansotaa}}, text width=4cm,edge=dotted, l=5cm]
                ]
            [Subjektiivinen kiintopiste
                [\small{\emph{ruoan jälkeen}}, text width=4cm, edge=dotted, l=5cm]
            ]
            ]
        [Adverbi- ja pronomini-ilmaukset
            %Itsenäiset & epäitsenäiset erikseen?
                [\small{\emph{joskus, silloin, jo, kauan}}, text width=4cm, edge=dotted, l=8.6cm]
        ]
        [Temporaaliset sivulauseet ja nominaalirakenteet, text width=4cm
                [\small{\emph{syötyämme menimme ulos, valvoin, kunnes silmissä sumeni}}, text width=4cm, edge=dotted, l=8.6cm]
        ]
        ]
]
\end{forest}
}


\normalsize
 
\noindent \headingfont \small \textbf{Kuvio 1}: Ajanilmausten muodostustapaan perustuva taksonomia \normalfont 


### Taksonomia 2: ajanilmausten semanttiset funktiot {#taksn2}


Edellä ajanilmauksia  luokiteltiin lähinnä sen perusteella, miten ne on
muodostettu (mikä on niiden alkuperä) ja mitä syntaktisia ominaisuuksia niillä
on. Nyt siirrytään tarkastelemaan, mitä eri ajallisia merkityksiä näillä
ilmauksilla haluaan välittää. Nimitän näitä merkityksiä Haspelmathin
[-@haspelmath1997space] terminologiaa soveltaen ajanilmausten *semanttisiksi
funktioiksi*.

Johanna Viimaranta [-@viimaranta2006talking, 319] erottaa yksityiskohtaisessa
katsauksessaan suomen ja venäjän ajanilmauksiin 56 eri merkitystä, joita puhujat
ajasta haluavat ilmaista. Viimarannan aineistosta selkeimmin esille nouseva
johtopäätös on, että pohjimmiltaan niin suomessa kuin venäjässä puhujat pyrkivät
ilmaisemaan ajasta samoja asioita -- esimerkiksi sitä, kuinka pitkään tapahtumat
kestävät, kuinka usein tapahtumat toistuvat ja tapahtuuko jokin tapahtuma
sopivaan tai oikeaan aikaan [-@viimaranta2006talking, 321]. Voidaan hyvin
olettaa, että monet näistä kategorioista ovat enemmän tai vähemmän
universaaleja. Suppeampia ja yleistettävyyteen pyrkiviä jaotteluja ovatkin
aikojen saatossa esittäneet muun muassa @kucera1975time, @klein1999,
@jacobson1978use, @haspelmath1997space sekä monet muut.

Otan tässä semanttisten funktioiden luokittelun pohjaksi Kuceran ja Trnkan
[-@kucera1975time] esittämän kolmijaon *T-adverbiaaleihin* (ajallista lokaatiota
ilmaiseviin), *D-adverbiaaleihin* (ajallista kestoa ilmaiseviin) ja
*F-adverbiaaleihin* (taajuutta ilmaiseviin). Tätä lähtökohtaa on käyttänyt myös
Sulkala [-@sulkala1981] analysoidessaan ajan adverbeja[^atermit] suomessa [vrt. myös esim. @quirk85, 230 ja englanti].
Lisäksi muun muassa Wolfgang Klein [-@klein1999, 149] lähtee vastaavasta kolmesta
kategoriasta, joita hän nimittää *positionaalisiksi*, *taajuutta ilmaiseviksi*
ja *kestoa ilmaiseviksi*. Oma jaotteluni on synteesi Kuceran ja Trnkan kolmesta
perusluokasta sekä Haspelmathin semanttisista funktioista, jotka puolestaan jakautuvat
ainoastaan kahteen pääluokkaan: ajalliseen lokaatioon ja ajalliseen ekstensioon
[-@haspelmath1997space, 8]. Varsinaisten *semanttisten ydinfunktioiden* lisäksi
erottelen *marginaalisempien semanttisten funktioiden* joukon.

[^atermit]: Sulkala todella keskittyy analyysissaan ainoastaan *adverbeihin*
sanaluokkana eikä adverbiaaleihin syntaktisena kategoriana.

#### Lokalisoiva semanttinen funktio


T-adverbiaalien kategorialla Kucera ja Trnka [-@kucera1975time, 12] tarkoittavat
yleisimmällä tasolla sitä, että ilmaus lokalisoi jonkin tapahtuman aikaan
vastaamalla kysymykseen *Milloin?*. Käytän tästä semanttisesta funktiosta
nimitystä *lokalisoiva funktio* eli *L-funktio*.  Virkkeet
\ref{ee_lokf} -- \ref{ee_lokf_piste} ovat esimerkkejä L-funktiosta:




\ex.\label{ee_lokf} \exfont \small Pirkko remontoi viime viikolla keittiötä.






\ex.\label{ee_lokf_piste} \exfont \small В шесть часов я еще спал.





\vspace{0.4cm} 

\noindent
Huomaa, että esimerkin \ref{ee_lokf} ajanilmaus ei viittaa vain yhteen hetkeen, vaan
tiettyyn ajanjaksoon, jonka pituutta ei kerrota eli jonka ekstensiota ei
määritellä. Sen sijaan virkkeen \ref{ee_lokf_piste} ajanilmaus osoittaa tarkan
pisteen, johon lauseessa esitetty propositio lokalisoidaan.

Virkkeet \ref{ee_lokf} -- \ref{ee_lokf_piste} ovat Haspelmathin mallissa esimerkkejä
*simultaanista lokaatiosta*: kummankin virkkeen ajanilmaukset identifioivat
jonkin viitepisteen (tai -jakson) ajassa, ja virkkeessä kuvattavasta
tapahtumasta annetaan informaatiota sikäli, kuin se koskettaa tätä
viitepistettä[^viitepm] [@haspelmath1997space, 29]. Kuvattavan tapahtuma siis
tapahtuu viitejakson sisällä tai viitepisteessä. Nimitän viitejakson sisällä
tapahtuvaa lokalisointia *kehyksiseksi* ja viitepisteessä tapahtuvaa
*punktuaaliseksi lokalisoinniksi.*

[^viitepm]: Viitepisteen käsitteen tarkemmasta määrittelystä ks. alaluku \ref{taksn3}.


Simultaanisen lokaation lisäksi Haspelmath [-@haspelmath1997space, 32,35] erottaa
sekventiaalisen lokaation eli lokalisoitavan tapahtuman sijoittumisen joko ennen
tai jälkeen viitepistettä (Pirkko remontoi keittiön ennen lamaa) sekä lokaation
mitattavana etäisyytenä (Pirkko aloitti keittiöremontin viikon kuluttua
lottovoitosta). Sekventiaalisen lokaation erikoistapauksena voidaan pitää
sellaisia ilmaisuja kuin *ensiksi*, *поначалу* ja *lopuksi* [vrt.
@viimaranta2006talking, 115] jotka ilmaisevat jonkin tapahtumajoukon sisäistä
järjestystä. Ajallinen etäisyys puolestaan voi olla definiittistä (*viikon
kuluttua*) tai indefiniittistä (*myöhemmin*).

Niin simultaani, sekventiaalinen kuin ajalliseen etäisyyteen perustuva lokaatio
ovat ajallista lokalisoimista myös Kuceran ja Trnkan mallissa [-@kucera1975time,
13-14]. Haspelmath [-@haspelmath1997space, 33] erottaa kuitenkin vielä
*sekventiaalis-duratiivisen* ajallisen lokalisoinnin: tapahtuman sijoittamisen
ennen tai jälkeen viitepistettä niin, että viitepiste sisällytetään kuuluvaksi
tapahtuman ajalliseen ekstensioon (Pirkko on remontoinut keittiötään
maaliskuusta asti). Niin kuin nimitys *sekventiaalis-duratiivinen* antaa
ymmärtää, tässä tapauksessa kyse on tietyssä mielessä keston ilmaisemisesta. On
kuitenkin olennaista huomata, ettei ilmaus *maaliskuusta asti* ainoastaan kerro
viestin vastaanottajalle että jokin tila on voimassa tietyn ajan, vaan myös lokalisoi
tapahtuman [keittiötä remontoidaan] aika-akselille toteuttaen lokalisoivaa
funktiota. Tämä käy selväksi, jos vertaa ilmausta *maaliskuusta asti* ilmaukseen
*kaksi kuukautta*, joka ilmaisee ainoastaan kestoa. Tästä havainnosta voidaan
johtaa seuraava semanttisia funktioita koskeva määritelmä:\linebreak

\noindent 
\noindent \headingfont \small \textbf{Määritelmä 3}: Yksi ja sama ajanilmaus voi toteuttaa
        useampaa semanttista funktiota\linebreak \normalfont \vspace{0.6cm}

[^tarkekst]: ks. seuraava kappale


L-funktiota voidaan pitää laajimpana tässä esitettävistä kolmesta funktiosta.
Sen ilmaisemiseksi on sekä suomessa että venäjässä käytettävissä lukuisia eri
keinoja ja niin kalendaariset kuin ei-kalendaariset ilmaukset toteuttavat tätä
funktiota usein. L-funktion realisoitumisen voi tiivistää seuraaviin kolmeen
kohtaan.

1. Kalendaaristen ilmausten osalta tavat ilmaista L-funktiota ovat monipuolisia.
   Melkein kaikki suomen paikallissijat voivat toimia L-funktion morfologisena
   ilmaisimena, samoin nominatiivi. Venäjässä rakenne в + akkusatiivi on
   yleinen, niin kuin myös на + akkusatiivi. Lisäksi tiettyjen substantiivien
   (месяц, год) yhteydessä näitä prepositioita seuraa prepositionaali
   [@vsevolodova1975, 28]. Myös instrumentaalimuoto ilman prepositiota on
   tavallinen. Muotoja утром, вечером, зимой, летом jne. voidaan itse asiassa
   pitää ennemmin taipumattomina adverbeinä kuin instrumentaalimuotoisina
   substantiiveinä [@haspelmath1997space, 112].

2. Aikaa kvantifioivat ilmaukset esiintyvät myös hyvin tavallisesti
   L-funktiossa. Esimerkkejä ovat *в те времена*, *sillä hetkellä* yms.

3. Ei-kalendaarisista (ei-nominaalisista) ajanilmauksista L-funktiota
   toteuttavat muun muassa *silloin*, *nyt*, tuolloin, тогда, потом, скоро ja
   monet muut.

#### Taajuutta ilmaiseva semanttinen funktio


Käsittelen toisena semanttisena funktiona Kuceran ja Trnkan F-adverbiaaleiksi
nimittämää luokkaa, koska tällä funktiolla voi nähdä hyvin läheisen suhteen
L-funktioon. F-adverbiaalit ilmaisevat Kuceran ja Trnkan [-@kucera1975time, 67]
mukaan tapahtuman toteutumista useamman kuin kerran, toisin sanoen taajuutta [ks.
myös @sulkala1981, 126]. Tässä suhteessa mahdollisuus määritelmän 3 
mukaiseen usean semanttisen funktion toteutumiseen
yhtä aikaa vaikuttaa todennäköiseltä, sillä usein ilmaistaessa jonkin tapahtuman
tai asiaintilan toistumista ilmaistaan myös, miten tapahtuma lokalisoituu
aika-akselille. Nimitän taajuutta ilmaisevaa semanttista funktiota F-funktioksi.
F-funktiota ilmaistaan suomessa ja venäjässä  karkeasti ottaen neljällä eri
tavalla[^vrtkuc].

[^vrtkuc]: Jaotteluni on eri kuin Kuceralla ja Trnkalla [-@kucera1975time,
67--68], jotka jakavat F-adverbiaalit a) sen perusteella, onko niillä
ankkurointipiste jossain muussa ajanjaksossa vai ei ja b) sen perusteella,
ilmaisevatko ne definiittistä vai indefiniittistä toistomäärää.

\subparagraph*{1. joka / kazhdyj + (kvantifioija) + substantiivi}

Ensimmäinen vaihtoehto on kalendaarisen ajanilmauksen yhdistäminen toistoa
ilmaiseviin *joka*- ja *каждый*-sanoihin. Varsinaisesti kalendaaristen
ilmausten, kuten *joka päivä* ja *joka vuosi* lisäksi myös sanat *hetki*,
*момент*, *мгновение* sekä *kerta/раз* voivat esiintyä tässä funktiossa. Tätä
toistoa voidaan myös kvantifioida lisäämällä järjestysluku substantiivin ja
määritteen väliin (joka *toinen* päivä). Käyttö on sikäli rajoitettua, että
vaikka positionaaliset *maanantai* ja *aamu* toteuttavat usein tämän tyyppistä
toistoa, on ilmaus *joka helmikuu* ainakin oman kielitajuni mukaan vähintäänkin
harvinainen. Venäjästä voidaan tähän ryhmään lukea lisäksi *еже*-alkuiset
kalendaarisista sanoista johdetut adverbit, kuten *ежемесячно*, *ежеминутно* ym.


\subparagraph*{2. Positionaalinen ajanilmaus + instr / po  + dat}


Kucera ja Trnka eivät erittele tätä ryhmää lainkaan F-adverbiaaleihin
kuuluvaksi, vaan analysoivat siihen liittyviä sanoja T-adverbiaalien yhteydessä
[vrt.  @kucera1975time, 12-13]. Myös Haspelmath [-@haspelmath1997space] pitää
vastaavia ilmauksia ainoastaan ajallisen lokaation määrittävinä. Selkeästi mukana
on kuitenkin myös tapahtumien useutta: itse asiassa monessa mielessä tämän
ryhmän ilmaukset (maanantaisin, по вечерам,) ovat synonyymisia edellisen ryhmän
ilmausten kanssa (vrt. kuitenkin \*joka aamupäivä). Määritelmän 3 mukainen taipumus toteuttaa useaa semanttista
funktiota vaikuttaisi erityisen ilmeiseltä juuri tämän ryhmän ilmauksilla.
Esimerkiksi *iltaisin* ei toki sijoita tapahtumaa absoluuttiselle aika-akselille
[vrt. @fillmore1975santa, 252], mutta kylläkin siihen sykliin, jonka osa
positionaalinen termi on.


\subparagraph*{3. Kerta/raz-sanat ja adverbijohdokset}

Toiston numeerisen määrän ilmaiseminen tapahtuu suomen *kerta*- ja venäjän
*раз*-sanojen avulla. Lisäksi kummassakin kielessä on lukusanojen pohjalta
muodostettavia adverbeja,[^kertaadv] joita voidaan usein käyttää synonyymisesti
ilmaisujen *kaksi kertaa* / *два раза* kanssa. Erityistä huomiota
on kiinnitettävä yhtä kertaa tarkoittaviin ilmaisuihin, joiden semanttinen
funktio on useimmiten tulkittava lokalisoivaksi ("kerran Pirkko päätti,
että...", "как-то раз/однажды я увидел на дороге..."). Määrälliselle toistolle
voidaan määrittää jakso, jonka sisällä toisto tapahtuu (esim. kaksi kertaa
*vuodessa*). Lisäksi kardinaalisesta toistosta voidaan erottaa ordinaalinen
toisto, esimerkiksi ilmaus *seitsemännen kerran* tai venäjän *впервые*. [ks.
@hakulinenkarlsson1979, 210].

[^kertaadv]: Suomessa *kahdesti*, *kolmesti* jne., venäjässä дважды, трижды,
четырежды sekä produktiivisemmat mutta vähemmän käytetyt кратно-johdokset
(пятикратно, шестикратно jne.)

\subparagraph*{4. Ei-numeerista taajuutta ilmaisevat adverbit ja aikaa kvantifioivat ilmaukset}

Erotan omaksi joukokseen muut kuin edellä mainitut lukusanoista johdetut
adverbit, jotka ilmaisevat taajuutta. Nämä jaetaan edelleen seuraaviin kolmeen
alaryhmään. Myös eräät aikaa kvantifioivat ilmaukset voi laskea osaksi näitä
ryhmiä.

\subparagraph*{4.a taajuutta korostavat adverbit}

Taajuutta korostavat adverbit kuvaavat tapahtumakertojen määrän puhujan
näkökulmasta suurena tai maksimaalisen suurena. Ensimmäiseen kategoriaan
kuuluvat mm. *usein*, *часто*, *постоянно*, *koko ajan*, *alituisesti*,
*беспрестанно*, jälkimmäiseen puolestaan lähinnä *aina* ja *всегда*.

\subparagraph*{4.b harvuutta korostavat ja neutraalit adverbit}

Tähän ryhmään kuuluu paitsi adverbeja, myös aikaa kvantifioivia ilmauksia kuten
*время от времени* ja *ajoittain*. Tyypillisiä harvuutta korostavia ovat
*редко*, *никогда*, *koskaan* ja joko neutraaleja tai harvuutta korostavia
*joskus*, *silloin tällöin*, *порой* ym.

\subparagraph*{4.c epämääräistä taajuutta ilmaisevat}

Tiettyjen ilmaisujen kohdalla käy hämäräksi, ovatko ne varsinaisesti ajallisia
ja ilmaisevatko ne varsinaisesti taajuutta.  Näiden ilmaisujen kuvaama
tapahtumien toistuvuus on epämääräistä ja useimmiten taajuutta korostavaa.
Luokittelen tähän sellaiset adverbit kuin *yleensä*, *обычно* ja *tavallisesti*.

#### Ekstensiota ilmaiseva semanttinen funktio

Semanttisista ydinfunktioista L-funktiota voi pitää laajimpana, mutta selkeästi
homogeenisempana joukkona kuin F-funktiota, joka on hajanainen ja myös
harvinaisempi[^taajuusgradu]. Ajallista ekstensiota ilmaiseva funktio
(E-funktio) on luultavasti harvinaisempi kuin L-funktio mutta yleisempi kuin
F-funktio[^kyleisyys], ja myös sille on ominaista tietty hajanaisuus, niin että
joitakin hämärärajaisia tapauksia [vrt. @kucera1975time, 56] luokitellaan tässä
varsinaisen *E-funktion* ulkopuolelle.

[^taajuusgradu]: Vertaa esimerkiksi pro gradu -tutkielmassani [-@gradulyhyt, ??]
tehty manuaalinen luokittelu ajankohtaan, taajuuteen ja kestoon, jossa taajuuden
luokka osoittautui huomattavasti muita suppeammaksi.

[^kyleisyys]: Suuntaa antavan käsityksen yleisyydestä voi myös tässä suhteessa
saada pro gradu -tutkielmassani esitettyjen lukumäärien perusteella.

E-funktion määrittäminen ajallisen ekstension käsitteen kautta on yritys välttää
*kesto-käsitteeseen* [vrt. @sulkala1981, 56] liittyvää hajanaisuutta. Samasta
syystä erotan omaksi ryhmäkseen *varsinaisesti ajallista ekstensiota ilmaisevat*
ja *tulkinnanvaraisesti ajallista ekstensiota ilmaisevat tapaukset*. Jaan
varsinaisesti ekstensiota ilmaisevan kategorian kahteen alaryhmään [vrt.
@haspelmath1997space, 38-39], joista ensimmäinen ilmaisee, kuinka kauan jotakin
toimintaa suoritetaan (Pirkko remontoi keittiötä kolme kuukautta) ja toinen,
missä ajassa toiminta suoritetaan (Pirkko remontoi keittiön kolmessa
kuukaudessa). Nimitän ensimmäistä merkitystä *duratiiviseksi ekstensioksi* ja
toista *resultatiiviseksi ekstensioksi*[^nimitystausta].

Duratiivisesta ekstensiosta on kyse myös silloin, kun asetetaan ajallinen
laajuus jonkin toiminnan tapahtumattomuudelle. Hakulisen ja Karlssonin
[-@hakulinenkarlsson1979, 210] nimittävät näitä esimerkkien \ref{ee_negekstfi} ja \ref{ee_negekstru}
kaltaisia tapauksia *kieltoduratiiviksi*.




\ex.\label{ee_negekstfi} \exfont \small En ole nähnyt häntä kahteen vuoteen.






\ex.\label{ee_negekstru} \exfont \small Уже два года, как я ее не вижу





\vspace{0.4cm} 

\noindent
Varsinainen resultatiivinen ekstensio on (niin venäjässä kuin suomessa)
sidoksissa aspektiltaan loppuun suoritettua toimintaa kuvaaviin verbeihin
[@sulkala1981, 38; @tobecited]. Tässä suhteessa sellaiset rakenteet kuin venäjän
*в течение + genetiivi* tai suomen genetiivi + mittaan/aikana/kuluessa eroavat
prototyyppisistä resultatiivisen ekstension ilmaisuista. Katson, että nämä ja
muut vastaavat ilmaukset muodostavat resultatiivisen ekstension alalajin,
*kehyksisen ekstension*, jonka tarkoituksena on ilmaista tietty määrä
resultatiivisuuden suhteen määrittelemättömiä toimintoja tietyn ajan sisällä
(kuten *olen tämän vuoden aikana pelannut paljon tietokonetta*). Samaan tapaan
kuin sekventiaalis-duratiivinen merkitys, myös kehyksisen ekstension merkityksen
voi joissakin tapauksissa nähdä ilmaisevan oikeastaan ajallista lokaatiota (vrt.
в течение прошлого лета / в прошлое лето). On syytä panna merkille, että
kehyksistä ekstensiota ilmaistaan usein myös sivulauseilla (*пока Анна и Ира
делали покупки, мы сидели в машине*).

[^nimitystausta]: Haspelmath käyttää näistä luokista termejä *atelic extent* ja
*telic extent*, Hakulisen ja Karlssonin suomenkielisessä terminologiassa ovat
käytössä käsitteet *duratiivinen* ja *punktuaalinen* kesto [nimityksistä
tarkemmin ks. @sulkala1981, 38].

Ajallinen ekstensio voidaan ilmaista joko implisiittisesti tai
eksplisiittisesti. Kaikki edellä luetellut tapaukset ovat esimerkkejä
implisiittisestä ekstension ilmaisemisesta. Eksplisiittistä ekstensiota
ilmaistaessa E-funktiota toteuttavat paitsi ajanilmaukset, myös ekstensiota
ilmaisevat verbit kuten *kulua*, *jatkua* ja *kestää*. Näissä tapauksissa
ajanilmaus on usein (muttei aina) syntaktisesti määriteltävissä subjektiksi
(keittiön remontoimiseen kului vuosi/kauan/viikkoja).

Seuraavassa on kuvattu tapauksia, joissa tarkasteltavien ilmausten funktio
ajallisen ekstension ilmaisimena tai ajallisuus ylipäätään eivät ole yhtä
selkeästi määriteltävissä kuin edellä esitettyjen esimerkkien kohdalla.




\ex.\label{ee_tulk_heti} \exfont \small He lähtivät heti/pian.






\ex.\label{ee_tulk_nop} \exfont \small Luin kirjan nopeasti.






\ex.\label{ee_tulk_siirto} \exfont \small Ilmoittautumisaikaa jatkettiin keskiviikkoon asti.





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_tulk_heti} voi nähdä ilmaisevan ajallista ekstensiota siinä
mielessä, että niissä kerrotaan, mikä on sen ajanjakson pituus, joka kului
puhehetkestä tapahtuman alkuun, tässä tapauksessa lähtemiseen. Voisi kuitenkin
myös sanoa, että kyseessä on ennemminkin tapahtuman lokalisointi
lähitulevaisuuteen. Esimerkki \ref{ee_tulk_nop} kertoo puolestaan jotain lukemiseen
käytetystä ajasta, mutta on luonteeltaan yhtä lailla myös *tapaa* ilmaiseva.
Esimerkissä \ref{ee_tulk_siirto} kyse on taas ikään kuin metatason ajallisesta
ekstensiosta: ajanilmauksen osoittama ekstensio koskee ennemmin yksittäistä
substantiivia kuin itse tapahtumaa. Esimerkkejä \ref{ee_tulk_heti} -- \ref{ee_tulk_siirto}
lähempänä varsinaista E-funktiota ovat esimerkin \ref{ee_tulk1} kaltaiset tapaukset:




\ex.\label{ee_tulk1} \exfont \small Он приехал сюда на год





\vspace{0.4cm} 

\noindent
Kucera ja Trnka [-@kucera1975time, 57] huomauttavat, että esimerkin \ref{ee_tulk1}
ajanilmaus *на год* ei kuitenkaan kerro lauseen varsinaisen tapahtuman [hän tuli
tänne] ekstensiosta mitään. Sen sijaan lause ikään kuin olettaa toisen
tapahtuman, esimerkiksi [hän viipyi täällä], ja *на год* määrittää juuri tämän
kestoa [vrt. myös @haspelmath1997space, 49]. Hakulinen ja Karlsson käyttävät
suomen kielen vastaavista rakenteista nimitystä "futuurinen duratiivi"
[-@hakulinenkarlsson1979, 210]. 


Esimerkkien \ref{ee_tulk_heti} -- \ref{ee_tulk1} kuvaamien merkitysten luokittelu
*tulkinnanvaraisesti ekstensiota ilmaiseviksi* erotuksena *varsinaisesti
ekstensiota ilmaisevista* on ennen muuta tarkoitettu rajaamaan tutkimuksen
fokusta, niin että huomio kiinnitetään pääasiassa varsinaisesti kestoa
ilmaiseviksi määriteltyihin tapauksiin.


Kummassakin tarkastellussa kielessä resultatiivisen ekstension ilmaisemiseen
käytettävät kielelliset keinot ovat melko selkeitä ja rajattuja. Suomessa
resultatiivista ekstensiota ilmaistaan lähinnä inessiivillä, venäjässä
ilmaisimena taas on rakenne *за* + akkusatiivi. Duratiivista kestoa ilmaisevat
sanat puolestaan ovat kummassakin kielessä syntaktisesti katsottuna
objekteja[^osma2] ja käyttäytyvät myös morfologisesti, kuten objektit, niin että
kummastakin kielestä itse asiassa puuttuu selkeä morfologinen duratiivisen
ekstension indikaattori. Tämä duratiivisen ekstension tunnuksettomuus vaikuttaa
itse asiassa Haspelmathin havaintojen perusteella [-@haspelmath1997space, 120]
melko universaalilta tendenssiltä.

Suomessa duratiivista ekstensiota ilmaisevat konstituentit ovat
totaaliobjekteja, joten ne esiintyvät aina joko genetiivissä tai nominatiivissa
[vrt. @visk, 925]. Venäjässä duratiivisen ekstension ajanilmaukset käyttäytyvät
kuten mitkä tahansa elottomat substantiivit akkusatiivissa [@vsevolodova1975,
29]. Kummassakin kielessä kiellon vaikutus objektiin realisoituu myös
ajanilmauksilla tuottaen suomessa partitiiviobjektin (en pelannut kahta
tuntiakaan) ja venäjässä genetiivimuotoisen objektin (я и двух часов не играл).
Adverbit esiintyvät luonnollisesti sellaisenaan, joskin esimerkiksi suomen
*kauan* saattaa kieltoduratiivissa saada partitiivitaivutuksen [@tobecited].

Varsinaisten E-funktioiden alalajeista voidaan todeta, että kieltoduratiivi
muodostetaan suomessa illatiivilla. Venäjässä kielteisyys ei läheskään aina näy
ajanilmauksessa morfologisesti, mutta se voidaan ilmaista rakenteella (уже) +
ajanilmaus + как -- kuten esimerkissä \ref{ee_negekstru}. Kehyksisen ekstension
ilmaisutavat käytiin läpi edellä luokan esittelyn yhteydessä.


[^osma2]: vrt. OSMAn käsite edellä.

Ajanilmausten muodostusta koskevaa taksonomiaa rakennettaessa tässä käytetty
malli sai monilta osin tukea Vsevolodovan [-@vsevolodova1975]
yksityiskohtaisesta jaottelusta. Semanttisten funktioiden osalta Vsevolodovan
analyysi [-@vsevolodova1975, 19-22] on jossain määrin erisuuntainen kuin omani.
Vsevolodovan mallissa ajanilmaukset jaetaan kolmeen tärkeimpään binääriseen
oppositioon. Ensimmäinen oppositio perustuu siihen, onko lausumassa kuvattu
toiminta samanaikaista kuin ajanilmauksen osoittama jakso vai ei (vrt ilmauksia
*koko yön* -- samanaikaista -- ja *ennen yötä* -- eriaikaista). Toinen oppositio
jaottelee ilmaukset sen perusteella, kuinka totaalisesti kuvattu toiminta
täyttää ilmausten osoittaman ajan (vrt. ilmauksia *прошлой зимой* -- vain
osittain -- ja *с июля по сентябрь* -- totaalisesti).  Kolmas oppositio vastaa
melko hyvin tässä esitettyä F-funktiota ja ilmaisee, onko kyse yksittäisestä vai
toimintakertoihin jaettavasta toiminnasta.

#### Muut semanttiset funktiot

L-, F-, ja E-funktiot muodostavat ajanilmausten semanttiset ydinfunktiot. Kuten
esimerkiksi Viimarannan 56-osaisen luokittelun perusteella voi todeta, näiden
lisäksi voidaan määritellä koko joukko muita, enemmän tai vähemmän
ydinfunktioiden kanssa limittyviä *marginaalisia semanttisia funktiota*.
Viimarannan jaottelussa yksittäiset semanttiset funktiot ryhmitellään kymmeneksi
laajemmaksi ryhmäksi, joista tässä määriteltyjen ydinfunktioiden ulkopuolelle
osuvat ryhmät *sopiva ja oikea aika*, *elämä aikana*, *ajan rajallisuus*, *ajan
kuluminen* ja *ajan likimääräisyys*. Viimarannan jaottelun lähtökohdat ovat
erilaiset kuin tässä tutkimuksessa ja jaottelu liikkuu paljon tarkemmalla
semanttisten nyanssien ja aikaan liittyvien metaforien tasolla. Tämän
tutkimuksen kannalta relevantti on lähinnä *ajan likimääräisyyden*
[@viimaranta2006talking, 148] luokka. Lisäksi tarkastelen ajanilmauksen suhdetta
viestijöiden odotuksiin (presuppositionaalinen funktio).

[presuppositionaalinen funktio tarkemmin kesken]

Haspelmath [-@haspelmath1997space, 48] huomauttaa, että monissa kielissä on oma
adpositionsa, jonka tehtävä on ilmaista ajanmääreen likimääräisyyttä.  Venäjässä
tätä funktiota toteuttaa *около-prepositio*, suomessa *maissa*-postpositio.
Lisäksi venäjässä voidaan käyttää lukusanojen yhteydessä rakennetta, joissa
lukusanan genetiivimuotoinen dependentti siirretään sen eteen (*часов в пять*).
Lisäksi likimääräisyyttä voidaan ilmaista delimitatiiveilla. Venäjässä tämä on
luontevampaa (*часок я постоял в магазине*) kuin suomessa (?*siinä kuluu ehkä
tunteroinen*).  Likimääräisyys ei kuitenkaan ole itsenäinen semanttinen funktio,
vaan likimääräisyyttä viestivä ajanilmaus toteuttaa aina myös joko lokalisoivaa
(tulimme takaisin *viiden maissa*) tai ekstension ilmaisevaa funktiota (*olimme
siellä päivän-pari*).

Taksonomia 2 on tiivistetty seuraavaan kaavioon:


\begin{landscape}

\scriptsize

\begin{forest}
  for tree={
    child anchor=west,
    parent anchor=east,
    grow'=east,
  %minimum size=1cm,%new possibility
  %text width=8cm,%
    %draw,
    anchor=west,
    %edge path={
    %  \noexpand\path[\forestoption{edge}]
    %    (.child anchor) -| +(-5pt,0) -- +(-5pt,0) |-
    %    (!u.parent anchor)\forestoption{edge label};
    %},
  }
[Ajanilmausten semanttiset funktiot, text width=1.5cm
[Ydinfunktiot
    [Lokalisoiva, text width=2cm  
        [simultaani
            [kehyksinen
                [\scriptsize{\emph{Pirkko remontoi viime viikolla keittiötä}}, text width=8cm, edge=dotted, l=7.65cm]
            ]
            [punktuaalinen
                [\scriptsize{\emph{Heräsin kello kuusi}}, text width=8cm, edge=dotted, l=7.65cm]
            ]
        ]
        [sekventiaalinen
            [\scriptsize{\emph{Pirkko remontoi keittiön ennen lamaa}}, text width=8cm, edge=dotted, l=9.4cm]
        ]
        [sekventiaalis-duratiivinen, name=sekvd
            [\scriptsize{\emph{Pirkko remontoi maaliskuuhun asti}}, text width=8cm, edge=dotted, l=9.4cm]
        ]
        [ajallinen etäisyys
            [\scriptsize{\emph{Remontti alkaa viikon päästä}}, text width=8cm, edge=dotted, l=9.4cm]
        ]
    ]
    [Taajuutta ilmaiseva., text width=2cm
        [joka/каждый + N
            [\scriptsize{\emph{joka hetki, joka vuosi}}, text width=8cm, edge=dotted, l=9.4cm] 
        ] 
        [positionaalinen ilmaus + sija/prepositio
            [\scriptsize{\emph{maanantaisin, iltaisin}}, text width=8cm, edge=dotted, l=9.4cm] 
        ] 
        [kerta/раз + johdokset
            [\scriptsize{\emph{kaksi kertaa vuodessa, kolme kertaa, neljästi}}, text width=8cm, edge=dotted, l=9.4cm] 
        ] 
        [ei-numeerinen taajuus
            [taajuutta korostavat
                [suuri taajuus
                    [\scriptsize{\emph{usein, alituisesti}}, text width=8cm, edge=dotted, l=3.5cm]
                ]
                [maksimaalinen taajuus 
                    [\scriptsize{\emph{aina}}, l=3.5cm, edge=dotted]
                ]
            ]
            [harvuutta korostavat/neutr.
                [korostavat
                    [\scriptsize{\emph{harvoin, koskaan}}, text width=8cm, edge=dotted, l=2.65cm]
                ] 
                [neutraalit
                    [\scriptsize{\emph{joskus, silloin tällöin}}, text width=8cm, edge=dotted, l=2.65cm] 
                ]
            ] 
            [epämääräinen taajuus
                [\scriptsize{\emph{yleensä, tavallisesti}}, text width=8cm, edge=dotted, l=6.2cm]
            ] 
        ] 
    ]
    [Ekstensiota ilmaiseva., text width=2cm, name=ekstf
       [Varsinaiset
           [duratiivinen ekstensio
               [myöntöduratiivi
                   [\scriptsize{\emph{Pirkko remontoi keittiötä kolme kuukautta}}, text width=8cm, edge=dotted, l=4.4cm]
               ]
               [kieltoduratiivi
                   [\scriptsize{\emph{En ole nähnyt häntä kahteen vuoteen}}, text width=8cm, edge=dotted, l=4.4cm]
               ]
           ]
           [resultatiivinen ekstensio
               [varsinainen res.ekst.
                   [\scriptsize{\emph{Pirkko remontoi keittiön kolmessa kuukaudessa}}, text width=8cm, edge=dotted, l=4.1cm]
               ]
               [kehyksinen ekstensio
                   [\scriptsize{\emph{Olen tämän vuoden aikana pelannut paljon tietokonetta}}, text width=8cm, edge=dotted, l=4.1cm]
               ]
           ]
       ] 
       [Tulkinnanvaraiset
           [päämäärää ilmaisevat
               [\scriptsize{\emph{Hän saapui kolmeksi kuukaudeksi}}, text width=8cm, edge=dotted, l=6.5cm]
           ]
           [aikaa ja tapaa ilmaisevat
               [\scriptsize{\emph{Remontti tehtiin nopeasti}}, text width=8cm, edge=dotted, l=6.5cm]
           ]
           [metatason ekstensio
               [\scriptsize{\emph{Tapahtuma siirrettiin loppuvuoteen}}, text width=8cm, edge=dotted, l=6.43cm]
           ]
       ] 
       [ekstension eksplisiittisyys
          [implisiittinen
              [\scriptsize{\emph{Remontoimme kaksi kuukautta}}, text width=8cm, edge=dotted,l=5.6cm]
          ] 
          [eksplisiittinen
              [\scriptsize{\emph{Remontti kesti / remonttiin kului kaksi kuukautta}}, text width=8cm, edge=dotted, l=5.6cm]
          ] 
       ]
    ]
]
[Muut funktiot
[Presuppositionaalinen funktio, text width=4cm
  [\scriptsize{\emph{jo, vielä, vasta}}, text width=4cm, edge=dotted, l=11.34cm]
]
[Likimääräisyyttä ilmaiseva,text width=4cm
  [\scriptsize{\emph{maissa, aikoihin}}, text width=4cm, edge=dotted, l=11.34cm]
]
]
]
\draw[->,dotted] (sekvd) to[out=west, in=north] (ekstf);
\end{forest}

\normalsize

\end{landscape}
 
\noindent \headingfont \small \textbf{Kuvio 2}: Ajanilmausten semanttisiin funktioihin perustuva taksonomia \normalfont 



### Taksonomia 3: ajanilmausten viitepisteet {#taksn3}

Ajanilmausten eri jaottelutavoista on otettava esille vielä yksi ominaisuus,
joka osittain limittyy niin edellä määriteltyjen semanttisten funktioiden kuin
toisaalta ajanilmausten muodostustapojenkin kanssa. Tämä ominaisuus jakaa
ajanilmaukset sen perusteella, onko niillä jokin viittauskohde eli referentti
vai ei.

Tarkastellaan esimerkkiä \ref{ee_paikref}:




\ex.\label{ee_paikref} \exfont \small Tavataan kello 14 Mikon työhuoneessa





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_paikref} ilmauksilla *kello 14*, *Mikon* ja *Mikon työhuoneessa*
on kaikilla jokin viittauskohde, jonka kumpikin viestijä (puhuja ja viestin
vastaanottaja) oletettavasti kykenee identifioimaan. *Mikko* viittaa tiettyyn,
kummankin viestijän tunnistamaan henkilöön ja *Mikon työhuoneessa* vastaavasti
kummankin viestijän tunnistamaan paikkaan. Sekä *työhuone* että Mikko ovat
konkreettisia objekteja viestijöiden mielen ulkoisessa maailmassa. Ilmauksen
*kello 14* määrittelemisen konkreettiseksi osaksi fyysistä todellisuutta voi
nähdä riippuvan määrittelijän aikakäsityksestä, mutta oleellista on se, että
puheessa myös siihen suhtaudutaan samalla tavalla konkreettisena objektina kuin
paikanilmauksen *Mikon työhuoneessa* viittauskohteeseen: kognition tasolla
ilmauksen *kello 14* referentti on aivan yhtä todellinen kuin ilmausten
*työhuone* ja *Mikko* referentit.


Ajanilmausten viittauskohteet voidaan tulkita pisteiksi aika-akselilla aivan
samaan tapaan kuin paikanilmausten viittauskohteet ovat pisteitä jollakin
kolmesta spatiaalisesta akselista [@haspelmath1997space, 21]. Samalla tavoin
kuin ilmaus *siellä* viittaa kohtaan maapallolla, jonka ekstensio voi olla suuri
(esimerkiksi valtion alue) tai pieni (esimerkiksi talon piha), ilmaus *silloin*
voi viitata ekstensioltaan suureen (vuosisatoja) tai pieneen (sekunteja) jaksoon
aika-akselilla. Lisäksi ilmaus *sillä hetkellä* tarkoittaa yhtä konkreettista
pistettä aika-akselilla samalla tavoin kuin ilmaus *sillä kohtaa* voi viitata
yhteen konkreettiseen pisteeseen jollakin spatiaalisella akselilla. 

Ajanilmaukset voivat siis kantaa mukanaan referentiaalista tietoa eli olettaa
jonkin viittauspisteen. Näin ei kuitenkaan aina ole, vaan on olemassa myös
ei-referentiaalisia ajanilmauksia. Ei kuitenkaan ole yksiselitteistä
määritellä, onko ilmauksella referentti vai ei, vaan referentiaalisuus on
nähtävä binäärisen opposition sijaan jatkumona, jonka toisessa päässä ovat
kiistatta referentiaaliset, toisessa päässä kiistatta ei-referentiaaliset.
Tarkastellaan seuraavaksi, minkälaiset ilmaukset selkeimmin akselin kumpaankin
ääripäähän sijoittuvat.

#### Referentiaalisuuden indikaattoreita


Intuitiivisesti vaikuttaisi siltä, että jako kalendaarisesti ja
ei-kalendaarisesti käytettyihin ilmauksiin korreloi melko suoraan
referentiaalisuuden kanssa. Kalendaarisilla ilmauksilla, kuten *ensi
huhtikuussa*, *в два часа" on selkeät viittauskohteet aikajanalla, kun taas
*olimme siellä minuutin* ei oletettavasti identifioi mitään tiettyä aikajanan
minuuttia. Kuitenkin on todettava, että myös kaikissa taksonomian 1
ei-kalendaarisissa ryhmissä on ilmauksia, jotka voivat esiintyä
referentiaalisesti. Näitä ovat esimerkiksi indefiniittisesti aikaa kvantifioiva
*siihen aikaan*, ei-kalendaarinen nominilauseke *buurisodan jälkeen*, adverbi
*тогда* ja niin edespäin.

Vaikuttaisikin siltä, että kalendaarisuus on riittävä, joskaan ei välttämätön
ehto ilmauksen referentiaalisuudelle. Tämä yleistys ei kuitenkaan ole täysin
pätevä, sillä on myös löydettävissä kalendaarisia ilmauksia, jotka *eivät* ole
referentiaalisia. Esimerkiksi lauseen *sellaisina päivinä ei jaksaisi edes
hampaitaan harjata* ajanilmaus on kalendaarinen, muttei sisällä mitään
varsinaisesti identifioitavissa olevaa[^ident_seur] viitepistettä aikajanalta.
Kucera ja Trnka [-@kucera1975time, 13] mainitsevat lisäksi sellaiset ilmaukset
kuin *ночью* ja *в апреле*, jotka eivät myöskään välttämättä viittaa yhteen
identifioitavissa olevaan pisteeseen. Tällöin kyse on lähinnä geneerisistä
lauseista, kuten *в апреле обычно снега уже мало* tai *ночью мне всегда
холодно*.


Toisena referentiaalisuuden todennäköisenä indikaattorina voidaan pitää
lokalisoivaa semanttista funktiota. Verrattuna muihin semanttisiin funktioihin
L-funktio tuntuu varmimmin ennustavan jonkin ilmauksen referentiaalisuutta:
edellä esitetyistä esimerkeistä ei-referentiaalinen *olimme siellä minuutin*
toimii E-funktiossa, mutta *ensi huhtikuussa* ja *два часа* L-funktiossa.
E-funktiokaan ei kuitenkaan sulje pois referentiaalisuutta -- esimerkiksi
duratiivista ekstensiota ilmaiseva ajanilmaus lauseessa *pelasin korttia koko
yön* on selkeästi referentiaalinen. Jälleen kerran vaikuttaisi siltä, että
ajallinen lokalisoitavuus on riittävä, joskaan ei välttämätön ehto
referentiaalisuudelle. Kalendaarisuuden riittävyyden kyseenalaistaneet esimerkit
osoittavat kuitenkin, ettei L-funktiokaan ole vielä tae referentiaalisuudesta,
sillä lauseen *в апреле обычно снега уже мало* ajanilmauksella on nimenomaan
lokalisoiva funktio -- lokalisointi vain ei tapahdu absoluuttisella aikajanalla,
vaan pikemminkin *suhteessa positionaalisen ajanilmauksen implikoimaan sykliin*.
Vastaavien ajanilmausten roolia informaation välittämisessä tutkitaan tarkemmin
seuraavassa alaluvussa.

[^ident_seur]: Tarkemmin identifioitavuuden käsitteestä ks. seuraava alaluku.

Vastaesimerkeistä huolimatta voidaan todeta, että kalendaariset ja L-funktiossa
toimivat ajanilmaukset ovat selkeimmin referentiaalisia. E-funktiossa toimivat
ei-kalendaariset ilmaukset puolestaan ovat lähtökohtaisesti myös
ei-referentiaalisia, ja E-funktiossa toimivilla kalendaarisillakin ilmauksilla
viittauskohde on vain harvoin. Lisäksi ei-numeerista taajuutta ilmaisevat sanat
kuten *joskus* ja *tavallisesti* samoin kuin numeerista taajuutta ilmaisevat
*пять раз* ja *трижды* ovat referentiaalisuusjatkumon ei-referentiaalisessa
ääripäässä. F-funktion alaluokkien 1 ja 2 osalta määritteleminen on jossain
määrin haastavampaa: onko esimerkiksi ilmauksella *maanantaisin* viittauskohde?

Luonnollisesti myös ajanilmauksen itsenäisyys ja syntaktinen luonne ovat
tekijöitä, joilla on vaikutusta referentiaalisuuteen. Esimerkiksi
presuppositionaalisessa funktiossa käytettävillä  *vielä-*, *jo-* tai
*уже-*adverbeilla ei tavallisesti ole omaa viittauskohdettaan. Kuitenkin myös
nämä sanat voivat tietyissä konteksteissa viitata *puhehetkeen*: ilmauksissa
*почему он еще не пришел?* tai  *Lähdetään jo!* sanoilla *еще* ja *jo* on
presuppositionaalisen funktion lisäksi myös lokalisoiva funktio, ja ne
ankkuroivat *tulla-* ja *lähteä*-tyyppisten verbien kuvaaman toiminnan
aikajanalle.[^ankkurihuom] Toisaalta näissä ja monissa muissa tapauksissa myös ajanilmauksen
ajallisuus sinänsä alkaa horjua, ja sanojen funktio kallistuu kohti
diskurssipartikkelin roolia. Tällaisilla temporaalisuutensa menettäneillä
ilmaisuilla, kuten usein sanojen nyt, sitten ja *потом* kanssa, ei
luonnollisesti ole myöskään referenttiä.

[^ankkurihuom]: Käytän referentiaalisuudesta puhuttaessa termiä *ankkuroida*
tarkoittamaan kiinnekohdan löytämistä aikajanalta. Ankkuroimista informaatiorakenteen
kannalta käsitellään luvussa x.x [ks. Prince 81:237]

#### Viittauskohteen tarkkuus

Referentiaalisten ajanilmausten viittauskohteet voivat olla tarkempia tai
epämääräisempiä. Punktuaalista lokalisoivaa funktiota edustavilla
ajanilmauksilla viittauskohde on yksittäinen piste, kehyksistä lokalisoivaa
funktiota edustavilla puolestaan lyhyempi tai pidempi jakso. Kummassakin
funktiossa esiintyvillä sanoilla voi olla joko eksakteja tai epämääräisiä
referenttejä. Esimerkiksi virkkeen \ref{ee_pallootsa} ajanilmauksen referentti on
punktuaalinen (pallon iskeytyminen otsaan viittaa yhteen jakamattomaan hetkeen)
mutta myös epämääräinen (viitepisteen tarkkaa sijaintia aikajanalla ei
ilmaista). Virkkeessä \ref{ee_myoharem} sen sijaan ajanilmaus toteuttaa kehyksistä
lokalisoivaa funktiota, ja referentti on samalla tavoin epämääräinen. 




\ex.\label{ee_pallootsa} \exfont \small Sain hiljattain pallon otsaani






\ex.\label{ee_myoharem} \exfont \small Remontti tehdään sitten myöhemmin.





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_pallootsa} ja \ref{ee_myoharem} eroavat lisäksi siinä, että edellisessä
viittaushetki on puhujan tiedossa ja sille voidaan määrittää tarkka aikapiste
puhujan (muttei vastaanottajan) subjektiivisella aikajanalla, kun taas
esimerkissä \ref{ee_myoharem} viittauspisteen tarkka sijainti ei ole edes puhujan
tiedossa.

\todo[inline]{ Lisää settiä ajatellen elimme silloin vs. silloin hän..}

#### Deiktiset ajanilmaukset

Referentiaalisista ajanilmauksista voidaan erottaa omaksi ryhmäkseen ilmaukset,
joiden viittauskohde määritellään suhteessa puhehetkeen. Näitä ajanilmauksia
on perinteisesti nimitetty *deiktisiksi* [vrt. deiktisyyden määritelmät Fillmorella
-@fillmore1975santa, 256].[^shoredeikt] Deiktisiä ajanilmauksia ovat

1. monet puhehetkeen liittyvät adverbit kuten *nyt, kohta,
äsken, недавно, скоро*

2. puhehetkeä lähellä olevia päiviä tarkoittavat *eilen,
tänään, huomenna, toissapäivänä, ylihuomenna.*

3. päivän osien ja viikonpäivien viittauskohteet, jotka ovat
deiktisiä usein, mutteivät aina.  Esimerkiksi lauseessa
*aamulla minulla oli huono olo* ainoastaan
suhteuttaminen puhehetkeen tekee mahdolliseksi
aamulla-sanan referentin identifioimisen juuri tietyksi
(puhehetkellä käynnissä olevan päivän) aamuksi.

4. muut puhehetkeen suhteutetut kalendaariset tai aikaa
kvantifioivat ilmaukset kuten *через два дня*, *kaksi
vuotta sitten*, *в прошлое воскресенье* sekä *hetki
sitten*.

[^shoredeikt]: Deiktiset ilmaukset kattavat paitsi ajan, myös paikan ilmauksia
[ks. esim. @tobecited], ja tämän vuoksi myös adjektiivi *aikadeiktinen*
esiintyy tutkimuskirjallisuudessa [ks. ainakin @shore2008, 31].



#### Ei-deiktiset referentiaaliset ajanilmaukset

Jos referentiaalinen ilmaus ei ole deiktinen, se viittaa, kuten edellä
määriteltiin, joko johonkin yleisesti tunnistettavaan historialliseen
tapahtumaan (*Rooman palon aikana*) tai johonkin subjektiivisen aikajanan
pisteeseen (*в молодости*, *herättyäni*). Viittauskohteet voivat lisäksi olla
tavalla tai toisella edeltävässä diskurssissa muodostettuja, 
kuten esimerkissä (\ref{ee_refTT}):




\ex.\label{ee_refTT} \exfont \small Muistan sen päivän hyvin. Aamulla minulla oli huono olo... Sitten se
onneksi helpotti.





\vspace{0.4cm} 

\noindent
Lause *muistan sen päivän hyvin* asettaa ajan, josta puheenvuoron seuraavat
lauseet kertovat. Wolfgang Kleinin termein [-@klein1999, 5, 39] tätä aikaa
voitaisiin nimittää "ajalliseksi topiikiksi" (TT, topic time)[^bondhuom]. Puhehetken
(jäljempänä myös Kleinia mukaillen UT, utterance time)ja
erilaisten eksplisiittisesti nimettyjen subjektiivisen tai objektiivisen
aikajanan pisteiden lisäksi TT tarjoaa kolmannen tavan ankkuroida
ajanilmaukselle referentti. Sellaisissa ilmauksissa kuin *toisen maailmansodan
jälkeen* tai *äsken* TT asetetaan ikään kuin automaattisesti, mutta -- kuten
esimerkistä \ref{ee_refTT} havaitaan -- se voi syntyä myös implisiittisesti edeltävän
diskurssin tuloksena. Näin tapahtuu erityisen usein juuri positionaalisten
ajanilmausten yhteydessä.

[^bondhuom]: Ks. mysö Bondarko 2005:260

Ajallisen topiikin käsite auttaa myös määrittämään tiettyjen F-funktiossa
toimivien ajanilmausten sijaintia aikajanalla. Tätä valottaa seuraava esimerkki:




\ex.\label{ee_mikkeli} \exfont \small Toisen maailmansodan jälkeen isoisä muutti Kuopiosta Mikkeliin. Siellä
hänen elämänsä asettui raiteilleen. \emph{Aamuisin} hän käveli
kirjastoon lukemaan lehtiä. \emph{Joka sunnuntai} hän kävi
tuomiokirkossa jumalanpalveluksessa.





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_mikkeli} ajanilmausten *aamuisin* ja *joka sunnuntai* voi nähdä
viittaavan tiettyyn joukkoon aamuja ja sunnuntaita. Joukko on selvästi rajattu:
esitetyt väitteet koskevat vain sitä ajanjaksoa, jonka isoisä vietti Mikkelissä,
mahdollisesti vielä tarkemmin tuon ajanjakson alkuvaiheita. Tämä ajanjakso on
esimerkin \ref{ee_mikkeli} jälkimmäisten virkkeiden TT[^histvert]. Tässä mielessä
ajanilmaukset *aamuisin* ja *joka sunnuntai* ovat myös referentiaalisia. 

[^histvert]: Vertaa myös @le2003time historiallisten tekstien ajallisten
topiikkien rakentumisesta (tarkemmin luvussa x)

TT:n käsitteeseen läheisesti liittyvä muttei identtinen ilmiö on referenttien
anaforisuus. Tyypillisesti esimerkiksi sellaisten ajanilmausten kuin *sitten*,
*sittemmin*, *после того*, *тогда* referenttinä on piste tai jakso aikajanalla,
joka ankkuroidaan suhteessa jonkin edellä mainitun ajanilmauksen referenttiin.
Fillmore [-@fillmore1975santa, 289] käyttää tähän liittyen termiä
*diskurssideixis* ja soveltaa sitä myös kirjoitettuun tekstiin kuvaamaan
esimerkiksi sanojen *aiemmin* ja *edellisessä kappaleessa* referenttejä.


Referentiaaliset ajanilmaukset voidaan siis jakaa viiteen kategoriaan sen
perusteella, miten niiden viitepiste on ankkuroitu viestijöiden kognitiossa:

1. Ankkurointi suhteessa puhehetkeen (UT)
2. Ankkurointi absoluuttiselle aikajanalle 
3. Ankkurointi subjektiiviselle aikajanalle 
4. Ankkurointi suhteessa ajalliseen topiikkiin (TT)
5. Anaforinen ankkurointi 


Ajanilmaukset, konstruktiot ja informaatiorakenne {#aimkonstr}
==============================




Edellä osiossa \ref{taksn2} mainittiin Johanna Viimarannan väitöstutkimus,
jossa selvitettiin semanttiselta kannalta sitä, *minkälaisia asioita*
ajanilmauksilla viestitään. Viimarannan tutkimuksessa tarkasteltiin sitä,
minkälaisin kielenaineksin suomalaiset ja venäläiset puhujat ilmaisevat aikaa
ja minkälaisiin viestintätarpeisiin temporaaliset käsitteet suomessa ja
venäjässä liittyvät [@viimaranta2006talking, 321]. Tuloksena oli, että tarpeet
ja keinot ovat yllättävänkin samanlaisia. Kuten johdantoluvussa totesin, tässä
tutkimuksessa luotava näkökulma ajanilmauksiin on ennemmin syntaktinen kuin
semanttinen: ratkaisevana pyrkimyksenä on tutkia, minkä takia ajanilmaukset
sijaitsevat juuri tietyssä kohden lausetta. 

Niin suomen kuin venäjän sanajärjestystä on perinteisesti pidetty "vapaana"
siinä mielessä, että lauseen konstituenteilla on huomattava määrä eri
variaatioita, jotka eivät vaikuta sen *kieliopillisuuteen* [vrt. @puskas2000,
41]. Generatiivisessa perinteessä suomen ja venäjän kaltaisista kielistä
voidaan käyttää termiä *discourse configurational*, joka Kissin [-@kiss1995, 5]
mukaan merkitsee ennen kaikkea sitä, etteivät lauseen \todo[inline]{virkkeen?}
tärkeimpiä konstituentteja ole syntaktisesti määritellyt subjekti ja
predikaatti vaan informaation järjestämiseen liittyvät *topiikki* ja *fokus*. 
Topiikin ja fokuksen määritelmiin palataan alempana, osiossa
\ref{informaatiorakenne}, mutta tässä kohtaa on oleellista kiinnittää huomiota
siihen, että suomessa ja venäjässä sanajärjestys on prosodian[^prosodia] ohella toinen
niistä keinosta, joiden avulla puhuja tai kirjoittaja määrittelee, mikä rooli
jollakin lauseen osalla on informaation välittämisessä. Tästä seuraa, että
etsittäessä syitä ajanilmauksen sijaintiin on ennen kaikkea vastattava
kysymykseen siitä, minkälaisia erilaisia tehtäviä ajanilmauksilla voi
olla viestinnässä ja viestintään liittyvässä informaationkulussa.

[^prosodia]: Kun puhutaan sanajärjestyksestä ja prosodiasta informaation välittämisen
keinoina, on paikallaan muistuttaa, että nyt käsillä olevan tutkimuksen
varsinaisena aineistona -- ja siten myös tutkimuskohteena -- on nimenomaan
kirjoitettu kieli. Olen kuitenkin eri mieltä kuin esimerkiksi Susanna Shore
[-@shore2008] siitä, ettei kirjoitettuja tekstejä tutkittaessa olisi tarpeen
tehdä oletuksia kirjoittajan ja vastaanottajan tiedontiloista ja siten tulkita
tekstin lukemista yhtenä keskustelun muotona. Oma käsitykseni on lähempänä
Yokoyaman [-@yokoyama1986, 144] ajatusta, jonka mukaan kirjoitetun tekstin
lukeminen on kirjoittajan ja lukijan välinen viestintätilanne samassa mielessä
kuin tavallinen keskustelu on kahden suullisesti ja reaaliaikaisesti viestivän
osallistujan välinen viestintätilanne (vertaa myös Shoren itsensä mainitsema
@halliday2013, 59--90). Janko [-@janko2001, 14] puolestaan
toteaa, että ääneen luettaessa lukija osallistuu merkityksen rakentamiseen
yhdessä tekijän kanssa yrittäessään kuvitella kirjoittajan tarkoittamat
prosodiset piirteet. Itse olen valmis menemään askeleen pidemmälle ja
esittämään, että vaikka tekstiä ei ääneen luettaisikaan, sille on *kuviteltava*
tietyt prosodiset piirteet -- voi hyvin olla, että eri lukijat (kirjoittaja itse
mukaan lukien) kuvittelevat eri tavoin, mutta yhtä kaikki tekstin merkityksen
rakentamiseksi sitä ei voi lukea ikään kuin kirjallisessa tyhjiössä, vailla
minkäänlaista, edes kuviteltua, prosodiaa. 

\todo[inline]{TARKISTA hallidayn painoksesta, että sama kohta kuin Shorella}

Tutkiessani sitä, minkälaisia tehtäviä ajanilmauksilla on viestinnässä (ja sitä
kautta syitä ajanilmausten eri sijainteihin), lähestymistapani on tarkastella
ajanilmauksia osana *konstruktioita*, joilla toteutetaan erilaisia
viestinnällisiä ja tiedonvälityksellisiä tavoitteita.  Konstruktioiden sisällä
ajanilmausten ja muiden elementtien sijaintia säätelee ennen kaikkea
*informaatiorakenne*, jonka piiriin edellä mainitut topiikin ja fokuksen
käsitteetkin kuuluvat. Paitsi yksittäisten konstruktioiden informaatiorakenteesta, usein
on mielekästä puhua myös kokonaisen diskurssin tai tekstin informaatiorakenteesta, jolloin 
on perinteisesti käytetty esimerkiksi *tekstistrategian* [
@virtanen1992discourse, 42] tai systeemis-funktionaalisessa
paradigmassa *teemankulun* [@shore2008] käsitteitä. Esittelen seuraavassa tarkemmin
ensin konstruktion käsitteen ja tämän jälkeen informaatiorakenteeseen liittyvät
*diskurssistatuksen* ja *diskurssifunktioiden* käsitteet.[^konstr_tekst]

[^konstr_tekst]: Huomautus tekstistä konstruktiona?

Ajanilmaukset osana konstruktioita {#vsvtk}
----------------------------------------------

Aloitan konstruktion käsitteen tarkastelemisen tutkimalla seuraavia 
ajanilmauksen sisältäviä lauseita:




\ex.\label{ee_pros1} \exfont \small Kokous kestää \emph{tunnin}.






\ex.\label{ee_syn1} \exfont \small Папа \emph{часто} играет в бадминтон.






\ex.\label{ee_oikeustiede} \exfont \small Eräs tuttavani pääsi \emph{viime vuonna} opiskelemaan oikeustiedettä.





\vspace{0.4cm} 

\noindent
Luvussa \ref{ajanilmausten-kolme-taksonomiaa} esitettyjen taksonomioiden perusteella
voidaan todeta, että esimerkit \ref{ee_pros1} -- \ref{ee_oikeustiede} edustavat kukin
yhtä ajanilmausten semanttisten funktioiden pääluokkaa, esimerkki \ref{ee_pros1}
E-, esimerkki \ref{ee_syn1} F- ja esimerkki \ref{ee_oikeustiede} L-funktiota. Lauseita
voisi hyvin verrata esimerkkeihin \ref{ee_pros2} -- \ref{ee_tiusanen}:




\ex.\label{ee_pros2} \exfont \small Sessio jatkuu \emph{puoli kolmeen}.






\ex.\label{ee_syn2} \exfont \small Я \emph{редко} смотрю телевизор.






\ex.\label{ee_tiusanen} \exfont \small Kunnallisvaltuuston puheenjohtaja Tiusanen kertoi \emph{eilen} uusista
leikkauksista.





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_pros1} on selvästikin jotain samaa kuin esimerkissä \ref{ee_pros2}
Samoin esimerkit \ref{ee_syn1} ja \ref{ee_syn2} sekä
\ref{ee_oikeustiede} ja \ref{ee_tiusanen} muistuttavat toisiaan. Voisi ajatella, että
mainitut esimerkkiparit ovat ikään kuin samalla muotilla tuotettuja yksilöitä,
samanlaisessa merkitsijä--merkitty-suhteessa kuin vaikkapa sananmuodot
*kirjoitan* ja *kirjoitat* (jotka merkitsevät samaa lekseemiä) tai morfit /ом/
ja /ой/ (jotka merkitsevät samaa morfeemia). 

Edellä mainitussa kahtiajaossa on tietysti kyse kielitieteen kannalta
perustavanlaatuisesta, tavallisesti Ferdinand de Saussureen jäljitettävästä
ajattelutavasta, jonka mukaan kieli jakautuu lukuisiin
merkitsijä/merkitty-pareihin kuten allofoni--foneemi, morfi--morfeemi tai
sane--sana [ks. esim. @mccabe2011, 6]. Yksi *konstruktiokieliopin* [esim. @fillmore1988regularity,
@kay1999, @goldberg1995constructions, @goldberg1995constructions, @hoffmann2013oxford]
keskeisimpiä ajatuksia onkin, että
samaa jaottelua voi laajentaa kuvaamaan myös esimerkkien \ref{ee_pros1} -- \ref{ee_syn2}
kaltaisia tapauksia, jolloin kunkin yllä esitetyn esimerkkiparin edustamana
merkittynä on kussakin tapauksessa oma  *konstruktionsa*. Itse asiassa 
myös kategoriat, joihin tavallisesti on viitattu sellaisilla käsitteillä kuin *lekseemi*,
tai *morfeemi*, ovat lopulta samassa mielessä konstruktioita kuin esimerkkien \ref{ee_pros1}
ja \ref{ee_pros2} taustalla oleva rakenne: molemmat kuvaavat kielenkäyttäjän
muistiin on tallentuneita kielen yksiköitä, eikä konstruktiokieliopin piirissä
jaottelu esimerkiksi sanoihin ja lauseisiin ole oleellinen [@ostman2004, 12].

Konstruktioajattelua voidaan siis pitää pohjimmiltaan melko radikaalinakin
irtautumisena koko 1900-luvun loppupuolta hallinneesta generatiivisesta
ajattelutavasta, jota John Taylor [-@taylor2012, 42] luonnehtii karrikoiden
*sanakirja + kielioppi* -malliksi: generatiivisessa (chomskylaisessa)
perinteessä kieli nähdään koostuvan toisaalta joukosta opittuja sanoja sekä
toisaalta joukosta sääntöjä, joiden perusteella sanoista voidaan muodostaa
rajaton määrä erilaisia lauseita. Kontrastina tähän konstruktiokielioppi lähtee
ajatuksesta, jonka mukaan kieli on konstruktioiden muodostama verkko
("konstruktikko"[^tikko]), tallentunut käyttäjän muistiin joukkona toisiinsa
linkittyneitä itsenäisiä yksiköitä. 

\todo[inline]{tosin adjunktiasia, ks. boas 2010 ym.}

[^tikko]: Englanninkielisessä kirjallisuudessa termi *constructicon* on
yleisessä käytössä [ks. esim. @hilpert2014], mutta suomesta vastaava termi vaikuttaisi koko
lailla puuttuvan -- ainoa löytämäni maininta on Petri Jääskeläisen väitöskirja
[-@jaaskelainen2004instrumentatiivisuus, 16].

Kun edellä kuvattua ajatusta soveltaa kysymykseen ajanilmauksen sijainnista,
tämän tutkimuksen kohteeksi tarkentuu sen selvittäminen, minkälaisia
ajanilmauksen sisältäviä konstruktiota -- vakiintuneita malleja tai skeemoja --
suomen- ja venäjänkielisillä viestijöillä mahdollisesti on käytössään. Jaakko
Leino [-@leino2003, 72] huomauttaa, ettei edellä kuvattu ajattelutapa ole
fennistisessä perinteessä välttämättä erityisen vieras tai uusi, vaan
esimerkiksi *ilmaustyypin* ja jopa *lausetyypin* [esim. @hakulinenkarlsson1979]
käsitteet ovat pohjimmiltaan sukua ajatukselle konstruktioista. Eräässä
mielessä tässä tutkimuksessa tarkasteltavia *ajanilmauskonstruktioita* voisikin
varovasti ajatella perinteisemmän fennistisen terminologian mukaisesti omina,
erillisinä lausetyyppeinään siinä missä eksistentiaali- tai kokijalauseetkin.

Konstruktioiden esittämiseen on tavallisesti käytetty niin kutsuttuja
attribuutti--arvo-matriiseja, joissa niin konstruktioon kuuluvien konstituenttien
kuin konstruktioiden itsensä ominaisuuksia kuvataan ikään kuin muuttujina, jotka
saavat erilaisia arvoja [vrt. @leino2003, 64].
Matriisit ovat perusrakenteeltaan laatikkokuvioita, joissa vierekkäisten
ja sisäkkäisten laatikoiden avulla pystytään kuvaamaan konstituenttien välisiä riippuvuussuhteita ja
järjestystä [@ostman2004, 25]. Kussakin laatikossa kuvattavan informaation 
luonne ja määrä voi vaihdella paljonkin ja on luonnollisesti sovellettavissa sen mukaan,
mikä kulloinkin on tutkijan tarkkailun kohteena [@leino2003, 79].
Koska tässä tutkimuksessa tarkasteltavien erilaisten syntaktisten rakenteiden 
variaatio on pitkälti teknisistä ja aineistonhankinnallisista syistä
rajattu minimiin (ks. osio \ref{taks4}), suurin osa esittämistäni
attribuutti-arvo-matriiseista muistuttaa seuraavaa yksinkertaista esimerkkiä:


\noindent



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & act+ \\
df & top \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


funct freq \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{3.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 1}: Esimerkki attribuutti--arvo-matriisista \normalfont \vspace{0.6cm}

\todo[inline]{huom: tässähän ei aina ole objekteista kyse }

Matriisi 1 voisi olla kuvaus esimerkkien \ref{ee_syn1} ja
\ref{ee_syn2} ilmentämästä konstruktiosta, jossa ajanilmaus sijaitsee subjektin ja
verbin välissä. Matriisi koostuu ulommasta laatikosta ja tämän sisällä olevasta
neljästä vierekkäisestä laatikosta, joista kukin kuvaa esimerkkien \ref{ee_syn1} ja
\ref{ee_syn2} edustaman konstruktion konstituentteja. Sisemmillä laatikoilla on
vaihteleva määrä attribuutteja -- tarkoituksenani on
pyrkiä välttämään redundanssia, niin että vain arvot, jotka ovat kulloinkin
tarkasteltavan konstruktion kannalta relevantteja, ilmoitetaan. Esimerkiksi
viimeiseen laatikkoon on kirjattu ainoastaan *gf*-attribuutti arvolla *compl*,
joka kertoo, että kyseinen konstituentti on verbin täydennys; tätä edeltävässä laatikossa
esitetty lauseen verbi on puolestaan merkitty kirjaamalla *cat*-attribuutin
(leksikaalinen kategoria) arvoksi V, muttei tarkempia, esimerkiksi
semantiikasta kertovia ominaisuuksia. Sen sijaan semanttisia ominaisuuksia on
kirjattu ajanilmaukselle, joka lisäksi sisältää semanttisen funktion
ilmaisevan *funct*-attribuutin. Vielä on paikallaan lisätä, että attribuutit
voidaan ryhmitellä kimpuiksi, kuten ensimmäisessä laatikossa, jossa
*prag*-attribuutti koostuu useammasta piirteestä. Nämä piirteet kuvaavat
konstituenttiin liittyviä pragmaattisia ominaisuuksia, ennen muuta seuraavassa
alaluvussa käsiteltävää informaatiorakennetta. 

Palaan konstruktioiden käyttämiseen nimenomaan erilaisten sanajärjestyksien
kuvaajina ja toisaalta kontrastiivisen tutkimuksen välineenä sekä aineistona
olevien lauseiden suppeaan syntaktiseen variaatioon tarkemmin tutkimuksen
myöhemmissä luvuissa, joissa täydennetään matriisissa
1 esitettyä konstruktionotaatiota. Tässä vaiheessa on syytä kuitenkin 
sivuta vielä kysymystä konstruktioiden välisistä linkeistä.

Kuten edellä todettiin, konstruktiot ovat monin tavoin sidoksissa toisiinsa,
niin ettei kieltä nähdä vain listana erilaisia kielenkäytön skeemoja, vaan
erilaisia linkkejä vilisevänä verkkona. 
Keskeisin konstruktioita yhdistävä konsepti on
*periminen* (inheritance): konstruktioiden voi nähdä edustavan eri
abstraktiotasoja, niin että spesifimmät, alemman tason konstruktiot perivät
piirteitä yleisluontoisemmilta ylemmän tason konstruktioilta [@hilpert2014, 66].
Esimerkiksi matriisissa 1 kuvatun konstruktion 
voisi ajatella perivän ominaisuuksia kaikkein yleisimmällä tasolla *verbi +
täydennys* -konstruktiosta. 
Konstruktiokieliopin piirissä on perinteisesti listattu ennen kaikkea
Goldbergin [-@goldberg1995constructions] tekemään jaotteluun nojaten neljä
erilaista perintälinkkiä: toteutumalinkki, polysemialinkki, metaforisen
laajentuman linkki ja osarakennelinkki [suomenkielisistä nimistä ks.
@leino2003, 83]. Tämän tutkimuksen kannalta erilaisten linkkien tarkempi
erottelu ei kuitenkaan ole tarkoituksenmukaista, vaan olennaista on itse  ajatus ominaisuuksien 
perimisestä ja jakamisesta perivien konstruktioiden kesken.

\todo[inline]{Tieto siitä, miten vaihtoehtoiset sijainnit koodataan, vasta myöhemmin
("progressive revelation") 
ajattele myös sitä, että linkkien kuvaaminen voisi ratkaista järjestysongelmaa...
}


\todo[inline]{
    Konstruktiot ja linkit: Selitä, että totta kai jokainen kompleksisempi syntaktinen
    konstruktio koostuu subpart-linkeillä yhdistellyistä pienemmistä 
    osasista (Hilpert 71), mutta tämän tutkimuksen tarkoitusten kannalta ei ole syytä
    keskittyä kuvaamaan niitä

}


Ajanilmaukset ja informaatiorakenne {#informaatiorakenne}
-------------------

Informaatiorakenteen tutkimisessa on nimensä mukaisesti kyse sen
tarkastelemisesta, miten tekstissä välitetään tietoa. Tätä tiedon jäsentämistä
on pidetty morfologian ja syntaksin ohella kolmantena luonnollisen kielen
peruskomponenttina [@lambrecht1996, ??]. Informaatiorakenteen tutkimuksen
kannalta merkittäviä ja etenkin slavistisessa perinteessä käytettyjä ovat niin
sanotun Prahan koulukunnan tutkimukset ([@tobecited] Danes, Firbas, Matesius),
joiden perua ovat muun muassa klassiset käsitteet teema ja reema. \todo[inline]{Princen
maininta?} Kuten
esimerkiksi Leino [-@jleino2013, 319] toteaa, informaatiorakenne on
kuitenkin ollut merkittävä tutkimuskohde myös konstruktiokieliopin piirissä,
missä yhteydessä käsiteparin teema--reema sijasta on tavattu käyttää termejä
topiikki ja fokus.

Yksinkertaistetusti voidaan sanoa, että informaatiorakenteessa oleellista on sen
erittely, mikä tieto on vanhaa tai annettua ja mikä uutta. Jo varhain
informaatiorakenteen tutkijat ovat kuitenkin tunnistaneet, etteivät uutuus ja
annettuus sinänsä ole riittävän tarkkoja käsitteitä kuvaavaan erityyppistä
informaatiota. Esimerkiksi Jeanette Gundel [-@gundel1999topic, 4], toteaakin,
että jokin lauseen elementti voi olla uusi, vanha tai jotain siltä väliltä
ainakin kahdella tasolla: ensinnäkin, jonkin konstituentin -- kuten pronominin
*hän* tai lausekkeen *eräs tamperelainen nainen* -- viittauskohde voi olla
viestijöille tuttu tai tuntematon sekä lisäksi joko aktiivisen huomion kohteena
tai syvemmällä tajunnassa; toiseksi, jokin tämän viittauskohteen ominaisuus tai
viittauskohteeseen liittyvä tapahtuma voi olla viestijöiden tiedossa tai heille
tuntematon. Nimitän tässä ensimmäisenkaltaista informaatiota *diskurssistatukseksi*
ja jälkimmäistä *diskurssifunktioiksi*.[^vilkunadf] Jälkimmäinen termi on
kuitenkin tässä tutkimuksessa myös laajemmassa käytössä kuin ainoastaan 
määrittelemässä tiedossa olemista tai olemattomuutta (vrt. osio \ref{muut-df}).

[^vilkunadf]: Kattavana katsauksena informaatiorakenteen värikkääseen
terminologiaan ks. esim. [@kruijff2003discourse, 254]


### Diskurssistatus

Edellä mainittiin ohimennen ajatus siitä, ettei kirjoitettua tekstiä
tutkittaessa tarvitsisi tehdä oletuksia viestijöiden (Shoren termein)
*tiedontiloista*. Esitin, että kirjoitettua tekstiä kuitenkin voidaan hyvinkin
käsitellä ilmiönä, joka muistuttaa keskustelua: toisin sanoen tilanteena, jossa
viestijä viestii jotakin viestin vastaanottajalle. Oma näkemykseni on, että
viestin rakentumiseen todella vaikuttaa se, minkälaisia oletuksia kirjoittaja
tekee lukijan tiedontiloista. Tämä on erityisen ilmeistä Internet-viestinnässä,
jota suuri osa tämän tutkimuksen aineistoa edustaa [@tobecited]. Kaiken
kaikkaan on kuitenkin korostettava, että vaikka pidän perusasetelmaa (viestijä
ja viestin vastaanottaja) puhutussa ja kirjoitetussa diskurssissa samanlaisena,
en väitä, etteikö puhutun ja kirjoitetun viestintätilanteen välillä olisi myös
merkittäviä eroja.

Olga Yokoyama [-@yokoyama1986] on vienyt ajatuksen tiedontilojen tulkitsemisesta
erityisen pitkälle esittelemällä *tietojoukon* (*set of knowledge* /
*когнитивное множество*)[^yokkaannos] käsitteen.  Yokoyaman mukaan (mts. 3)
jokainen viestintätilanne vaatii vähintään neljä eri komponenttia: viestijät A
ja B sekä joukot, jotka kuvaavat viestijöiden *tämänhetkisen huomion kohdetta*
(*matter of current concern*, *ven. käännös*): $C_a$ ja $C_b$. A ja B voidaan
määrittää tarkemmin tarkoittamaan ei viestijöitä itseään vaan heidän hallussaan
pitämää tietomäärää ylipäänsä. Jotta viestintä ylipäätään olisi mahdollista,
joukkojen A ja B on *leikattava toisensa*: on oltava tiettyjä asioita
(propositioita, referentiaalista tietoa muun muassa eri henkilöistä, yhteinen kieli), jotka
ovat sekä A:n että B:n tiedossa. Tätä leikkauskohtaa, *intersektiota*, voidaan
merkitä joukko-opin notaatiota lainaten $A \cap B$.

[^yokkaannos]: Yokoyaman teos *Discourse and word order* ilmestyi
venäjänkielisenä käännöksenä nimellä xxx vuonna yyy.

$A \cap B$-intersektiota oleellisempi on kuitenkin intersektio $C_a \cap C_b$
eli kohta, jossa tämänhetkisen huomion kohteena olevat asiat leikkaavat.
Tavallisesti tähän joukkoon kuuluvat aina vähintään deiktiset elementit (ks.
osio \ref{x}) eli esimerkiksi keskustelun osapuolet (minä, sinä) ja -- tämän
tutkimuksen kannalta oleellisesti -- nykyhetki ja siihen viittaavat ilmaukset
kuten *nyt*, *сейчас* ja *скоро*. Yokoyama esittää, että viestijät tekevät
jatkuvasti oletuksia siitä, missä tietojoukossa jokin elementti sijaitsee, ja
erityisesti venäjän (kuten myös suomen) kaltaisissa kielissä nämä oletukset
ilmaistaan sanajärjestyksen avulla. Hän antaa (mts. 218) muun muassa seuraavan
yksinkertaisen esimerkin venäjästä (Yokoyamalla numero 30):





\exg.  Я сейчас уезжаю в Бостон
\label{ee_yokboston}  
 minä nyt lähteä PREP Boston-AKK
\




\vspace{0.4cm} 

\noindent
Yokoyaman analyysin mukaan esimerkissä \ref{ee_yokboston} *Я* ja *сейчас* edustavat
tietoa, joka on kummankin viestijän tämänhetkisen huomion kohteena eli osa sekä
$C_a$- että $C_b$ joukkoa ja sitä kautta $C_a \cap C_b$-intersektiota. *Boston*
puolestaan on osa viestin lähettäjän tämänhetkistä huomiota. Mitä tulee viestin
vastaanottajaan, lähettäjä olettaa että a) *Boston* ei ole viestin lähettämisen
hetkellä vastaanottajan huomion kohteena  ja b) vastaanottaja kuitenkin
ylipäätään tuntee sanan *Boston* viittauskohteen, eli se on osa B-joukkoa,
joskaan ei $C_b$-joukkoa -- tarkemmin ilmaistuna Boston-sanan referentti kuuluu 
joukkoon $C_a \cap (B-C_b)$. 

Yokoyaman mukaan esimerkin \ref{ee_yokboston} kaltaisissa tapauksissa
(konstruktioissa) venäjänkielinen viesti rakennetaan yksinkertaistetusti siten,
että $C_a \cap C_b$-joukkoon kuuluva informaatio sijoitetaan ennen verbiä ja
$C_a \cap (B-C_b)$-joukkoon kuuluva verbin jälkeen. Toisin sanoen, sijoittamalla
ilmauksen tiettyyn kohtaan lausetta puhuja tekee "oletuksen vastaanottajan
tiedontilasta" eli siitä, kuuluuko elementti ylipäätään vastaanottajan hallussa
pitämään tietoon ja siitä, kuinka lähellä vastaanottajan tajunnan pintaa
elementti sijaitsee.

Vaikka Yokoyaman tietojoukkoteoria ei ole erityisen käytetty ja se on ainakin
slavistipiireissä saanut kritiikkiäkin (Anton Zimmerling, henkilökohtainen
tiedonanto), on sen keskeinen ajatus tajunnan eri tasoista ja tiedontiloista
tehtävistä oletuksista kiinnostavan lähellä sitä, mitä monet laajemmin tunnetut
pragmaatikot ovat esittäneet. Vastaavia ajatuksia voidaan havaita esimerkiksi
Knud Lambrechtilla, joka Leinon [-@jleino2013, 319] mukaan on
konstruktiokieliopin piirissä eniten informaatiorakennetta käsitellyt tutkija.

Lambrechtilla idea jonkin tiedon sijaitsemisesta joko viestijän hallussa
ylipäätään tai nykyisen huomion kohteena olevaksi tiedoksi tarkentuu ajatukseksi siitä, kuinka
lähellä tajunnan pintaa jokin tieto on ja kuinka mahdollista tieto on toisaalta
tunnistaa, toisaalta tuoda lähemmäs pintatasoa. Tämä taas ilmenee
konkreettisesti *tunnistettavuuden* (identifiability) ja *aktivoituneisuuden*
(activation) käsitteissä, jotka Lambrecht on edelleen kehittänyt Chafen 
[-@chafe1976; -@chafe1987; esim. -@chafe1994discourse] pohjalta.

Tunnistettavissa oleva tieto -- tai tarkemmin jokin viittauskohde, (diskurssi)referentti --
on Lambrechtin [-@lambrecht1996, 87] ja Chafen [-@chafe1976, 39] mukaan
sellainen, josta virkkeen lausumishetkellä on olemassa representaatio niin
lausujan kuin vastaanottajankin mielessä. Kuulija pystyy oletettavasti
tunnistamaan (identifioimaan) jonkin ilmauksen referentin silloin, kun
keskustelijoiden jakamassa keskusteluavaruudessa (Yokoyaman termein
tietojoukossa $A \cap B$) on jokin yksittäinen elementti, johon ilmaus voi
viitata. Jos elementti on tunnistettavissa, voidaan kysyä, kuinka lähellä
viestijöiden tajunnan pintaa se sijaitsee, jolloin puhutaan aktivoituneisuudesta
[@lambrecht1996, 93--94]. Jos elementti on hyvin lähellä tajunnan pintaa (vertaa
Yokoyaman $C_a$- ja $C_b$-joukot), sen on Lambrechtin (mts. 76, 165) termein
*aktivoitu*; hieman kauempana tajunnan pinnasta olevia elementtejä voidaan nimittää
*käytettävissä oleviksi* ja tunnistettavissa olevia mutta kaukana tajunnan
pinnasta sijaitsevia *käyttämättömiksi*.

Nyt esiteltyjen käsitteiden avulla *diskurssistatus* voidaan alustavasti
määritellä seuraavasti:\linebreak

\noindent 

\noindent 
\noindent \headingfont \small \textbf{Määritelmä 4}: Jotta elementillä ylipäätään voi olla diskurssistatus,
    elementin on oltava referentiaalinen eli sillä on oltava viittauspiste. Jos
    elementillä on viittauspiste, voidaan kysyä, onko viittauspiste sellainen,
    jonka kaikki viestintätilanteen osapuolet voivat tunnistaa. Jos elementti
    voidaan tunnistaa, se on joko käyttämätön, käytettävissä oleva tai
    aktiivinen. Jos taas elementti ei kuulu viestijöiden jakamaan tietoon, se
    on -- Princen [-@prince1981, ??] termein -- täysin uusi (brand new).\linebreak \normalfont \vspace{0.6cm} 

Oman tutkimukseni kannalta on kuitenkin useimmiten riittävä yksinkertaistaa määritelmässä
4 esitettyä käsiteverkkoa, ja keskittyä kahteen olennaiseen
erotteluun: toisaalta siihen, onko jonkin tekstissä esiintyvän kielellisen yksikön
viittauskohde eli diskurssireferentti esitetty täysin uutena vai
tunnistettavana ja toisaalta siihen, onko tunnistettavana esitetty
viittauspiste esitetty aktiivisena vai ei. Käytän ajanilmauskonstruktioita kuvatessani
tähän liittyen merkintää *ds ident+* kuvaamaan niitä referenttejä, joiden
on kuvattavassa konstruktiossa tultava esitetyiksi tunnistettavissa olevina sekä
merkintää *ds act+* kuvaamaan niitä referenttejä, joiden on tultava esitetyiksi
aktiivisina. Selvennän näitä käsitteitä seuraavassa esimerkkien \ref{ee_eduskunta_teoria} -- \ref{ee_libya_teoria}
avulla.




\ex.\label{ee_eduskunta_teoria} \exfont \small Eduskunta käsittelee keskiviikkona täysistunnossaan Libyan tilannetta.





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_eduskunta_teoria} on keksitty suomenkielinen virke, jota käsitellään
tarkemmin varsinaisen tutkimusaineiston analyysin yhteydessä luvussa \ref{s1-sim-pos} ja
joka on kuviteltu kirjoitetuksi osana uutistekstiä keväällä 2011.
Esimerkistä voidaan ensinnäkin erotella, että siinä on kaksi kielellistä elementtiä,
joille on helppo määrittää viittauskohde -- toisin sanoen kaksi selkeän referentiaalista 
elementtiä. Näitä ovat toisaalta *eduskunta* (jonka referenttiä, Suomen 
200-jäsenistä parlamenttia, merkitsen  *r1*), toisaalta *Libyan
tilannetta* (jonka referenttiä, Libyassa helmikuussa 2011 alkanutta aseellista
konfliktia, merkitsen *r2*). 

Esimerkissä \ref{ee_eduskunta_teoria} ei ole ainuttakaan täysin uutena lukijalle esiteltävää
($A - B$ -tietojokkoon kuuluvaa) elementtiä. Sekä r1 että r2 ovat sen sijaan tunnistettavissa.
Jos esimerkin \ref{ee_eduskunta_teoria} esittäisi osiossa \ref{vsvtk} esiteltyjen
attribuutti--arvo-matriisien avulla, notaatio olisi tässä tutkimuksessa
seuraavanlainen:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & ident+ \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem paikka \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]



prag \[ ds & ident+ \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 2}: Esimerkki \ref{ee_eduskunta_teoria} attribuutti--arvo-matriisina \normalfont \vspace{0.6cm}

Matriisissa 2 lauseen subjektin ja objektin
diskurssistatuksiksi on merkitty yllä kuvatun konvention mukaisesti *ds ident+*.
Huomattakoon, että myös kahden adverbiaalin, *keskiviikkona* ja *täysistunnossa*
voi ajatella viittaavan konkreettiseen, tunnistettavissa olevaan diskurssireferenttiin:
tapahtumaan *eduskunnan keskiviikkoinen täysistunto*. Vastaaviin adverbiaaleihin liittyvien
diskurssireferenttien ja sitä kautta diskurssistatusten määrittely on kuitenkin
usein huomattavasti hämärämpää kuin esimerkin \ref{ee_eduskunta_teoria} subjektiin ja objektiin
liittyvien viittauskohteiden, joilla luvuissa \ref{ajanilmaukset-ja-alkusijainti} --
\ref{ajanilmaukset-ja-loppusijainti} havaitaan usein olevan myös huomattavasti
enemmän selitysvoimaa pohdittaessa ajanilmauksen saamaa sijaintia. Itse ajanilmauksen
diskurssistatukseen liittyvät määrittelyongelmat tulevat esille esimerkiksi
seuraavassa, esimerkin \ref{ee_eduskunta_teoria} aloittamaa diskurssia jatkavassa
lauseessa:




\ex.\label{ee_eduskunta_teoria2} \exfont \small Se on viime aikoina kärjistynyt entisestään.





\vspace{0.4cm} 

\noindent
Ajanilmauksella *viime aikoina* voidaan nähdä referenttinä
epämääräinen jakso aikajanalta (vrt. luku \ref{viittauskohteen-tarkkuus}). Kuten luvussa
\ref{selittuxe4vuxe4t-muuttujat} todetaan, tämä ei loppujen lopuksi kuitenkaan ole ajanilmauksen sijainnista
tehtävien päätelmien kannalta erityisen selitysvoimainen ratkaisu, vaan oleellisempaa
on ylipäätään se, että kyseessä on lokalisoivaa funktiota edustava ilmaus. Tämän vuoksi
luvuissa \ref{ajanilmaukset-ja-alkusijainti} -- \ref{kokoava-kontrastiivinen-analyysi}
käsiteltävissä ajanilmauskonstruktioissa ei juurikaan oteta kantaan ajanilmausten
omaan diskurssistatukseen, vaikka sen määrittely alaluvussa \ref{taksn3} kuvatulla
teoreettisella tasolla olisikin mahdollista.

Esimerkki \ref{ee_eduskunta_teoria2} eroaa esimerkistä \ref{ee_eduskunta_teoria} myös siltä osin, että
siinä on vain yksi kielellinen elementti, jolle on mahdollista yksiselitteisesti
määrittää viittauskohde. Tämä viittauskohde on sama kuin toinen esimerkin \ref{ee_eduskunta_teoria} 
sisältämistä referenteistä, Libyassa vallitseva konfliktitilanne (r2). R2-referentin 
diskurssistatus on kuitenkin muuttunut, niin ettei se ole enää vain tunnistettavissa
vaan aktiivinen. Lambrechtin [-@lambrecht1996, ??] mukaan juuri pronominin käyttäminen
substantiivilausekkeen sijasta on kielestä riippumatta tyypillisimpiä keinoja
ilmaista jonkin diskurssireferentin aktiivisuutta. R2-referentin aktiivisuus esimerkissä
\ref{ee_eduskunta_teoria2} kuvattaisiin konstruktionotaatiossa seuraavasti:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & act+ \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


\vspace{2.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 3}: Esimerkki \ref{ee_eduskunta_teoria2} attribuutti--arvo-matriisina \normalfont \vspace{0.6cm}


Matriisissa 3 subjektin diskurssistatukseksi
on merkitty *act+*, eikä *ident+* kuten matriisissa 2.

Määritelmässä 4 mainituista diskurssistatukseen liittyvistä
käsitteistä on vielä selvittämättä, mitä tässä tutkimuksessa ymmärretään 
niillä diskurssireferenteillä, jotka eivät ole aktiivisia eivätkä edes tunnistettavia,
vaan täysin uusia. Esimerkkinä tällaisista viittauskohteista voidaan tarkastella 
vaikka seuraavaa Libyan vuoden 2011 konfliktista kertovaan Wikipedia-artikkeliin
kuuluvaa virkettä:[^libyavirke]





\ex.\label{ee_libya_teoria} \exfont \small Maan entinen oikeusministeri Mustafa Mohamed Abud Ajleil kertoi
lauantaina 26. helmikuuta, että hän pyrkii muodostamaan väliaikaisen
hallituksen.





\vspace{0.4cm} 

\noindent

Esimerkin \ref{ee_libya_teoria} subjektina oleva substantiivilauseke *Maan entinen
oikeusministeri Mustafa Mohammed Abud Ajleil* (r3) eroaa edellä käsitellyistä
esimerkkien \ref{ee_eduskunta_teoria} ja \ref{ee_eduskunta_teoria2} r1- ja
r2-referenteistä siinä, ettei se ole sen paremmin aktiivinen kuin
tunnistettavissakaan. Kuten määritelmässä 
todettiin, tällöin kyse on täysin uudesta viittauskohteesta. R3-referentistä
voidaan kuitenkin sanoa, että se esitellään lukijalle suhteuttamalla se
johonkin, minkä lukija voi tunnistaa (kyseessä on puheenaiheena olevan maan
entinen oikeusministeri). Lambrecht [-@lambrecht1996, 86, 167]
lainaa vastaavia tapauksia varten Princeltä [-@prince1981, 236] käsitteen
*brand new anchored* (täysin uusi, mutta *ankkuroitu*). Prince itse käytti alun
perin esimerkkeinä englannin lauseita *I got on a bus yesterday* ja *A guy I
work with says he knows your sister*, joista ensimmäisessä *a bus*-lausekkeen
referentin hän analysoi täysin uudeksi ja ankkuroimattomaksi ja jälkimmäisessä
*a guy I work with*-lausekkeen referentin taas täysin uudeksi mutta
ankkuroiduksi. Viittaan tässä tutkimuksessa diskurssistatukseltaan täysin
uusiin mutta ankkuroituihin diskurssireferentteihin merkinnällä *ds anch+*.

[^libyavirke]: https://fi.wikipedia.org/wiki/Libyan\_sis%C3%A4llissota#Tapahtumat, tarkistettu 5.4.2018

### Diskurssifunktio

Käytän termiä *diskurssifunktio* lähtökohtaisesti kuvaamaan
informaatiorakenteen perinteisiä ydinkäsitteitä *topiikki* ja *fokus* (teema ja
reema). Termin  yksi tarkoitus on tässä suhteuttaa toisiinsa Gundelin
[-@gundel1999topic, 125] mainitsemat informaatiorakenteen kaksi osatekijää,
joista toisen muodostavat diskurssifunktiot (*suhteellinen* merkitys Gundelin
termein), toisen edellä käsitelty diskurssistatus (*referentiaalinen* merkitys
Gundelin termein).[^termiero] Laajennan kuitenkin vielä käsitettä myös varsinaisen
informaatiorakenteen tutkimuksesta tekstintutkimuksen suuntaan, niin että
topiikin tai fokuksen lisäksi etenkin tutkimuksessa tarkasteltavien ajanilmauksilla
voi olla myös eräitä muita diskurssifunktioita, joita käsitellään alaluvussa
\ref{muut-df}. Tämä on sikäli olennainen lisäys, että ajanilmausten määritteleminen
juuri topiikiksi tai fokukseksi ei useinkaan ole yksiselitteistä, ja juuri 
topiikista ja fokuksesta poikkeavat diskurssifunktiot ovatkin ajanilmauksen 
sijaintia mietittäessä selitysvoimaisempia.


[^termiero]: Huomattakoon, että käyttämäni diskurssifunktion käsite eroaa esimerkiksi
Vilkunan [-@vilkuna1989, 38] tai Virtasen [-@virtanen1992discourse, 44]
käyttämistä *discourse function* -termeistä. Sen sijaan tässä käytetty diskurssifunktion
käsite on lähellä Dikin [-@dik1989, 266] termiä *pragmatic function*.

#### Topiikki

Lambrechtin [-@lambrecht1996, 118] pohjalta voidaan määritellä, että *topiikin*
diskurssifunktion täyttää lauseessa[^lausediscl] se elementti, josta kyseinen
lause  kertoo tai josta lauseessa on kyse. Kuten Vilkuna [-@vilkuna1989, 79]
huomauttaa, informaatiorakenteen tutkimuksessa vastaavan määritelmän
kilpailijana on perinteisesti pidetty määritelmää, jonka mukaan topiikki (tai
teema) on se osa lauseesta, joka edustaa vanhaa tai annettua (*old/given*)
informaatiota.
*Uutuus* ja *annettuus* ovat nähdäkseni kuitenkin itsessään liian
yksinkertaistavia käsitteitä. Yhdistänkin niiden merkityspiiriin kuuluvat ilmiöt
tässä ennemmin diskurssistatukseen kuin diskurssifunktioihin. Lambrecht
[-@lambrecht1996, 117; vrt. myös @guijarro2001, 104] korostaa lisäksi eroa siihen, että topiikki
määriteltäisiin lineaarisesti niin, että mikä tahansa on virkkeessä ensimmäisenä
elementtinä, on topiikki. Vastaava määritelmä olisi erityisen ongelmallinen
tämän tutkimuksen kannalta: koska tarkoituksena on muun muassa pyrkiä
ymmärtämään eri syitä ajanilmauksen lauseenalkuiseen sijaintiin, olisi hyödytön
kehäpäätelmä määrittää *a priori*, että kaikkien lauseenalkuisten ajanilmausten
diskurssifunktio on topiikki.

[^lausediscl]:  Puhun tässä yksinkertaisuuden vuoksi *lauseista*, vaikka
kyse saattaa yhtä hyvin olla kompleksisemmista rakenteista kuten yhdyslauseista,
tai -- kuten alempana nähdään -- laajemmistakin tekstikokonaisuuksista.

Koska tutkimuksen kohteena ovat suomalaiset ja  venäläiset ajanilmaukset, on
syytä kiinnittää erityistä huomiota venäläisen tutkimustradition tapaan
määrittää topiikki tai topiikinkaltainen diskurssifunktio. Venäläinen
informaatiorakenteen tutkimus nojaa voimakkaasti Prahan koulukunnan
tutkimustuloksiin, ja merkittävimpänä nimenomaan venäjän kieltä käsittelevänä
kontribuutiona pidetään yleisesti Irina Kovtunovan [-@kovtunova1976] tutkimusta
[@zimmerling2013, 262-263]. Kovtunovan määritelmän mukaan teema[^teematop] on
*lauseen lähtökohta* ja *aihe* [-@kovtunova1976, 6-7]. Janko [-@janko2001, 23,
25] määrittelee teeman suhteessa tämän vastakohtaan, reemaan, niin että teema on
*viestin ei-toteava osa*, joka muodostaa *lähtökohdan* viestille. 

\todo[inline]{Tähän väliin King + d-neutral?}

[^teematop]: käytän toistaiseksi käsitettä teema venäläisestä
tutkimuksesta puhuttaessa

Oleellinen kysymys suhteessa topiikin käsitteeseen on, minkälaiset elementit
voivat toimia tai tyypillisesti toimivat topiikin funktiossa. Ensinnäkin on
tyypillistä, että lauseen subjekti on topiikki [@lambrecht1996, 131]. Voidaan
sanoa, että subjekti on eräänlainen oletuskandidaatti[^dt] topiikin funktioon.
Esimerkiksi lauseen \ref{ee_subtop} Миша on selkeästi referentti, josta lause kertoo:




\ex.\label{ee_subtop} \exfont \small Миша играет на скрипке





\vspace{0.4cm} 

\noindent

Subjektit eivät kuitenkaan ole ainoita mahdollisia topiikkeja, eikä se, että
lauseessa on subjekti, automaattisesti tarkoita, että subjekti saisi topiikin
funktion. Tämän tutkimuksen kannalta tärkeää on huomata, että myös adverbiaalit
voivat toimia topiikin funktiossa. Topiikkina ei voi silti esiintyä mikä
tahansa lauseen elementti, vaan funktioon liittyy olennaisia rajoitteita, joita
on hyvä havainnollistaa edellä käsitellyn diskurssistatuksen avulla.

[^dt]: vrt. käsite *DT* Vilkunalla [-@vilkuna1989, 41]

Ensinnäkin, topiikiksi kelpaa vain elementti, jolla
ylipäätään on jokin diskurssistatus --  toisin sanoen elementin
on oltava luonteeltaan referentiaalinen. On vaikea esittää lauseen kertovan
*jostakin*, jos potentiaalisella topiikilla ei voi olla viittauskohdetta
viestintätilanteen osapuolten mielessä. Tarkemmin sanottuna lauseet eivät ylipäätään
kerro jostakin sanasta tai konstituentista, vaan juuri *referentistä*, johon
konstituentti viittaa [@lambrecht1996, 161].

\todo[inline]{ Lisäksi... [@lambrecht1996, 150] }

[^DT]: Vilkunan [@vilkuna1989] analyysissä T-kentällä (ks. osio 4.x) on
esimerkiksi oma alalajinsa, DT, joka viittaa ennen kaikkea
juuri subjekteihin.

Toiseksi, topiikiksi kelpaavuuden voi nähdä asteittaisena ominaisuutena, niin
että todennäköisyys toimia topiikin funktiossa kasvaa sitä suuremmaksi, mitä
lähempänä tajunnan pintaa viestin lähettäjä olettaa entiteetin vastaanottajalla
sijaitsevan [-@lambrecht1996, 165]. Kaikkein todennäköisimpiä topiikkeja ovat
tämän oletuksen mukaan diskurssistatukseltaan aktivoidut entiteetit, hivenen
vähemmän todennäköisiä käytettävissä olevat ja edelleen vähemmän todennäköisiä
käyttämättömät entiteetit. Tunnistamattomat eli täysin uudet elementit ovat
topikaalisuusasteikon alapäässä niin, että täysin uudet ankkuroidut ilmaukset
 ovat kuitenkin todennäköisempiä topiikkeja kuin täysin
uudet ankkuroimattomat. 


#### Fokus

Fokuksen diskurssifunktio on perinteisesti ollut toinen osapuoli käsiteparissa,
jonka toisena osapuolena on topiikki tai teema. Esimerkiksi Kovtunova
[@kovtunova1976, 6, 39] määrittelee, että lauseet voidaan jakaa kahteen osaan,
joista ensimmäisen tehtävänä on toimia lähtökohtana (teema) ja toisen *kertoa
jotakin ensimmäisestä, toisin sanoen vastata kysymykseen, joka esitetään suhteessa
ensimmäiseen* (kursivointi omani). Gundel [-@gundel1999focus, 295] käyttää tällä
tavalla määritellystä fokuksesta termiä *semanttinen fokus*. Hänen mukaansa
semanttinen fokus on se osa lausetta, joka vastaa (implisiittiseen tai
eksplisiittiseen) lausetta koskevaan kysymykseen siinä kontekstissa, jossa lausetta
käytetään.

Kovtunovalta saatava määritelmä on sikäli puutteellinen, ettei kaikissa
tapauksissa Lambrechtin [-@lambrecht1996, 206] mukaan voida erottaa "ensimmäistä
osaa", mistä seuraa, ettei fokusta aina voi määrittää suhteessa topiikkiin tai
teemaan.[^topiktonesim] On myös tärkeä huomata, ettei
*uutuuden* käsite automaattisesti nivoudu fokuksen käsitteeseen. Esimerkiksi
Gundelin [-@gundel1999topic, 126] mukaan jokin entiteetti voi olla
viestintätilanteen osapuolille tuttu tai annettu, mutta silti toimia fokuksen
funktiossa [@gundel1999topic, 126].

[^topiktonesim]: Yksi Lambrechtin (mts. 177) antama esimerkki topiikittomista
lauseista ovat viestintätilanteen aloittavat *esittelylauseet* (*presentational
sentences*) tyyppiä *olipa kerran prinssi*. 

Jotta tässä käytettävä Lambrechtin oma määritelmä fokuksesta kävisi ymmärrettäväksi, on syytä
tarkastella kahta hänen teoriansa kannalta olennaista käsitettä, pragmaattista
olettamaa  (*pragmatic presupposition*) ja *pragmaattista väittämää* (*pragmatic
assertion*). Lambrecht selventää käsitteiden sisältöä seuraavalla
esimerkillä [-@lambrecht1996, 52]:




\ex.\label{ee_lambfinally} \exfont \small I finally met the woman who moved in downstairs





\vspace{0.4cm} 

\noindent
Lambrechtin (mts. 54) mukaan henkilö, joka esittää lauseen \ref{ee_lambfinally}, olettaa
vastaajan tietävän, että a) joku on muuttanut alakertaan, b) tuo joku on
sukupuoleltaan nainen ja c) viestin lähettäjän olisi olettanut tapaavan kyseisen
henkilön jo aiemmin. Tämä informaatio muodostaa esimerkin \ref{ee_lambfinally}
kontekstissa pragmaattisen olettaman. Pragmaattinen väittämä puolestaan on
Lambrechtin (mts. 52) mukaan *se lauseen ilmaisema propositio, joka viestin
vastaanottajan oletetaan tietävän sen jälkeen, kun tämä on vastaanottanut
viestin*, esimerkissä \ref{ee_lambfinally} se, että alakertaan on muuttanut joku
nainen joka viestin lähettäjän olisi jo pitänyt tavata sekä se, että viestin
lähettäjä on nyt tavannut kyseisen henkilön.

Lambrecht [-@lambrecht1996, 213] esittää fokuksen määritelmäksi, että kyseessä on 
 *se (semanttinen) osa propositiota, jonka suhteen pragmaattinen
väittämä eroaa pragmaattisesta olettamasta"*. Jos esimerkissä
\ref{ee_lambfinally} pragmaattinen väittämä kattaa tiedot (vastaanottajan oletetaan
tietävän että) [joku nainen on muuttanut
alakertaan; viestin lähettäjä on tavannut naisen; tapaamisen olisi pitänyt
tapahtua jo aiemmin] ja pragmaattinen olettama
näiden lisäksi tiedon [joku nainen on muuttanut alakertaan], on fokus silloin se, että viestin
lähettäjä on tavannut naisen. Huomaa, että tällä tavoin määriteltynä fokus on
todella nimenomaan oletettujen tiedontilojen suhde: se ei
määräydy syntaktisesti tietyn lauseaseman mukaan, eikä siitä toisaalta voida
sanoa, että jokin tietty sana tai lauseke on fokus, vaan pikemminkin fokus on
viestinjälkeisin tiedon suhde viestiä edeltävään.

Palaan tässä kohtaa diskurssistatuksen käsitettä määriteltäessä käytettyyn
esimerkkiin \ref{ee_eduskunta_teoria2}, joka esitetään uudestaan numerolla
\ref{ee_eduskunta_teoria2b}:




\ex.\label{ee_eduskunta_teoria2b} \exfont \small Se on viime aikoina kärjistynyt entisestään.





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_eduskunta_teoria2b} -- jos ajatellaan, että tätä ennen on esitetty
esimerkki \ref{ee_eduskunta_teoria} -- pragmaattiseksi olettamaksi voitaisiin määritellä 
esimerkiksi seuraavat väitteet
[Suomessa on olemassa eduskunnaksi kutsuttu elin, joka käsittelee erilaisia kysymyksiä;
Eduskunta aikoo käsitellä Libyan tilannetta; Libyan tilanne on negatiivinen (siellä on konflikti)]. 
Esimerkin \ref{ee_eduskunta_teoria2b} pragmaattinen väittämä lisää tähän väittämien listaan
tiedon siitä, että Libyan tilanne on muuttunut vielä negatiivisemmaksi.

Selvitän lopuksi ajanilmausten esiintymistä fokuksena tarkastelemalla vielä yhtä
vuoteen 2011 liittyvää keksittyä virkettä \ref{ee_libyakesti}:




\ex.\label{ee_libyakesti} \exfont \small Libyan sisällissota kesti kahdeksan kuukautta.





\vspace{0.4cm} 

\noindent
Esimerkistä \ref{ee_libyakesti} voidaan todeta, että ajanilmaus *kahdeksan kuukautta*
on vähintäänkin osa fokuskenttää [vrt. Lambrechtin  käsite *focus domain* -@lambrecht1996, x]
tai *fokaalinen*. Konstruktionotaationa esimerkki \ref{ee_libyakesti} esitettäisiin
tässä tutkimuksessa seuraavasti:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & foc \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 4}: Esimerkki \ref{ee_libyakesti} attribuutti--arvo-matriisina \normalfont \vspace{0.6cm}

Matriisista 4 on tarkoituksella jätetty pois muut
kuin ajanilmauksen sijainnin kannalta olennaiset diskurssifunktioihin ja
-statuksiin liittyvät merkinnät. Koska ajanilmauksen sijainti on parhaiten
selitettävissä sen fokaalisuudella (vrt. alaluku \ref{loppus_np} ja *fokaalisen
konstruktion* määrittely), on sen diskurssifunktioksi (df) merkitty fokaalista
konstituenttia tarkoittava *foc*. 


#### Ajanilmauksen topikaalisuuteen liittyviä määrittelyongelmia

Kuten edellä on jo useammassa yhteydessä käynyt ilmi, en näe mielekkääksi
pyrkiä sovittamaan ajanilmauksia aina joko topiikin tai fokuksen
diskurssifunktioihin. Erityisen paljon määrittelyongelmia liittyy topiikin
käsitteeseen.

Topiikki määriteltiin edellä yksinkertaistaen siksi, mistä jokin lause kertoo.
Kuten Shore [-@shore2008, 39] huomauttaa, puheenaiheena oleminen on lopulta siinä
mielessä petollinen määritelmä, että oikeasta kulmasta katsottuna lauseen voi
nähdä kertovan melkeinpä mistä tahansa siinä mainitusta. Esimerkiksi lauseen
\ref{ee_eduskunta_teoria2b} voi periaatteessa tulkita kertovan ajanilmauksen *viime aikoina*
referentistä -- siitä aikajanan segmentistä, johon ajanilmaus viittaa, mutta
ajanilmauksen kutsuminen lauseen topiikiksi vaikuttaisi silti jossain määrin 
ongelmalliselta ja epämääräiseltä.

Avuksi tähän määrittelyn epämääräisyyteen voidaan ottaa Lambrechtin
[-@lambrecht1996, 119] ajatus siitä, että myös topikaalisuus tulisi ymmärtää
asteittaisena ilmiönä, niin että jokin propositio *p* kertoo jossain määrin
referentistä *a*, jossain määrin -- mahdollisesti enemmän -- referentistä *b*,
ja yhdessä ja samassa lauseessa voi siis olla useampia topiikkeja. Myös
esimerkin \ref{ee_eduskunta_teoria2b} *viime aikoina* -ajanilmauksen voi nähdä
jossain määrin *topikaalisena*, sikäli kuin tällaisella analyysilla on
jonkinlaista lisäarvoa pohdittaessa syitä ajanilmauksen sijainnille. Selkeimmin
lauseen topiikkina on kuitenkin nimenomaan Libyan tilanne, josta kerrotaan
tiettyä ajallista taustaa vasten. 

Ajatus useasta topiikista (teemasta) on keskeinen myös Jankolla, joka esittää
seuraavan esimerkin: [-@janko2001, 78][^alunperinp]

[^alunperinp]: Alun perin esimerkki peräisin Paduchevalta 19xx, joka on
poiminut sen Tshehovin novellista X.




\ex.\label{ee_grob} \exfont \small На другой день в двенадцатом часу гробовщик и его дочери вышли из
калитки.





\vspace{0.4cm} 

\noindent
Jankon analyysin mukaan virkkeessä \ref{ee_grob} sekä *на другой день*, *в двенадцатом
часу вечера* että *гробовщик и его дочери* ovat samanlaisia, "täysiverisiä"
(*полноценные*), teemoja. Jos mietitään mainittujen topiikkien
informaatiorakenteellista painoarvoa, niiden väliltä todella on vaikea löytää eroja:
esimerkki \ref{ee_grob} tuntuu kertovan yhtä paljon ja samansuuruisella painotuksella
niin seuraavasta päivästä, kahdennestatoista tunnista kuin arkuntekijästä ja tämän
tyttäristäkin. Vastaavissa tapauksissa on toisaalta usein ollut tapana puhua
myös erityyppisistä topiikeista tai teemoista. Esimerkiksi Shore
[-@shore2008, 45] käyttää nimitystä *orientoiva sivuteema* ja havainnollistaa 
käsitettä seuraavalla lauseella:




\ex.\label{ee_shore45} \exfont \small Viime vuosikymmenellä me suomalaiset kulutimme ennätysmäärän ulkomaisia
tuotteita.





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_shore45} Shore analysoi ajanilmauksen *viime vuosikymmenellä*
(orientoivaksi) sivuteemaksi ja subjektin *me suomalaiset* varsinaiseksi
teemaksi. Samantyyppiseen analyysiin viittaavat myös esimerkiksi Chafen
[-@chafe1976] käyttämä nimitys *scene-setting topics* [vrt. @le2003time ja
*frame-setting topics*] sekä venäjänkielisessä tutkimusperinteessä tavallinen
termi *lokalisaattori* (*локализатор*) [ks. esim  @suleimanova2015, 210], joka
kuitenkin luonnollisesti rajoittuu lokalisoiviin ilmauksiin. Kaikkien mainittujen 
käsitteiden ongelmana tämän tutkimuksen kannalta on, että niiden määrittely
tuntuu olevan väkisinkin jo valmiiksi sidoksissa niiden sijaintiin: *sivuteema*
on jotakin, joka ilmaistaan *varsinaista teemaa* edeltävällä konstituentilla.
Samaten *scene-setting topic* viittaa juuri lauseen alkuun [@lambrecht1996,
??]. 

Esimerkin \ref{ee_shore45} kaltaisista lokalisoivista ilmauksista on myös
tavallisesti huomautettu, että ne ovat syntaktisesti irrallaan muusta lauseesta
ja liittyvät siihen ennen kaikkea semanttisesti [@shi2000topic, 388]. Ne voi
tulkita verbin mutta toisaalta myös koko lauseen määritteiksi: tällaisen
syntaktisen määritelmän nojalla erityisesti fennistiikassa on ollut tapana
nimittää esimerkin \ref{ee_shore45} kaltaisia ajanilmauksia *kehysadverbiaaleiksi*
[ks. esim. @hakulinenkarlsson1979, xx, @visk, xx].  Kovtunovalla
[-@kovtunova1976, 61] puolestaan vastaava nimitys on *adverbiaalideterminantti*
(обстоятельственный детерминант). Oma 
lähestymistapani  on kuitenkin olla ottamatta 
kantaa ajanilmauksen syntaktisen aseman hienojakoisempaan kuvaukseen tai
topiikkien jaotteluun esimerkiksi erilaisiksi sivuteemoiksi. Sen sijaan 
näen jossain määrin hyödylliseksi toisentyyppisen, topiikkien suhdetta
ylemmän tason tekstuaalisiin kokonaisuuksiin kuvaavan jaottelun,
jonka esitän lyhyesti seuraavassa.

#### Eri tason topiikkeja

Edellä mainittiin, että lausetason lisäksi informaatiorakennetta on mahdollista
tarkastella ylemmällä, esimerkiksi kokonaisten tekstien tasolla. Toteutan
ylemmän tason tarkastelun hyödyntämällä Simon Dikin [-@dik1989] tapaa
jaotella topiikit useampaan eri alaluokkaan riippuen siitä, mikä rooli niillä
on sen laajemman tekstikokonaisuuden kannalta, jonka osina ne esiintyvät.
[esimerkkinä jaottelun kontrastiivisesta sovellutuksesta ks. @hildalgo2010].

Dik (mts. 267) lähtee liikkeelle siitä, että minkä tahansa diskurssin
(kirjoitetun tekstin, luennon, keskustelun ym.) voi aina nähdä *kertovan
jostakin*. Jos tätä vertaa edellä esitettyyn topiikin määritelmään, voidaan
perustellusti tulkita, että myös kokonaiset diskurssit toteuttavat topiikin
funktiota. Näitä ylätason topiikkeja Dik nimittää *diskurssitopiikeiksi*.
Diskurssitopiikeista on syytä huomata, että niitä voi olla monentasoisia ja ne voivat
olla hierarkkisessa suhteessa keskenään: esimerkiksi tämä kappale kokonaisuudessaan
on kertonut diskurssitopiikista, mutta samalla se on osa informaatiorakenteesta kertovaa
osiota ajanilmausten sijaintia käsittelevässä väitöskirjassa.

Diskurssitopiikin lisäksi Dik määrittelee erilaisiksi topiikin lajeiksi *uudet
topiikit* (new topic), *esitellyt topiikit* (given topic), *alatopiikit*
(subtopic) ja *palautetut topiikit* (resumed topic). Ajatus annetuista ja
palautetuista topiikeista palvelee osin edellä kuvattuun diskurssistatukseen
liittyvän käsitteistön kanssa päällekkäisiä tarkoitusperiä, mutta erityisesti
uuden topiikin ja alatopiikin käsitteet ovat myös tämän tutkimuksen kannalta
selitysvoimaisia konsepteja. Käytän niistä jatkossa termejä 
*esittelytopiikki* ja *alatopiikki* [vrt. @sandberg19]. 

Tekstin luominen voidaan Dikin mallissa nähdä prosessina, jossa viestijä
esittelee uusia diskurssitopiikkeja ja toisaalta pyrkii pitämään jo esitellyt
topiikit käytettävissä (vrt. diskurssistatus ja käytettävissä oleminen edellä)
viittaamalla niihin eri tavoin -- tätä prosessia on Dikin (mts. 271) mukaan
nimitetty muun muassa käsitteellä *topiikkiketju* [ks. @givon1983topic]. 
Topiikkiketjut rakentuvat tavallisesti yksinkertaisimmillaan niin, että 
ketjun alussa uusi diskurssitopiikki tuodaan esittelytopiikkina
mukaan tekstiin, minkä jälkeen siihen voidaan joko viitata
jo esiteltynä topiikkina tai siitä voidaan esimerkiksi lohkaista uusi 
alatopiikki. Topiikkiketjun ajatusta voidaan havainnollistaa seuraavalla
analyysitapaa soveltaneen Jesus Guijarron [-@guijarro2001, 110]
esittämällä esimerkillä (tässä lyhennettynä):




\ex.\label{ee_guijarro} \exfont \small ...Crete (1) has a fascinating history... Being the largest of the Greek
islands, its landscape (2) changes at every road bend... ...whaterver
your idea of a good time, Crete’s (3) got something just for you!





\vspace{0.4cm} 

\noindent
Guijarron analyysin mukaan katkelman ensimmäinen *Crete* (1) on esittelytopiikki.
Tässä numerolla 2 merkitty *landscape* edustaa Kreetan saaren esittelyn myötä 
diskurssistatukseltaan semiaktiiviseksi tullutta entiteettiä (ks. 
\ref{diskurssistatus} edellä), jota voidaan käyttää alatopiikin roolissa.
Kohdassa 3 on puolestaan kyseessä esitelty topiikki:
kirjoittaja viittaa kohteeseen, joka on kerran jo tuotu tekstiin ja  siten sijaitsee 
oletettavasti lähellä lukijan tajunnan pintaa eikä vaadi erityistä lauseasemaa
tai tarkentavia määreitä.

Tutkimuksen luvussa \ref{likim-s1} havaitaan, että alatopiikit ovat tärkeässä
roolissa, kun mietitään etenkin lauseenalkuisen ajanilmauksen diskurssifunktioita
tekstissä. Tässäkin yhteydessä on otettava huomioon luvussa \ref{ajanilm-kasite}
esitetty ajatus siitä, että aika ja temporaalisuus hahmotetaan usein paikan 
ja spatiaalisuuden tarjoaman mallin mukaan. Vastaavalla tavalla kuin esimerkissä \ref{ee_guijarro}
kirjoittaja rakentaa tekstiään pilkkomalla käsiteltävää aihetta erilaisiin maantieteellisiin
palasiin, on tavallista, että jokin diskurssitopiikki kuvataan
erilaisten ajallisten segmenttien kautta. Esimerkiksi jonkun tekstin aiheena olevan
henkilön elämää voitaisiin käsitellä jaottelemalla se relevantteihin ajanjaksoihin kuten
lapsuus, nuoruus ja aikuisuus [vrt. @vilkuna1989, 92].

Esittelytopiikeista on paikallaan huomata, että niitä voidaan toteuttaa
lukuisilla eri strategioilla. Monet uuden topiikin
esittelytavat ovat enemmän tai vähemmän universaaleja, mutta esimerkiksi
venäjässä tyypilliset verbialkuiset rakenteet kuten Yokoyaman [-@yokoyama1986,
217] esimerkki *К вам пришла Галина Петровна* [vrt. myös @kovtunova1976, 68]
eivät tulisi kysymykseen suomessa. Joskus
kirjoittaja voi käyttää erityisiä metakielellisiä rakenteita kuten *Kerron nyt X:stä*, 
mutta usein uuden topiikin esitteleminen kuitenkin tapahtuu hienovaraisemmin,
esimerkiksi tiettyjen ilmestymistä merkitsevien verbien avulla [@dik1989, 268 vrt. myös
@vilkuna1989, 165; @janko2001, 161]. 

Dik (mts. 269) mainitsee, että
esittelytopiikit sijaitsevat tyypillisesti kielestä riippumatta lauseen
loppupuolella. Ei kuitenkaan ole lainkaan harvinaista, että esittelytopiikkina
on esimerkiksi lauseen alussa oleva subjekti. Esimerkiksi Lambrecht
[-@lambrecht1996, 143, 177] käyttää uuden topiikin diskurssiin tuovista
rakenteista nimitystä *esittelykonstruktio* (*presentational construction*),
jollaisista ja antaa esimerkkinä yhtä lailla satukirjan aloittavan *once there
was a wizard* kuin arkisessa puhetilanteessa käytettävän *JOHN called* (isot
kirjaimet Lambrechtin tapa osoittaa prosodista korostamista). 
Esittelykonstruktiosta irrallinen ilmiö ovat tavallisesti *topikalisoiduiksi*
kutsutut tapaukset, joissa jokin elementti tuodaan topiikille 
tyypilliseen asemaan lauseen alkuun [@lambrecht1996, 147; @vilkuna1989, 91].


#### Muut diskurssifunktiot {#muut-df}

Informaatiorakenteen tutkimuksessa on usein tapana erotella paitsi topikiin ja
fokuksen diskurssifunktiot, myös suorittaa tarkempia jaotteluita esimerkiksi
sen perusteella, onko kyseessä *kontrastiivinen* fokus tai topiikki.
Esimerkiksi Jekaterina Janko, joka tarkastelee informaatiorakennetta puheaktien
käsitteen kautta, erottelee toisistaan *puheakteja muodostavat funktiot* kuten
fokus ja *puheakteja muokkaavat funktiot*, joita ovat muun muassa kontrasti,
vahvistus ja emfaasi [-@janko2001, 46]. 

Jankon (mts. 47) mukaan kontrastin käsitteelle on olennaista ajatus siitä,
että jostakin vaihtoehtojen joukosta valitaan tai osoitetaan oikeaksi yksi
tietty vaihtoehto. Gundell [-@gundel1999focus, 294] puolestaan määrittelee, että
kontrastin tapauksessa viestin lähettäjä ajattelee, ettei viestin vastaanottaja
ole keskittänyt huomiotaan siihen entiteettiin, jossa huomion lähettäjän
mielestä pitäisi olla. Janko antaa muun muassa seuraavan esimerkin:




\ex.\label{ee_vasjamasha} \exfont \small Вася пришел (а не Маша)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_vasjamasha} olennaista on, että Vasja on erotettu muusta
lauseesta intonaation keinoin. Lauseesta on helppo määrittää, että siinä
kuvataan kontrasti *Vasja*- ja *Maša*-sanojen referenttien välillä. Voisi
määritellä, että *Vasja* on lauseen kontrastiivinen fokus.

Informaatiorakennetta käsittelevässä kirjallisuudessa ei kuitenkaan ole aina
yksiselitteistä, katsotaanko kontrastin kohteena oleva entiteetti
yläkäsitteeltään topiikiksi vai fokukseksi. Esimerkiksi Gundell esittää edellä
mainitun määritelmänsä yhteydessä seuraavan esimerkin:




\ex.\label{ee_gundellrob} \exfont \small To ROB you can complain about anything having to do with the PROGRAM,
and to CHRISTINE you can address all the OTHER complaints.





\vspace{0.4cm} 

\noindent
Gundellin mukaan esimerkissä \ref{ee_gundellrob} on kyse
puhtaan kontrastiivisesta *fokuksesta.* Toisaalta 
esimerkiksi Carola Féry ja Manfred Krifka [-@fery2009information, 127] kuvaavat seuraavaa
esimerkkiä kontrastiiviseksi *topiikiksi*:




\ex.\label{ee_fery8} \exfont \small A: What are your sisters playing? B: My YOUNGER sister plays the VIOLIN,
and my OLDER sister, the FLUTE.





\vspace{0.4cm} 

\noindent
Esimerkeistä \ref{ee_gundellrob} ja \ref{ee_fery8} toinen on siis analysoitu kontrastiiviseksi
fokukseksi, toinen kontrastiiviseksi topiikiksi, vaikka päällisin puolin
kumpikin kuvaa melko samanlaista viestintätilannetta. Itse
en näe tämän tutkimuksen kannalta tarkoituksenmukaiseksi tehdä
mitään ehdotonta erottelua kontrastiivisten topiikkien ja kontrastiivisen fokusten välillä, ja
katsonkin kontrastin yksinkertaisesti omaksi diskurssifunktiokseen -- yhdeksi mahdolliseksi
diskurssifunktio-attribuutin arvoksi siinä missä topiikin ja fokuksenkin.

Kontrastin ohella toinen topiikin ja fokuksen lisäksi käyttämäni diskurssifunktio
on *affekti*, jolla viittaan Isoa suomen kielioppia (VISK §1707) mukaillen siihen, miten *puhuja 
osoittaa suhtautumistaan tai asennoitumistaan puheenalaiseen asiaan tai
puhekumppaniinsa*. Esimerkiksi Yokoyaman [-@yokoyama1986, 256] mukaan suhtautumisella viestittävään
asiaan tai viestissä mainittaviin referentteihin on ainakin venäjässä
vaikutusta myös syntaksin tasolle ja siihen, mihin kohtaa lausetta
elementtejä sijoitetaan. Yokoyama viittaa ilmiöön *empatian* käsitteellä
[Alun perin ks. @kuno1977empathy]. 

\todo[inline]{@besnier1990 ? }

Topiikin, fokuksen, kontrastin ja affektin lisäksi ajanilmuksen diskurssifunktioksi
määritellään joissain tapauksissa pelkästään *muu*. Tälläinen tapaus
koskee esimerkiksi seuraavan varsinaiseen tutkimusaineistoon
kuuluvan virkettä, jota käsitellään tarkemmin luvussa \ref{varsinainen-resultatiivinen-ekstensio}.




\ex.\label{ee_rutiinit_teoria} \exfont \small Arjen rutiinit rikkoutuvat ja \emph{äkkiä} elämä saa odottamattoman
käänteen.





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_rutiinit_teoria} ei ole tarkoituksenmukaista puhua *äkkiä*-sanan
diskurssifunktiosta siinä mielessä, että sen voisi jollain tasolla katsoa
olevan virkkeessä fokuksena tai topiikkina tai toisaalta kontrastin tai
affektin ilmaisimena. Tästä huolimatta sillä on
selkeä viestinnällinen tehtävä: se auttaa luomaan ajatuksen
siirtymisestä staattisesta tilasta kohti jotakin muutosta ja toimii
eräänlaisena konnektorina tai "segmentaatiomarkkerina" ['segmentation marker', vrt. @bestgen1995, 387].

Luvussa  \ref{morf-sama-keski} tarkasteltavassa esimerkissä \ref{ee_atempnyt_teoria}
nyt-sanalla puolestaan on lähes tyystin ei-ajallinen diskurssipartikkelin funktio:




\ex.\label{ee_atempnyt_teoria} \exfont \small Jos joku nyt vetää herneen siitä nenään, että blogin pitäjä sattuu
pitämään jostakin/ jonkun mielipiteestä, niin ehkä kannattaisi sitten
seurata jotain blogia, jonka kirjoittajalla ei ole mielipiteitä.





\vspace{0.4cm} 

\noindent
\todo[inline]{Esimerkki konstruktionotaatiosta vasta/jo tänne?}

Myös esimerkin \ref{ee_atempnyt_teoria} kaltaiset tapaukset esitetään
konstruktioita kuvaavissa piirrematriiseissa *muu*-merkinnällä.



Suomen ja venäjän ajanilmauskonstruktiot kontrastiivisena tutkimuskohteena {#kontr}
===========================================================================



Edellä osiossa \ref{vsvtk} esiteltiin *ajanilmauskonstruktion* käsite  ja
todettiin sen olevan nyt käsillä olevan tutkimuksen varsinainen kohde. Tämän
luvun tehtävänä on eritellä tarkemmin, minkälaiset ajanilmauskonstruktiot
ovat suomessa ja venäjässä tutkimuskirjallisuuden perusteella mahdollisia ja
miten yleisiä minkäkin tyyppiset konstruktiot ovat. Ennen siirtymistä
suomen ja venäjän konkreettisiin ominaisuuksiin tarkastelen kuitenkin sitä, 
minkälainen kieltenvälinen vertailu on tämänkaltaisen tutkimuksen kannalta
mielekästä.

Ajanilmauskonstruktiot ja kontrastiivinen (funktionaalinen) analyysi
----------------------------------------------------------------------

Andrew Chesterman [-@chesterman1998contrastive, 53] esittää, että kieltenvälisen
vertailun -- kontrastiivisen analyysin -- tavoitteena on tuottaa ja testata
falsifioitavissa olevia hypoteeseja *jonkin ilmiön samanlaisuudesta* kahden kielen
välillä. Tässä tutkimuksessa tutkittavana ilmiönä ovat ajanilmauskonstruktiot ja
tutkimuksen tavoitteena sen tarkasteleminen, kuinka samanlaisia tietyt
ajanilmauksen sisältävät rakenteet ovat suomessa ja venäjässä.

Mielenkiintoista kyllä, konstruktiokieliopin piirissä kontrastiivinen tutkimus
on esimerkiksi Hans Boasin [-@boas2010, 2] mukaan ollut verrattain vähäistä. Kuten Boas
argumentoi, tämä ei kuitenkaan johdu siitä, etteikö konstruktionistinen teoria
sopisi hyvin kontrastiivisen vertailun lähtökohdaksi. Päinvastoin, juuri
ajanilmauskonstruktioita ajatellen konstruktiokielioppi tarjoaa erityisen hedelmällisen 
teoreettisen lähtökohdan siitä syystä, että konstruktion käsite antaa
mahdollisuuden vertailla kieliä kaikilla tasoilla -- ei vain leksikaalisella tai
syntaktisella vaan myös esimerkiksi informaatiorakenteen tasolla (mts. 15).

On korostettava, että nyt käsillä olevassa tutkimuksessa konstruktion käsite on
ennen muuta työkalu, käsitteellinen tila, jonka sisällä suomea ja venäjää
voidaan vertailla. Lähtökohtani on ennemmin kontrastiivinen (alhaalta ylös) kuin
typologinen (ylhäältä alas) [vrt. @boas2010, 7]: en lähde ajatuksesta, jonka
mukaan suomen ja venäjän ajanilmauskonstruktiot heijastaisivat jotakin
universaalia konstruktiomallia -- ennemminkin lähestyn konstruktioita itsenäisinä,
kielispesifeinä ilmiöinä, joiden keskinäisestä samanlaisuudesta voidaan tehdä
päätelmiä. Aivan kuten Leino [-@leino2010, 125], katson siis, ettei ole
mielekästä puhua yhden ja saman konstruktion realisoitumisesta kielessä A ja
kielessä B, vaan pikemminkin kahdesta erillisestä konstruktiosta, joiden
samanlaisuutta (Leinolla *correspondence*) voidaan arvioida.

\todo[inline]{katso kielispesifisyydestä Croft 2001 (lk.8-9)}

Samanlaisuus sinänsä on pintapuolisesta yksinkertaisuudestaan ja
arkipäiväisyydestään huolimatta melko monitahoinen  käsite, johon liittyy paljon
määrittelyongelmia [@chesterman1998contrastive, 6]. Chesterman mainitsee
samanlaisuuden määrittelyn kannalta merkittävinä tutkijoina muun muassa Tverskyn
[-@tversky77], Medinin ja Goldstonen [-@medingoldstone1995] ja Sovranin
[-@sovran1992]. 

Yksi keskeisimmistä samanlaisuuteen liittyvistä käsitteistä ovat *piirteet* ja
*piirrejoukot*: Chestermanin (mts., 7) mukaan yksinkertaisimmillaan voidaan
ajatella, että jos kahdella entiteetillä on jokin yhteinen piirre, ovat ne
ainakin jossain määrin samanlaisia ja jos niillä on pelkästään yhteisiä
piirteitä, ne ovat identtisiä. Olennaista kuitenkin on erottaa ne piirteet,
jotka kulloisenkin vertailun kannalta ovat relevantteja tai keskeisiä ja
ymmärtää, että samanlaisuuden asteeseen vaikuttavat aina myös olosuhteet, joissa
vertailu suoritetaan (mts. 9). Lisäksi, kuten Leino [-@leino2010, 132] painottaa,
on huomattava, että samanlaisuudessa on aina kyse asteittaisesta ominaisuudesta.
Konstruktioiden tapauksessa on Leinon mukaan (mp.) ajateltava paitsi
merkityksen, myös muodon tason samanlaisuutta.

Kieltenvälisestä vertailusta puhuttaessa ei samanlaisuuden käsitteen lisäksi voi
välttää termiä *ekvivalenssi*. Chestermanin [-@chesterman1998contrastive, 20-25]
mukaan termi on kulkenut käännöstieteissä pitkän matkan: alun perin kyse on
ollut oletuksesta lähde- ja kohdetekstin yhtäläisyydestä, mistä sittemmin
on siirrytty jaottelemaan ekvivalenssia eri alalajeihin (kuten Nidan formaalinen
ja dynaaminen ekvivalenssi) ja lopulta relativistiseen näkemykseen, jonka mukaan
lähde- ja kohdeteksteistä ei ylipäätään ole mielekästä pyrkiä määrittelemään,
ovatko ne millään tasolla samoja -- järkevämmäksi on  nähty se, että tarkastellaan nimenomaan
samanlaisuutta ja pohditaan esimerkiksi sitä, onko lähdetekstin suhde johonkin
tiettyyn kohdetekstiin tarkoituksenmukainen. Vaikka lähtökohtaisesti voisi
ajatella, että se, mitä kontrastiivisessa kielitieteessä ymmärretään
ekvivalenssilla eroaisi siitä, miten käsite
ymmärretään käännöstieteessä, Chesterman (mts. 39) esittää, että myös
kontrastiivisessa tutkimuksessa ajatus kääntämisestä on yleensä vähintään
implisiittisesti läsnä: kieliä vertailevat tutkijat arvioivat loppujen lopuksi
sitä, voisiko jokin kielen A esiintymä olla käännös jollekin kielen B
esiintymälle.[^leinotrekvo] 

[^leinotrekvo]: esimerkiksi Leino käyttääkin spesifisti termiä *translation
equivalence* ekvivalenssista puhuessaan. 

Nyt käsillä olevan tutkimuksen kannalta ekvivalenssi ei kuitenkaan ole erityisen
keskeinen käsite. Allekirjoitan Chestermanin [-@chesterman1998contrastive, 28]
mainitseman Touryn [-@toury1981, 256] oletuksen siitä, että kontrastiivisen ja
käännöstieteellisen tutkimuksen välillä voidaan nähdä työnjako: siinä missä
edellisen tehtävänä on selvittää, mitä samanlaisuuksia kielten välillä on,
pyrkii jälkimmäinen selittämään, miksi kääntäjä on valinnut juuri tietyn
samanlaisuuden käännösratkaisunsa pohjaksi. Näen oman tutkimukseni selkeämmin
osana kontrastiivista kenttää, enkä pyri niinkään arvioimaan
ajanilmauskonstruktioihin liittyviä käännösratkaisuja kuin tarjoamaan tällaisten
arvioiden tekijöille materiaalia arvioinnin pohjaksi. Tässä mielessä omalta
kannaltani on oleellisempaa puhua ennemmin yksinkertaisesti samanlaisuudesta
kuin ekvivalenssista -- olkoonkin, että määritellessäni jotkin konstruktiot
samanlaiseksi tulen samalla tehneeksi jonkinlaisen esityksen niiden
käännettävyydestä.

\todo[inline]{Leinon (2010) "vastaavuus"-käsite (correspondence)}

Metodologiselta kannalta ajateltuna noudattelen
samanlaisuuden määrittelemisessä Chestermanin funktionaaliseksi
kontrastiiviseksi analyysiksi nimittämää strategiaa, joka  etenee siten, että
aluksi tutkijalla on jokin havainto, jonka perusteella hän katsoo kielen A
ilmauksen X olevan mahdollisesti samanlainen kuin kielen B ilmauksen Y. Kussakin
konkreettisessa vertailussa on erikseen määriteltävä, mitä ovat ne ilmaukseen ja
sen käyttöön liittyvät piirteet, joiden samanlaisuus on vertailun kannalta
relevanttia [@chesterman1998contrastive, 56]. Kun tutkija on määritellyt
kriteerit, joiden perusteella samanlaisuutta arvioidaan, hän esittää
lähtökohtaiseksi hypoteesikseen, että ilmaukset X ja Y ovat identtisiä (mts.
57). Tämän jälkeen hypoteesia testataan ja mikäli ilmaukset eivät ole
identtisiä, muodostetaan uusi hypoteesi, jossa X:n ja Y:n välistä suhdetta
kuvaillaan joksikin muuksi kuin identtiseksi ja esitetään, millä tavoin X ja Y
eroavat (mts. 58). Tätä muokattua hypoteesia testataan ja muokataan edelleen.

Palaan ajanilmauskonstruktioiden samanlaisuutta mittaaviin kriteereihin
tarkemmin luvussa \ref{kokoava-kontrastiivinen-analyysi}, jonka tavoitteena on
etsiä konkreettisia konstruktioryppäitä ja esittää arvioita niiden
samanlaisuudesta suomessa ja venäjässä. Jaan itse vertailtavat konstruktiot
ajanilmauksen sijainnin mukaan neljään pääryhmään, jotka määritellään lähemmin
seuraavassa.

\todo[inline]{Jotain ehkä vielä tekstistrategiasta: konstruktiotasoa isommat kokonaisuudet
ja (ehkä samankaltaisten) konstruktioiden *käyttö* vs. rakenne}

Neljäs taksonomia: sijainti {#taks4}
---------------------------

Tässä yhteydessä on tarpeen esitellä eräs oleellinen tässä tutkimuksessa
analysoitavaa tutkimusaineistoa määrittelevä rajoite. Koska tarkoituksena on
tutkia nimenomaan *ajanilmauksen* sijaintia eikä esimerkiksi sanajärjestystä
yleensä, pyrin rajoittamaan tarkasteltavien lauseiden joukkoa vakioimalla sen
syntaktisen ympäristön, jossa ajanilmauksia tarkastellaan. Tällaisena
vakioituna syntaktisena ympäristönä toimivat *myönteiset SVO-lauseet* eli
lauseet

- jotka eivät ole kieltolauseita
- joissa on nominatiivimuotoinen subjekti, finiittiverbi ja objekti (tai
  vastaava valenssin kannalta olennainen verbin argumentti, ks. osio \ref{tutkimusaineistot})
- joissa mainitut kolme elementtiä esiintyvät nimenomaan järjestyksessä
  subjekti--verbi--objekti.

Valittu rajaus ei välttämättä kuvasta kaikkein yleisintä syntaktista ympäristöä
sen paremmin suomessa kuin venäjässä, eikä se ole täysin tasapuolinen kielten
välillä. Oman näkemykseni mukaan tärkeimmät rajaukseen liittyvät ongelmakohdat
voidaan tiivistää seuraavaan listaan:

1. Kummassakin kielessä on hyvin tavallista, että subjekti ilmaistaan osana
   taivutuspäätettä tai jätetään kokonaan ilmaisematta.
2. Kummassakin kielessä on paljon erilaisia subjektittomia lausetyyppejä [ks. @mlein]
3. Venäjässä SVO-järjestys ei ole yhtä tavallinen kuin suomessa[^kinghuom]

[^kinghuom]: Vaikka venäjää on perinteisesti pidetty SVO-kielenä, 
on esimerkiksi esimerkiksi King [-@king1995] esittänyt perusteita
jopa VSO-järjestyksen ensisijaisuudelle. 
\todo[inline]{Intransitiivilauseista...}

Yhtä kaikki, on myös selvää, että SVO-lauseet ovat sellainen syntaktinen
ympäristö, joka kummassakin kielessä on kaikesta huolimatta tavallinen. Se, mitä
ajanilmausten sijainnista selviää myönteisiä SVO-lauseita tarkastelemalla, on
luultavasti relevanttia ja yleistettävissä laajemmallekin. Ehdottomasti tärkein
syy juuri tämän rajauksen valitsemiselle on kuitenkin tekninen: SVO-lauseet on
kaikkein helpoin erottaa koneellisen annotoinnin perusteella ja toisaalta niiden
osalta koneellisen annotoinnin tarkkuus myös on oletettavasti hyvä [@tobecited].

\todo[inline]{huomauta subjektittomista}

SVO-lauseiden valinta syntaktiseksi rajaukseksi saattaa ensi silmäyksellä  olla
epätyypillinen myös konstruktiokieliopin näkökulmasta -- onhan
konstruktiokieliopissa nähty ensiarvoisen tärkeäksi nimenomaan perifeeristen
rakenteiden tutkiminen ja vastustettu filosofiaa, jossa lähdetään liikkeelle
yksinkertaisista tapauksista ja siirrytään asteittain kompleksisempiin
[@ostman2004, 15]. Tässäkin yhteydessä on kuitenkin korostettava uudestaan,
että syyt juuri SVO-lauseiden valintaan ovat ennen kaikkea tekniset. Koska
konstruktiokieliopin näkökulmasta tavoitteena on lopulta kattaa *kaikki
mahdolliset* kielen ilmiöt -- yksinkertaiset ja kompleksiset -- ei
SVO-lauseiden valitseminen syntaktiseksi rajaukseksi ainakaan mitenkään sodi
tätä ajatusta vastaan, vaan ne ovat rakenteita siinä missä muutkin.
Kielteisistä lauseista on vielä todettava, että pääsyy niiden sulkemiseen
aineiston ulkopuolelle on se, että suomessa kieltorakenteet muodostetaan
erityistä kieltoverbiä käyttäen tavalla, joka eroaa selvästi venäjän
kieltorakenteiden muodostamisesta [suomen kieltoverbistä ks. esim
@miestamo2004suomen; venäjän kieltorakenteista esim. @paduceva2013].
Koneelliseen analyysiin liittyviin ongelmiin sekä rajauksen tekniseen
toteutukseen pureudutaan tarkemmin osiossa
\ref{tutkittavien-ajanilmaustapausten-erottaminen-verrannollisista-korpuksista}.

SVO-järjestyksessä pitäytymisellä on myös se etu, että se tekee ajanilmauksen
sijainnin määrittelemisen melko selkeäksi. Esitänkin seuraavassa edellä 
käsiteltyjen kolmen ensimmäisen ajanilmausten taksonomian lisäksi neljännen, 
sijainnin mukaan tehtävän luokittelun. Tämän luokittelun mukaan
ajanilmauksen sijainti voidaan jakaa neljään kategoriaan, joita nimitän
lyhenteillä S1--S4. Sijainnit ovat:

1. Sijainti ennen sekä subjektia että verbiä (S1)
2. Sijainti juuri ennen verbiä, mutta subjektin jäljessä. (S2)
3. Sijainti juuri verbin jälkeen, ennen objektia (S3)
4. Sijainti sekä verbin että sen (muiden) täydennysten jälkeen (S4)

Tarkemmalla syntaktisella tasolla on huomautettava, että mikäli lauseen
pääverbinä on etenkin suomessa tavallinen liittomuoto (kuten lauseessa *Aatos
on pitkään harrastanut jalkapalloa*), katsotaan ajanilmauksen sijainniksi S3,
jos se sijaitsee verbin persoonamuotoisen osan jälkeen -- suluissa annetun
lauseen sijainniksi määriteltäisiin siis S3.

Tutkimuksen rajauksesta SVO-tapauksiin seuraa se, että vaikka tutkimuksen 
tuloksina esitettävät konstruktiot ovat luultavasti useimmiten yleistettävissä
moniin erilaisiin lausetyyppeihin ja syntaktisiin rakenteisiin, esitän
ne tässä tutkimuksessa aina käyttämällä rakenteita, joissa  edellä esitetyn
matriisin 1 tavoin on läsnä subjekti, verbi, objekti tai
muu täydennys sekä adverbiaali. Jos kulloinkin käsiteltävän konstruktion 
kannalta ajanilmauksen sijainti on olennainen -- toisin sanoen, jos
ajanilmauksen sijaitseminen esimerkiksi  S3-asemassa S1-aseman sijaan tuo
mukanaan jotain olennaista eroa konstruktion merkitykseen tai käyttöön --
käytän konstruktioista nimityksiä *S1-konstruktio*, *S2-konstruktio* ja niin edelleen.
Sitä, miten usein ajanilmaukset tutkimuskirjallisuuden perusteella sijaitsevat
missäkin edellä määritellyistä asemissa, käsitellään tarkemmin 
seuraavissa kahdessa alaluvussa.

Ajanilmauksen tyypilliset sijainnit venäjässä {#konstr-ven}
---------------------------------

Kuten osiossa \ref{jotain} mainittiin, venäjän sanajärjestystutkimuksen
perusteos on Kovtunova [-@kovtunova1976], jonka perusteella kirjoitetussa
kielessä esiintyvälle ajanilmaukselle voidaan määrittää kaksi oletusasemaa. 

Ensinnäkin, mikäli ajanilmaus on tavallinen adverbi, oletussijainti on
kiinteästi aivan verbin edessä (mts. 166). Esimerkkinä tästä voidaan
tarkastella lauseita tutkimusaineiston ryhmistä L8a ja
E6a:




\ex.\label{ee_rulc8_oletus} \exfont \small Кто-то из нас скоро умрет (press\_ru)






\ex.\label{ee_ruex6_oletus} \exfont \small Я долго искал правильного партнера (press\_ru)





\vspace{0.4cm} 

\noindent

Toiseksi, on tavallista, että ajanilmaus sijaitsee koko lauseen ensimmäisenä.
Tämä on tilanne erityisesti siinä tapauksessa, että ajanilmaus toimii Kovtunovan
termein *determinanttina*. Determinantin käsite liittyy lauseen jakamiseen
*predikatiivilausekkeeksi* (предикативная группа) ja predikatiivilausekkeen ulkopuolisiksi
elementeiksi. Predikaattilausekkeen muodostavat subjekti ja finiittiverbi, ja
näiden muodostamaa kokonaisuutta määrittävät elementit[^tark] ovat determinantteja (mts: 61).
Määritelmä muistuttaa pitkälti edellä mainittua kehysadverbiaalin
käsitettä (ks. osio \ref{topiikin-ja-fokuksen-tarkempi-erittely}). Kovtunova
itse antaa seuraavan esimerkin determinanttina lauseen alussa toimivasta
ajanilmauksesta (ajanilmaus kursivoitu):




\ex.\label{ee_kovtunova_191} \exfont \small \emph{В раннем детстве} меня посещала странная грёза





\vspace{0.4cm} 

\noindent

Kahdesta oletussijainnista poikkeavat lauseasemat ovat Kovtunovan mukaan aina
seurausta joistakin lauseen informaatiorakenteessa tai Prahan koulukunnan termein
*funktionaalisessa lauseperspektiivissä* [ks. esim. @tobecited] tapahtuvista
muutoksista. Esimerkiksi sijainti lauseen lopussa on mahdollinen, jos
ajanilmaus täyttää reeman funktion [@kovtunova1976, 68] tai jos lause on jollain tapaa
tyylillisesti värittynyt ja korostaa determinanttia (mts. 106).

[^tark]: tarkenna määritelmää myös subjektittomiin lauseisiin ym.

Sijainti heti finiittiverbin jälkeen mutta ennen objektia on venäjässä
mahdollinen, mutta rajoittunut. Esimerkiksi Kallestinova & Slabakova 
[-@kallestinova08, 200] toteavat, ettei verbin ja objektin välinen sijainti ole täysin kieliopin
vastainen, mutta yhtä kaikki huomattavasti epätyypillisempi kuin verbinetinen
sijainti. Tähän huomioon palataan tarkemmin luvussa \ref{kesk-sis}.


<!-- Tähän lisää esim. Kingiltä? Ja Zimmerlingiltä? -->

Ajanilmauksen tyypilliset sijainnit suomessa
--------------------------------------------

\todo[inline]{Karlsson 1999?}

Maria Vilkunan [-@vilkuna1989; -@vilkuna1995] sanajärjestysesityksillä ei
välttämättä ole aivan yhtä käytettyä statusta suomen sanajärjestystutkimuksen
historiassa kuin Kovtunovalla venäjän osalta. Yhtä kaikki Vilkunan esitykset ovat
laajimpia ja kaiken kaikkiaan käyttökelpoisia lähtökohtia, kun mietitään
ajanilmauksille tavallisia sijainteja suomessa.

Vilkunan sanajärjestysteorian kulmakivi on lauseen jaottelu
diskurssifunktioihin (huomaa ero termiin niin kuin se on käytössä tässä
tutkimuksessa) eli K-, T- ja V-kenttiin [-@vilkuna1995, 244]. K-kenttään
sijoittuu usein (muttei aina) kontrastiivista aineista, T-kenttä kattaa lauseen
topikaalisen ytimen ja T:n jälkeinen aines kuuluu V-kenttään [@vilkuna1989, 38-39].
Vilkunan kuvaus on tarkoituksellisen lineaarinen, niin että diskurssifunktiot
ovat staattisia kenttiä, eivätkä mahdollisesti eri lauseasemin toteuttavia
informaatiorakenteellisia funktioita. Sekaannuksen välttämiseksi käytän
Vilkunan diskurssifunktioista vastedes erillistä termiä *diskurssikenttä*.
Huomattakoon, että Vilkunan kenttä-käsitteet ovat pohjana myös Ison suomen kieliopin 
tavalle kuvata sanajärjestystä (ks. esim. § 1369).

Diskurssikenttämallin hankaluus nyt käsillä olevassa tutkimusasetelmassa on,
ettei juuri adverbiaalien sijoittaminen malliin ole ongelmatonta, vaan usein --
joskaan ei aina --  esimerkiksi lauseenalkuinen ajanilmaus vaikuttaisi vaativan
kokonaan oman diskurssikenttänsä esitettyjen kolmen lisäksi [ks. alaviite
@vilkuna1989, 58]. Verrattuna venäjään  lauseenalkuinen ajanilmaus on suomessa
vaikeammin määriteltävä: se on myös suomessa melko tavallinen, mutta sen
funktioiden erottelu on usein monimutkaista. Shore [-@shore2008, 45] toteaakin
tämän tutkimuksen kannalta kiinnostavasti:

\begin{quote}%
-\/- näiden konstituenttien {[}lauseenalkuisten adverbiaalien, objektien
ym.{]} tekstuaalisten tehtävien perusteelliseksi ymmärtämiseksi
tarvittaisiin laajaan aineistoon ja eri tekstilajeihin perustuvaa
tutkimusta.
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Tutkimuksen luvut \ref{ajanilmaukset-ja-alkusijainti} ja \ref{kokoava-kontrastiivinen-analyysi}
pyrkivät ajanilmausten osalta vastaamaan tähän haasteeseen. Tavoitteena
on esittää lauseenalkuiset ajanilmaustapaukset niiden tekstuaalisten  -- tai paremminkin
ylipäätään *viestinnällisten* -- tehtävien ymmärtämiseksi osina erilaisia
S1-sijaintia hyödyntäviä konstruktioita, joilla kaikilla on oma käyttötarkoituksensa
ja -tilanteensa. Kuten luvussa \ref{ajanilmaukset-ja-alkusijainti} käy ilmi, suomessa
nämä käyttötilanteet ovat lopulta yllättävänkin rajattuja. Esimerkkinä voidaan
esittää virke \ref{ee_fi_lc6_alku}




\ex.\label{ee_fi_lc6_alku} \exfont \small Kosteikkovahvero on paljolti suppilovahveron näköinen ja myös
erinomainen ruokasieni, mutta joskus rustonupikka saattaa harhauttaa
kerääjää. (Araneum Finnicum: sienikoto.fi)





\vspace{0.4cm} 

\noindent
Esimerkin @ee_fi0a_alku *joskus*-ajanilmauksen voi Vilkunan mallissa katsoa kuuluvan
K-kenttään -- tämän tutkimuksen kehyksessä ajanilmaus on kuvailtavissa
diskurssifunktioltaan kontrastiiviseksi. Vastaavat tapaukset,
joissa rinnastetaan esimerkiksi jokin tavallisesti voimassa oleva asiaintila
ja siihen tietyissä tilanteissa liittyvät poikkeukset, tulkitaan
luvussa \ref{ei--deiktiset-adverbit} *kontrastiivinen konstruktio* -nimisen
rakenteen edustajiksi.


\todo[inline]{katso vertailun vuoksi tähän sulkala ja manninen SVOA:sta, ehkä myös hkv}

Tavallisin adverbiaalin ja sitä kautta ajanilmausten sijainti on
Vilkunan mukaan V-kentän sisällä: suoraan finiittiverbin jäljessä ennen verbin
muita täydennyksiä, ennen kaikkea objektia [-@vilkuna1989, 25, 40]. Tästä
esimerkkinä on seuraavassa esitetty lauseet \ref{ee_filc1_oletus} ja
\ref{ee_fiex1_oletus} tutkimusaineiston ryhmistä L5a ja E1b.




\ex.\label{ee_filc1_oletus} \exfont \small Jäsenmaksuja kertyi vuonna 1992 yhteensä 22700 mk. (Araneum Finnicum:
gamma.nic.fi)






\ex.\label{ee_fiex1_oletus} \exfont \small Albumi myi \emph{jo kolmessa päivässä} tuplaplatinaa. (Araneum Finnicum:
himosareena.fi)





\vspace{0.4cm} 

\noindent
Vaikka edellä on todettu sijainnin subjektin ja verbin välissä (T-kentän
lopussa ennen V-kenttää) olevan suomessa rajoitettu, sekin on mahdollinen
ja jopa tavallinen etenkin sivulauseissa [@vilkuna1989, 59]. Näin on myös
tutkimusaineiston ryhmää L6a edustavassa esimerkissä \ref{ee_filc5_sivul}:




\ex.\label{ee_filc5_sivul} \exfont \small Vapaa-aika nyt on jokaisen oma asia ja vaikka tuohon onkin kerätty ehkä
kaikkein tyrkyimmät tyypit, niin voisin kuvitella, että hekin
\emph{kymmenen vuoden päästä} häpeävät omaa käytöstään ja olemustaan
(Araneum Finnicum: forum.hevostalli.net)





\vspace{0.4cm} 

\noindent
Paitsi lauseenalkuinen, myös lauseenloppuinen sijainti vaikuttaa
lähtökohtaisesti olevan suomessa paikoin venäjää monitulkintaisempi. Samoin
kuin venäjässä, loppusijainti liittyy luonnollisesti tilanteisiin, joissa
ajanilmaus on fokuksessa. Kuitenkin suomessa on myös käyttötilanteita,
joissa lauseen lopussa sijaitseva ajanilmaus ei merkitse mitenkään erityisellä tavalla
uutta informaatiota [vrt. @vilkuna1989, 89, esimerkki 31] . Vastaavat lauseet, kuten esimerkki
\ref{ee_fi_lauseenlop} tutkimusaineiston ryhmästä L1b, ovat myös erityishuomion
kohteena tutkimuksen osiossa  \ref{deiktiset-adverbit-ja-positionaalisuus}.




\ex.\label{ee_fi_lauseenlop} \exfont \small ...ja teki oikein kovasti mieli listiä se vanha äijä, joka kopeloi minua
\emph{tänään.} (araneum\_fi)





\vspace{0.4cm} 

\noindent


Ajanilmaukset tutkimusaineistossa
=================================



Tutkimusaineistot
-----------------

Edellä luvussa \ref{kontr}
painotettiin Chestermanin [-@chesterman1998contrastive:
57--58] näkemystä, jonka  mukaan oleellista (funktionaalisessa)
kontrastiivisessa analyysissa on, että hypoteeseja voidaan testata empiirisellä
aineistolla. Kenties tavallisin empiirisen aineiston muoto niin kieli- kuin
käännöstieteissä ovat jo pitkään olleet erilaiset sähköiset tekstikokoelmat eli
korpukset, joiden voidaan jopa väittää mullistaneen sen, miten kieltä tutkitaan
[@mikhailovcooper, 16]. Kuten jo Fried & Östman [-@ostman2004, 24] toteavat 
[tuoreempana katsauksena ks. @gries2013data], korpusaineistojen
käyttö on ollut erityisen keskeisellä sijalla konstruktiokieliopin piirissä
tehtävässä tutkimuksessa.[^usgbsed]

[^usgbsed]: Äärimmilleen vietynä (laajoilla) korpusaineistoilla
tehtävät päätelmät ovat olleet käyttöperusteisessa kieliopissa (usage-based)
[ks. esim. @bybee]. 

Tehtäessä nimenomaan vertailevaa tutkimusta korpusten hankkimiseen ja käyttöön
liittyy tiettyjä erityispiirteitä. Kahta kieltä vertailtaessa on tavallisesti
turvauduttu joko *rinnakkaisiin* (*parallel*) tai *verrannollisiin*
(*comparable*) korpuksiin [@mikhailovcooper, 5]. Edelliset Mikhailov ja Cooper (mp.)
määrittelevät lyhyesti korpuksiksi, jotka sisältävät vähintään kaksi versiota 
samasta[^samat] tekstistä [termistä *parallel corpora* ks. @teubert1996]; jälkimmäisistä taas esimerkiksi Stig Johansson
[-@johansson2007, 9] tiivistää, että ne 
koostuvat erillisistä kahden- tai useammankielisistä alkuperäisteksteistä, jotka vastaavat
toisiaan tiettyjen kriteerien kuten genren tai julkaisuajankohdan osalta.

[^samat]: Kysymys tekstien samuudesta on luonnollisesti mutkikas, ks. [@chesterman1998contrastive,?]

Tässä tutkimuksessa keskitytään käyttämään kahta verrannollista kokonaisuutta, joista toinen --
Internet-tekstien verrannollinen korpus -- on alun perin verrannolliseksi koottu
ja toinen -- lehtitekstien verrannollinen korpus -- sisältää kaksi täysin
riippumatonta aineistolähdettä. Viittaan näihin kaikkiin verrannollisina korpuksina,
joskin verrannollisuus on tässä yhteydessä ymmärrettävä jossain määrin väljästi.

Syynä käytettävien korpusten väljään verrannollisuuteen on aineistojen
tavoitellussa koossa. Luonnollisesti voisi argumentoida, että mitä tiukempien
kriteerien perusteella tutkittavat tekstimassat valitaan, sitä paremmin
verrannollinen tuloksesta tulee -- jos palataan edellisessä luvussa pohdittuun
samanlaisuuden käsitteeseen, voidaan todeta, että jonkin hyvin kapeasti
määritellyn kentän sisällä samanlaisuuden aste on väistämättä korkea
[@chesterman1998contrastive, 11]. Tiukasti rajatun aineiston ongelma on
kuitenkin jo määritelmällisesti itse rajauksessa, sillä kapean rajauksen väistämättömänä
seurauksena mahdollisen tutkittavan tekstiaineksen määrä pienenee.
\todo[inline]{Korpusten kokoamisessa onkin puhuttu balansoiduista korpuksista jne.} 
Tämän tutkimuksen kannalta aineiston on kuitenkin oltava laaja ainakin
seuraavista syistä:

1. Vaikka ajanilmaukset sinänsä ovat melko frekventti ilmiö, pyrkimykseni on
   tarkastella mahdollisimman monia (luvussa 1 määritellyillä tavoilla)
   *erilaisia* ilmauksia. 
2. Edellisessä luvussa määritelty syntaktinen rajaus (myönteiset SVO-lauseet) ei
   ole mahdollinen, ellei aineistoa ole paljon.
3. Koska tavoitteena on tehdä kvantitatiivisia päätelmiä, ei riitä, että
   tutkittavana olisi vain joitakin yksittäisiä tapauksia, vaan mitä enemmän
   aineistoa saadaan, sitä varmempia päätelmiä pystytään tekemään ja sitä
   hienojakoisempien ilmiöiden vaikutuksen tarkastelu mahdollistuu (ks. osio 5.1).

Vaatimus aineiston laajuudesta oli myös tärkein yksittäinen tekijä, jonka perusteella
tutkimuksessa päädyttiin nimenomaan verrannollisiin eikä rinnakkaisiin
korpuksiin.[^ksgradu] Lisäksi verrannollisiin aineistoihin keskittyminen 
auttaa välttämään ne ongelmat, joita syntyy, kun käännöksiä
käytetään kieltenvälisten vertailun pohjana -- muun muassa 
von Waldenfelsin [-@waldenfels2012, 279] mainitsemat käännetyn kielen erityispiirteet[^ksuomeksi]
ja ekvivalenssin osittaisuuden (*partial equivalence*).

[^ksuomeksi]: Ilmiöstä suomen osalta katso [@ksuom].
[^ksgradu]:Puhtaasti rinnakkaisteksteihin perustuvana käsittelynä samasta
aiheesta katso kuitenkin [@gradu].

### Verrannollisten korpusten rakenne {#aineistot-vrn}

Tarpeeksi laajoja aineistoja etsittäessä luonnollinen lähestymiskulma on hakea
tekstimassoja Internetistä, jonka sisältämän aineiston määrä on käytännössä
loputon [ks. esim. @kilgarriff2003introduction, @kanerva2014syntactic].
Internet-aineiston käyttöön liittyy kuitenkin monia ongelmia. Internet-korpusten
ongelmia sanakirjojen kokoamisen kannalta tutkineet Tarp ja Fuertes-Oliveira
[-@tarp, 278] listaavat muun muassa seuraavat, myös yleisemmällä tasolla pätevät
vaikeudet:

- Tekstien laatua ja alkuperää ei pystytä kontrolloimaan
- Tekstit voivat olla koneellisesti tuotettuja
- Tekstit voivat olla peräisin henkilöltä, jonka kielitaito on puutteellinen

Lisäksi merkittävä ongelma on tekstien heterogeenisyys: genren määritteleminen
Internet-aineistojen tapauksessa on usein ongelmallista ja esimerkiksi pelkkä
"Internet-teksti" on lajityypin määritteenä miltei absurdi.[^klusteriviite]
Kaikki puutteet ja haitat huomioidenkin Internetin tarjoama laajuus itsessään on
nähdäkseni silti riittävän suuri hyöty, jotta Internet-aineistojen käyttöä
voidaan harkita. Tässä on loppujen lopuksi kyse ilmiöistä, jota
informaatiotutkimuksessa kutsutaan termeillä *precision* ja *recall* --
vakiintuneiden suomennosten puuttuessa käytän tässä nimityksiä *tarkkuus* ja
*kattavuus*. Encyclopedia of Machine Learning -käsikirjan [-@encyclopmachine,
781] määritelmää soveltaen esitän nämä käsitteet seuraavanalaisina suhteina:


\vspace{0.3cm}

\noindent \emph{Kattavuus} = kaikki tutkittavan ilmiön kannalta olennaiset esiintymät, jotka
tutkija onnistuu erottamaan aineistosta / kaikki tutkittavan ilmiön kannalta
olennaiset esiintymät, jotka aineisto sisältää

\vspace{0.3cm}

\noindent \emph{Tarkkuus} = kaikki tutkittavan ilmiön kannalta olennaiset esiintymät, jotka
tutkija onnistuu erottamaan aineistosta / kaikki esiintymät, jotka tutkija
erottaa aineistosta

\vspace{0.3cm}

[^klusteriviite]: Internet-korpusten genreistys on kuitenkin mahdollista.
Mielenkiintoisena lähestymistapana ks. esim. Laippala ja kumppanit x.

Voidaan argumentoida, että tiukasti rajattu, genreltään ja ajankohdaltaan hyvin
samanlaisia tekstejä sisältävä verrannollinen kokonaisuus saavuttaisi hyvän
tarkkuuden, mutta auttamattomasti liian pienen kattavuuden. Väljempi rajaus
puolestaan varmistaa hyvän kattavuuden: mukana on paljon epämääräistä ainesta
(karkeammin ilmaistuna "roskaa" / engl. *noise*), mutta toisaalta tarpeeksi suuren
koon voi nähdä aiheuttavan sen, että marginaalisten tekstien vaikutus
kokonaiskuvaan jää pieneksi. Tutkimukseen valittiin kaksi erityyppistä
verrannollista kokonaisuutta -- Internet-aineistojen lisäksi lehtitekstit --
juuri sen takia, että tällöin voidaan selkeämmin erottaa tapaukset, joissa on
kyse ennemmin Internet-aineiston ongelmallisuuden aiheuttamasta anomaliasta kuin
suomeen tai venäjään todella liittyvästä piirteestä tai ominaisuudesta.

Tutkimukseen valituissa Internet-aineistoissa on se hyvä puoli, että ne on alun
perin koottu vertailevaa käyttöä varten. Valitut Internet-aineistot ovat osa
Vladimir Benkon [-@benko2014] kokoamaa Aranea-projektia[^araneaurl], joka kattaa noin
kaksikymmentä erikielistä korpusta, jotka tekijöiden mukaan ovat verrannollisia
seuraavista kolmesta syystä (mts. 247):

- Korpukset on kerätty Internetistä suurin piirtein samaan aikaan
- Korpuksia koottaessa on pyritty siihen, että kukin korpus sisältäisi saman
  sekoituksen erilaisia Internetissä tavattavia tekstityyppejä, genrejä ja rekistereitä
- Korpukset ovat samankokoisia

Toinen Benkon mainitsema verrannollisuuden aspekti vaatii selvennystä:
todellisuudessa Aranea-korpusten tiedonkeruuprosessiin ei varsinaisesti ole
kuulunut vaihetta, jossa valittujen tekstien genrejä olisi pyritty jollakin
tavalla lajittelemaan tai karsimaan. Tekstimassat Aranea-korpuksiin on kerätty
hyödyntämällä BootCAT-nimistä sovellusta, joka toimii siten, että hakukoneeseen
(tässä tapauksessa Google) syötetään tiettyjen avainsanojen satunnaisia yhdistelmiä, ja
hakutuloksien perusteella saatujen sivujen osoitteet tallennetaan
jatkokäsittelyä varten [@baroni2004bootcat, 2]. Aranea-korpusten lähtökohtana
toimivat avainsanalistat on kunkin korpuksen osalta valittu samoista[^samadiscl]
asiakirjoista: Ihmisoikeuksien julistuksesta, Johanneksen evankeliumin
ensimmäisestä luvusta sekä *polkupyörä*- ja *rakkaus*-sanojen
Wikipedia-artikkeleista [@benko2014, 249]. Korpukset ovat siis samanrakenteisia
tässä hyvin löyhässä mielessä: niiden *siemeninä* on käytetty 
sanoja, jotka ovat peräisin oletettavasti samankaltaisista tekstilähteistä.

[^samadiscl]: Sana *sama* ei tässä kohden toki ole korrekti, vaan tarkkaan
ottaen mainittujen tekstien "samuus" on hyvin erilaista eikä missään nimessä
tarkoita identiteetin yhtenevyyttä.

Tarkkaan ottaen tässä tutkimuksessa hyödynnettävät Internet-korpukset ovat
Araneum Finnicum (Aranea-projektin suomenkielisistä aineistoista koostuva korpus) ja 
Araneum Russicum (Aranea-projektin venäjänkielisistä aineistoista koostuva
korpus). Molemmista korpuksista käytetään Maius-versioita eli suurempia, noin
miljardin saneen kokoisia korpuksia[^maiusminus]. Finnicum- ja
Russicum-korpusten koosta kertovat tilastot on esitetty 
taulukossa 2:


-------------------------------------------------------
&nbsp;            Araneum Finnicum   Araneum Russicum  
----------------- ------------------ ------------------
Saneet            1 200 000 100      1 200 000 258     

sanat             816 931 276        859 319 823       

virkkeet          88 733 910         71 616 173        

kappaleet         33 218 052         29 510 308        

lähdeasiakirjat   2 279 618          1 826 514         
-------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 2}: Suomen ja venäjän Araneum-korpusten koko \normalfont \vspace{0.6cm}

[^araneaurl]: http://ella.juls.savba.sk/aranea/, viitattu 24.4.2017
[^maiusminus]: Aranea-korpuksista on saatavilla erikseen Minus-versiot (noin
sanetta) ja suuremmat Maius-versiot (noin miljardi sanetta). Maius-versioiden
käyttö edellyttää rekisteröitymistä.



Taulukosta 2 havaitaan konkreettisesti, että vaikka
korpuksia rakennettaessa on pyritty likimain samaan kokoon, tämä ei ole aivan
toteutunut. "Sama koko" on selvästikin tarkoittanut sitä, että sanemäärät ovat
yhteneviä. Eri kielten vertailu sanemäärien avulla on kuitenkin kielten
rakenteellisten erilaisuuksien takia ongelmallista  [vrt. @harme17, 36]. Suomea ja venäjää verrattaessa
on otettava huomioon ainakin [@tobecited]

1. yhdistämisen tavallisuus suomen sananmuodostuskeinona
2. suffiksien ja liitepartikkeleiden suuri määrä suomessa
3. prepositioiden tavallisuus venäjässä

Mainitut kolme seikkaa vaikuttavat siihen, että jos jokin teksti sisältää
esimerkiksi 1000 sanaa venäjässä, on siinä oletettavasti vähemmän informaatiota
(todellista "pituutta") kuin suomenkielisessä 1000-sanaisessa tekstissä.
Yksinkertaistavana indikaattorina erosta voidaan tarkastella edellä mainittuja
ihmisoikeuksien julistusta ja Johanneksen evankeliumia. Ihmisoikeuksien
julistuksen suomenkielinen käännös sisältää 1 246 ja
venäjänkielinen 1 563 sanetta; koko Johanneksen evankeliumin
suomenkielinen käännös sisältää 14 101 ja venäjänkielinen 
15 161 sanetta.[^johstats] 

[^johstats]: Laskettuna suomessa vuoden 1992 käännöksestä ja venäjässä vuoden
2003 Радостная весть -käännöksestä.

Kun otetaan huomioon suomen ja venäjän väliset rakenteelliset erot, on
todettava, että Araneum Finnicum on suurempi korpus kuin Araneum Russicum. Tämä
oletus vahvistuu, kun tutkitaan taulukkoa 2 muiden
ominaisuuksien kuin sanemäärien osalta. Suomenkielinen aineisto sisältää suurin
piirtein yhtä monta sanetta mutta peräti 
17 117 737 virkettä
enemmän kuin venäjänkielinen. Lukuihin ei voi suhtautua täysin varauksetta,
sillä korpukset eivät tarjoa tarkkaa tietoa tavasta, jolla määrät on laskettu,
ja vaikka laskelmat olisivatkin tarkkoja, ei virkemääräkään ole pituusmittana
ongelmaton. 

Tässä tutkimuksessa hyödynnettävien Internet-aineistojen yksi merkittävä etu
tutkittaessa sanajärjestykseen ja informaatiorakenteeseen liittyviä kysymyksiä
on siinä, että ne tarjoavat helpomman pääsyn yksittäisten esimerkkien laajempaan kontekstiin. 
Ensinnäkin, koska aineistot ovat alun perinkin sijainneet vapaasti saatavilla
Internetissä, ne on voitu sisällyttää Araneum-korpuksiin siten, että 
hakutuloksia on mahdollista tarkastella suoraan yhtä virkettä laajempina
konteksteina. Toiseksi hakutulokset sisältävät suoran linkin itse 
alkuperäistekstiin, niin että mikäli teksti on tutkimuksen tekohetkellä
yhä olemassa alkuperäisellä palvelimellaan, on sen tarkasteleminen 
alkuperäisessä julkaisuympäristössä nopeaa ja helppoa.

Internet-aineiston vastapainona käytän aineistoja, joita nimitän
lehtiaineistoiksi, vaikka kyseessä eivät sinänsä ole puhtaasti sanoma- ja
aikakauslehdistä koostuvat tekstikokoelmat, vaan joukkoon mahtuu myös
esimerkiksi Internetissä julkaistuja uutistoimistojen tekstejä. Oleellista
näissä vastapainoksi valituissa korpuksissa on, että on, että siinä missä
Aranea-korpukset edustavat jäsentämätöntä ja heterogeenistä tekstimassaa, tässä
lehtikorpuksina käsitellyt aineistot edustavat ammattimaisesti tuotettuja
tekstejä, joiden alkuperä ja lajityyppi ovat hyvin tiedossa [Internet-korpuksen ja
perinteisemmän korpuksen eroista venäjän tapauksessa ks. @kutuzov2015comparing].

Suomenkielinen lehtitekstiaineisto koostettiin kahdesta Kielipankin
(www.kielipankki.fi) aineistokokonaisuuksiin kuuluvasta korpuksesta: Suomen
kielen tekstikokoelmasta (http://urn.fi/urn:nbn:fi:lb-201403268; vastedes: SKT) sekä
Kansalliskirjaston sanoma- ja aikakauslehtikokoelmasta
(http://urn.fi/urn:nbn:fi:lb-201405276; vastedes: KLK).[^viitatutkp]
KLK-korpus kattaa materiaalia aina 1770-luvulta asti, mutta koska tavoitteenani
ei ole tehdä diakronisen tason havaintoja, korpuksesta erotettiin käyttöön
ainoastaan vuonna 1990 ilmestyneet ja sitä tuoreemmat aineistot. KLK:sta valittu
otos koostuu pääasiassa Länsi-Savo-lehden arkistoista[^klktark]; SKT:n
tekstilähteet ovat moninaisemmat, joskin siitä on rajattu pois eräät koko
SKT:hen kuuluvat kirja-aineistot, niin että tässä käytetty SKT koostuu
sanomalehdistä (mm. Helsingin sanomat, Aamulehti, Turun sanomat)
ja aikakauslehdistä (mm. Tekniikan maailma, Suomen kuvalehti).[^skttark]

[^klktark]: Tarkka listaus KLK-aineiston lehdistä saatavissa
https://www.kielipankki.fi/wp-content/uploads/klk-lehdet-fi.pdf (tarkistettu
25.4.2017)

[^skttark]: Tarkka listaus SKT-aineiston lehdistä saatavissa
 https://kitwiki.csc.fi/twiki/bin/view/FinCLARIN/KielipankkiAineistotFtc (tarkistettu 25.4.2017)

[^viitatutkp]: URL-osoitteet tarkistettu 25.4.2017

Venäjänkielinen lehtitekstiaineisto on peräisin venäjän kansalliskorpukseen
(www.ruscorpora.ru) kuuluvasta lehdistökorpuksesta. Lehdistökorpuksen kuvaus on
saatavilla osoitteessa http://ruscorpora.ru/corpora-structure.html (tarkistettu
25.4.2017), jossa todetaan, että korpus koostuu 2000-luvulla julkaistuista
teksteistä -- paitsi painetuista lehdistä kuten Izvestija, Sovetski sport ja
Komsomolskaja pravda, myös uutistoimistojen, kuten RIA novosti, tarjoamista
sähköisistä materiaaleista. Kansalliskorpuksen lehdistökorpuksen aineisto
otettiin kokonaisuudessaan mukaan tutkimukseen.

Viittaan jatkossa suomen- ja venäjänkielisiin lehdistökorpuksiin lyhenteillä
FiPress ja RuPress. Kuten annetuista kuvauksista käy ilmi, FiPress koostuu
vanhemmasta materiaalista kuin RuPress: edellinen kattaa pääosin 1990-luvulla,
jälkimmäinen taas 2000-luvulla julkaistuja tekstejä. Koska tutkimuskohteena ovat
syntaksin eivätkä esimerkiksi sanaston tason ilmiöt, en näe ajankohtaeroa
erityisen haitallisena aineistojen vertailukelpoisuuden kannalta. Myös
lehdistökorpuksista on kuitenkin todettava, että suomenkielinen aineisto on
venäjänkielistä laajempi: Siinä missä RuPress-korpus kattaa 16 669 748 virkettä
ja 228 521 421 sanetta, kattaa FiPress-korpus 27 622 211 virkettä ja noin 294
460 000 sanetta.[^kokoinfot]

[^kokoinfot]: Informaatio korpusten koista on saatu suomen osalta
Korp-käyttöliittymän kautta (korp.csc.fi) ja venäjän osalta lehdistökorpuksen
hakutulossivulta. (tarkistettu 25.4.2017)

Lehtiaineistojen huono puoli verrattuna Internet-aineistoihin on siinä, että
laajemman kontekstin saaminen esimerkkeihin on usein huomattavasti vaikeampaa,
sillä Lehdistöaineistojen käyttöoikeudet eivät mahdollista laajempien
kontekstien tarkastelua suoraan korpuskäyttöliittymien kautta. Pääosin melko
tuoreista julkaisuista koostuva venäjänkielinen alkuperäisaineisto on tästä
huolimatta monissa tapauksissa saatavilla digitaalisesti lehtien
Internet-sivuilta, mutta koska suomenkielinen lehtikorpus koostuu uusimmillaankin
vuonna 2000 julkaistusta materiaalista, on laajempien kontekstien saamiseksi
turvauduttava lähinnä kirjastojen mikrofilmikokoelmiin.

Lehtiteksteistä on vielä huomautettava, että aivan samaan tapaan kuin 
käännösten on esitetty edustavan omaa kielimuotoaan, käännöskieltä (*translationese*),
myös lehtitekstit voi nähdä lehdistökielen (*journalese*) edustajina. Gries [-@gries2009, 7]
korostaa tämän seikan huomioimisen tärkeyttä: on tiedostettava, että
lehtitekstejä tutkimalla saadaan informaatiota ennen kaikkea siitä melko
spesifistä rekisteristä ja tyylistä, jota ne edustavat. 
Tutkimuksen Internet-aineistojen roolina on kompensoida -- siinä määrin kuin ne itse
eivät ole lehdistötekstejä -- tätä erityispiirrettä. 


### Tutkittavien ajanilmaustapausten erottaminen verrannollisista korpuksista

Edellä kuvattujen neljän korpuksen rooli tutkimuksen kannalta on ollut toimia
eräänlaisena työstämättömänä raaka-aineena -- lähtökohtana, jonka pohjalta
varsinainen tutkimusaineisto on jalostettu. Jalostamisella tarkoitan tässä
yhteydessä karkeasti ottaen sitä, että korpuksista erotettiin tutkittavaksi
sellaiset ajanilmauksen sisältävät virkkeet, jotka noudattavat osiossa \ref{taks4}
määriteltyä syntaktista rajausta. Tarkoituksena ei ollut tutkia kaikkia
mahdollisia ajanilmauksia, vaan ennemminkin valita laaja joukko erilaisia
ajanilmausryhmiä mahdollisimman hyvin edustavia yksittäisiä
esimerkki-ilmauksia. Käsiteltävien ajanilmausten laaja-alaisuus pyrittiin
takaamaan valitsemalla tarkasteltavat esimerkki-ilmaukset siten, että ne
edustaisivat mahdollisimman monia eri haaroja luvussa
\ref{ajanilmausten-kolme-taksonomiaa} esitetyistä taksonomioista.

Esimerkki-ilmauksia valittaessa ensimmäinen lähtökohta olivat osiossa \ref{taksn2}
määritellyt semanttiset funktiot, niin että mukaan pyrittiin ottamaan niin
L-funktiota, E-funktiota kuin F-funktiota edustavia ilmauksia. Edelleen eri
semanttisten funktioiden sisältä pyrittiin poimimaan kunkin funktion eri
alalajeja edustavia ilmauksia, niin että mukaan tulisi paitsi *simultaanista*
(kuten *viime viikolla*), myös esimerkiksi *sekventiaalista* (kuten *sodan
jälkeen*) L-funktiota edustavia tapauksia. Lisäksi esimerkki-ilmausten
valinnassa pyrittiin huomioimaan osiossa \ref{taksn1} mainitut eri muodostustavat, niin
että aineisto käsittäisi paitsi kalendaarisia (kuten *maanantaina*), myös
adverbisiä (*usein*) ja aikaa indefiniittisesti kvantifioivia (*vähän aikaa*)
ilmauksia. Valintaprosessin lopputuloksena saatiin 
50 *aineistoryhmää*,
jotka on tarkemmin esitelty alla osioissa \ref{l1a-l1b-l1c}--\ref{lm1}.
Osa valituista aineistoryhmistä
koostuu vain yhdestä yksittäisestä ilmauksesta: esimerkiksi ryhmään L6b
kuuluvat suomen ilmaus *sodan jälkeen* sekä venäjän ilmaus *после войны*.
Jotkin ryhmistä taas kattavat useampia samankaltaisia ilmauksia, kuten ryhmä
F1a, joka koostuu suomen ilmauksista *joka päivä*, *joka viikko*, *joka
kuukausi* ja *joka vuosi* sekä venäjän ilmauksista *каждый день*, *каждую
неделю*, *каждый месяц*  ja *каждый год*.

Varsinaisen tutkimusaineiston kokoamisen **ensimmäinen vaihe** oli suorittaa
jokaisen aineistoryhmän osalta konkordanssihaut [ks. esim. @mikhailovcooper,
48] kaikista neljästä tutkimuskorpuksesta. Tämä vaihe oli toisten
aineistoryhmien kohdalla -- erityisesti merkitykseltään yksiselitteisten
adverbien kuten *tavallisesti*/*обычно* -- yksinkertainen, toisten kohdalla --
esimerkiksi ryhmä E1a eli ilmaukset tyyppiä *kaksi tuntia* tai *kolme vuotta*
-- taas monimutkaisempi. Konkordanssihaut muodostettiin korpuskäyttöliittymän
niin salliessa hyödyntämällä erityisiä kyselykieliä -- FiPress-korpuksen
tapauksessa CQP-kieltä [@evert2010ims] ja Araneum-korpusten tapauksessa
CQL-kieltä [@jakubicek2010fast].[^nkrjahuom] Alla on esimerkki CQP-kyselystä,
jota käytettiin L9a-aineistoryhmän suomenkielisten ilmausten konkordanssien
hakemiseksi:

    [word="vuonna" %c]
    [word="199."]
    [msd != ".*AgPcp.*" 
        & msd != ".*CASE_(G|I|A|P|E|T|K).*PCP_PrfPrc.*" 
        & msd != ".*PCP_PrsPrc.*"]

Ajatuksena tässä kuvatun kaltaisissa kyselyissä on, että tulosten joukosta
suodatetaan pois mahdollisimman suuri määrä tapauksia, joita ei haluta
tarkastella. Yllä olevassa esimerkissä kolmas hakasulkein erotettu kokonaisuus
pyrkii karsimaan tuloksista pois ajanilmausta seuraavat partisiippimuodot, jotta
esimerkiksi sellaiset tapaukset kuin *vuonna 1994 perustettu yhtiö* eivät
sisältyisi hakutuloksiin. 

[^nkrjahuom]: Venäjän kansalliskorpus ei tue kyselykieliä, mutta tarjoaa
graafisen käyttöliittymän, jonka avulla päästään lähes samoihin tuloksiin. Myös
Aranea-korpukset ja Korp-käyttöliittymä sisältävät graafiset vaihtoehdot
kyselyjen tekemiselle.

Vaikka jo konkordanssihaun yhteydessä pyrittiin kohtalaiseen hakutarkkuuteen,
varsinainen tarkempi suodattaminen tapahtui tutkimuksen **toisessa vaiheessa**,
jossa saatujen konkordanssilistojen perusteella muodostettiin ikään kuin uudet
neljä korpusta. Nämä neljä jalostettua korpusta -- joita kutsun myös myös tämän
vaiheen jälkeen nimillä FiPress, RuPress, Araneum Finnicum ja Araneum Russicum
-- eivät enää käsittäneet satoja miljoonia saneita, vaan ennemminkin kymmeniä
tuhansia konteksteja, joita kaikkia yhdistää se, että ne sisälsivät vähintään
yhden luvussa \ref{aryhmat} määriteltävän ajanilmauksen. 

Oleellinen seikka tutkimuksen toisessa vaiheessa muodostetuista jalostetuista
korpuksista on se, että jalostettuja korpuksia muodostettaessa kaikki
ensimmäisessä vaiheessa mukaan suodatetut kontekstit annotoitiin koneellisesti.[^annotsel]
Kaikki tekstit sisälsivät alun perinkin vähintään morfologisen annotoinnin --
toisin sanoen teksteihin oli jo alun perin lisätty koneellisesti saatu
informaatio esimerkiksi kunkin nominin suvusta, luvusta ja sijamuodosta. Tutkimuksen toisen vaiheen
korpuksia muodostettaessa kaikki kontekstit kuitenkin annotoitiin uudelleen,
niin että lopputuloksena kaikki suomenkieliset aineistot noudattivat omaa
yhteneväistä annotointikaavaansa ja kaikki venäjänkieliset omaansa.

[^annotsel]: Annotoinnilla viitataan tässä yhteydessä karkeasti ottaen siihen,
että tekstien sisältämiin sanoihin merkitään niiden ominaisuuksia. Tarkemmin
kirjallisissa korpuksissa yleensä käytettävistä erilaisista annotoinneista ks.
[@gries2017linguistic, 383-385].

Morfologisen analyysin lisäksi tutkittaville konteksteille tehtiin myös
syntaktinen analyysi eli aineistot jäsennettiin dependenssijäsentimellä. Niin
suomen kuin venäjän tapauksessa hyödynnettiin vapaasti saatavilla olevia
ohjelmistoja, suomenkielisen aineistojen osalta Turun yliopistossa kehitettyä
jäsennintä [@haverinen2013tdt][^fdeplink]  ja venäjänkielisen aineistojen osalta
Serge Sharoffin ja Joakim Nivren MALT-ohjelmistoa (http://maltparser.org)
hyödyntävää jäsennintä [@sharoffnivre]. Koska dependenssijäsentimet eivät voi
toimia ilman morfologista analyysiä, molempiin jäsentimiin sisältyvät 
sanojen lemmatisoinnista ja morfologisesta annotoinnista vastaavat komponentit,
joita luonnollisesti myös hyödynsin -- toisin sanoen, vaikka kaikki tekstit oli
jo alkuperäisissä korpuksissa morfologisesti annotoitu, tutkimuksen toiseen vaiheeseen 
otetut kontekstit annotoitiin uudestaan niillä työkaluilla, joita käytetyt
dependenssijäsentimet hyödyntävät.

[^fdeplink]: Jäsentimen lataaminen ja asennusohjeet:
http://turkunlp.github.io/Finnish-dep-parser/ (tarkistettu 26.4.2017)

Kahden kielen vertailu koneellisesti annotoitujen aineistojen perusteella on
lähtökohtaisesti ongelmallista, sillä eri jäsentimet toimivat usein jossain määrin eri
periaatteiden mukaisesti ja tuottavat myös eri käsitteistöön nojaavia
lopputuloksia. Yhtenä kiinnostavana ratkaisuna ongelmaan voidaan nähdä Universal
Dependencies -projekti (http://universaldependencies.org)[^udtark], jonka tarkoituksena
on kehittää yli kielirajojen sovellettavia annotaatiomalleja: standardoidut
merkinnät sanaluokasta, morfologisista ominaisuuksista, syntaktisista suhteista
ja periaatteista, joiden mukaan syntaktisia rakenteita analysoidaan
[@luotolahti2015towards, 211]. Tutkimusaineistoa kerättäessä suomi oli yksi
projektissa täysipainoisesti mukana olevista kielistä, mutta venäjään
sovellettavia työkaluja ei vielä ollut saatavilla, minkä vuoksi 
projektin tuloksia ei varsinaisesti ole hyödynnetty tässä tutkimuksessa. On
kuitenkin todettava, että tässä käytetty suomen kielen jäsentimen
annotaatiomalli noudattaa Universal dependecies 1.0 -standardia. Lause \ref{ee_depex}
antaa esimerkin siitä, miltä suomen kielen jäsentimen tuottama annotointi
näyttää. Lauseessa \ref{ee_depex_ru}  puolestaan on esimerkki venäjän jäsentimen
tuottamasta lopputuloksesta.




\ex.\label{ee_depex} \exfont \small 





\vspace{0.4cm} 

\noindent


\scriptsize

\begin{dependency}
\begin{deptext}
Venäjä \& aikoo \& huomenna \& pyytää \& valuuttarahastolta \& 4 \& miljardin \& dollarin \& lainaa.  \\
\end{deptext}

\depedge{4}{1}{nsubj}
\depedge{4}{2}{aux}
\depedge{4}{3}{advmod}
\depedge{4}{5}{nmod}
\depedge{7}{6}{compound}
\depedge{8}{7}{nummod}
\depedge{9}{8}{nmod:poss}
\depedge{4}{9}{dobj}
\end{dependency}


\normalsize






\ex.\label{ee_depex_ru} \exfont \small 





\vspace{0.4cm} 

\noindent


\scriptsize

\begin{dependency}
\begin{deptext}
Я \& завтра \& собрался \& подводить \& очередные \& промежуточные \& итоги \& конкурса.  \\
\end{deptext}

\depedge{3}{1}{предик}
\depedge{3}{2}{обст}
\depedge{3}{4}{1-компл}
\depedge{7}{5}{опред}
\depedge{7}{6}{опред}
\depedge{4}{7}{1-компл}
\depedge{7}{8}{квазиагент}
\end{dependency}




\normalsize

[^udtark]: Tarkistettu 26.4.2017

Koneellista syntaksin analyysia hyödynnettiin siten, että sen avulla
tutkimuksen **kolmanteen vaiheeseen** suodatettiin mukaan ainoastaan lauseet,
jotka täyttivät seuraavat ehdot (vrt. osio \ref{taks4}):

- Ehto 1: Lauseessa on oltava (nominatiivimuotoinen) subjekti, finiittiverbi ja
  objekti
- Ehto 2: Subjektin, verbin ja  objektin keskinäinen järjestys on oltava
  SVO.
- Ehto 3: Ajanilmauksen on esiinnyttävä itsenäisenä, suoraan
  finiittiverbistä riippuvana lauseen elementtinä. Lisäksi on
  pystyttävä toteamaan, että myös subjekti ja objekti ovat kyseisen verbin dependenttejä.


Ehdon 2 tarkastelu koneellisen analyysin perusteella oli melko suoraviivaista:
kun lauseesta oli eroteltu subjekti, objekti, verbi ja ajanilmaus ja varmistettu
että subjekti, objekti ja ajanilmaus ovat verbin dependenttejä, voitiin niiden
järjestys määrittää yksinkertaisesti vertaamalla jäsentimen kullekin sanalle
antamia järjestysnumeroita (esimerkissä \ref{ee_depex} sanalla *Venäjä* on
järjestysnumero 1, sanalla *aikoo* numero 2 ja niin edelleen). 

Ehtoihin 1 ja 3 liittyy enemmän hankaluuksia, joista
ensimmäinen koskee lauseen objektin tunnistamista. Kuten esimerkeistä \ref{ee_depex}
ja \ref{ee_depex_ru} havaitaan, jäsentimet merkitsevät jokaiselle analysoitavan
virkkeen sanalle *dependenssiroolin*. Subjektin määritteleminen tällaisen
dependenssiroolin perusteella on yksinkertaista: suomen jäsennin merkitsee
nominatiivisubjekteille dependenssiroolin *nsubj*, venäjänkielinen puolestaan
*предик*. Esimerkissä \ref{ee_depex} siis subjektiksi saadaan siis substantiivi *Venäjä*,
esimerkissä \ref{ee_depex_ru} pronomini *я*.

Objektin määritteleminen on kuitenkin yksi niistä seikoista, jonka osalta
käytetyt jäsentimet eroavat toisistaan. Suomen jäsennin sisältää koko lailla
selkeän dependenssiroolin *dobj*, mutta venäjän jäsentimellä vastaavaa
kategoriaa ei ole. Sen sijaan venäjän jäsennin erottaa viisi eri tasoista
luokkaa erilaisille verbin (tai esimerkiksi substantiivin)
täydennyksille:[^termhuomtayd] *1-kompl*, *2-kompl*, *3-kompl*, *4-kompl* ja
*5-kompl* [tämän merkintätavan juurista ks. @Melchuk1988]. Tästä erosta johtuen
olen tarkoituksella ymmärtänyt objektin käsitteen venäjän osalta jossain
määrin suomea laajemmin. Venäjänkielisessä aineistossa "objekteiksi" on luettu
kaikki sellaiset *1-kompl*-dependenssiroolin saavat sanat jotka

a)  eivät ole jäsentimen analyysin mukaan adpositioita, verbejä, konjuktioita,
  partikkeleita tai adverbejä
b) riippuvat (eräin poikkeuksin) suoraan samasta finiittiverbistä kuin ajanilmaus ja subjektikin

[^termhuomtayd]: Tähän huomio terminologiasta: täydennys / argumentti / yms.

Tämä tarkoittaa, että mukaan otettiin paitsi esimerkissä \ref{ee_depex_ru} *1-kompl*-roolin saanut 
puhtaan akkusatiivimuotoinen[^nomkalt] sana *итоги*, myös esimerkin \ref{ee_pokupkatovara} *покупкой*:





\ex.\label{ee_pokupkatovara} \exfont \small 





\vspace{0.4cm} 

\noindent


\scriptsize
\begin{dependency}
\begin{deptext}
Я \& давно \& занимался \& покупкой \& различного \& товара \& в \& заграничных \& магазинах \& через \& интернет \\
\end{deptext}

\depedge{3}{1}{предик}
\depedge{3}{2}{обст}
\depedge{3}{4}{1-компл}
\depedge{6}{5}{опред}
\depedge{4}{6}{1-компл}
\depedge{3}{7}{обст}
\depedge{9}{8}{опред}
\depedge{7}{9}{предл}
\depedge{9}{10}{атриб}
\depedge{10}{11}{предл}
\end{dependency}
\normalsize


On kuitenkin huomattava, että esimerkissä \ref{ee_pokupkatovara} myös sana *товара*
on analysoitu roolilla *1-kompl*. Tätä sanaa ei kuitenkin sellaisenaan laskettaisi
objektiksi, koska se ei ole finiittiverbin vaan substantiivin dependentti. 

Kysymys *1-kompl*-merkinnällä annotoidun sanan pääsanasta vaatii vielä
toisenkin tarkennuksen, joka liittyy edellä esitettyyn ehtoon 3.
Esimerkissä \ref{ee_depex_ru} sana *итоги* ei nimittäin riipu samasta
finiittiverbistä kuin ajanilmaus *завтра*. Itse asiassa *итоги* ei riipu
finiittiverbistä lainkaan, vaan infiniittimuodosta *подводить*. Näissä
tapauksissa lauseiden suodatussääntöjä tarkennettiin siten, että jos
ajanilmauksen pääverbin dependettinä 

a) ei ole objektia mutta 
b) on infinitiivimuotoinen verbi, jolla puolestaan on objekti (kuten esimerkissä \ref{ee_depex_ru})

infinitiivimuotoisen verbin objekti katsottiin lauseen objektiksi, vaikka sillä
oli eri pääsana kuin ajanilmauksella ja vaikka pääsana oli infiniittinen. Tämä
tarkennus koskee vain venäjänkielisiä lauseita, sillä kuten
esimerkistä \ref{ee_depex} havaitaan, suomen jäsennin analysoi joka tapauksessa niin
subjektin, objektin kuin ajanilmauksen riippuvaksi samasta verbistä. Suomen
jäsentimeen liittyy kuitenkin toinen tähdennys: kuten esimerkistä \ref{ee_depex}
nähdään, apuverbitapauksissa ajanilmauksen, subjektin ja objektin *kaikkien*
pääverbiksi on analysoitu infinitiivimuoto, jonka dependentiksi myös lauseen
finiittiverbi on luettu. Näissä tapauksissa toimittiin siten, että mikäli     

a) ajanilmaus on infinitiivimuotoisen verbin dependentti ja
b) saman infinitiivimuotoisen verbin dependenttinä on finiittiverbi

lause otettiin mukaan tutkimukseen.

Ehdosta 3 on vielä todettava, että sen avulla pyrittiin karsimaan pois
tapaukset, joissa ajanilmaus on esimerkiksi partisiippimuodon dependentti,
kuten lauseessa *Eilen järjestetyn tapahtuman osallistujat olivat kotoisin
kymmenestä eri maasta* (vrt. myös edellä esitelty CQL-lauseke, jolla pyrittiin
samaan päämäärään).

\todo[inline]{Lukuja siitä, kuinka paljon karsiutui pois?}

[^nomkalt]: joskin tässä tapauksessa akkusatiivi on nominatiivin kaltainen

\todo[inline]{Maininta siitä, jos subjektina venäjässä verbi?}

\todo[inline]{Johonkin väliin: kirjoitetun aineiston erityispiirteet esim. prosodian suhteen}

\todo[inline]{ 
- kirjoitetusta kielestä
- määristä ja eroista
- nojatuoli vs. empiria: luku 6 kompromissina? 
- JOTAIN pythonista?? Vai Kolmoslukuun?
}



Aineistoryhmät {#aryhmat}
--------------

Tutkimusaineiston perusyksiköksi muodostuivat siis edellä kuvatun prosessin perusteella
aineistoryhmät, joiden tarkoituksena on mahdollisimman monipuolisesti
edustaa suomen- ja venäjänkielisissä teksteissä esiintyviä ajanilmauksia.
Tässä osiossa kukin näistä aineistoryhmistä käydään lyhyesti läpi listaamalla
ne taksonomioiden 1--3 mukaiset ominaisuudet, joita kunkin ryhmän on tarkoitus
edustaa. Lisäksi tarvittaessa kerrotaan niistä haasteista, joita ryhmään kuuluvien
ilmausten erotteleminen aineistosta asetti ja jotka mahdollisesti vaikuttivat
siihen otokseen, joka lopulta päätyi osaksi varsinaista tutkimusaineistoa.

Aineistoryhmät on luokiteltu seuraavassa kolmen semanttisten funktioiden yläluokan
mukaan siten, että osio \ref{lokalisoiva-funktio} kattaa L-funktiota, osio
\ref{ekstensiota-ilmaiseva-funktio} E-funktiota, osio 
\ref{taajuutta-ilmaiseva-funktio} F-funktiota sekä 
osio \ref{muut-funktiot} muita semanttisia funktioita edustavat tapaukset.
Aineistoryhmät on nimetty niin, että nimen alussa oleva kirjain tai
kirjainyhdistelmä (L, E, F, LM tai P) kertoo ylemmän tason semanttisen funktion.
Samaa ylemmän tason semanttista funktiota edustavat ryhmät on erotettu toisistaan
numeroin (esim. L3 ja E4), ja lisäksi ryhmät, jotka joko merkitykseltään tai ominaisuuksiltaan
ovat lähellä toisiaan, on yhdistetty saman numeron alle ja erotettu toisistaan kirjaimilla (esimerkiksi
E1a, E1b ja E1c).


### Lokalisoiva funktio

L-funktiota kuvattiin luvussa  \ref{lokalisoiva-semanttinen-funktio} yleisimmäksi semanttiseksi funktioksi. Tästä
johtuen se on myös tutkimusaineistossa laajimmin edustettuna:
tutkimusaineistoon valikoitui kaikkiaan 23
L-funktiota edustavaa aineistoryhmää.

#### L1a, L1b, L1c


| Ryhmän nimi   | suomi            | venäjä             |
| ------------- | ---------------- | ------------------ |
| L1a           | eilen            | вчера              |
| L1b           | tänään           | сегодня            |
| L1c           | huomenna         | завтра             |

Nämä kolme aineistoryhmää kattavat ne suomen ja venäjän ilmaukset, joilla viitataan
puhehetkeä ympäröiviin päiviin. Tarkemmalta semanttiselta funktioltaan
kaikki ryhmät voitaisiin luokitella simultaanisen funktion edustajiksi.
Muodostustavan kannalta mietittynä kyseessä ovat adverbit, joiden voi lisäksi todeta 
olevan referentiaalisia: niiden viittauspisteenä on puhehetki.

L1a--L1c-ryhmien ilmaisuja etsittäessä aineistosta pyrittiin hakemaan ainoastaan itsenäisesti
aikaa ilmaisevat tapaukset. Tämä tarkoittaa, että esimerkiksi ilmaukset *eilen
viideltä, eilen aamulla* ja *eilen kello kaksi* on jätetty haun ulkopuolelle.

#### L2a, L2b

| Ryhmän nimi | suomi                                 | venäjä                           | 
|-------------| ------------------------------------- | -------------------------------- | 
| L2a         | tiistaina, keskiviikkona, torstaina   | во вторник, в среду, в четверг   | 
| L2b         | maaliskuussa, lokakuussa              | в марте, в октябре               | 

Ryhmät L2a ja L2b edustavat myös simultaanisia ja  referentiaalisia
ilmaisuja, joskin se, miten ne aikajanalle ankkuroidaan, vaihtelee. Toisaalta esimerkiksi *tiistaina*
saattaa kytkeytyä puhe- tai kirjoitushetkeen, jos puhujan tai kirjoittajan
tarkoituksena on viitata tätä hetkeä lähimpään (joko menneeseen tai tulevaan) tiistaipäivään.
Toisaalta kyseessä voi olla jonkin aiemmassa diskurssissa määritellyn viikon tiistai. L2-aineistoryhmien
oleellinen ominaisuus onkin, että ne ovat lähtökohtaisesti luvussa
\ref{taksn1} määritellyllä tavalla positionaalisia eli nimeävät tietyn jäsenen vakiintuneesta,
toistuvaa ajanjaksoa kuvaavasta ajanilmausten joukosta (viikosta tai vuodesta).
Ryhmä L2b kattaa ekstensioltaan selkeästi laajempia ajanjaksoja kuin ryhmä L2a.


Myös näiden ryhmien tapauksessa hakutuloksista pyrittiin karsimaan pois
epäitsenäiset tapaukset kuten *tiistaina aamulla*. Huomaa, että niin kuin
yllä olevasta taulukosta käy ilmi, mukaan ei valittu kaikkia mahdollisia viikonpäiviä
ja kuukausia, vaan ainoastaan yllä luetellut kolme päivää ja kaksi kuukautta.

L2-ryhmät ovat molemmissa kielissä nominaalisia.[^nomhuom]

[^nomhuom]: Lasken tässä nominaalisiksi yhtä lailla suomen pelkästä sijamuodossa taivutetusta
nominista kuin venäjän prepositiosta ja nominista koostuvat tapaukset erotukseksi adverbisistä
tapauksista.



#### L3


| Ryhmän nimi   | suomi                                    | venäjä                                         |
| ------------- | ---------------------------------------- | ---------------------------------------------- |
| L3            | kahdelta, kolmelta ... kahdeltatoista    | в два часа, в три часа... в двенадцать часов   |

Ryhmä L3 sisältää tasatuntisiin kellonaikoihin viittaavat ilmaisut. Nämäkin
ovat luonteeltaan simultaanisia ilmauksia, mutta toisin kuin edelliset ryhmät,
ne viittaavat punktuaalisesti ajalliseen viitepisteeseensä eivätkä kehyksisesti
koko tiettyyn ajanjaksoon. Samalla ilmaukset eivät ankkuroi referenttiään suhteessa puhehetkeen, 
vaan ennemminkin suoraan absoluuttiselle aikajanalle. Kolme pistettä yllä olevassa taulukossa
merkitsee sitä, että mukana ovat kaikki luvut kahden ja kahdentoista väliltä (yksi on jätetty
pois sen takia, että venäjässä kello yhteen viitataan tavallisesti ilman numeroa, pelkästään
sanalla *час*, 'tunti').

Hakutuloksista on karsittu ilmaukset tyyppiä *kahdelta aamulla, kahdelta illalla*.


#### L4a, L4b


| Ryhmän nimi   | suomi                          | venäjä                                   |
| ------------- | ------------------------------ | ---------------------------------------- |
| L4a           | aamulla, illalla               | утром, вечером                           |
| L4b           | yhtenä aamuna/päivänä/iltana   | однажды утром / вечером / днём           |

Ryhmä L4a on samantapainen kuin L1-ryhmät siinä, että kyseessä ovat
(tavallisesti) puhehetkeen ankkuroidut simultaaniset ilmaukset, jotka lokalisoivat
tapahtumia kehyksisesti tietyn ajanjakson sisään. Lisäksi L4a on L2-ryhmien ohella
yksi tutkimusaineiston positionaalisista aineistoryhmistä, sikäli kuin aamu ja ilta
nähdään päivää jaottelevina ajanyksiköinä.[^apip]
Ryhmä L4b on puolestaan valittu tutkimukseen mahdollisena esimerkkinä ajallisesti
ankkuroimattomasta (ei-referentiaalisesta) ilmauksesta. 
 Ilmausten morfologisesta
rakenteesta on syytä huomauttaa, että venäjän *утром* ja *вечером* on tapana
analysoida leksikaalistuneiksi adverbeiksi, vaikka ne onkin alun perin muodostettu instrumentaalin
sijapäätteellä substantiiveista утро ja вечер [@akademitsheskaja, 398].
Kieltenvälisen vertailun helpottamiseksi tulkitsen tästä huolimatta myös venäjän L4-ilmaukset
nominaalisiksi, mitä puoltaa sekin, että aineistossa on myös selvästi ei--leksikaalistuneita
tapauksia kuten luvussa \ref{deiktiset-adverbit-ja-positionaalisuus} annettu esimerkki
\ref{ee_legkimutrom} osoittaa.

[^apip]: Tarkemmin päivän jaottelusta suomessa ja venäjässä ks. @harme17.

Myös ryhmän L4a ilmauksissa on pyritty itsenäisyyteen karsimalla esiintymät
tyyppiä *aamulla viideltä* ja *eilen aamulla*.


#### L5a, L5b, L5c {#l5a-l5b}

<!--  

- Epäkohta: suomen monikot! 
- Mahdollinen ratkaisu: poista R:ssä.

-->


| Ryhmän nimi   | suomi                | venäjä                            |
|---------------|----------------------|-----------------------------------|
| L5a           | viime vuonna         | в прошлом году                    |
| L5b           | viime viikolla       | на прошлой неделе, в прошлом году |
| L5c           | hiljattain/äskettäin | недавно                           |

Myös L5-ryhmät edustavat simultaanista L-funktiota. Adjektiivi *viime* ankkuroi
a- ja b-ryhmien ilmaukset puhehetkeen, ja myös c-ryhmän voi tulkita puhehetkeen
ankkuroiduksi. A- ja b-ryhmien ajallinen ekstensio on moniin edellisiin ryhmiin verrattuna
melko laaja, c-ryhmän osalta taas poikkeuksellinen epämääräinen. C-ryhmä on morfologiselta rakenteeltaan
adverbi, muut ovat nominaalisia ryhmiä.

#### L6a, L6b

| Ryhmän nimi | suomi                                                   | venäjä                                    |
|-------------| ------------------------------------------------------- | ----------------------------------------- |
| L6a         | kahden,kolmen... päivän/viikon/vuoden kuluttua/päästä   | через/спустя два,три... дней/недель/лет   |
| L6b         | sodan jälkeen                                           | после войны                               |

Ryhmät L6a ja L6b edustavat sekventiaalista L-funktiota. L6a on
referentiaalinen ja se voi olla ankkuroitu joko puhehetkeen tai ajalliseen
topiikkiin (TT). L6b on myös referentiaalinen, mutta ankkuroituu eri tavalla:
suhteessa objektiivisen aikajanan pisteeseen. Kaikki näiden ryhmien ilmaukset
ovat nominaalisia.

#### L7a, L7b, L7c, L7d

| Ryhmän nimi | suomi                                                       | venäjä                            |
|-------------| ----------------------------------------------------------- | --------------------------------- |
| L7a         | siitä asti/lähtien/pitäen                                   | с тех пор                         |
| L7b         | siihen aikaan                                               | в то время                        |
| L7c         | silloin                                                     | тогда                             |
| L7d         | sellaisena hetkenä/hetkinä, sellaisella hetkellä/hetkillä   | в такой момент, в такие моменты   |

\todo[inline]{
varmista, että anaforisuutta sinänsä käsitelty jossain, vaikka ref ei oma muuttujansa
}

L7-ryhmät sisältävät monta taksonomian 1 perusteella indefiniittisesti aikaa
kvantifioivaksi luokiteltavaa ilmausta. L7a on L-funktioltaan
sekventiaalis-duratiivinen, muut kolme ryhmää simultaanisia. Ryhmistä L7b, L7c
ja L7d vain L7b viittaa selkeästi ajanjaksoon eikä yhteen pisteeseen
(kehyksinen vs. punktuaalinen -jaottelu). L7c voi viitata niin yksittäiseen
hetkeen kuin laajempaankin ajanjaksoon.

L7-ryhmät ovat erityisiä nimenomaan referentiaalisuutensa osalta. Ryhmien
L7a-L7c viitepistettä voi pitää anaforisena, edeltävässä diskurssissa
perustettuna. L7d-ryhmän tapaukset taas ovat esimerkkejä ankkuroimattomista,
ei-referentiaalisista ajanilmauksista. *Silloin*- ja *тогда*-sanat ovat morfologiselta
rakenteeltaan adverbejä, muutoin kyse on nominaalisista ryhmistä.


#### L8a, L8b, L8c


| Ryhmän nimi   | suomi              | venäjä   |
| ------------- | ------------------ | -------- |
| L8a           | pian               | скоро    |
| L8b           | nykyisin/nykyään   | теперь   |
| L8c           | nyt                | сейчас   |

L8-aineistoryhmät koostuvat lyhyistä puhehetkeen ankkuroituvista ja tarkemmalta semanttiselta funktioltaan
simultaanisista adverbeista.
Jako L8b- ja L8c-ryhmien välillä on tehty venäjän tähden, sillä теперь- ja
сейчас-sanoilla on tunnetusti erityislaatuinen työnjako (ks. 
luku \ref{l7l8-erot-kesk}). Erityisesti L8c mutta
venäjässä myös L8b voi lokalisoida paitsi kehyksisesti, myös punktuaalisesti.


#### L9a, L9b, L9c, L9d


| Ryhmän nimi   | suomi                                               | venäjä                                       | tarkempi sem. funktio      |
| ------------- | --------------------------------------------------- | -------------------------------------------- | -------------------------  |
| L9a           | vuonna 1990...1999                                  | в 1990...1999 году                           | simultaaninen, kehyksinen  |
| L9b           | Tämän / viime  vuoden tammikuusta ... joulukuusta   | с января ... декабря этого / прошлого года   | sekventiaalis-duratiivinen |
| L9c           | vuoden 1990...1999 tammikuusta ... joulukuusta      | с января ... декабря 1990..1999 года         | sekventiaalis-duratiivinen |
| L9d           | vuodesta 1900...1999                                | с 1900...1999 года                           | sekventiaalis-duratiivinen |

L9-ryhmät sisältävät vuosiin ankkuroituja ajanilmauksia. Ryhmät L9b-L9d ovat
sekventiaalis-duratiivisia, L9a kehyksisesti lokalisoiva ja simultaaninen.
Kaikkia ryhmiä yhdistää se, että niiden voi nähdä ankkuroituvan absoluuttiselle
aikajanalle sillä tarkennuksella, että L9b on attribuuttiensa myötä ankkuroitu
puhehetkeen. Toisaalta myös muissa ryhmissä puhehetki on implisiittisesti mukana,
sillä asiaintilan voi katsoa olevan voimassa vielä puhehetkellä. Ryhmissä
L9b--L9d tutkittavien tapausten ulkopuolelle onkin tarkoituksella jätetty
ilmaukset, joissa myös ajanjakson loppupää on ilmaistu (vuodesta 1900 vuoteen
1910). Suomessa mukaan on otettu niin postposition *asti* tai *lähtien* sisältävät ilmaisut kuin
ilmaisut ilman postpositiota. Kaikki L9-tapaukset ovat nominaalisia.


### Ekstensiota ilmaiseva funktio

Siirrytään nyt tarkastelemaan ekstensiota ilmaisevia aineistoryhmiä, joita on kaikkiaan
14 kappaletta.

#### E1a, E1b, E1c

<!-- venäjän несколько ym. lisäksi минута >> poista R:ssä? -->

| Ryhmän nimi | suomi                                                                 | venäjä                                        | 
|-------------|-----------------------------------------------------------------------|-----------------------------------------------|
| E1a         | kaksi...kymmenen päivää/vuotta/viikkoa/tuntia                         | два...десять дней, лет, недель, часов         | 
| E1b         | kahdessa...kymmenessä päivässä/vuodessa/viikossa/tunnissa             | за два...десять дней, лет, недель, часов      | 
| E1c         | kahdeksi...kymmeneksi kuukaudeksi/viikoksi/päiväksi/vuodeksi/tunniksi | на два...десять месяцев/недель/дней/лет/часов | 

E1-ryhmien ilmaukset sisältävät lukusanan ja kalendaarisen ajanyksikön. E1a
edustaa duratiivista, E1b varsinaista resultatiivista (erotuksena kehyksisestä
resultatiivisesta kuten E3-ryhmät alla) ja E1c teelistä
E-funktiota. Kuten E-funktiolle on tavallista, näistä yksikään ei ole referentiaalinen ilmaus.

Erityisesti E1a-ilmausten etsiminen aineistosta oli haastavaa, sillä numero +
ajanyksikkö -yhdistelmää haettaessa tarkkuus on väistämättä huono. Selkein
hakutuloksissa ohitettava tapausjoukko olivat ne venäjän tapaukset, joissa
etsityn ilmauksen jäljessä oli tai niitä edelsi adpositio (esimerkiksi *назад*,
*спустя* tai *через*).

Kaikki E1-ryhmät ovat morfologiselta rakenteeltaan nominaalisia.

\todo[inline]{
Lisää käytetyistä tarkkuutta parantavista rajoituksista
Pääverbeistä karsittu pois дать, получить
}


#### E2a, E2b

| Ryhmän nimi | suomi              | venäjä              |
|-------------| ------------------ | ------------------- |
| E2a         | viikon             | неделю              |
| E2b         | koko viikon        | всю неделю          |

E2-ryhmien välinen ero on referentiaalisuudessa. Siinä missä E2a ei viittaa
mihinkään tiettyyn jaksoon aikajanalla, ankkuroi E2b-ilmauksen attribuutti
ilmauksen vähintäänkin puhujan subjektiiviselle aikajanalla, jossain mielessä
myös absoluuttiselle, ajatellen että kyseessä on joka tapauksessa ollut
kaikille ihmisille sama, tietty viikko. Kummatkin ryhmät edustavat duratiivista E-funktiota. 

Ajanyksikkönä *viikko* valikoitui näihin ryhmiin, koska venäjän *неделя* on
feminiinisukuinen ja sitä kautta sen akkusatiivimuodot on helppo erottaa
aineistosta. Kaiken kaikkiaan kyseessä ovat kuitenkin myös E2-ryhmien
tapauksessa ilmaisut, joita haettaessa tarkkuus ei ole erityisen hyvä. Suomessa
tarkkuutta on parannettu poistamalla ilmausta seuraavat *ajan, mittaan,
jälkeen* ym.; venäjässä ennen kaikkea poistamalla adpositio *назад*. Samaten
ryhmää E2a haettaessa oli luonnollisesti poistettava ryhmään E2b kuuluvat
tapaukset sekä muut referentiaalisuutta implikoivat attribuutit kuten
*viime/ensi*. Molemmat ryhmät ovat nominaalisia.



#### E3a, E3b {#e3a-e3b}

| Ryhmän nimi | suomi                                                                | venäjä                                                                     |
|-------------| -------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| E3a         | vuoden/viikon/kuukauden/kevään...talven kuluessa                     | в течение годя/месяца/недели/весны...зимы                                  |
| E3b         | viime/koko/sen/tuon vuoden/viikon/kuukauden/kevään...talven aikana   | в течение этого/того/прошлого/последнего годя/месяца/недели/весны...зимы   |

E3-ryhmiin on koottu kehyksisiä resultatiivisen ekstension ilmauksia eli jonkin
ajanjakson mittaiselle ajalle sijoitettavia tapahtumia. Koska nämä
ilmaukset ovat melko harvinaisia, ajanyksiköinä on käytetty mahdollisimman
laajaa eri kalendaaristen ilmausten joukkoa. E3a ei ole referentiaalinen, mutta
E3b-tapauksissa attribuutit ankkuroivat ilmauksen joko puhujan subjektiiviselle
aikajanalle tai suhteessa puhehetkeen. Suomessa ryhmää E3a varten on käytetty
*aikana*-adposition sijaan adpositiota *kuluessa*, millä on pyritty parantamaan
E3a-ryhmän hakutulosten tarkkuutta: tuloksena on varmemmin ei-referentiaalisia
ilmauksia kuin mitä *aikana*-sanan avulla olisi saatu. E3-ryhmät ovat nominaalisia.



#### E4

| Ryhmän nimi | suomi        | venäjä       |
|-------------|--------------|--------------|
| E4          | siinä ajassa | за это время |

E4-ryhmä käsittää yhden aikaa indefiiniittisesti kvantifioivan ilmauksen.
E4-ryhmä ilmaisee varsinaista resultatiivista ekstensiota, mutta ryhmän
referentiaalisuus ei ole selkeästi
määriteltävissä. Voisi ajatella, että *siinä* ja *это* ankkuroivat ilmauksen
tiettyyn, keskustelussa aiemmin muodostettuun ajalliseen referenttiin, vaikka
toisaalta ne voivat myös vain viitata tiettyyn aiemmin mainittuun
mittayksikköön. Kummankinkieliset ilmaukset ovat nominaalisia.



#### E5a, E5b

| Ryhmän nimi | suomi              | venäjä                      | 
|-------------| ------------------ | -------------------         | 
| E5a         | hetkessä           | в/во/за мгновенье/мгновение | 
| E5b         | äkkiä, yhtäkkiä    | вдруг                       | 

E5-ryhmät edustavat lähtökohtaisesti varsinaista resultatiivista ekstensiota,
joskin kummassakaan tapauksessa -- etenkään E5b-ryhmän kohdalla -- tämä ei
läheskään aina ole yksiselitteistä: kuten luvussa
\ref{varsinainen-resultatiivinen-ekstensio} todetaan, E5b-ryhmän sanoilla on
etenkin venäjässä usein jopa ei-temporaalinen diskurssipartikkelin funktio.
E5a-ryhmä on aineistossa merkittävämpi suomen kuin venäjän kannalta, sillä
suomessa *hetkessä*-ilmaukset ovat verrattain yleisiä. E5b-ilmaukset ovat
(temporaalisesti käytettynä) merkityksensä puolesta lähellä E5a-ilmauksia,
mutta ilmentävät tiettyä punktuaalista luonnetta. 

E5a-ryhmä on aineistossa analysoitu nominaaliseksi, E5b-ryhmä taas adverbiseksi.

#### E6a, E6b, E6c

| Ryhmän nimi   | suomi                                     | venäjä                         |
|---------------|-------------------------------------------|--------------------------------|
| E6a           | kauan                                     | долго                          |
| E6b           | pitkään aikaan, pitkiin aikoihin, pitkään | давно                          |
| E6c           | jonkin aikaa                              | какое-то время, некоторе время |


E6-ryhmän ilmaukset edustavat kaikki duratiivista E-funktiota.
Venäjässä E6a ja E6b ovat puhtaasti adverbejä, suomessa E6b voidaan muodostaa paitsi adverbisesti,
myös *aika*-sanan avulla. Suomen E6b-ilmausten konkordanssihakujen tuloksia olikin 
siistittävä tarkkuuden varmistamiseksi: lukuun ottamatta *aika*-sanaa E6b-ryhmästä karsittiin
pois kaikki sellaiset tapaukset, joissa *pitkään* olisi syntaktisessa analyysissa luokiteltu
jonkin substantiivin dependentiksi. E6-ryhmien ilmauksilla ei lähtökohtaisesti
ole viittauspisteitä, joskin E6c-ilmausten voi nähdä sisältävän jonkinlaisen vihjeen
käsiteltävänä olevan ajanjakson esittämisestä tiettynä, eroteltavissa olevana aikajanan lohkona.

#### E7

| Ryhmän nimi |  suomi    | venäjä   |
|-------------|-----------|----------|
| E7          |  nopeasti | быстро   |

Ryhmään E7 kuuluvat ajanilmaukset määriteltiin luvussa
\ref{ekstensiota-ilmaiseva-semanttinen-funktio} aikaa ja tapaa
ilmaiseviksi: niiden ajallisuus on jossain määrin tulkinnanvaraista. Nämä
ilmaukset toimivatkin hyvänä rajatapauksena, jonka avulla voidaan tarkastella
ajanilmausten sijainnin suhdetta tavanilmausten sijaintiin. Luonnollisesti
kyseessä ovat morfologiselta rakenteeltaan adverbiset ilmaukset.


### Taajuutta ilmaiseva funktio

F-funktiota edustaa tutkimusaineistossa 10 aineistoryhmää.

#### F1a, F1b

| Ryhmän nimi | suomi                              | venäjä                          | 
|-------------| ---------------------------------- | ------------------------------  | 
| F1a         | joka päivä/viikko/kuukausi/vuosi   | каждый день/неделю/месяц/год    | 
| F1b         | maanantaisin...sunnuntaisin        | по понедельникам...воскресеньям | 

F-funktiota edustavien ilmausten referentiaalisuus on hankalasti
määriteltävissä. Lähtökohtaisesti niillä ei ole ajallista viitepistettä, mutta
toisaalta ne ankkuroivat, etenkin ryhmässä F1b, tapahtumia keskustelijoiden
tajunnassa tietyille kohdin toistuvia ajallisia syklejä. F1-ryhmät ovat nominaalisia.
F1a-ryhmässä huomionarvoista on se, että kyseessä on L2a-, L2b- ja L4a-ryhmien
tavoin positionaalinen aineistoryhmä. Koska *maanantaisin*- ja *по понедельникам* -tyyppiset
ilmaukset ovat aineistossa huomattavasti harvinaisempia kuin L2a-ryhmän viikonpäivätapaukset,
otettiin F1b-ryhmään mukaan kaikki seitsemän viikonpäivää. F-aineistojen ryhmiä
ei tutkimuksessa käytettävässä tilastollisessa mallissa (ks. seuraava luku) ole erikseen jaoteltu
alaluokkiin, mutta luvussa \ref{taajuutta-ilmaiseva-semanttinen-funktio}
esitetyn luokittelun perusteella voidaan todeta, että tässä esitellyt ryhmät
edustavat kalendaarisia F-funktiotapauksia.



#### F2a, F2b

| Ryhmän nimi | suomi                              | venäjä                         |
|-------------| ---------------------------------- | ------------------------------ |
| F2a         | kaksi....kymmenen kertaa           | два...десять раз               |
| F2b         | kahdesti, kolmesti                 | дважды, трижды                 |

F2-ryhmistä ensimmäinen pitää sisällään nominaalisia tapauksia ja toinen adverbejä.
Tarkemmassa F-funktion jaottelussa ryhmien voisi luokitella
edustavan numeerista F-funktiota. Näillä ilmauksilla ei ole referenttejä.


#### F3a, F3b, F3c, F3d, F3e

| Ryhmän nimi | suomi                              | venäjä                         | 
|-------------| ---------------------------------- | ------------------------------ | 
| F3a         | usein                              | часто                          | 
| F3b         | aina                               | всегда                         | 
| F3c         | harvoin                            | редко                          | 
| F3d         | joskus                             | иногда                         | 
| F3e         | tavallisesti, yleensä              | обычно                         | 

F3-ryhmät ovat joukko adverbejä, joista F3a-b korostavat yleisyyttä, F3c
harvuutta, F3d on neutraali ja F3e alaryhmältään epämääräistä taajuutta
ilmaiseva. Tämän luokan ilmaukset ovat koko aineiston frekventeimpiä. Niillä ei 
lähtökohtaisesti ole viittauspisteitä aikajanalla.


#### F4

| Ryhmän nimi | suomi                              | venäjä                         |
|-------------| ---------------------------------- | ------------------------------ |
| F4          | ajoittain                          | временами                      |

F4-ryhmä edustaa aikaa indefiniittisesti kvantifioivia F-funktiotapauksia.
Merkitykseltään se on lähellä ryhmää F3d ja myös se on aineistossa tulkittu
adverbiryhmäksi, joskin tässä tapauksessa leksikaalistuneen ilmauksen pohjalla
oleva nomini ja siihen liittyvät sijapäätteet ovat kummassakin kielessä
yhä hyvin näkyvissä.


### Muut funktiot

L-, E- ja F-ajanilmausten lisäksi tutkimusaineistoon on kerätty pieni edustus
myös edellä presuppositionaaliseksi ja likimääräiseksi määritellyistä
semanttisista funktioista.

#### P1, P2

| Ryhmän nimi | suomi   | venäjä   |
|-------------| ------- | -------- |
| P1          | jo      | уже      |
| P2          | vielä   | еще      |

Presuppositionaalista funktiota edustavat aineistossa suomen *jo*- ja *vielä*- 
sekä venäjän *уже*- ja *еще*-sanat, jotka luonnollisesti ovat morfologiselta
rakenteeltaan adverbejä. Kummassakin kielessä näihin sanoihin liittyy ymmärrettävästi
paljon tapauksia, joiden tulkitseminen ajallisiksi on usein ongelmallisia. Lähtökohtaisesti
aineistoa on suodatettu niin, että mukaan on otettu ainoastaan itsenäiset tapaukset,
ei siis esimerkiksi ilmausta *jo eilen*.

#### LM1

| suomi                                               | venäjä                      |
| --------------------------------------------------- | --------------------------- |
| viiden...kahdentoista maissa/tienoilla/hujakoilla   | около двух...двенадцати     |

Likimääräisyyttä edustavaan aineistoryhmään valittiin kummastakin kielestä
ilmaukset, joissa likimääräisyys saadaan aikaan adposition avulla Venäjästä tarkastelun
kohteeksi otettiin *около*-prepositio, suomesta joukko postpositioita.
Venäjänkielistä aineistoa oli suodatettava siten, ettei mukaan tule E-funktiota
ilmaisevia tapauksia kuten *он играл на скрипке около двух часов* ('hän soitti viulua noin kaksi tuntia').


Yleiskatsaus sijainneista tutkimusaineiston perusteella {#sij-yleisk}
-------------------------------------------------------


Luon luvun  \ref{ajanilmaukset-tutkimusaineistossa} lopuksi alustavan
katsauksen siihen, miten ajanilmausten neljä sijaintia suomen- ja
venäjänkielisessä tutkimusaineistossa jakautuvat.
Jos tarkastellaan kaikkia aineistoryhmiä yhdessä ja tutkitaan ainoastaan
suhteellisia osuuksia, saadaan taulukon 3 mukainen
prosentuaalinen jakauma:

\todo[inline]{Mukaan myös absoluuttiset luvut?}


---------------------------------------
&nbsp;   S1      S2      S3     S4     
-------- ------- ------- ------ -------
fi       11.31   5.57    47.6   35.52  

ru       36.79   49.13   4.89   9.19   
---------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 3}: Yleiskuva prosentuaalisista jakaumista \normalfont \vspace{0.6cm}

Taulukko 3 antaa karkean yleiskuvan kielten välisestä
erosta. Oletusten mukaisesti sijainnit S1 ja S2 ovat yleisimmät venäjässä, ja
sijainti S3 on yleisin suomessa. Kontrasti tutkittavien kielten välillä on
kuitenkin yllättävänkin selvä. Suomi ja venäjä näyttävät ajanilmausten
sijoittumisen valossa suorastaan toistensa peilikuvilta: venäjässä painottuvat
sijainnit ennen verbiä, suomessa sijainnit verbin jälkeen. Jo taulukon
3 tarkastelu osoittaa, että venäjän ja suomen välillä todella
on selvä systemaattinen ero siinä, miten ajanilmaukset lauseessa sijoittuvat.
Ero on jyrkempi kuin pelkästään tutkimuskirjallisuuden tai intuition
perusteella saattoi olettaa ja ulottuu paitsi ennalta oletettavissa olleisiin
S2- ja S3-sijainteihin, myös muihin.

\todo[inline]{T.kys.ollenkaan?}

Ehkä yllättävin havainto koskee eroa S1-sijainnin osuudessa. Kuvio
3 esittää S1-sijainnin suhteellisen yleisyyden eri
aineistoryhmien välillä:

![\noindent \headingfont \small \textbf{Kuvio 3}: S1-sijainnin suhteellinen yleisyys aineistoryhmissä \normalfont](figure/boxplots1-1.pdf)

Kuvion 3 perusteella S1 on kauttaaltaan yleisempi sijainti
venäjässä kuin suomessa -- suurimmassa osassa venäjänkielisiä aineistoryhmiä S1-sijainnin osuus
asettuu suurin piirtein 20 ja 50 prosentin välille, kun taas  suomessa valtaosa
ryhmistä asettuu 5 ja 20 prosentin välille. Havainnosta tekee yllättävän se,
ettei pelkän tutkimuskirjallisuuden tai intuition perusteella nouse yhtä
selkeää ajatusta siitä, mikä S1-sijainteja voisi tarkastelluissa kielissä
erottaa. Päinvastoin, tutkimuskirjallisuuden pohjalta oli syytä olettaa, että
S1-sijoittuneen ajanilmauksen sisältävät konstruktiot (S1-konstruktiot) ovat
yhtä lailla tavallisia niin venäjässä kuin suomessa. Esimerkiksi Hakulinen ym. [-@hkv1980, 123]  
toteavat oman kvantitatiivisen tutkimuksensa yhteydessä, että suomessa ajan adverbiaalit ovat
esimerkiksi paikkaan verrattuna selvemmin "kasaantuneet alkuasemaan". 





Tilastollinen malli  {#tilastollinenmalli}
============================================



## Bayesiläinen päättely tilastollisen analyysin lähtökohtana

Kvantitatiivisten tutkimustulosten tulkinnassa ovat viime vuosikymmeninä
voimakkaasti yleistyneet niin kutsutut bayesiläiset tilastomenetelmät [@andrews2013, 2]. 
Bayesiläinen tulokulma on tavallisesti nähty vastakohtana perinteiselle
eli frekventistiselle tilastolliselle päättelylle, joka oli hallitseva
lähestymistapa koko 1900-luvun ajan (mts. 5). 

Frekventistisessä päättelyssä keskeisellä sijalla on sen toteaminen,
voidaanko niin kutsuttu nollahypoteesi hylätä vai ei. Yhden käytetyimmistä bayesiläisen päättelyn oppaista
kirjoittanut John Kruschke[^kruschkeopas] [-@kruschke2014doing, 298] käyttää tämän
vuoksi frekventistisestä metodiikasta termiä Null hypothesis significance
testing (NHST). 

[^kruschkeopas]: Onko hyvä heittää tällaista määrettä? Saisiko jostain lähdettä
Kruschken käytettyydelle?

Nollahypoteesimenetelmien (jatkossa NHST-menetelmät) olennainen rajoitus on,
että ne todella antavat mahdollisuuden ainoastaan hypoteesin kumoamiseen, eivät
vahvistamiseen. Andrewsin  ja Baguleyn [-@andrews2013, 3] mukaan juuri ajatus
*käänteisen todennäköisyyden* mittaamisesta oli 1700-luvulla eläneen Thomas
Bayesin ja 1800-luvulla eläneen Pierre-Simon Laplacen tärkein saavutus
tilastotieteen edistymisen kannalta. Ajatus käänteisestä todennäköisyydestä on
pohjimmiltaan yksinkertainen: jos NHST-menetelmien mukaisen merkitsevyystestin
perusteella voidaan osoittaa viiden prosentin todennäköisyys sille, että
kymmenen kertaa heitetty kolikko on epäreilu (tuottaa esimerkiksi kruunia
useammin kuin klaavoja), tutkija ei silti voi todeta, että todennäköisyys
kolikon reiluudelle olisi 95%. Bayesiläisissä menetelmissä etuna on se, että
tutkija pystyy suoraan arvioimaan sitä, kuinka varma hän on kolikon reiluudesta
-- ei vain tietyissä tapauksissa hylkäämään oletusta epäreiluudesta.

Bayesiläisen päättelyn ytimessä on ennakko-oletusten määritteleminen. Kruschke
demonstroi ennakko-oletusten mallintamisen tärkeyttä seuraavasti. Kuvitellaan,
että tutkijan olisi tehtävä päätelmä siitä, kannattaisiko jalkapallo-ottelun
aloittaja valita heittämällä kolikon sijasta *naulaa* 
[@kruschke2014doing, 315]. Naulaa heitettäessä kaksi mahdollista heittotulosta
ovat a) se, että naula putoaa kyljelleen sekä b) se, että naula jää seisomaan
kannalleen. Etenkin pienellä aineistolla (ainoastaan muutama heitto) mitattuna
NHST-menetelmin toimiva tutkija ei voisi perustellusti hylätä ajatusta siitä,
että naula saattaisi olla tasapuolinen väline aloittajan arpomiseksi.
Bayesiläinen tutkija taas pystyy sisällyttämään tilastolliseen malliinsa
perustellun ennakko-oletuksensa: sen, että naulan putoaminen kannalleen on
fysikaalisesti jollei mahdotonta niin ainakin vaikeaa ja siten äärimmäisen
harvinaista. 

Bayesiläinen päättely lähteekin siitä, että tutkija määrittelee
ennakko-oletuksensa jonkin tapahtuman todennäköisyydestä. Nämä ennakko-oletukset
ilmaistaan tilastollisten jakaumien (*priorijakauma*) muodossa. Tämän jälkeen
mallia *päivitetään*, eli katsotaan, missä määrin käsitys jonkin ilmiön
todennäköisyydestä muuttuu, kun otetaan huomioon ilmiötä kuvaavaa dataa. Myös 
muuttuneet käsitykset ilmaistaan jakaumana (*posteriorijakauma*).

## Tutkimuksessa käytettävä tilastollinen malli {#tutkimuksen-malli}


Nyt käsillä olevan tutkimuksen tärkeimpiä tavoitteita on  vastata kysymykseen
siitä, mitkä tekijät vaikuttavat ajanilmauksen sijoittumiseen suomessa ja
venäjässä -- toisin sanoen, mitkä piirteet ovat tyypillisiä suomen, mitkä taas
venäjän S1-, S2-, S3- ja S4-konstruktioille. Mikko Ketokivi [-@ketokivi2015, 131]
tähdentää omassa tilastollisen päättelyn johdantoteoksessaan, että
tämänkaltaiset tutkimuskysymykset ovat kysymyksiä, joissa yhden muuttujan
arvojen vaihtelua eli varianssia selitetään toisten muuttujien varianssilla.
Muuttujaa, jonka varianssia pyritään selittämään, on tavallisesti kutsuttu
esimerkiksi *riippuvaksi* muuttujaksi (dependent variable) tai y-muuttujaksi.
Muuttujat, joiden avulla yhden muuttujan varianssia pyritään selittämään, ovat
usein nimeltään *riippumattomia muuttujia* (independent variable). Käytän tässä
Ketokiven esittelemiä, mielestäni kuvaavia suomenkielisiä termejä *selitettävä*
ja *selittävä* muuttuja, jotka vertautuvat hyvin englanninkielisessä
kirjallisuudessa esiintyviin ja esimerkiksi Rolf Baayenin [-@baayen2012, 14]
suosittelemiin nimityksiin *predicted variable* ja *predictor variable*.




Selitettävänä muuttujana tässä tutkimuksessa on ajanilmauksen sijainti.
Kyseessä on kategorinen (nominaalinen)[^ordinaalimuut] muuttuja, jolle 
oli alun perin tarkoitus antaa neljä mahdollista arvoa: S1, S2, S3 ja S4. Tämä jaottelu
olisi kuitenkin heikentänyt mallin selitysvoimaa.

Jos katsotaan edellisessä luvussa esitettyjä yleishavaintoja eri sijaintien osuuksista
tarkastelluissa kielissä, voidaan todeta -- niin kuin tutkimuskirjallisuuden perusteellakin 
saatettiin olettaa -- että suomen ja venäjän välillä on lähtökohtainen, osin kieliopillinen
ero S2- ja S3-sijaintien käytössä. Suomi ei juurikaan hyödynnä S2-sijaintia (siihen 
osuu ainoastaan 5,57 % tapauksista); vastaavasti venäjä ei juurikaan hyödynnä
S3-sijaintia (johon osuu 4,89 % tapauksista). Toisin päin katsottuna S2 on venäjän
yleisin sijainti (49,13 %) ja S3 suomen yleisin sijainti
(47,60 %).
Tästä seuraa, että olisi melko hedelmätöntä vertailla keskenään suomen ja venäjän S3-sijainteja
tai venäjän ja suomen S2-sijainteja. Mielekkäämpien vertailujen mahdollistamiseksi 
sijainnit S2 ja S3 onkin tilastollisessa mallissa yhdistetty, niin että selitettävällä
muuttujalla on kolme arvoa: S1, S2/S3 ja S4. Nimitän näitä jatkossa myös *alku-*, *keski-*
ja *loppusijainneiksi.*


Jos käytettävässä tilastollisessa mallissa ei olisi yhtään selittävää
muuttujaa, malli vastaisi kysymykseen *mikä on minkäkin sijainnin
todennäköisyys*. Esitän alla, miten merkitsen näitä todennäköisyyksiä jatkossa
ja havainnollisuuden vuoksi liitän karkeimpana mahdollisena arviona kustakin
todennäköisyydestä tiedon siitä, kuinka suuri osuus aineistossa kuhunkin sijaintiin
sijoittuu:

- Alkusijainnin todennäköisyys:  P(S1) ~ 22.98 %
- Keskisijainnin todennäköisyys:  P(S2/S3) ~ 53.56 %
- Loppusijainnin todennäköisyys:  P(S4) ~ 23.46 %

Ymmärrettävästi tällaisenaan malli olisi koko lailla epäinformatiivinen. Sen
avulla voisi ainoastaan tehdä päätelmiä siitä, millä todennäköisyydellä
satunnaisesti valittu suomen- tai venäjänkielinen positiivinen SVO-lause sisältää S1-, S2-,
S3- tai S4-sijoittuneen ajanilmauksen. Olennaisin selittävä muuttuja onkin
kunkin tapauksen kieli, jolloin tutkimuskysymys muuttuu muotoon *Mikä on
sijainnin Y todennäköisyys, jos tiedossa on, että kyseessä on kieltä X edustava
lause?* Tällaiset ehdolliset todennäköisyydet merkitsen pystyviivan avulla, niin
että S1-sijainnin todennäköisyys suomenkieliselle lauseelle merkittäisiin
$P(S1|lang=fi)$, S3-todennäköisyys venäjänkieliselle lauseelle $P(S3|lang=ru)$ ja
niin edelleen. 

[^ordinaalimuut]: Voisi esittää, että tässä käytetyt neljä sijaintimuuttujan
arvoa tekevät muuttujasta itse asiassa ordinaalimuuttujan eli muuttujan, jossa
eri arvojen järjestyksellä on merkitystä. Tätä lähestymistapaa en kuitenkaan
noudata, sillä järjestyksen selitysvoima sinänsä ei ole niin suuri, että sitä
olisi mieltä erikseen mallintaa. Olennaista on kysymys siitä, mikä neljästä
sijaintikategoriasta jossakin datapisteessä havaitaan.



\todo[inline]{Perustele lausetyyppiä ja sitä, että ei a-konjunktiota mukana}

\todo[inline]{Käsittele myös referentiaalisuuden typistetty esittäminen}


\todo[inline]{OLISI HALUTTU: referentiaalisuus, mutta sitä ei ollut mahdollista operationalisoida
järkevästi. Ei olisi myöskään ollut järkevää ottaa yhtä muuttujaa,jossa on 52 eri arvoa.
Näin ollen päädyttiin ottamaan selkeimmin aineistoa ryhmitteleväksi muuttujaksi semanttinen funktio,
joka on jossain määrin kompromissi luvusta 1. Sitä pidetään suuntaviivana, ja esimerkiksi referentiaalisuutta
tutkitaan ja tarkastellaan selittävänä tekijänä tämän muuttujan *sisällä*.
}


\todo[inline]{

SELOSTA myös..

}

### Selittävät muuttujat

\todo[inline]{AJATUS: OTA varsinaiseen isoon malliin mukaan vain morph + funct + pos... 
Mainitse jo tässä ja huomioi myöhemmin, että yksittäisiä kohtia voidaan selittää
myös clausestatus, corpus tms. avulla.
}

Tutkimuksen johdannossa esitettiin yhdeksi tärkeimmistä tavoitteista selittävien
tekijöiden löytäminen ajanilmauksen sijainnille. Luvussa  \ref{aimm} pyrittiin
määrittelemään mahdollisimman tarkasti, mitä *ajanilmauksella* tässä
tutkimuksessa tarkoitetaan ja minkälaisia erilaisia ajanilmauksia voidaan
erottaa. Nämä erilaisten ajanilmausten erot olivat tärkein lähtökohta myös
määriteltäessä niitä selittäviä muuttujia, joita tutkimuksessa käytetään.

Mahdollisiksi selittäviksi muuttujiksi voidaan taksonomioiden 1, 2 ja 3 pohjalta (ks. alaluvut \ref{taksn1}, \ref{taksn2} ja \ref{taksn3})
erotella *muodostustapa*, *semanttinen funktio* ja *referentiaalisuus.* Lisäksi yksi
vaihtoehto olisi ollut tarkastella aineistoryhmää yhtenä ainoana muuttujana, jolla tässä tapauksessa 
olisi ollut 50 eri arvoa ja joka ei olisi säilyttänyt
edellä muodostettujen taksonomioiden kautta saatua informaatiota erilaisten ajanilmausten luonteesta.
Muita oletettavasti sijaintiin vaikuttavia ja aineistosta mitattavissa olevia tekijöitä ovat lausetyyppi  eli
se, onko kyseessä esimerkiksi alisteinen sivulause, relatiivilause tai kysymyslause sekä 
mahdollisesti korpus, josta lause on peräisin.




Ajanilmauksen muodostustapa operationalisoitiin lopullisessa tilastollisessa
mallissa siten, että siitä erotettiin kaksi mitattavaa ominaisuutta:
ajanilmauksen morfologinen rakenne  ja ajanilmauksen positionaalisuus.

Morfologisella rakenteella  tarkoitetaan tässä yhteydessä lähtökohtaisesti sitä, ovatko
ajanilmaukset nominaalisia kuten ilmauksessa *viime vuonna* vai adverbisiä
kuten ilmauksessa *kauan*. Adverbisistä ajanilmauksista eroteltiin kuitenkin omaksi joukokseen
*deiktiset adverbit*, joihin laskettiin aineistoryhmät L1a--L1c,L5c ja L8a--L8c
(ks. luvut \ref{l1a-l1b-l1c}, \ref{l5a-l5b} ja \ref{l8a-l8b-l8c} edellä). Muita adverbisiä aineistoryhmiä
ovat E5b, E6a--E6b, F2b, F3a--F3e sekä P1--P2 (luvut \ref{e5a-e5b}--\ref{e7},\ref{f2a-f2b}--\ref{f4},  \ref{l7a-l7b-l7c-l7d} ja \ref{p1-p2}).
Morfologinen rakenne -muuttujan eri arvot sekä niiden suhteelliset kielikohtaiset osuudet on esitetty
seuraavassa kuviossa:


![\noindent \headingfont \small \textbf{Kuvio 4}: Morfologinen rakenne -muuttujan arvot ja kielikohtainen jakautuminen \normalfont](figure/morph.counts-1.pdf)

\todo[inline]{TODO: mitkä adverbisistä oikeasti pronominaalisia, mainitse erikseen}

Kuvio 4 osoittaa, että aineistoryhmien
kokoeroista johtuen morfologisen rakenteen jakautuminen muuttujan eri arvojen välillä 
ei ole täysin samanlainen suomessa ja venäjässä. Kummassakin kielessä deiktiset adverbit muodostavat
kuitenkin pienimmän joukon.

Positionaalisuutta mitattiin tutkimusaineistossa kahdella arvolla: 0 (ei
positionaalinen) ja 1 (positionaalinen). Positionaalisia aineistoryhmiä
tutkimusaineistossa edustavat ryhmät L2a-L2b (luku \ref{l2a-l2b}), L4a
(\ref{l4a-l4b} ) ja F1b (\ref{f1a-f1b}), mikä tarkoittaa, että suurin osa
kaikista tutkimusaineiston tapauksista on ei-positionaalisia. Suomessa
positionaalisten tapausten osuus kaikista on
11,71
ja venäjässä 
6,44 prosenttia.

Morfologisen rakenteen ja positionaalisuuden lisäksi tilastollisessa mallissa
otettiin kolmantena selittävänä muuttujana huomioon ajanilmauksen semanttinen funktio.
Ajanilmaukset ryhmiteltiin eri semanttisiin funktioihin luvussa \ref{taksn2} esitellyn
toisen ajanilmaustaksonomian perusteella siten, että tuloksena saatiin kuvion
 5 mukainen jakauma, joka kattaa kaiken kaikkiaan
11 eri kategoriaa:[^katsel]


[^katsel]: Eri kategorioista käytettyjen lyhenteiden selitykset on annettu luvun \ref{aineistoryhmuxe4t} alussa

![\noindent \headingfont \small \textbf{Kuvio 5}: Semanttinen funktio -muuttujan arvot ja kielikohtaiset suhteelliset osuudet \normalfont](figure/semf.tab.counts-1.pdf)

Kuvio 5 osoittaa, että niin suomessa kuin venäjässäkin tavallisimman 
semanttinen funktio -muuttujan kategorian muodostavat simultaanista lokalisoivaa funktiota edustavat
tapaukset, joita kummassakin kielessä on liki puolet kaikista tapauksista. Vähiten on likimääräistä
funktiota edustavia tapauksia. 

Edellä lueteltujen kolmen selittävän muuttujan lisäksi luvussa \ref{taksn3}
esitetyn kolmannen, referentiaalisuuteen liittyvän taksonomian perusteella oli
alun perin tarkoitus rakentaa vielä yksi selittävä muuttuja. Kävi
kuitenkin ilmi, ettei referentiaalisuuden kuvaaminen
omana erillisenä muuttujanaan tarkentanut tilastollista mallia, vaan se informaatio,
joka erillisellä referentiaalisuus-muuttujalla aineistosta olisi voitu saada,
saatiin selville myös semanttista funktiota tarkastelemalla. Lisäksi malliin 
kokeiltiin liittää selittävinä muuttujina edellä mainittuja
lähdekorpusta ja lausetyyppiä. Näiden selitysvoima koko aineiston
kannalta havaittiin kuitenkin olemattoman pieneksi, minkä seurauksena nekin suljettiin
pois varsinaisesta, koko aineistoon sovellettavasta mallista. Niin lähdekorpusta kuin sovelletusti
myös lausetyyppiä käytettiin kuitenkin vain tiettyjä aineistoryhmiä tarkastelevissa
pienemmissä tilastollisissa malleissa (ks. esim. alaluvut \ref{tilastollinen-malli-johdantokonstruktion-sijaintien-kartoittamiseksi}
ja  \ref{deiktiset-adverbit-ja-positionaalisuus}). 

Tutkimusta lähdettiin tekemään edellä kuvatulla mallilla, jossa kielen lisäksi 
on kolme selittävää muuttujaa. Tutkimuksen kuluessa
 havaittiin kuitenkin, että malliin olisi mielekästä sisällyttää informaatiota myös
siitä, onko ajanilmauksen sisältämässä lauseessa läsnä sanoja, jotka on automaattisessa
morfologisessa analyysissa merkitty *numeraaleiksi*. Näin ollen lopullinen malli
sisälsi myös viidennen selittävän muuttujan, *numeraalin läsnäolon*. Tämä muuttuja 
ja perustelut sen käyttämiseksi on esitetty osiossa \ref{kehres-s1}.


\todo[inline]{Kysy ehkä Michaelilta tarkemmin, miten kuvata mallin eri vaihtoehtojen kokeilemista}

### Mallin kuvaus kootusti


Tilastollinen malli, joka edellä kuvattujen selitettävän ja selittävien
muuttujien perusteella rakennettiin, on luonteeltaan regressiomalli
[@ketokivi2015, 134]. Kenties tavallisin regressiomallin muoto on
lineaarinen regressiomalli, jossa mitataan jatkuvien muuttujien, esimerkiksi
ihmisen painon ja viikoittaisen liikuntaan käytetyn ajan, yhteyttä. Kun
selitettävä muuttuja on, niin kuin tässä, kategorinen, käytetään tavallisesti
*logistista regressiomallia*. Nyt käsillä olevassa tapauksessa, kun
selitettävällä kategorisella muuttujalla on enemmän kuin kaksi mahdollista
arvoa, kyseessä on malli, jota Ketokivi (mts. 135) nimittää *kategoriseksi
regressioksi* ja esimerkiksi Kruschke [-@kruschke2014doing, 668] *ehdolliseksi
logistiseksi regressioksi* (*conditional logistic regression*).

Tarkastellaan tässä käytettävää kategorista regressiomallia lähemmin mallin
rakennetta havainnollistavan kuvion X avulla.

(kuvio puuttuu vielä)

Kuviosta nähdään, että ennakkokäsitykset kunkin muuttujan ja niiden välisten
interaktioiden välillä on ilmaistu puoli-Cauchy-jakaumilla (Gelman-viite). 
(tämä osio siis vielä kirjoittamatta, koska tilastollisen mallin tarkka muoto
vielä osin työn alla).
Koko malli rakennettiin hyödyntämällä R-ohjelman rjags-kirjastoa [@rjags]. Itse mallin kuvaus
kirjoitettiin bugs-muodossa ja se on nähtävillä Bitbucket-palvelussa osoitteessa
https://tinyurl.com/y9uagcfr (tarkistettu 15.3.2018).


\todo[inline]{Mainitse ROPE ja HDI.. Mainitse myös @mustanoja2011idiolekti, 106--109}


## Mallissa havaittavat vaikutukset yleisellä tasolla {#ylvaik}


\todo[inline]{ks. onko esim. mustanojalla viitettä keskihajontojen käyttämiseen}

Tarkastelen seuraavaksi
yleisellä tasolla, miten hyvin edellä luetellut muuttujat tässä esitellyn
tilastollisen mallin perusteella selittävät aineistossa havaittavaa
sijaintimuuttujan varianssia. Arvioitaessa eri muuttujien vaikuttavuutta yksi tärkeimmistä
mittareista ovat muuttujien keskihajonnat: mitä lähempänä nollaa keskihajonta
on, sitä pienempi selitysvoima muuttujalla voidaan nähdä. Tässä
käytettyjen selittävien muuttujien keskihajonnat käyvät ilmi kuviosta 
6:


![\noindent \headingfont \small \textbf{Kuvio 6}: Selittävien muuttujien keskihajonnat \normalfont](figure/std.all-1.pdf)

Kuvion 6 kaltaisia kuvioita kutsutaan tavallisesti termillä
*caterpillar plot*. Tässä käytetyt caterpillar-kuviot on luotu R-ohjelman
*ggmcmc*-paketilla [@ggmcmc], ja koska ne ovat tutkimuksen kannalta olennainen
keino kuvata tilastollisia havaintoja, annan tässä kohtaa muutamia suuntaviivoja
niiden tulkitsemiseksi.

Caterpillar-kuvioiden y-akselilla ovat tarkkailtavat muuttujat, kuvion 
6 tapauksessa kielen (std.lang), morfologisen rakenteen
(std.morph), semanttisen funktion (std.funct) ja positionaalisuuden
(std.pos) keskihajonnat. X-akseli puolestaan kuvaa arviota
kyseisen muuttujan vaikutuksen voimakkuudesta tai tarkemmin sanottuna siitä, kuinka paljon
varianssia muuttuja selittää. Kuviossa 6 kyse on keskihajonnoista,
joten x-akseli lähtee nollasta (keskihajonta ei voi olla negatiivinen) eikä
sillä ole varsinaista ylärajaa. Kuten edellä selitettiin, tässä luvussa käytetyn
tilastollisen mallin linkkifunktio on logaritminen, minkä tähden useimmissa
luvun kuvioissa X-akselin arvot voivat olla myös negatiivisia. Kuvioissa
esitettyjä lukuja ei ole järkevää yrittää tulkita absoluuttisesti, vaan eri
muuttujien arvoja on verrattava toisiinsa. Nyrkkisääntönä voidaan ajatella, että
mitä kauempana nollasta X-akselin "toukat" ovat, sitä suurempi on kyseisen
muuttujan vaikutus. 

Kuviosta 6 nähdään, että std.lang-muuttujan kohdalla
x-akselille on piirretty kolmeosainen kuvio: ohut viiva, paksumpi viiva sekä
ympyrä. Viivat kuvastavat sitä, kuinka paljon tilastollinen malli on muuttanut
ennakkokäsitystä kielen vaikutuksesta eri sijainteihin -- toisin sanoen viivat
esittävät posteriorijakauman kielen keskihajonnalle. Ympyrä osoittaa jakauman
mediaanin: sen, mikä on oletettavasti paras yksittäinen arvio vaikutuksen
voimakkuudesta. Paksumpi viiva osoittaa, mille välille 65% \todo[inline]{TARKISTA} arvioista 
osuu ja ohuin viiva sen, mille välille osuu 95% kaikista arvioista (ks. *HDI*
edellä). Kuviossa 6 osa arvioista 
(morfologisen rakenteen ja positionaalisuuden keskihajonnat)
on niin tarkkoja, että ohuita
ja paksuja viivoja ei kuvasta juuri voi erottaa. Kokoavasti
voidaan sanoa, että semanttisen funktion vaikutus näyttäisi ensi katsomalta kaikkein
suurimmalta -- tähän arvioon samoin kuin toisiksi voimakkaampana näyttäytyvään 
kieli-muttujaan liittyy kuitenkin sen verran epävarmuutta, että näiden muuttujien
keskinäiinen järjestys voisi olla myös päinastainen. Yhtä kaikki, nämä kaksi
muuttujaa erottuvat selvästi muista.

\todo[inline]{Käytä Kruschken kahviesimerkkiä edellä logaritmisuuden selittämisessä}

Caterplot-kuvioiden lisäksi luvussa 5 tarkastellaan paikoitellen myös
varsinaisia posterijakaumia. Tällöin kyseessä voi olla joko yhden tietyn
muuttujan jakauma tietylle arvolle tai tavallisemmin kahden muuttujan tai arvon
välinen vertailu. Kuvio 7 esittää tarkemmin sen,
minkä arvion käytetty tilastollinen malli antaa semanttisen funktion
vaikutuksesta. Vastaavat jakaumat on piirretty edellä mainitun John Kruschken
[-@kruschke2014doing] teoksen tarjoamien R-funktioiden avulla.


![\noindent \headingfont \small \textbf{Kuvio 7}: Semanttisen funktion vaikutuksen posteriorijakauma \normalfont](figure/morphsdpost-1.pdf)

Kuviosta 7 havaitaan, että arvio semanttisen 
funktion vaikutuksesta todella on sekin lopulta kohtalaisen tarkka. Kuviossa 6
näkyvä mediaani on noin 0,660, ja  95% mahdollisista arvoista osuu
välille 0,627 -- 0,696.


Kuvio 6 on merkittävä muutenkin kuin vain
keinona esitellä caterplot-kuvioiden tulkintaa. Se, että kielellä todella on
paljon -- mahdollisesti jopa eniten -- vaikutusta selitettävän muuttujan
saamiin arvoihin, vahvistaa edellä puhtaiden prosenttiosuuksien perusteella
tehdyn päätelmän siitä, että suomen ja venäjän välillä todella on
systemaattinen ero siinä, miten ajanilmaukset jakautuvat eri sijainteihin.

Jos kielen vaikutusta tutkitaan tilastollisen mallin valossa tarkemmin, saadaan
itse asiassa samankaltainen vaikutelma kuin edellä puhtaita prosenttiosuuksia
tarkasteltaessa. Tämä näkyy kuviosta 8, joka
esittää, miten todennäköinen kukin tarkastelluista neljästä sijainnista on
*suomessa verrattuna venäjään*. Juuri tämä *suomi verrattuna venäjään*
-näkökulma on luvuissa \ref{ajanilmaukset-ja-alkusijainti} --
\ref{ajanilmaukset-ja-loppusijainti} käytäntönä, kun vastaavia kuvioita
tarkastellaan enemmän. Kuten todettu, bayesiläisittäin rakennettu tilastollinen
malli toimii siten, että ensin kullekin sijainnille määritellään jokin
ennakkoarvio sen todennäköisyydestä. 
Kysyttäessä todennäköisyyttä $P(S1|fi)$[^propnot] kysytään lopulta sitä,
minkä verran ja mihin suuntaan arvio S1-sijainnin todennäköisyydestä muuttuu,
jos lang-muuttujan kahdesta mahdollisesta arvosta valitaan *fi* (suomi). Jos
tarkasteltavalla selittävällä muuttujalla on kaksi arvoa, kuten kielen
tapauksessa, on eri arvojen (*fi* ja *ru*) vaikutus täsmälleen toistensa
peilikuva: jos *fi*-arvo aiheuttaa oletusarvoon poikkeaman -0.5, on vastaava
*ru*-arvon aiheuttama poikkeama tällöin +0.5. Tämän vuoksi jatkossa
vertailtaessa suomea ja venäjää kuvioissa näytetään ainoastaan fi-arvoa
koskevat posteriorijakaumat -- ru-arvon jakaumat ovat aina näiden
käänteislukuja. 

[^propnot]: Koska kulloinkin käsiteltävät selittävät muuttujat käyvät aina ilmi
itse käsittelyn kontekstista, jätän todennäköisyyttä kuvaavissa merkinnöissä jatkossa muuttujan
nimen ilmaisematta, niin että merkintä *fi* tarkoittaa 'kieli-muuttuja arvolla fi'.

![\noindent \headingfont \small \textbf{Kuvio 8}: Kielen vaikutus eri sijaintien todennäköisyyteen \normalfont](figure/justlang-1.pdf)


Kuvio 8 havainnollistaa hyvin suomen ja venäjän
välisiä eroja eri sijaintien käytössä. Kuviosta nähdään, että jos kielen tiedetään  olevan suomi, 
on S4-sijainti selvästi oletusarvoa todennäköisempi ja S1-sijainti vastaavasti
epätodennäköisempi. S2/S3-sijainti taas on mitä luultavimmin jotakuinkin yhtä tavallinen 
kielestä riippumatta, joskin on täysin mahdollista, että suomessa S2/S3 on jonkin verran
venäjää epätodennäköisempi. Kuvio 8 toimii
mallina siitä, miten vastaavia logaritmiselle asteikolle sijoittuvia kuvioita tulisi
tämän tutkimuksen yhteydessä tulkita: positiiviset arvot tarkoittavat, että
jokin selitettävän muuttujan arvo eli ajanilmauksen sijainti on
oletustodennäköisyyttä
(sijaintimuuttujan arvoa koskeva priori) todennäköisempi ja negatiiviset puolestaan sitä, että
jokin sijainti on oletustodennäköisyyttä epätodennäköisempi. Kuvion 8 
kaltaisissa suomea ja venäjää vertaavissa kuvioissa on lisäksi täydennettävä, että
"oletustodennäköisyyttä todennäköisempi" ja "oletustodennäköisyyttä epätodennäköisempi"
tulee ymmärtää tarkasteltuna suomessa verrattuna venäjään.

\todo[inline]{tarkenna edellistä niin, että puhutaan vetosuhteesta.}

Palataan vielä keskihajontoja käsittelevään kuvioon 6 .
Edellä jo todettiin, että semanttisella funktiolla näyttäisi
olevan lähes yhtä suuri merkitys ajanilmauksen sijainnin kannalta 
kuin kielellä. Myös morfologisella rakenteella, positionaalisuudella ja
numeraalin läsnäololla
keskihajonnat erottuvat nollasta, mikä tarkoittaa, että kaikki
mainitut muuttujat todella selittävät ainakin jonkin verran 
sijaintimuuttujan arvojen vaihtelua.
Tämän tutkimuksen kannalta olennaisimmat havainnot eivät kuitenkaan liity pelkästään
kieliin tai pelkästään muihin selittäviin muuttujiin, vaan ennen muuta siihen,
miltä osin semanttinen funktio, morfologinen rakenne, positionaalisuus ja
numeraalin läsnäolo vaikuttavat
*kielestä riippuen* eli interaktiossa kielimuuttujan kanssa. Voidaanko
esimerkiksi sanoa sekä 1) *jos ajanilmaus on morfologiselta rakenteeltaan
nominaalinen, sillä on yleisesti
ottaen adverbistä ajanilmausta suurempi todennäköisyys sijoittua lauseen
loppuun*  että 2) *jos kyseessä on venäjänkielinen ajanilmaus, havaittu
vaikutus on suurempi verrattuna suomenkieliseen ajanilmaukseen*
(jälkimmäistä todennäköisyyttä voitaisiin muodollisemmin merkitä
$P(S4|ru,NP)$)?.

Kuvio 9 esittää, minkä verran kielen ja muiden
selittävien muuttujien interaktioilla on mallin mukaan vaikutusta. Vaikutukset
ovat pienempiä ja osin epävarmempia kuin muuttujilla ilman interaktion huomioimista,
mutta eroja on yhtä kaikki havaittavissa.

![\noindent \headingfont \small \textbf{Kuvio 9}: Kielen ja muiden selittävien muuttujien välisten interaktioiden keskihajonnat \normalfont](figure/sd.interact-1.pdf)

Kuviossa 6 havaittu semanttisen funktion voimakas vaikutus näkyy
kuvion 9  mukaan myös yhdistettynä kieleen, niin 
että juuri kieli + semanttinen funktio -yhdistelmä erottuu selitysvoimaltaan 
selvästi muista selittävistä muuttujista. Positionaalisuuden ja numeraalin 
läsnäolon kielisidonnaiset vaikutukset ovat lähellä toisiaan, ja morfologinen
rakenne sijoittuu vaikutukseltaan näiden kahden ja semanttisen funktion välimaastoon.



## Analyysin rakenne 

Kontrastiivista tutkimusta tehdessä asioiden esittämisjärjestyksen
määrittäminen on välillä ongelmallista. Koska tutkittavaa ilmiötä on
vertailtava monesta näkökulmasta, mikä tahansa valittava käsittelyjärjestys
kärsii jonkin verran siitä, että tiettyjä asioita on jo vertailtu aiemmin ja
toisaalta siitä, että tiettyjen asioiden vertailu tapahtuu vasta myöhemmin.
Olen tässä tutkimuksessa jakanut tilastollisen mallin tulkinnan ja aineiston varsinaisen analyysin
siten, että käsittelen kutakin kolmesta selitettävän muuttujan arvosta omana lukunaan.
Luvussa  \ref{ajanilmaukset-ja-alkusijainti} keskitytään siis alkusijaintiin
eli S1-konstruktioihin, luvussa \ref{ajanilmaukset-ja-keskisijainti} taas keskisijaintiin
eli S2- ja S3-konstruktioihin ja lopulta luvussa
\ref{ajanilmaukset-ja-loppusijainti} loppusijaintiin ja S4-konstruktioihin.

Sijaintikohtaisilla käsittelyluvuilla on karkeasti katsottuna kaksi päämäärää.
Ensinnäkin tarkoituksena on kartoittaa, minkälaisten konstruktioiden osina
ajanilmaukset kussakin sijainnissa kummassakin kielessä toimivat. Toiseksi
tavoitteena on löytää niitä eroja, joita kuhunkin sijaintiin ja sitä
hyödyntäviin konstruktioihin suomen ja venäjän välillä liittyy.
Sijaintikohtaisten lukujen jälkeisessä kokoavassa luvussa
\ref{kokoava-kontrastiivinen-analyysi} suoritetaan varsinainen kontrastiivisen
funktionaalisen analyysin menetelmää mukaileva samanlaisuushypoteesien
arviointi: verrataan luvuissa \ref{ajanilmaukset-ja-alkusijainti} --
\ref{ajanilmaukset-ja-loppusijainti} tehtyjä havaintoja ja määritellään, mitä
suomen ajanilmauskonstruktioita millekin venäjän ajanilmauskonstruktioille
voidaan pitää vastineina ja minkälaisissa tilanteissa.
 
\todo[inline]{
Tänne selitystä myös siitä, miksi s2 ja s3 samassa...

Tänne SD-arviot sekä yksittäisistä muuttujista että interaktioista..

Tänne myös ajatus siitä, onko esim korpustyypillä vaikutusta vai ei

Verbiajatuksen esittelyä?
}

Ajanilmaukset ja alkusijainti
=============================



Sijaintia lauseen alussa voidaan pitää monella tapaa erityisenä, ja -- kuten
Tuija Virtanen [-@virtanen2004point, 94] asian ilmaisee -- sillä on paljon
*kognitiivista potentiaalia*. Virtasen (mp.) mukaan lauseenalkuisia
adverbiaaleja käytetään yli kielirajojen muun muassa puheenaiheen alustamiseen,
koherenssin luomiseen, tekstin segmentoimiseen ja adverbiaalia seuraavan
varsinaisen lauseen ilmaiseman affektin tai mielipiteen esille tuomiseen.

Vaikka lauseenalkuisilla adverbiaaleilla yleensä ja epäilemättä myös
ajanilmauksilla erityisesti on monia kielessä kuin kielessä esiintyviä
tehtäviä, luvuissa  \ref{ajanilmaukset-tutkimusaineistossa} ja
\ref{tilastollinenmalli} esitettyjen lukujen valossa vaikuttaa selvältä, että
suomi ja venäjä eroavat melko suurestikin siinä, miten paljon ajanilmauksia
lauseen alussa käytetään. Tämän luvun tarkoituksena on pureutua mainittujen
tilastollisten erojen taustalla oleviin tekijöihin ja selvittää, miksi
alkusijainti on suomessa venäjää harvinaisempi. Nyt käsillä oleva luku on siinä
mielessä keskeinen, että juuri tässä esitellään suurin osa niistä
konstruktioista, joiden perusteella suomen ja venäjän välisiä eroja
ajanilmausten sijoittumisessa voidaan selittää.

Niin tässä esitettävän alkusijainnin kuin lukujen
\ref{ajanilmaukset-ja-keskisijainti}--\ref{ajanilmaukset-ja-loppusijainti}
aiheena olevien keski- ja loppusijaintien osalta käsittelyn lähtökohtana on
edellisessä luvussa kuvattu tilastollinen malli ja sen perusteella suomen- ja
venäjänkielisissä aineistoissa havaittavat erot ja yhtäläisyydet. Tavoitteenani
ei ole esittää jäännöksetöntä listausta kaikista suomen ja venäjän
alkusijaintiin liittyvistä ajanilmauskonstruktioista, vaan keskittyä niihin
tapauksiin, jotka tilastollisen mallin perusteella nousevat esiin ja jotka
parhaiten havainnollistavat tässä tarkasteltujen kielten suhdetta toisiinsa.
Kuten edellisen luvun lopuksi todettiin, tässä eriteltyjen konstruktioiden
varsinainen kontrastiivinen (funktionaalinen) analyysi toteutetaan luvussa
\ref{kokoava-kontrastiivinen-analyysi}.


## Samanlaisia vaikutuksia {#s1-samans}

Luvussa \ref{sij-yleisk} havaittiin, että tutkimusaineiston 
 86 737 suomenkielisestä lauseesta 
 9 806
sisältää S1-sijoittuneen ajanilmauksen. Vastaavasti 73 302
venäjänkielisestä lauseesta S1-ajanilmaus havaittiin 
 26 967
tapauksessa. Tässä osiossa etsitään niitä piirteitä,
jotka mainituille 9 806 
suomenkieliselle ja 26 967 
venäjänkieliselle lauseelle olisivat yhteisiä.

Kun arvioidaan, mitkä ovat kummallekin kielelle yhteisiä piirteitä, edellisessä luvussa
rakennettua tilastollista mallia on hyödynnettävä kahdella tasolla. Ensinnäkin
on tarkasteltava selittävien muuttujien itsenäisiä kokonaisvaikutuksia
aineistossa. Jos halutaan löytää tapauksia, joissa S1-konstruktio on
todennäköinen sekä suomessa että venäjässä, on tutkittava niitä selittävien
muuttujien arvoja, joiden vaikutus S1-sijainnin todennäköisyyteen on koko
aineistolla mitattuna positiivinen. Nämä koko aineiston tasolla mitatut
vaikutukset on esitetty kuvioissa 10 -- 
12.

Toiseksi on etsittävä niitä selittävien muuttujien ja kielen välisiä
interaktiotapauksia, joissa interaktio on mahdollisimman pieni. Vaikutuksen toteamiseksi
suomessa ja venäjässä samanlaiseksi ei vielä riitä,
että jokin selittävän muuttujan X arvo on positiivinen koko aineiston tasolla:
on myös pystyttävä osoittamaan, ettei kielten välillä ole eroa siinä, miten
muuttuja vaikuttaa. Selittävien muuttujien ja kielen väliset interaktiot on
esitetty alempana kuvioissa 15 -- 
17. Kuten edellä kuvion 8 yhteydessä
todettiin, nämä kuviot on piirretty siten, että niissä verrataan *suomea
suhteessa venäjään*. Toisin sanoen esimerkiksi morfologisen rakenteen osalta kuvio 
 16 kertoo, että *verrattuna venäjään* deiktiset adverbit
sijoittuvat suomessa keskimääräistä epätodennäköisemmin S1-asemaan.

Aloitan selittävien muuttujien ja S1-aseman välisen yhteyden tarkastelemisen
kuitenkin semanttisen funktion muuttujasta, jonka koko aineistoa koskevat vaikutukset näkyvät
kuviosta 10. Semanttinen funktio koostuu
kaiken kaikkiaan 11 eri
arvosta, ja voidaankin ajatella, että kyseessä on tutkimukseen
valituista selittävistä muuttujista tarkin. Semanttisen funktion avulla
aineistoa kuvataan siis kaikkein hienojakoisimmin ja se
muodostaakin tärkeimmän niistä kehyksistä, joiden avulla suomea ja venäjää tässä ja
seuraavissa kahdessa luvussa vertaillaan.
\todo[inline]{vapausasteet?}

![\noindent \headingfont \small \textbf{Kuvio 10}: Semanttisten funktioiden vaikutus alkusijaintiin \normalfont](figure/functs1-1.pdf)

Kuvion  10  perusteella voidaan alustavasti todeta, että
koko aineiston tasolla S1-sijainnin todennäköisyyttä kasvattavat likimääräinen,
sekventiaalinen ja simultaaninen funktio sekä molemmat resultatiivisista
funktioista. Koko aineiston tasolla S1-sijainnin todennäköisyyttä taas näyttäisivät
pienentävän ainakin teelinen, tapaa ilmaiseva ja presuppositionaalinen funktio. 
Jos kuviota 10 
verrataan alempana esitettyyn kuvioon 15, havaitaan
kuitenkin edelleen, ettei kielen ja semanttisen funktion välinen interaktio ole
yhdenkään edellä mainitun kategorian osalta täysin neutraali, vaan kielet 
eroavat toisistaan siinä, miten eri semanttiset funktiot vaikuttavat alkusijainnin
todennäköisyyteen. Pienin kielen ja semanttisen funktion välinen interaktio
havaitaan kuvion 15 mukaan varsinaisella resultatiivisella
ekstensiolla, joka näyttäisi kasvattavan S1-aseman todennäköisyyttä koko lailla
kielestä riippumatta. Samansuuntainen, kielestä lähes riippumaton vaikutus
voidaan todeta myös simultaanisella funktiolla. Presuppositionaalinen, tapaa
ilmaiseva  ja teelinen funktio puolestaan vaikuttavat kielestä riippumatta
pienentävän S1-todennäköisyyttä.

Edellä mainituista havainnoista kuvion 10 vasenta puolta koskevat
(semanttiset funktiot, jotka kielestä riippumatta pienentävät alkusijainnin todennäköisyyttä)
eivät ole erityisen yllättäviä tai informatiivisia. Niin suomessa kuin venäjässä
on odotuksenmukaista, etteivät tapaa ilmaisevat *быстро* tai *nopeasti*
useinkaan esiinny lauseen alussa; presuppositionaalisten
*jo, vielä, уже* tai *еще* esiintyminen ilman toista ajanilmausta lauseen
alussa on puolestaan monin paikoin
kieliopillisesti mahdotonta tai käyttömahdollisuuksiltaan hyvin rajoittunutta[^presupmahd].
Sama koskee pitkälti myös teelisiä ilmauksia kuten *kahdeksi vuodeksi* tai *на два года*.

[^presupmahd]: Vertaa esimerkiksi tutkimusaineistoon kuuluvaa suomenkielistä esimerkkiä
*Lontoolainen astronomi Ian Crawford on jo julkaissut raportin, jonka mukaan
moderni fysiikka saattaa avata mahdollisuuksia kiertää Einsteinin suhteellisuusteoria.*, jossa
jo-sanan siirtäminen virkkeen alkuun tuottaisi version *Jo lontoolainen astronomi Ian Crawford on
julkaissut...*

Havainnot kuvion 10 oikealta puolelta (semanttiset
funktiot, jotka kielestä riippumatta kasvattavat alkusijainnin
todennäköisyyttä) vaativat sen sijaan tarkempaa analyysia. Kuten mainittu,
näitä tapauksia edustavat varsinainen resultatiivinen ekstensio ja
simultaaninen funktio, joista jälkimmäinen on sikäli ongelmallinen tarkempien
havaintojen pohjaksi, että se on kategoriana koko tutkimusaineiston suurin ja
siten melko heterogeeninen. Simultaanista funktiota tutkitaankin tarkemmin
kieliä erottavien vaikutusten yhteydessä osiossa \ref{simultaaninen-funktio}. Varsinainen
resultatiivinen ekstensio on puolestaan huomion kohteena seuraavassa alaluvussa.

Kuviot 11 ja 12 
havainnollistavat sitä, miten morfologinen rakenne ja
positionaalisuus vaikuttavat alkusijainnin todennäköisyyteen koko aineiston
tasolla.

![\noindent \headingfont \small \textbf{Kuvio 11}: Morfologisen rakenteen vaikutus alkusijaintiin \normalfont](figure/morphs1-1.pdf)

![\noindent \headingfont \small \textbf{Kuvio 12}: Positionaalisuuden vaikutus alkusijaintiin \normalfont](figure/poss1-1.pdf)

Kuviosta 11 nähdään, että erillään tarkasteltuna
morfologinen rakenne -muuttujan  ADV-arvo ('ei-deiktinen adverbi')
kasvattaa alkusijainnin todennäköisyyttä samoin kuin kuvion 
12 perusteella positionaalisuusmuuttujan
0-arvo ('ei positionaalinen').
Alempana esitettävät kuviot 16 ja 17 kuitenkin
osoittavat, että vaikutukset ovat suomessa venäjään nähden voimakkaampia.

Tarkastelen lopuksi vielä numeraalin läsnäolo -muuttujan vaikutusta 
alkusijainnin todennäköisyyteen:

![\noindent \headingfont \small \textbf{Kuvio 13}: Numeraalin läsnäolon vaikutus alkusijaintiin \normalfont](figure/isnumerics1-1.pdf)

Kuvion 13 tarkastelu rinnakkain kieli + numeraalin
läsnäolo -interaktiota esittävän 
kuvion 18 kanssa antaa pitkälti samanlaisen
vaikutelman kuin positionaalisuutta mittaavien kuvioiden tarkastelu. Koko
aineiston tasolla tarkasteltuna numeraalin läsnäolo kasvattaa
S1-todennäköisyyttä, mutta vaikutuksessa on selvä kielestä riippuva interaktio,
niin että suomessa positiivinen vaikutus on venäjään verrattuna voimakkaampi.

Kaiken kaikkiaan tilastolliseen malliin alkusijainnin osalta luotu yleiskatsaus 
viittaa siihen, etteivät suomen ja venäjän S1-asemat 
ole rakenteeltaan kovin samanlaisia. Selkein aineistosta löydettävissä oleva
yhteneväisyys koskee varsinaista resultatiivista ekstensiota edustavia
ilmauksia, joita seuraavassa tutkitaan lähemmin. On kuitenkin tärkeä huomata,
että vaikka aineistossa ei juuri havaita sellaisia tekijöitä, jotka kielestä
riippumatta kasvattaisivat S1-todennäköisyyttä, varsinaista resultatiivista
ekstensiota edustavat ilmaukset tuskin ovat ainoita tapauksia, joiden osalta
suomen ja venäjän S1-konstruktiot vastaavat toisiaan. Yhteneväisyyksiä
voidaankin löytää paitsi tarkastelemalla niitä tekijöitä, jotka erottuvat 
aineistosta positiivisesti, myös tutkimalla sitä tapausten joukkoa, joka 
jää jäljelle, kun kieliä erottavat vaikutukset on selvitetty. Monet tässä
tutkittavia kieliä yhdistävät konstruktiot tulevatkin esille luvussa 
\ref{s1-eri} tutkittaessa tilastollisen mallin kielisidonnaisia vaikutuksia.


### Varsinainen resultatiivinen ekstensio

Kuvioita 10 -- 12 
sekä 15 -- 17 
vertailemalla saatettiin todeta, että
tutkimuksen kannalta hedelmällisin alkusijaintia koskeva suomen ja venäjän
välinen yhteneväisyys liittyy varsinaisen resultatiivisen ekstension semanttiseen
funktioon. Tätä semanttista funktiota edustavat tutkimusaineistossa 
aineistoryhmät E1b, E4, E5a ja E5b eli muun muassa ilmaukset *kahdessa
päivässä*, *siinä ajassa*, *hetkessä* ja *äkkiä* (tarkemmin ks. luvut
\ref{e1a-e1b-e1c}, \ref{e4} ja \ref{e5a-e5b}).
Kaikkiaan näihin ryhmiin kuuluu
 5 732 suomen- ja 
 4 314 venäjänkielistä ajanilmaustapausta.
Kuvio 14 esittää tarkemmin varsinaisen resultatiivisen ekstension
posteriorijakaumat S1-aseman osalta. Vasemmalle  on kuvattu jakauma
vaikutuksesta koko aineiston tasolla, oikealle
vaikutus suomessa verrattuna venäjään.

![\noindent \headingfont \small \textbf{Kuvio 14}: Varsinaista resultatiivista ekstensiota koskevat posteriorijakaumat \normalfont](figure/resvarsposts-1.pdf)

Kuvion 14  oikea puoli osoittaa, etteivät
suomi ja venäjä tilastollisen mallin valossa juurikaan eroa siinä, mikä vaikutus
varsinaisella resultatiivisella ekstensiolla on S1-aseman todennäköisyyteen: luultavasti
suomessa varsinaisella resultatiivisella ekstensiolla S1-todennäköisyyttä kasvattava vaikutus
on jonkin verran voimakkaampi (posteriorijakauma on suurimmaksi osaksi nollan yläpuolella), mutta
ero ei ole suuri. Kuvion vasen puoli taas kertoo, että mallin mukainen arvio
vaikutuksesta koko aineistolla on melko selkeä ja voimakas, sillä
kaikki posteriorijakauman arvot sijoittuvat selkeästi nollan yläpuolelle.

Tilastollisen mallin perusteella on siis syytä olettaa, että varsinaista
resultatiivista ekstensiota edustavat ilmaukset sijoittuvat niin suomessa kuin
venäjässä keskimääräistä tyypillisemmin lauseen alkuun, joskin suomessa
poikkeama tavallisesta S1-todennäköisyydestä saattaa olla jonkin verran venäjää
suurempi. Yksi looginen selitys havainnolle olisi, että niin suomessa kuin venäjässä on
käytössä enemmän tai vähemmän samanlaisia varsinaista resultatiivista
ekstensiota edustavia ajanilmauskonstruktioita. Tarkastelen seuraavassa
esimerkkien avulla, minkälaisia nämä mahdolliset konstruktiot voisivat olla.

Katsotaan ensin aineistoryhmää E5b eli suomen *äkkiä*- ja venäjän *вдруг*-sanoja,
joita edustavat esimerkit \ref{ee_rutiinit} ja \ref{ee_kulbit}:




\ex.\label{ee_rutiinit} \exfont \small Arjen rutiinit rikkoutuvat ja \emph{äkkiä} elämä saa odottamattoman
käänteen. (Araneum Finnicum: tikkurilanteatteri.fi)







\exg.  И \emph{вдруг} сюжет нашей акции совершил неожиданный кульбит. (RuPress:
Комсомольская правда)
\label{ee_kulbit}  
 Ja äkkiä kulku meidän-gen tempaus-gen tehdä-PRET odottamaton käänne
\




\vspace{0.4cm} 

\noindent

Esimerkkien \ref{ee_rutiinit} ja \ref{ee_kulbit} tapauksessa *äkkiä*- ja *вдруг*-sanat
todella vaikuttaisivat toimivan osina hyvin samanlaista konstruktiota. Kummassakin
tapauksessa ajanilmauksen diskurssifunktiosta voisi todeta, ettei se liity
niinkään informaatiorakenteellisiin topiikin ja fokuksen rooleihin, vaan sen
tehtävä on ennemminkin toimia kerronnan segmenttien rajaa ilmaisevana
diskurssipartikkelina. Niin esimerkin \ref{ee_rutiinit} *äkkiä* kuin esimerkin
\ref{ee_kulbit} *вдруг* johdattavat lukijan huomaamaan, että tapahtumien
siihenastisessa kulussa tapahtuu jokin (äkillinen) muutos. Kutsun näitä
konstruktioita yksinkertaisesti *äkkiä*-konstruktioksi. Ne voidaan SVO-lauseiden
osalta kuvata seuraavan matriisin avulla:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]



prag \[ df & muu \\
\] \\[5pt]


sem aika,muutos \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{3.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 5}: Suomen ja venäjän äkkiä-konstruktiot \normalfont \vspace{0.6cm} 

Matriisissa 5 ajanilmauksen semanttisiin ominaisuuksiin
on merkitty, että kyseessä on jotakin muutosta vallitsevaan asiaintilaan
merkitsevä ajanilmaus -- kriteeri, jonka oletettavasti voivat täyttää monet
muutkin ilmaukset kuin suomen *äkkiä* ja venäjän *вдруг*. Varsinaisen
resultatiivisen ekstension kielestä riippumaton S1-painottuminen ei selitykään
pelkästään äkkiä- ja вдруг-sanoilla tai äkkiä-konstruktiolla sinänsä.
Esimerkkejä \ref{ee_rutiinit} ja \ref{ee_kulbit} voidaan verrata seuraaviin nominaalista
E1b-ryhmää edustaviin esimerkkeihin:




\ex.\label{ee_itavalta} \exfont \small \emph{Kolmessa vuodessa} se on houkutellut jo kaksi miljoonaa kävijää ja
näin noussut Itävallan toiseksi suosituimmaksi nähtävyydeksi Schönbrunin
linnan jälkeen. (FiPress: Keskisuomalainen\_1999-03-07)






\ex.\label{ee_salaiset} \exfont \small \emph{Kahdessa päivässä} me voimme hankkia sitä salaisista
varastopaikoistamme niin suuret määrät kuin haluamme. (Areneum Finnicum:
netikka.net)







\exg.  Всего \emph{за несколько дней} дорожники отремонтировали центральную
улицу города. (RuPress: РИА Новости)
\label{ee_remontulitsy}  
 Vain muutamassa-päivässä työmiehet korjata-PRET keskus(adj.) katu-AKK kaupunki-GEN
\






\exg.  Но \emph{за несколько лет} местная фауна подавила весь этот сброд.
(RuPress: kp.ru/daily)
\label{ee_fauna}  
 Mutta muutamassa-vuodessa paikallinen fauna painaa-PRET koko tämä joukko
\




\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_itavalta} -- \ref{ee_fauna} ei ole samassa mielessä kyse
*muutoksesta* vallitsevaan asiaintilaan kuin esimerkeissä \ref{ee_rutiinit} ja
\ref{ee_kulbit}. Olennaista näissä tapauksissa tuntuu olevan ennen kaikkea se, että
jotakin on tapahtunut odotuksiin nähden yllättävän nopeassa ajassa. Käytän
matriisissa 6 esitettyä *affektiivista konstruktiota*
kuvaamaan vastaavia tapauksia, joissa kirjoittaja pyrkii korostamaan tapahtumaan kuluneen
ajan ja itse tapahtuman suhteen yllättävyyttä:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & affekti \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

sem   Ajanilmaukseen nähden yllättävä propositio \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{0.3em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 6}: Suomen ja venäjän affektiiviset konstruktiot \normalfont \vspace{0.6cm} 

Matriisissa 6 on esitetty ajanilmauksen
diskurssifunktioksi *affekti*, sillä juuri neutraalia sävyä voimakkaamman
tunteen välittäminen vaikuttaisi selkeimmältä ajanilmaukseen kohdistettavalta
viestintätehtävältä yllä annetuissa esimerkeissä. Jos palataan
luvun  \ref{aimkonstr} alussa esitettyyn
ajatukseen siitä, että myös kirjoitettua viestiä vastaanottava lukija
vähintäänkin kuvittelee lukemalleen tietyt prosodiset piirteet, voidaan
affektiivisen konstruktion osalta todeta, että niin suomessa kuin venäjässä
lauseenalkuista ajanilmausta hyvin todennäköisesti korostetaan esimerkiksi
äkillisesti nousevalla intonaatiolla.[^eifon] Ajanilmauksen ja itse lauseessa
ilmaistavan väittämän suhdetta kuvaa subjektin, verbin ja täydennyksen
yhdistävä laatikko, jonka semanttiseksi ominaisuudeksi (sem-attribuutti) on
merkitty yllättävyys ajanilmaukseen
nähden. Kuten luvussa \ref{s1-f1a} havaitaan, yllättävyydellä
voidaan viitata paitsi tapahtuman nopeuteen, myös esimerkiksi frekvenssiin,
jolla väittämässä kuvattu tapahtuma toistuu.


[^tdlyh]: Puhun jatkossa myös S1-todennäköisyydestä, S2-todennäköisyydestä jne. sen sijaan, että
käyttäisin koko pitkää muotoa *S1-aseman todennäköisyys*.

[^eifon]: En kuitenkaan merkitse foneettisia ominaisuuksia
konstruktiomatriiseihin, vaan tyydyn yksittäisiin mainintoihin silloin, kun
prosodialla on esimerkiksi tässä kuvatun kaltainen affektin käsitteeseen
olennaisesti liittyvä funktio.



Huomattakoon, ettei sen paremmin matriisissa 5 kuin
matriisissa 6 kuvata lainkaan tarkemmin
subjektiin, verbiin ja täydennyksiin kohdistuvia ominaisuuksia sinänsä. Tämä johtuu
siitä, että niin äkkiä-konstruktio kuin affektiivinen konstruktiokin esiintyvät
monenlaisissa konteksteissa ja lauseissa, jotka saattavat esimerkiksi
informaatiorakenteen osalta erota toisistaan merkittävästikin.[^lisahuom_m1ja2] Oleellista
vaikuttaisi olevan nimenomaan se, että äkkiä-konstruktioissa ajanilmausten
ajallisuus on pienemmässä roolissa ja ennemmin kyse on äkillistä muutosta
indikoivasta diskurssipartikkelin tehtävästä; affektiivisissa konstruktioissa
taas juuri ajanilmauksen ja muun lauseessa välitettävän  viestin välisen suhteen odottamattomuus
on keskeistä.

[^lisahuom_m1ja2]: Lisäksi on paikallaan muistuttaa, että vaikka tutkimusaineisto lähinnä teknisistä
syistä koostuu vain SVO-lauseista, koko proposition syntaktinen rakenne ylipäätään
voisi luultavasti olla tyystin erilainen, ja silti sopia affektiiviseen konstruktioon.

\todo[inline]{keskustele mahd. linkeistä ja läheisistä konstruktioista

Mieti myös sitä, että jos diskurssifunktion laajentaa käsittämään myös affektisuuden
ja muun, pitäisikö selitystä tarkentaa suuntaan "jokin näistä on oleellinen / dominoiva"

Jospa notaatiossa voisi toimia niin (vrt. ehkä Kuningas ja Leino), että sellaiset
sisälaatikot merkittäisiin katkoviivalla, joita eivät koske ne ominaisuudet, jotka
kuitenkin koskevat koko ympäröivää ulompaa laatikkoa.

} 

\todo[inline]{Huom! Virtasen mainitsema affekti! n. s. 93}

Varsinaista resultatiivista funktiota ja S1-asemaa
tarkasteltaessa on nostettava esiin vielä yksi pelkästään venäjään liittyvä
piirre, eli *вдруг*-sanan tyystin ei-temporaalinen käyttö merkityksessä *siltä varalta
että* [vrt. @diskslova, 325]. Tätä kuvaa esimerkki \ref{ee_vzryvtshatka}:





\exg.  Взрывчатку я оставил у себя, побоялся, \emph{вдруг} Чикита сделает
что-то не так. (rulit.org)
\label{ee_vzryvtshatka}  
 räjähde-AKK minä jättää-PRET PREP itse-GEN pelätä-PRET äkkiä T. tehdä jokin ei kunnossa
\




\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_vzryvtshatka} edustamille konstruktioille ei suomessa ole *äkkiä*-sanan avulla
toteutettavia vastineita.

\todo[inline]{
Mielenkiintoista kyllä: vertaa viron äkki, jolla tämä merkitys on, ks. esim.

https://fi.wiktionary.org/wiki/%C3%A4kki ja
http://www.eki.ee/dict/ekss/index.cgi?Q=%C3%A4kki&F=M

}

\todo[inline]{ "Muutamassa päivässä ovat tuomet puhjenneet kukkaan" 
jes! nimenomaan affekti ! :)
}


## Eriäviä S1-vaikutuksia {#s1-eri}

Kuten edellä todettiin, tutkimuksen luvuilla
\ref{ajanilmaukset-ja-alkusijainti} -- \ref{ajanilmaukset-ja-loppusijainti}
on kaksioisainen tehtävä. Toisaalta tavoitteena on
ylipäätään selvittää, mitä alkusijaintiin liittyviä ajanilmauskonstruktioita
tässä tarkasteltavissa kielissä on, toisaalta taas sen eksplikoiminen,
miltä osin suomi ja venäjä eroavat toisistaan. Erityisesti jälkimmäiseen tavoitteeseen
pyrkimisessä olennainen rooli on sen tarkastelemisella, miten morfologisen rakenteen,
positionaalisuuden, numeraalin läsnäolon  ja semanttisen funktion muuttujat
vaikuttavat ajanilmauksen sijaintiin vuorovaikutuksessa kielimuuttujan kanssa.

\todo[inline]{Jonnekin maininta siitä, että jälkimmäinen tavoite tärkeämpi}

Jos palataan edellä osiossa \ref{ylvaik} tehtyihin havaintoihin eri interaktioiden
keskihajonnoista eli siitä, millä kieli + muuttuja -yhdistelmällä on koko aineiston
kannalta eniten selitysvoimaa (ks. kuvio 9),
voidaan todeta, että suurin keskihajonta on semanttisen 
funktion ja kielen välisellä interaktiolla. Eri funktiota edustavat ajanilmaukset
siis käyttäytyvät suomessa jossain määrin eri tavalla kuin venäjässä.
Kuten jo todettua, juuri semanttisen funktion tarkastelu muodostaa tässä
tehtävän analyysin rungon. S1-aseman osalta kielen ja semanttisen 
funktion väliset interaktiot on esitetty kuviossa 15. 
Muistettakoon, että kuvio esittää *suomen ja semanttisen funktion* välisen interaktion
voimakkuuden suhteessa keskiarvoon. Venäjän ja semanttisen funktion välinen interaktio
on aina tässä esitetyn käänteisluku.


![\noindent \headingfont \small \textbf{Kuvio 15}: Semanttisten funktioiden ja kielen interaktio alkusijainnissa \normalfont](figure/langfuncts1-1.pdf)

Kuviosta 15 on erotettavissa kolme kutakuinkin
yhtä suurta funktiojoukkoa. 

1. Ensimmäiseksi joukoksi voidaan eritellä funktiot, joilla interaktio kielen kanssa
   on lähellä neutraalia. Edellä jo käsiteltiin varsinaista resultatiivista ekstensiota, jonka
   vaikutus ei juuri eroa kielten välillä mutta jolla koko aineiston tasolla
   katsottuna on S1-sijaintia painottava efekti. Lisäksi osiossa \ref{s1-samans}
   mainittiin presuppositionaalinen, tapaa ilmaiseva ja teelinen funktio, joiden 
   neutraaliudessa kieliä kohtaan ei nähty juuri mitään yllättävää. Kuviosta 15 
   nähdään, että teelisen funktion ja kielen väliseen interaktioon liittyy itse asiassa huomattava
   määrä epävarmuutta, mikä johtuu siitä, että teelistä funktiota edustavia
   S1-tapauksia on kaiken kaikkiaan hyvin vähän.
2. Kaksi funktiota, likimääräinen ja kehyksinen resultatiivinen, erottuvat
   selkeästi muista siten, että niillä on suomessa venäjää suurempi
   S1-todennäköisyyttä kasvattava vaikutus. Myös sekventiaalisella funktiolla
   venäjään verrattava S1-vaikutus on selvästi positiivinen. Tutkin näitä
   funktioita tarkemmin osiossa \ref{kasv-s1}.
3. Kolmannen joukon muodostavat funktiot, jotka pienentävät suomen S1-aseman
   todennäköisyyttä suhteessa venäjään -- toisin sanoen ne funktiot, joiden
   jakaumat ovat kuviossa 15 vasemmalla eli
   arvoiltaan negatiivisia. Selkeimmin tällaisia ovat 
   sekventiaalis-duratiivinen ja duratiivinen funktio ja F-funktiot
   Nämä funktiot ovat lähemmän tarkastelun kohteena osiossa \ref{pien-s1}.
   Mainitussa alaluvussa tutkitaan myös tarkemmin simultaanisia funktioita, joka
   ryhmänä on hyvin heterogeeninen ja josta siten ei
   välttämättä ole mielekästä tehdä koko funktiota koskevia yleistyksiä.[^teeliset_ei]

[^teeliset_ei]: Kuten edellä mainittiin, myös teelisen funktion
posteriorijakauma on kauttaaltaan nollan alapuolella. Lauseenalkuiset teeliset
tapaukset ovat kuitenkin niin marginaalinen ilmiö, ettei niitä käsitellä tässä sen
tarkemmin.

\todo[inline]{Korjaa huomio duratiivisesta ja muutenkin tarkista}

Semanttinen funktio ei kuitenkaan ole ainoa selitysvoimainen muuttuja, kun
pohditaan suomen ja venäjän välisiä eroja S1-aseman todennäköisyyteen
vaikuttavissa tekijöissä. Morfologisen rakenteen osalta kuviosta 
16 nähdään, että deiktiset adverbit vaikuttaisivat
suomessa selvästi epätodennäköisemmiltä S1-sijoittujilta kuin morfologisen
rakenteen kaksi muuta kategoriaa, muut adverbit ja nominaaliset ilmaukset.


![\noindent \headingfont \small \textbf{Kuvio 16}: Morfologisen rakenteen vaikutus alkusijaintiin suomessa verrattuna venäjään \normalfont](figure/langmorphs1-1.pdf)

Deiktisiä adverbejä käsitellään tarkemmin alaluvussa \ref{sim-l5b}.
Katsotaan lopuksi positionaalisuuden ja kielen välistä vuorovaikutusta. 
Myös positionaalisuudella ja kielellä on selvä interaktio, joka
havaitaan kuviosta 17: 

![\noindent \headingfont \small \textbf{Kuvio 17}: Positionaalisuuden vaikutus alkusijaintiin suomessa verrattuna venäjään \normalfont](figure/pos.lang.s1-1.pdf)

Kuvion 17 mukaan positionaalisuus-muuttujan vaikutus
arvolla 0 ('ei-positionaalinen') on suomessa positiivinen ja arvolla 1
('positionaalinen') negatiivinen. Toisin sanottuna positionaalisuus on edellä
mainittujen deiktisten adverbien sekä muun muassa sekventiaalis--duratiivisen
ja F-funktioiden ohella yksi niistä tekijöistä, jotka pienentävät alkusijainnin
todennäköisyyttä suomessa verrattuna venäjään. Vaikka vaikutus on pienempi kuin
esimerkiksi deiktisillä adverbeillä ylemmässä kuviossa, osiossa
\ref{s1-sim-pos} käy ilmi, että tällä havainnolla on lopulta melko paljon
selitysvoimaa pohdittaessa eroja suomen ja venäjän S1-konstruktioissa.

Kuvio 18 esittää vielä numeraalin 
läsnäolo -muuttujan kielisidonnaiset vaikutukset S1-sijaintiin.

![\noindent \headingfont \small \textbf{Kuvio 18}: Numeraalin läsnäolon vaikutus alkusijaintiin suomessa verrattuna venäjään \normalfont](figure/langisnumerics1-1.pdf)

Kuviosta 18 havaitaan jotakuinkin samansuuruiset
kieltenväliset erot kuin positionaalisuuden kohdalla kuviossa 17.
Suomessa numeraalin läsnäolo kasvattaa venäjään verrattuna
S1-sijainnin todennäköisyyttä, ja vaikka vaikutus ei ole yhtä voimakas kuin
vaikkapa eräillä semanttisen funktion arvoilla, voidaan siihen suhtautua
verrattain suurella varmuudella, sillä posteriorijakauman kaikki arvot
ovat hyvin lähellä toisiaan.

Tarkastelen suomen ja venäjän välisiä eroja seuraavassa kahteen alaryhmään
jaoteltuna. Aloitan niistä hieman harvinaisemmista tapauksista, joissa tietty
muuttujan arvo nostaa S1-aseman todennäköisyyttä suomessa enemmän kuin
venäjässä. Osiossa \ref{pien-s1} siirrytään analysoimaan
tapauksia, joissa tietty muuttujan arvo pienentää suomen S1-todennäköisyyttä
venäjään nähden.

### Tekijät, jotka kasvattavat S1-todennäköisyyttä suomessa verrattuna venäjään {#kasv-s1}



Edellisessä osiossa tehdyn katsauksen perusteella tutkimusaineistosta voidaan
erotella karkeasti ottaen viisi tekijää, jotka kasvattavat alkusijainnin
todennäköisyyttä suomessa enemmän kuin venäjässä. Näitä ovat
ei--deiktiset adverbit, likimääräinen, kehyksinen resultatiivinen ja
sekventiaalinen funktio sekä numeraalin läsnäolo lauseessa. Koska luvussa
\ref{sij-yleisk} esitettyjen tilastojen mukaan S1 on suomessa paljon venäjää
harvinaisempi ja siten oletettavasti käytöltään rajatumpi sijainti, voisi
olettaa, että mainitut viisi tekijää ovat oleellisessa asemassa, kun pyritään
määrittämään, mitkä ovat suomen tärkeimpiä alkusijaintikonstruktioita.
Asian voisi muotoilla myös niin, että koska venäjässä alkusijainti on tasaisen yleinen
eikä sen suosio heilahtele samalla tavalla kuin suomessa, auttavat edellisen
osion katsauksessa suomen osalta havaitut kohoumat selvittämään niitä erityissyitä,
joita ajanilmauksen sijoittuminen lauseen alkuun suomessa oletettavasti vaatii.

#### Ei--deiktiset adverbit {#eideikt}



Kuvioissa 15 -- 18 
hieman yllättävääkin oli se, että yksi suomen S1-todennäköisyyttä venäjään nähden 
kasvattavista tekijöistä vaikuttivat olevan ne aineiston adverbit, joita ei ole lueteltu
deiktisiksi adverbeiksi (viittaan näihin lyhyesti ilmauksella *ei--deiktiset adverbit*).
Ei--deiktisten adverbien joukossa onkin monia ryhmiä, joissa alkusijainti on suorastaan
harvinainen. Esimerkiksi suomen aineistoryhmissä F2b ja F3b (ks. luvut \ref{f2a-f2b} -- \ref{f3a-f3b-f3c-f3d-f3e})
S1-tapauksia on hyvin vähän (2,07 % ja 2,28 % tapauksista), 
yhtenä harvoista esimerkeistä seuraavat:




\ex.\label{ee_kahdesti} \exfont \small Kahdesti espoolaiset tekivät virheen, ja kahdesti KalPa rankaisi
menemällä nopeasti 2-0 johtoon. (FiPress: Länsi-Savo)






\ex.\label{ee_ainajoku} \exfont \small Näissä enemmän pelatuissa lajeissa aina joku on tiennyt voittajan.
(FiPress: Iltalehti)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_kahdesti} alkusijainti juontaa juurensa edellä kuvattuun
affektiiviseen konstruktioon sekä luonnollisesti esimerkistä selkeästi
erottuvaan toistorakenteeseen; esimerkin \ref{ee_ainajoku} tapauksessa  voisi
puolestaan nähdä nimenomaan *aina*-adverbin ja *joku*-pronominin yhdistelmään
liittyvän oman, melko spesifin konstruktionsa. Niin kuin yllä esitettyjen
prosenttilukujen pienuudesta voidaan päätellä, nämä ovat kuitenkin marginaalisia 
tapauksia, eivätkä selitä ei--deiktisten adverbien venäjään verrattuna 
suurempaa S1-todennäköisyyttä kasvattavaa vaikutusta.

Ei--deiktisten adverbien joukko on kuitenkin melko laaja
(yhteensä 14 aineistoryhmää), ja
sen sisällä ennen kaikkea aineistoryhmä L7c (luku \ref{l7a-l7b-l7c-l7d}) nostattaa
S1-sijoittuvien tapausten kokonaismäärää. 
Suomen L7c-tapauksista 34,40% eli yhteensä
 528 sijoittuu esimerkin 
\ref{ee_ostaja} tavoin S1-asemaan:




\ex.\label{ee_ostaja} \exfont \small Kun ostajan ja myyjän välillä on todellinen vastavuoroisuuden ketju,
silloin ostaja ostaa kallistakin ja jatkaa ketjua vaikeinakin aikoina.
(Araneum Finnicum: start-upcenter.fi)





\vspace{0.4cm} 

\noindent
L7c-ryhmään liittyviin määrittelyongelmiin ja sen merkitykseen laajemmin palataan
luvussa \ref{l7l8-erot-kesk}. Toinen tavallista enemmän S1-tapauksia sisältävä
suomen ei--deiktinen adverbiryhmä on E5b eli *äkkiä*-sana, jota käsiteltiin
edellä varsinaisen resultatiivisen ekstension yhteydessä (ks. esimerkki \ref{ee_rutiinit}).
Näiden ryhmien lisäksi alkusijainti on jonkin verran koko aineiston keskiarvoa tavallisempi
kahdessa taajuutta ilmaisevassa aineistoryhmässä, F3c:ssä
(11,92) ja 
F3d:ssä (13,72), joista
suomenkielisiä esimerkkejä edustavat \ref{ee_joskuspaiva} -- \ref{ee_aikaharvoin}:




\ex.\label{ee_joskuspaiva} \exfont \small Joskus on sellaisia päiviä ettei juoksuttaminen eroa rodeosta
paljoakaan, ja joskus Dotti ojentelee jalkojaan sievästi ja
tahdikkaasti. (Araneum Finnicum: angelfire.com)






\ex.\label{ee_voittajat} \exfont \small Yleensä päävoiton voittajat ilmoittautuvat meille heti, mutta joskus he
odottavat protestiajan päättymistä ennen kuin ottavat yhteyttä.
(FiPress: Aamulehti)






\ex.\label{ee_aikaharvoin} \exfont \small Aika harvoin tamperelaistuomarit kuljettelevat rintataskussa pistoolia.
(FiPress: Aamulehti)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_aikaharvoin} sisältää F3c-aineistoryhmän ajanilmauksen *Aika harvoin*.
Tapaus muistuttaa pitkälti edellä esitettyjä affektiivisen konstruktion edustajia kuten
esimerkkejä \ref{ee_itavalta} ja \ref{ee_salaiset}: virkkeessä ilmaistu propositio on 
epätavallinen tai odotuksista poikkeava, mitä korostetaan lauseenalkuisella ajanilmauksella.
F3d-aineistoryhmää edustavat esimerkit \ref{ee_joskuspaiva} ja \ref{ee_voittajat} voidaan sen sijaan nähdä 
yhden ja saman konstruktion edustajina. Nimitän näiden virkkeiden taustalla
olevaa rakennetta *kontrastiiviseksi konstruktioksi*, joka on kuvattu tarkemmin
matriisissa 7 :



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & kontr \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

sem   sääntö,ennen,aiemmin,... \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{0.3em}
}}}\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]



prag \[ df & kontr \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

sem   poikkeus,jälkeen,nykyisin,... \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{0.3em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 7}: Kontrastiivinen konstruktio \normalfont \vspace{0.6cm}

Matriisi 7 esittää tilanteen, jossa rinnastetaan kaksi
ajanilmauksella alkavaa lausetta (selkeyden vuoksi konjunktiota ei ole matriisiin erikseen 
merkitty). Kuten muissa tässä tutkimuksessa esitettävissä
attribuutti--arvo-matriiseissa, myös tässä molemmat konstruktioon kuuluvat lauseet ovat 
SVO-lauseita, mutta epäilemättä konstruktio on tavallinen myös kompleksisemmilla lausetyypeillä.
Olennaista on, että ajanilmauksen diskurssifunktiona on kontrastin luominen kahden 
virkkeessä esitettävän proposition välillä. Esimerkin \ref{ee_voittajat} tapauksessa kontrasti
on tyyppiä *sääntö--poikkeus*, mutta alempana osiossa \ref{s1-sim-pos} nähdään, että myös esimerkiksi
*ennen--jälkeen* tai *aiemmin--nyt* ovat tavallisia. Esimerkissä 
\ref{ee_joskuspaiva} puolestaan rinnastuksen kohteena ovat kaksi toisiinsa nähden vastakohtaista asiaintilaa 
(rodeota muistuttava juoksuttaminen / nilkkojen sievä ojentelu). Kaksi joskus-sanalla alkavaa lausetta sisältävät
virkkeet voitaisiin tarkemmin analysoida myös omaksi kontrastiivisesta konstruktiosta periväksi konstruktiokseen,
jossa kontrasti ei aina ole yhtä selvä kuin esimerkissä \ref{ee_joskuspaiva}.

Kontrastiivinen konstruktio samoin kuin F3c-ryhmään liittyvä affektiivisten konstruktioiden
käyttö eivät ole ainoastaan suomen adverbiryhmiä leimaavia ominaisuuksia, vaan hyvin
samanlaiset rakenteet ovat käytössä myös venäjässä, kuten seuraavat kolme
esimerkkiä osoittavat:

\todo[inline]{Käytä rohkeasti linkkejä tässä niin että puhut sekä joskus--joskus -konstruktiosta
että kontrastiivisesta konstruktiosta?}

\todo[inline]{Tuolla alempana voisi taajuusanalyysin yhteydessä huomioda, jos näillä on ikään kuin liian
pieni s1-tod kasvattava vaikutus suomessa}





\ex.\label{ee_svuzami} \exfont \small \emph{Очень редко} компании заключают договоры с вузами на организацию
практик и стажировок. (RuPress: Известия)






\ex.\label{ee_vombat} \exfont \small Обычно вомбаты ночные животные, но \emph{иногда} они принимают солнечные
ванны в специально подготовленных местах, близко к входу в нору.
(Araneum Russicum: iucn.ru)






\ex.\label{ee_peremeny} \exfont \small \emph{Иногда} эти перемены кардинально меняют нашу жизнь к лучшему,
\emph{когда-то} они просто хорошие, \emph{иногда} не очень, а
\emph{иногда} такие, что даже и вспоминать не хочется. (Araneum
Russicum: novostroikaspb.ru)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_svuzami} vertautuu esimerkkiin \ref{ee_aikaharvoin}, sillä molemmissa kuvataan jokin
harvinainen asiaintila, niin että  harvinaisuus tulee korostetuksi lauseenalkuisella ajanilmauksella.
Esimerkki \ref{ee_vombat} sisältää esimerkkiä \ref{ee_voittajat} muistuttavan sääntö--poikkeus-tyyppisen
kontrastin, ja esimerkki \ref{ee_peremeny} on rinnastettavissa esimerkkiin \ref{ee_joskuspaiva}. Viimeinen
tapaus osoittaa myös sen, että vaikka kontrastiivisessa ja siitä mahdollisesti perivissä
konstruktioissa tyypillisemmin on kaksi rinnastettavaa propositiota, voi määrä olla 
suurempikin.[^inogdahuom] Luvussa \ref{s1-sim-pos} pyrin kuitenkin tekemään
selkeän eron kontrastiivisten ja näitä tietyissä tapauksissa muistuttavien
alatopiikkikonstruktioiden väillä.

[^inogdahuom]: joku sivuhuomio kogda-to:n käytöstä

Kaiken kaikkiaan tässä tarkastellut suomessa tavallista todennäköisemmät
S1-adverbitapaukset eivät ole millään muotoa epätavallisia venäjässä. Tämä
nostaa kysymyksen siitä, minkä takia ei--deiktiset adverbit ylipäätään
vaikuttavat S1-sijaintiin suomessa voimakkaammin kuin venäjässä. Ilmiön
taustalla olevat tekijät hahmottuvatkin
paremmin tutkimalla kuviota 19,
joka esittää S1-aseman prosentuaalisen osuuden kaikissa ei--deiktisissä
adverbiryhmissä ja suhteuttaa tämän kummassakin kielessä koko aineiston tasolla 
havaittaviin S1-osuuksiin.


![\noindent \headingfont \small \textbf{Kuvio 19}: Ei--deiktiset adverbiaineistoryhmät ja suhteellinen S1-osuus suomessa ja venäjässä. Tummempi katkoviiva kuvaa S1-tapausten osuutta koko suomenkielisessä, vaaleampi koko venäjänkielisessä aineistossa. \normalfont](figure/adv.erot.s1-1.pdf)

Kuvio 19  osoittaa, että venäjässä kaikilla
adverbisillä aineistoryhmillä lukuun ottamatta L7c:tä S1-osuus on pienempi kuin
koko aineiston tasolla mitattuna ja että monissa ryhmissä erotus koko aineiston
tasolla mitattavaan osuuteen on huomattavan suuri. Suomen osalta sen sijaan kaikki 
neljä tässä alaluvussa mainittua adverbiryhmää ylittävät  koko
aineiston S1-osuuden. Koska S1-osuus on koko aineiston tasollakin mitattuna
melko matala (11,31 %), ei poikkeama
S1-sijainnissa harvinaisten adverbiryhmienkään tapauksessa ole erityisen
suuri. Lopputuloksena tästä tilastollinen malli ennustaa aivan oikein, että
tietyt adverbitapaukset -- lähinnä tässä käsitellyt F-funktiota edustavat, edellä käsitelty E5b-ryhmä
ja myöhemmin tarkasteltavat L7c-ryhmän lauseet -- nostavat suomen S1-todennäköisyyttä
verrattuna venäjään. Pohjimmiltaan kyse on siitä, että venäjässä alkusijainti 
on yleiskäyttöisempi, mutta suomessa syynä alkusijainnin käytölle
ovat useimmiten edellä kuvatun kaltaiset melko spesifit konstruktiot. Venäjässä
alkusijainnilla ilmaistaan oletettavasti myös asioita, joita voitaisiin
yhtä hyvin ilmaista muillakin sijainneilla, kun taas suomen alkusijaintitapaukset
ovat usein sellaisia, että ainoastaan alkusijaintia hyödyntävät konstruktiot
täyttävät sen viestintätarpeen, joka kirjoittajalla on. Tätä oletusta testataan 
tarkemmin alaluvussa \ref{pien-s1}.



#### Likimääräinen funktio {#likim-s1}

Likimääräinen semanttinen funktio (ks. osiot \ref{muut-semanttiset-funktiot} ja \ref{lm1}) kattaa
 esimerkiksi suomen *viiden maissa*- ja venäjän *около пяти* -tyyppiset ilmaukset.
Kuvion 15 perusteella likimääräisellä funktiolla
on suurin piirtein sama vaikutus suomen S1-aseman todennäköisyyteen kuin kehyksisellä 
resultatiivisella ekstensiolla. Likimääräisestä funktiosta on huomautettava, että se on aineistossa
verrattain harvinainen: suomenkielisiä tapauksia on kaiken kaikkiaan 
 134 ja venäjänkielisiä
 158. Tästä huolimatta likimääräisen funktion vaikutuksesta suomen S1-asemaan
voidaan tilastollisen mallin perusteella olla melko varmoja. Suomen kielen ja likimääräisen
funktion interaktion posteriorijakauma on esitetty tarkemmin kuviossa 20.

![\noindent \headingfont \small \textbf{Kuvio 20}: Likimääräisen semanttisen funktion posteriorijakaumat \normalfont](figure/likimposts-1.pdf)

Kuvion 20 mukaan likimääräisyyden vaikutus suomessa verrattuna venäjään
on kaikilla 95 % luottamusvälin sisään osuvilla arvoilla selvästi positiivinen ja se vaihtelee
0,392 ja 
0,728 
välillä. Koska likimääräinen funktio ei ole erityisen yleinen,  kokonaiskuvan
saaminen etenkin kaikista suomen likimääräisistä S1-tapauksista on melko helppoa. 
Näitä on aineistossa 
kaikkiaan 35 kappaletta, 
joista virke \ref{ee_kiuruvesi} on edustava esimerkki:




\ex.\label{ee_kiuruvesi} \exfont \small \emph{Ilta kahdeksan maissa} Kiuruveden kalastusalueen isännöitsijä
Pekka Eerikinharju otti laatikollisen ankeriaita takapenkillensä ja
\emph{jo yhdeksän maissa} nämä päivälennolla saapuneet ankeriaat
uiskentelivat Kiuruvedessä. (Araneum Finnicum: kiuruvesi-lehti.fi)





\vspace{0.4cm} 

\noindent
Olennaista esimerkissä \ref{ee_kiuruvesi} on sen informaatiorakenne, jonka
lähemmäksi tarkastelemiseksi on paikallaan tutkia tarkemmin esimerkkiä sen
laajemmassa kontekstissa. Kyseessä on lyhyt Kiuruvesi-lehden verkkosivujen uutinen,
joka tätä kirjoitettaessa oli edelleen saatavissa internetistä:

\begin{quote}%
Tuhat ankeriasta istutettiin Kiuruveteen

KIURUVETEEN istutettiin torstai-iltana tuhat lasiankeriasta. Ranskasta
peräisin olevat ankeriaat saapuivat Helsinki-Vantaan lentokentälle
puolenpäivän jälkeen oltuaan ensin karanteenissa Ruotsissa.
Styroksilaatikoissa Ylä-Savoon matkanneet 3000 ankeriasta tulivat
Iisalmeen Pohjois-Savon kalatalouskeskuksen kalatalousneuvoja Tuula
Keinosen kyydissä. \emph{Ilta kahdeksan maissa} Kiuruveden
kalastusalueen isännöitsijä Pekka Eerikinharju otti laatikollisen
ankeriaita takapenkillensä ja \emph{jo yhdeksän maissa} nämä
päivälennolla saapuneet ankeriaat uiskentelivat Kiuruvedessä.
Kiuruveteen on tiettävästi viimeksi istutettu ankeriaita 70-luvulla.-\/-
(Tarkka lähde: http://www.kiuruvesilehti.fi/uutiset/104/, tarkistettu
6.3.2017)
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Edellä luvussa \ref{eri-tason-topiikkeja} tehdyn jaottelun mukaisesti voidaan todeta,
että esimerkin \ref{ee_kiuruvesi} kontekstina olevan tekstin 
*diskurssitopiikkina* ovat Kiuruveteen istutetut ankeriaat. Tästä topiikista
kirjoittaja kertoo *alatopiikkeja* hyödyntävällä strategialla: pilkkomalla
diskurssitopiikin useammaksi segmentiksi, jolloin teksti on ikään kuin näistä
segmenteistä koostuva lista. Kuten esimerkiksi Vilkuna [-@vilkuna1989, 92]
ja Shore [-@shore2008, 46] huomauttavat, on hyvin tavallista, että
jaottelu tehdään nimenomaan ajallisiin segmentteihin, jolloin on luonnollista,
että ajanilmaukset sijaitsevat esimerkin \ref{ee_kiuruvesi} tavoin aivan lauseen alussa
(vrt. myös osio \ref{eri-tason-topiikkeja} edellä).
Tutkimusaineiston *kahdeksan maissa* -tyyppiset likimääräiset ilmaukset
vaikuttavatkin lähes poikkeuksetta liittyvän tilanteisiin, joissa kirjoittaja
kuvaa jotakin topiikkia yhden päivän ajalle ulottuvalta ajanjaksolta.
Esimerkin \ref{ee_kiuruvesi} lisäksi tämä näkyy muun muassa seuraavassa virkkeessä:

\todo[inline]{vrt. spatiaaliset alatopiikit  @guijarro2001, 110, esimerkki 7}




\ex.\label{ee_pirttimaa} \exfont \small \emph{Kolmen maissa iltapäivällä} Pirttimaa hakee vaimonsa takaisin
kotiin.





\vspace{0.4cm} 

\noindent
Myös esimerkistä \ref{ee_pirttimaa} oli kirjoitushetkellä saatavilla laajempi
konteksti, josta käy ilmi, että tekstin varsinainen diskurssitopiikki on
henkilö nimeltä *Veijo Pirttimaa* ja tämän rooli vaimonsa omaishoitajana.
Kuten esimerkissä \ref{ee_kiuruvesi}, myös esimerkin \ref{ee_pirttimaa} tapauksessa
varsinaisesta diskurssitopiikista kerrotaan jaottelemalla se ajallisiin
segmentteihin:

\begin{quote}%
Töitä kellon ympäri

Pirttimaa on vaimonsa omaishoitaja ja käy lisäksi päivätöissä. Työpäivän
ajaksi Pirttimaa lähettää vaimonsa invataksilla hoitokotiin. Herätys on
joka aamu kello 4.30, sillä Pirttimaan työt alkavat jo kuudelta.
\emph{Kolmen maissa iltapäivällä} Pirttimaa hakee vaimonsa takaisin
kotiin.

http://www.studio55.fi/terveys/article/veijon-raskas-osa-dementia-muutti-vaimoni-taysin-vain-42-vuotiaana/126002
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
\todo[inline]{Jaottelua enemmän siitä, onko kyseessä ajallinen diskurssitopiikki vai esim. henkilö?}

Vastaava alatopiikkeihin nojaava tapa kertoa diskurssitopiikista näkyy myös seuraavissa
esimerkeissä:




\ex.\label{ee_jakkara} \exfont \small Kello kahden maissa mies kävi naisen kimppuun ja alkoi hakata tätä
metallisella jakkaralla. (Araneum FInnicum: sanasinko.net)






\ex.\label{ee_aik} \exfont \small Kymmenen maissa Aik herättää siipan. (Araneum Finnicum:
aistitasolla.blogspot.fi)






\ex.\label{ee_sopiimus} \exfont \small Yhdeksän maissa minä ja Ron kokoonnuimme konsulin ruokasalin pöydän
ääreen ja allekirjoitimme sopimuksen. (Araneum Finnicum:
pohjolafilmi.net)





\vspace{0.4cm} 

\noindent
Esitän, että esimerkkien \ref{ee_kiuruvesi} -- \ref{ee_sopiimus} 
tapauksessa ajanilmauksen alkusijainnin taustalla on kaikissa sama
*alatopiikkikonstruktio*, jonka avulla 
kirjoittaja kuvaa varsinaista diskurssitopiikkiaan jaottelemalla sen 
ajallisiin segmentteihin. Konstruktio on kuvattu tässä aineistossa tyypillisimmin
esiintyvässä muodossaan matriisissa 8:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{

D-topic   \#1 \\


\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & muu \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

\#1   \\[5pt]


gf subj \\[5pt]



prag \[ ds & act \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]


\vspace{3.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 8}: Alatopiikkikonstruktio \normalfont \vspace{0.6cm}

Matriisin 8 vasemmanpuolimmaisessa laatikossa
ajanilmauksen diskurssifunktioksi on merkitty *muu*. Tällä tarkoitetaan
sitä, että ajanilmauksen tehtävänä on nimenomaan tekstin jäsentely
eri segmentteihin ja näiden segmenttien rajojen merkitseminen [vrt. @shore2008,
58; @bestgen1995, 387]. Subjektista puolestaan on merkitty, että se on diskurssistatukseltaan
aktiivinen, mikä ei ole konstruktion kannalta välttämätöntä, mutta tavallisin tapa, jolla
alatopiikkikonstruktio tässä tutkimusaineistosta ilmenee. Tämä näkyy erityisesti
esimerkeissä \ref{ee_pirttimaa} -- \ref{ee_sopiimus}, joissa subjektit *Pirttimaa*,
*mies*,*Aik* sekä *minä ja Ron* ovat selkeästi aktiivisia ja edustavat oletettavasti kunkin
tekstin varsinaisia diskurssitopiikkeja. Jälkimmäinen ominaisuus on merkitty matriisissa
 8 indeksillä #1.[^pekkae]

[^pekkae]: Esimerkki \ref{ee_kiuruvesi} osoittaa, etteivät subjektin aktiivisuus ja
linkki diskurssitopiikkiin ole kuitenkaan konstruktion kannalta välttämättömiä.
Esimerkin subjektista *Kiuruveden kalastusalueen isännöitsijä Pekka
Eerikinharju* on kuitenkin todettava, että se on diskurssistatukseltaan
ankkuroitu: Koko tekstijakso kertoo Kiuruvedestä ja kalanistutuksesta, joten
*Kiuruveden kalastusalueen isännöitsijä* on tämän kehyksen kautta helposti
esiteltävissä lukijalle.


Matriisissa 8 kuvattu konstruktio vaikuttaa siis
hyvältä selitykseltä S1-aseman suosiolle suomen likimääräistä funktiota
edustavissa lauseissa. On kuitenkin muistettava, että likimääräisellä
funktiolla on S1-todennäköisyyttä kasvattava vaikutus nimenomaan suomessa
*verrattuna venäjään*. Ei ole mitään syytä epäillä, etteikö
alatopiikkikonstruktio olisi venäjässäkin tavallinen syyy ajanilmauksen
sijoittumiselle S1-asemaan: alempana muun muassa osioissa xx ja xx nähdään
useitakin venäjänkielisiä tapauksia, joissa (ks. mm. esimerkit xx,xx,xx)
selvästikin on kyse juuri alatopiikkikonstruktiosta. Se, miksei
alatopiikikonstruktio kasvata likimääräisen funktion S1-osuutta venäjässä
samassa suhteessa kuin suomessa, johtunee lopulta siitä, että suomen *viiden
maissa*-tyyppiset ilmaukset käyttäytyvät jossain määrin eri tavoin kuin venäjän
ilmaukset mallia *около пяти*. Yksi esimerkki tästä on virke \ref{ee_propazha}:




\ex.\label{ee_propazha} \exfont \small Но только \emph{около девяти} утра охранники обнаружили пропажу.
(RuPress: Komsomolskaja pravda)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_propazha} ajanilmauksen diskurssifunktio voidaan nähdä
fokaalisena: se kertoo, milloin vartijat havaitsivat katoamisen. Huomiota on
kiinnitettävä erityisesti ajanilmausta määrittävään *только*-sanaan, jonka
suomenkielisenä vastineena voidaan nähdä partikkeli *vasta*. Sellaiset
ilmaukset kuin *vasta viiden maissa* eivät kuitenkaan vaikuta suomessa samalla
tavoin yleisiltä kuin esimerkin \ref{ee_propazha} kaltaiset tapaukset venäjässä.
Toinen selitys voidaan havaita esimerkissä \ref{ee_stadion}:




\ex.\label{ee_stadion} \exfont \small Вчера \emph{около пяти} часов дня они могли увидеть друг друга на
стадионе «Трактор» (RuPress: Sovetski sport)





\vspace{0.4cm} 

\noindent
Suomessa *viiden maissa* -tyyppisiä ilmauksia ei samalla tavoin voi yhdistää
muiden ajanilmausten määritteeksi:[^maarsel] esimerkiksi 
*Eilen viiden maissa he näkivät toisensa* ei ole erityisen
todennäköinen virke suomessa. Kaiken kaikkiaan voidaan todeta, että tutkimusaineistoon
valitut suomen ja venäjän likimääräiset ilmaukset eroavat jossain määrin
toisistaan käyttönsä osalta, minkä seurauksena suomenkielisessä aineistossa
korostuvat S1-asemaa hyödyntävät alatopiikkikonstruktiot.
Likimääräisten ilmausten arvo tämän tutkimuksen kannalta ja syy niille
tässä kohdin annettuun huomioon onkin juuri siinä, että ne tarjoavat 
ikkunan siihen, miten tiukasti suomen alkusijaintitapaukset usein
ovat seurausta yhdestä tietystä konstruktiosta siinä missä venäjässä
syyt käyttää alkusijaintia ovat tavallisesti paljon moninaisemmat ja vaikeammin
tyhjentävästi määriteltävissä. Kuten tutkimuksen edetessä nähdään,
alatopiikkikonstruktio sinänsä on yksi tärkeimmistä suomen S1-tapauksia
selittävistä tekijöistä.

[^maarsel]: selitä, miksi tässä ei ole itsenäisyyden vaatimusta. Esim. siksi,
että olisi vaikea erottaa aineistosta...


\todo[inline]{Tarkista Chafe-viite}


#### Kehyksinen resultatiivinen ekstensio ja numeraalin läsnäolo {#kehres-s1}



Käsittelen kolmantena suomen S1-todennäköisyyttä kasvattavana tekijänä
numeraalin läsnäoloa ajanilmauksen sisältävässä lauseessa. Syynä 
tämän muuttujan sisällyttämiseen osaksi analyysia oli alun perin toinen
suomen S1-todennäköisyyteen positiivisesti vaikuttanut parametri, 
kehyksisen resultatiivisen ekstension semanttinen funktio. Kuten eri semanttisten funktioiden
kieli-interaktiota vertailevasta kuvioista 15 
nähdään, kehyksisen resultatiivisen funktion positiivinen vaikutus suomen
S1-todennäköisyyteen on yksi voimakkaimmista. Tätä semanttista funktiota
edustavat muun muassa seuraavat suomen S1-tapaukset:




\ex.\label{ee_vaikana} \exfont \small \emph{Viime vuoden aikana} työvoimaneuvojat kirjoittivat 11 500
työvoimapoliittista lausuntoa. (FiPress: Länsi-Savo)






\ex.\label{ee_15var} \exfont \small \emph{21 viime vuoden aikana} hän on viettänyt yli 15 vuotta
kotiarestissa. (Araneum Finnicum: maailma.net)






\ex.\label{ee_nokialiik} \exfont \small \emph{Viiden viime vuoden aikana} Nokia on kerännyt liikevoittoa 36
miljardia markkaa. (FiPress: Turun Sanomat)






\ex.\label{ee_itsm} \exfont \small Hallituksen tilastojen mukaan \emph{yhdentoista viime vuoden aikana} yli
180 000 intialaista maanviljelijää on tehnyt itsemurhan. (Araneum
Finnicum: global.finland.fi)






\ex.\label{ee_rahlaina} \exfont \small \emph{Viime vuoden aikana} kaupunki on antanut rahoituslainaa 350 000
markkaa. (FiPress: Länsi-Savo)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_vaikana} -- \ref{ee_rahlaina} ovat tyypillisiä resultatiivisten E3a- ja
E3b-ryhmien (ks. luku \ref{e3a-e3b}) edustajia suomen S1-aineistossa. Lauseista
on helppo huomata eräs säännönmukaisuus: jokainen tapaus käsittelee jollain
tavalla tietyn rajatun ajanjakson sisällä saavutettuja tai syntyneitä määriä --
kirjoitettuja työvoimapoliittisia lausuntoja, kotiarestissa vietettyjen vuosien osuutta,
kertynyttä liikevoittoa, tehtyjä itsemurhia ja myönnettyä rahoituslainaa. Joissakin tapauksissa
määrä on ilmaistu lauseen objektissa (kuten \ref{ee_vaikana}), joissakin objektin määreessä (kuten \ref{ee_nokialiik}), 
joissakin taas subjektissa (\ref{ee_itsm}). Näiden tapausten taustalla vaikuttaisi olevan oma, 
*määrää painottava konstruktionsa*, joka voidaan kuvata seuraavanlaisella matriisilla:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika, rajattu ajanjakso \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


val sem:määrä \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{1.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 9}: Määrää painottava konstruktio \normalfont \vspace{0.6cm} 

Matriisissa 9 lauseenalkuisen adverbiaalin
semanttisiksi ominaisuuksiksi on listattu paitsi ajallisuus sinänsä, myös
tarkempi huomautus siitä, että kyseessä on nimenomaan tavalla tai toisella
rajattu ajanjakso, jonka sisälle voidaan ajatella jokin tietyn tuloksen
käsittävä tapahtuma. Ajatus siitä, että jonkin konstituentin (joko subjektin,
objektin tai näiden määreen) on ilmaistava määrää, on kuvattu merkitsemällä
määrä osaksi verbin valenssia. Esimerkit \ref{ee_inspektsija} ja \ref{ee_sportsmeny}
osoittavat, että myös venäjässä on käytössä samanalainen konstruktio:




\ex.\label{ee_inspektsija} \exfont \small В течение года контрольно-ревизионный отдел министерства провел 41
инспекцию в своих образовательных учреждениях. (Araneum Russicum:
engsi.ru)






\ex.\label{ee_sportsmeny} \exfont \small В течение года спортсмены области приняли участие в 160 региональных,
всероссийских и международных соревнованиях, на которых ими было
завоевано 294 медали. (Araneum Russicum: zrpress.ru)





\vspace{0.4cm} 

\noindent
Suurinta osaa esimerkkejä \ref{ee_vaikana} -- \ref{ee_sportsmeny} yhdistää se, että
kirjoittaja suhtautuu mainitsemiinsa määriin koko lailla neutraalisti: kyseessä on nimenomaan
raportti siitä, mitä tietyn ajanjakson sisällä on tapahtunut. Tämä erottaa nyt
käsitellyt tapaukset varsinaista resultatiivista ekstensiota edustavista 
esimerkeistä \ref{ee_itavalta} ja \ref{ee_salaiset}, joita edellä tarkasteltiin affektiivisen
konstruktion edustajina -- näissä tapauksissa ajanilmausta seuraavassa propositiossa
oli aina jotakin ilmoitettuun ajanjaksoon nähden yllättävää tai muuten tunteita herättävää.
Rajankäynti ei tosin aina ole suoraviivaista, ja ainakin itsemurhista kertova 
esimerkki \ref{ee_itsm} on helppo nähdä myös jossain määrin affektiivisena.

Määrää painottava konstruktio on epäilemättä yksi selittävä tekijä sille, miksi
kehyksinen resultatiivinen ekstensio vaikuttaa suomessa S1-sijainnin todennäköisyyteen
positiivisesti. Se on edellä käsiteltyjen adverbi- ja alatopiikkitapausten
ohella yksi niistä selkeistä syistä, joiden läsnäolo vaikuttaisi olevan 
edellytys alkusijainnin käytölle suomessa. Koska määrän painottamisen
ei sinänsä pitäisi olla millään tavalla sidottu vain kehyksistä resultatiivista
ekstensiota edustaviin tapauksiin, voisi kuitenkin olettaa, että
konstruktion vaikutus näkyisi suomenkielisessä aineistossa laajemminkin.
Juuri tätä oletusta testaamaan tutkimuksen tilastolliseen malliin sisällytettiin
numeraalin läsnäoloa mittaava muuttuja.



Kuviosta 18 nähtiin, että 
numeraalin läsnäolo lauseessa todella lisää sen S1-todennäköisyyttä suomessa verrattuna
venäjään, vaikka tarkastelun kohteena olisi koko tutkimusaineisto. Karkeasti
ottaen numeraalin läsnäololla tarkoitetaan tässä tapauksia, joissa morfologinen
jäsennin on analysoinut jonkin ajanilmauksen sisältävän lauseen sanoista (pois lukien ajanilmaukseen itseensä kuuluvat sanat) 
sanaluokaltaan numeraaliksi ja joissa ajanilmausta seuraava sana on suomessa
partitiivissa tai venäjässä genetiivissä.[^partgensel] Numeraalisuuden määrittelevää algoritmia pyrittiin
lisäsi tarkentamaan edelleen esimerkiksi poislukemalla tapaukset, joissa numeraali liittyy johonkin
ajanilmaukseen tai muuten selvästi muuhun kuin määrään viittaavaan ilmaukseen.
Algoritmin tarkka toteutus on nähtävillä Github-palvelussa, osoitteessa 
https://github.com/hrmJ/phdR/blob/master/R/getting\_numeric\_cases.R (tarkistettu
22.2.2018).

[^partgensel]: Tällä haluttiin lisätä numeraalin läsnäolon liittymistä nimenomaan määrää painottavaan konstruktioon.
Niin suomen partitiivi kuin venäjän genetiivikin viittaavat yleensä numeraalin jälkeen siihen, että kyseessä
todella on tietty määrä jotakin entiteettiä, kun taas numeraalin jälkeiset muut sijat liittyät usein esimerkiksi
juuri ajan tai pelkän hinnan ilmaisemiseen. Toisaalta itsenäisinä esiintyvät numeraalit
liittyvät usein esimerkiksi urheilutuloksiin. Partitiivin/genetiivin vaatimisella pyrittiin siis lisäämään
haun tarkkuutta, eikä tästä mahdollisesti seurannutta kattavuuden laskua
katsottu tehtävien päätelmien kannalta haitaksi.


Jos tarkastellaan numeraalin sisältävien tapausten osuuksia kaikista tapauksista, voidaan
todeta, että kaiken kaikkiaan suomenkielinen aineisto sisältää
 3 222 numeeriseksi
merkittyä lausetta, mikä tarkoittaa
 3,71
prosenttia kaikista suomenkielisistä tapauksista; venäjänkielisessä aineistossa numeerisia tapauksia taas on 
 1 991, mikä vastaa
 2,72
prosenttia koko venäjänkielistä aineistoa. 

Numeraalin sijaitseminen lauseessa on tarkennetunkin algoritmin jälkeen
luonnollisesti ainoastaan välttämätön, ei riittävä ehto sille, että lause
todella edustaisi määrää painottavaa konstruktiota. Se, että numeeriset
tapaukset tilastollisen mallin mukaan nostavat suomen S1-todennäköisyyttä
verrattuna venäjään, on yhtä kaikki loogisimmin selitettävissä määrää
painottavan konstruktion vaikutuksena. Jos numeeristen tapausten suhteellista
osuutta tarkastellaan aineistoryhmätasolla, nähdään, että suurimmat osuudet
osuvat suomessa ryhmille L5a, E1b, E3b, L9b,  L9a ja F1a. Esimerkeissä
\ref{ee_toukokuusta} -- \ref{ee_opaskoira} on esitetty yksi numeerinen tapaus jokaisesta
ryhmästä:




\ex.\label{ee_saalisti} \exfont \small \emph{Viime vuonna} joukkue saalisti runkosarjassa 87 pistettä ja
pudotti Pittsburghin ensimmäisellä pudotuspelikierroksella. (FiPress:
Keskisuomalainen)






\ex.\label{ee_jooga} \exfont \small \emph{Viidessä vuodessa} äijäjooga on notkistanut tuhatkunta
suomalaismiestä. (Araneum Finnicum: sivistys.net)






\ex.\label{ee_pillit} \exfont \small \emph{Kahdeksan viime vuoden aikana} kuusi henkilöä on lyönyt pillit
pussiin. (Araneum Finnicum: suomenystavat.org)






\ex.\label{ee_toukokuusta} \exfont \small \emph{Viime vuoden toukokuusta} työttömien määrä on pienentynyt 29 200
henkilöä (FiPress: Länsi-Savo)






\ex.\label{ee_riistanhp} \exfont \small \emph{Vuonna 1999} Etelä-Savon riistanhoitopiiri myönsi siis 60
prosenttia enemmän hirven kaatolupia kuin vuonna 1998. (FiPress:
Länsi-Savo)






\ex.\label{ee_opaskoira} \exfont \small \emph{Joka vuosi} noin 30 näkövammaista saa avukseen Opaskoirakoulussa
koulutetun opaskoiran. (FiPress: Demari)





\vspace{0.4cm} 

\noindent
Kaikkia tässä lueteltuja esimerkkejä voidaan pitää määrää painottavina siitä
huolimatta, että ne edustavat eri semanttisia funktioita ja eri aineistoryhmiä.
Yhdistävä tekijä esimerkeille \ref{ee_toukokuusta} -- \ref{ee_opaskoira} kuitenkin on,
että kaikissa esiintyy sana *vuosi*. Voisikin ajatella, että vuosi on
tyypillisesti sellainen rajattu aika, josta tehdään sen kuluessa saavutettuja
tuloksia arvioivia katsauksia -- tämä pätee myös silloin, kun kyse on yhden
vuoden sijasta vuosista yleensä, kuten F1a-aineistoryhmässä (tarkemmin ks. osio
\ref{s1-f1a} myöhemmin tässä luvussa). Vastaavat katsaukset ovat omiaan
motivoimaan määrää painottavan konstruktion käyttämistä. 

S1-aseman käyttömahdollisuuksien laajuusero suomessa ja venäjässä tulee
hyvin esille, kun verrataan esimerkkien \ref{ee_toukokuusta} -- \ref{ee_opaskoira}
kaltaisten numeeristen tapausten osuutta kaikista tapauksissa.
Vertailu on esitetty taulukossa 4:


|       |       L5a|       L9b|       E1b|       E3b|       L9a|      F1a|
|:------|---------:|---------:|---------:|---------:|---------:|--------:|
|suomi  | 26.008969| 26.829268| 21.398305| 19.058296| 16.119403| 10.81081|
|venäjä |  9.227614|  1.960784|  4.215457|  1.960784|  3.594352|  5.83174|

\noindent \headingfont \small \textbf{Taulukko 4}: Numeraalin sisältävien tapausten osuus kaikista S1-tapauksista -- suomi ja venäjä vertailussa. \normalfont \vspace{0.6cm} 

Taulukosta nähdään, että tässä tarkastelluissa aineistoryhmissä suomen ja venäjän välillä
on huomattavia eroja siinä, miten suuri osa S1-tapauksista voidaan ainakin potentiaalisesti
selittää määrää painottavalla konstruktiolla. Venäjässä numeerisia S1-tapauksia ei kaikissa
tässä luetelluista ryhmistä ole ollenkaan ja enimmilläänkin tapauksia on L5a-ryhmässä
vajaat kymmenen prosenttia. Suomessa taas kaikissa tässä luetelluissa aineistoryhmissä
numeraali havaitaan pienimmillään noin kymmenessä prosentissa 
kaikkia S1-tapauksia (F1a-ryhmä), suurimmillaan lähes kolmanneksessa (L5a-ryhmä).
 Etenkin tässä kuvattujen 
*vuosi*/*год*-sanan sisältävien ryhmien osalta nähdään siis selvästi se, että
suomessa S1-sijainnin käyttötavat ovat venäjään verrattuna rajatummat, ja yksi tietty
konstruktio -- tässä määrää painottava -- saattaa olla syynä sille, että jossakin
aineistoryhmässä on tavallista enemmän S1-tapauksia.


\todo[inline]{Määrän ilmaisemisesta ja resultatiivisuudesta on vielä todettava eräs seikka, joka 
mahdollisesti vaikuttaa venäjän resultatiivisten ryhmien käyttäytymiseen. Venäjässä
nimittäin ovat mahdollisia sekä ilmaukset *за последние 5 лет* että *в течение последних пяти лет*.
Suomessa ilmaus *viidessä viime vuodessa* on puolestaan vähintäänkin epätodennäköinen, 
mikä osaltaan saattaa selittää sitä, että juuri kehyksisen ekstension ilmauksia käytetään
enemmän määrää painotettaessa.}


#### Sekventiaalinen funktio {#sekv-s1}

Käsittelen sekventiaalista funktiota viimeisenä niistä tekijöistä, jotka
kasvattavat S1-aseman todennäköisyyttä suomessa verrattuna venäjään. Ennen
erojen analysointia on kuitenkin huomattava, että kuvion 
10 perusteella sekventiaaliseen funktioon liittyy myös yksi
voimakkaimmista koko aineiston tasolla havaittavista S1-todennäköisyyttä
kasvattavista tendensseistä, mikä tarkoittaa, että suomen ja venäjän välillä 
todennäköisesti on myös selviä yhtäläisyyksiä sekventiaalisen funktion käytössä.
Kummassakin kielessä sekventiaalinen funktio oletettavasti kasvattaa S1-aseman
todennäköisyyttä, mutta kuvion 15 perusteella
kasvu on suomessa suhteessa voimakkaampaa.

Sekventiaalista funktiota edustavat tutkimusaineistossa ryhmät 
L6a ja L6b eli sellaiset ilmaukset kuin *kahden viikon päästä* ja *sodan
jälkeen* (ks. osio \ref{l6a-l6b}). Tarkastelen ensiksi mainittua aineistoryhmää  lähemmin esimerkkien
\ref{ee_utonul} ja \ref{ee_vahvisti} valossa:





\exg.  Только \emph{через 10 лет} мы получили извещение о том, что он утонул на
рыбалке. (Araneum Russicum: markday.ru)
\label{ee_utonul}  
 vasta 10-v.-kuluttua me saada-PRET viesti PREP se-PREP että hän hukkua-PRET PREP kalastusreissu
\





\ex.\label{ee_vahvisti} \exfont \small \emph{Parin päivän päästä} erikoislääkäri vahvisti diagnoosin. (Araneum
Finnicum: zeninan.com)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_utonul} ja \ref{ee_vahvisti} tuovat hyvin esiin sekventiaalisten
ilmausten erityislaatuisuuden suhteessa referentiaalisuuden käsitteeseen.
Nämä ilmaukset erottaa monista muista se, miten ne ankkuroituvat
viestijöiden mielessä olevalle aikajanalle. Sekventiaalisista
ilmauksista voidaan nimittäin todeta

1. ettei niiden ankkuripisteenä ole puhehetki 
2. ettei niiden ankkuripisteenä ole mikään absoluuttisen aikajanan piste (kuten vuosi 1987).

Kuten osiossa \ref{ei-deiktiset-referentiaaliset-ajanilmaukset} esitettiin,
esimerkkien \ref{ee_utonul} ja \ref{ee_vahvisti} voi nähdä ankkuroituvan
suhteessa *ajalliseen topiikkiin*  eli TT:hen [@klein1999, 5], joka on muodostettu edeltävässä 
tekstissä. Esimerkin \ref{ee_utonul} tapauksessa laajempi konteksti näyttää 
seuraavalta:

\begin{quote}%
Мой непутёвый отец хоть и не пил спиртное и не курил, но имел две другие
страсти – рыбалка и конфеты. Испытав «прелести» семейной жизни в
питерской коммуналке, он бросил мою беременную мать и исчез. Только
через 10 лет мы получили извещение о том, что он утонул на рыбалке.

https://www.armadaboard.com/topic42118.html, 18.1.2018
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_utonul} laajemmasta kontekstista nähdään, että ajallinen topiikki (TT), johon itse 
esimerkissä viitataan, muodostetaan virkkeessä *испытав прелести семейной жизни\-\-он бросил мою
беременную мать и исчез*. Lukija muodostaa tämän virkkeen kohdatessaan
mielessään kuvan siitä, että virkkeessä kuvattu
traaginen tapahtuma on sattunut jossain tietyssä kohtaa menneisyyttä. Ilmaus *только
через 10 лет* suhteuttaa seuraavat tapahtumat tähän ajalliseen kehykseen, toisin sanoen TT:hen.

Suomenkielisen  esimerkin laajempi konteksti puolestaan näyttää tältä:

\begin{quote}%
Vauvoilla takaisinvirtaus sinänsä on normaalia, mutta häiritsevänä ja
kipuilevana sekä yhdessä muiden oireiden kanssa, se ei ole normaalia.
Eräänä unettomana yönä mies sen sitten googletti: hiljainen refluksi.
\emph{Parin päivän päästä} erikoislääkäri vahvisti diagnoosin. Mikä
helpotus; oireilla on nimi ja nyt myös hoitokeinot!

http://zeninan1.rssing.com/chan-11859303/all\_p7.html
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Aivan kuten venäjänkielisessä tapauksessa edellä, myös tässä ajallinen topiikki
muodostetaan varsinaista esimerkkiä edeltävässä virkkeessä. Edeltävän virkkeen
*eräänä yönä* saa lukijan muodostamaan käsityksen aikajanalla jossain kohtaa
menneisyyttä sijaitsevasta TT:stä, johon seuraavan virkkeen *parin päivän
päästä* ankkuroidaan. 

Ajanilmauksen diskurssifunktio ei esimerkeissä \ref{ee_utonul} ja \ref{ee_vahvisti}
määräydy ensisijaisesti informaatiorakenteen perusteella siinä mielessä,
että ajanilmausta itseään voisi pitää virkkeen varsinaisena topiikkina tai fokuksena.
Ennemminkin kyse on siitä, että sekventiaaliset ajanilmaukset nimensä 
mukaisesti suhteuttavat toisiinsa peräkkäisiä hetkiä tai ajanjaksoja --
tämänkaltainen diskurssifunktio muistuttaa edellä *äkkiä*-konstruktion yhteydessä
käsiteltyä. Esimerkkien \ref{ee_utonul} ja \ref{ee_vahvisti} taustalla
voidaan nähdä molemmissa kielissä tavallinen ja pitkälti samanlainen 
konstruktio, jota nimitän *sekventiaaliseksi konstruktioksi*.



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]



prag \[ df & muu \\
ref & TT \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{3.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 10}: Suomen ja venäjän sekventiaaliset konstruktiot \normalfont \vspace{0.6cm} 

Tässä esitetty matriisi sisältää uutena piirteenä prag-piirrekimppuun liitetyn
ref-attribuutin, jolle on annettu arvo TT. Tällä merkitään sitä konstruktion
ominaisuutta, että ajanilmauksen referenttinä on edellisessä diskurssissa muodostettu
ajallinen topiikki. Se, että sekventiaalisessa konstruktiossa alkusijainti on seurausta nimenomaan viittauksesta
edellisessä virkkeessä muodostettuun TT:hen, käy selvemmäksi, jos tarkastellaan
virkkeen \ref{ee_vahvisti} ajanilmausta esimerkiksi suomessa  tavallisemmassa 
S3-sijainnissa:

\begin{quote}%
Eräänä unettomana yönä mies sen sitten googletti: hiljainen refluksi.
Erikoislääkäri vahvisti \emph{parin päivän päästä} diagnoosin.
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Muokattu esimerkki on ainakin oman intuitioni mukaan kömpelö: tekstin koheesio
rikkoontuu pahasti, kun ajanilmauksen siirtää pois alkusijainnista. Tässä valossa
onkin hyvin ymmärrettävää, että sekventiaaliset ilmaukset -- ainakin aineistoryhmän L6a
osalta -- nostavat S1-sijainnin todennäköisyyttä suomessa jopa venäjää enemmän.
Oma kysymyksensä on toinen sekventiaalinen aineistoryhmä L6b eli ilmaukset *sodan jälkeen/после
войны*. Näillä ankkuripiste toki saattaa olla edellisessä virkkeessä muodostettu
referentti (mikäli juuri edellisessä virkkeessä on puhuttu jostakin tietystä
sodasta), mutta myös jokin *objektiivisen aikajanan* (ks. luku
\ref{ei-deiktiset-referentiaaliset-ajanilmaukset}) piste: molempien viestijöiden
tunnistama sota -- kenties tavallisimmin toinen maailmansota. 

Sekventiaalisen S1-konstruktion voi itse asiassa nähdä koskevan muitakin kuin suoraan
sekventiaaliseksi merkittyjä aineistoryhmiä. Luvussa 
\ref{ei-deiktiset-referentiaaliset-ajanilmaukset} tehtiin ero TT:n perusteella
ankkuroivien ilmausten (kuten *parin päivän päästä*) ja *anaforisesti* ankkuroivien
ilmausten välillä. Jälkimmäistä referentiaalisuuden tyyppiä edustavat
tutkimusaineistossa ryhmät L7a, L7b ja L7c (ks. osio
\ref{l7a-l7b-l7c-l7d}). Tutkitaan tarkemmin muutamaa esimerkkiä näistä
aineistoista:




\ex.\label{ee_edellinenlaina} \exfont \small Edellisen kerran vastaavaa lainaa myönnettiin joulukuussa 2011,
\emph{silloin} pankit ottivat lainarahaa 489 miljardin euron arvosta.
(Araneum Finnicum: essee.wordpress.com)






\ex.\label{ee_guinnespata} \exfont \small Olen kuullut irkkupubeissa tehtävän Guinness-pataa, ja \emph{siitä asti}
ajatus onkin vainonnut minua. (Araneum Finnicum: blogit.mtv.fi)






\ex.\label{ee_pakit} \exfont \small \emph{Siihen aikaan} pakit yleensä asettelivat kiekkoa ja virittelivät
lämäreitään sekuntikaupalla. (Araneum Finnicum: kiekkoareena.fi)






\ex.\label{ee_gruppa} \exfont \small Группа была образована в 1998 году, тогда же ребята подписали контракт с
компанией EMI Records. (RuPress: РИА Новости)






\ex.\label{ee_kollegi} \exfont \small Коллеги избрали ее председателем профкома в 1996 году, и \emph{с тех
пор} Наталья Александровна успешно совмещает обязанности учителя химии с
деятельностью профсоюзного лидера. (Araneum Russicum: ug.ru)






\ex.\label{ee_skotovodstvo} \exfont \small \emph{В то время} жители этих мест занимались в основном скотоводством,
земледелием. (Araneum Russicum: himki-nedv.ru)





\vspace{0.4cm} 

\noindent
Loppujen lopuksi ero aineistoryhmien L7a, L7b ja L7c ja
sekventiaalisen L6a-ryhmän referenttien välillä on siinä, että ilmauksissa
*siitä asti*, *siihen aikaan* ja *silloin*[^sillointark] viittauskohteena olevaa ajanjaksoa
käsitellään konkreettisesti pronominin korrelaattina, kun taas
L6a-ryhmän tapauksessa viittaus on hivenen implisiittisempi: lukijan 
tai viestin vastaanottajan pitää ikään kuin itse täyttää aukko *parin päivän
päästä* -ilmauksen ja sen viitepisteenä olevan ajanjakson välillä [vrt.
@mcnamara2013, x]. Kaikissa tapauksissa tätäkään eroa ei kuitenkaan ole,
vaan myös anaforisissa lauseissa ajanilmauksen referentti
voi tulla muodostetuksi vähemmän konkreettisesti. Näin on ennen kaikkea
esimerkissä \ref{ee_pakit}, jonka tarkempi konteksti on esitetty
alla:

[^sillointark]: *Silloin*- ja тогда-sanoihin (esimerkit \ref{ee_edellinenlaina} ja
\ref{ee_gruppa}) liittyy monia erityispiirteitä ja määrittelyvaikeuksia, joita on
tarkemmin käsitelty luvussa \ref{l7l8-erot-kesk}. 


\begin{quote}%
Lukko saavutti ensimmäisen aikuisten SM-mitalinsa - hopeisen - kaudella
1960-61, jolloin se vielä pelasi luonnonjäällä Lonsissa lähellä
silloista Rauma-Repolan tehdaskompleksia. Sillä kaudella näin
ensimmäistä kertaa katsomosta Teppo Rastion taituruuden. Hän pelasi
puolustajana, teki peliä kuin rakennusmestari taloa, piti kiekkoa
hallussaan, puijasi ja psyykkasi vastustajia ja syötteli
millimetrintarkasti.-\/-Siihen aikaan pakit yleensä asettelivat kiekkoa
ja virittelivät lämäreitään sekuntikaupalla.

http://kiekkoareena.fi/kolumnit/mestarien-mestari
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_pakit} laajemmasta kontekstista käy ilmi, että ajallinen topiikki
muodostetaan kappaleen alussa virkkeellä *Lukko saavutti ensimmäisen \-\-
SM-mitalinsa \-\- kaudella 1960-61.* Tämän virkkeen ja ilmauksen *siihen
aikaan* välissä on muita virkkeitä, niin ettei viittaus seuraa välittömästi
viittauspisteen muodostamista. Huomionarvoista kyllä, ennen esimerkkiä \ref{ee_pakit}
kappaleen alussa muodostettuun TT:hen viitataan myös toisella lauseenalkuisella
anaforisella ilmauksella, *sillä kaudella*.

Esimerkin \ref{ee_pakit} voidaan kuitenkin tulkita myös eroavan esimerkistä
\ref{ee_vahvisti}. Esimerkissä \ref{ee_vahvisti} on vaikea puhua ajanilmauksen 
informaatiostatuksesta, koska ajanilmauksen tehtävä on ennen kaikkea
ilmaista ajanilmauksen referenttinä olevan TT:n ja itse lauseen ajallista 
peräkkäisyyssuhdetta. Esimerkin \ref{ee_pakit} kohdalla voidaan kuitenkin
sanoa, että koko propositio [pakit asettelivat kiekkoa ja virittelivät
lämäreitään sekuntikaupalla] esitetään nimenomaan viittauskohteena olevasta
ajanjaksosta itsestään -- lause kertoo *siitä ajasta*, ja ajanilmauksella on
siis Lambrechtin perusmääritelmän [-@lambrecht1996, 118] mukainen topiikin diskurssifunktio.
Erotankin esimerkin \ref{ee_pakit} ilmentämän konstruktion matriisissa
 10 esitetystä sekventiaalisesta konstruktiosta ja tulkitsen
 sen ennemminkin matriisissa 11 kuvatun
 *topikaalisen konstruktion* edustajaksi:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]



prag \[ df & top \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{2.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 11}: Topikaalinen konstruktio. \normalfont \vspace{0.6cm} 

Matriisien 10 ja 11 erona
on, että ajanilmauksen diskurssifunktiona ei ole *muu* vaan topiikkiin
viittaava *top*. Huomattakoon, ettei
ajanilmauksen referentin näissä tapauksessa ole välttämätöntä olla juuri
TT, vaan se voi olla myös muulla tavalla muodostettu, johonkin ajalliseen
referenttiin viittaava topiikki.

Käsitys topikaalisesta konstruktiosta tarkentuu, kun tutkitaan myös
L7d-aineiston ilmauksia (esim. *sellaisina hetkinä*). Kuten esimerkit
\ref{ee_pariskunnat} -- \ref{ee_mentalnojetelo} osoittavat, näissä on vielä suuremmassa
määrin kyse *tietynlaisten hetkien* kuvaamisesta, toisin sanoen ajallisista
referenteistä, jotka muodostavat lauseen topiikin.




\ex.\label{ee_pariskunnat} \exfont \small Kuitenkin, jos elokuvista on kyse, niin \emph{tällaisina hetkinä}
pariskunnat katsovat ensin toisiaan rakastuneesti, he tarttuvat toisiaan
käsistä ja molemmat itkevät hellästi ja pyyhkivät toistensa kyyneleet.
(Araneum Finnicum: inthecity.fi)






\ex.\label{ee_mentalnojetelo} \exfont \small \emph{В такие моменты} вы можете ощутить себя чем-то вроде ментального
тела, плавающего в физическом мире. (Araneum Russicum: sleeplive.ru)





\vspace{0.4cm} 

\noindent
\todo[inline]{ remarkable: ota loppusijainti-kappaleessa kantaa siihen, miten kivasti nämä toimivat
lopussakin: Siksi minä puolustan heitä tällaisina hetkinä. }

Esimerkkien \ref{ee_pariskunnat} ja \ref{ee_mentalnojetelo} topikaalisuus
tulee esiin siinä, että kumpikin niistä on mahdollista  parafraseerata 
rakenteella *Mitä tällaisiin hetkiin tulee...* [vrt. @tobecited].
Näissä tapauksissa ero sekventiaaliseen konstruktioon
on melko selvä, joskin yhdistävänä tekijänä on edelleen se, että
ajanilmaukset on ankkuroitu tavalla tai toisella edeltävässä 
kontekstissa muodostetulle TT:lle.


### Tekijät, jotka pienentävät S1-todennäköisyyttä suomessa verrattuna venäjään  {#pien-s1}

Osion \ref{s1-eri} alussa kielen ja semanttisen funktion välisestä
interaktiosta tehtävät päätelmät jaettiin kolmeen joukkoon: (1) kielen suhteen
neutraaleihin vaikutuksiin, (2) niihin, joissa suomeen kohdistuu (voimakkaampi)
S1-todennäköisyyttä kasvattava vaikutus ja (3) niihin, joissa suomeen kohdistuu
(voimakkaampi) S1-todennäköisyyttä pienentävä vaikutus. Edellisessä osiossa
tutkin toista joukkoa, jonka avulla voitiin tehdä joitakin päätelmiä ennen
kaikkea siitä, minkälaiset S1-konstruktiot ovat suomessa keskimääräistä
tavallisempia. Saadut tulokset viittasivat pitkälti konstruktioihin, jotka
ovat kutakuinkin samanlaisina käytössä sekä suomessa että venäjässä mutta
joiden merkitys vain suomessa korostuu, koska S1-sijainti ei muuten ole
erityisen yleinen. Kolmatta joukkoa tarkastelemalla voidaan kuitenkin saada
eksplisiittisemmin tietoa siitä, mitä ovat ne S1-asemaan liittyvät
konstruktiot, joille suomessa ei varsinaista vastinetta ole tai jotka syystä
tai toisesta ovat suomessa venäjää harvinaisempia.

Kuvion 15 perusteella S1-todennäköisyyttä
suomessa pienentävät semanttisista funktioista selvimmin 
sekventiaalis-duratiivinen ja  duratiivinen funktio sekä F-funktiot, mutta 
jossain määrin myös simultaaniset tapaukset. Näiden lisäksi
selvä pienentävä vaikutus havaittiin kuvioiden 17 
ja 16 perusteella ilmauksen positionaalisuudella
ja morfologisen rakenteen määrittelemisellä deiktiseksi adverbiksi. Aloitan
suomen S1-todennäköisyyttä laskevien tekijöiden tarkastelun kahdesta viimeksi
mainitusta tapausjoukosta.

#### Positionaalisuus {#s1-sim-pos}

Kuvion 17 mukaan positionaalisuuden (ks. osio \ref{pot-kal}) ja kielen
välillä on interaktio, niin että suomessa positionaalisuus pienentää S1-todennäköisyyttä
verrattuna venäjään. Positionaalisia ilmauksia tutkimusaineistossa edustavat aineistoryhmät
F1b (ks. osio \ref{f1a-f1b}), L2a--b (\ref{l2a-l2b}) ja L4a (\ref{l4a-l4b}).
Jos näitä aineistoryhmiä katsotaan tarkemmin, havaitaan, että ryhmien välillä
on melko selkeitäkin eroja siinä, kuinka yleinen alkusijainti niissä on. Venäjässä
F1b-ryhmä on ainoa, jossa alkusijaintitapausten osuus on selvästi alle koko aineiston keskiarvon;
suomessa taas ainoastaan L4a-ryhmässä alkusijaintitapauksia on enemmän kuin
mitä koko aineiston tasolla keskimäärin.
Kuvio 21 tiivistää ryhmäkohtaiset S1-osuudet ja niiden
asettumisen suhteessa koko aineiston keskiarvoon. Aivan kuten kuviossa 19 edellä,
myös tässä tummempi katkoviiva merkitsee S1-tapausten keskimääräistä
osuutta koko aineiston tasolla suomessa ja vaalea katkoviiva 
vastaavaa keskimääräistä osuutta venäjässä.

![\noindent \headingfont \small \textbf{Kuvio 21}: Positionaaliset aineistoryhmät ja suhteellinen S1-osuus suomessa ja venäjässä. \normalfont](figure/pos.erot.s1-1.pdf)

Kuviosta 21 nähdään muun muassa se, että viikonpäiviä
sisältävät ilmaukset (ryhmät F1b ja L2a) sijoittuvat kummassakin kielessä keskimääräistä 
harvemmin S1-asemaan. Suomen L2a-ryhmässä alkusijainti on erityisen epätavallinen:
siinä S1-osuus on vain 3,15 prosenttia, mikä
on noin kymmenen kertaa vähemmän kuin vastaava venäjän osuus (33,12 %). 
Myös L2b-ryhmässä suomen ja venäjän aineistoryhmien välinen suhteellinen ero on 
melko suuri (yli kuusinkertainen) verrattuna L4a-ryhmään (noin nelinkertainen)
ja F1b-ryhmään (noin kolminkertainen).
Koska F1b-ryhmää tarkastellaan lähemmin luvussa \ref{s1-f1a} F-funktioiden
vaikutuksen yhteydessä ja L4a-ryhmään syvennytään tarkemmin loppusijainnin
yhteydessä luvussa \ref{deiktiset-adverbit-ja-positionaalisuus},
keskitän positionaalisten ilmaisujen tarkasteluni seuraavassa 
L2a- ja L2b-ryhmiä edustaviin tapauksiin.




Suomen L2a- ja L2b-ryhmiin (jatkossa myös lyhyesti *L2-ryhmiin*) kuuluu kaiken kaikkiaan 7 867
tapausta, joista S1-asemaan sijoittuu 500. Venäjän vastaavat luvut
ovat 4 073 (kokonaismäärä)
ja 1 753 (S1-tapaukset).
Suomen osalta kyseessä on siis kohtalaisen pieni ryhmä, jolle tyypilliset piirteet
on mahdollista eritellä melko tarkastikin. Lähden liikkeelle näiden piirteiden
kartoittamisessa tutkimalla esimerkkejä
\ref{ee_kipinavartti} ja \ref{ee_maunuvaara}:




\ex.\label{ee_kipinavartti} \exfont \small Helmikuussa toteutettiin Kipinävartti, \emph{maaliskuussa} palaveri
vanhempainyhdistyksen kanssa tuotti kaksi mediapysäkkiä Suosmereen.
(Araneum Finnicum: meka.tv)






\ex.\label{ee_maunuvaara} \exfont \small Tiistaina, keskiviikkona ja \emph{torstaina} aamulla klo 8-9 välillä
Tapio Maunuvaara soittaa trumpetillaan serenadeja aamuvirkuille.
(Araneum Finnicum: victoriamedia.fi)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_kipinavartti} ja \ref{ee_maunuvaara} voidaan tulkintani mukaan analysoida 
alatopiikkikonstruktion ilmentymiksi. Tähän mennessä käsitellyistä alatopiikkirakenteista esimerkeissä
\ref{ee_kiuruvesi} ja \ref{ee_pirttimaa} yhtä päivää on käytetty sinä ajallisena kokonaisuutena,
josta lohkottujen segmenttien valossa kirjoittaja on
kertonut jotakin tekstin varsinaisesta diskurssitopiikista, esimerkkien \ref{ee_kiuruvesi}
ja \ref{ee_pirttimaa} tapauksessa 
ankeriaista tai omaishoitajasta. Kuten edellä mainittiin, hyvin usein ajallisiin
segmentteihin pohjaavia alatopiikkeja käytetään myös kerrottaessa jonkun 
henkilön elämästä, jolloin ylemmän tason ajallisen kehyksen muodostaa huomattavasti
pidempi, kymmeniä vuosia kattava ajanjakso.

Positionaalisten ajanilmauksien erityispiirteenä vaikuttaisi olevan, että niissä
on ikään kuin sisäänrakennettuna ylemmän tason ajallinen kehys: kuukausien tapauksessa vuosi, viikonpäivien
tapauksessa viikko. Tämä käy hyvin ilmi, kun katsotaan esimerkin \ref{ee_kipinavartti}
laajempaa kontekstia. Tekstin otsikko kuuluu
*Minne meri katosi ja muita tarinoita Suosmerestä 2006*, mikä
antaa ymmärtää, että tekstissä käsitellään nimenomaan yhtä tiettyä vuotta (2006):

\begin{quote}%
Suistomaat iski Suosmereen kyläillan merkeissä tammikuussa 2006, jolloin
kartoitettiin kylän mahdollisuuksia. \emph{Helmikuussa} toteutettiin
Kipinävartti, \emph{maaliskuussa} palaveri vanhempainyhdistyksen kanssa
tuotti kaksi mediapysäkkiä Suosmereen. Pysäkit toteutettiin maaliskuun
ja toukokuun välisenä aikana. Kaikkiaan pysäkeillä tehtiin kuusi
elokuvaa, jotka nähtiin ensi-illassa Suosmeren koululla toukokuussa.

http://meka.tv/suistomaat/ulvila/suosmeri.htm
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin  \ref{ee_maunuvaara} laajempi konteksti puolestaan paljastaa, että 
katkelmassa mainittu trumpetin soitto liittyy
ruusuja Oulun keskustaan vuonna 2013 tuoneeseen taideteokseen, joka on ollut
esillä viikon ajan.

\begin{quote}%
Kaisa Salmen ruusuteos valtasi Oulun keskustan kadut

Ruusuteos on avoinna 26.8.-1.9 välisen ajan. Viikon ajan ruusuteoksessa
on monenlaista ohjelmaa. Tiistaina, keskiviikkona ja torstaina aamulla
klo 8-9 välillä Tapio Maunuvaara soittaa trumpetillaan serenadeja
aamuvirkuille. Keskiviikkona 28.8. tanssija Pirjo Yli-Maunula tanssii
ruuhka-aikoina liikennevaloissa. Perjantaina ja lauantaina lounasaikaan
klo 11-13 välillä ihmiset voivat rentoutua riippumatoissa kuunnellen
jousitrion soittamaa klassista musiikkia. Taiteilija Kaisa Salmi antaa
samalla rentoutushoitoja. Sunnuntaina 1.9. klo 10 alkaen ruusujuurakot
jaetaan maksutta ihmisille.
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkeissä \ref{ee_kipinavartti} ja \ref{ee_maunuvaara} ajanilmauksen diskurssifunktio
on sikäli erilainen kuin muissa edellä kuvatuissa alatopiikkitapauksissa,
että siinä laajempi ajallinen kehys -- vuosi tai viikko -- on lopulta samalla
myös se topiikki, jota
eri ajanilmaukset pilkkovat osiin. Toisin kuin vaikka esimerkissä \ref{ee_pirttimaa},
lauseen subjekti ei ole diskurssistatukseltaan aktiivinen topiikki, josta 
lukijalle kerrottaisiin fokuksena jokin uusi tieto, vaan fokuksena on itse 
lauseessa ilmaistava propositio, jokin tapahtuma ([järjestettiin Kipinävartti],[Tapio Maunuvaara soittaa serenadeja]).
Ajanilmaukselle itselleen -- tarkimmin sanottuna sen referentille --
jää varsinaisen alatopiikin rooli, eikä pelkästään tekstin eri segmenttejä erotteleva 
tehtävä. Tätä rakennetta voidaan oikeastaan pitää 
varsinaiseen alatopiikkikonstruktioon linkittyneenä omana konstruktionaan,
jota nimitän *ajalliseksi alatopiikkikonstruktioksi* ja joka on kuvattu
tarkemmin matriisissa 12:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{

D-topic   \#1 \\
sem   aika \\


\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & subtop \\
D-topic & \#1 \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

df   foc \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & act- \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{2.5em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 12}: Ajallinen alatopiikkikonstruktio \normalfont \vspace{0.6cm}

Matriisissa 12 käynnissä olevan tekstijakson
diskurssitopiikista on merkitty, että se on nimenomaan ajallinen (esimerkissä \ref{ee_kipinavartti} vuosi,
esimerkissä \ref{ee_maunuvaara} viikko). Ajanilmauksen
diskurssifunktioksi on puolestaan merkitty alatopiikki, jonka yhteys 
ajalliseen diskurssitopiikkiin on ilmipantu indeksimerkinnällä. Ajallisen diskurssitopiikin
lisäksi tämän konstruktion ilmentymissä  voi implisiittisemmin olla läsnä myös
jokin koko tekstin kannalta oleellisempi diskurssitopiikki, jollaiseksi
esimerkin \ref{ee_kipinavartti} tapauksessa on helppo erottaa Suosmeri. Ajallisen
alatopiikkikonstruktion ja tavallisen alatopiikkikonstruktion välinen
ero ei monesti olekaan helposti määriteltävissä, ja
viittaankin jatkossa alatopiikkikonstruktioista puhuessani sekä varsinaiseen
alatopiikkikonstruktioon että matriisissa 12 esitettyyn, hyvin lähellä
varsinaista alatopiikkikonstruktiota olevaan ajalliseen alatopiikkikonstruktioon, joka
on tyypillinen juuri positionaalisille ilmauksille.

Paitsi alatopiikkikonstruktio, positionaalisiin ilmauksiin liittyy 
olennaisesti myös edellä matriisissa 7 esitelty 
kontrastiivinen konstruktio. Esimerkit \ref{ee_kasvuarvio} ja \ref{ee_kolmevanhempaa}
vertautuvatkin hyvin ei--deiktisten adverbitapausten yhteydessä esitettyihin esimerkkeihin 
\ref{ee_joskuspaiva} ja \ref{ee_aikaharvoin}:




\ex.\label{ee_kasvuarvio} \exfont \small \emph{Joulukuussa} 2011 markkinointiviestinnän alan yritykset arvioivat
tämän vuoden kasvuksi -0, 2 prosenttia, ja \emph{maaliskuussa} ne
ennustivat 2, 7 \%:n kasvua. (Araneum Finnicum: julkaisija.fi)






\ex.\label{ee_kolmevanhempaa} \exfont \small Brittituomioistuin peruutti joulukuussa jenkkinaisen karkotuksen, koska
tämä on efektiivisesti kolmen hengen avioliitossa brittien kanssa;
\emph{maaliskuussa} toinen määräsi lapselle kolme vanhempaa, lesboparin
ja homoisän. (Araneum Finnicum: vapaasana.net)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_kasvuarvio} ja \ref{ee_kolmevanhempaa} erottaa alatopiikkikonstruktiota
edustavista esimerkeistä \ref{ee_kipinavartti} ja \ref{ee_maunuvaara} se, ettei vuosi ole
niissä ylemmän tason topiikki, jota ajanilmausten avulla segmentoitaisiin, vaan
esimerkeissä mainitut kuukaudet ovat itsenäisiä diskurssireferenttejä, jotka
rinnastetaan toisiinsa. Niin esimerkissä \ref{ee_kasvuarvio} kuin esimerkissä
\ref{ee_kolmevanhempaa} tämä käy epäsuorasti ilmi siitä, että rinnastettavia
ajanilmauksia on lueteltu järjestyksessä *joulukuu*--*maaliskuu*, mikä antaa
syyn olettaa, että ajanilmausten viittauskohteet kuuluvat itse asiassa eri
vuosiin. 




Ajallinen alatopiikkikonstruktio ja kontrastiivinen konstruktio vaikuttavat päällisin
puolin kenties kaikkein tavallisimmilta  syiltä sijoittaa
suomen L2-ajanilmaus S1-asemaan. Näiden konstruktioiden yleisyyttä voidaan arvioida eksaktimmin
tutkimalla, kuinka suuri osa aineiston lauseista sisältää 
esimerkkien \ref{ee_kipinavartti} -- \ref{ee_kolmevanhempaa} tavoin vähintään kaksi 
viikonpäivää tai kuukauden nimeä. Suomen L2a-aineistosta tällaisia tapauksia
on 30,50 ja L2b-aineistosta 15,32 prosenttia. 
Kiinnostavaa kyllä, venäjässä vastaavien tapausten osuus on selvästi
pienempi: L2a-ryhmässä 6,66 ja L2b-ryhmässä 6,57
prosenttia. Esimerkki \ref{ee_vsjanedelja} edustaa venäjän ajallista alatopiikkikonstruktiota,
esimerkki \ref{ee_initsiroval} puolestaan kontrastiivista konstruktiota:




\ex.\label{ee_vsjanedelja} \exfont \small Вся неделя разбита на различные тематические дни: во вторник вас ждет
школа мам, в среду череда познавательных лекций, \emph{в четверг} вы
сможете посмотреть кино на оригинальном языке, а в пятницу и субботу
стать участником городского события и заняться спортом. (Araneum
Russicum: droogie.ru)






\ex.\label{ee_initsiroval} \exfont \small Так, \emph{в марте} Медведев инициировал начало процесса передачи в
собственность церкви недвижимости - храмов и земельных участков, а
\emph{в октябре} пообещал придать государственный статус дипломам
духовных школ и уче ным степеням богословов. (Araneum Russicum:
humanism.al.ru)





\vspace{0.4cm} 

\noindent



Vaikka niin suomessa kuin venäjässä L2-ryhmien tapaiset positionaaliset ilmaukset voivat 
esiintyä kontrastiivisissa ja alatopiikkikonstruktioissa, venäjässä ne muodostavat
suomeen verrattuna vain murto-osan kaikista alkusijaintitapauksista. Lisää merkillepantavia
eroja kielten L2-ryhmien koostumuksessa nähdään, jos vertaillaan loppujen L2-tapausten
(niiden, joissa esiintyy ainoastaan yksi viikonpäivä tai kuukausi)
kollokaatteja eli sanoja, joiden yhteydessä ajanilmaukset esiintyvät [@tobecited].
L2a-ryhmän osalta kymmenen yleisintä vasemmanpuoleista kollokaattia
on esitetty taulukossa 5:

\FloatBarrier

\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Var1 & Freq & percent\\
\midrule
tänään & 10 & 10.20\\
viime & 10 & 10.20\\
ja & 6 & 6.12\\
mutta & 6 & 6.12\\
sillä & 5 & 5.10\\
\addlinespace
viimeksi & 5 & 5.10\\
eilen & 4 & 4.08\\
jo & 4 & 4.08\\
vielä & 4 & 4.08\\
aiemmin & 3 & 3.06\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Var1 & Freq & percent\\
\midrule
--- & 577 & 67.49\\
что & 22 & 2.57\\
а & 19 & 2.22\\
вечером & 11 & 1.29\\
ранее & 11 & 1.29\\
\addlinespace
уже & 11 & 1.29\\
однако & 9 & 1.05\\
утром & 9 & 1.05\\
так & 6 & 0.70\\
вот & 5 & 0.58\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 5}: L2a-aineistoryhmän S1-tapausten yleisimmät vasemmanpuoleiset kollokaatit suomessa ja venäjässä \normalfont \vspace{0.6cm} 

\FloatBarrier

Venäjän osalta taulukko 5 on silmiinpistävän yksipuolinen: suurimmalla 
osalla tapauksista ei yksinkertaisesti ole vasemmanpuoleista kollokaattia, vaan L2a-ryhmän ajanilmaus 
sijaitsee koko lauseen ensimmäisenä. Suomessa tällaiset tapaukset liittyvät käytännössä kaikki
alatopiikki- tai kontrastiiviseen konstruktioon. Sen sijaan yleisimpiä kollokaatteja suomessa 
ovat ajanilmausta tarkentavat *tänään* ja *viime*. Edellinen kattaa lähinnä esimerkin \ref{ee_lukkotiistai}
kaltaisia lyhyitä mainoksia (esimerkki annettu tässä koko kontekstissaan):




\ex.\label{ee_lukkotiistai} \exfont \small Tänään \emph{tiistaina} Rauman Lukon liigamiehistö pelaa vierasottelunsa
Tampereella ja vastaan asettuu Ilves. Tule katsomoaan ottelu suorana
Kivikylän Areenan Fox Grill and Baariin. Ottelu alkaa klo 18.30 ja ovet
aukeavat viimeistään klo 18.00. Tervetuloa.





\vspace{0.4cm} 

\noindent
Niin suomessa kuin venäjässäkin suhteellisen tavanomaista on, että L2a-ryhmän ajanilmaus esiintyy suoraan
konjunktion jäljessä. Tällöin on usein kyse esimerkkien \ref{ee_akilles} ja \ref{ee_fatih} kaltaisista äkkiä-konstruktion
ilmentymistä:




\ex.\label{ee_akilles} \exfont \small Akillesjännevaiva on viivästyttänyt kauden alkua, mutta
\emph{keskiviikkona} HIFK:n juoksija avaa kautensa. (Araneum Finnicum:
urheilulehti.fi)






\ex.\label{ee_fatih} \exfont \small Фатих две недели занимался индивидуально, а \emph{в четверг} он
возобновил тренировки в общей группе. (RuPress: Советский спорт)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_akilles} kuvataan tiettyä, vallalla olevaa
asiaintilaa -- juoksija ei ole voinut aloittaa kauttaan -- johon ajanilmauksen
kuvaamalla hetkellä tulee muutos (vertaa myös esimerkki \ref{ee_vainolin}
seuraavassa alaluvussa). Venäjänkielinen esimerkki \ref{ee_fatih} on sekä rakenteeltaan että asiasisällöltään
hyvin samantyyppinen: myös siinä kuvataan urheilijan oletettavasti loukkaantumisesta
johtunutta poikkeavaa harjoittelua, joka on ollut vallitseva asiantila torstaina tapahtuneeseen
muutokseen asti.

Suomenkielisestä aineistosta on syytä mainita erikseen melko frekventit
*viimeksi*-sanan sisältävät tapaukset, joita tässä kuvaa esimerkki
\ref{ee_lipponen}:




\ex.\label{ee_lipponen} \exfont \small Viimeksi \emph{keskiviikkona} pääministeri Paavo Lipponen torjui
ajatuksen pääjohtajan toimikauden jakamisesta. (FiPress: Turun Sanomat)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_lipponen} voidaan nähdä affektiivisen konstruktion yhtenä ilmentymänä: ajanilmauksen alkusijainnilla
korostetaan tässä sitä, miten lähellä nykyhetkeä on vielä tapahtunut jotakin käsiteltävän aiheen kannalta 
merkittävää. Vastaavia affektiivisen konstruktion edustajia ovat monet suomen *vielä*-tapaukset, kuten 
esimerkki \ref{ee_tomba}:




\ex.\label{ee_tomba} \exfont \small Vielä \emph{torstaina} Italian Alberto Tomba arvosteli kärkevästi
Kranjska Goran suurpujottelurinteen kuntoa, jätti kisan kesken ja uhkasi
lähteä pois paikkakunnalta. (FiPress: Aamulehti)





\vspace{0.4cm} 

\noindent
Jos käännetään huomio L2b-ryhmään, havaitaan, että kielten väliset erot ovat 
vähemmän jyrkkiä, mutta yhtä kaikki selviä ja samansuuntaisia. 
Taulukko 6 esittää 
tämän ryhmän yleisimmät vasemmanpuoleiset kollokaatit niissä S1-tapauksissa,
joissa viikonpäiviä tai kuukauden nimiä ei ole useampia:


\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Var1 & Freq & percent\\
\midrule
viime & 110 & 36.18\\
vuoden & 32 & 10.53\\
ja & 30 & 9.87\\
jo & 15 & 4.93\\
ensi & 11 & 3.62\\
\addlinespace
että & 10 & 3.29\\
esimerkiksi & 7 & 2.30\\
vielä & 6 & 1.97\\
mutta & 4 & 1.32\\
aiemmin & 3 & 0.99\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Var1 & Freq & percent\\
\midrule
--- & 363 & 46.42\\
что & 59 & 7.54\\
уже & 33 & 4.22\\
еще & 32 & 4.09\\
а & 23 & 2.94\\
\addlinespace
но & 16 & 2.05\\
и & 15 & 1.92\\
однако & 13 & 1.66\\
так & 11 & 1.41\\
когда & 8 & 1.02\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 6}: L2a-aineistoryhmän yleisimmät oikeanpuoleiset kollokaatit suomessa ja venäjässä \normalfont \vspace{0.6cm} 




Myös taulukossa 6 venäjä erottuu suomesta siinä, että 
venäjässä kollokaatittomat tapaukset ovat erittäin tavallisia, vaikka suomessa
ne loistavat poissaolollaan kymmenen yleisimmän tapauksen joukosta.
Tutkittaessa venäjän kollokaatittomia L2b-ilmauksia on kuitenkin 
huomattava, että suomessa huomattavan tavallinen *viime*-adjektiivi voi venäjän
kuukausista puhuttaessa usein olla myös kuukauden nimen jäljessä kuten esimerkissä 
\ref{ee_eksperty}:




\ex.\label{ee_eksperty} \exfont \small \emph{В октябре} прошлого года эксперты из iSight Partners уличили
российских хакеров в шпионаже за НАТО, ЕС и Украиной. (Araneum Russicum:
jacta.ru)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_eksperty} kaltaisia tapauksia ei kuitenkaan ole aineistossa kovinkaan paljon. Jos lasketaan
kaikki tapaukset, joissa kuukauden nimeä seuraa jokin genetiivimuotoisista adjektiiveista
*прошлого*, *того*, *текущего* tai *этого*, saadaan tulokseksi yhteensä
 58 tapausta eli 6,93 prosenttia
kaikista venäjän L2b-ryhmän lauseista. 

\todo[inline]{Todo: в прошлом январе -tapaukset erikseen mukaan aineistoon?}

Minkä takia suomessa sitten on niin runsaasti *viime tammikuussa* ja *viime maanantaina* -tyyppisiä
tapauksia (joista 91,07 % on koko lauseen ensimmäisiä),
mutta tuskin lainkaan tapauksia, joissa *tammikuussa* tai *maanantaina*
olisi itsessään lauseen aloittavana sanana? Kysymyksen ymmärtämiseksi
on syytä tarkastella ensin erästä niistä venäjänkielisen aineiston 
tapauksista, joissa kuukausi tai viikonpäivä todella on koko lauseen
alussa -- tapauksista, joiden suomenkielisten vastineiden
todettiin edellä edustavan käytännössä poikkeuksetta joko ajallista
alatopiikkikonstruktiota tai kontrastiivista konstruktiota.





\exg.  \emph{В среду} Госдума на очередном пленарном заседании рассмотрит
ситуацию в Ливии. (RuPress: Новый регион 2 2011)
\label{ee_livija}  
 PREP keskiviikko-AKK duuma PREP jälleen-yksi-PREP täysi-PREP istunto-PREP käsitellä tilanne-AKK PREP Libya-PREP
\




\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_livija} erityistä on, että kun sitä tarkastellaan osana
laajempaa kontekstia (kyseessä on lyhyt Internetissä julkaistu *Новый регион* -lehden uutinen), 
havaitaan, että ajanilmaus *в среду* sijaitsee paitsi koko lauseen, myös koko
tekstin ensimmäisenä:

\begin{quote}%
Госдума готовится осудить военную операцию в Ливии

В среду Госдума на очередном пленарном заседании рассмотрит ситуацию в
Ливии. Такое решение только что принял Совет Думы.

По итогам слушаний депутаты планируют принять официальное заявление, в
котором ГД осудит действия стран, совершающих военную операцию в
отношении Ливии.

«Государственная Дума попросит президента Российской Федерации
Д.А.Медведева поручить Министерству иностранных дел предпринять меры по
скорейшему созыву Совета Безопасности ООН с тем, чтобы незамедлительно
прекратить военные действия и направить в Ливию комиссию в составе
представителей стран – постоянных членов Совета Безопасности ООН и
государств – представителей Африканского Союза и Лиги Арабских
Государств», – сообщил «Новому Региону» автор одного из проектов
заявления (всего их два), эсэр Олег Шеин.

https://newdaynews.ru/moskow/325004.html, 7.3.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Niin kuin esimerkiksi Shore [-@shore2008, 44] huomauttaa, koko tekstin ensimmäiset lauseet
ovat erityisasemassa analysoitaessa informaatiorakennetta. Tämän tutkimuksen
teoreettisessa viitekehyksessä voidaan todeta, että tekstin aloittavat
lauseet sisältävät tavallisesti esittelytopiikkeja (ks. osio \ref{eri-tason-topiikkeja}), eli 
niissä esitellään lukijalle se diskurssitopiikki, josta seuraavassa jaksossa
(usein ortografisen kappaleen mittaisessa segmentissä) kerrotaan.
Esimerkistä \ref{ee_livija} voidaan analysoida, että siinä esitellään
diskurssireferentti *Libyan tilanne*, josta koko uutinen kertoo. 
Lauseen subjektina oleva *Госдума* voidaan nähdä diskurssistatukseltaan 
tunnistettavana entiteettinä: lukija pystyy oletettavasti identifioimaan yhden 
ainoan *duuman*, johon kirjoittaja viittaa [@lambrecht1996, ?].

Myös suomessa olisi helposti kuviteltavissa uutinen, joka alkaa sanalla *eduskunta*,
niin ettei eduskuntaa ole sen kummemmin ankkuroitu (olisi päinvastoin outoa aloittaa uutisteksti 
esimerkiksi tarkentavalla määritteellä *Suomen eduskunta*). Esimerkin \ref{ee_livija}
vastineena voisi olla vaikkapa virke *Eduskunta käsittelee keskiviikkona täysistunnossaan Libyan tilannetta* tai 
*Eduskunnan on määrä käsitellä Libyan tilannetta täysistunnossaan keskiviikkona*. Suomen ja venäjän
L2-aineistojen välinen ero on kuitenkin siinä, että lauseenalkuisen ajanilmauksen
sisältävä tekstinalkuinen tapaus vaikuttaa tässä yhteydessä mahdottomalta ratkaisulta 
niin oman kielitajuni kuin tutkimusaineistonkin perusteella. Ainoita mielekkäitä tulkintakehyksiä
lauseelle *Keskiviikkona eduskunta käsittelee täysistunnossaan Libyan tilannetta* olisivat --
kuten edellä olevat esimerkkien \ref{ee_kipinavartti} -- \ref{ee_kolmevanhempaa} kaltaiset tapaukset
ovat osoittaneet -- ajallinen alatopiikkikonstruktio tai kontrastiivinen konstruktio.

Vaikka esimerkissä \ref{ee_livija} lauseen subjekti on ilmaistu yhdellä
tunnistettavaan referenttiin viittaavalla sanalla, ovat venäjänkielisissä L2-ryhmissä
tavallisia myös tapaukset, joissa subjekti on erikseen ankkuroitu
selittävillä määreitä. Tällaisia ovat muun muassa esimerkit \ref{ee_wto} ja
\ref{ee_magaziny}:





\exg.  Во вторник в штаб-квартире ВТО в Женеве президент Украины Виктор Ющенко
и генеральный директор организации Паскаль Лами подписали протокол о
присоединении Украины к Всемирной торговой организации. (RuPress: РИА
Новости 2008)
\label{ee_wto}  
 PREP tiistai-AKK PREP päämaja-PREP WTO PREP Geneve-PREP presidentti Ukraine-GEN V. J. ja pää(adj.) johtaja järjestö-GEN P. L. allekirjoittaa-PREP pöytäkirja PREP liitäminen-PREP Ukraina PREP maailman(adj)-DAT kauppa-DAT järjestö-DAT 
\






\exg.  В среду специалисты Межгосударственного авиакомитета (МАК) обнародовали
итоги расследования авиакатастрофы в Иркутске, в которой погибли 124
человека. (RuPress: Известия 2006)
\label{ee_magaziny}  
 PREP keskiviikko-AKK asiantuntijat kansainvälinen-GEN ilmailukomitea-GEN lyh. julkaista-PRET loppupäätelmät tutkimus-GEN lento-onnettomuus-GEN PREP Irkutsk-PREP PREP joka-PREP kuolla-PRET 124 ihminen-GEN
\




\vspace{0.4cm} 

\noindent
Sekä esimerkki \ref{ee_wto} että esimerkki \ref{ee_magaziny} muistuttavat esimerkkiä
\ref{ee_livija} siinä, että kumpikin voidaan tarkempien kontekstien[^kontekstit_wto]
perusteella todeta uutistekstien ensimmäiseksi virkkeeksi. Viittaan näiden 
kolmen esimerkin ilmentämään konstruktioon jatkossa *johdantokonstruktiona*,
jonka muodollinen kuvaus on annettu matriisissa 13:

[^kontekstit_wto]: tänne urlit...




\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{

D-topic   - \\


\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & muu,top \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

df   foc \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & ident+/anch+ \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{2.5em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 13}: Johdantokonstruktio \normalfont \vspace{0.6cm}

Matriisissa 13 lauseen subjektista on merkitty, että
sen on oltava yleensä diskurssistatukseltaan joko tunnistettavissa tai ainakin
ankkuroitu. Esimerkin \ref{ee_livija} osalta
huomautettiin jo, että tunnistettavuus syntyy siitä, että tekstin oletettavalle lukijalle *duuma* 
identifioituu selkeästi entiteettiin, joita on vain yksi ja joka on lukijalle tuttu. Esimerkissä
\ref{ee_wto} taas *президент Украины* toimii muuten diskurssistatukseltaan täysin uuden
subjektin ankkurina. 

Toisin kuin diskurssistatus, johdantokonstruktion ajanilmauksen diskurssifunktio on vaikeammin
määriteltävissä. Usein ajanilmaukset  ovat johdantokonstruktioissa tulkittavissa melko topikaaliseksi: 
lauseiden voi nähdä kertovan päivistä tai kuukausista, joihin esimerkeissä viitataan, ja kun muu informaatio
lauseessa on uutta, toimii ajanilmaus ikään kuin helpoimpana saatavissa olevana lähtökohtana lukijalle -- jonakin
tuttuna, josta käsin uutta varsinaista topiikkia voidaan lähteä esittelemään
(vrt. myös Jankon esimerkistä \ref{ee_grob} tarjoama analyysi edellä osiossa
 \ref{topiikin-ja-fokuksen-tarkempi-erittely}). 
Joissain johdantokonstruktion tapauksissa voisi kuitenkin argumentoida, että
ajanilmauksella on lähinnä tekstiä jäsentävä, *muu*-luokkaan merkittävä
tehtävä. Koko ajanilmausta seuraavan proposition diskurssifunktioksi voidaan määrittää fokus,
sillä juuri itse propositiossa ilmaistava tapahtuma on se, jolta osin 
pragmaattisen väittämän voi nähdä eroavan pragmaattisesta olettamasta (ks. luku \ref{fokus}).

\todo[inline]{viittaus lambrechtin hot news -esimerkkiin s.45?}
 
Kenties olennaisinta matriisissa 13 on lopulta sen
yläosan merkintä *D-topic -*, millä viittaan siihen, ettei kyseisen
tekstisegmentin diskurssitopiikkia ole vielä muodostettu. Juuri tästä syntyy ero
myös moniin niihin taulukon 6 mukaan tavallisiin
tilanteisiin, joissa suomessa ajanilmausta edeltää adjektiivi *viime* ja jotka 
sijaitsevat koko lauseen ensimmäisenä. Yksi tällainen esimerkki on 
virke \ref{ee_citylehti}:




\ex.\label{ee_citylehti} \exfont \small \emph{Viime lokakuussa} Corporate Europe Observatory (CEO)
-tutkimusryhmä vuoti julkisuuteen salaisen asiakirjan, joka paljasti
Euroopan komission reagoineen Euroopan laajuiseen kritiikkiin luomalla
TTIP-sopimusta käsittelevän viestintästrategian. (Araneum Finnicum:
city.fi)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_citylehti} muistuttaa esimerkkiä \ref{ee_magaziny} siltä osin, että
tässäkin subjekti on samalla tavoin eksplisiittisesti
ankkuroitu: CEO-tutkimusryhmä oletetaan lukijalle toistaiseksi tuntemattomaksi referentiksi,
ja se esitellään kirjoittamalla koko ryhmän nimen lyhenne auki. Tästä huolimatta kyseessä 
ei ole koko tekstin ensimmäinen virke, vaan vasta toinen varsinainen kappale. Tekstin laajempi
konteksti on seuraavassa esitetty tarkemmin:


\begin{quote}%
"Hienot sopat" eli Transatlanttinen vapaakauppasopimus

“Kun kyse on viidestäkymmenestä prosentista maailman
bruttokansantuotetta ja kolmestakymmenestä prosentista koko
maailmankauppaa, saadaan vielä hienot sopat keitettyä.” -\/- Eurooppa-
ja ulkomaankauppaministeri Alexander Stubb

Puolet maailman bruttokansantuotteesta ja kolmannes maailmankaupasta
ovat isoja lukuja. Niin isoja, että niitä on hyvä toistella
julkisuudessa-\/-\/- Paljon muuta ei sopimuksesta sitten kuulukaan,
sillä neuvotteluja käydään suljettujen ovien takana.

\emph{Viime lokakuussa} Corporate Europe Observatory (CEO)
-tutkimusryhmä vuoti julkisuuteen salaisen asiakirjan, joka paljasti
Euroopan komission reagoineen Euroopan laajuiseen kritiikkiin luomalla
TTIP-sopimusta käsittelevän viestintästrategian. Strategian tavoite
on-\/-

-\/-\/-

Vuoden 2011 alussa valtiovarainministeri Jyrki Katainen vaati että
Euroopan pysyvään kriisinhallintamekanismiin (EVM) kirjataan selkeästi
myös yksityisten sijoittajien vastuu. -\/-

Sama tarina toistuu ympäri maailmaa. Argentiinan vuoden 2001 kriisi ja
CEO:n raportti osoittavat, miten -\/-

Kun vastaavan mittakaavan asioista päätettiin viimeeksi, oli vuosi 1998
ja asialistalla Emu-jäsenyys-\/-
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkki \ref{ee_citylehti} osoittautuu laajemmassa kontekstissaan tarkasteltuna
City-lehden julkaisemaksi pitkäksi blogitekstiksi, 
jonka aiheena ovat monikansalliset kauppaa säätelevät sopimukset ja niihin
liittyvät ongelmat. Tämä tekstin varsinainen diskurssitopiikki on muodostettu 
ensimmäisessä kappaleessa ja sitä edeltävässä sitaatissa. Esimerkki \ref{ee_citylehti}
aloittaa aiheen tarkastelun eri näkökulmista esittelemällä narratiiviseen
tyyliin CEO-tutkimusryhmän vuotaman asiakirjan: Tämä tapahtuma kuvataan ikään kuin 
yhtenä monikansallisia sopimusten ongelmia valottavana episodina. Tekstin mittaan
episodeja esitetään muitakin, esimerkiksi loppupuolella samaan tapaan ajanilmauksella
alkavassa kappaleessa *Vuoden 2011 alussa valtiovarainministeri... *.
Kyseessä on siis lopulta melko paljon alatopiikkikonstruktioita muistuttava rakenne. 
Diskurssin muotoutuminen voitaisiin pafafraseerata seuraavasti:

*Kerron nyt teille monikansallisista kauppasopimuksista ja niiden ongelmista.
Viime lokakuussa kävi niin, että.. Vuoden 2011 alussa sattui näin...
Sama tarina toistuu ympäri maailmaa...
*

Erotan vastaavat alatopiikkikonstruktiota muistuttavat rakenteet varsinaisesta
(globaalista) johdantokonstruktiosta kutsumalla niitä *lokaaleiksi* johdantokonstruktioiksi,
jotka ovat lähellä alatopiikkikonstruktiota mutta joissa tyypillisesti  ei ole 
tarkemmin määriteltyä ajallista kehystä, jonka sisältä uusia topiikkeja lohkottaisiin,
vaan jotka esittelevät satunnaisempia diskurssitopiikkiin liittyviä tapahtumia.
Lokaalin johdantokonstruktion rakenne on kuvattu matriisiin 14.

\todo[inline]{Tässä kohtaa viittaus Kirsin episode opener -juttuun}



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{

D-topic   \#1 \\


\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & muu,top \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

D-topic   ~\#1 \\

df   foc \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & ident \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{2.5em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 14}: Lokaali johdantokonstruktio \normalfont \vspace{0.6cm}




Matriisi 14 eroaa matriisista 13 
siinä, että diskurssitopiikki oletetaan jo muodostetuksi ja lauseessa 
välitettävän proposition katsotaan liittyvän tähän diskurssitopiikkiin.
Vaikka tutkimusaineiston rakenne ei mahdollista jokaisen tapauksen systemaattista
tutkimista koko tekstin tasolla (mikä olisi myös mittakaavaltaan liian suuri
työ sisällytettäväksi tähän), voidaan diskurssitopiikin statuksesta (onko se jo esitelty
vai ei) sanoa melko paljon esimerkiksi tutkimalla tarkemmin subjekteja
niissä suomen tapauksissa, joissa vasemmanpuoleisena kollokaattina on *viime*
ja jotka ovat koko virkkeen ensimmäisiä konstituentteja. 




Suomen lauseenalkuisten *viime* + kuukausi -ilmausten subjekteista 
nähdään esimerkiksi se, että leksikaalisella tasolla katsottuna yleisin
subjekti on pronomini *hän* (6 tapausta). Jos taas katsotaan
subjektin pituutta (mittarina se, kuinka monta sanaa subjektiin kuuluu), voidaan 
todeta, että 46,08 prosentissa tapauksia subjektikonstituentti on vain yhden sanan 
pituinen. Pronominin käyttöä subjektina tai ylipäätään yksisanaista, tarkemmin
ankkuroimatonta subjektia voi pitää vahvana viitteenä siitä, ettei kyseinen 
lause voi sijaita koko tekstin alussa.

Tapauksia, joissa subjekti koostuu vähintään kahdesta sanasta on lähtökohtaisesti 
 55, mutta tarkemmassa analyysissä näistä
 16 osoittautui partisiippitapauksiksi, jotka ovat virheellisesti päässeet läpi
korpusaineistojen ajanilmauslauseita suodatettaessa (esimerkiksi *viime
lokakuussa vaikeaan sairauteen kuollut Morita*;
ks. osio \ref{tutkittavien-ajanilmaustapausten-erottaminen-verrannollisista-korpuksista})
Kun nämä tapaukset ottaa huomioon, jäljelle jää
 39 virkettä, jotka potentiaalisesti
voisivat sijaita tekstin alussa ja siten olla ennemmin globaaleja johdantokonstruktioita kuin 
lokaaleja johdantokonstruktioita tai esimerkiksi alatopiikikonstruktioita. Kun tästä 
lausejoukosta erottelee vain Araneum-korpusta edustavat tapaukset (joiden tarkemman kontekstin
selvittäminen on yksinkertaista), saadaan 
 22
lauseenalkuista *viime* + kuukausi -tapausta. Näiden tapausten esiintymisyhteydet tarkistettiin,
ja tuloksena löydettiin lopulta viisi lähdettä, joissa ajanilmaus sijaitsi koko 
teksin ensimmäisenä. Näistä yksi on esimerkki \ref{ee_uusitalo}:




\ex.\label{ee_uusitalo} \exfont \small Viime lokakuussa Juha Uusitalo levytti urkuri Kalevi Kiviniemen,
sellisti Jussi Peltosen ja Euga-sekstetin kanssa körttijohtaja Väinö
Malmivaaran rukousvirsiä. (Araneum Finnicum: seurakuntalainen.fi)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_uusitalo} koko konteksti on esitetty seuraavassa. Kyseessä on 
tiivistelmä lehtijutusta:

\begin{quote}%
Siionin virsiä ei voi vain laulaa. Ne pitää tulkita. Kuolet tylsyyteen
minkä tahansa virren kanssa, jos sanoituksen merkitys ei sinulle avaudu,
oopperalaulaja Juha Uusitalo sanoo Hengellisessä Kuukauslehdessä
(6/2014).

Viime lokakuussa Juha Uusitalo levytti urkuri Kalevi Kiviniemen,
sellisti Jussi Peltosen ja Euga-sekstetin kanssa körttijohtaja Väinö
Malmivaaran rukousvirsiä.

”Olen aina tykännyt hengellisistä lauluista. Niissä on samanlaista
jykevyyttä kuin työväen marsseissa. Sanoma tulee niissä vahvasti esiin”,
Uusitalo pohtii Hengellisen Kuukauslehden haastattelussa.

Levyn julkistamiskonsertti on Lapuan herättäjäjuhlien yhteydessä 5.
heinäkuuta.

https://www.seurakuntalainen.fi/uutiset/oopperalaulaja-hengellisissa-lauluissa-on-jykevyytta/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkki \ref{ee_uusitalo} ja muut aineistosta löydetyt neljä tapausta osoittavat,
että myös globaalit johdantokonstruktiot ovat suomessa mahdollisia. L2b-ryhmä
on mahdollisuudelle selvästi avoimempi kuin L2a-ryhmä, ja myös siinä
kyseeseen tulevat vain tapaukset, joissa ajanilmausta edeltää *viime*-adjektiivi.
Tämä on selitettävissä toisaalta sillä, että *viime* yksilöi kuukauden, johon kirjoittaja viittaa
ja ikään kuin vapauttaa ilmauksen alkusijainnin muuten pakottamasta
alatopiikkitulkinnasta: aloittamalla *viime lokakuussa Juha Uusitalo levytti....* (eikä
*lokakuussa Juha Uusitalo levytti*) kirjoittaja ei laukaise
lukijalla odotusta siitä, että käsillä olevan segmentin diskurssitopiikkia käsiteltäisiin
jaottelemalla se kuukauden mittaisiin lohkoihin. Siinä, missä itsenäinen kuukauden nimi
edellyttäisi väistämättä, että lauseen subjekti olisi diskurssistatukseltaan aktiivinen,
antaa *viime lokakuussa* mahdollisuuden tulkita virke johdantokonstruktioksi,
jossa subjektissa esitellään kokonaan uusi  referentti.

Kaiken kaikkiaan L2-ryhmien osalta suomen ja venäjän välillä havaittavat
tavallista suuremmat erot alkusijainnin yleisyydessä vaikuttaisivat
olevan selitettävissä sillä, että suomessa alkusijainnin käyttö
positionaalisten ilmausten yhteydessä -- erityisesti tiiviisti laajempaan ajalliseen kehykseensä 
linkittyvien viikonpäivien tapauksessa -- on melko rajattua. Etenkin
ilman yksilöivää *viime*- tai muuta vastaavaa määritettä 
esiintyvät lauseenalkuiset viikonpäivät ja kuukaudet ovat käytännössä poikkeuksetta
suomessa joko alatopiikki- tai kontrastiivisen konstruktion osia, kun taas
venäjässä myös johdantokonstruktiot ovat tällaisissa tapauksissa mahdollisia.
Jos ajanilmaus sisältää yksilöivän määreen, on suomessakin johdantokonstruktio
mahdollinen, muttei missään nimessä frekventti.


#### Deiktiset adverbit {#sim-l5b}

\todo[inline]{Venäläinen leksikografinen lähde...}

\todo[inline]{
ALKUUN tai johonkin alkupäähän huomiot morph-MUUTTUJASTA!
}

Kuviosta 16 havaittiin edellä, että
siinä missä ei--deiktiset adverbit kasvattavat suomen S1-todennäköisyyttä 
suhteessa venäjään, on deiktisten adverbien (aineistoryhmät
L1a--c, L5c ja L8a--c, ks. osiot \ref{l1a-l1b-l1c}, \ref{l5a-l5b} ja \ref{l8a-l8b-l8c}) vaikutus päinvastainen ja vieläpä voimakkaampi.
Aivan kuten edellä positionaalisten ilmausten kohdalla, myös deiktisten adverbien
tapauksessa on valaisevaa katsoa suomen ja venäjän eroja yksittäisten deiktisten 
aineistoryhmien tasolla. Kuten aikaisemmissa kuvioissa, myös tässä
vaalea katkoviiva osoittaa koko aineiston S1-keskiarvon venäjässä ja tumma 
suomessa.

![\noindent \headingfont \small \textbf{Kuvio 22}: Deiktiset aineistoryhmät ja suhteellinen S1-osuus suomessa ja venäjässä. \normalfont](figure/deikt.erot.s1-1.pdf)

Kuvio 22 havainnollistaa, miten suomessa deiktisistä adverbeistä S1-asemaan
sijoittuva osuus on halki kaikkien tässä kuvattujen ryhmien melko lähellä koko aineiston keskiarvoa. Venäjässä taas
etenkin L1-ryhmissä alkusijainti on huomattavasti tavallisempi kuin koko aineistossa keskimäärin.
L1- ja L8-ryhmiin pureudutaan tarkemmin keski- ja loppusijainnin yhteydessä luvuissa \ref{l1-erot-kesk},
\ref{l7l8-erot-kesk} ja \ref{deiktiset-adverbit-ja-positionaalisuus}. Keskityn tässä 
yhteydessä tarkastelemaan L5c-ryhmää, jonka sijoittuminen S1-asemaan on suomessa vielä harvinaisempaa
kuin edellä L2a-ryhmän kohdalla havaittiin, mutta joka venäjässä on kuitenkin jopa hieman keskimääräistä
tavallisempi S1-asemassa.

Kokonaisuutena tarkasteltuna L5c-ryhmä on siitä mielenkiintoinen, että
se kuuluu niin  suomessa kuin venäjässäkin  koko aineiston yleisimpiin.
Suomessa L5c-ryhmä kattaa kaikkiaan 4400 tapausta, venäjässä
taas 3 216 tapausta. S1-sijaintiin osuu näistä suomessa 
 132 eli 
 ainoastaan 3 %,  venäjässä taas
 1 331 eli 
 peräti 41,39 %. 

Esitetyt luvut osoittavat, että Suomenkielisten L5c-ilmausten sijoittuminen
lauseen alkuun on harvinaista, muttei mahdotonta. Suomenkielisten S1-lauseiden joukossa
on ensinnäkin tapauksia, jotka muistuttavat ei--deiktisten adverbien ja L2-aineistoryhmien kohdalla
käsiteltyä kontrastiivista konstruktiota kuten esimerkit \ref{ee_twinsta} -- \ref{ee_haavi}. 




\ex.\label{ee_twinsta} \exfont \small Ensin Twitter esti kavereiden etsimisen Instragramin sisällä
Twitter-tunnuksen perusteella, ja \emph{äskettäin} Instagram esti kuvien
näyttämisen tweettien sisällä. (Araneum Finnicum: fonectaenterprise.fi)






\ex.\label{ee_iea} \exfont \small Tämän sanoi IEA, kansainvälinen energiajärjestö viime syksynä ja
\emph{äskettäin} IPCC vahvisti saman. (Araneum Finnicum: satuhassi.net)






\ex.\label{ee_haavi} \exfont \small Huhtikuussa virkavallan haaviin tarttui amfetamiinia, ja
\emph{äskettäin} poliisi paljasti Mikkelin Nuijamiehenkadulta huumeiden
käyttöpaikan ja myyntitukikohdan. (FiPress: Länsi-Savo)





\vspace{0.4cm} 

\noindent
Kaikissa esitetyistä esimerkeissä ajallisia alatopiikkeja on kaksi: yksi
kauempana menneisyydessä ja toinen hyvin lähellä nykyhetkeä. Kontrastiivisuudesta
puhuminen on tosin tässä yhteydessä sikäli harhaanjohtavaa, etteivät
rinnastettavina ajankohtina tapahtuvat asiat ole mitenkään välttämättä toisiinsa
nähden vastakkaisia -- esimerkin \ref{ee_iea} tapauksessa voisi jopa puhua samasta
tapahtumasta. 

Toiseksi, suomen L5c-aineisto sisältää myös alatopiikkikonstruktioita kuten esimerkki \ref{ee_mainetta}
ja tätä muistuttavia lokaaleja johdantokonstruktioita kuten esimerkki \ref{ee_gibbons}:




\ex.\label{ee_mainetta} \exfont \small Aivan \emph{äskettäin} hän sai suurta mainetta kansainvälisestä
toiminnastaan. (FiPress: Kaleva)






\ex.\label{ee_gibbons} \exfont \small Aivan \emph{hiljattain} Sudanissa asuva brittiopettaja Gillian Gibbons
sai syytteen uskonnon pilkkaamisesta ja vihan levittämisestä. (Araneum
Finnicum: humanisti.wordpress.com)





\vspace{0.4cm} 

\noindent

Vaikkei tässä annetusta lehtiesimerkistä ole yksinkertaisesti saatavissa
laajempaa kontekstia, voidaan todeta, että esimerkki \ref{ee_mainetta} muistuttaa monia jo aiemmin käsiteltyjä
alatopiikkikonstruktioita siinä, että sen subjekti on diskurssistatukseltaan
aktiivinen ja ilmaistu persoonapronominilla. Tämän voi nähdä kertovan siitä,
että kyseisen tekstin tai ainakin kyseisen segmentin varsinainen
diskurssitopiikki on henkilö, johon persoonapronomini viittaa. 
Esimerkissä \ref{ee_gibbons} tilanne on  toinen: subjektina on täysin uusi
diskurssireferentti *Sudanissa asuva brittiopettaja Gillian Gibbons* (vrt.
esimerkki \ref{ee_citylehti} edellä) ja
varsinainen diskurssitopiikki on erilainen kuin
esimerkissä \ref{ee_mainetta}. Diskurssitopiikin luonne käy hyvin ilmi, kun tarkastellaan
tekstiä, jonka osana esimerkki alun perin esiintyy. Kyseessä on laajahko
blogikirjoitus aiheesta *islam ja ihmisoikeudet kuuluvat yhteen*. Esimerkin
sisältävä osio on otsikoitu *Mistä negatiivinen islamkuva johtuu?*
Esitän seuraavassa lyhennettyinä esimerkkiä \ref{ee_gibbons} edeltävät
kappaleet ja itse esimerkin:

\begin{quote}%
\emph{Viime vuosina} mediassa on esitetty lukuisia esimerkkejä muslimien
radikalisoitumisesta ympäri maailman-\/-

Paavi Benedictus XVI puolestaan erehtyi siteeraamaan Saksassa
\emph{syyskuussa 2006} pitämässään puheessa erästä 1300-luvulla elänyttä
keisaria-\/-

\emph{Hieman myöhemmin} Saksassa nousi valtava kohu Berliinin Deutsche
Operin päätöksestä perua esitys, jonka lopussa mm. Jeesus ja Muhammad
menettävät päänsä.-\/-

\emph{Aivan hiljattain} Sudanissa asuva brittiopettaja Gillian Gibbons
sai syytteen uskonnon pilkkaamisesta ja vihan levittämisestä. -\/-

https://humanisti.wordpress.com/2009/04/09/projekti-ijtihad-\%e2\%80\%93-islam-ja-ihmisoikeudet-kuuluvat-yhteen/,
tarkistettu 6.7.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkki \ref{ee_gibbons} on siinä mielessä lähempänä varsinaista alatopiikkikonstruktiota
kuin esimerkki \ref{ee_citylehti} edellä, että tässä tapauksessa tekstin avaava
kappale eksplisiittisesti asettaa ylemmän tason ajanjakson,
jota myöhemmillä ajanilmauksilla jaotellaan segmentteihin. Ajanjaksoksi
muodostuvat sumearajaisesti "viime vuodet", ja 
tästä jaksosta kerrotaan mainitsemalla kronologisessa järjestyksessä
eri tapahtumia, jotka kaikki lokalisoidaan lauseenalkuisella ajanilmauksella,
niin että tapahtumat kulkevat järjestyksessä *syyskuussa
2006*--*hieman myöhemmin*--*aivan hiljattain*. 

Kolmanneksi suomen L5c-aineistosta voidaan, kuten tavallista, löytää esimerkkejä äkkiä-konstruktiosta,
kuten seuraava virke:[^muuga]




\ex.\label{ee_muuga} \exfont \small Muugaan on kaavailtu noin 600 miljoonan dollarin investointeja ja
\emph{äskettäin} Tallinnan satamajohtaja Peeter Palu välitti tiedon,
jonka mukaan rahoituksesta on-\/- (FiPress: Länsi-Savo)





\vspace{0.4cm} 

\noindent
Samaan tapaan kuin edellä esimerkissä \ref{ee_rakennusperinto},
esimerkissä \ref{ee_muuga} voidaan nähdä, että voimassa on ollut tietty asiaintila,
jonka kulminoituminen johonkin yksittäiseen tärkeään tai kokonaisuuden kannalta
kuvaavaan tapahtumaan on esitettty ajanilmauksella alkavalla lauseella.

[^muuga]: korpusesimerkki katkeaa kesken lauseen.

Kaiken kaikkiaan suomen S1-asemaan sijoittuva osa L5c-aineistosta muistuttaa
pitkälti positionaalisia L2-aineistoja siinä mielessä, että se koostuu
pääasiassa alatopiikkikonstruktion tai lokaalin johdantokonstruktion, äkkiä-konstruktion ja kontrastiivisen konstruktion
ilmentymistä. Venäjänkielistä aineistoa tarkasteltaessa on huomattava, että *недавно*-sana
on ylipäätään laajakäyttöisempi ja sisältää myös konteksteja, joissa
suomessa hiljattain- ja äskettäin-sanoja luontevampi vaihtoehto olisivat esimerkiksi 
ilmaukset *jokin aika sitten* tai *vähän aikaa sitten*. Tämä näkyy erityisesti siinä, että
venäjänkielinen aineisto sisältää melko runsaasti affektiivisen konstruktion
piiriin analysoitavia tapauksia kuten seuraavat esimerkit:




\ex.\label{ee_kanaly} \exfont \small Еще недавно люди чувствовали себя несчастными, потому что у них было три
телевизионных канала. (Araneum Russicum: jn.com.ua)






\ex.\label{ee_raznajarabota} \exfont \small Еще недавно участники форума занимались совершенно разной социальной
работой. (Araneum Russicum: apn-nn.ru)





\vspace{0.4cm} 

\noindent




Esimerkkien \ref{ee_kanaly} ja \ref{ee_raznajarabota} kaltaisia *ещё недавно*- tai *ещё
совсем недавно*-tapauksia venäjän L5c-aineistossa on itse asiassa
 131 kappaletta
(9,84 %)
siinä missä suomenkielisessä aineistossa vastaavia vielä-sanan sisältäviä tapauksia on huomattavasti
vähemmän -- 3 kappaletta eli 
 2,27 prosenttia.

Selkein eroavaisuus  suomen- ja venäjänkielisen aineiston välillä vaikuttaisi
kuitenkin liittyvän seuraavanlaisiin käyttötilanteisiin:




\ex.\label{ee_sinagoga} \exfont \small Совсем \emph{недавно} я начал посещать синагогу, но, честно признаюсь,
не для того, чтобы молиться, а чтобы почувствова ть принадлежность к
своему народу. (Araneum Russicum: jn.com.ua)






\ex.\label{ee_iroijasudby} \exfont \small \emph{Недавно} я пересмотрел "Иронию судьбы" и подумал, что режиссер, ее
снявший ― парень нахальный, но, как говаривал один мой знакомый,
небесспособный человек. (RuPress: Труд-7)






\ex.\label{ee_uvolili} \exfont \small Владимир Александрович, \emph{недавно} вы уволили начальника отдела
ГИБДД по Юго-Восточному округу, когда он сам нарушил правила движения.
(RuPress: Комсомольская правда)





\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_sinagoga} -- \ref{ee_uvolili} vaikuttaisi ainakin ensi näkemältä
olevan kyse johdantokonstruktioista. Esimerkit \ref{ee_sinagoga} ja \ref{ee_uvolili}
ovat siitä mielenkiintoisia, että kummassakin on laajemman kontekstin tarkastelun
perusteella kyseessä haastattelu. Esimerkin \ref{ee_sinagoga} tekstiyhteys on seuraavanlainen:


\begin{quote}%
-\/- Одно из интервью с вами называется "Плохой хороший еврей". А какие
у вас отношения с собственным еврейством сложились?

-\/- Совсем недавно я начал посещать синагогу, но, честно признаюсь, не
для того, чтобы молиться, а чтобы почувствовать принадлежность к своему
народу. Я хожу на праздники, у меня есть приглашения из всех синагог
Москвы. Вот моя супруга Иришка - она такая "еврейка евреистая". Она
всегда дружила только с мальчиками-евреями. Она и меня приобщила. Я-то
родом из Советского Союза, и как в то время относились к религии,
известно. Я вырос атеистом, но теперь чувствую, что вера у человека
должна быть.

https://press.try.md/item.php?id=117227, tarkistettu 6.7.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_sinagoga} laajempi konteksti osoittaa, että haastateltava henkilö
todella käyttää siinä ajanilmausta johdantokonstruktion tapaan ankkurina, jota vasten
kerrotaan jokin propositio. Kyseessä on kuitenkin ennemmin lokaali kuin globaali 
johdantokonstruktio siinä mielessä, että katkelma on keskeltä haastattelua ja
ylätason diskurssitopiikki, haastateltava itse, on sekä esitelty että luonnollisesti
diskurssistatukseltaan aktiivinen puheenvuoroon siirryttäessä.
Sama rakenne toimii esimerkissä \ref{ee_uvolili} toisesta näkökulmasta: ajanilmauksen
sisältävä lause on osa haastattelijan puheenvuoroa, ja lauseen fokuksessa
on koko propositio [haastateltava erotti liikennepoliisin osaston johtajan].
Esimerkin \ref{ee_uvolili} laajempi konteksti näyttää tältä:


\begin{quote}%
-\/- Владимир Александрович, недавно вы уволили начальника отдела ГИБДД
по Юго-Восточному округу, когда он сам нарушил правила движения. Не
слишком ли серьезные меры - сразу уволить?

-\/- Нарушила его подчиненная. Я считаю, если инспектор позволяет себе
на глазах у местного населения пересекать две сплошные полосы, выезжая
из своего подразделения, она не имеет права работать в милиции.

http://www.kompravda.eu/daily/24565/738572/, tarkistettu 6.7.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Kaiken kaikkiaan esimerkeissä \ref{ee_sinagoga} -- \ref{ee_uvolili} on merkillepantavaa se,
että niissä lauseiden subjekteina ovat ensimmäisen ja toisen persoonan persoonapronominit.
Tämä on melko tyypillistä venäjän L5c-aineiston S1-tapauksille, jotka sisältävät
yhteensä 242 ensimmäistä  ja  26
toista persoonaa edustavaa pronominisubjektia, niin että kaikkiaan ensimmäisen
tai toisen persoonan pronominisubjekti on
20,14 prosentissa
tapauksia. Vahvana kontrastina tähän suomen 
L5c-aineiston alkusijaintitapauksissa vastaavia lauseita ei ole ainuttakaan.
Jos verrataan kaikkien pronominaalisten ja ei--pronominaalisten
subjektien suhdetta, voidaan todeta, että venäjässä 33,73 % 
L5c-aineiston S1-lauseiden subjekteista on pronomineja, kun taas suomenkielisessä
aineistossa vastaava lukema on 18,18 %.

Erot pronominitapausten määrässä viittaavat siihen, että 
esimerkkien \ref{ee_sinagoga} -- \ref{ee_uvolili} kaltaiset lokaalit johdantokonstruktiot
vaikuttaisivat olevan venäjässä tavallisia, mutta suomessa verrattain 
harvinaisia. Pronominitapausten lisäksi venäjänkielinen aineisto sisältää
kuitenkin myös sellaisia johdantokonstruktioita, joissa subjekti on diskurssistatukseltaan
aktivoimaton, kuten seuraavassa:




\ex.\label{ee_znakomij} \exfont \small Совсем \emph{недавно} один мой хороший знакомый прислал мне воспоминания
дочери Александра Бердника Мирославы -\/- (Araneum Russicum:
varjag-2007.dreamwidth.org)





\vspace{0.4cm} 

\noindent
L5c-ryhmän lähempi tarkastelu antaa samansuuntaisia viitteitä kuin L2a-ryhmän
tarkastelu edellä: keskeinen venäjää ja suomea erottava tekijä vaikuttaisivat
olevan johdantokonstruktiot ja niiden yleisyys. Kyse ei ole siitä, että
vastaavat konstruktiot olisivat suomessa kieliopillisesti mahdottomia, vaan
ennemminkin siitä, että niitä hyödyntävät viestintästrategiat ovat tyypillisiä
venäjälle mutta selvästikin harvinaisia suomelle. Tämä venäjälle tyypillinen 
tapa tuoda diskurssiin uutta informaatiota ikään kuin ajanilmaukseen nojaten
on sinänsä tutkimuskirjallisuudessa tunnettu ilmiö, ja siihen on kiinnittänyt huomiota
muun muassa Tracy King [-@king1995, 101].





#### Simultaaninen funktio




Edellä käsiteltyjä positionaalisia ilmauksia ja deiktisiä adverbejä yhdistää
kaikkia se, että ne edustavat simultaanista semanttista funktiota. Luvun alussa kuviosta
10 nähtiin, että koko aineiston tasolla katsottuna
simultaanisuus sinänsä on S1-todennäköisyyttä kasvattava ominaisuus; toisaalta 
kuvion 15 perusteella voidaan tehdä se jossain määrin varovainen
tulkinta, että suomessa simultaanisuuden positiivinen vaikutus S1-sijainnin
todennäköisyyteen on venäjää pienempi. Tulkintaan liittyvä epävarmuus johtuu pitkälti
siitä, että simultaaninen funktio on kategoriana hyvin laaja: kaiken kaikkiaan
se kattaa 32 579 venäjänkielistä 
ja 38 108
suomenkielistä tapausta. Tästä seuraa, että simultaanisten tapausten aineisto
on väistämättä melko heterogeeninen. Jos verrataan kaikkia simultaanisia 
aineistoryhmiä -- mukaan lukien edellä jo käsiteltyjä L2- ja L5c-ryhmiä -- keskenään, saadaan
kuvion 23 mukainen tulos (vrt. kuviot
 21 ja 22). Kuvion 
yläreunassa ovat ne aineistoryhmät, joissa suomen ja venäjän välinen suhteellinen ero S1-osuuksissa
on pienimmillään; alareunaan sijoittuvat ryhmät, joissa suhteellinen ero on suurimmillaan.

![\noindent \headingfont \small \textbf{Kuvio 23}: Kaikki simultaaniset aineistoryhmät ja suhteellinen S1-osuus suomessa ja venäjässä. \normalfont](figure/sim.erot.s1-1.pdf)

Kuvio 23 osoittaa hyvin, miksi tilastollisessa mallissa simultaanisuus on
koko aineiston tasolla S1-todennäköisyyttä kasvattava tekijä ja toisaalta, miksi kielen
ja simultaanisuuden interaktioon liittyy jonkin verran epävarmuutta. Kuvion yläpäässä olevissa L7-ryhmissä 
(joita käsiteltiin edellä osiossa \ref{sekv-s1}) suomenkin S1-osuudet ovat liki 50 prosenttia ja siis 
reippaasti tummalla katkoviivalla kuvattua koko aineiston keskiarvoa ylempänä. Samaten suomessakin 
yleisiä S1-sijoittujia ovat esimerkiksi luvussa \ref{l4l5-erot-kesk} käsiteltävät L5a- ja L5b-ryhmät,
L1c-tapaukset sekä L4b-tapaukset (ks. osio \ref{l4a-l4b}). Toisaalta kuvion alapäässä
on eräitä simultaanisia aineistoryhmiä, joissa venäjän S1-osuus on yli koko aineiston keskiarvon,
mutta suomessa selvästi alle. Edellä käsitellyn L5c-ryhmän lisäksi tällaisia suomen ja venäjän 
osalta silmiinpistävän erilaisia ryhmiä ovat L3 ja L9a (ks. osiot \ref{l3} ja \ref{l9a-l9b-l9c-l9d}).
Tarkastelen näitä ryhmiä seuraaviksi omissa alaluvuissaan \ref{s1-l3}


##### Aineistoryhmä L3 {#s1-l3}

L3- ja L9a-ryhmät muistuttavat toisiaan ensinnäkin siinä mielessä, että molemmat
ovat nominaalisia ja toiseksi siltä osin, että molemmat kykenevät ilmaisemaan
*punktuaalista* simultaanista funktiota (ks. osio \ref{lokalisoiva-semanttinen-funktio}).

L3-ryhmän ilmaisema simultaanisuus on luonteeltaan yksinomaan punktuaalista:
se ei kuvaa jotakin kehystä, jonka sisälle tapahtumia lokalisoidaan, vaan 
erottelee aina konkreettisen pisteen aikajanalta, kuten esimerkissä \ref{ee_dezhurnaja}:





\exg.  \emph{В 22 часа 57 минут} дежурная смена обнаружила быстрое поступление
воды в машинный зал. (Araneum Russicum: 15kwt.ru)
\label{ee_dezhurnaja}  
 PREP 22 tunti-GEN 57 minuutti-GEN päivystävä vuoro havaita-PRET nopea-AKK tuleminen-AKK vesi-GEN PREP kone(adj.)-AKK huone-AKK
\




\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_dezhurnaja} kuvataan tapahtumaa [vuoropäivystäjä havaitsi veden
nopean virtauksen konesaliin]. Ajanilmauksen tehtävänä on erottaa se piste
aikajanalta, jolle tapahtuma sijoitetaan. 

Kuten kuviosta 23 havaittiin, suomen- ja venäjänkielisen
L3-aineiston S1-tapausten määrässä todella on huomattava ero. Kaiken kaikkiaan
 307 suomenkielisestä L3-tapauksesta S1-asemaan sijoittuu
ainoastaan 15 kappaletta; venäjänkielinen L3-aineisto
taas kattaa 476 tapausta, joista 213
osuu S1:een. Toisin kuin koko aineiston tasolla, venäjänkielinen aineisto on
siis L3-ryhmän kohdalla ylipäätäänkin suurempi, mikä johtunee siitä, että
venäjää  varten oli mahdollista rakentaa kattavuudeltaan laajempi L3-ryhmän
hakulauseke (ks. luku \ref{l3}).

Suomen vähät L3-tapaukset on käytännössä kaikki kuvattu seuraavilla neljällä esimerkillä:




\ex.\label{ee_vainolin} \exfont \small Minä vain olin ja \emph{puoli kymmeneltä} professori Barnes työnsi
käteeni paperilapun ja käski minun kirjoittaa nimeni siihen. (Araneum
Finnicum: )






\ex.\label{ee_metelia} \exfont \small Ensin tosin odottelin nöyrästi ukkosmyräkän loppumista ja kuivattelin
kotimatkalla litimäriksi kastuneita farkkujani, mutta puoli yhdeksältä
linnut pitivät jo sellaista meteliä että arvelin ulkoilun olevan
turvallista. (Araneum Finnicum: ruusuistaelamaa.livejournal.com)






\ex.\label{ee_mikenousi} \exfont \small Mike nousi ylös yleensä viisitoista yli kolme yöllä ja puoli
\emph{neljältä} hän teki erilaisia asentoja ja hengitysharjoituksia
ashramissaan oppilaidensa kanssa. (Araneum Finnicum:
kotipetripaavola.com)






\ex.\label{ee_lutakko} \exfont \small Syötyäni aamupalan ja pakattuani kassin lähdin kuuden aikoihin bussilla
keskustaan ja \emph{puoli seitsemältä} Mika Saastamoinen, spt-salibandyn
yleismies Suomessa noukki minut Lutakon pihasta kyytiin. (Araneum
Finnicum: kesset.com)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_vainolin} ja \ref{ee_metelia} ovat tulkintani mukaan äkkiä-konstruktion ilmentymiä. Etenkin
esimerkkiin \ref{ee_vainolin} tiivistyy hyvin ajatus ensin vallitsevasta staattisesta
olemisen tilasta ja tähän ajanilmauksen kautta tuotavasta muutoksesta. Loput 
kaksi esimerkkiä puolestaan vertautuvat osiossa \ref{sekv-s1} käsiteltyihin
sekventiaalisiin konstruktioihin: niissä muodostetaan ensin jokin ajallinen topiiki (TT),
johon seuraavan, ajanilmauksella alkavan lauseen välittämät propositiot
suhteutetaan (vrt. esimerkit \ref{ee_vahvisti} -- \ref{ee_skotovodstvo} edellä).

Venäjässä L3-aineistoon liittyvien lauseenalkuisten konstruktioiden kirjo on oletettavasti
suomea suurempi -- onhan tämän aineiston koko suomen vastaavaan verrattuna jopa
monikymmenkertainen. Venäjänkielisestä aineistosta onkin löydettävissä ainakin
kaksi sellaista nimenomaan L3-ilmauksille tyypillistä konstruktiota, jotka
eivät ole sen paremmin äkkiä- kuin sekventiaalisia konstruktioitakaan. Ensimmäistä
näistä edustaa tämän alaluvun aluksi esitetty esimerkki \ref{ee_dezhurnaja}, jonka
laajempi konteksti on seuraavanlainen:

\todo[inline]{[vrt. viisi pientä ankkaa -laulu?}]

\begin{quote}%
Технический инцидент на Загорской ГАЭС-2 произошел 17 сентября 2013
года. В 22 часа 57 минут дежурная смена обнаружила быстрое поступление
воды в машинный зал. В тот момент на территории станционного узла
находились 15 человек, все они организованно покинули здание и не
пострадали. В течение нескольких часов произошло подтопление машинного
зала и пристанционной площадки. В результате осмотра места происшествия
было установлено, что произошла осадка здания ГАЭС.

http://tass.ru/tek/3414430
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Tarkemman kontekstin perusteella voidaan esittää, että esimerkissä \ref{ee_dezhurnaja}
on kyse omasta raportoivaan kirjalliseen tyyliin kuuluvasta rakenteestaan,
jonka toisena edustajana voi nähdä esimerkin \ref{ee_pistolet}:
\todo[inline]{Jossain tyylioppaassa tähän liittyviä huomioita?}




\ex.\label{ee_pistolet} \exfont \small \emph{В два часа ночи} вооруженный пневматическим пистолетом пьяный
гражданин выломал входную дверь в редакции "Радио-3", находящуюся в
здании Дома прессы, и проник в студию. (gdf.ru)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_pistolet} on myös selvästi osa tyyliltään melko virallista
raporttia, kuten seuraavasta laajemmasta kontekstista käy ilmi:

\begin{quote}%
\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{84}
\tightlist
\item
  ОКТЯБРЬ, 11
\end{enumerate}

Геннадий Шеренговский

В Петропавловске-Камчатском ночью было совершено нападение на ведущего
камчатской радиостанции "Радио-3" Геннадия Шеренговского. В два часа
ночи вооруженный пневматическим пистолетом пьяный гражданин выломал
входную дверь в редакции "Радио-3", находящуюся в здании Дома прессы, и
проник в студию. Там, приставив к голове Г. Шеренговского пневматический
пистолет, хулиган требовал поставить его любимую песню. При этом он
угрожал разнести всю студию, стоимость которой оценивается в десятки
тысяч долларов. Г. Шеренговский вызвал милицию, хулиган был обезврежен и
доставлен в четвертый территориальный отдел милиции
Петропавловска-Камчатского. Ведется следствие.

http://www.gdf.ru/attacks\_on\_journalists/list/2002
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkkien \ref{ee_dezhurnaja} ja \ref{ee_pistolet} laajemmat asiayhteydet antavat syyn
olettaa, ettei niiden tapauksessa kyse ole esimerkiksi äkkiä-konstruktiosta,
vaan, kuten todettu,  omasta raportoivasta konstruktiostaan, jota voidaan
kuvata seuraavalla notaatiolla:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{

tyyli   virallinen \\


\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika,punkt \\[5pt]



prag \[ df & muu \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

df   foc \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{0.3em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 15}: Raportoiva konstruktio. \normalfont \vspace{0.6cm}

Matriisissa ajanilmauksen diskurssifunktioksi on merkitty *muu*, koska 
ajanilmauksen tehtävänä on ennen muuta muussa lauseessa ilmaistavan proposition
lokalisoiminen tiettyyn hetkeen. Oleellista on, että ajanilmausta
seuraava lause ilmaisee jonkin kokonaisen tapahtuman, joka lukijalle
välitetään uutena tietona. Tapahtuman osanottajiin liittyvä diskurssistatus
voi vaihdella, joskin oletettavaa on, että esimerkiksi subjektit
ovat vähintään tunnistettavissa.

Erityistä raportoivassa konstruktiossa on se, ettei sen ilmentymiä ole
lainkaan havaittavissa suomenkielisessä L3-aineistossa. Jos tässä esitettyjä
venäjänkielisiä rakenteita yrittää sovittaa suomenkielisiin teksteihin,
syntyy vastaavanlainen ongelma kuin mitä edellä johdantokonstruktioiden
yhteydessä havaittiin: suomessa vaikuttaisi odottamattomalta
sijoittaa ajanilmaus tekstiin ennen kuin varsinaista diskurssitopiikkia
tai tekstin keskeisiä toimijoita on lainkaan esitelty; venäjässä
tämä näyttäisi olevan yleisemmälläkin tasolla tavallista ja ajanilmaus 
nimenomaan toimii ponnahduslautana, jonka avulla uusien entiteettien
esittelyjä tehdään.

Raportoivan konstruktion kaltainen on myös seuraava venäjänkielisen aineiston esimerkki:




\ex.\label{ee_galereja} \exfont \small 7 марта \emph{в 17 часов} Художественная галерея приглашает на концерт
инструментальной и вокальной музыки Смоленского гитарного ансамбля под
руководством Заслуженного работника культуры, доцента Виктора Федоровича
Павлюченкова. (smolensk-museum.ru)





\vspace{0.4cm} 

\noindent
Esitän myös virkkeen \ref{ee_galereja} laajemmassa kontekstissaan:

\begin{quote}%
Дорогие друзья!

7 марта в 17 часов Художественная галерея приглашает на концерт
инструментальной и вокальной музыки Смоленского гитарного ансамбля под
руководством Заслуженного работника культуры, доцента Виктора Федоровича
Павлюченкова.

Исполнители: Инна Нилова, Анна Петрова, Александр Скальский, Кристина
Помозова, Валентин Рогов, Александр Ильюшкин.

Солистка – Ольга Чекалина (вокал)

В программе прозвучит классическая и современная музыка, русские романсы
и лирические песни.

Стоимость билетов – 300 рублей

http://www.smolensk-museum.ru/novosti/muzeynaya\_zhizn/news\_968.html
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
\todo[inline]{
kuvaile kontekstia... http://www.smolensk-museum.ru/novosti/muzeynaya_zhizn/news_968.html
}

Tapaus muistuttaa edellä L2-ryhmien yhteydessä käsiteltyä suomenkielistä
esimerkkiä \ref{ee_maunuvaara}, joka sekin on luonteeltaan tapahtumailmoitus. 
On kuitenkin todettava, että toisin kuin esimerkki \ref{ee_maunuvaara}, esimerkki \ref{ee_galereja}
ei ole vain yksi tapahtuma laajemmassa tapahtumien sarjassa vaan oma itsenäinen ilmoituksensa.
Merkillepantavaa esimerkissä \ref{ee_galereja} on kuitenkin ennen kaikkea se, että ajanilmaus todella
sijaitsee paitsi lauseen, myös koko tekstin alussa. 
Esimerkin \ref{ee_galereja} voisi nähdä ilmentävän omaa, raportoivasta
konstruktiosta erillistä konstruktiotaan, jonka kuvaan seuraavalla tavalla:





\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{

tyyli   mainostava \\


\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika--tarkka pvm,punkt \\[5pt]



prag \[ df & muu \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

df   foc \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{0.3em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}




 \noindent \headingfont \small \textbf{Matriisi 16}: Ilmoituksen aloittava konstruktio \normalfont \vspace{0.6cm}


Siinä missä raportoiva konstruktio vaikuttaa epätyypilliseltä suomessa, voisi
tässä kuvatun ilmoituksen aloittavan konstruktion ainakin kuvitella olevan enemmän tai
vähemmän käytössä kummassakin tarkastelluista kielistä. Yksi syy siihen, ettei
konstruktiosta löydy suomenkielisiä esimerkkejä, lienee siinä, että
L3-aineistoon valikoitiin suomen osalta ilmaukset
tyyppiä *seitsemältä* eikä ilmauksia tyyppiä *kello seitsemän*. Voisi nimittäin
olettaa, että jälkimmäinen ilmaisutapa sopisi luontevammin yhteen esimerkiksi
päivämäärän ilmauksen kanssa (vrt. 17. maaliskuuta kuudelta / 17. maaliskuuta
kello 6). 

##### Aineistoryhmä L9a {#sim-l9a}

L9a-aineistoryhmässä suomen S1-tapausten määrä
on niin suhteellisesti katsottuna kuin lukumäärällisestikin suurempi kuin
L3-aineistoryhmässä. Kaiken kaikkiaan L9a-aineisto kattaa suomessa peräti
 3 691 tapausta, joista 
  335 eli 
 9,08 %
osuu S1:een. Venäjässä koko L9a-aineiston koko 
on 2 695 tapausta, joista
 1 558 eli 
57,81 % sijoittuu S1:een.

Yksi merkittävä syy L9a-ilmausten laajemmalle lauseenalkuiselle käytölle
suomessa lienee se, että L9a-ajanilmauksia voi (kielestä riippumatta) käyttää paitsi punktuaalisesti
kuten esimerkissä \ref{ee_ekbfond}, myös kehyksisesti kuten esimerkissä \ref{ee_vallantoiset}. 




\ex.\label{ee_ekbfond} \exfont \small \emph{В 1999 году} в Екатеринбурге появился фонд "Город". (RuPress:
Комсомольская правда)






\ex.\label{ee_vallantoiset} \exfont \small Suomessa luvut ovat vallan toiset: \emph{vuonna 1991} Suomen hallitus
piti 104 valtioneuvoston istuntoa, joissa käsiteltiin yhteensä peräti 4
091 asiaa. (FiPress: Länsi-Savo)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_ekbfond} paikannetaan aikajanalta se piste, jossa Gorod-nimisen
säätiön synty tapahtui. Huomaa, että venäjänkieliset esimerkit sopivat siinä
mielessä hyvin kuvaamaan punktuaalisuutta, että niissä punktuaalisuus voidaan
kytkeä perfektiiviseen aspektiin, jolle tyypillistä on tunnetusti nimenomaan
tapahtuman käsittäminen rajattuna kokonaisuutena [@tobecited]. Esimerkissä
\ref{ee_vallantoiset} puolestaan *vuonna 1991* ei ole piste aikajanalla, vaan kehys,
jonka sisälle tietyt tapahtumat lokalisoidaan [vrt. @haspelmath1997space, 29].
Tässä suhteessa esimerkin \ref{ee_vallantoiset} ajanilmauksen voisi jopa korvata
kehyksisen resultatiivisen ekstension ilmaisulla *vuoden 1991 aikana* (vrt. osio
\ref{kehres-s1} edellä).

Myös Suomen L9a-aineistosta on kuitenkin mahdollista löytää eräitä
punktuaalisia S1-tapauksia, kuten seuraava esimerkki:




\ex.\label{ee_sigyn} \exfont \small Vuonna 1993 Turun kaupunki ja Åbo Akademin säätiö perustivat Museoalus
Sigynin säätiön ja seuraavana vuonna valmistui aluksen säilyttämistä
helpottava uivatelakka. (Araneum Finnicum: ts.fi)





\vspace{0.4cm} 

\noindent
Tarkemman kontekstin perusteella esimerkistä \ref{ee_sigyn} voidaan todeta, että 
-- kuten suomen S1-tapaukset usein -- se ilmentää alatopiikikonstruktiota.
Laajempi konteksti on esitetty seuraavassa:

\begin{quote}%
Museolaiva Sigynin kallistuminen huolestutti ohikulkijoita

-\/-\/-

Åbo Akademin säätiö osti Sigynin Turkuun Suomen ensimmäiseksi
museolaivaksi vuonna 1939. Alus kunnostettiin 1970-luvulla. \emph{Vuonna
1993 }Turun kaupunki ja Åbo Akademin säätiö perustivat Museoalus Sigynin
säätiön ja \emph{seuraavana vuonna} valmistui aluksen säilyttämistä
helpottava uivatelakka.

http://www.ts.fi/uutiset/paikalliset/2775219/Museolaiva+Sigynin+kallistuminen+huolestutti+ohikulkijoita
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_sigyn} laajempi konteksti osoittaa, että kyseessä on uutisteksti, jonka yksi kappale
kertoo museolaiva Sigynin historian käyttäen ajallisia alatopiikkeja.

Toista punktuaalisen L9a-ilmauksen sisältävää lausetta \ref{ee_rakennusperinto} voisi puolestaan 
verrata esimerkkiin \ref{ee_vainolin}, joka edellä analysoitiin äkkiä-konstruktion
ilmentymäksi:




\ex.\label{ee_rakennusperinto} \exfont \small Kiinnostus kasvoi nopeasti ja \emph{vuonna 1991} Euroopan neuvosto
perusti rakennusperintöpäivät virallisesti. (Araneum Finnicum:
rakennusperinto.fi)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_rakennusperinto} erityispiirteenä on se, ettei sen edellä ole
nähtävissä varsinaista äkisti katkeavaa ajan tasaista virtausta, vaan pikemminkin
kehittyvä tendenssi, joka kulminoituu ajanilmauksella aloitettavaan lauseeseen.
Kaiken kaikkiaan myös L9a-ryhmän perusteella vaikuttaa siltä, että
punktuaalista lokalisointia esiintyy suomessa lauseen alussa joko alatopiikki-,
äkkiä- tai sekventiaalisen konstruktion ilmentymänä. Katsotaan kuitenkin vielä kahta tapausta, joiden
osalta lokalisoinnin määrittely joko punktuaaliseksi tai kehyksiseksi ei 
ole aivan yksiselitteistä:




\ex.\label{ee_yva} \exfont \small Jo \emph{vuonna 1996} Yva ry ja Uudenmaan ympäristökeskus järjestivät
YVA-päivänsä koordinoidusti peräkkäisinä päivinä yhteisenä
kokonaisuutena ja seuraavana vuonna päivät järjestettiin kokonaan
yhteisinä (Araneum Finnicum: yvary.fi)






\ex.\label{ee_varsinkin} \exfont \small Varsinkin \emph{vuonna 1992} nuorimmat vastaajat torjuivat pornografian
ja prostituution. (Araneum Finnicum: mv.helsinki.fi)





\vspace{0.4cm} 

\noindent
Esimerkkejä \ref{ee_yva} ja \ref{ee_varsinkin} yhdistää ajanilmauksen saama korostava
määrite -- esimerkissä \ref{ee_yva} *jo* ja esimerkissä \ref{ee_varsinkin} *varsinkin*.
Ensi katsomalta vaikuttaisi houkuttelevalta ratkaisulta luokitella tällaiset
ajanilmausta jollakin tavalla  korostavat tapaukset omaksi konstruktiokseen.
Näin todella voisi tehdä siinä mielessä, että jo-sana lisää oman
merkityksensä lauseeseen. Varsinaisena syynä ajanilmauksen lauseenalkuiseen sijaintiin
on kuitenkin jälleen kerran nähtävä alatopiikkikonstruktio. Itse asiassa
jo esimerkin \ref{ee_yva} otsikko antaa odottaa, että tekstin diskurssitopiikkina
olevia YVA-päiviä käsitellään segmentoimalla instituution 20-vuotinen historia
ajallisiin lohkoihin:

\begin{quote}%
20 vuotta YVA-päiviä – a Success Story

YVA-lain tullessa voimaan 1994 perustettiin myös YVA ry, ja sen
ensimmäisiä ponnistuksia oli YVA-päivien järjestäminen 1995. -\/-

Koska monilla valtakunnallisilla toimijoilla oli pääkonttorit
pääkaupunkiseudulla, herättivät nämä YVA-päivät heti alusta saakka
kiinnostusta myös tällaisissa toimijoissa eikä heidän osallistumiselleen
toki nähty mitään esteitäkään, vaikka niiden toiminta sijaitsi jossain
muualla. \emph{Jo vuonna 1996} Yva ry ja Uudenmaan ympäristökeskus
järjestivät YVA-päivänsä koordinoidusti peräkkäisinä päivinä yhteisenä
kokonaisuutena ja seuraavana vuonna päivät järjestettiin kokonaan
yhteisinä. Vuodesta 2007 Yva ry on ottanut täyden vastuun päivien
järjestämisestä niin sisällöllisesti kuin taloudellisestikin. -\/-

http://www.yvary.fi/tag/yva-palkinto/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Tässä esitetty laajempi konteksti osoittaa, että vaikka jo-sana tuo esimerkkiin
\ref{ee_yva} oman lisämerkityksensä, on alatopiikkikonstruktio varsinaisen syy
ajanilmauksen S1-sijaintiin.

Esimerkissä \ref{ee_varsinkin} käytetyn viestintästrategian tulkitseminen on hieman
esimerkkiä \ref{ee_yva} monimutkaisempaa. Tarkasteltaessa esimerkin laajempaa kontekstia[^varskont]
havaitaan, että kyseessä on html-muodossa[^htmlhuom] julkaistu tieteellinen
artikkeli. Artikkelissa käsitellään kolmea kyselyaineistoa, jotka kerättiin
vuosina 1971, 1992 ja 1999. Näistä vuosista muodostuu eräänlainen
positionaalisten ilmausten joukko (vrt. L2-ryhmien analyysi luvussa \ref{s1-sim-pos}), ja esimerkin
\ref{ee_varsinkin} *vuonna 1992* on lopulta tulkittavissa osaksi ajallista
alatopiikkikonstruktiota, jossa nuorimpien vastaajien suhtautumista käsitellään kolmesta vuodesta
koostuvana kokonaisuutena, joka jaetaan segmentteihin 1971, 1992 ja 1999.
Tässäkin tapauksessa ajanilmausta korostava *varsinkin*-sana tuo siis vain oman
leksikaalisen lisämerkityksensä sinänsä jo käsitellyn kaltaiseen
alatopiikkikonstruktioon.

[^varskont]: http://www.mv.helsinki.fi/home/jproos/Toivonen.htm, tarkistettu 21.6.2017

[^htmlhuom]: Koska Internet on nykyisin merkittävä tieteellisten julkaisujen jakelukanava,
voidaan kysyä, kuinka suuri osa Araneum-aineiston teksteistä on esimerkiksi
tieteellisiä artikkeleita. Esimerkin \ref{ee_varsinkin} tilanne on kuitenkin siitä poikkeuksellinen, että
teksti on julkaistu nimenomaan verkkosivuna, html-muodossa, eikä erillisenä pdf-tiedostona, kuten yleensä
on tapana. Tämä selittää tekstin valikoitumisen Araneum-korpukseen.

Tutkimusaineistosta huomataan nopeasti, että edellä käsitellyt
alatopiikkikonstruktiot ja äkkiä-konstruktion variantit ovat käytössä myös
venäjässä. Esimerkkeinä voidaan tarkastella virkkeitä \ref{ee_poslerazvoda}
ja \ref{ee_nagorle}:




\ex.\label{ee_poslerazvoda} \exfont \small После развода у Татум начались проблемы с наркотиками и \emph{в 1998
году} она потеряла опеку над детьми и они переехали жить к отцу.
(Araneum Russicum: runyweb.com)






\ex.\label{ee_nagorle} \exfont \small \emph{В 1997 году} музыкант перенес операцию на горле, а в марте 2001
года―на легких. (RuPress: РИА Новости)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_poslerazvoda}  ajanilmaus aloittaa rinnasteisen
lauseen, joka vertautuu samalla tavalla rinnasteiseen esimerkkiin
\ref{ee_rakennusperinto}: kummassakin tapauksessa ajanilmaus kuvaa tiettyä käänteen
tuovaa hetkeä, jolloin tapahtuu jotakin merkittävää. Kyse on siis 
äkkiä-konstruktion variantista.

Esimerkki \ref{ee_nagorle} puolestaan muistuttaa pitkälti edellä käsiteltyä
alatopiikkikonstruktioita hyödyntävää suomenkielistä esimerkkiä \ref{ee_sigyn}.
Esimerkin tarkempi konteksti osoittaa, että kyse on George
Harrisonin elämää muistelevasta kirjoituksesta, jossa viestintästrategiana on
biografiselle tekstille tyypillisesti [vrt. vastaava esimerkki Vilkunalla
-@vilkuna1989, 92]
jaotella laulajan elämä ajallisiin alatopiikkeihin:

\begin{quote}%
Сегодня Джорджу Харрисону исполнилось бы 65 лет

Джордж Харрисон родился 25 февраля 1943 года в Ливерпуле. В юности он
увлекся современной музыкой-\/-

В 1962 году окончательно сформировался состав «Битлз»:-\/-

После распада группы Джордж Харрисон выпустил свою первую сольную запись
- песню «All Things Must Pass». В 1971 году он организовал первый
крупнейший благотворительный «Концерт для Бангладеш», который состоялся
в нью-йоркском «Мэдисон сквер-гарден».-\/-

В 1988 году Харрисон , Боб Дилан, Том Петти, Джефф Линн и Рой Орбюсон
объединились в группу «Traveling Wilburys», выпустившую несколько
успешных альбомов.

В 1997 году музыкант перенес операцию на горле, а в марте 2001 года - на
легких.

http://m.tatar-inform.ru/news/2008/02/25/99387/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Kuten yllä tarkastelluista esimerkeistä käy ilmi, suomen ja venäjän välillä on
yhtäläisyyksiä siinä, mitä ajanilmauskonstruktioita L9a-aineistoryhmän
ajanilmaukset edustavat. Kielten välillä voidaan  kuitenkin havaita myös
eroja, joiden tarkastelun aloitan esimerkeistä \ref{ee_ostatkimon} ja \ref{ee_liberaly}.




\ex.\label{ee_ostatkimon} \exfont \small Только \emph{в 1995 году} государство передало остатки монастыря церкви.
(RuPress: Труд-7)






\ex.\label{ee_liberaly} \exfont \small Уже \emph{в 1990 году} либералы поставили вопрос о массовом переходе к
фермерству. (Araneum Russicum: economicsandwe.com)





\vspace{0.4cm} 

\noindent
Kuten esimerkeissä \ref{ee_yva} ja \ref{ee_varsinkin}, myös tässä tarkastelluissa
tapauksissa ajanilmausta määrittää adverbi. Siinä missä edellä analysoidut
suomenkieliset esimerkit osoittautuivat pohjimmiltaan selvästi
alatopiikkikonstruktioiksi, ovat tässä esitetyt venäjänkieliset tapaukset 
monitulkintaisempia. Katsotaan ensin esimerkin \ref{ee_ostatkimon} laajempaa
kontekstia:


\begin{quote}%
ЧУДО НА ТРЕЗВУЮ ГОЛОВУ

-\/-

Через речку от мужского монастыря находится женский Введенский Владычный
монастырь, где согласно легенде и было в прошлом веке явление
чудотворной иконы. Старый вояка и пьяница приполз к ней на четвереньках
аж из Тверской губернии, а вышел на своих ногах, при этом "страсть
пьянки его оставила".

Только в 1995 году государство передало остатки монастыря церкви.
Настоятельница матушка Алексия, которая в мирской жизни была
учительницей музыки, а теперь мужественно поднимает к небу исторические
руины, осторожно говорит о том, что их чудотворная икона пропала во
время революционного лихолетья. И в мужском, и в женском монастырях
теперь висит список с той "Неупиваемой чаши" (неистощимой чаши
исцеления).

http://www.trud.ru/article/07-06-2000/7189\_chudo\_na\_trezvuju\_golovu.html
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin sisältävän tekstin aiheena on ikoni, jonka väitetään parantavan
alkoholismin. Tekstin alussa puhutaan munkkiluostarista ja esimerkkiä
\ref{ee_ostatkimon} edeltävässä kappaleessa huomio siirretään joen toisella puolen
sijaitsevaan nunnaluostariin. Eräässä mielessä esimerkin sisältävän kappaleen
voisi nähdä edustavan alatopiikkeja hyödyntävää strategiaa: esimerkkiä seuraavassa virkkeessä
kerrotaan, miten ihmeitä tekevä ikoni hävisi *vallankumouksen melskeiden
aikana*, ja kappaleen viimeisessä virkkeessä näkökulma siirretään nykypäivään
(*теперь-sana*). Suomesta poikkeavaa on kuitenkin se, että koko kappale alkaa S1-asemaan
sijoittuvalla ajanilmauksella, niin että esimerkkivirkkeen taustalla voidaan
nähdä lokaali johdantokonstruktio.

Siirrytään nyt esimerkin \ref{ee_liberaly} laajempaan kontekstiin:

\begin{quote}%
ОБ ЭФФЕКТИВНОСТИ СВИНСТВА

России необходимо создавать и развивать свиноводческие комплексы
индустриального типа, иначе уже к 2020 году отечественных производителей
могут потеснить зарубежные конкуренты после вступления страны во
Всемирную торговую организацию (ВТО). Об этом на заседании коллегии
Минсельхоза заявил министр сельского хозяйства РФ Николай Федоров.-\/-

Заявлением Федорова завершен, по сути, долгий этап заигрываний либералов
с мелкосерийным производством (оно же – «малый бизнес» и «фермерство»)
на селе. Рухнула ещё одна идеологическая опора ельцинизма, обнажив всю
ущербность возводимой конструкции государства. Уже в 1990 году либералы
поставили вопрос о массовом переходе к фермерству. Земля, по их мнению,
должна была быть только у семейного фермера. Это стало краеугольным
камнем и одновременно «священной коровой» аграрной политики «новой
России».

http://economicsandwe.com/2F0A3873DB1AE68C/, 21.6.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Tekstikatkelman toinen kappale alkaa toteamuksella siitä, että maidontuotanto
on pitkään ollut keskeinen kysymys liberaaleille ja että ensimmäisessä
kappaleessa mainittu maatalousministerin lausunto asettaa tälle tarinalle
tietynlaisen pisteen. Esimerkin \ref{ee_liberaly} sisältävä virke puolestaan siirtää
huomion tarinan alkupisteeseen kertomalla, että kaikki alkoi vuonna 1990.
Tässäkin tapauksessa kyseessä on siis ennemmin lokaali johdantokonstruktio
kuin alatopiikkikonstruktio. Lokaalin ja toisaalta myös globaalin
johdantokonstruktion tavallisuus venäjän L9a-aineistossa näkyy 
seuraavista esimerkeistä:




\ex.\label{ee_pfizer} \exfont \small \emph{В 1996 году} Pfizer использовала антибиотик для лечения вспышки
менингита в нигерийской провинции Кано, после чего умерли 11 детей и
десятки других остались парализованными.






\ex.\label{ee_peking} \exfont \small \emph{В 1993 году} пекинские власти ввели запрет на запуск фейерверков и
взрыв петард в черте города.






\ex.\label{ee_autoru} \exfont \small \emph{В 1996 году} вы зарегистрировали домен Auto.ru.





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_pfizer} on osa lyhyttä uutista, joka on otsikoitu
"Сотрудников Pfizer арестуют":
        
\begin{quote}%
Суд Нигерии ищет троих обвиняемых по делу об испытании антибиотика на
детях

В понедельник суд Нигерии выдал ордер на арест троих сотрудников
американского фармацевтического гиганта Pfizer Inc., которые
проигнорировали присланные им повестки и не явились на заседание по делу
о незаконных клинических испытаниях нового препарата. \emph{В 1996 году}
Pfizer использовала антибиотик для лечения вспышки менингита в
нигерийской провинции Кано, после чего умерли 11 детей и десятки других
остались парализованными. Власти Нигерии требуют, чтобы компания
выплатила почти 10 млрд долл. компенсации. Специалисты Pfizer проходят
по делу как обвиняемые.

http://www.media-atlas.ru/anons/print.php?id=4941
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkissä \ref{ee_pfizer} on laajemman kontekstin perusteella itse
asiassa kaksi johdantokonstruktiota, sillä koko teksti alkaa
viikonpäivään viittaavalla lokalisoivalla ajanilmauksella.
Varsinainen esimerkissä \ref{ee_pfizer} esitetty virke on enemminkin 
lokaali johdantokonstruktio, sillä Pfizer toimijana on jo esitelty lukijalle
ja tekstin toisella ajanilmauksella esitellään se tapahtuma, joka
alun perin johti tekstissä mainittuun oikeuskäsittelyyn.

Myös esimerkki \ref{ee_peking} kuvaa jossain määrin samanlaista tilannetta. 
Esimerkki on osa kiinalaisten uuden vuoden vietosta kertovaa uutista:

\begin{quote}%
Дома и улицы во всех городах и поселениях Китая украшены самодельными
бумажными фонариками: считается, что они открывают человеку путь в Новый
год. А начинать празднование принято с запуска фейерверков, которые, по
преданию, должны отпугивать злых духов. Хотя в этом году продажу петард
в Поднебесной сильно ограничили по соображениям безопасности, тем более,
что в китайской столице уже почти месяц стоит густой смог.

\emph{В 1993 году} пекинские власти ввели запрет на запуск фейерверков и
взрыв петард в черте города, аргументировав это тем, что они наносят
существенный вред окружающей среде и служат причиной многочисленных
пожаров. Запрет был отменен в 2006 году под давлением общественного
мнения.
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_peking} *в 1993 году* tuo jo tutuksi käyneeseen tyyliin
lukijan tietoon jonkin tapahtuman, joka toimii lähtöpisteenä kappaleen
varsinaiselle diskurssitopiikille. Diskurssitopiikki sinänsä käsitellään 
koko lailla narratiiviseen tyyliin aloittamalla kappaleessa käsiteltävän ilotulitekiellon
syntyyn johtaneesta tapahtumasta ja lopettamalla kiellon päättäneeseen
tapahtumaan. Tämä on kuitenkin lähtökohtaisesti hyvin erilaista aikajanan
hyödyntämistä tekstin rakentamisessa kuin mitä alatopiikkitapauksissa,
joissa kirjoittajan ote voi olla hyvinkin analyyttinen ja ei--narratiivinen,
eikä välttämättä edes etene kronologisesti [@tobecited, viittaus tekstityyppilähteisiin].

\todo[inline]{tobecited mk-artikkeliin liittyviä lähteitä?}

Tarkastellaan vielä esimerkkiä \ref{ee_autoru} laajemmassa yhteydessään. Kyseessä on
yksi haastattelijan esittämistä kysymyksistä tekstissä, joka kertoo
Auto.ru-verkkopalvelun omistajasta:

\begin{quote}%
Поговорим об истории создания Auto.ru. \emph{В 1996 году} вы
зарегистрировали домен Auto.ru. Насколько было тяжело первое время? Я
понимаю, что вы работали первые 2 года, если не больше, совсем один.
(Araneum russicum: forbes.ru)

http://www.forbes.ru/tehno-opinion/internet-i-telekommunikatsii/81438-mihail-rogalskii-autoru-my-strannaya-kompaniya-i-ne
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_autoru} konteksti on hyvin samanlainen kuin toisessa edellä käsitellyssä 
haastatteluesimerkissä \ref{ee_uvolili}: myös tässä haastattelija rakentaa
kysymyksensä esittelemällä jonkin tapahtuman haastateltavan menneisyydestä.
Tässä esimerkissä erityistä on se, että ajanilmauksen sisältävää virkettä
edeltää metatekstuaalinen *Поговорим об истории создания Auto.ru*. Kuten
suurimmassa osassa edellä esitettyjä venäjänkielisiä esimerkkejä, tässäkin
kokonainen kronologisesti etenevä tarina aloitetaan lauseenalkuisella 
ajanilmauksella, selkeällä (tässä tapauksessa oikeastaan ennemmin globaalilla
kuin lokaalilla) johdantokonstruktiolla. 

Yleisesti ottaen L9a-ryhmän tarkastelu paljastaa seuraavan
suomen ja venäjän välisen strategiatason eron: suomessa on tyypillisempää lähteä
liikkeelle ensimmäisessä virkkeessä tarinan päähenkilöstä tai muusta keskeisestä
referentistä ja mahdollisesti vasta seuraavassa virkkeessä siirtyä ajallisten
alatopiikkien avulla kertomaan jonkin ilmiön tai henkilön eri vaiheista;
venäjässä kirjoittaja tai puhuja asettaa usein ensin ajallisen näyttämön, ja
tälle valmiille näyttämölle sitten marssitetaan tarinan hahmot ja kohteet.
Tätä suomen ja venäjän välillä havaittavaa eroa kuvaa hyvin edellä esitetyn
esimerkin \ref{ee_yva} laajemman kontekstin aloittava virke *YVA-lain tullessa
voimaan 1994 perustettiin myös YVA ry, ja sen ensimmäisiä
ponnistuksia oli YVA-päivien järjestäminen 1995.*. Virkkeen aloittavassa
lauseessa kirjoittaja lähtee liikkeelle esittelemällä ensin
YVA-lain käsitteen ja kertomalla vasta sitten lain
voimaantulovuoden. Myös virkkeen jälkimmäinen lause lähtee nimenomaan
YVA-päivien käsitteen esittelemisestä, ja vuosiluku on ennemmin ylimääräinen
liite kuin käsitteen esittelemiseen käytettävä ponnahduslauta.


#### F-funktiot

Samoin kuin positionaalisuudella, deiktisillä adverbeillä ja simultaanisuudella,
myös F-funktiolla on suomessa venäjään verrattuna negatiivinen vaikutus S1-aseman
todennäköisyyteen. Kuvion 15 perusteella  vaikutusta
voidaan pitää jonkin verran voimakkaampana kuin simultaanisen funktion kohdalla,
joskin tähän arvioon liittyy myös epävarmuutta.

F-funktion negatiivista vaikutusta voidaan tarkastella myös edellä hyödynnetyllä tavalla
aineistoryhmittäin kuvion 24 avulla.

![\noindent \headingfont \small \textbf{Kuvio 24}: Erot F-funktiota edustavien aineistoryhmien S1-osuuksissa \normalfont](figure/feroja-1.pdf)

F-funktiota edustavat aineistoryhmät erottaa edellä tarkastelluista tapauksista
(vrt. kuviot 19 -- 23 )
se, että tällä kertaa myös venäjänkieliset aineistoryhmät sisältävät suurimmaksi
osaksi keskimääräistä vähemmän S1-tapauksia. Suhteessa suurimmat erot suomen ja
venäjän välillä havaitaan aineistoryhmissä F4 (ks. osio \ref{f4}), F1a (\ref{f1a-f1b}), 
F2b (\ref{f2a-f2b}), F1b (\ref{f1a-f1b}) ja F3d (\ref{f3a-f3b-f3c-f3d-f3e}).
F2b-ryhmässä S1-sijainti on kuitenkin melko harvinainen sekä suomessa että
venäjässä, joten hedelmällisimmiltä tutkimuskohteilta suomen ja venäjän erojen
kannalta vaikuttavat ryhmät F4, F1a--b sekä F3d. Näitä tarkastellaan lähemmin
seuraavissa alaluvuissa. Mielenkiintoista kyllä,
ryhmässä F3c (osio \ref{f3a-f3b-f3c-f3d-f3e}) suomen ja venäjän S1-osuudet ovat
käytännössä identtiset.

##### Aineistoryhmä F1a {#s1-f1a}




F1a-ryhmä kattaa suomessa kaikkiaan 2077 tapausta, joista 
 148 sijoittuu lauseen alkuun; venäjässä koko F1a-aineiston
koko on 2 405 ja 
S1-tapausten määrä 1 046.
Kuten kuviosta 24 nähdään,  venäjän S1-osuus on 
yli 40 %, mutta suomessa vain noin 5%. Jos tutkitaan tarkemmin,
minkälaisia tapauksia suomen S1-lauseiden joukkoon mahtuu, havaitaan monia
aiemmista analysoiduista ryhmistä tuttuja konstruktioita. Vertaa esimerkiksi
kehyksisen resultatiivisen ekstension yhteydessä luvussa \ref{kehres-s1} käsiteltyä virkettä \ref{ee_itsm} 
virkkeeseen  \ref{ee_itsm2}, joka muistuttaa esimerkkiä \ref{ee_itsm}
paitsi rakenteeltaan, myös sisällöltään:




\ex.\label{ee_itsm2} \exfont \small \emph{Joka päivä} neljä suomalaista tekee itsemurhan. (FiPress:
Länsi-Savo)





\vspace{0.4cm} 

\noindent
Aivan kuin esimerkissä \ref{ee_itsm}, esimerkissä \ref{ee_itsm2} on kyse määrästä, joka tietyn ajanjakson
sisällä saavutetaan. Esimerkissä \ref{ee_itsm} rajaavaksi ajanjaksoksi on määritelty yksi vuosi,
esimerkissä \ref{ee_itsm2} puolestaan yksi päivä. Erona näiden esimerkkien välillä on se, että esimerkissä
\ref{ee_itsm2} rajaavaa ajanjaksoa käsitellään toistuvana. Myös esimerkki \ref{ee_itsm2} on kuitenkin
perustellusti tulkittavissa määrää painottavan konstruktion (ks. matriisi 9 )
ilmentymäksi. Toinen vastaava esimerkki suomen F1a-aineistosta on virke \ref{ee_lampesuda}:




\ex.\label{ee_lampesuda} \exfont \small \emph{Joka vuosi} kymmenet tuhannet ihmiset maksavat viimeiset säästönsä
paetakseen arkeaan ja tehdäkseen vaarallisen merimatkan Afrikan
pohjoisrannikolta Italian Lampedusan saarelle paremman tulevaisuuden
toivossa. (Araneum Finnicum: pelastakaalapset.fi)





\vspace{0.4cm} 

\noindent
Jos mitataan määrän ilmaisemisen tavallisuutta luvussa \ref{kehres-s1} esitetyllä 
menetelmällä, havaitaan, että suomen F1a-ryhmän S1-tapauksista 
10,81 % sisältää esimerkkien \ref{ee_itsm2} ja \ref{ee_lampesuda}
tapaan ajanilmauksen jälkeisen numeraalin (vrt. myös taulukko 4 edellä).
Esimerkkien \ref{ee_itsm2} ja \ref{ee_lampesuda} kaltaiset
määrää korostavat tapaukset muodostavat siis tärkeän osan suomen S1-asemaan sijoittuvista
F1a-lauseista. Muun muassa esimerkki \ref{ee_kropasta} kuitenkin
osoittaa, ettei määrää korostava konstruktio ole ainoa tilanne, jossa
F1a-ryhmän ilmaus sijoittuu suomessa lauseen alkuun:




\ex.\label{ee_kropasta} \exfont \small \emph{Joka päivä} mä löydän mun kropasta kohtia, joista en tykkää.
(Araneum Finnicum: justdoitviviana.blogspot.com)





\vspace{0.4cm} 

\noindent
Esimerkistä \ref{ee_kropasta} voidaan suoraan sanoa, ettei se ole tyyliltään
neutraali, vaan siihen liittyy \todo[inline]{Ks. Kuno?+ krylova ja havr.} kirjoittajan ilmaisema tunnelataus.
Esimerkki voidaankin analysoida affektiivisen konstruktion edustajiksi samoin 
kuin seuraavat tapaukset:




\ex.\label{ee_synti} \exfont \small \emph{Joka päivä} me teemme syntiä, ja siksi \emph{joka päivä} me
tarvitsemme armoa. (Araneum Finnicum: rukoilevaisuus.com)






\ex.\label{ee_museohaku} \exfont \small Melkein \emph{joka päivä} joku on hakenut tätä museota kylällä. (Araneum
Finnicum: museopaivakirja.20six.fr)





\vspace{0.4cm} 

\noindent
Virkkeessä \ref{ee_museohaku} oleellista on se, että aiheena olevaa museota on
haettu yllättävän usein -- niin usein, että lukijan voi odottaa hämmästyvän
kuullessaan hakujen tavallisuudesta. Samoin esimerkissä \ref{ee_synti} 
kirjoittaja olettaa kuulijan käsityksen synnin tekemisen taajuudesta olevan jotain 
paljon harvempaa kuin "joka päivä". Juuri tämä taajuuden yllättävyys (joka on
läsnä myös esimerkissä \ref{ee_kropasta}) näyttäisikin olevan 
affektiivisuuden taustalla suurimmassa osassa tätä konstruktiota edustavia
F1a-tapauksia. Tämä korostuu myös esimerkissä \ref{ee_kuolemalta}, josta
on helppo tulkita, ettei lasten pelastaminen varmalta kuolemalta ole
tapahtuma, jonka olettaisi toistuvan niinkin tiheästi kuin joka päivä:




\ex.\label{ee_kuolemalta} \exfont \small \emph{Joka päivä} hän pelasti pieniä lapsia varmalta kuolemalta.
(Araneum Finnicum: pudonneitaomenoita.blogspot.fi)





\vspace{0.4cm} 

\noindent
Yksi F1a-aineiston affektiivisista tapauksista erottuva piirre
on, että ne liittyvät hyvin usein esimerkkien \ref{ee_kropasta} -- \ref{ee_museohaku} 
tavoin ensimmäiseen persoonaan. Tarkkaan ottaen suomen F1a-aineiston alkusijaintitapausten
joukossa ensimmäistä persoonaa edustavia lauseita on melko vähän,
kaikkiaan 12 kappaletta (9,02 %). Lähemmässä tarkastelussa 
näistä yhtä vaille kaikki osoittautuivat affektiivisiksi.

Mielenkiintoista kyllä, venäjän F1a-aineiston S1-lauseissa ensimmäinen persoona
on paljon suomea tavallisempi -- sitä edustaa kaikkiaan 28,99 %
kaikista 1 046 venäjän S1-tapauksesta. 
Useimmiten (193 tapausta) kyse
on monikon ensimmäisestä persoonasta, ja tässä joukossa on muun
muassa seuraavanlaisia esimerkkejä:




\ex.\label{ee_bldene} \exfont \small \emph{Каждый месяц} мы проводим благотворительный день. (Araneum
Russicum: organicshopaz.ru)






\ex.\label{ee_neko} \exfont \small \emph{Каждый день} мы помогаем различным компаниям разрешать задачи в
различных областях: экспертизы, оценки, и сопутствующие им. (Araneum
Russicum: neko-business.ru)






\ex.\label{ee_provodim2} \exfont \small \emph{Каждый месяц} мы проводим мероприятие и, как правило, не одно.
(Araneum Russicum: itrend.ru)






\ex.\label{ee_rassylka} \exfont \small \emph{Каждую неделю} мы осуществляем бесплатную рассылку на тему
интернет-маркетинга, разработки и продвижения сайтов. (Araneum Russicum:
seogigant.ru)





\vspace{0.4cm} 

\noindent

Esimerkit \ref{ee_bldene} -- \ref{ee_rassylka} eroavat edellä esitetyistä suomenkielisistä
tapauksista siinä, etteivät ne korosta niissä kuvattujen tapahtumien
esiintymistiheyden yllättävyyttä, vaan toteavat neutraalisti, että joka päivä
tai joka kuukausi tapahtuu jotakin. Esimerkiksi virke \ref{ee_bldene} 
voidaan laajemman kontekstin perusteella analysoida alatopiikkikonstruktioksi:

\begin{quote}%
Organic Shop - это модная сеть органических магазинов, которым доверяют.

В Organic Shop собрано более 80 брендов и 2500 наименований натуральной
косметики-\/- Поэтому в Organic Shop вы найдете только самые лучшие
средства, наполненные силой самой природы.

Каждый месяц мы проводим благотворительный день. Совершая покупку в этот
день, вы помогаете детским домам, домам малютки и детям, оставшимся в
сложной жизненной ситуации.

Organic Shop – это территория, объединяющая единомышленников, которые
выбирают для себя лучшее и пользуются только органической и натуральной
косметикой, зная все ее преимущества.

http://www.organic-shops.ru/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkissä \ref{ee_bldene} koko tekstin (ja tekstin taustalla olevan sivuston)
diskurssitopiikkina on *Organic Shop* -yritys, josta teksti luettelee
listamaisesti erilaisia tietoja -- yhtenä tietona sen, että yritys järjestää
joka kuukausi hyväntekeväisyyspäivän. Erona moniin aiemmin käsiteltyihin
alatopiikkitapauksiin on se, ettei tekstiä ole segmentoitu 
lähtökohtaisesti ajallisin perustein, vaan esimerkin \ref{ee_bldene} ajanilmaus on vain yksi 
diskurssitopiikkia pilkkova lohko muiden joukossa. 

Kuten aiemmin tässä luvussa on nähty, alatopiikkikonstruktiot sinänsä
ovat suomessakin tavallinen keino jäsentää tekstiä. Tässä luvussa
toistaiseksi käsitellyt tapaukset ovat kuitenkin useimmiten olleet 
lähtökohtaisesti kronologisesti rakennettuja tekstejä, kuten vaikkapa esimerkit 
\ref{ee_kiuruvesi}, \ref{ee_pirttimaa} ja \ref{ee_kipinavartti}. Myös suomen F1a-aineistosta
kuitenkin löytyy joitakin virkettä \ref{ee_bldene} muistuttavia tapauksia, joissa 
esimerkiksi jotakin yritystä kuvataan esittämällä jokin siihen liittyvä toistuva tapahtuma.
Tällainen on esimerkki \ref{ee_hullutpaivat}:




\ex.\label{ee_hullutpaivat} \exfont \small \emph{Joka vuosi} tavaratalo täyttää kaupungin keskustan sekä pysyvillä
että kävelevillä mainoksilla. (Araneum Finnicum: sitvasfi.wordpress.com)





\vspace{0.4cm} 

\noindent
Esimerkin laajempi konteksti on seuraavanlainen:

\begin{quote}%
Stockmannin Hullut Päivät ovat taas täällä. Keltaisten kassien armeija
on saapumassa Helsingin kaduille. Joka vuosi tavaratalo täyttää
kaupungin keskustan sekä pysyvillä että kävelevillä mainoksilla. Hullut
päivät tarjoaa mahdollisuuden kuluttaa tavoilla, joita ei normaalisti
tule ajatelleeksi.

https://sitvasfi.wordpress.com/2013/10/09/paljunostajaa-etsimassa/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Laajempi konteksti osoittaa esimerkin \ref{ee_hullutpaivat} olevan peräisin
*Sitoutumattoman vasemmiston* verkkosivuilta, joiden alaotsikkona on *Punkkua
porvarin pöytäliinalla jo vuodesta 1983*. Esimerkin \ref{ee_hullutpaivat}
kirjoittajan lähtökohtana voikin hyvin pitää kulutusyhteiskunnan kritisoimista,
ja Hulluja päiviä käsitellään kaikkea muuta kuin neutraalissa valossa. Tässä
mielessä esimerkin  lauseenalkuinen *Joka vuosi* ei ole tyyliltään samanlainen
alatopiikki kuin esimerkissä \ref{ee_bldene}, vaan lähempänä affektiivisia
konstruktioita.

Suomen ja venäjän välillä F1a-aineistossa havaittavassa erossa ei kuitenkaan
ole kyse siitä, etteikö alkusijainnilla olisi affektiivista vaikutusta myös venäjässä.
Venäjänkielisen aineisto sisältääkin muun muassa seuraavan affektiiviseksi
tulkittavissa olevan tapauksen:




\ex.\label{ee_narushat} \exfont \small И \emph{каждый день} мы нарушаем неписаные законы: приходим в слишком
фривольном декольте, приглашаем на кофе секретаршу старшего босса или
распечатываем новый роман Акунина на принтере компании. (RuPress:
Советский спорт)





\vspace{0.4cm} 

\noindent
Lisäksi on todettava, että vaikka määrää painottavia tapauksia on selvästi
vähemmän kuin suomen F1a-aineistossa (5,83 %), 
esiintyy niitä yhtä kaikki myös venäjässä:




\ex.\label{ee_superjob} \exfont \small \emph{Каждый год} более чем 1000 человек находят работу с помощью нашего
кадрового холдинга (Araneum Russicum: volgograd.superjob.ru)






\ex.\label{ee_deti70k} \exfont \small \emph{Каждый год} более 70 тысяч детей из таких семей получают за счет
казны продукты, одежду и обувь, посещают культурные мероприятия.
(RuPress: Труд-7)





\vspace{0.4cm} 

\noindent
Kolmanneksi  kummankin kielen F1a-aineistossa tavataan  matriisissa 11 esiteltyä topikaalista konstruktiota, joita
edustavat seuraavat tapaukset:




\ex.\label{ee_baikalforum} \exfont \small Форум на берегу Байкала проходит уже в третий раз, и каждый год депутат
находит время в своем плотном графике, чтобы пообщаться с молодыми
инженерами. (Araneum Russicum: sergeyten.ru)






\ex.\label{ee_ruokaviesti} \exfont \small Joka vuosi viestimme on poikinut vilkkaan keskustelun myös mediassa
elintarvikkeiden tuoreudesta, jäljitettävyydestä ja laadusta ”, haastaa
Kärkkäinen. (Araneum Finnicum: sisa-savo.fi)





\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_baikalforum} ja \ref{ee_ruokaviesti} vuosiin suhtaudutaan samalla
tavalla topiikkeina kuin vaikkapa *siihen aikaan* esimerkissä \ref{ee_pakit} osiossa
\ref{sekv-s1}. Venäjänkielisessä esimerkissä tämä topiikki ikään kuin alustetaan
yhdyslauseen ensimmäisessä lauseessa, jossa kerrotaan, että foorumi on
järjestetty kolme kertaa; jälkimmäisessä lauseessa kustakin järjestämiskerrasta kerrotaan
jokin (yksi ja sama) väite. Suomenkielisessä tapauksessa topiikin alustaminen tapahtuu
hivenen aiemmin, kuten laajemmasta kontekstista käy ilmi:


\begin{quote}%
Marja-, karja- ja Järvi-Suomen keskeltä polkaistava Lähiruokaviesti
ajellaan tänä vuonna elokuun lopussa, ma-pe 23.8.–27.8.2010. Jo
neljännen kerran järjestettävä polkupyöräviesti päättyy tasavallan
presidentin linnaan-\/-

-\/-Joka vuosi viestimme on poikinut vilkkaan keskustelun myös mediassa
elintarvikkeiden tuoreudesta, jäljitettävyydestä ja laadusta”, haastaa
Kärkkäinen:

https://www.yrittajanpaiva.fi/yrittajan-paivan-tapahtumat-2010/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Tyyliltään neutraalien alatopiikkikonstruktioiden lisäksi toinen suomen ja venäjän alkusijaintitapauksia 
erottava konstruktio ovat F1a-aineistojenkin kohdalla johdantokonstruktiot. Venäjänkielisestä
aineistosta voidaan löytää muun muassa seuraava tapaus:




\ex.\label{ee_truby} \exfont \small Каждый день люди пользуются канализацией для бытовых нужд. (Araneum
Russicum: vse-o-trubah.ru)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_truby} laajempi konteksti osoittaa, että kyseessä on koko tekstin
ensimmäinen virke ja siten tarkemmin määriteltynä globaali johdantokonstruktio:

\begin{quote}%
Современное оборудование для прочистки труб

Каждый день люди пользуются канализацией для бытовых нужд. Со временем
может наступить момент, когда накопившийся на стенках мусор полностью
перекроет канализационную магистраль. Помочь в таком случае сможет
специальная машина для прочистки труб. Эта проблема достаточно актуальна
и для промышленных предприятий. Эффективно устранить возникающие
неприятности сможет правильный выбор оборудования.

http://vse-o-trubah.ru/oborudovanie-dlya-prochistki-trub.html
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Venäjänkielisessä aineistossa ovat ylipäätään tavallisia tapaukset, joissa
subjektina on geneerisesti kaikkiin ihmisiin viittaava *люди* -- kaiken 
kaikkiaan tällaisia tapauksia on 15 kappaletta, ja 
ne ovat myös muissa tapauksissa usein joko globaaleja tai lokaaleja johdantokonstruktioita,
kuten seuraavat esimerkit:




\ex.\label{ee_pripravy} \exfont \small Буквально каждый день люди употребляют в пищу различные приправы и
специи. (Araneum Russicum: kylesh.ru)






\ex.\label{ee_molvu} \exfont \small Каждый день люди пишут о разных компаниях, распространяя молву. (Araneum
Russicum: creditcoop.ru)






\ex.\label{ee_ispolzujetvodu} \exfont \small Каждый день на протяжении всей жизни человек использует воду в различных
целях. (Araneum Russicum: vedeniehoziaystva.ru)





\vspace{0.4cm} 

\noindent
Suomessa vastaavia tapauksia ei löydy, ja periaatteessa merkitykseltään
geneerisetkin S1-tapaukset, kuten esimerkki \ref{ee_synti}
edellä, saavat useimmiten affektiivisen tulkinnan.

Tiivistäen F1a-aineistosta voidaan todeta, että siihen kuuluvat ajanilmaukset
esiintyvät monissa sellaisissa konstruktioissa, joita edellä ollaan jo
käsitelty ja joiden käytössä jo edellä avaittiin tiettyjä kieltenvälisiä
eroavaisuuksia -- esimerkiksi johdantokonstruktion liittyminen selvemmin juuri
venäjään ja määrää painottavan konstruktion tyypillisyys suomelle. Niin suomi
kuin venäjä hyödyntävät taajuutta ilmaisevaa alatopiikkikonstruktiota, joskin
sillä erotuksella, että venäjässä käyttö on laajempaa ja ulottuu selvemmin myös
ensimmäistä persoonaa edustavan subjektin sisältäviin tapauksiin. Suomessa taas 
F1a-ryhmän S1-tapausten erityispiirteeksi havaittiin affektinen konstruktio, jonka
olemassaolon voi ylipäätään nähdä S1-sijainnin käyttöä rajoittavana tekijänä
suomessa. 

##### Aineistoryhmät F1b, F4 ja F3d {#s1-f1b}

Käännän nyt huomioni aineistoryhmiin F1b, F4 ja F3d, joista kaksi jälkimmäistä (ks. osio \ref{f3a-f3b-f3c-f3d-f3e})
eroavat F1a-ryhmästä muun muassa siinä, että ne ovat morfologiselta rakenteelaan adverbisiä -- F3d-tapauksia
käsiteltiinkin jonkin verran ei--deiktisten adverbien yhteydessä luvussa \ref{eideikt}.
Kuten osiossa  \ref{s1-sim-pos} puolestaan huomautettiin, F1b-ryhmä
(\ref{f1a-f1b}) pitää sisällään positionaalisia ilmauksia. F1b-tapaukset käyttäytyvätkin pitkälti
samoin kuin toinen viikonpäiviä sisältänyt ryhmä L2a edellä. Koko suomen F1b-aineistoa
kuvaa hyvin esimerkki \ref{ee_etatoita}, josta esitän seuraavassa myös varsinaista tutkimusaineiston
virkettä edeltävän virkkeen:




\ex.\label{ee_etatoita} \exfont \small Maanantaisin, tiistaisin ja keskiviikkoisin Saarinen matkustaa junalla
työpaikalleen Helsinkiin ja takaisin. \emph{Torstaisin} hän tekee
etätöitä kotona ja perjantaisin on vapaalla lastensa kanssa. (Araneum
Finnicum: uralehti.fi)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_etatoita} näyttäytyy tyypillisenä ajallisen alatopiikkikonstruktion
edustajana: kuten suomen S1-sijoittuneissa viikonpäivätapauksissa yleensä, 
tässäkin esitetyllä tekstikappaleella on ajallinen diskurssitopiikki (viikko), jota
käsitellään alatopiikkeina toimivien viikonpäivien avulla. Esimerkki vertautuukin
hyvin esimerkkeihin \ref{ee_kipinavartti} ja \ref{ee_maunuvaara} edellä.
Kaiken kaikkiaan suomen ja venäjän F1b-ryhmien suhteen voikin nähdä samana kuin L2-ryhmien
suhteet edellä.

F4- ja F3d-ryhmät ovat merkitykseltään hyvin lähellä toisiaan, joten voisi
olettaa, että ajanilmaukset sijoittuvat niissä toistensa kaltaisesti.
Kuten kuviosta 24 havaitaan, kummassakin kielessä F3d-ryhmä
on kuitenkin tavallisempi lauseen alussa kuin F4-ryhmä -- suomessa melko selvästikin.
Suomessa S1-tapausten määrän suhde kaikkiin tapauksiin on nimittäin F3d-ryhmässä
 241 / 1 756 
 (13,72 %) ja F4-ryhmässä
 121 / 2 501
 (4,84 %);
venäjässä vastaavat suhteet ovat
 597 / 1 667 
 (35,81 %) ja 
 219 / 725 (30,21 %).
Lähemmässä tarkastelussa molempien kielten F4-tapaukset muistuttavat edellä esimerkissä 
\ref{ee_voittajat} nähtyä F3d-aineiston kontrastiivista konstruktiota:




\ex.\label{ee_tyokiireet} \exfont \small Isä huomioi tyttäriään, puuhailee heidän kanssaan, vaikka
\emph{ajoittain} työkiireet vievät aikaa. (Araneum Finnicum:
vaestoliitto.fi)






\ex.\label{ee_liikenne} \exfont \small Liikenne oli melko vähäistä, joskin \emph{ajoittain} rekat keräsivät
lyhyitä jonoja. (Araneum Finnicum: noro.fi)






\ex.\label{ee_krovl} \exfont \small Во-вторых, технические характеристики мягкой кровли не уступают, а
\emph{временами} – существенно превышают остальные виды кровли. (Araneum
Russicum: krovlia.com.ua)






\ex.\label{ee_tshernota} \exfont \small Глубокая чернота ночного неба в состоянии не только манить наш взор, но
\emph{временами} она дает нам ответы на заданные вопросы. (Araneum
Russicum: astrohit.ru)





\vspace{0.4cm} 

\noindent
Venäjänkielisestä F4-aineistosta on kuitenkin
löydettävissä myös esimerkiksi seuraavanlaisia tapauksia, joissa *временами*-sanaa edeltää
*jos*- tai *kun*-sivulause:




\ex.\label{ee_protokoly} \exfont \small Когда у терапевта уже есть опыт в использовании этих протоколов,
\emph{временами} он обнаруживает удивительное соответствие между
образами, символами и темами метафор и еще не раскрытыми аспектами жизни
клиента. (Araneum Russicum: nlpr.ru)






\ex.\label{ee_vibratsija} \exfont \small Теперь, если некоторое время держать прибор в руках, временами вы
почувствуете кратковременную вибрацию. (Araneum Russicum: desole.ru)






\ex.\label{ee_potok} \exfont \small Даже сейчас, когда сила потока была гораздо слабее, временами он
подхватывал нас и нес ... вверх, против течения. (Araneum Russicum:
barque.ru)





\vspace{0.4cm} 

\noindent
Esimerkkien \ref{ee_protokoly} -- \ref{ee_potok} tapauksissa kielten välillä on lähinnä
kieliopillinen ero siinä, ettei alkusijainti ole suomessa mahdollinen.

Loppujen lopuksi tässä tarkastellut muut F1-ryhmät eivät anna erityisen paljon lisävaloa suomen
ja venäjän alkusijaintia koskeville eroavaisuuksille. Mietittäessä sitä, miksi  F-funktio
vaikuttaa suomen S1-todennäköisyyteen venäjään nähden negatiivisesti, suurin selitysvoima on
toisaalta edellä käsitellyillä
F1a-tapauksilla ja niihin suomessa liittyvällä affektiivisuudesta, toisaalta
F1b-ryhmän positionaalisuudella ja toisaalta F4-ryhmän kohdalla havaituilla
osin kieliopillisillakin eroilla. 



#### Sekventiaalis-duratiivinen ja duratiivinen funktio

\todo[inline]{Tähän myös duratiivinen funktio,
mutta maininta, että enemmän käsitellään myöhemmissä
luvuissa.}




Suomen alkusijainnin todennäköisyyttä pienentävistä tekijöistä viimeisenä tarkastellaan
sekventiaalis--duratiivista ja duratiivista funktiota, joista jälkimmäinen on kuitenkin 
tarkemman huomion kohteena myös loppusijainnin yhteydessä luvussa
\ref{duratiivinen-funktio}. Sekventiaalis--duratiivisten ajanilmausten 
ja kielen välinen interaktio on kuvion 15
perusteella muita semanttisia funktioita voimakkaampi,
mutta lähemmässä tarkastelussa kävi ilmi, ettei 
niiden selitysvoima ole erityisen suuri mietittäessä suomen ja venäjän välisiä
eroja yleisellä tasolla. Itse asiassa suurimman sekventiaalis--duratiivisen ryhmän, L9d:n, osalla
S1-tapausten määrään on venäjänkielisessä aineistossa vaikuttanut
myös ongelma korpushaun tarkkuudessa. Osiossa \ref{l9a-l9b-l9c-l9d} L9d-ryhmään määriteltiin
haettavaksi tapauksia, joissa c-prepositiota seuraa genetiivissä oleva vuosiluku, mutta
esimerkit \ref{ee_gostparada} -- \ref{ee_bavarskaja} osoittavat, että näiden tapausten joukkoon
mahtuu myös eräitä simultaanista funktiota toteuttavia tapauksia [@tobecited]:




\ex.\label{ee_gostparada} \exfont \small Почетным гостем парада этого года была Индия, страна, с которой \emph{с
1998 года} Франция заключила договор о стратегическом партнерстве.
(Araneum Ru ssicum: echo.msk.ru)






\ex.\label{ee_bavarskaja} \exfont \small \emph{С 1972 года} Баварская академия наук заключила с ним контракт для
дальнейшей исследовательской работы в области древнерусского
богослужебного пения. (Araneum Russicum: portal-credo.ru)





\vspace{0.4cm} 

\noindent
Kaikissa esimerkeissä \ref{ee_gostparada} -- \ref{ee_elkarty} ajanilmaus lokalisoi nimenomaan yksittäisen 
tapahtuman eikä jotakin jatkuvaa asiaintilaa. Vastaavat esimerkit eivät ole aineistossa erityisen
yleisiä, mutta niiden läsnäolo saattaa osaltaan selittää sitä, ettei sekventiaalis--duratiivisten
tapausten selitysvoima suomen ja venäjän sijaintieroille lopulta ole niin suuri, kuin
kuvion 15 perusteella ensi katsomalta voisi olettaa. Taustalla on myös
syvempi merkitysero suomen ja venäjän sekventiaalis--duratiivisissa ilmauksissa. Venäjässä
sekventiaalis--duratiivisilla ilmauksilla kuvataan myös ylipäätään sellaisia tilanteita,
joissa jokin toiminta alkaa tietyllä hetkellä ja jatkuu sen jälkeen, kuten esimerkissä 
\ref{ee_elkarty}:




\ex.\label{ee_elkarty} \exfont \small Где-то \emph{с 1994 года} я начал делать электронные карты Москвы.
(Araneum Russicum: alexjourba.dreamwidth.org)





\vspace{0.4cm} 

\noindent
Suomessa vastaavat *alkaa* + infinitiivi -rakenteet kuitenkin toteutetaan simultaanisilla
ajanilmauksilla, kuten *vuonna 1994*. Tämä sekventiaalis--duratiivisten
aineistojen taustalla olevan semanttisen eronkin voi nähdä vaikuttaneen siihen, miksi 
suomessa sekventiaalis--duratiivisten ilmausten lauseenalkuiset käyttötilanteet
ovat niin selkeästi venäjää harvinaisemmat.

Kaiken kaikkiaan sekventiaalis-duratiivinen funktio koostuu aineistoryhmistä 
L7a, L9b ja L9d. Kuten edellä kävi ilmi, näistä suurin on  L9d,
joka suomessa kattaa 3 947 
ja venäjässä 1 008 tapausta.
Suomessa S1-asemaan sijoittuu näistä 106 (2,69 %)
ja venäjässä 271 (26,88 %). 
Voidaan siis sanoa, että kummassakin kielessä L9d-aineistoryhmän edustajat sijoittuvat
lauseen alkuun keskimääräistä harvinaisemmin, joskin niin, että suomessa poikkeama on 
selvästi suurempi.

L9d-aineistoryhmä on merkitykseltään lähellä edellä käsiteltyä L9a-ryhmää siinä
mielessä, että kummassakin on kyse tapahtumien lokalisoimisesta vuosilukujen avulla.
Voisikin ajatella, että myös L9d-ryhmässä havaittavaa sijaintieroa selittävät
ne syyt, jotka kielten välisiin eroihin löydettiin L9a-ryhmää analysoitaessa,
eli toisaalta erot alatopiikkikonstruktion käytössä, toisaalta venäjälle
tyypillinen johdantokonstruktio. Tämä tulkinta saa tukea, kun katsotaan
toista sekventiaalis-duratiivista aineistoryhmää L9b, joka ei viittaa konkreettisiin vuosilukuihin
vaan pienempiin, tiettyihin kuukausiin ankkuroitaviin ajanjaksoihin kuten
*viime vuoden tammikuusta*. Näissä tapauksissa -- samoin kuin kolmannessa sekventiaalis-duratiivisessa
aineistoryhmässä L7a -- suomen S1-osuus on itse asiassa keskimääräistä
suurempi. Kuviossa 15 havaitun  suomen S1-todennäköisyyttä pienentävän
vaikutuksen taustalla on siis nähtävä juuri L9d-aineistoryhmä.

Jos suomen 106:ta L9d-ryhmän S1-tapausta tutkitaan
tarkemmin, havaitaan, että kuten L9b-ryhmän kohdalla, myös L9d-tapauksissa merkittävä
syy lauseenalkuiseen sijaintiin on alatopiikkikonstruktio, josta yksi selvä esimerkki
on virke \ref{ee_oopperat}:




\ex.\label{ee_oopperat} \exfont \small \emph{Vuodesta 1997} hän on säännöllisesti kouluttanut laulajia
Riikassa, Latvian Kansallisoopperassa ja vuodesta 2000 Suomen
Kansallisoopperassa. (Araneum Finnicum: kangasniemenmusiikkiviikot.fi)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_oopperat} laajempi konteksti osoittaa, että kyse on Irina
Gavrilovici -nimistä muusikkoa kuvailevasta tiiviistä elämäkertamaisesta tekstistä, eli 
tyypillisestä alatopiikkistrategian käyttökohteesta (vrt. mm. esimerkki \ref{ee_nagorle} edellä):

\begin{quote}%
Romanialaissyntyinen professori Irina Gavrilovici opiskeli aluksi
pianonsoittoa ja laulua L. Boicescu-Grosowscajan johdolla.
Opiskeluaikanaan Irina Gavrilovici esiintyi sekä laulajana että
pianistina. Hän valmistui Bukarestin Musiikkiakatemiasta Romaniasta-\/-

Muutettuaan 1986 Itävaltaan Wieniin Irina Gavrilovici perusti oman
laulustudion, johon tulee oppilaita useista eri maista. -\/-

Irina Gavrilovici opettaa Kangasniemen lisäksi mestarikursseilla Sofian
Musiikkiakatemiassa, Wienissä ja Turussa. \emph{Vuodesta 1997} hän on
säännöllisesti kouluttanut laulajia Riikassa, Latvian
Kansallisoopperassa ja vuodesta 2000 Suomen Kansallisoopperassa.

http://www.kangasniemenmusiikkiviikot.fi/fi/kurssit/laulun-kesaakatemia/kesaakatemia-opettajat
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkki \ref{ee_oopperat} tuo hyvin ilmi sen, että tyypillinen syy vuosiluvun esiintymiseen
lauseen alussa on etenkin suomessa alatopiikkien käyttö -- riippumatta siitä, edustaako
ajanilmaus simultaanista vai sekventiaalis-duratiivista funktiota.
Sekventiaalis--duratiivisilla ilmauksilla tekstiä jaottelevan alatopiikin rooli
vaikuttaa kuitenkin harvinaisemmalta kuin monet muut roolit, minkä takia niin
suomessa kuin venäjässä S1-tapauksia on keskimääräistä vähemmän. 
Yksi suomen- ja venäjänkielisiä L9d-aineistoja erottava tekijä ovat kuitenkin 
esimerkin \ref{ee_columbia} kaltaiset virkkeet, jotka eivät ole tulkittavissa
esimerkin \ref{ee_oopperat} tavoin alatopiikkitapauksiksi, vaan osoittautuvat
lähemmässä tarkastelussa monien edellä tutkittujen aineistoryhmien tavoin
johdantokonstruktioksi:




\ex.\label{ee_columbia} \exfont \small С 1979 года с помощью Уникальной Системы Всестороннего Образования
Columbia International College готовит студентов стать ответственными
гражданами и в то же самое время поступить в лучшие университеты мира.
(Araneum Russicum: bec-dnepr.com)





\vspace{0.4cm} 

\noindent
Esimerkin laajempi konteksti osoittaa, että kyseessä on kappaleensa ensimmäinen virke,
Columbia International College -yliopistosta kertovan sivuston ESL-ohjelmasta kertovassa
osiossa:

\begin{quote}%
Программа ESL (Английский как второй язык)

С 1979 года с помощью Уникальной Системы Всестороннего Образования
Columbia International College готовит студентов стать ответственными
гражданами и в тоже самое время поступить в лучшие университеты мира.
Эта уникальная система раскрывает потенциал студента, его академические
способности и помогает воплотить его мечты в реальность. Система
Всестороннего Образования – самая лучшая канадская система образования
для международных студентов.

http://www.bec-dnepr.com/school/canada/columbia\_international\_college
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Johdantokonstruktion lisäksi venäjän L9d-aineisto pitää kuitenkin sisällään
myös seuraavat esimerkit, joiden erityispiirteenä on se, että informaatiorakenteen kannalta
tarkasteltuna niissä välitettävät propositiot ovat osa pragmaattista
väittämää:




\ex.\label{ee_voldemort} \exfont \small Примерно с 1950 года Волан-де-Морт начал активную деятельность. (Araneum
Russicum: ru.harrypotter.wikia.com)






\ex.\label{ee_spereryvom} \exfont \small С 1925 года с небольшим перерывом он занимает этот пост до 1953 года, то
есть до самой своей кончины. (Araneum Russicum: historians.in.ua)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_voldemort} on osa Harry Potter -fanisivuston Voldemort-hahmosta
kertovaa elämäkertaa, ja sijoittuu tekstin keskivaiheille. Tähän mennessä
lukijan voi perustellusti tulkita olevan tietoinen propositiosta [Voldemort on
joskus aloittanut aktiivisen toiminnan]. Esimerkki \ref{ee_voldemort} spesifioi,
että Voldemortin aktiivinen toiminta on alkanut suurin piirtein vuonna 1950,
mikä tarkoittaa, että ajanilmauksen funktio on koko lailla fokaalinen --
huolimatta siitä, että se sijaitsee lauseen alussa. Tämä näkyy vielä selvemmin
esimerkissä \ref{ee_spereryvom}, jossa pronomini-ilmaus *этот пост* osoittaa, että
kirjoittaja olettaa lukijan olevan tietoinen propositiosta [x on hoitanut
pestiä y] ja määrittelee nyt, mistä mihin pesti on kestänyt.

Kuten jo tämän osion alussa todettiin, kuvion 15
mukaan duratiivisenkin funktion vaikutus S1-sijainnin todennäköisyyteen on
suomessa venäjään nähden selvästi pienentävä. Duratiivisen funktion osalta
esimerkkien \ref{ee_voldemort} ja \ref{ee_spereryvom} kaltaisia fokaalisia mutta
lauseenalkuisia tapauksia on etenkin E1a-aineistoryhmästä (ks. osio \ref{e1a-e1b-e1c})
helppo löytää:




\ex.\label{ee_istoriju} \exfont \small Шесть часов я писал эту историю. (Araneum Russicum: pelevin.nov.ru)






\ex.\label{ee_mest} \exfont \small -\/- Восемь лет он вынашивал план мести и только в феврале 2010-го решил
его осуществить. (RuPress: Комсомольская правда)






\ex.\label{ee_marihuanu} \exfont \small Восемь лет я курила марихуану, из них пять лет марихуану выращенную
гидропоническим способом, которая имеет огромное негативное влияние на
весь организм человека. (Araneum Russicum: marinagribanova.com)






\ex.\label{ee_tseremonii} \exfont \small Пять лет я веду чайные церемонии и тонко чувствую настроение группы.
(Araneum Russicum: teamaster.info)





\vspace{0.4cm} 

\noindent
Ajanilmauksella voidaan esimerkeissä \ref{ee_istoriju} -- \ref{ee_tseremonii} kaikissa
nähdä nimenomaan fokaalinen funktio. Tämän lisäksi on kuitenkin todettava, että
tyylillisesti niitä leimaa alkusijainnille tyypillinen affektiivisuus.
Kirjoittajan tarkoituksena vaikuttaisi olevan paitsi sen raportoiminen, kuinka
kauan esimerkiksi jutun kirjoittaminen on kestänyt, myös sen vaikutelman
välittäminen, että kulunut aika on pidempi kuin ehkä ennalta olettaisi. Tässä
mielessä esimerkit vertautuvat F1a-ryhmän yhteydessä tarkasteltuihin
tapauksiin, joissa niissäkin ajanilmauksille voidaan määritellä myös jossain
määrin fokaalinen diskurssifunktio.

Jos katsotaan pelkästään E1a-ryhmää, voidaan todeta, että venäjässä S1-tapauksia on kutakuinkin saman
verran kuin koko aineistossa keskimäärin, 38,47 %
(222 kappaletta), mutta suomessa selvästi keskimääräistä vähemmän
(5,70 % / 
 64 kpl). Ero vaikuttaisi
selittyvän sillä, että  vaikka esimerkkien \ref{ee_istoriju} -- \ref{ee_tseremonii}
kaltaiset fokaaliset rakenteet ovat suomessa mahdollisia, niiden käyttäminen ei
viestintästrategiana  ole likimainkaan yhtä yleistä kuin venäjässä. Esimerkkejä
suomen vastaavista rakenteista ovat seuraavat lauseet:




\ex.\label{ee_mikkanen} \exfont \small Peräti kuusi vuotta Mikkanen kutoi neuleita tunnetulle
tekstiilitaiteilijalle ja neulesuunnittelijalle Sirkka Könöselle.
(FiPress: Aamulehti)






\ex.\label{ee_tv7} \exfont \small Jo viisi vuotta hän on järjestänyt toimintaa ja erilaisia tilaisuuksia
ilahduttaakseen lapsensa menettäneitä vanhempia ja tukeakseen heidän
päivittäistä jaksamistaan. (Araneum Finnicum: tv7.fi)





\vspace{0.4cm} 

\noindent
Toisin kuin venäjän E1a-aineistossa, jossa esimerkit \ref{ee_istoriju} --
\ref{ee_tseremonii} edustavat valtavirtaa, ovat esimerkkien \ref{ee_mikkanen} ja \ref{ee_tv7}
kaltaiset tapaukset suomessa kuitenkin vähemmistössä. Tavallisemmin suomen
E1a-aineiston alkusijaintitapaukset muistuttavat esimerkkejä \ref{ee_ekat10} ja
\ref{ee_stipendiaatit}:




\ex.\label{ee_ekat10} \exfont \small Ensimmäiset kymmenen vuotta hän johti Eläketiedotusta, josta siirtyi
koordinoimaan sidosryhmätyötä. (Araneum Finnicum: tela.fi)






\ex.\label{ee_stipendiaatit} \exfont \small Niistä kaksi viikkoa stipendiaatit asuvat perheissä, joissa on
samanikäisiä nuoria, ja toiset kaksi viikkoa he tekevät osana
kansainvälisiä koululaisryhmiä retkiä esimerkiksi Köln/ Bonniin,
Müncheniin ja Hampuriin. (Araneum Finnicum: helsinki.diplo.de)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_ekat10} laajempana kontekstina on seuraava ingressimäinen kappale
työeläkevakuuttajien uutiskirjeessä:

\begin{quote}%
Esittelyssä Markku J. Jääskeläinen

Markku J. Jääskeläinen toimii sidosryhmäsuhteista vastaavana
johtajanamme. Hän on tullut taloon vuonna 2001. Ensimmäiset kymmenen
vuotta hän johti Eläketiedotusta, josta siirtyi koordinoimaan
sidosryhmätyötä.

https://www.tela.fi/uutiskirje/artikkeli/1/0/esittelyssa\_markku\_j\_jaaskelainen
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkki \ref{ee_ekat10} voidaankin tulkita tekstin diskurssitopiikkina olevan
Markku Jääskeläisen elämää jaottelevaksi alatopiikkikonstruktioksi. Esimerkki \ref{ee_stipendiaatit}
puolestaan on seuraavassa esitetyn laajemman tekstiyhteytensä perusteella 
tarkemmin luokiteltavissa ajalliseksi alatopiikkikonstruktioksi:

\begin{quote}%
Tämän ryhmän lisäksi matkaan lähtee myös neljä erityisstipendin saajaa
Espoosta, Vaajakoskelta, Kirkkonummelta ja Nokialta. Heidän stipendiinsä
sisältyy neljän viikon oleskelu Saksassa. Niistä kaksi viikkoa
stipendiaatit asuvat perheissä, joissa on samanikäisiä nuoria, ja toiset
kaksi viikkoa he tekevät osana kansainvälisiä koululaisryhmiä retkiä
esimerkiksi Köln/Bonniin, Müncheniin ja Hampuriin.
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Kontekstinsa perusteella esimerkki \ref{ee_stipendiaatit} vertautuu muun muassa
esimerkkeihin \ref{ee_maunuvaara} ja \ref{ee_etatoita}, joissa kirjoittaja nimenomaan
luonnehtii tiettyä rajattua ajanjaksoa käsittelemällä erikseen sen ennalta
tiedettyjä osasia.

Esimerkit \ref{ee_istoriju} -- \ref{ee_tv7} ovat siinä mielessä erityisen
merkillepantavia, että ne osoittavat hyvin samanlaisten rakenteiden
olevan käytössä niin venäjässä kuin suomessakin. Kuitenkin suomessa rakenteiden
käyttö on selvästi vähäisempää, mikä havaittiin myös vastaavien affektiivisten
rakenteiden kohdalla F1a-aineistossa. Mielenkiintoista kyllä, samaan tapaan kuin 
monet F1a-aineiston yhteydessä tarkastellut suomen alkusijaintitapaukset, myös esimerkki  \ref{ee_tv7}
on peräisin uskonnollisesta kontekstista (kristillinen televisiokanava TV7). Voidaankin
ehkä tehdä se tulkinta, että suomessa esimerkin \ref{ee_tv7} kaltaisten alkusijaintitapausten
affektiivisuus tulkitaan venäjää voimakkaampana ja selkeämpänä poikkeamana normista, jolloin
niiden käyttö onkin rajoittunut vain melko kapeisiin konteksteihin -- vähemmän spesifeissä
ympäristöissä tapaukset tulisivat mahdollisesti tulkituksi turhaksi patetiaksi.

Fokaalisten mutta tyyliltään affektiivisten tapausten lisäksi toisen alkusijaintia hyödyntävien,
etenkin venäjässä suosittujen rakenteiden joukon muodostavat ne E2b-ryhmän tapaukset, joissa
ajanilmauksen diskurssifunktio on päinvastoin topikaalinen. Näistä esimerkkejä 
ovat muun muassa seuraavat:




\ex.\label{ee_masterili} \exfont \small Всю неделю ребята мастерили игрушки, игры, собирали книги, приносили
диафильмы, видеофильмы для детей ГПД. (Araneum Russicum: доброштаб.рф)






\ex.\label{ee_pozdravljajut} \exfont \small Всю неделю люди активно поздравляют друг друга, а страна в эти дни
словно превращается в один большой праздник. (Araneum Russicum:
tlvision.net)






\ex.\label{ee_ukrashenija} \exfont \small Всю неделю ребята делали всевозможные новогодние украшения, как для
ёлки, так и в качестве подарков для родителей и друзей. (Araneum
Russicum: seymourhouse.ru)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_masterili} noudattavat samaa muun muassa esimerkin \ref{ee_pakit} yhteydessä käsiteltyä
rakennetta, jossa ensin edeltävässä diskurssissa jokin ajankohta esitellään siten,
että myöhemmässä tekstissä siihen voidaan viitata niin kuin mihin tahansa muuhun
referentiaaliseen entiteettiin. Esimerkin \ref{ee_masterili} tapauksessa referentin esittely näkyy 
tekstin laajemmassa kontekstissa selvästi, sillä koko tekstin otsikko on "Акции Осенней недели добра-2012" 
('Vuoden 2012 syksyllä järjestetyn hyvän tekemisen viikon tempauksia').
Jostain syystä nämä topikaaliset rakenteet puuttuvat melkein kokonaan suomenkielisestä aineistosta.

Ylipäätään suomen 121 E2b-tapauksesta ainoastaan kuusi
sijoittuu S1-asemaan, siinä missä venäjän 615 tapauksesta
alkusijaintiin osuu 267 ( 43,41 %).
Jo E2b-aineistojen yleinen kokoero on merkillepantava, ja kertoo siitä, ettei *koko viikon* ole suomessa
erityisen tyypillinen tapa viitata viikkoihin. S1-sijainnin epäluontevuus käy ilmi, jos
esimerkkiä \ref{ee_masterili} koettaa sovittaa suomalaiseen kontekstiin, kuten seuraavassa keksityssä
esimerkissä:




\ex.\label{ee_masterili_fi} \exfont \small Koulussamme järjestettiin hyvän tekemisen teemaviikko. Koko viikon
oppilaat askartelivat leluja, pelejä...





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_masterili_fi} ainakin oman tulkintani mukaan selvän sopimattomuuden voi nähdä olevan seurasta siitä,
että ilmaus *koko viikon* ei suomessa voi toimia samanlaisena anaforisesti ajalliseen topiikkiin
viittaavana ilmauksena kuin venäjässä. Asia käy selvemmäksi, kun tutkitaan tarkemmin suomenkieliseen aineistoon
todella päätyneitä alkusijaintitapauksia, kuten esimerkkejä \ref{ee_ojanko} ja \ref{ee_paivitti}




\ex.\label{ee_ojanko} \exfont \small Koko viikon Ojangon kävijät voivat äänestää omaa kuvasuosikkiaan.
(Araneum Finnicum: hskh.net)






\ex.\label{ee_paivitti} \exfont \small Koko viikon hän päivitti ilmatieteen laitoksen sivuja, ja iloitsi
jokaisesta tipahtaneesta lämpöasteesta. (Araneum Finnicum:
kissanpaivia.com)





\vspace{0.4cm} 

\noindent
Niin esimerkissä \ref{ee_ojanko} kuin \ref{ee_paivitti} on tulkintani mukaan kyse samasta
konstruktiosta kuin edellä E1a-aineiston yhteydessä käsiteltyjen affektiivisten
tapausten kohdalla. Esimerkissä \ref{ee_ojanko} kirjoittaja korostaa, että aikaa suosikin äänestämiselle
on koko viikko -- mahdollisesti korjatakseen potentiaalista väärinkäsitystä siitä, että äänestysaika
olisi jollain lailla rajatumpi. Esimerkki \ref{ee_paivitti} puolestaan on laajemman kontekstinsa perusteella
kommentti vuoden 2010 Helsinki city marathon -tapahtumaa käsittelevän blogikirjoituksen alla. Kesä 2010
oli erityisen helteinen, ja tähän liittyen kommentoija kirjoittaa:

\begin{quote}%
Minun työkaveri on juoksemassa tuota maratoonia. Koko viikon hän
päivitti ilmatieteen laitoksen sivuja, ja iloitsi jokaisesta
tipahtaneesta lämpöasteesta. Hän kylläkin toivoi vesisadetta, mutta
varmaankin siksi, ettei aurinko paistaisi. Semmoinen keli on kyllä ihan
mahdoton, kun sateen jälkeen paistaa aurinko, ja kaikki se satanut
kosteus imeytyy ilmaan ja ilma on niin tukahduttavaa, eikä hiki haihdu
iholta.. hyi olkoon!

http://www.kissanpaivia.com/24/?p=3050
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkissä \ref{ee_paivitti} ajanilmauksen diskurssifunktio on laajemman kontekstin perusteella
selvästi affektiivinen: kirjoittaja kuvaa tietynasteisella hämmästyksellä sitä, että työkaveri on käyttänyt huomattavan
osan ajastaan säätiedotusten epätavallisen tarkkaan seuraamiseen. Jos suomeksi olisi tarkoitus käyttää 
esimerkkien \ref{ee_masterili} -- \ref{ee_ukrashenija} tapaista topikaalista konstruktiota, olisi ajanilmaukseksi
vaihdettava esimerkiksi *viikon ajan* tai *sen ajan*.

Kaiken kaikkiaan duratiivisessa aineiston S1-erojen taustalla havaittavat tekijät ovat selvemmin
eriteltävissä kuin esimerkiksi aiemmin tässä osiossa käsitellyillä
sekventiaalis--duratiivisilla ilmauksilla. Erot palautuvat toisaalta jo F1a-aineiston
kohdalla havaittuun suomen maltillisempaan affektiivisen konstruktion käyttöön
ja toisaalta E2b-ryhmän referentiaalisuuteen liittyviin erityispiirteisiin.
Kuten tuonnempana muun muassa luvuissa \ref{l7l8-erot-kesk} ja  \ref{deiktiset-adverbit-ja-positionaalisuus}
havaitaan, venäjän S1-sijaintiin voi myös perustellusti nähdä liittyvän oman *fokaalisen konstruktionsa*,
ja alkusijainnin fokaalinen käyttö onkin johdantokonstruktion lisäksi toinen
merkittävimmistä suomen ja venäjän S1-sijaintiin liittyviä käyttöeroja
selittävistä tekijöistä.



\todo[inline]{
moka:

Не так давно он украл в магазине недорогой мобильник, его поймали, но судья сжалилась над многодетным отцом и приговорила воришку не к тюремному сроку, а к исправительным работам―140 часов. (RuPress: Комсомольская правда)

}

Ajanilmaukset ja keskisijainti
=============================



Kuten luku \ref{ajanilmaukset-ja-alkusijainti}, myös nyt käsillä oleva luku
rakentuu siten, että tarkastelen ensin (osio \ref{s2-samans}) suomessa ja
venäjässä tilastollisen mallin perusteella havaittavia selvimpiä yhtäläisyyksiä
-- niitä muuttujien arvoja, joiden vaikutus on samansuuntainen kielestä
riippumatta. Tämän jälkeen (osio \ref{s2-eri}) siirrytään niihin tekijöihin,
jotka suomessa ja venäjässä vaikuttavat erisuuntaisesti tai samansuuntaisesti
mutta eri voimakkuudella.

Esitin luvussa \ref{tutkimuksen-malli}, että tilastollisen mallin kannalta on
hedelmällistä yhdistää S2- ja S3-sijainnit yhdeksi ainoaksi sijaintimuuttujan
arvoksi, *keskisijainniksi*. S2-ja S3-sijaintien yhdistäminen on ennen kaikkea
tilastollista mallia varten tehty olettamus -- todellisuudessa suomen
S2-asemalla voi hyvin olla tehtäviä, joita S3-asemalla ei ole; samaten venäjän
S3-asemalla voi olla tehtäviä, jotka venäjän S2-asemalta puuttuvat. Näitä
keskisijainnin sisäisiä eroja tarkastellaan erikseen alaluvussa \ref{kesk-sis}.

## Samansuuntaisia vaikutuksia {#s2-samans}


Luvussa \ref{sij-yleisk} esitettyjen tilastojen perusteella nähdään, että
keskisijaintiin sijoittuu kaikkiaan 86 737 tutkimusaineiston
suomenkielisestä lauseesta  46120
eli 53,17 prosenttia  ja
kaikkiaan 73 302 tutkimusaineiston venäjänkielisestä lauseesta
 39 601  eli
54,02 prosenttia.
Tilanne on siis merkittävällä tavalla erilainen kuin tarkasteltaessa alkusijaintia kuten 
luvussa \ref{ajanilmaukset-ja-alkusijainti} tai loppusijaintia kuten luvussa
\ref{ajanilmaukset-ja-loppusijainti}: nyt tutkittavan sijainnin suhteellinen
osuus on kummassakin kielessä hyvin pitkälti samansuuruinen.

Edellisessä luvussa tilastollisen mallin perusteella ei ollut löydettävissä
kovin paljon sellaisia muuttujien arvoja, joilla olisi selvä positiivinen tai negatiivinen
vaikutus tarkasteltavan sijainnin todennäköisyyteen  tutkittavasta kielestä
riippumatta. Analysoitaessa keskisijaintia tilanne muuttuu jonkin verran, niin
että sekä semanttisen funktion että  morfologisen rakenteen kohdalla on
nähtävissä arvoja, jotka a) kasvattavat tai pienentävät keskisijainnin
todennäköisyyttä koko aineiston tasolla ja b) ovat kielen ja sijainnin välisen
interaktion suhteen enemmän tai vähemmän neutraaleja. Näiden vaikutusten luonne
käy tarkimmin ilmi kuvioista 25 ja 26, jotka 
kuvaavat semanttisen funktion ja morfologisen rakenteen merkitystä koko aineiston tasolla sekä
kuvioista 29  ja 30, jotka 
puolestaan kuvaavat kielen ja semanttisen funktion sekä kielen ja morfologisen 
rakenteen välisiä interaktioita.

Kuviosta 25 voidaan erottaa yksi selvästi muita
positiivisempi vaikutus sekä joukko vähemmän voimakkaita mutta yhtä kaikki
selkeitä tendenssejä:

![\noindent \headingfont \small \textbf{Kuvio 25}: Semanttisten funktioiden vaikutus keskisijaintiin \normalfont](figure/functs2s3-1.pdf)

Positiivisimmin koko aineiston tasolla keskisijainnin todennäköisyyteen
vaikuttaa presuppositionaalinen funktio, jonka vaikutus on myös kuvion 29 
perusteella koko lailla kielestä riippumaton (mahdollisesti hivenen voimakkaampi suomessa).
Negatiivisin vaikutus on likimääräisellä funktiolla, ja kuvion 29 
mukaan tämä vaikutus on täysin kielestä riippumaton. Toisen negatiivisen vaikutuksensa
perusteella erottuvan funktion muodostavat sekventiaaliset ilmaukset, joissa kuvion
 29 perusteella voidaan kuitenkin kiistatta todeta 
myös kieleen liittyvää interaktiota.
 

Myös morfologisen rakenteen kohdalla (kuvio 26) voidaan nähdä
selviä koko aineiston tason vaikutuksia, jotka kuvion 30 
perusteella ovat kielestä riippumattomia. Kuviot osoittavat, että nominaalisten
ajanilmausten todennäköisyys sijoittua keskisijaintiin on koko aineiston
tasolla selvästi pienempi kuin kummankaan adverbiryhmän. 


![\noindent \headingfont \small \textbf{Kuvio 26}: Morfologisen rakenteen vaikutus keskisijaintiin \normalfont](figure/morphs2s3-1.pdf)

Kuviosta 27 nähdään, että positionaalisten ilmausten
todennäköisyys sijoittua keskisijaintiin on suurempi
kuin ei-positionaalisten. Kuvio 17 
vahvistaa, ettei kielen ja positionaalisuuden välillä
juurikaan ole interaktiota.


![\noindent \headingfont \small \textbf{Kuvio 27}: Semanttisten funktioiden vaikutus keskisijaintiin \normalfont](figure/poss2s3-1.pdf)

Esitän viimeisenä tilastollisen mallin muuttujana numeraalin läsnäolon vaikutuksen. Kuviot 28 
ja 32 osoittavat, ettei muuttujalla keskisijainninkaan suhteen ole suurta merkitystä -- etenkin koko
aineiston tasolla vaikutus on kiistatta olematon:


![\noindent \headingfont \small \textbf{Kuvio 28}: Numeraalin läsnäolon vaikutus keskisijaintiin \normalfont](figure/isnumerics2s3-1.pdf)

Kuten alkusijainnin yhteydessä, myös keskisijaintia tarkasteltaessa tutkin
seuraavassa lähemmin ensin kummallekin kielelle yhteisiä vaikutuksia, minkä
jälkeen siirryn kielten välillä havaittaviin eroihin. Päähuomio yhtäläisyyksien
tarkastelussa keskittyy morfologisen rakenteen kohdalla havaittuun adverbisiä
ajanilmauksia voimakkaasti puoltavaan tendenssiin.

### Morfologinen rakenne {#morf-sama-keski}

Edellä Luvussa \ref{konstr-ven} todettiin tutkimuskirjallisuuden perusteella,
että S2 on venäjässä ennen kaikkea adverbien sijainti [vrt. @kovtunova1976, 166].
Tältä  pohjalta on helppo ymmärtää, että myös tilastollisen mallin mukaan 
adverbisten ilmausten todennäköisyys sijoittua keskisijaintiin on keskimääräistä suurempi.
Suomessa adverbisyyden vaikutus S3-aseman yleisyyteen ei kuitenkaan tutkimuskirjallisuuden 
perusteella ole samalla tavalla yksiselitteistä. Esimerkiksi Vilkuna [-@vilkuna1989, 25, 40] kyllä
mainitsee S3-asema olevan *tavallisin* adverbisijainti, mutta kyse vaikuttaisi venäjää selkeämmin
olevan ennemmin tendenssistä kuin kieliopillisesta määräytymisestä. Itse asiassa Sulkala
[-@sulkala1992, 72] jopa toteaa  eksplisiittisesti, ettei adverbiaalin sijainti
suomessa riipu sen morfologisesta rakenteesta:

\begin{quote}%
The position of an adverbial does not depend on its structure, i.e.
whether it is an adverb, a prepositional or postpositional phrase or a
noun phrase.
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Se, ettei suomen S3 ehkä ole samalla tavalla adverbipainotteinen sijainti kuin 
venäjän S2, ilmenee alempana kuviossa 30, jonka mukaan
kielen ja morfologisen rakenteen välillä vaikuttaisi olevan jonkin verran  interaktiota:
suomessa nimenomaan muilla kuin deiktisillä adverbeillä on venäjään nähden mahdollisesti
jonkin verran pienempi keskisijainnin todennäköisyyttä kasvattava vaikutus.
Tämä näkyy myös, jos tutkitaan kaikkia (ei-deiktisiä) adverbisiä aineistoryhmiä:
venäjässä keskisijainnin osuus niissä on keskimäärin 76,77 %,
suomessa taas 68,59 %. Se, että lukema on suomessakin 
liki 70 prosenttia, kertoo kuitenkin siitä, että keskisijainti[^mainitsemolemmat]
todella on tyypillisin adverbisijainti ja tilastollinen malli syystäkin katsoo
adverbisyyden lisäävän sen todennäköisyyttä pitkälti kielestä riippumatta.

Esimerkiksi F3a-ryhmässä (ks. osio \ref{f3a-f3b-f3c-f3d-f3e}) keskisijainnin
osuus on suomessa ja venäjässä käytännössä identtinen (kummassakin noin 79%),
niin että tätä ryhmää edustavat lauseet muistuttavat suurimmaksi osaksi
seuraavia tapauksia:




\ex.\label{ee_kekkonen} \exfont \small Presidentti Urho Kekkonen valitsi \emph{usein} itse lentomatkojensa
ruokalistan. (FiPress: Turun Sanomat)






\ex.\label{ee_druzja} \exfont \small Друзья Валеры \emph{часто} навещают семью погибшего друга. (RuPress:
Комсомольская правда)





\vspace{0.4cm} 

\noindent

Suomesta ja venäjästä voidaankin tämän tutkimuksen puitteissa erotella *adverbiset konstruktiot*,
joilla kuvaan niitä tapauksia, joissa ajanilmaus sijoittuu ikään kuin
oletuksena S2:een tai S3:een, ennen kaikkea
siksi, ettei mikään viestinnällinen tavoite ohjaa käyttämään muita sijainteja (vrt.
Kovtunovan [-@kovtunova1976, 166] edellä mainittu huomautus S2-aseman
oletusasemasta venäjässä). Venäjässä konstruktio voidaan kaikessa yksinkertaisuudessaan
kuvata matriisin 17 tavoin, suomessa erona on ajanilmauksen sijainti
verbin jäljessä.



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & muu \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{3.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 17}: Adverbinen S2-konstruktio. \normalfont \vspace{0.6cm}


Vaikka kummassakin tutkitussa kielessä keskisijaintiin liittyvä adverbinen konstruktio on 
tavallinen, voidaan tutkimusaineiston adverbisten ryhmien joukossa havaita
eräitä, joiden osalta suomi ja venäjä poikkeavat
toisistaan paikoin suurestikin. Selkein tällainen tapaus on aineistoryhmä E6a (\ref{e6a-e6b-e6c}),
jonka edustajista  keskisijaintiin osuu venäjässä peräti 90,33 %, mutta
suomessa ainoastaan 27,92 prosenttia. Esimerkin \ref{ee_paikallis} tapaiset
lauseet eivät siis suomessa ole mitenkään erityisen yleisiä, kun taas esimerkin \ref{ee_mike}
kaltaiset tapaukset kattavat suurimmat osan venäjän E6a-ryhmästä.
E6a-ryhmien välillä havaittaviin eroihin pureudutaan tarkemmin luvussa \ref{ajanilmaukset-ja-loppusijainti}.




\ex.\label{ee_paikallis} \exfont \small Paikallisväestö hyödynsi kauan Vattaja-aluetta yhteisenä laidunalueena.
(Araneum Finnicum: julkaisut.metsa.fi)






\ex.\label{ee_mike} \exfont \small Майк долго рассказывал свою историю. (Araneum Russicum: klikin.ru)





\vspace{0.4cm} 

\noindent
E6a-ryhmän lisäksi venäjän keskisijaintiosuus on selkeästi suomea suurempi ainestoryhmissä
F2b (ks. osio \ref{f2a-f2b}), F3b(\ref{f3a-f3b-f3c-f3d-f3e}) ja E7 (\ref{e7}, lisäksi
tarkemmin tämän luvun osiossa \ref{s2s3-pien}).
Kiinnostavia poikkeuksia ovat kuitenkin suomen adverbit *silloin*, *joskus*, ja *tavallisesti*
eli aineistoryhmät L7c, F3d ja F4. Näissä ryhmissä  keskisijainti 
on selvästi tavallisempi vaihtoehto suomessa kuin venäjässä. Esimerkin \ref{ee_ostoskeskus}
tapaan suomen L7c-ryhmästä keskisijaintiin sijoittuu 60,07 %,
siinä missä esimerkin \ref{ee_gryzlov} kaltaisesti venäjän keskisijaintiin osuu ainoastaan
32,28 % tapauksista:




\ex.\label{ee_ostoskeskus} \exfont \small Ostoskeskuksen johto teki silloin poikkeuksen lupakäytäntöön ja antoivat
tukensa harjoittajille. (Araneum Finnicum: fi.clearharmony.net)






\ex.\label{ee_gryzlov} \exfont \small Грызлов тогда отметил важность продолжения конструктивного диалога с
приднестровскими партнерами, напомнив о том, что между партией «Единая
(RuPress: Новый регион 2)





\vspace{0.4cm} 

\noindent
\todo[inline]{Mannista loppusijaintilukuun?}

Edellä havaittu keskisijainnin osuuksien heittely osoittaa hyvin sen, kuinka
adverbeistä puhuttaessa on pidettävä mielessä se, että kyse on yksittäisistä
lekseemeistä, joiden sijoittumiseen pääsevät voimakkaammin vaikuttamaan myös
sanakohtaiset ominaisuudet. Tarkastelen seuraavassa esimerkkeinä
leksikaalisten ominaisuuksien vaikutuksesta suomen *nyt*- ja venäjän
*однажды*-adverbejä, joiden leksikaaliset erityispiirteet
ovat saaneet huomiota olemassaolevassa tutkimuskirjallisuudessa.[^kerranselitys]

[^mainitsemolemmat]: Huomaa, että luvussa ovat mukana sekä S2- että S3-sijainnit.

[^kerranselitys]: Alun perin suomen kerran- ja venäjän однажды-sanat oli tarkoitus
saada osaksi varsinaista tutkimusaineistoa. Juuri edellä mainitusta merkityksen
ambivalenttiudesta johtuen ilmausten hakeminen aineistosta ja määritteleminen
osoittautui kuitenkin niin hankalaksi, että näiden ilmausten käsittelemisestä osana
varsinaista aineistoa päätettiin luopua.

Niin kuin Hakulinen [-@hakul95, 482] toteaa, suomen nyt-sanaa voidaan
käyttää joko ajallisesti tai ei-temporaalisessa merkityksessä,
diskurssipartikkelina. Sen määritteleminen, kummasta käytöstä on kyse, tapahtuu
osittain sanajärjestyksen avulla, niin että juuri S2-sijoittunut *nyt* on usein
todennäköisesti ei-temporaalinen, kuten esimerkissä \ref{ee_atempnyt}:




\ex.\label{ee_atempnyt} \exfont \small Jos joku \emph{nyt} vetää herneen siitä nenään, että blogin pitäjä
sattuu pitämään jostakin/ jonkun mielipiteestä, niin ehkä kannattaisi
sitten seurata jotain blogia, jonka kirjoittajalla ei ole mielipiteitä.
(Araneum Finnicum: soininvaara.fi)





\vspace{0.4cm} 

\noindent
Myös venäjän однажды-adverbin osalta sijainti määrittelee sanan merkitystä, joskin
tässä tapauksessa kumpaakin merkitystä voi pitää temporaalisena.
F.I. Pankovin [-@pankov1997, 163-164] mukaan lauseenalkuisella однажды-sanalla on eri merkitys kuin
lauseenloppuisella, niin että lopussa merkitys on fokaalinen ja  korostaa tapahtuman
kertaluontoisuutta, mutta alussa merkitys liittyy ajalliseen
lokalisointiin. Pankov itse antaa seuraavat esimerkit (hänellä 1a ja 1b):





\exg.  Однажды я был в Киеве
\label{ee_pankov1}  
  kerran minä olla-PRET PREP Kiova
\






\exg.  В Киеве я был лишь однажды
\label{ee_pankov2}  
  PREP Kiova minä olla-PRET vain kerran
\




\vspace{0.4cm} 

\noindent
Mielenkiintoista kyllä, suomen kerran-sana käyttäytyy samalla tavoin useimmiten,
muttei aina. Aivan kuten venäjän *однажды*, myös suomen *kerran* saa lauseen alkuun
sijoittuessaan yleensä lokalisoivan funktion;
loppuun sijoittuessaan se saa taajuutta ilmaisevan funktion ja on
fokaalinen. Tästä yleisestä taipumuksesta on kuitenkin myös poikkeuksia, kuten
seuraava Araneum-korpuksesta erikseen haettu esimerkki osoittaa:




\ex.\label{ee_pappi} \exfont \small "Lapset pitävät nöyrinä", totesi eräs pappi \emph{kerran.} (Araneum
Finnicum / liisijokirjanta.fi)





\vspace{0.4cm} 

\noindent
Suomen *kerran*- ja venäjän *однажды*-sanojen käytössä oletettavasti ilmenevä ero on
mielenkiintoinen seuraavan luvun kannalta, kun  pohditaan suomen ja venäjän loppusijaintien välisiä
eroja. Esimerkin \ref{ee_pappi} ilmentämään suomen loppusijainnin lokalisoivaan funktioon palataan tarkemmin
osiossa \ref{deiktiset-adverbit-ja-positionaalisuus}.


\todo[inline]{
    Totea jossain kohtaa selvemmin, että keskisijainti on looginen laskeutumispaikka
    monille adverbeille, koska lausen alku on topikaalisia ja loppu fokaalisia elementtejä
    varten, jolloin niissä tapauksissa kun ei ole kyse kummastakaan, jää jäljelle
    keskisijainti. TOSIN suomen S3 rikkoo tätä kaavaa siinä mielessä, että se ottaa
    mielellään vastaan myös topikaalisia elementtejä...
}

### Muut samansuuntaiset vaikutukset {#presup-sama-keski}

Edellisessä alaluvussa mainittiin erityisesti venäjän ominaispiirteeksi
se, että tietyt adverbiset aineistoryhmät sijoittuvat lähes yksinomaan
keskisijaintiin. Samassa yhteydessä huomautettiin myös, että adverbien
sijaintia tutkittaessa on pidettävä mielessä leksikaalisten ominaisuuksien suuri
merkitys. Tämä näkyy myös kuviossa 25, jossa presuppositionaaliset
adverbit eli aineistoryhmät P1 ja P2 (osio \ref{p1-p2-p3}) eroavat selvästi
kaikista muista semanttisista funktioista: 
niillä on erittäin selvä keskisijainnin todennäköisyyttä kasvattava 
vaikutus, jonka kuvio 29 alempana vahvistaa kielestä riippumattomaksi.
Molemmilla presuppositionaalisilla aineistoryhmillä keskisijainnin osuus kaikista 
sijainneista on niin suomessa kuin venäjässä yli 80% -- venäjän P1-ryhmällä osuus 
on peräti 98,34 %. Toisin sanoen, esimerkkien \ref{ee_kooma}
ja etenkin \ref{ee_arendy} kaltaiset tapaukset ovat miltei ainoita mahdollisia
presuppositionaalisten ilmausten kohdalla:




\ex.\label{ee_kooma} \exfont \small Uran vajottua koomaan John harkitsi jo lentäjän ammattia (FiPress:
Iltalehti)






\ex.\label{ee_arendy} \exfont \small Мы уже заключили договоры аренды на помещения в двух торговых центрах
Санкт-Петербурга — «Атлантик-Сити» (RuPress: РБК Daily)





\vspace{0.4cm} 

\noindent
Suomen *jo*- ja *vielä* sekä venäjän *уже*- ja *еще*-sanojen sijoittumiseen liittyy tunnettuja 
kieliopillisia rajoitteita, niin että sijaintien painottuminen on ymmärrettävää.\todo[inline]{PARANNA}
[@tobecited]

Toinen semanttisiin funktioihin liittyvä selvä samansuuntainen vaikutus ei sen
sijaan koske adverbejä, vaan edellisessä luvussa (ks. osio \ref{likim-s1})
laajasti käsiteltyjä likimääräisiä ilmauksia. Näiden tapausten kohdalla
keskisijainti on niin suomen- kuin venäjänkielisessäkin aineistossa
keskimääräistä harvinaisempi: suomessa LM1-aineiston S2/S3-osuus on 
13,43 % 
ja venäjässä 26,58 %. Myös F-funktiota
edustavat ajanilmaukset käyttäytyvät pitkälti samalla tavoin keskisijainnin
osalta. Tämän taustalla on todennäköisesti se, että monet F-funktiota edustavista
aineistoryhmistä ovat morfologiselta rakenteeltaan adverbisiä.

Morfologisen rakenteen ja semanttisten funktioiden lisäksi on vielä todettava, 
että kielestä riippumatta positionaaliset ilmaukset suosivat keskisijaintia. \todo[inline]{Venäjässä
tämä johtuu enemmän utro:sta suomessa maanantai ym.??} Etenkin suomen osalta
tämä on ymmärrettävää siitä syystä, että alkusijainnin käyttömahdollisuudet ovat positionaalisten 
ilmausten kohdalla monia muita ilmauksia rajatummat.
Tietyistä samankaltaisuuksista huolimatta myös keskisijainnin kohdalla
selkein tilastollisen mallin perusteella tehtävä päätelmä kuitenkin on, 
että suomen ja venäjän välillä on olennaisia eroja siinä, minkälaisissa
konstruktioissa ja strategioissa ajanilmaus sijoitetaan S2- tai S3-asemiin.
Näiden erojen käsittelyyn käytetään tästäkin luvusta suurin osa.


## Eriäviä vaikutuksia {#s2-eri}

Tilastollisen mallin perusteella havaittavat kielen ja eri selittävien muuttujien
interaktiot on havainnollistettu edellä jo useampaan kertaan mainittujen 
kuvioiden 29 -- 31 
avulla. Aloitan seuraavassa keskisijaintia koskevien eriävien vaikutusten
tutkimisen luomalla ensin tarkemman yleiskatsauksen siihen,
mitkä alla olevien kuvioiden perusteella ovat kaikkein selvimmät
suomea ja venäjää erottavat tekijät. Lähden liikkeelle kielen
ja semanttisen funktion välisistä interaktioista, jotka 
on esitetty kuviossa 29.
Huomaa, että kuten tähänkin asti, kuvio esittää, missä määrin vaikutukset
eroavat *suomessa verrattuna venäjään*.

![\noindent \headingfont \small \textbf{Kuvio 29}: Semanttisten funktioiden ja kielen interaktio keskisijainnissa \normalfont](figure/langfuncts2s3-1.pdf)

Jos kuviota 29 vertaa edellisessä luvussa esitettyyn
kuvioon 15, nähdään, että siinä missä alkusijainnin osalta
löydettiin enemmän tekijöitä, jotka *pienensivät* tarkasteltavan sijainnin todennäköisyyttä
suhteessa venäjään, on tilanne keskisijainnin osalta päinvastainen. Tältä pohjalta voisi ajatella,
että suomen keskisijaintia hyödyntävät konstruktiot on mahdollisesti monissa
tapauksissa analysoitavissa venäjän alkusijaintia hyödyntävien konstruktioiden vastineiksi. Tätä argumenttia
pohditaan tarkemmin luvussa \ref{kokoava-kontrastiivinen-analyysi}.

Toinen ero kuvion 29  ja 15 
välillä on siinä, että kielen ja semanttisen funktion välistä interaktiota on ylipäätään
havaittavissa enemmän siinä mielessä, että alkusijainnin tapauksessa suurempi
osa funktioista kuului harmaalla merkittyyn vyöhykkeeseen, eli tapauksiin,
joissa interaktio on lähellä nollaa. Keskisijainnin kohdalla tällaisia funktioita
ovat F-funktiot ja molemmat resultatiivisista funktioista.

Jos tutkitaan lähemmin niitä semanttisia funktioita, joiden kohdalla suomen ja
venäjän keskisijainnit vaikuttaisivat eroavan toisistaan, voidaan tehdä
seuraavat havainnot:

1. Keskisijainnin todennäköisyyttä suomessa suhteessa venäjään pienentävät
   erityisesti duratiivinen ja  tapaa ilmaiseva funktio. Duratiivisen funktion
   kohdalla vaikutus on huomattavan suuri, mutta myös tavan ilmauksilla
   selkeästi nollasta erottuva. Kuten edellä havaittiin, presuppositionaaliset
   funktiot eivät suomessa ole aivan yhtä tiukasti sidottuja keskisijaintiin
   kuin venäjässä.
2. Simultaaninen ja sekventiaalis--duratiivinen funktio, jotka edellä kuvion 
   15 mukaan
   pienensivät suomen alkusijainnin todennäköisyyttä, kasvattavat
   keskisijainnin todennäköisyyttä. 
3. Edellä mainittujen lisäksi suomen keskisijainnin todennäköisyyttä
   kasvattavat myös sekventiaalinen funktio\todo[inline]{silloin-adverbi}, sekä jossain
   määrin teelinen funktio -- hivenen myös varsinainen resultatiivinen funktio.
   Sekventiaalisten ilmausten osalta kasvattamisen voi nähdä johtuvan edellä
   alaluvussa \ref{morf-sama-keski} käsitellystä *silloin*-sanasta
   (aineistoryhmä L7c), joka suomessa sijoittuu keskisijaintiin venäjän vastaavaa
   sanaa useammin.


Edellä käsiteltiin jonkin verran morfologisen rakenteen kielestä riippumatonta
vaikutusta keskisijainnin todennäköisyyteen. Kielellä ja morfologisella
rakenteella on keskisijainnin tapauksessa vain vähän interaktiota, kuten
kuvio 30 osoittaa:


![\noindent \headingfont \small \textbf{Kuvio 30}: Morfologisen rakenteen vaikutus keskisijaintiin suomessa verrattuna venäjään \normalfont](figure/langmorphs2s3-1.pdf)

Ainoa kuviosta ilmi tuleva interaktiohavainto on, että suomessa ei--deiktisten
adverbien S2/S3-todennäköisyys on mahdollisesti jonkin verran venäjää pienempi ja deiktisten
(sekä nominaalisten ilmausten) taas vastaavasti hivenen suurempi. 

Kuten jo todettua, positionaalisuuden ja keskisijainnin välillä interaktiota
ei havaita, mutta numeraalin läsnäolo näyttäisi alkusijainnin tapaan 
hieman kasvattavan keskisijainnin todennäköisyyttä suomessa verrattuna venäjään.
Nämä vaikutukset käyvät ilmi kuvioista 31  ja 
32:

![\noindent \headingfont \small \textbf{Kuvio 31}: Positionaalisuuden vaikutus keskisijaintiin suomessa verrattuna venäjään \normalfont](figure/pos.lang.s2s3-1.pdf)

![\noindent \headingfont \small \textbf{Kuvio 32}: Numeraalin läsnäolon vaikutus keskisijaintiin suomessa verrattuna venäjään \normalfont](figure/langisnumerics2s3-1.pdf)

Siirryn nyt tutkimaan edellä yleistasolla kuvattuja eroja yksityiskohtaisemmin.

### Tekijät, jotka pienentävät keskisijainnin todennäköisyyttä suhteessa venäjään {#s2s3-pien}

Koska keskisijainnin todennäköisyyttä suomessa kasvattavat tekijät vaikuttavat
tähdellisemmiltä suomen ja venäjän välisiä eroja selitettäessä, tyydyn käsittelemään
keskisijainnin todennäköisyyttä pienentäviä tekijöitä melko lyhyesti. 
Kuvion 29 mukaan selvimmät
keskisijainnin todennäköisyyttä pienentävät vaikutukset ovat toisaalta duratiivisilla,
toisaalta tapaa ilmaisevilla ajanilmauksilla (aineistoryhmä E7, ks. osio \ref{e7}).
Keskityn tässä tapaa ilmaisevaan semanttiseen funktioon ja käsittelen
duratiivista funktiota tarkemmin loppusijainnin yhteydessä luvussa
\ref{duratiivinen-funktio}. Taulukko 7 esittää E7-ryhmän
tapausten tarkemman jakautumisen eri sijainteihin suomessa ja venäjässä:


|       |S1               |S2/S3              |S4                 |
|:------|:----------------|:------------------|:------------------|
|venäjä |5,65 % (159 kpl) |87,86 % (2474 kpl) |6,50 % (183 kpl)   |
|suomi  |2,57 % (88 kpl)  |51,55 % (1765 kpl) |45,88 % (1571 kpl) |

\noindent \headingfont \small \textbf{Taulukko 7}: Suomen ja venäjän E7-ryhmän jakautuminen eri sijainteihin \normalfont \vspace{0.6cm}

Kuten taulukko 7  osoittaa, venäjässä keskisijainti on huomattavasti
suomea hallitsevampi. Yhtenä todennäköisenä selityksenä tähän voidaan pitää 
esimerkiksi Satu Mannisen [-@manninen2003, 226] mainitsemaa tavan adverbeihin
suomessa liittyvää taipumusta sijoittua ennemmin lauseen loppuun kuin keskelle
siksi, että S3-sijoittunut adverbi johtaisi kontrastiiviseen fokustulkintaan.
Tämä käy hyvin ilmi Mannisen esimerkeistä \ref{ee_mann1} ja \ref{ee_mann2} (hänellä 40a
ja 40c):




\ex.\label{ee_mann1} \exfont \small Sirkku kohteli Pulmua hyvin






\ex.\label{ee_mann2} \exfont \small Sirkku kohteli hyvin Pulmua (eikä Kerttua)





\vspace{0.4cm} 

\noindent
On ymmärrettävää, että esimerkin \ref{ee_mann2} kaltaiset tapaukset ovat harvinaisempia, sillä
kontrastiivisen diskurssifunktion ilmaisemisen voi nähdä harvinaisempana viestintätavoitteena
kuin tavallisen, ei-kontrastiivisen fokuksen.

Tutkimusaineistoon valitut tapaa ilmaisevat suomen *nopeasti* ja
venäjän *быстро* eivät kuitenkaan käyttäydy täysin samalla tavoin kuin
esimerkkien \ref{ee_mann1} -- \ref{ee_mann2} *hyvin*. Esimerkiksi seuraavassa
tutkimusaineiston virkkeessä *nopeasti* sijoittuu S3:een, mutta kontrastiivinen
tulkinta on silti epätodennäköinen:




\ex.\label{ee_katon} \exfont \small Ihmiset purkivat nopeasti talomme katon. (Araneum Finnicum: opsti.net)





\vspace{0.4cm} 

\noindent
S4- ja S3-sijainnit vaikuttavat esimerkin \ref{ee_katon} kaltaisissa tilanteissa itse
asiassa käytännössä identtisiltä, kuten toinen aineistoesimerkki \ref{ee_vaatimukset}
osoittaa:




\ex.\label{ee_vaatimukset} \exfont \small Nuori oppii ammatin vaatimukset nopeasti, ja sen jälkeen hän on
asenteellisesti valmis joukkuetyöskentelyyn uudistuneessa
yrityskulttuurissa. (FiPress: Karjalainen\_1994-05-27)





\vspace{0.4cm} 

\noindent
Vaikka *nopeasti*-sana esiintyy suomen S3-aineistossa myös ei--kontrastiivisesti,
voidaan hyvin olettaa, että kontrastin välttäminen on tärkein yksittäinen selitys
sille, ettei S3 ole suomessa yhtä hallitseva sijainti E7-ryhmän ajanilmauksille
kuin venäjässä.

### Tekijät, jotka kasvattavat suomen keskisijainnin todennäköisyyttä suhteessa venäjään

Kuvion 29 perusteella havaittiin, että 
voimakkaimmin keskisijainnin todennäköisyyttä suomessa verrattuna venäjään
kasvattaa simultaaninen semanttinen funktio. Koska simultaaniset funktiot
kattavat suuren määrän melko erilaisiakin ajanilmaustapauksia, tutkin
seuraavassa simultaanisen funktion ja keskisijainnin suhdetta useamman 
eri alaluvun (\ref{l1-erot-kesk} -- \ref{l7l8-erot-kesk}) kautta.

Edellä osiossa \ref{simultaaninen-funktio} esitettiin kaikkien simultaanisten aineistoryhmien 
S1-osuuksia vertaileva kuvio 23, jonka perusteella nähtiin, että
yhtä lukuun ottamassa kaikissa simultaanista funktiota edustavissa ryhmissä S1 oli tavallisempi venäjässä
kuin suomessa.  Kun samanlaisen vertailun tekee keskisijainnin osalta, ovat
erot päinvastaisia, niin että miltei joka aineistoryhmässä keskisijainti on tavallisempi suomessa
kuin venäjässä. Tämä vertailu on esitetty kuviossa 33. Aiempien
vastaavien kuvioiden tapaan myös tässä koko suomenkielisen aineiston keskiarvo on esitetty
tummalla ja venäjänkielisen aineiston keskiarvo vaalealla katkoviivalla. Keskisijainnin tapauksessa
nämä ovat, kuten edellä havaittiin, lähes päällekkäisiä.

![\noindent \headingfont \small \textbf{Kuvio 33}: Erot simultaanisten aineistoryhmien S2/S3-osuuksissa \normalfont](figure/simeroja_s2s3-1.pdf)

\todo[inline]{Viittaus vastaavaan kuvioon loppusijaintiluvussa}

Kuviot 23  ja 33  eroavat toisistaan siinä, että
edellisessä kieltenväliset erot ovat selvästi jälkimmäistä jyrkempiä.
Alkusijainnin osalta selvimmät suomen ja venäjän väliset erot koskevat aineistoryhmiä
L5c,  L2a,  L3, L9a ja L2b. Kuvion 33 mukaan
L9a-, L2a- ja L3-ryhmien osalta ei sen sijaan juuri havaita eroja keskisijainnin suhteen.
Mainittujen kolmen ryhmän lisäksi suomen keskisijainti ei ole venäjän keskisijaintia tavallisempi
ryhmissä L7b ja L7d (\ref{l7a-l7b-l7c-l7d}). 

Mahdollisten suomen ja venäjän välisten systemaattisten erojen kannalta olennaisimpia
kuviosta 33 tehtäviä havaintoja ovat seuraavat:

1. L1-aineistoryhmät eli deiktiset adverbit *eilen*,*tänään* ja *huomenna* (ks. osio \ref{l1a-l1b-l1c})
   ovat kaikki selvästi tavallisempia S2/S3-sijoittujia suomessa.
2. Nominaaliset aineistoryhmät L4b ja L5a (osiot \ref{l4a-l4b} ja \ref{l5a-l5b}) ovat jyrkimpien eroajien joukossa.
3. Erot ovat suuria myös yksittäisistä adverbeistä koostuvissa L7c- ja L8b-ryhmissä (osio \ref{l8a-l8b-l8c}).

Keskitän simultaanisen funktion tarkasteluni seuraavassa näihin kolmeen aineistoryhmien joukkoon.

#### Erot L1-aineistoryhmissä {#l1-erot-kesk}


Kuvion 33 mukaan kaikissa L1-aineistoryhmissä suomen ja venäjän
välinen ero on huomattava. L1a-ryhmä erottuu kuitenkin vielä selvästi L1b- ja L1c-ryhmistä.
Suomenkielisessä tutkimusaineistossa on 3 818 L1a-aineistoryhmän
tapausta, joista 2448 (64,12 %)
sijoittuu keskisijaintiin; venäjänkielisen aineiston 4 898 tapauksesta
puolestaan keskisijaintiin osuu 1 432
eli  29,24 %.
Kuviosta 33 nähdään, että suomen L1a-ryhmän alkusijaintiosuus
on jonkin verran yli koko suomenkielisen aineiston keskiarvon -- venäjässä vastaavasti melko selvästi alle.
Tutkin seuraavassa mahdollisia selityksiä L1a-ryhmässä havaittavalle erolle ja testaan tämän jälkeen,
ovatko löydetyt vastaukset sovellettavissa myös muihin L1-ryhmiin.

Lähtökohtaisesti voidaan todeta, että yllä esitettyjen lukujen perusteella
keskisijainti on niin suomen kuin venäjänkin L1a-ryhmällä tavallinen ja paljon käytetty.
Esimerkiksi lauseet \ref{ee_hakkiset} ja \ref{ee_sergei} muistuttavat ainakin ensi
katsomalta toisiaan monella tapaa:




\ex.\label{ee_hakkiset} \exfont \small Mika ja Erja Häkkinen saivat eilen toisensa jo toistamiseen. (FiPress:
Turun Sanomat)






\ex.\label{ee_sergei} \exfont \small Президент Владимир Путин вчера назначил руководство «Ростехнологий»
(RuPress: РБК Daily)





\vspace{0.4cm} 

\noindent
Kummassakin lauseessa ajanilmauksella on hyvin tavallisen tuntuinen
lokalisoiva funktio. Lisäksi molempien lauseiden subjektit ovat
diskurssistatukseltaan aktivoimattomia (mutta tunnistettavia), ja molemmissa lauseissa kerrotaan jokin
yksinkertainen propositio siitä, mitä edellisenä päivänä on tapahtunut.
Molemmat lauseet ovat myös tyyliltään melko muodollisia ja edustavatkin
kummankin kielen lehdistökorpuksia. Tarkemmin analysoituna  esimerkeistä
voidaan myös todeta, että ne sijaitsevat todennäköisesti hyvin lähellä 
tekstin alkua, mahdollisesti koko tekstin alussa.[^sergeivahv]
Kyseessä on siis käytännössä osiossa \ref{s1-sim-pos} esiteltyä 
globaalia johdantokonstruktiota vastaava mutta keskisijaintia hyödyntävä konstruktio.
Suomen osalta konstruktion voisi kuvata matriisilla 18,
joka eroaa matriisista 13 tietysti ajanilmauksen 
sijainnin osalta, mutta myös sen suhteen, että ajanilmaukselle on vaikeampi määritellä
minkäänlaista topikaalista diskurssifunktiota. Vaikka matriisi 18 
kuvaa konstruktion sellaisena kuin se tutkimuksen suomenkielisessä aineistossa
esiintyy, voitaisiin venäjänkielisen aineiston perusteella esittää muuten samanlainen
kuvio, jossa ajanilmauksen sijainti vain olisi verbin edessä.

[^sergeivahv]: Esimerkin \ref{ee_sergei} osalta asia voitiin myös vahvistaa, ks. osoite x.




\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{

D-topic   - \\


\minibox[frame,pad=4pt,rule=0.1pt]{

df   foc \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & ident+/anch+ \\
\] \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & muu \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{3.5em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 18}: (Globaali) S3-Johdantokonstruktio suomessa. \normalfont \vspace{0.6cm}


Kun molempien kielten L1a-aineistoja tutkii tarkemmin, vaikuttaisi siltä, että
juuri johdantokonstruktion käyttö on yksi selvimmistä syistä sille, miksi suomen
L1a-tapauksissa keskisijainti on niin paljon suositumpi kuin venäjässä.
Esitän tämän olettamuksen uskottavuuden arvioimiseksi kolme  tunnuspiirrettä, joiden
voisi nähdä viittaavan siihen, että jokin aineiston lause edustaa juuri johdantokonstruktiota.


Ensinnäkin, kuten edellisessä luvussa määriteltiin, johdantokonstruktion avulla
kirjoittaja esittelee viestin vastaanottajalle yhden tai useamman uuden
topiikin, jotka tavallisesti ovat myöhemmän tekstin kannalta tärkeitä
diskurssitopiikkeja. Hyvin tavallista on, että kuten esimerkeissä \ref{ee_hakkiset},
tällaisia esiteltäviä topiikkeja ovat subjektit. Jos subjektit ovat
diskurssifunktioltaan johdantotopiikkeja, ovat ne myös diskurssistatukseltaan
aktivoimattomia, vaikkakin oletusarvoisesti tunnistettavia (ks. luku
\ref{diskurssistatus}). Toisaalta, vaikka myöhempää tekstiä varten esiteltävä
asia ei liittyisi subjektiin, antaa subjektin diskurssistatus joka tapauksessa
viitteitä siitä, missä kohden tekstiä lause sijaitsee: tekstin alussa
subjektit, kuten muutkin referentit ovat yleensä väistämättä aktivoimattomia.
Näin ollen subjektin aktivoimattomuutta voidaan pitää yhtenä
johdantokonstruktion indikaattorina.

Toiseksi, tietynlaisten verbien voi ajatella liittyvän uusien topiikkien
esittelemiseen \todo[inline]{ ..[vrt. @ambridge2008island, 358 -- "complements of factive
verbs are presupposed"]..}
Tällaisina verbeinä voidaan oman tulkintani mukaan pitää esimerkiksi:

- verbejä, joilla ilmoitetaan jokin uutinen: esimerkiksi suomen *julkistaa*,
  *esitellä*, *ilmoittaa* tai *venäjän* *сообщить*, *объявить*, *озвучить*.
  Tämän joukon verbit voisi nähdä kuuluvan esimerkiksi Framenet-projektin [@framenet_baker]
  *Telling*-kehykseen.[^frnet1]
- verbejä, jotka kuvaavat jonkin alkuun panemista tai voimaan asettamista: esimerkiksi
  suomen *lanseerata*, *vahvistaa* ja venäjän *запустить*, *потвердить*.
  Jossain määrin vastaavia verbejä kuvaa Framenet-projektin *cause to start* -kehys.[^frnet2] 

[^frnet1]: vrt. https://framenet2.icsi.berkeley.edu/fnReports/data/frameIndex.xml?frame=Telling

[^frnet2]: vrt. https://framenet2.icsi.berkeley.edu/fnReports/data/frameIndex.xml?frame=Cause\_to\_start,
           myös [-@levin1993english, 172]

Nimitän näitä verbejä tässä yhteydessä *esittelyverbeiksi*. Esimerkit \ref{ee_hesari}
ja \ref{ee_ervaz} edustavat tällaisen verbin sisältäviä L1a-aineiston lauseita:




\ex.\label{ee_hesari} \exfont \small Helsingin Sanomat julkisti \emph{eilen} kahden suurimman
rakennuttajayhtiön, YH - Rakennuttajien ja Sato-yhtiöiden hallitusten
kuntapaikat. (FiPress: Helsingin Sanomat)






\ex.\label{ee_ervaz} \exfont \small \emph{Вчера} Ervaz Group представила свои результаты по МСФО за первое
полугодие 2009 года. (RuPress: РБК Daily)





\vspace{0.4cm} 

\noindent
Esittelyverbien tarkemmaksi kartoittamiseksi suomen ja venäjän L1a-aineistot
käytiin läpi *kaikkien sellaisten ajanilmauksen pääverbien osalta, jotka
esiintyvät aineistossa vähintään viisi kertaa*. Näistä jokaisesta
verbilekseemistä määriteltiin erikseen, voidaanko sitä pitää esittelyverbinä
vai ei. Tulokseksi saatiin, että suomen L1a-aineistossa tavallisimpia
esittelyverbejä ovat seuraavat (kunkin lemman yleisyys pääverbinä on annettu
toisessa sarakkeessa):




|verbi          | frekvenssi|
|:--------------|----------:|
|julkistaa      |        109|
|julkaista      |         62|
|esitellä       |         57|
|allekirjoittaa |         31|
|esittää        |         30|
|vahvistaa      |         30|
|myöntää        |         22|
|paljastaa      |          8|
|julistaa       |          6|
|lanseerata     |          6|
|Yht.           |        361|

\noindent \headingfont \small \textbf{Taulukko 8}: Luomista merkitsevät verbit suomen L1a-aineistoissa \normalfont \vspace{0.6cm}

Venäjänkielisessä aineistossa (joka L1a-ryhmän kohdalla on poikkeuksellisesti suomenkielistä suurempi)
tavallisemmat esittelyverbit on 
puolestaan listattu taulukkoon 9:


|verbi        | frekvenssi|
|:------------|----------:|
|опубликовать |        177|
|подписать    |        102|
|представить  |         59|
|объявить     |         47|
|подтвердить  |         47|
|озвучить     |         31|
|раскрыть     |         20|
|запустить    |         15|
|выпустить    |         14|
|сообщить     |         14|
|презентовать |         13|
|анонсировать |         12|
|заявить      |         12|
|Yht.         |        563|

\noindent \headingfont \small \textbf{Taulukko 9}: Luomista merkitsevät verbit venäjän L1a-aineistoissa \normalfont \vspace{0.6cm}

\todo[inline]{käsittele sitä, että juuri *eilen* otollinen}

On vielä todettava, että kuten edellä osiossa \ref{s1-sim-pos} mainittiin, johdantokonstruktio
vaikuttaisi tyypillisemmältä muodollisemmalle tyylille. Tällä perusteella voisi ajatella,
että tutkimusaineiston lehdistöosassa johdantokonstruktioita on enemmän kuin 
internetosassa. Käytänkin kunkin ajanilmaustapauksen *lähdekorpuksen tyyppiä*
kolmantena mahdollisena johdantokonstruktion indikaattorina. Tähdennettäköön,
että kaikki tässä esitellyt kolme indikaattoria ovat ainoastaan *mahdollisia
vihjeitä* siitä, että kyseessä saattaa tavallista todennäköisemmin olla
johdantokonstruktio -- eivät välttämättömiä tai edes riittäviä ehtoja jonkin
tapauksen tulkitsemiseksi johdantokonstruktioksi.

##### Tilastollinen malli johdantokonstruktion sijaintien kartoittamiseksi

Yllä esitellyistä kolmesta indikaattorista ja L1a-aineistosta muodostettiin
keskisijainnin rakenteen tarkemmaksi tutkimiseksi tilastollinen malli, jossa
selittävinä muuttujina ovat kieli, subjektin todennäköinen aktivoitumattomuus,
lauseen pääverbin kuuluminen edellä määriteltyihin esittelyverbeihin sekä
lähdekorpuksen tyyppi. Subjektin todennäköistä aktivoimattomuutta arvioitiin
jakamalla lauseiden subjektit kahteen luokkaan, *pitkiin* ja *lyhyisiin*.
Lyhyiksi subjekteiksi laskettiin kaikki pronominit sekä muut ainoastaan yhdestä
sanasta koostuvat subjektit. Pitkiksi taas laskettiin kahdesta tai useammasta
sanasta koostuvat subjektit.[^subjtarkmit] Esittelyverbeihin kuulumista
puolestaan mitattiin yksinkertaisesti arvoilla *kyllä* tai *ei* riippuen siitä,
oliko lauseen pääverbinä jokin 
taulukoissa 8 -- 9 
esitetyistä verbeistä. Korpustyyppimuuttujan arvoja ovat *araneum* ja
*lehdistö*. Selitettävänä muuttujana on, kuten tähänkin asti, ajanilmauksen
sijoittuminen joko alku-, keski- tai loppusijaintiin.

[^subjtarkmit]: tarkemmin parsereista ym., minkä avulla mittaus tehty.


Malli on esitetty graafisesti kuviossa x:

    [L1a-MALLI]




Lähden liikkeelle tässä määritellyn tilastollisen mallin tarkastelussa tutkimalla yllä esiteltyjen
kolmen mahdollisen johdantokonstruktioindikaattorin ja kielen välisten
yhteyksien voimakkuutta. Tämä tapahtuu vertailemalla näiden kolmen
muuttujan ja kielen välisen interaktion keskihajontoja. Vertailu on esitetty
kuviossa 34 

![\noindent \headingfont \small \textbf{Kuvio 34}: Kielen ja muiden selittävien muuttujien välisten interaktioiden keskihajonnat \normalfont](figure/l1a.sd.interact-1.pdf)

Kuviosta 34 nähdään, että mahdollisesti voimakkain --
joskin myös epävarmin -- yhteys on verbityypin ja kielen välillä, heikoin
puolestaan korpustyypillä ja kielellä. Jälkimmäinen havainto on ymmärrettävä,
sillä kuten luvussa \ref{aineistot-vrn} todettiin, korpustyyppi ei ole
muuttujana kovinkaan tarkka. Korpustyypin ja kielen välisen interaktion
keskihajonta ei kuitenkaan ole täysin nollassa, joten jotain
johdantokonstruktion ja sijainnin välisestä yhteydestä voitaneen sanoa myös
korpustyyppimuuttujan avulla. Interaktion suunta selviää kuviosta 
35, joka esittää posteriorijakaumat 1) korpustyypin
ja kielen vaikutukselle suomen keskisijainnin todennäköisyyteen sekä 2)
korpustyypin ja kielen vaikutukselle venäjän alkusijainnin todennäköisyyteen:


![\noindent \headingfont \small \textbf{Kuvio 35}: Lähdekorpuksen vaikutus alku- ja keskisijainteihin suomen ja venäjän L1a-aineistoissa \normalfont](figure/l1a_corpustype-1.pdf)

Kuviossa 35 esitetyistä jakaumista on ensinnäkin todettava,
että kummatkin ovat kaikilta arvoiltaan positiivisia. Vasemmanpuoleisen kuvion mukaan
ajanilmaustapauksen kuuluminen lehdistökorpukseen kasvattaa sen todennäköisyyttä sijoittua suomessa
keskisijaintiin, joskaan tämä todennäköisyyttä kasvattava vaikutus ei ole erityisen suuri -- jakauman
moodi on 0,075.
Oikeanpuoleinen jakauma puolestaan kuvaa lehdistökorpukseen kuulumisen vaikutusta venäjän alkusijainnin
todennäköisyyteen. Jakauman kaikki arvot ovat selkeästi positiivisia, ja kuviosta havaitaan, että
venäjän S1-aseman ja lehdistökorpuksen välinen positiivinen yhteys on voimakkaampi kuin 
suomen S2/S3-aseman ja lehdistökorpuksen välinen positiivinen yhteys (moodi tässä tapauksessa 0,172 ). 
Esimerkki \ref{ee_ospeking}
kuvaa venäjän lehdistökorpuksen lausetta, jossa L1a-ajanilmaus sijoittuu lauseen alkuun ja esimerkki
\ref{ee_pmk} puolestaan suomen lehdistökorpuksen lausetta, jossa vastaava ilmaus sijaitsee lauseen keskellä.
Molemmat ovat tulkittavissa johdantokonstruktion ilmentymiksi.




\ex.\label{ee_ospeking} \exfont \small Вчера сборная России провела четвертый матч в группе А отборочного
турнира к Олимпийским играм в Пекине. (RuPress: Советский спорт)






\ex.\label{ee_pmk} \exfont \small Pieksämäki Volleyn lentopallon SM-liigajoukkue teki eilen vuoden
mittaisen pelaajasopimuksen jo kaksi edellistä kautta joukkueessa
pelanneen Heli Mantilan( 24, 176) kanssa. (Fipress: Länsi-Savo)





\vspace{0.4cm} 

\noindent
Keskihajontojen tarkastelun perusteella subjektin pituudella on korpustyyppiä
suurempi vaikutus L1a-tapausten sijoittumiseen. Kuten korpustyypin tapauksessa,
myös subjektin pituuden pohjalta tehtävät päätelmät tukevat olettamaa siitä,
että johdantokonstruktiot painottuvat suomessa keski- ja venäjässä
alkusijaintiin. Pitkät subjektit -- joita tässä tarkastellaan mahdollisina
vihjeinä johdantokonstruktiosta -- nimittäin kasvattavat suomen keskisijainnin
todennäköisyyttä verrattuna venäjään, samoin kuin myös venäjän alkusijainnin todennäköisyyttä 
verrattuna suomeen. Tilastollisen mallin mukaiset posteriorijakaumat näille kielen
ja subjektin pituuden interaktioille on esitetty kuviossa 36.

![\noindent \headingfont \small \textbf{Kuvio 36}: Subjektin pituuden vaikutus alku- ja keskisijainteihin suomen ja venäjän L1a-aineistoissa \normalfont](figure/l1a_subjtype-1.pdf)

Kuvion 36 posteriorijakaumat osoittavat, että myös
subjektityypin osalta venäjän ja S1-sijainnin interaktio on voimakkaampi kuin suomen ja S2/S3-sijainnin -- 
suomea ja keskisijaintia kuvaavan posteriorijakauman moodi on 0,070,
venäjän ja alkusijainnin osalta vastaava luku on 0,242, joten ero on itse asiassa
vielä suurempi. Myös subjektityypin osalta kummatkin jakaumat sijaitsevat  kokonaisuudessaan nollan
yläpuolella. Edellä esitetyt esimerkit \ref{ee_ospeking}
ja \ref{ee_pmk} paitsi edustavat lehdistökorpuksia, sisältävät kumpikin myös pitkät subjektit
samoin kuin seuraavat kaksi esimerkkiä. Tässäkin yhteydessä venäjänkielinen esimerkki 
on S1- ja suomenkielinen S3-tapaus.




\ex.\label{ee_lynch} \exfont \small Вчера Merrill Lynch обнародовал результаты опроса управляющих активами.
(RuPress: РБК Daily)






\ex.\label{ee_ptavat} \exfont \small Hämeenlinnassa ilmestyvä Hämeen Sanomat syytti eilen pääkirjoituksessaan
Riihimäen seutua pöytätapojen unohtamisesta. (FiPress: Hyvinkään
Sanomat)





\vspace{0.4cm} 

\noindent
Käsittelen viimeisenä johdantokonstruktion indikaattorina esittelyverbin
läsnäoloa, jolla kuvion 34 perusteella on suuri
joskin melko epävarma kieleen sidottu vaikutus ajanilmauksen sijoittumiseen. Kuten korpustyypin
ja subjektin pituuden kohdalla, myös esittelyverbien osalta tilastollisen
mallin perusteella tehtävät havainnot tukevat edellä esitettyä olettamaa johdantokonstruktioiden
ja sijainnin välisestä yhteydestä. Mallin mukaiset posteriorijakaumat
on esitetty kuviossa 37.


![\noindent \headingfont \small \textbf{Kuvio 37}: Esittelyverbien vaikutus alku- ja keskisijainteihin suomen ja venäjn L1a-aineistoissa \normalfont](figure/l1a_haspverb-1.pdf)

Kuvion 37 mukaan esittelyverbeillä ja kielellä
selvästi on yhteisvaikutusta, mutta vaikutuksen tarkka voimakkuus ei selviä
tässä käytetystä aineistosta. Vaikutuksen suunta on kuitenkin sama kuin muilla
tässä käsitellyillä muuttujilla: suomessa esittelyverbin läsnäolo kasvattaa
keskisijainnin todennäköisyyttä, venäjässä taas alkusijainnin -- jälleen
kerran, yhteys venäjän S1-sijaintiin on huomattavasti voimakkaampi kuin yhteys
suomen S2/S3-sijaintiin. Suomen ja keskisijainnin tapauksessa 95 % luottamusvälin alimmat
arvot ovat käytännössä nollassa, ylimmät noin 0,17
ja moodi 0,09; venäjässä alinkin 95 % luottamusvälin
arvo on 0,16, ylin peräti 
0,39 eli huomattavasti enemmän kuin millään muulla
tässä tarkastellulla muuttujalla.
Esimerkit \ref{ee_fsfr} -- \ref{ee_idc} kuvaavat suomen ja venäjän esittelyverbin sisältäviä tapauksia,
jotka on tulkittavissa johdantokonstruktion ilmentymiksi:




\ex.\label{ee_fsfr} \exfont \small Вчера Федеральная служба по финансовым рынкам( ФСФР) опубликовала свое
решение о приостановке выдачи паев ПИФов под управлением УК «ОФГ
(RuPress: РБК Daily)






\ex.\label{ee_idc} \exfont \small Tutkimusyhtiö IDC julkaisi eilen tutkimuksen, jonka mukaan Ruotsissa
hyödynnetään tietotekniikkaa muita valtioita paremmin. (Araneum
Finnicum: sektori.com)





\vspace{0.4cm} 

\noindent
Kokoavasti voidaan todeta, että tässä esitellyn tilastollisen mallin mukaan
kaikki johdantokonstruktion indikaattorit

a) kasvattavat keskisijainnin todennäköisyyttä suomessa ja ennen kaikkea 
b) kasvattavat alkusijainnin todennäköisyyttä venäjässä verrattuna suomeen

Kuten todettua, tässä käytetyt indikaattorit ovat  ainoastaan *vihjeitä*
tapauksen mahdollisesta johdantokonstruktioluonteesta.  Niiden perusteella
saadut tulokset ovat kuitenkin hyvin pitkälle yhteensopivia sen hypoteesin kanssa, että
juuri johdantokonstruktiot ovat yksi tärkeä syy suomen L1a-aineistossa
havaittavalle korkealle S2/S3-osuudelle.

Hypoteesin kannalta kiinnostavaa on myös se, että jokaisen indikaattorin
kohdalla venäjän ja alkusijainnin välinen yhteys osoittautui voimakkaammaksi
kuin suomen ja keskisijainnin välinen. Tämä sopii hyvin yhteen 
niiden  alkusijaintia koskevien havaintojen kanssa, joita tehtiin edellisessä
luvussa. Siinä suomi osoittautui monessa mielessä venäjää 
rajoittuneemmaksi S1-konstruktioiden käytössä, eikä nimenomaan S1-johdantokonstruktioita
juuri havaittu suomenkielisessä aineistossa, vaan koko ilmiö voitiin nähdä
pitkälti venäjälle ominaisena. Kuten vaikkapa osion \ref{l1-erot-kesk} aluksi
esitetty esimerkki \ref{ee_sergei} osoittaa, venäjä ei kuitenkaan ole vastaavalla tavalla rajoittunut
keskisijainnin käytössä, vaan johdantokonstruktioita esiintyy *myös* S2-versioina.
Tällainen tapaus on edellä esitellyn esimerkin myös esimerkit \ref{ee_broker} ja \ref{ee_minzdrav}:




\ex.\label{ee_broker} \exfont \small Еврокомиссар по энергетике Андрис Пиебалгс вчера подписал протокол о
правилах мониторинга транзита российского газа через Украину. (RuPress:
РБК Daily)






\ex.\label{ee_minzdrav} \exfont \small Глава Минздрава Татьяна Голикова вчера представила свои аргументы в
пользу обнародованного во вторник Владимиром Путиным решения об
увеличении ставки страховых взносов до 34\%-\/-(RuPress: Новый регион 2)





\vspace{0.4cm} 

\noindent

Esimerkeissä \ref{ee_broker} ja \ref{ee_minzdrav} on erityistä se, että niissä esiintyvät
kaikki tilastollisessa mallissa käytetyt johdantokonstruktion indikaattorit: niin 
lähteenä oleva lehdistökorpus, pitkä subjekti kuin esittelyverbikin. Jos tarkastellaan lähemmin
vastaavia kaikki indikaattorit sisältäviä tapauksia, voidaan todeta, että vaikka suurin 
osa (77,87) näistä
sijoittuu venäjässä S1:een, myös S2/S3 on hyvin 
mahdollinen (21,34 %). Suomessa sen sijaan S1
on erittäin harvinainen: siihen sijoittuu ainoastaan 2,48 % kaikki 
indikaattorit sisältävistä lauseista. Tältä pohjalta on helppo ymmärtää,
miksi johdantokonstruktion indikaattorien, venäjän kielen ja S1-sijainnin välinen
yhteys on tilastollisessa mallissa niin voimakas kuin se on.

Kaikki indikaattorit sisältäviä tapauksia tutkittaessa avautuu kuitenkin myös toinen
mielenkiintoinen seikka: suomessa nämä tapaukset sijoittuvat paitsi keski-, myös
loppusijaintiin. Kielten välillä vaikuttaisikin olevan eräänlainen symmetria, jonka 
mukaan kummallakin kielellä on 

- ensisijainen sijainti johdantokonstruktioille (venäjässä alkusijainti, suomessa keskisijainti)
- toissijainen mutta mahdollinen sijainti johdantokonstruktioille (venäjässä keskisijainti, suomessa loppusijainti)
- sijainti, joka ei toteuta johdantokonstruktioita (venäjässä loppusijainti, suomessa alkusijainti).

Tämä jako konkretisoituu kuviossa 38, joka esittää eri sijaintien 
suhteelliset osuudet l1a-ryhmissä toisaalta kaikki tapaukset huomioon ottaen (kuvion vasen sarake),
toisaalta vain niiden tapausten osalta, jotka sisältävät kaikki kolme
johdantokonstruktioon viittaavaa indikaattoria (kuvion oikea sarake). Suomenkielinen
aineisto on esitetty ylä- venäjänkielinen alarivillä.


![\noindent \headingfont \small \textbf{Kuvio 38}: Kaikki kolme johdantokonstruktion indikaattoria sisältävien L1a-tapausten sijoittuminen \normalfont](figure/l1a_jk_locs-1.pdf)

Kuviosta 38 havaitaan, että kummankin kielen
kaikki kolme indikaattoria sisältävien tapausten joukossa yksi sijainti
on erityisen selvästi muita hallitsevampi ja toisaalta yksi sijainti
erityisen selvästi muita harvinaisempi: venäjän S1 ja suomen S2/S3 kattavat
molemmat lähes 80 % kaikista tapauksista; toisaalta venäjän S4:n ja suomen S1:n
osuus jää selvästi alle viiden prosentin, kuitenkin niin että pudotus
on suurempi suomessa. Kummassakin kielessä ero on siis
jyrkempi kuin jos tarkastellaan kaikkia L1a-tapauksia ylipäätään.



##### L1a-ryhmän suhde muihin L1-ryhmiin

Edellisessä osiossa kuvatun tilastollisen mallin perusteella
johdantokonstruktiota voi pitää kohtalaisen uskottavana selityksenä
L1a-ryhmässä havaittavalle  poikkeuksellisen suurelle erolle keskisijainnin
yleisyydessä. On kuitenkin kysyttävä, kuinka hyvin L1a-aineiston perusteella
tehtävät päätelmät ovat yleistettävissä muihin L1-aineiston ryhmiin eli
*tänään*/*сегодня*- ja *huomenna*/*завтра*-sanoihin tai toisaalta muihin
simultaanisiin aineistoryhmiin ylipäänsä.

Kuvion 33 perusteella voidaan todeta, että L1b- ja L1c-aineistoryhmillä
erot suomen ja venäjän keskisijaintien yleisyydessä eivät ole yhtä suuria kuin L1a-ryhmällä:
siinä, missä L1a-ryhmän osalta suomen keskisijainti on 34,88 prosenttiyksikköä
venäjän keskisijaintia yleisempi, ovat vastaavat erot L1b- ja L1c-ryhmissä
23,91 ja  17,81 prosenttiyksikköä.
Muiden L1-ryhmien kohdalla havaittavat pienemmät erot sopivat kuitenkin melko
hyvin yhteen edellä esitetyn johdantokonstruktiohypoteesin kanssa: voisi väittää, että
yksinomaan menneisyyteen viittaavilla *eilen*- ja *вчера*-sanoilla
johdantokonstruktiot ovat tavallisimpia kuin myös nykyhetkeen ja tulevaisuuteen
viittaavilla *tänään*- ja *сегодня*-sanoilla tai pelkästään tulevaan
viittaavilla *huomenna*- ja *завтра*-sanoilla. Tätä väitettä voidaan perustella
ainakin kahdella argumentilla.

Ensinnäkin, kaikki luvussa \ref{ajanilmaukset-ja-alkusijainti} esitetyt
esimerkit johdantokonstruktiosta (ks. virkkeet \ref{ee_livija} -- \ref{ee_magaziny} ja \ref{ee_pfizer} -- \ref{ee_autoru})
viittasivat nimenomaan menneeseen aikaan, vaikka viikonpäiviin viittaavat ajanilmaukset
voisivat tarkoittaa myös puhehetken (tai tarkemmin: oletetun lukemishetken) jälkeisiä viikonpäiviä.
Itse asiassa suurimmaksi osaksi menneisyyteen viittaavissa L9a-ryhmän tapauksissa venäjän 
S1-asema osoittautui 48,73 prosenttiyksikköä 
suomen S1-asemaa tavallisemmaksi, kun taas vain osittain menneisyyteen viittaavissa L2a-ryhmän tapauksissa
eroksi havaittiin 29,97.

Toiseksi, on informaatiorakenteen kannalta perusteltua, että
johdantokonstruktioita esiintyy enemmän juuri menneisyyteen viittaavilla
ajanilmauksilla. Tulevaisuuteen viittaavien ajanilmausten kuten *huomenna*
tai *ensi viikolla* yhteydessä viestijä nimittäin
usein kertoo jotakin uutta topiikista, joka on jo tunnettu, niin että
ajanilmaus itse saattaa hyvinkin olla osa fokuksen diskurssifunktiota. \todo[inline]{ESIMERKKI}
Johdantokonstruktioissa ajanilmaukset kuitenkin ovat luonteeltaan ennemminkin
topikaalisia ainakin siinä mielessä, että niiden roolina on usein toimia sinä
taustana, jota vasten viestin vastaanottajalle esitellään uusia referenttejä
\todo[inline]{ESIMERKKI} . [@tobecited]
\todo[inline]{tarkoitus oli kirjoittaa tähän jotakin subjektin aktivoimattomuudesta, mutta
ajatus katkesi...}


Edellä esitetty päättelyketju ei tarkoita, etteikö johdantokonstruktiota
esiintyisi muissa aikamuodoissa kuin menneessä. Esimerkiksi aina tulevaisuuteen viittaavasta
L1c-ryhmästä voidaan esittää seuraava johdantokonstruktioesimerkki:




\ex.\label{ee_vientivaltti} \exfont \small Yksi Suomen vahvimmista vientivaltteista julkasee \emph{HUOMENNA} uuden
odotetun neljännen albuminsa "The Island Of Disco Ensemble". (Araneum
Finnicum: vanha.elmu.fi)





\vspace{0.4cm} 

\noindent
Jos kuitenkin oletetaan, että johdantokonstruktio on tavallisempi menneessä
ajassa, antaa se yhden uskottavan selityksen sille, miksi suomen ja venäjän
välinen ero keskisijainnin osuuksissa on L1a-aineistossa muita L1-ryhmiä
suurempi. 

\todo[inline]{ HUOM! Pohdi vielä venäjän fokus-zavtra + S1?}



#### Erot L5a--L5b-aineistoryhmissä  {#l4l5-erot-kesk}

L1a-ryhmän jälkeen suurin kuviosta 33 havaittava
suomen ja venäjän välinen ero koskee L5b-ryhmää, ja jotakuinkin yhtä suuri on myös L5a-ryhmän kohdalla
nähtävä ero. Tarkastelen seuraavassa, miten näissä ryhmissä ilmenevien erojen
taustalla vaikuttavat syyt suhtautuvat niihin, joita edellisessä osiossa
havaittiin L1-ryhmien osalta.

L5a-ryhmään kuuluu suomessa yhteensä 3 701 ja venäjässä yhteensä
 2 229 tapausta. Keskisijainnin tarkat osuudet
ovat 43,50 ja 25,03 prosenttia.
L5b-ryhmän osalta kokonaismäärät ovat 1007 ja 
 831 tapausta ja keskisijainnin prosenttiosuudet
45,08 % ja 24,31%. 

Niin L5a- kun L5b-ilmausten perusominaisuuksiin kuuluu, että ne viittaavat väistämättä
menneeseen aikaan, kuten L1a-ryhmä edellisessä alaluvussa. Voisikin ajatella, että
L1a-ryhmästä tehdyt havainnot pätisivät L5a-ryhmäänkin, niin että selvin suomen
ja venäjän välistä sijaintieroa selittävä tekijä olisi johdantokonstruktion
käyttö. 

Etenkin L5b-aineistosta onkin mahdollista löytää monia johdantokonstruktioksi analysoitavia
esimerkkejä, kuten seuraavat suomenkielisen aineiston keskisijaintia ja venäjänkieliset
alkusijaintia edustavat virkkeet:




\ex.\label{ee_euroskep} \exfont \small Hollannin hallitus nimitti viime viikolla virallisesti ehdokkaaksi
tunnetun euroskeptikon Frits Bolkensteinin, joka mm. vastusti Emua ja
yhteistä rahaa. (FiPress: Turun Sanomat)






\ex.\label{ee_evira} \exfont \small Evira julkisti viime viikolla vuoden 2013 eläinsuojeluvalvonnan
tulokset. (Araneum Finnicum: elaintieto.fi)






\ex.\label{ee_sivari} \exfont \small На прошлой неделе суд Нижнего Новгорода подтвердил возможность для двух
здешних призывников проходить альтернативную гражданскую службу в
местной больнице. (RuPress: РБК Daily)






\ex.\label{ee_sredstva} \exfont \small На прошлой неделе все муниципальные образования Свердловской области
получили средства на доплату за классное руководство. (RuPress: Новый
регион 2)





\vspace{0.4cm} 

\noindent
\todo[inline]{Suomalaisen tekstiili- ja vaatetusteollisuuden työnantaja- ja toimialajärjestö Finatex kutsui viime viikolla joukon alan keskeisiä yrity
ksiä keskustelemaan suomalaisen tekstiili- ja vaatetus- sekä jalkinealan koulutuksen kehittämisestä vastaamaan paremmin yritysten tarpeita.
(Araneum Finnicum: finatex.fi)}

Jos tutkitaan L1a- ja L5a--b-aineistojen koostumusta edellisessä alaluvussa
käytettyjen johdantokonstruktion indikaattoreiden osalta, huomataan, että
ryhmien koostumus on pääpiirteissään samansuuntainen, muttei identtinen. 
Erot ja yhtäläisyydet on havainnollistettu kuvioon 39,
joka esittää pitkien subjektien, esittelyverbien ja lehdistökorpuksen osuudet L1a- sekä
L5a--b-aineistoissa -- suomen osalta keskisijainnissa, venäjän osalta
alkusijainnissa.

![\noindent \headingfont \small \textbf{Kuvio 39}: Johdantokonstruktion indikaattorit suomen L1a- ja L5a--L5b-aineistojen keskisijainneissa ja venäjän vastaavien aineistojen alkusijainnissa. \normalfont](figure/indicomp2ab-1.pdf)
Kuvion vasen sarake kertoo, kuinka suuri osa kustakin aineistoryhmän osasta muodostuu
lehdistökorpuksesta peräisin olevista tapauksista (vaalea väri). Huomionarvoista kyllä,
niin suomen keski- kuin venäjän alkusijainnissa L1a-aineisto vaikuttaisi olevan
kahta muuta aineistoa lehdistövoittoisempi: lehdistötapausten osuudet
ovat suomessa -- kuvion ylempi rivi --
72,71 % (L1a),
58,20 % (L5a) ja 
57,27 % (L5b) 
ja venäjässä -- alempi rivi --
70,07 % (L1a),
48,94 % (L5a) ja 
57,50 % (L5b).

Kuvion toinen sarake kuvaa esittelyverbin sisältävien tapausten osuutta kussakin
aineistoryhmän osassa: Niin suomen keski- kuin venäjän alkusijainnissa
esittelyverbin sisältävien tapausten osuus on pienimmillään L5a-ryhmässä. Tarkat luvut
ovat suomen osalta
10,83 % (L1a),
5,22 % (L5a) ja 
14,98 % (L5b),
venäjän osalta taas
13,10 % (L1a),
3,96 % (L5a) ja 
12,65 % (L5b).

Kolmannessa sarakkeessa on esitetty pitkien subjektien osuus. Näistä havaitaan
sama kuin esittelyverbeistä: L5a-ryhmässä havaittavien johdantotopiikin indikaattoreiden
osuus on kummassakin kielessä pienempi
kuin muissa tarkastelluissa ryhmissä Tarkkaan ottaen pitkän subjektin
osuudet ovat suomessa
57,07 % (L1a),
47,52 % (L5a) ja 
56,61 % (L5b),
venäjässä
48,68 % (L1a),
35,27 % (L5a) ja 
44,52 % (L5b).


Lehdistökorpuksen suurempi osuus L1a-aineistossa saattaa olla selitettävissä
sillä, että eiliseen viittaaminen on  sanomalehtiteksteille oletettavasti
erityisen tyypillistä. Sanomalehtien tehtävänä on perinteisesti ollut
nimenomaan kertoa lukijalle, mitä tapahtui eilen: lehden ja lukijan edellinen
kohtaaminen on oletettavasti tapahtunut noin vuorokausi sitten, ja eräässä
mielessä päivittäisten lehtien koko idea on raportoida edellisestä päivästä --
ajasta, joka lehden ja lukijan edellisestä kohtaamisista on kulunut [@sommerville1996, 4].

\todo[inline]{Eroa suomi- ja venäjäaineistojen välillä?}

L5a-aineiston muita ryhmiä pienempää pitkän subjektin tai esittelyverbin
sisältävien tapausten määrää voidaan puolestaan ymmärtää, kun otetaan huomioon
ajanilmauksen referentin etäisyys kirjoitushetkeen: voisi nimittäin ajatella,
että mitä kauempana menneisyydessä ajallinen viittauspiste sijaitsee, sitä
harvemmin ajanilmausta käytetään uusien asioiden esittelemiseen lukijalle.
*Eilen* ja *viime viikolla* ovat ajanjaksoja, joista voidaan kertoa vaikkapa
esimerkkien \ref{ee_pmk} ja \ref{ee_euroskep} kaltaisia uutisia, mutta sama ei yhtä helposti
päde L5a-aineistoon ja *viime vuonna* / *в прошлом году* -ilmauksiin.
Tätä voidaan havainnollistaa tarkastelemalla seuraavaa suomen Araneum-aineiston
L1a-esimerkkiä.




\ex.\label{ee_missfd} \exfont \small Yhdysvaltalainen electroa ja industrialia luova yhden naisen
musiikkiprojekti Miss FD (www.missfd.com) julkaisi \emph{eilen} netissä
uuden musiikkivideon. (Araneum Finnicum: gootti.net)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_missfd} tarkemmasta kontekstista selviää, että kyseessä on lyhyen
uutistekstin  avausvirke:

\begin{quote}%
Miss FD:ltä "Enter the Void" -musiikkivideo

Yhdysvaltalainen electroa ja industrialia luova yhden naisen
musiikkiprojekti Miss FD (www.missfd.com) julkaisi eilen netissä uuden
musiikkivideon. Videoksi päätynyt kappale "Enter the Void" kuvattiin
Yhdysvaltojen Miamissa ja Ft. Lauderdalessa. Klippiä kuvattiin kolmen
päivän aikana useissa eri kohteissa. Sen ohjaajana toimi graafikonkin
töitä tehnyt Danny Rendo. -\/-

http://www.gootti.net/miss-fd-enter-the-void, 17.8.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Vaikuttaisi lähes mahdottomalta löytää vastaavaa esimerkkiä, jossa L1a-ilmauksen tilalla
olisi L5a-aineiston ilmaus *viime vuonna*, niin että laajemman kontekstin alku näyttäisi 
seuraavalta:

\begin{quote}%
Miss FD:ltä "Enter the Void" -musiikkivideo

Yhdysvaltalainen electroa ja industrialia luova yhden naisen
musiikkiprojekti Miss FD (www.missfd.com) julkaisi viime vuonna netissä
uuden musiikkivideon. Videoksi-\/-
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
*Viime vuonna* viittaa yksinkertaisesti niin kauas menneisyyteen, että sillä on
vaikea kuvitella olevan mahdolliselle lukijalle uutisarvoa. L1a-ryhmän ja L5a-ryhmän
välinen perusero konkretisoituu hyvin seuraavassa L5a-aineiston esimerkissä, joka on
genreltään samanlainen kuin esimerkki \ref{ee_missfd}:




\ex.\label{ee_oceania} \exfont \small Pitkän uran tehnyt yhtye julkaisi viime vuonna yhdeksännen "Oceania"
-nimeä kantavan albumin, jonka tiimoilta bändi on kiertänyt ahkerasti
vuoden ajan. (Araneum Finnicum: chaostube.net)





\vspace{0.4cm} 

\noindent
Esimerkkien  \ref{ee_missfd} ja \ref{ee_oceania} välinen ero tulee esille, kun
tarkastellaan lauseiden subjekteja ja niiden luvussa  \ref{eri-tason-topiikkeja}
esitellyn Dikin jaottelun mukaisia tarkempia diskurssifunktioita. Esimerkissä
\ref{ee_missfd} subjekti on diskurssistatukseltaan täysin uusi ja saa
esittelytopiikin diskurssifunktion. Esimerkissä \ref{ee_oceania} -- yhdessä
harvoista esittelyverbin sisältävistä L5a-tapauksista -- puolestaan on
selvästikin kyse diskurssistatukseltaan aktiivisesta subjektista, joka täyttää
esitellyn topiikin funktion. Laajempi konteksti osoittaa, että tällä kertaa
ajanilmauksen sisältävä virke on kappaleessa toisena:


\begin{quote}%
SMASHING PUMPKINS KLUBIKEIKALLE HELSINGIN THE CIRCUKSEEN

Tärkeimpiin alternative rockin suunnannäyttäjiin lukeutuva Smashing
Pumpkins saapuu Suomeen. Pitkän uran tehnyt yhtye julkaisi \emph{viime
vuonna} yhdeksännen "Oceania"-nimeä kantavan albumin, jonka tiimoilta
bändi on kiertänyt ahkerasti vuoden ajan. Viimeksi Helsingin Jäähallissa
vuonna 2008 nähty Smashin Pumpkins saapuu tällä kertaa intiimille
klubikeikalle Helsingin The Circukseen sunnuntaina 4. elokuuta.-\/-

https://chaos-tube.blogspot.fi/2013/04/smashing-pumpkins-klubikeikalle.html,
17.8.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Yllä olevien kontekstien vertailusta käy ilmi, että esimerkkien \ref{ee_missfd} ja
\ref{ee_oceania} varsinainen informaatiorakenteellinen ero ei koske ajanilmauksen
vaan lauseen subjektin edustaman referentin  diskurssistatusta ja -funktiota.
Jälkimmäinen esimerkki edustaa yhtä tavallisimmista informaatiorakenteen piirissä 
tarkasteltavista rakenteista, topiikki--kommentti-rakennetta [@lambrecht1996, 121]. Siinä viestin lähettäjä
olettaa, että viestin vastaanottajalta
puuttuu jokin tieto referentistä x (tässä tapauksessa Smashing Pumpkins -yhtyeestä). 
Esimerkki \ref{ee_missfd} puolestaan edustaa Lambrechtin [-@lambrecht1996, 124] termein
tapahtumasta raportoivaa (*event-reporting*) rakennetta, jossa viestin lähettäjä olettaa 
vastaanottajalta puuttuvan tiedon kokonaisesta tapahtumasta. Esimerkkien välistä
eroa voidaan konkretisoida vertailemalla kumpaankin esimerkkiin liittyviä
pragmaattisia olettamia ja väittämiä (ks. luku \ref{fokus} ja taulukko  edellä).
Vertailu on esitetty taulukossa 10:



-----------------------------------------------------------------------------------------------
lause                           olettama                       väittämä                        
------------------------------- ------------------------------ --------------------------------
*Yhdysvaltalainen electroa ja   \-\-                           Miss FD on yhdysvaltalainen     
industrialia luova yhden                                       electroa ja industrialia luova  
naisen musiikkiprojekti Miss                                   yhden naisen musiikkiprojekti.  
FD julkaisi eilen netissä                                      Miss FD on julkaissut           
uuden musiikkivideon.*                                         musiikkivideon. julkaisu        
                                                               tapahtui eilen                  

*Pitkän uran tehnyt yhtye       On olemassa yhtye nimeltä      S.P. on julkaissut albumin.     
julkaisi viime vuonna           Smashing Pumpkins. S.P. on     julkaisu tapahtui viime vuonna  
yhdeksännen 'Oceania' -nimeä    tärkeä alternative rockin                                      
kantavan albumin, jonka         suunnannäyttäjä. S.P. saapuu                                   
tiimoilta bändi on kiertänyt    Suomeen                                                        
ahkerasti vuoden ajan.*                                                                        
-----------------------------------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 10}: Esimerkkien \ref{ee_missfd} ja \ref{ee_oceania} pragmaattiset olettamat ja väittämät \normalfont \vspace{0.6cm}

Kuten taulukosta käy ilmi, sen paremmin esimerkin \ref{ee_missfd} kuin esimerkin
\ref{ee_oceania} tapauksessa ajanilmausta itseään (tarkemmin sanottuna sen viittauskohdetta
aikajanalla) ei pysty luontevasti määrittämään osaksi olettamaa tai väittämää. Ajanilmauksen
mukanaan tuoma informaatio on sen sijaan kuvattu lauseilla *julkaisu tapahtui eilen*
ja *julkaisu tapahtui viime vuonna*. Voidaan sanoa, että ajanilmaukset yksinkertaisesti lokalisoivat
väittämässä kerrottavan tapahtuman aikajanalle (ks. luku \ref{lokalisoiva-semanttinen-funktio}).
Esimerkkien \ref{ee_missfd} ja \ref{ee_oceania} välillä on kuitenkin se olennainen ero, että edellisessä
tapauksessa pragmaattinen väittämä pitää sisällään myös tekijänä olevan referentin esittelemisen.

\todo[inline]{POINTTI? = VENÄJÄSSÄ todella eri viestintästrategia, niin että ajanilmauksesta *voidaan* määrittää erikseen,
että se on osa olettamaa?
vrt olettama: EILEN tapahtui jotakin.

}


Käännetään nyt huomio kielensisäisistä eroista kieltenvälisiin. Edellä osioissa
\ref{s1-sim-pos} ja \ref{l1-erot-kesk} on jo kuvailtu suomen ja venäjän välistä
eroa esimerkin \ref{ee_missfd} kaltaisissa johdantokonstruktiotapauksissa.
Kokoavasti informaatiorakenteeltaan esimerkin \ref{ee_missfd} kaltaisista
tapauksista voidaan todeta, että venäjässä toisin kuin suomessa ajanilmaus
on tavallista sijoittaa S1-johdantokonstruktion mukaisesti lauseen alkuun. Näin on tehty muun
muassa esimerkissä  \ref{ee_vedushaja}, jonka alla esitetty laajempi konteksti
osoittaa lähdetekstin ensimmäiseksi virkkeeksi.




\ex.\label{ee_vedushaja} \exfont \small Вчера известная ведущая Анфиса Чехова отпраздновала свой день рождения.
(Araneum Russicum: topdaynews.ru)





\vspace{0.4cm} 

\noindent
\begin{quote}%
Муж Анфисы Чеховой сделал трогательное признание

Вчера известная ведущая Анфиса Чехова отпраздновала свой день рождения.
К 36 годам она успела сделать отличную карьеру на телевидении, завести
семью, родить сына. Ее гражданский супруг-\/-

http://topdaynews.ru/index.php?nma=news\&fla=stat\&nums=8031, 18.8.2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Jos esimerkin jaottelee edellä kuvatun tapaan pragmaattisiksi
olettamiksi ja väittämiksi, saadaan taulukon 11 mukainen tulos:


-------------------------------------------------------------------
lause                         olettama   väittämä                  
----------------------------- ---------- --------------------------
*Вчера известная ведущая      \-\-       eilen tapahtui jotakin    
Анфиса Чехова отпраздновала              relevanttia. on olemassa  
свой день рождения.*                     tunnettu juontaja A.Tš..  
                                         juontajalla oli eilen     
                                         syntymäpäivä              
-------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 11}: Esimerkin \ref{ee_vedushaja} pragmaattiset olettamat ja väittämät \normalfont \vspace{0.6cm}

Toisin kuin edellä taulukossa 10, esimerkin \ref{ee_vedushaja}
tapauksessa ajanilmauksen viittauskohteelle on mahdollista määrittää
diskurssistatus ja  diskurssifunktio. Tulkintani mukaan eilinen tuodaan
esimerkissä \ref{ee_vedushaja} mukaan välitettävään viestiin konkreettisena referenttinä,
jota käsitellään diskurssistatukseltaan tunnistettavissa olevana.
Tämä referentti kuitenkin aktivoidaan samaan tapaan kuin lauseiden subjektien viittauskohteet esimerkeissä 
\ref{ee_missfd} ja \ref{ee_vedushaja}, niin että kyse on on lopulta samantyyppisestä esittelytopiikista.
Tämän takia taulukossa 11 ajanilmauksen referenttiin liittyy oma itsenäinen
väittämänsä toisin kuin taulukossa 10, jossa
ajanilmauksella on ainoastaan tapahtumaa lokalisoiva funktio.

\todo[inline]{Prosodinen tutkimus olisi paikallaan...}

Suomen ja venäjän välillä alku- ja keskisijainnin käytössä selvästi havaittavan eron
voisikin nähdä liittyvän juuri taulukossa 11 kuvatun 
kaltaiseen informaatiorakenteeseen. Vaikuttaisi nimittäin siltä,
että syy juuri S1-johdantokonstruktion harvinaisuuteen suomessa on siinä, ettei 
suomessa ole yhtä tavallista tehdä ajanilmauksesta konkreettista, topikaalista referenttiä,
jota vasten koko seuraava viesti kerrotaan.
Jos vaikkapa esimerkki \ref{ee_vedushaja} olisi peräisin suomenkielisestä
aineistosta, alkaisi se hyvin todennäköisesti *Tunnettu juontaja Anfisa
Tšehova...* eikä *Eilen tunnettu juontaja Anfisa Tšehova...*.  Suomelle tyypillisempää
onkin nimenomaan lähteä liikkeelle esiteltävästä agentista, mikä tehdään ilman ajanilmauksen
tarjoamaa "topikaalista ponnahduslautaa", toisin sanoen S3-johdantokonstruktioilla.

\todo[inline]{Huomaa, että venäjässä suomen kaltainen strategia kyllä käytössä.}

Tilanne kuitenkin muuttuu jonkin verran, kun siirrytään esimerkkien \ref{ee_missfd}
ja \ref{ee_vedushaja} kaltaisista tapauksista esimerkin \ref{ee_oceania} kaltaisiin
tapauksiin. Koska tekstin varsinainen diskurssitopiikki Smashing Pumpkins on jo
esitelty tekstin ensimmäisessä virkkeessä, voisi hyvinkin olla mahdollista,
että jossain myöhemmässä kohden tekstiä olisi ajanilmauksella alkava lause
*Viime vuonna pitkän uran tehnyt yhtye julkaisi\-\-*, jolloin kirjoittaja
oletettavasti turvautuisi alatopiikkeihin: *edellinen vuosi* saisi segmentoivan
funktion, eli sen avulla tekstin varsinaisesta, jo esitellystä topiikista
kerrottavaa informaatiota jaoteltaisiin erilaisiksi kerrottaviksi palasiksi.
Koska L5a-aineiston ajanilmaukset esiintyvät tyypillisemmin jo esiteltyjen
diskurssitopiikkien yhteydessä, vastaavat viestintästrategiat ovat mahdollisia ja
jopa jossain määrin tavallisia. Alatopiikkikonstruktion lisäksi kysymykseen
tulevat ainakin kontrastiivista (ks. osio \ref{s1-sim-pos} edellä) tai
topikaalista konstruktiota (esim. osio \ref{sekv-s1}) hyödyntävät strategiat.
Seuraava virke on valaiseva esimerkki lauseenalkuisen *viime vuonna* -ilmauksen
käytöstä osana alatopiikkikonstruktiota:




\ex.\label{ee_paltrow} \exfont \small \emph{Viime vuonna} Gwyneth julkaisi keittokirjan My Father's Daughter,
jonka lähtökohtana on hänen intohimonsa ruokaan ja hänen lapsuutensa ja
nuoruutensa isänsä Bruce Paltrow'n kanssa, joka oli loistava
ruuanlaittaja. (Araneum Finnicum: lindex.com)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_paltrow} on osa pitkää, näyttelijä Gwyneth Paltrow'sta kertovaa
blogikirjoitusta, jonka kirjoittaja käyttää suurimman osan ensimmäisestä kappaleesta
diskurssitopiikkinsa esittelemiseen. Esimerkin \ref{ee_paltrow} sisältävä kappale seuraa 
teksin loppupuolella ja *Viime vuonna* on siinä tyypillinen ajallinen alatopiikki,
jonka avulla Paltrow'n elämästä kerrotaan vielä yksi siivu:

\begin{quote}%
Gwyneth - All you need to know

Hän on yksi aikamme suosituimpia muoti-ikoneja ja näyttelijättäriä.
Hänet tunnetaan muun muassa elokuvista Emma, The Royal Tennenbaum's,
Rakastunut Shakespeare ja tietenkin romanttisesta draamasta Sliding
Doors. Gwyneth Paltrow.-\/-

Viime vuonna Gwyneth julkaisi keittokirjan My Father's Daughter, jonka
lähtökohtana on hänen intohimonsa ruokaan ja hänen lapsuutensa ja
nuoruutensa isänsä Bruce Paltrow`n kanssa, joka oli loistava
ruuanlaittaja. Ruuanlaitto on merkittävä osa hänen elämäänsä ja perheen
ja ystävien kanssa seurustelu ruuan lomassa on Gwynethin mukaan tärkeä
osa onnellista elämää.

https://www.lindex.com/fi/blogi/2012/3/21/gwyneth-all-you-need-to-know/,
18.8. 2017
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_paltrow} kaltaisten strategioiden hyödyntäminen vaikuttaisi
todella olevan olennaista, kun mietitään tilanteita, joissa suomessa
tyypillisesti käytetään S1-asemaa. Jos
tarkastellaan L1a-ryhmän tyyppistä aineistoa, jossa ajanilmaus usein esiintyy
juuri diskurssistatukseltaan aktivoimattomien, vasta esiteltävien topiikkien
yhteydessä, on S1-sijainnin osuus kaikista ilmauksista 
melko pieni, 16,42 prosenttia. L5b-aineistoryhmässä 
samassa lauseessa esiteltävät topiikit ovat mahdollisia mutteivät yhtä tavallisia,
ja niinpä S1-sijainnin osuuskin on suurempi, 24,83 prosenttia.
L5a-ryhmässä, jossa ei edellä esitetyn perusteella juuri esiinny johdantokonstruktioita,
S1-sijainnin osuus on peräti 36,15 prosenttia.

Kiinnostavaa kyllä, venäjän L1a-, L5b- ja L5a-ryhmien välillä ei havaita likimainkaan
vastaavaa vaihtelua S1-sijainnin yleisyydessä kuin suomessa:
L1a-ryhmässä S1-sijainnin osuus on 68,91 %,
L5b-ryhmässä 71,36 
ja L5a-ryhmässä 65,63 %. Vaikuttaisi siis siltä,
että tarkasteltaessa L1a- ja L5a/b-ryhmistä muodostettua osa-aineistoa aineistoryhmällä
on vaikutus suomen, muttei venäjän alkusijainnin yleisyyteen. Tämän kielen
ja aineistoryhmän välisen interaktion olemassaolo voidaan varmistaa muodostamalla
sen tutkimiseksi oma tilastollinen mallinsa. Mallin
selitettävänä muuttujana on, kuten tavallista, ajanilmauksen
sijainti. Selittäviä muuttujia ovat kieli ja aineistoryhmä. Aineistoryhmän (L1a/L5a) 
vaikutus todella on mallin mukaan suomessa ja venäjässä selvästi erilainen, mikä käy ilmi,
kun vertaa mallin muuttujien keskihajontoja kuviossa 40 :


```
## Ladataan tallennetua dataa Bayes-malliin nimeltä l1al5a
```

![\noindent \headingfont \small \textbf{Kuvio 40}: L1a- ja L5a-aineistoja vertailevan mallin keskihajonnat \normalfont](figure/l1al5a.std-1.pdf)

Kuvion 34 perusteella kielellä on odotuksenmukaisesti suurin
merkitys sen kannalta, mihin sijaintiin ajanilmaus L1a- ja L5a-ryhmistä
muodostetussa osakorpuksessa päätyy. Myös aineistoryhmä sinänsä on
merkityksellinen. Olennaisin havainto on, että aineistoryhmän ja kielen välillä
on selvä, nollasta erottuva yhteisvaikutus. Tämä yhteisvaikutusta on esitetty alkusijainnin
osalta kuviossa 41, jossa totuttuun tapaan näkyy vaikutus suomessa
verrattuna venäjään:

![\noindent \headingfont \small \textbf{Kuvio 41}: L5a-ryhmän vaikutus alkusijainnin todennäköisyyteen suomessa verrattuna venäjään \normalfont](figure/l5a_interact2-1.pdf)

Kuvion 41 perusteella suomi kielenä ja L1a aineistoryhmänä
ovat yhdistelmä, joka vähentää alkusijainnin todennäköisyyttä. Suomen ja L5b-ryhmän
vaikutus on neutraali ja suomen ja L5a-ryhmän vaikutus puolestaan S1-sijainnin todennäköisyyttä kasvattava.
Havainnot sopivat edellä esitettyyn ajatukseen siitä, että L1a, L5b ja L5a muodostavat ikään kuin
jatkumon, jonka toisessa päässä (L1a) ovat ilmaukset, jotka esiintyvät usein osana johdantokonstruktiota
ja siten suomessa vain melko harvoin S1-asemassa ja toisessa päässä (L5a) ilmaukset, jotka eivät juuri 
esiinny johdantokonstruktiossa.

Sen lisäksi, että L5a-ryhmältä puuttuu alkusijainnin käyttöä suomessa
vähentäviä ominaisuuksia, sillä voidaan nähdä myös ainakin yksi alkusijainnin
käyttöä lisäävä ominaisuus: taipumus esiintyä osana määrää painottavaa
konstruktiota. Kuten edellä osiossa \ref{kehres-s1} (ks. taulukko )
todettiin, määrää painottava konstruktio on erityisen tavallinen ilmauksilla,
jotka viittaavat vuoden käsitteeseen. Suomen L5a-ryhmässä onkin koko joukko
esimerkin \ref{ee_tuontikala} kaltaisia tapauksia. Esimerkki \ref{ee_katoliki}
puolestaan on vastaava venäjänkielinen esimerkki.




\ex.\label{ee_tuontikala} \exfont \small Viime vuonna suomalainen söi kalaa runsaat 14 kiloa, josta puolet
tuontikalaa. (FiPress: Keskisuomalainen






\ex.\label{ee_katoliki} \exfont \small В прошлом году немецкие католики пополнили за счет налогов бюджет церкви
на 5 миллиардов 200 миллионов евро. (Araneum Russicum:
germania-online.ru)





\vspace{0.4cm} 

\noindent
On kuitenkin huomattava, että määrää painottava konstruktio ei suomessa rajoitu alkuaseman käyttöön, vaan
myös seuraavien virkkeiden kaltaisia keskisijaintitapauksia on runsaasti:




\ex.\label{ee_tarkastajat} \exfont \small Alkoholitarkastajat tekivät Oulun läänissä viime vuonna vähittäismyynti-
ja anniskelupaikkoihin yhteensä 321 valvontakäyntiä. (FiPress: Kaleva)






\ex.\label{ee_matkat} \exfont \small Suomalaiset tekivät viime vuonna ulkomaille 7,8 miljoonaa erilaista
vapaa-ajanmatkaa. (Araneum Finnicum: kokoomusnaiset.fi)





\vspace{0.4cm} 

\noindent
Määrää painottavan konstruktion ja keskisijainnin väliseen yhteyteen palataan tarkemmin
alempana osiossa \ref{num-s2s3}.

Kaiken kaikkiaan L5a- ja L5b-aineistojen perusteella tehdyissä havainnoissa
korostuu, että simultaanisen funktion erilainen vaikutus suomen ja venäjän
S2/S3-todennäköisyyksiin on sidoksissa siihen, minkälaisissa
viestintästrategioissa ajanilmauksia käytetään. Jos aineistoryhmälle on
tyypillistä olla osana esittelytopiikkeja sisältäviä tapahtumista raportoivia
rakenteita, jäävät lauseenalkuisen sijainnin käyttötilanteet suomessa
vähäisemmiksi. Vaikuttaa siltä, että suomessa johdantokonstruktioon liittyy
selvästi S3-sijoittunut ajanilmaus, siinä missä venäjässä sekä S1- että
S2-sijaintia noudattavat konstruktiot ovat mahdollisia mutta S1-konstruktiot
tavallisempia johtuen topikaalisen, lauseenalkuisen ajanilmauksen sisältävän
viestintästrategian tavallisuudesta. \todo[inline]{paranna}


#### Erot L7c- ja L8b-ryhmissä  {#l7l8-erot-kesk}

Siirryn nyt tarkastelemaan kahta edellisistä melko selvästi poikkeavaa aineistoryhmää,
L7c:tä (ks. osio \ref{l7a-l7b-l7c-l7d}) ja L8b:tä (osio \ref{l8a-l8b-l8c}). Kuvio 33 
osoittaa, että suomessa näiden molempien käyttö painottuu huomattavasti venäjää voimakkaammin
keskisijaintiin. L7c-ryhmän osalta keskisijainnin osuus on suomessa (60,07 %) 
lähes kaksinkertainen verrattuna venäjään 
(32,28 %). Suomen L8b-ryhmässä taas keskisijaintiin
osuu peräti  79,48 %, siinä missä venäjän vastaava osuus on 43,85 %.

Edellä suomen *äkkiä* ja venäjän *вдрyг*-sanoja (osio
\ref{varsinainen-resultatiivinen-ekstensio}) analysoitaessa havaittiin tilanne,
jossa toisessa tarkasteltavista kielistä ajanilmauksella on ei-temporaalinen
merkitys, joka toisen kielen vastaavalta ilmaukselta puuttuu. *Silloin*- ja
*тогда*-sanoista voidaan puolestaan todeta, että kumpaankin sanaan liittyy
samanlainen ei-temporaalinen merkitys. Niin venäjän *тогда* kuin suomen
*silloin* saavat sanakirjojen mukaan [@ushakov, тогда; @ktsk, silloin]
perusmerkityksensä lisäksi merkityksen 'siinä tapauksessa, nämä olosuhteet
huomioon ottaen'. Yksi selkeä konteksti, jossa näitä merkityksiä esiintyy, ovat
jos-lauseen sisältävät virkkeet kuten \ref{ee_traktorimarssi} ja \ref{ee_proigrajete}:




\ex.\label{ee_traktorimarssi} \exfont \small Jos taas EU aikoo leikata maataloustukiaisia, niin silloin ovat Ranskan
talonpojat pian väkivaltaisella traktorimarssilla tai tuhoamassa kaikkia
tuonti elintarvikkeita. (Araneum Finnicum: takku.net)






\ex.\label{ee_proigrajete} \exfont \small Но учтите, что если вы проиграете дело в арбитражном суде Уральского
округа, тогда ваша борьба потеряет смысл. (RuPress: Новый регион 2)





\vspace{0.4cm} 

\noindent
Etenkin venäjässä toinen melko tyypillinen ympäristö tälle ei-temporaaliselle
merkitykselle on syytä selvittävien kysymyssanojen *почему*
ja *зачем* jälkeen. Näitä tapauksia aineistossa on 
 17 kappaletta, esimerkiksi
seuraava virke:




\ex.\label{ee_zatshem} \exfont \small Зачем тогда Следственный комитет ищет владельцев аэропорта? (RuPress:
Комсомольская правда)





\vspace{0.4cm} 

\noindent
Kaikissa tapauksissa L7c-ryhmään liittyvän temporaalisen merkityksen ennustaminen
kontekstin perusteella ei kuitenkaan ole mahdollista, vaan etenkin suomenkielisestä
aineistosta on mahdollista löytää myös seuraavanlaisia esimerkkejä, joista
selvä kontekstuaalinen vihje puuttuu mutta merkitys on silti ei-ajallinen:




\ex.\label{ee_sieluparka} \exfont \small Sellainen sieluparka on silloin antanut elämänsä sielunvihollisen
rääkättäväksi. (Araneum Finnicum: rukoilevaisuus.com)





\vspace{0.4cm} 

\noindent
\todo[inline]{
Детский стоматолог только тогда имеет право называться хорошим врачом, если он любит детей и умеет с ними общаться.
 (Araneum Russicum: styledent.ru)
 }





L7c-aineistoryhmän ei-temporaalinen merkitys vaikuttaisi venäjässä
olevan  vahvasti linkittynyt lauseenalkuiseen asemaan: edellä kuvatun kaltainen 
vihje merkityksen ei-temporaalisuudesta on 233 tapauksessa ja näistä
 188 (80,69 %) sijoittuu S1-asemaan.
Suomessa vihje ei-temporaalisuudesta on 148 tapauksessa, joista
95 (64,19 %) osuu S1-asemaan ja
50 (33,78 %) S2/S3-asemaan.
L7c-aineistoissa kokonaisuutena havaittavaan kieltenväliseen eroon näillä ei-temporaalisilla
rakenteilla ei kuitenkaan näyttäisi olevan juuri merkitystä.
 Jos edellä kuvatut ei-temporaalisen vihjeen sisältävät tapaukset
karsii pois, keskisijainnin osuudeksi saadaan  venäjässä 
34,42 prosenttia ja suomessa 
63,45, jolloin kielten välinen ero keskisijainnin
osuuksissa on itse asiassa suurempi, kuin mitä sen koko L7c-aineistossa havaittiin olevan.



L7c-ryhmässä onkin erityistä se, että loppusijainti on sille poikkeuksellisen harvinainen:
venäjän L7c-tapauksista S4:ään sijoittuu ainoastaan
1,04 %, ja suomessakin vastaava lukema on vain 
5,54 %. Esimerkkien \ref{ee_komulainen} ja \ref{ee_sebjatogda}
kaltaiset tapaukset ovat siis mahdollisia, joskaan eivät yleisiä:




\ex.\label{ee_komulainen} \exfont \small Hakkarainen tuurannee Komulaista silloin. (FiPress: Kaleva)






\ex.\label{ee_sebjatogda} \exfont \small -\/- Как вы чувствовали себя тогда? (RuPress: Комсомольская правда)





\vspace{0.4cm} 

\noindent
Suomen ja venäjän välillä L7c-ryhmässä havaittava ero näyttää siis
muodostuvan siitä, miten kielet hyödyntävät niitä kahta sijaintia,
jotka jäävät jäljelle, kun loppusijainti on rajattu mahdollisuuksien
ulkopuolelle. Venäjässä tapaukset keskittyvät, kuten tavallista, S1-sijaintiin,
suomessa taas S3-sijaintiin, vaikkakin myös suomessa S1 on keskimääräistä
yleisempi (34,40 % L7c-tapauksista). Suomen L7c-aineiston S1-tapauksia
tarkastelemalla voidaan havaita ainakin esimerkkien \ref{ee_kirjastoauto} -- \ref{ee_daavid} kaltaiset
käyttötilanteet, joita kaikkia on käsitelty luvussa  \ref{ajanilmaukset-ja-alkusijainti} alkusijainnin
analyysin yhteydessä:




\ex.\label{ee_kirjastoauto} \exfont \small Torstai on kirjastoauton käyntipäivä, ja silloin talo tarjoaa kahvit,
kertoo kaupassa toisena yrittäjänä toimiva helsinkiläinen Pipsa Nyblin
(FiPress: Hämeen Sanomat)






\ex.\label{ee_parivaljakko} \exfont \small Sama parivaljakko ratkaisi cupin viime kaudellakin, silloin Pateri
voitti kuopiolaiset suoraan luvuin 3-0. (FiPress: Turun Sanomat)






\ex.\label{ee_tamma} \exfont \small -\/-mutta kun aika tehdä töitä, silloin tämä nuori tamma muistaa
tehtävänsä – etenkin jos kyse on hyppäämisestä. (Araneum Finnicum:
still-hof.com)






\ex.\label{ee_vajota} \exfont \small Välillä voimien ehtyessä se meinasi vajota, mutta silloin se kokosi
voimansa ja sai taas uimisen rytmistä kiinni. (Araneum Finnicum:
tekstaritupu.net)






\ex.\label{ee_daavid} \exfont \small Silloin hän vihdoin katsoo ensin tummuvaa itseään ja sitten edessä
kirkastuvaa polkua; silloin hän lopulta huokaisee Daavidin sanoin:
Jumala, ole minulle armollinen hyvyytesi tähden; pyyhi pois minun
syntini suuren laupeutesi tähden. (Araneum Finnicum:
kristitynfoorumi.fi)





\vspace{0.4cm} 

\noindent
Ensinnäkin, kuten luvussa \ref{sekv-s1} huomautettiin, *silloin*-sana kuuluu
niihin ajanilmauksiin, jotka selvimmin viittaavat johonkin edellisessä
tekstissä muodostettuun ajalliseen topiikkiin. Kun silloin-pronominin ajallinen
korrelaatti on läsnä edeltävässä kontekstissa, esimerkkien \ref{ee_kirjastoauto} ja
\ref{ee_parivaljakko} kaltainen sekventiaalista konstruktiota muistuttava rakenne on
hyvin mahdollinen. Toiseksi lauseenalkuisuus on tyypillistä esimerkin \ref{ee_tamma}
kaltaisille tapauksille, jotka voitaisiin lukea myös edellä käsiteltyihin
ei-temporaalisiin silloin-sanan konteksteihin. Kolmanneksi *silloin* esiintyy
usein *mutta*-lauseen alussa, jolloin se rinnastuu *äkkiä*-konstruktioon:
esimerkissä \ref{ee_vajota} *silloin* ilmaisee yllättävää, tapahtumien aiemmasta
kulusta poikkeavaa kontrastia hyvin samalla tapaa kuin vaikkapa esimerkeissä \ref{ee_rutiinit}
ja \ref{ee_rakennusperinto} edellä. Neljänneksi on vielä todettava, että lauseenalkuinen *silloin*
voidaan nähdä myös osiossa \ref{s1-f1a} mainitun affektiivisen konstruktion
ilmentymänä. Tämä tulee hyvin ilmi esimerkissä \ref{ee_daavid}, joka vertautuu sekä
tyylilajiltaan että aihepiiriltään esimerkkiin \ref{ee_synti} edellä.

L7c-aineistossa on kuitenkin erityistä se, ettei siinä suomen ja venäjän välillä esiintyvää
eroa voida monien muiden simultaanisten ilmausten tavoin selittää johdantokonstruktioiden
käytöllä. Koska *silloin* ja *тогда* ovat lähtökohtaisesti anaforisia ilmauksia, 
on niiden kohdalla tavallisesti oletettava, etteivät ne voi sijaita tekstin alussa,
vaan niiden on väistämättä liityttävä johonkin jo aiemmassa tekstissä muodostettuun
ajalliseen topiikkiin (TT). Suomen ja venäjän välillä L7c-aineistossa havaittavan eron taustalla
vaikuttaneekin ennen kaikkea se, miten edeltävässä kontekstissa muodostettuihin ajallisiin topiikkeihin
on tyypillistä viitata. 



Yksi venäjän L7c-ilmausten S1-tapauksia leimaava piirre on, että *тогда*-sanaa edeltää
adverbi *именно* ('juuri, nimenomaan'). Jos katsotaan venäjän S1-lauseiden vasemmanpuoleisia
kollokaatteja, *именно*-tapaukset ovat heti *и*-konjunktion jälkeen toiseksi tavallisimpia
(79 kappaletta 
kaikkiaan 1 220 S1-tapauksesta). Esimerkkejä näistä lauseista ovat
seuraavat:




\ex.\label{ee_udmurtii} \exfont \small Именно тогда Правительство Удмуртии будет рассматривать проект заявки.
(RuPress: Комсомольская правда)






\ex.\label{ee_burisko} \exfont \small Именно тогда владелец уникальной синематеки Жан Мари Бурсико впервые
представил свою коллекцию на суд зрителей. (RuPress: РБК Daily)






\ex.\label{ee_otbor} \exfont \small Отбор танцоров начинается в январе, именно тогда организаторы начинают
поиск молодых и талантливых парней и девушек. (Araneum Russicum:
foxtour.ru)





\vspace{0.4cm} 

\noindent
Venäjän именно + тогда -tapausten taustalla oleva konstruktio näkyy selvimmin esimerkissä 
\ref{ee_otbor}, jossa *именно тогда* toimii ikään kuin suomen *jolloin*-adverbia vastaavana
lauseita yhdistävänä, konjunktiomaisena sanana. Esimerkkien \ref{ee_udmurtii} ja \ref{ee_otbor}
laajemmat kontekstit osoittavat vastaavan käyttötavan:

\begin{quote}%
Но кроме этих домов в заявку 2010 года могу попасть и другие. Какие
именно, решится в конце этой недели. Именно тогда Правительство Удмуртии
будет рассматривать проект заявки.

https://www.izh.kp.ru/daily/24460/622000/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
\begin{quote}%
"Ночь пожирателей рекламы" проводится ежегодно по всему миру с 1981
года. Именно тогда владелец уникальной синематеки Жан Мари Бурсико
впервые представил свою коллекцию на суд зрителей

https://lenizdat.ru/articles/1061710/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Kaikissa tässä kuvatuissa esimerkeissä on ensin eksplisiittisesti muodostettu ajallinen
topiikki, josta tämän jälkeen tulee itse asiassa diskurssifunktioltaan 
seuraavan lauseen fokus. Esimerkissä \ref{ee_udmurtii} propositio [hallitus käsittelee
valintaprosessia] voidaan ymmärtää diskurssistatukseltaan aktiivisena: lukija
vähintäänkin implisiittisesti tietää, että jotain tälläistä on tapahtunut tai tulee
tapahtumaan, ja ajanilmaus määrittelee, milloin näin käy. Konstruktion -- jota
nimitän *fokaaliseksi S1-konstruktioksi* -- rakenne on siis pääpiirteissään
seuraavanlainen:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & foc \\
\] \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

ds   act+ \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{0.3em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 19}: Fokaalinen S1-konstruktio \normalfont \vspace{0.6cm}

Huomattakoon, etten näe tarpeelliseksi pyrkiä erikseen määrittelemään, mikä
matriisissa 19 on topiikkina -- monissa tapauksissa
topiikin diskurssifunktio langennee prototyyppisesti subjektille. Oleellista
on, että itse ajanilmauksen jälkeen välitettävä propositio on jotakin sellaista,
josta viestin vastaanottajalta kirjoittajan mielestä puuttuu tieto siitä, milloin kyseinen asia
on tapahtunut (tai ainakin tästä ajankohdan ja proposition yhteydestä halutaan muistuttaa).

Vaikka matriisissa 19 esitelty rakenne tulee erityisen
selvästi ilmi juuri *именно*-sanan kautta, ei *именно*-sanan läsnäolo 
ole konstruktion toteutumisen kannalta välttämätöntä, sillä vastaavaa funktiota
voidaan toteuttaa myös *же*-partikkelilla, kuten esimerkeissä \ref{ee_donskoj}
ja \ref{ee_blavatskaja}:




\ex.\label{ee_donskoj} \exfont \small Именно отсюда Дмитрий Донской повел свое войско на Куликовскую битву, и
тогда же он дал обет в случае победы построить церковь Всех святых.
(RuPress: Комсомольская правда)






\ex.\label{ee_blavatskaja} \exfont \small А в ноябре 1875 года Олкотт, Блаватская и ещё несколько человек (в их
числе Уильям Джадж) сформировали Теософское общество. - Тогда же
Блаватская начала писать свою первую книгу. (Araneum Russicum:
nowimir.ru)





\vspace{0.4cm} 

\noindent
*Же*-partikkeli on kaiken kaikkiaan heti persoonapronominien jälkeen seitsemänneksi 
yleisin oikeanpuoleinen kollokaatti S1-asemaan sijoittuvilla venäjän
L7c-ilmauksilla (21 tapausta).

Matriisissa 19 kuvatun kaltainen konstruktio vaikuttaa
suomessa melko harvinaiselta. Esimerkiksi suomen *juuri* + *silloin* -tapaukset
liittyvät useimmiten seuraavanlaisiin *äkkiä*-konstruktiota edustaviin tapauksiin:




\ex.\label{ee_susi} \exfont \small Tuo meni matalempaan asentoon hitaasti, mutta juuri \emph{silloin} susi
päästi ilmoille korviariipivän karjaisun ja syöksyi suurella raivolla
kohti silmiinsä piirtynyttä ihmistä. (Araneum Finnicum:
chenevalie.palstani.com)





\vspace{0.4cm} 

\noindent
Vaikuttaisikin siltä, että esimerkkien @ee_ -- \ref{ee_blavatskaja} kaltaisten rakenteiden
sijaan suomessa on tyypillisempää viitata edellisessä kontekstissa muodostettuihin
ajallisiin topiikkeihin S3-konstruktioilla, kuten esimerkissä \ref{ee_syksy}:




\ex.\label{ee_syksy} \exfont \small Syksy on myös uuden alkua ja moni aloittaakin silloin uuden
terveellisemmän elämän. (Araneum Finnicum: fast.fi)





\vspace{0.4cm} 

\noindent
Myös esimerkistä \ref{ee_syksy} voidaan tulkita, että ajatus uuden, terveellisemmän elämän aloittamisesta
on tekstin lukijoille tuttu -- etenkin, kun esimerkki on peräisin urheiluravinteita
valmistavan *Fast*-yrityksen sivuilta. Esimerkissä \ref{ee_syksy} nimenomaan
yhdistetään ajatus terveellisemmän elämän aloittamisesta ja tieto siitä, että
se liittyy usein juuri syksyyn (*silloin*-sanan referentti). Juuri tämä yhteys muodostaa
esimerkin pragmaattisen väittämän ja siten toteuttaa fokuksen diskurssifunktiota. Asia käy
selvemmin ilmi, kun esimerkkiä \ref{ee_syksy} vertaa keksittyyn S4-sijaintia hyödyntävään
lauseeseen *ja moni aloittaakin uuden terveellisemmän elämän silloin*, jossa ajanilmauksen fokusrooli
olisi korostetumpi.




Fokaalisuuden ja venäjän S1-sijainnin välinen yhteys näkyy myös niissä
L7c-ryhmän lauseissa, joissa *тогда*- ja *silloin*-sanoja edeltävät
presuppositionaaliset adverbit *уже* tai *jo*. Presuppositionaaliset
ilmaukset tuovat usein mukanaan fokaalisen tulkinnan [@tobecited], mikä näkyy siinä,
että suomen L7c aineistossa *jo silloin* -tapaukset 
(kaikkiaan 54 kappaletta) sijoittuvat huomattavan usein
myös S4-asemaan. Suomen *jo silloin*- ja venäjän *уже тогда* -tapausten (yht. 42 kpl)
jakautuminen eri sijainteihin on esitetty tarkemmin seuraavassa kuviossa:

![\noindent \headingfont \small \textbf{Kuvio 42}: Suomen ja venäjän presuppositionaaliset tapaukset L7c-aineistossa. \normalfont](figure/unnamed-chunk-11-1.pdf)

Kuvio 42 osoittaa, että venäjässä *уже тогда* -ilmauksia esiintyy
lähes yhtä usein (tarkkaan ottaen 20 kpl) alkusijainnissa kuin
keskisijainnissa, kun taas suomessa loppusijainti on paljon alkusijaintia tavallisempi.
Yksi venäjän S1-tapauksista  on esimerkki \ref{ee_hudozhniki}, joka on tulkintani mukaan
tulkittavissa nimenomaan fokaalisen S1-konstruktion ilmentymäksi:




\ex.\label{ee_hudozhniki} \exfont \small Уже тогда художники пытались, используя мастерство каждого, выработать
единый творческий почерк. (RuPress: РБК Daily)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_hudozhniki} proposition [taiteilijat yrittävät saavuttaa
omaa tyyliään] on oletettavasti lukijan tajunnassa aktiivisena -- esimerkki
välittää lukijalle nimenomaan tiedon siitä yhteydestä, että
propositio liittyy myös *тогда*-sanan osoittamaan ajankohtaan. Suomessakin
S1-tapauksia on 11 kappaletta, mutta
paljon tavallisempaa on, että *jo silloin* -adverbiaalit sijaitsevat
keskisijainnissa kuten esimerkki \ref{ee_kykysi} tai loppusijainnissa kuten
esimerkki \ref{ee_poikkeusta}:




\ex.\label{ee_kykysi} \exfont \small Kykysi kohdata ihmisiä teki \emph{jo silloin} vaikutuksen ja ajattelin
(ja ajattelimme) että sinusta on tulossa aivan loistava pappi. (Araneum
Finnicum: kirkonkellari.fi)






\ex.\label{ee_poikkeusta} \exfont \small Liittymissopimukseen ei neuvoteltu poikkeusta, vaan Suomi hyväksyi
säännön \emph{jo silloin.} (FiPress: Keskisuomalainen)





\vspace{0.4cm} 

\noindent
Vaikuttaisi siis siltä, että juuri fokaalisen konstruktion mahdollisuus toteutua
venäjässä S1-asemassa on yksi tärkeimmistä tekijöistä, jotka selittävät venäjän S1-sijainnin 
yleisyyttä L7c-aineistossa. Suomessakin *silloin*-sanan anaforinen luonne
nostaa S1-sijainnin osuutta jonkin verran, mutta tämä tapahtuu ennemmin loppu- kuin
keskisijainnin kustannuksella.

Siirryn nyt tarkastelemaan L8b-ryhmiä eli suomen *nykyisin-/nykyään*-sanoja
ja venäjän *теперь*-sanaa. Kuten luvussa \ref{l8a-l8b-l8c} todettiin, aineistoryhmien
välillä on pieni epätasapaino siinä, että suomessa *теперь*-sanan vastineena on usein myös
yksinkertaisesti sana *nyt*, joka kuitenkin tässä tutkimuksessa asetettiin
ennemmin *сейчас*-sanan pariksi.

*Сейчас*- ja *теперь*-sanojen välillä on venäjässä tunnettu työnjako, jota kirjallisuudessa
on käsitelty laajasti [ks. esim. @krongauz1989, @grenoble98, @janko2001 ja @nesset2013here]. 
Kenties merkittävimmin aiheesta kirjoittanut Igor Melchuck [-@melchuck85, xx] erittelee *теперь*-sanalle
kaksi merkitystä, temporaalisen ja ei-temporaalisen. Temporaalisessa
merkityksessä теперь-sanaan liittyy tavallisesti kontrasti sen lokalisoiman
tapahtuman ja jonkin muun ajanjakson välillä; ei-temporaalisessa merkityksessä *теперь*
voidaan käsittää konjunktiona, jolla viestijä ilmaisee, että *теперь*-virkettä
edeltävä asia on käsitelty ja nyt on aika siirtyä seuraavaan ajatukseen.
Tutkimusaineistossa temporaalinen merkitys on edustettuna muun muassa esimerkissä \ref{ee_obuv}, ei-temporaalinen
puolestaan esimerkissä \ref{ee_umaltshivajut}:




\ex.\label{ee_obuv} \exfont \small Если ранее предприятие выпускало детскую обувь в низкой ценовой группе,
то теперь оно смогло предложить более высокое качество товара и занять
среднеценовую нишу. (Araneum Russicum: kotofey.ru)






\ex.\label{ee_umaltshivajut} \exfont \small А теперь я расскажу то, о чём умалчивают "продавцы." (Araneum Russicum:
adtimes.ru)





\vspace{0.4cm} 

\noindent
*Теперь*-sanan ei-temporaalinen, konjunktiomainen käyttötapa liittää sanan
luonnollisesti aivan lauseen alkuun. Voidaan myös todeta, että *теперь* on
perusluonteeltaan topikaalinen. Kuten Janko [vrt. -@janko2001, 253] huomauttaa,
fokuksen diskurssifunktiossa *теперь* ei esiinny lainkaan, ellei kyseessä
ole spesifisti kontrasti johonkin toiseen ajankohtaan. Tästä seuraa, että vaikka 
*тепреь*-sanan adverbisuus sinänsä antaisi syyn asettaa se keskisijaintiin, sen topikaaliset 
ominaisuudet -- ja toisaalta konjunktiomainen, merkityksen 2 mukainen käyttö --
aiheuttavat sen, että alkusijainti on itse asiassa yleisin. Eri sijaintien
suhteelliset osuudet *теперь*-sanalla ovatkin 
 55,20 %, 43,85 %
ja 0,95 %.

Myös suomen *nykyään/nykyisin*-sanoista voidaan todeta, että
ne  ovat ensinnäkin lähtökohtaisesti topikaalisia ja toiseksi myös niihin on ikään kuin
sisäänrakennettuna tietty kontrastiivisuus (nykyään -- aiemmin). Tässä ja
edellisessä luvussa havaittu suomen ja venäjän välinen perusero
näkyy kuitenkin mielenkiintoisella tavalla siinä, että suomessa L8b-ryhmä on alkusijainnissa
itse asiassa keskimääräistä harvinaisempi -- suomessa sijainnit jakautuvat 
suhteessa 11,27 %, 79,48 %
ja 9,25 %. *Теперь*-sanan ei-temporaalista merkitystä vaikuttaisi
sen sijaan suomessa usein vastaavan nyt-sana [@hakul95, xx; vrt. myös osio
\ref{morf-sama-keski} edellä], joka siis tutkimusaineistossa on mukana L8c-aineistoryhmän
osana. Tässäkään *nyt* ei kuitenkaan vaikuta positiivisesti S1-aseman
todennäköisyyteen, vaan myös L8c-ryhmässä
suomen S1-sijainnin todennäköisyys on alle keskiarvon (14,11 %).

Suomen L8b-aineiston alkusijaintitapauksista melko suuri osa edustaa esimerkin \ref{ee_nicki}
tavoin kontrastiivista konstruktiota:






\ex.\label{ee_nicki} \exfont \small 35-vuotias Nicki Philips oli ennen huippuluistelija, \emph{mutta}
nykyään hän painaa 127 kiloa eikä liiku yhtään. (Araneum Finnicum:
ruutu.fi)





\vspace{0.4cm} 

\noindent
Itse asiassa suomen L8b-aineiston S1-lauseista 27,06 % alkaa esimerkin \ref{ee_nicki}
tavoin *mutta*-sanalla. Loput
 159 S1-lausetta ilmentävät
esimerkiksi topikaalista konstruktiota kuten esimerkki \ref{ee_iltatyot} tai määrää painottavaa
konstruktiota kuten esimerkki \ref{ee_uniliitto}:




\ex.\label{ee_iltatyot} \exfont \small -\/- Tosin nykyään iltatyöt ja lapsenlapset vievät aikaa, joten
yhdistystoiminta ei ole enää niin suuressa osassa elämässäni, Koivu
toteaa. (Araneum Finnicum: hervannansanomat.fi)






\ex.\label{ee_uniliitto} \exfont \small Uniliitto ry:n ja Leiras Synthélabo Oy:n Taloustutkimuksella teettämän
tutkimuksen mukaan nykyisin lähes 35 prosenttia suomalaisista nukkuu
arkisin alle seitsemän tuntia, kun vuoden 1982 vertailutietojen mukaan
alle seitsemän tunnin yöunet riittivät vain noin seitsemälle prosentille
kansalaisista. (FiPress: Kaleva)





\vspace{0.4cm} 

\noindent
\todo[inline]{Huomioi nykyisin-vastineena tällaisissa tapauksissa ehkä todennäköisempi сегодня/ныне}




Samaan tapaan kuin vaikkapa esimerkissä \ref{ee_pakit} edellä, esimerkissä @ee_iltatyöt
kirjoittaja esittää väitteitä nimenomaan tietystä ajankaksosta, niin että
*nykyään*-ajanilmauksen referenttiä todella voidaan pitää lauseen varsinaisena
topiikkina. 

Kun kyse ei ole kontrastiivisesta konstruktiosta, on suomessa kuitenkin huomattavasti
tavallisempaa, että *nykyisin* tai *nykyään* sijoittuu keskisijaintiin.
Esimerkiksi virkkeen \ref{ee_uniliitto} tavoin moni suomen L8b-tapaus 
(kaikkiaan 75 kappaletta) sisältää määrän ilmaisemiseen liittyvän 
rakenteen *genetiivi* + *mukaan*, ja näistä 67:ssä
ajanilmaus osuu alkusijainnin sijasta keskisijaintiin, kuten esimerkeissä \ref{ee_opiaatit}
ja \ref{ee_siemenkauppa}:




\ex.\label{ee_opiaatit} \exfont \small Arvioiden mukaan noin 20 prosenttia kaikista opiaattien
ongelmakäyttäjistä saa nykyään korvaushoitoa. (FiPress: Hämeen Sanomat)






\ex.\label{ee_siemenkauppa} \exfont \small siemenkauppa asiantuntijoiden mukaan kourallinen suuryhtiöitä valvoo
nykyisin 30 prosenttia maailman siemenkaupasta. (FiPress: Kaleva)





\vspace{0.4cm} 

\noindent
Toinen kiinnostava tapausjoukko ovat esimerkkien \ref{ee_tietokone} ja \ref{ee_kotivakuutus} kaltaiset S3-lauseet:






\ex.\label{ee_tietokone} \exfont \small Moni tekee nykyään työnsä tietokoneen ääressä ja tietokoneen ympärillä
on kuivattavaa sähköistä pölyä. ( Araneum Finnicum:
purkkiarmeija.blogspot.fi)






\ex.\label{ee_kotivakuutus} \exfont \small Moni vuokranantaja vaatii nykyään kotivakuutuksen ottamista ehtona
vuokrasopimukselle. (Araneum Finnicum: nuortenelama.fi)





\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_tietokone} ja \ref{ee_kotivakuutus} kirjoittaja kuvaa jossain määrin esimerkin \ref{ee_iltatyot}
tapaisesti sitä aikaa, jota kirjoitushetkellä eletään. Kirjoittaja ei kuitenkaan käytä ajanilmausta lauseensa
lähtökohtana, vaan aloittaa esittelemällä lauseen agentin -- siinäkin tapauksessa, että se on niinkin
geneerinen kuin pronomini *moni*. Myös lauseenalkuista *nykyisin*-sanaa hyödyntävä strategia olisi 
mahdollinen, ja sitä edustaakin esimerkki \ref{ee_kaivos}:




\ex.\label{ee_kaivos} \exfont \small Kaivoksen tulo naapuriin ohjaisi nämä turistit muualle ja nykyisin aika
moni yritys elää ulkomaalaisista, sillä he ostavat enemmän
ohjelmapalveluita ja käyttävät ravintolapalveluita. (Araneum Finnicum:
adressit.com)





\vspace{0.4cm} 

\noindent
Kaikista suomen L8b-aineiston *moni*-pronominin sisältävistä 21
tapauksesta esimerkki \ref{ee_kaivos} on kuitenkin ainoa S1-sijaintia käyttävä.
Tämäkin kuvaa sitä, että suomessa keskisijainnin käyttö liittyy usein
yksinkertaisesti siihen, että kirjoittaja valitsee todennäköisemmin
keskisijaintia hyödyntävän viestintästrategian. Kaiken kaikkiaan, vaikka suomen
ja venäjän väliset L8b-aineistoissa havaittavat erot voidaan pitkälti jäljittää
venäjän *теперь*-sanaan liittyviin leksikaalisiin erityispiirteisiin, on
selitystä ainakin jossain määrin etsittävä myös siitä, että -- kuten aiemmin
tutkimuksessa on havaittu -- S1-konstruktioita hyödyntävät viestintästrategiat
eivät vain ole suomessa tavallisia. S1-konstruktioihin turvautumisen taustalla
on usein jokin keskisijainnin käyttöä rajoittava tekijä, kuten esimerkissä
\ref{ee_uniliitto} toinen ajanilmaus *arkisin*, joka käytännössä estää
*nykyisin*-sanan sijoittamisen samaan verbinjälkeiseen asemaan.


#### Muut semanttiset funktiot {#muutf-s2s3-kasv}

Kuvion 29  perusteella suomen keskisijainnin todennäköisyys
kasvaa venäjään nähden paitsi edellä laajasti käsitellyllä simultaanisella funktiolla,
myös sekventiaalisella ja sekventiaalis--duratiivisella funktiolla sekä jossain määrin teelisellä funktiolla.

Teelisten ilmausten (eli aineistoryhmän E1c, ks. \ref{e1a-e1b-e1c}) osalta voidaan
todeta, että suomen keskisijainnin venäjää suurempi todennäköisyys johtunee lähinnä
siitä, että alkusijainti on suomessa käytännössä mahdoton: kaikkiaan
 2 114 E1c-ilmauksesta S1-asemaan nimittäin 
sijoittuu ainoastaan 6 (0,28 %),
muun muassa esimerkki \ref{ee_tutkimusrahaa}.
Venäjässä esimerkin \ref{ee_podjomsprosa} kaltaiset tapaukset ovat kuitenkin jonkin verran tavallisempia,
sillä 1 209 venäjän E1c-tapauksesta alkusijaintiin osuu
 56 eli  4,63 prosenttia --
esimerkiksi virke \ref{ee_podjomsprosa}.




\ex.\label{ee_tutkimusrahaa} \exfont \small Kolmeksi vuodeksi hän sai myös HY:n myöntämää tutkimusrahaa. (Araneum
Finnicum: ylioppilaslehti.fi)






\ex.\label{ee_podjomsprosa} \exfont \small На пару лет это вызвало подъем спроса на загородном рынке. (Araneum
Russicum: fazenda.spb.ru)





\vspace{0.4cm} 

\noindent
Sekventiaalisista ilmauksista huomautettiin edellä luvussa \ref{sekv-s1}, että
niillä on suomessa venäjään nähden voimakkaampi S1-todennäköisyyttä kasvattava vaikutus.
Mielenkiintoista kyllä, samansuuntainen havainto voidaan tehdä keskisijainnin osalta:
vertaamalla kuvioita 15 ja  29,
voidaan todeta, että vaikutusten voimakkuus on pääpiirteissään samaa luokkaa
(hivenen voimakkaampi alkusijainnin tapauksessa). Havainnon taustalla olevia tekijöitä
valottaa kahdesta sekventiaalisesta ryhmästä suuremman, L6a:n (ks. osio \ref{l6a-l6b}),
lähempi tarkastelu: S4-sijainti on siinä poikkeuksellisen tavallinen, kuten taulukosta
 12 nähdään:


|   |S1    |S2/S3 |S4    |
|:--|:-----|:-----|:-----|
|fi |41,00 |17,06 |41,94 |
|ru |57,33 |17,52 |25,15 |

\noindent \headingfont \small \textbf{Taulukko 12}: Suomen ja venäjän L6a-ryhmän jakautuminen eri sijainteihin \normalfont \vspace{0.6cm} 

Taulukon 12 kolmannesta sarakkeesta havaitaan, että
venäjän L6a-ryhmän S4-osuus on noin 25,15
prosenttia, siinä  missä vastaava keskiarvo koko aineiston tasolla on 
ainoastaan 9,19 prosenttia. 
Suomen osalta taulukosta 12 havaittava S4-osuus
(41,94) on paljon lähempänä koko aineiston keskiarvoa
(35,52 %).

Mikä sitten on syynä sille, että venäjässä yllättävän suuri osa etenkin L6a-aineiston 
ilmauksista sijoittuu S4-asemaan, mutta suomessa suurin osa saa asemakseen S3:n?
Kuten taulukossa 12 havaittava venäjän merkillepantavan korkea
S4-osuus antaa olettaa, erojen taustalla voidaan osaksi nähdä L6a-aineiston suomen- ja
venäjänkielisten ilmausten välisiä merkitys- ja käyttöeroja, joita kuvaa
muun muassa esimerkki  \ref{ee_gagara}:




\ex.\label{ee_gagara} \exfont \small Василий Гагара совершил тот же путь почти через двести лет в два раза
быстрее. (Araneum Russicum: copticmasr.narod.ru)





\vspace{0.4cm} 

\noindent


Esimerkissä \ref{ee_gagara} ajanilmaukseen on liitetty *почти*-adverbi, mikä kertoo,
että venäjän L6a-ilmaukset lienevät jonkin verran laajempikäyttöisiä kuin
vastaavat suomenkieliset ilmaukset. Suomenkielisestä aineistossa ei esimerkiksi
ole lainkaan tapauksia, jossa ajanilmausta määrittäisi jokin sanoista *melkein*, *lähes*
tai *miltei*, kun taas venäjässä esimerkin \ref{ee_gagara} kaltaisia tapauksia on kaikkiaan
 11.
Vielä selvemmin erot ilmausten leksikaalisissa ominaisuuksissa tulevat esille,
jos tarkastellaan presuppositionaalisten *jo/ужe* -adverbien esiintymistä
L6a-ilmausten yhteydessä. Koko venäjän L6a-aineistossa *уже*-sana edeltää
ajanilmausta 187 kertaa 
(13,71), kun
taas suomen L6a-aineistossa *jo*-sana edeltää ajanilmausta ainoastaan
 5 kertaa 
 (1,18 %).
Kummastakin kielestä voidaan tähän liittyen antaa seuraavat esimerkit:




\ex.\label{ee_yesterday} \exfont \small Kanavat monipuolistuvat kaiken aikaa ja monet jutut ovat ”yesterday ’ s
news” jo tunnin päästä. (Araneum Finnicum: teematonta.wordpress.com)






\ex.\label{ee_bolgarii} \exfont \small Руководство Болгарии одобрила эти рекомендации уже через пять дней.
(Araneum Russicum: rueconomics.ru)






\ex.\label{ee_summu} \exfont \small Вы получите у нас нужную сумму уже через несколько дней с момента
обращения. (Araneum Russicum: de4u.ru)





\vspace{0.4cm} 

\noindent
Kaiken kaikkiaan sekventiaalisten ilmausten tapauksessa vaikuttaisi 
siis siltä, että suomen keskisijainnin korostuminen venäjään nähden 
ei niinkään kerro kielten välisistä syntaktisista tai informaatiorakenteen
tason eroista, vaan ennen muuta tässä käsiteltävistä konkreettisista
ilmauksista. Entä sitten sekventiaalis--duratiiviset ilmaukset,
jotka kuvion 29 perusteella  näyttäisivät
oikeastaan kasvattavan suomen keskisijainnin todennäköisyyttä enemmän kuin
mikään muu semanttinen funktio (joskin tähän havaintoon myös liittyy
esimerkiksi simultaanisia funktioita enemmän epävarmuutta)?

Toisin kuin sekventiaalisten, sekventiaalis--duratiivisten ilmausten huomattiin
edellä pienentävän suomen S1-todennäköisyyttä verrattuna venäjään. 
Luvussa \ref{sekventiaalis-duratiivinen-ja-duratiivinen-funktio} havaittiin,
että myös sekventiaalis--duratiivisilla funktioilla ja etenkin L9d-ryhmällä
suomen ja venäjän ilmaukset eroavat toisistaan jonkin verran merkityksensä
ja käyttöalansa puolesta: venäjän L9d-ilmauksia (kuten *c 1990 года*) voidaan 
käyttää *alkaa tehdä* -rakenteissa ja niillä voidaan myös viitata yksittäisiin
tapahtumiin toisin kuin suomen L9d-ilmauksilla. Nämä merkityserot samoin
kuin ilmausten fokaalinen ja johdantokonstruktioon liittyvä käyttö heijastuvat
siihen, että venäjässä S1-sijainnin osuus on suhteessa paljon suurempi kuin 
suomessa. 

Suomen sekventiaalis--duratiivisilla ilmauksilla S1 on lopulta venäjään verrattuna 
niin harvinainen sijainti (suomessa yhteensä vain
 154
tapausta kaikkiaan 
 4 108:sta;
 venäjässä
 832
 tapausta 
 1 868:sta),
että tilastollisen mallin mukaan 
kielen merkitseminen suomeksi nostaa niin keskisijainnin kuin loppusijainnin
todennäköisyyttä. Tutkin seuraavassa tarkemmin suomen ja venäjän L9d-ryhmän
keskisijaintitapauksia selvittääkseni, missä määrin venäjässä alkusijainnilla
toteutettavia rakenteita on suomessa mahdollisesti toteutettu keskisijainnilla.

Suurimmaksi osaksi suomen ja venäjän sekventiaalis--duratiiviset
keskisijaintitapaukset näyttäisivät  muistuttavan toisiaan. Esimerkiksi
virkkeissä \ref{ee_judo} -- \ref{ee_moko}
on kaikissa kyse melko tyypillisestä alatopiikkikonstruktiosta, jossa
ajanilmaus kuitenkin osuu alkusijainnin asemesta keskisijaintiin:




\ex.\label{ee_judo} \exfont \small Сидоркевич с 1993 года возглавляется федерацию дзюдо Санкт-Петербурга и
является вице-президентом Федерации дзюдо России( ФДР) и двукратным
чемпионом мира по самбо. (Araneum Russicum: zagolovki.ru)






\ex.\label{ee_progr} \exfont \small Компания с 1992 года занимается разработкой прогрессивных методов
обучения детей и молодежи, специализируется в разработке и реализации
больших образовательных проектов, в наборе и подготовке учащихся и
учителей для этих проектов. (Araneum Russicum: isra.com)






\ex.\label{ee_bewe} \exfont \small Bewe on luovuttanut vuodesta 1993 lähtien vuosittain Käpylän
selkäydinvammaisia kuntouttavalle sairaalalle aina maaleista kertyneet
tulot ja näin myös tapahtuu juhlaottelussa. (Araneum Finnicum:
helsinginuutiset.fi)






\ex.\label{ee_moko} \exfont \small Moko on myynyt vuodesta 1991 saakka kaikenlaista sisustustavaraa
säilytyslaatikoista julisteisiin. (Araneum Finnicum: nyt.fi)





\vspace{0.4cm} 

\noindent
Kaikissa tässä luetelluissa esimerkeissä on jokin selkeä, diskurssistatukseltaan aktiivinen
koko tekstin tason topiikki, johon viitataan sanoilla *Сидоркевич*, *Компания*, *Bewe* ja *Moko*
ja josta kerrotaan jokin uusi segmentti. Merkillepantavaa kyllä, suomen L9d-aineiston keskisijaintiin
vaikuttaisi kuitenkin kuuluvan venäjää enemmän myös tapauksia, joissa ei suinkaan ole kyse viittaamisesta
johonkin jo muodostettuun diskurssitopiikkiin vaan ennemminkin uuden toimijan esittelemisestä, kuten
seuraavissa esimerkeissä:




\ex.\label{ee_mahdollisuus} \exfont \small Etelä-Intiassa Chennain kaupungissa toimiva The Children’s Garden School
Society -järjestö on vuodesta 1937 lähtien antanut köyhistä oloista
tuleville lapsille, orvoille ja katulapsille mahdollisuuden
koulunkäyntiin. (Araneum Finnicum: yhteisetlapsemme.fi)






\ex.\label{ee_antonpaar} \exfont \small Anton Paar on vuodesta 1922 tuottanut korkealaatuisia analyyttisiä
mittalaitteita teollisuudelle ja yliopistoi lle sekä muille
oppilaitoksille. (Araneum Finnicum: nayta.monster.fi)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_mahdollisuus} ja \ref{ee_antonpaar} voidaan analysoida osiossa \ref{l1-erot-kesk}
esitellyn S3-johdantokonstruktion edustajiksi siinä mielessä, että ne
todella esittelevät tekstin diskurssitopiikin ja sijaitsevat koko tekstin alussa.
Erityisen selvästi tämä näkyy esimerkin \ref{ee_antonpaar} laajemmassa 
kontekstissa[^apkont], joka paljastaa kyseessä olevan työpaikkailmoituksen:

[^apkont]: Kirjoitushetkellä saatavilla ainoastaan Araneum-korpuksen kautta; alkuperäine url http://nayta.monster.fi/GetJob.aspx?JobID=135904546&WT.mc\_n=CRMFIILTALOUS

\begin{quote}%
Service Engineer

Anton Paar on vuodesta 1922 tuottanut korkealaatuisia analyyttisiä
mittalaitteita teollisuudelle ja yliopistoille sekä muille
oppilaitoksille. Anton Paar on globaali markkinajohtaja tiheys-,
konsentraatio-, reologia- ja CO2-mittauksen alueilla. Anton Paar pyrkii
jatkuvasti löytämään uusia ratkaisuja sekä kehittämään nykyisiä
instrumentteja vastatakseen tulevaisuuden vaatimuksiin.

-\/-\/-

Kasvustrategiamme mukaisesti Anton Paar Nordic kehittää ja laajentaa
toimintojaan Suomessa. Etsimme nyt huolto-osaajaa Kaakkois- ja
Itä-Suomeen.
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkissä \ref{ee_mahdollisuus} ei jää epäselvyyttä siitä, mistä koko teksti 
kertoo. Ajanilmauksen sisältämän virkkeen tehtävänä on nimenomaan esitellä
Anton Paar -yritys, ja tämä tehdään puristamalla yrityksen koko historia yhteen
lauseeseen. Myös esimerkin \ref{ee_mahdollisuus} laajempi konteksti osoittaa,
että esimerkkivirkkeessä esiteltävä The Children's Garden School Society -järjestö
on koko tekstin varsinainen aihe, joka esitellään samaan tapaan kertomalla
lyhyesti järjestön toiminnan historia:

\begin{quote}%
Terveys ja koulutus ovat kehitysmaiden lasten tulevaisuuden kannalta
keskeisiä tekijöitä. Etelä-Intiassa Chennain kaupungissa toimiva The
Children’s Garden School Society –järjestö on vuodesta 1937 lähtien
antanut köyhistä oloista tuleville lapsille, orvoille ja katulapsille
mahdollisuuden koulunkäyntiin. Järjestön kouluissa on noin 4 000 lasta
ja nuorta.

Järjestön perustajat, kasvatustieteiden tohtori N.V. Sharma ja rouva
Ellen Sharma, pyrkivät alusta pitäen yhdistämään koulussaan länsimaiset
kasvatusmenetelmät itämaiseen viisauteen.

http://www.yhteisetlapsemme.fi/kummi-ja-kehitysyhteistyo/intia/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
S3-johdantokonstruktioille on leimallista, että niissä keskitytään nimenomaan
jonkin toimijan esittelemiseen siinä, missä S1-johdantokonstruktioissa
esittelyn kohteena on usein jokin abstraktimpi kokonaisuus tai tapahtuma (vrt.
mm. esimerkki \ref{ee_livija} edellä). Tästä seuraa, että esimerkkien \ref{ee_mahdollisuus}
ja \ref{ee_antonpaar} kaltaisia rakenteita tavataan myös tekstin loppupuolella, kun 
jokin tekstin kannalta keskeinen toimija -- esimerkiksi sen kirjoittaja -- esitellään
tarkemmin lukijalle. Esimerkki \ref{ee_magix} on tyypillinen tällainen tapaus:




\ex.\label{ee_magix} \exfont \small Germanistiikan ja filosofian MA Roland Ziegler on kirjoittanut vuodesta
1999 alkaen MAGIX:in käsikirjoja. (Araneum Finnicum: magix.com)





\vspace{0.4cm} 

\noindent
Esimerkin laajempi konteksti osoittaa, että kyseessä on kirjaesittely, jonka lopussa on erillinen
osio otsikolla *Kirjoittajista*. Roland Ziegler on siis tietyllä tavalla jo lukijalle 
tuttu, mutta hänet ikään kuin erikseen esitellään vielä tekstin lopuksi S3-johdantokonstruktiota
hyödyntäen:

\begin{quote}%
Kirjoittajista

Germanistiikan ja filosofian MA Roland Ziegler on kirjoittanut vuodesta
1999 alkaen MAGIX:in käsikirjoja. Hän on kokenut ohjelmiston ja sen
monipuolisten käyttömahdollisuuksien tuntija.

http://www.magix.com/fi/kirjat/video-editing-made-simple-with-movie-edit-pro-kirja/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkkien \ref{ee_mahdollisuus} -- \ref{ee_magix} kaltaisia tapauksia on jonkin verran myös
venäjänkielisessä aineistossa, muun muassa seuraava (esimerkki esitetty suoraan laajemmassa
kontekstissaan):




\ex.\label{ee_tantsevalnyj} \exfont \small Российский Танцевальный Союз с 1996 года проводит в Кремле крупнейшие
международные профессиональные турниры WDC -\/- Кубки мира, Чемпионаты
Однако в этот раз танцоры выступили не как участники соревнования, а как
артисты. Мира и Европы. (Araneum Russicum: cheb.mk.ru)





\vspace{0.4cm} 

\noindent


Esimerkki \ref{ee_tantsevalnyj} on kuitenkin siinä mielessä erilainen kuin edellä käsitellyt suomalaiset
esimerkit, että tässä esitelty Venäjän tanssiliitto ei ole tekstin kannalta keskeinen toimija, vaan
keskeistä on ennemminkin ajatus siitä, että aikaisemminkin on järjestetty tanssikilpailuja, mutta
tällä kertaa kyseessä on ennemminkin ei-kilpailullinen näytösohjelma, mikä käy ilmi esimerkin 
jälkimmäisestä virkkeestä. Kaiken kaikkiaan tässä analysoituja suomenkielisiä esimerkkejä
lähempänä onkin muun muassa edellä luvussa \ref{sekventiaalis-duratiivinen-ja-duratiivinen-funktio}
käsitelty esimerkki \ref{ee_columbia}, jossa esimerkkien \ref{ee_mahdollisuus} ja \ref{ee_antonpaar} tavoin
nimenomaan esiteltiin järjestöjen ja yritysten kaltainen toimija (esimerkin \ref{ee_columbia} tapauksessa
oppilaitos) sen toiminnan historian kautta. Esimerkki on annettu tässä uudestaan numerolla 
\ref{ee_columbia2}:





\ex.\label{ee_columbia2} \exfont \small С 1979 года с помощью Уникальной Системы Всестороннего Образования
Columbia International College готовит студентов стать ответственными
гражданами и в то же самое время поступить в лучшие университеты мира.
(Araneum Russicum: bec-dnepr.com)





\vspace{0.4cm} 

\noindent
Ylipäätään S3-johdantokonstruktion yleisyydestä suomessa verrattuna
venäjään kertoo se, että johdantokonstruktiolle tyypillisiä pitkiä subjekteja 
(vrt. osio \ref{tilastollinen-malli-johdantokonstruktion-sijaintien-kartoittamiseksi} edellä) on 
suomen L9d-ryhmän keskisijaintitapauksista peräti 44,44 prosenttia --
venäjässä vastaava luku on vain 25,36 prosenttia. 
Voidaankin sanoa, että ainakin johdantokonstruktioiden osalta suomen keskisijaintitapauksista
todella on löydettävissä sellaisia, jotka venäjässä olisivat voineet tulla toteutetuiksi
myös S1-sijainnissa. Loppusijainnin suhteen kysymystä tarkastellaan lähemmin osiossa
\ref{muutf-s4}.


## Keskisijainnin sisäiset erot {#kesk-sis}

    KESKEN!




Luvussa \ref{tilastollinenmalli} esitettiin, että kaiken kaikkiaan aineistosta
tehtävien tilastollisten päätelmien kannalta on järkevämpää yhdistää S2- ja
S3-sijainnit yhdeksi *keskisijainniksi*, koska suomen S2 ja venäjän S3 ovat
käytöltään melko harvinaisia. S2- ja S3-sijainteihin liittyvät konstruktiot
eivät kuitenkaan kummassakaan kielessä ole täysin samoja, joten on paikallaan
luoda varsinaisen tilastollisen mallin ulkopuolella katsaus siihen, minkälainen
kummassakin tarkasteltavassa kielessä on keskisijainnin sisäinen rakenne.

Luvussa \ref{sij-yleisk} esitettiin, että kaikista suomenkielisen aineiston
ajanilmauksista S2-asemaan sijoittuu 3,02 %.
Suomen S2-asemaan liittyy tietynasteinen ajatus kieliopinvastaisuudesta, niin
että esimerkiksi kirjoitusoppaissa [vrt. @sorjanen2004, 49] S2-sijoittunutta
ajanilmausta pidetään ei-toivottuna ilmiönä. Tietyissä tapauksissa S2 vaikuttaa
kuitenkin paitsi hyväksytyltä, myös konstruktion toteuttaman viestintätehtävän
kannalta välttämättömältä tai vähintäänkin parhaana pidetyltä sijainnilta.
Tällaiset tapaukset liittyvät ennen kaikkea
adverbisiin ajanilmauksiin, kuten seuraavassa esimerkissä:




\ex.\label{ee_maalinteko} \exfont \small Ja he \emph{harvoin} osaavat sitä hiljaista tietoaan maalinteosta
välittää eteenpäin. (Araneum Finnicum: karhuherra.jatkoaika.com)





\vspace{0.4cm} 

\noindent
...sivulausetyypistä....

Jossain määrin yllättävästikin luvussa \ref{sij-yleisk} havaittiin, että
kaikista tutkimusaineiston venäjänkielisistä ajanilmauksista vain 
2,24 % sijoittuu S3-asemaan. Koska venäjän S3-sijoittuneet
adverbit ovat niin harvinaisia aineistossa, on kysyttävä, liittyykö niihin
samalla tavoin kuin suomen S2-ilmauksiin ajatusta kieliopinvastaisuudesta. Tätä
ovat itse asiassa adverbien osalta testanneet Elena Kallestinova ja Roumyana Slabakova
[-@kallestinova08], jotka tutkivat kyselyaineistolla, missä määrin verbinetiset
ja -jälkeiset adverbit ovat venäjänkielisten puhujien mielestä kieliopillisesti
hyväksyttäviä. Kallestinova ja Slabakova (mts. 200) mainitsevat, että ainakin
perinteisen käsityksen mukaan verbinetistä asemaa (tässä S2) todella on pidetty
luontevimpana ja verbinjälkeistä (S3) jopa epäkieliopillisena. Oletusta voidaan
tarkentaa toteamalla, että etenkin tavan adverbeillä S2-asemaa on pidetty koko
lailla ainoana mahdollisena  [@yokoyama1986, 290].

Kallestinovan ja Slabakovan testin perusteella oletus S3-aseman
ei-kieliopillisuudesta ei kuitenkaan ole erityisen pitävä. Testiin kuului niin
SVO- kuin OVS-lauseita sekä eri adverbityypeistä sekä tavan että -- tämän
tutkimuksen kannalta relevantisti -- taajuuden adverbeja. Testissä ilmausten
kieliopillisuutta pyydettiin arvioimaan asteikolla yhdestä viiteen. Tällä
asteikolla mitattuna S2-sijainnin kieliopillisuus taajuutta ilmaisevilla
SVO-lauseiden ajanilmauksilla oli 4,9 ja S3-sijainnin 2,9 (mts: 206). Esimerkki
Kallestinovan ja Slabakovan aineistossa käytetyistä lauseista on annettu tässä
numerolla \ref{ee_kall}:





\exg.  Оля часто готовила суп.
\label{ee_kall}  
 O. usein valmistaa-PRET keitto 
\




\vspace{0.4cm} 

\noindent
Voidaan siis todeta, että venäjän S3-asemaan liittyy ajatus tietynasteisesta
kieliopinvastaisuudesta (ilmauksia ei pidetty täysin hyväksyttävinä), mutta
mahdottomia tällaiset sijoittumiset eivät ole. Mahdollisesti 
S2-sijainnin vetovoima on vain
niin voimakas, että S3-tapaukset jäävät harvinaisiksi, vaikkei niiden käytöllä
mitään varsinaista kieliopillista estettä olisikaan. 


sivulauseista...

\todo[inline]{KUn-lauseiden S2-taipumuksesta ks. Shore 55}

Venäjän S3 on todennäköinen seuraavissa tapauksissa...

часто ks. https://link.springer.com/content/pdf/10.1007%2Fs11185-008-9030-7.pdf


\todo[inline]{


Esimerkkien \ref{ee_livija} -- \ref{ee_magaziny} avulla analysoidusta rakenteesta on vielä
todettava, että -- mielenkiintoista kyllä -- se vaikuttaisi venäjässä toimivan
myös S3-sijoitetulla ajanilmauksella, kuten virkkeessä \ref{ee_obamakomment}:





\exg.  Президент США Барак Обама прокомментировал \emph{во вторник}
высказывания президента России Владимира Путина относительно Украины.
(RuPress: РИА Новости 2014)
\label{ee_obamakomment}  
 Presidentti USA-GEN B. O. kommentoida-PRET tiistaina lausunnot presidentti-GEN Venäjä-GEN V. P. koskien Ukraina-GEN
\




\vspace{0.4cm} 

\noindent
}

\todo[inline]{venäjässä togda ehkä tavallista yleisemmin S3?}


Ajanilmaukset ja loppusijainti
=============================



Tarkasteltaessa keskisijaintia kävi ilmi, että suurin osa ajanilmauksen
sijainnin kannalta selitysvoimaisista konstruktioista esiteltiin jo
alkusijainnin yhteydessä luvussa \ref{ajanilmaukset-ja-alkusijainti}.
Keskisijainti vaikuttaisi monessa mielessä olevan melko neutraali -- monin
paikoin siinä havaittavat erot ovat tulkittavissa seurauksiksi
venäjän taipumuksesta käyttää alkusijaintia. Lähes peilikuvamaisesti voidaan
kuitenkin myös havaita, että suomessa vaikuttaisi olevan voimakkaampi taipumus
käyttää loppusijaintia kuin venäjässä.

## Samansuuntaisia S4-vaikutuksia {#s4-samans}

Luvussa \ref{sij-yleisk} esitettiin, että
loppusijaintiin sijoittuu kaikkiaan 
 86 737 tutkimusaineiston
suomenkielisestä lauseesta 
 30 811
eli 35,52 prosenttia  
ja kaikkiaan 73 302 tutkimusaineiston 
venäjänkielisestä lauseesta
 6 734 
 eli 9,19
prosenttia. Havaittavat erot muistuttavat alkusijainnin kohdalla havaittuja,
joskin tässä tapauksessa venäjä on kieli, jossa tarkastelun kohteena oleva
sijainti on harvinainen.


Kuten alku- ja keskisijainnin tapauksessa, lähden myös loppusijainnin kohdalla liikkeelle koko
aineiston tasolla havaittavista vaikutuksista. Semanttisen funktion osalta
koko aineiston tasolla havaittavat yleiset vaikutukset on 
kerätty kuvioon 43.


![\noindent \headingfont \small \textbf{Kuvio 43}: Semanttisten funktioiden vaikutus loppusijaintiin \normalfont](figure/functs4-1.pdf)

Kuvio 43 osoittaa, että teelisellä semanttisella
funktiolla on voimakkain koko aineiston tasolla S4-sijaintia kasvattava
vaikutus. Ymmärrettävää kyllä, presuppositionaalinen funktio taas näyttäisi
kielestä riippumatta pienentävän loppusijainnin todennäköisyyttä. Lisäksi
vaikuttaisi siltä, että ainakin tapaa ilmaiseva, sekventiaalis--duratiivinen sekä
mahdollisesti duratiivinen funktio
kasvattavat ja simultaaninen, sekventiaalinen ja resultatiiviset funktiot
vähentävät loppusijainnin todennäköisyyttä
koko aineiston tasolla. Jos kuviota 43 kuitenkin vertaa
kuvioon 47 alempana, on todettava, että ainoastaan
teelisen funktion vaikutus on käytännössä kielestä riippumaton.

Morfologinen rakenne osoittaa loppusijainnin osalta yhden selvästi koko
aineistolle yhteisen tendenssin, joka näkyy kuviossa 44:
muut kuin adverbiset ilmaukset ovat lähtökohtaisesti todennäköisempiä S4-sijoittujia.


![\noindent \headingfont \small \textbf{Kuvio 44}: Morfologisen rakenteen vaikutus loppusijaintiin \normalfont](figure/morphs4-1.pdf)

Kuviosta 44 tehtävä havainto voidaan vahvistaa
kielen ja morfologisen rakenteen välistä interaktiota esittävästä kuviosta
 48, jonka mukaan nominaalisuudella ja kielellä
kyllä on yhteisvaikutus (suomessa nominaalisuuden S4-todennäköisyyttä
kasvattava vaikutus on venäjää pienempi), mutta se ei ole kovin suuri.
Nominaalisuuden vaikutuksiin koko aineiston tasolla pureudutaan
tarkemmin alaluvussa \ref{loppus_np}.


Kuten tähänkin asti, positionaalisuuden vaikutukset ovat melko pieniä. Verrattuna alku- ja keskisijaintiin
on kuitenkin todettava, että loppusijainnissa on muita sijainteja vähemmän havaittavissa
koko aineistolle yhteistä tendenssiä, mikä näkyy myös kuviosta 45.


![\noindent \headingfont \small \textbf{Kuvio 45}: Positionaalisuuden vaikutus loppusijaintiin \normalfont](figure/poss4-1.pdf)


Myös numeraalin läsnäololla koko aineiston tasolla havaittavat vaikutukset
ovat pieniä, kuten seuraava kuvio osoittaa:


![\noindent \headingfont \small \textbf{Kuvio 46}: Numeraalin läsnäolon vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/isnumerics4-1.pdf)

Tarkastelen nyt lähemmin yllä esitetyn yleiskatsauksen perusteella selvimmiltä näyttäviä
koko aineiston tasolla havaittavia vaikutuksia. Lähden liikkeelle nominaalisuuden S4-
vaikutuksesta, minkä jälkeen tarkastelen lyhyesti semanttisten funktioiden tasolla
havaittavia koko aineiston laajuisia vaikutuksia.

### Nominaalisuus ja loppusijainti {#loppus_np}

Nominaalisuuden kielestä riippumattomalle linkittymiselle loppusijaintiin
voidaan ainakin lähtökohtaisesti esittää selvä informaatiorakenteellinen selitys, joka liittyy
siihen, että lauseen loppu on niin suomessa, venäjässä kuin yleisemmälläkin tasolla 
oletusasema fokuksen diskurssifunktiota toteuttaville elementeille [@tobecited]. 
Lisäksi voidaan väittää, että monille adverbeille
fokus on epätyypillinen tai jopa lähtökohtaisesti mahdoton diskurssifunktio, mistä
seuraa, että ne esiintyvät harvemmin lauseen lopussa. 

Esimerkit \ref{ee_hessu} -- \ref{ee_vsredu} kuvaavat tyypillisiä suomalaisia ja venäläisiä
fokaalisia ajanilmauksia:




\ex.\label{ee_hessu} \exfont \small Aikaisempien kesien tapaan Hessu hakee mökkeilijät Reposalmen rannasta
tiistaisin, torstaisin ja perjantaisin klo 10. (Araneum Finnicum:
mieto.fi)






\ex.\label{ee_akateeeminen} \exfont \small Akateeminen Kirjakauppa on jakanut suosituimman esikoisteoksen palkinnon
vuodesta 1990 lähtien. (Araneum Finnicum: stockmanngroup.fi)






\ex.\label{ee_valtuusto} \exfont \small Mikkelin kaupungin valtuusto hyväksyi esityksen järjestyssäännöstä
kuudes toukokuuta vuonna 1996. (Fipress: Länsi -Savo)






\ex.\label{ee_grazhdanstvo} \exfont \small Я получил российское гражданство в 1998 году. (Araneum Russicum:
korrespondent.net)






\ex.\label{ee_rimljane} \exfont \small Богатые римляне посещали баню два раза в день. (Araneum Russicum:
forest-stroy.ru)






\ex.\label{ee_vsredu} \exfont \small Объединенный ВС начал работу в среду. (Araneum Russicum: rapsinews.ru)





\vspace{0.4cm} 

\noindent
Luvussa \ref{diskurssifunktio} esitetyn Lambrechtin [-@lambrecht1996, 213]
määritelmän mukaan fokus on se osa propositiota, jonka suhteen pragmaattinen
väittämä eroaa pragmaattisesta olettamasta. Otan yllä esitetystä fokaalisten
aineistoesimerkkien listasta lähempään tarkasteluun virkkeet \ref{ee_hessu} ja
\ref{ee_grazhdanstvo}. Niihin liittyvät pragmaattiset olettamat ja väittämät on
esitetty taulukossa 13:


--------------------------------------------------------------------------------------------
lause                         olettama                       väittämä                       
----------------------------- ------------------------------ -------------------------------
*Aikaisempien kesien tapaan   Lukija tietää erisnimen        Mökkiläisten hakeminen         
Hessu hakee mökkeilijät       *Hessu* referentin. Hessulla   tapahtuu tiistaisin,           
Reposalmen rannasta           on tapana kuljettaa            torstaisin ja perjantaisin     
tiistaisin, torstaisin ja     mökkeilijöitä                  kello 10. Mökkiläiset haetaan  
perjantaisin klo 10.*                                        Reposalmen rannasta            

*Я получил российское         lukija tietää pronominin *Я*   pronominin *я* referentistä    
гражданство в 1998 году.*     referentin. pronominin *Я*     tuli venäjän kansalainen       
                              referentti on venäjän          vuonna 1998                    
                              kansalainen, mutta ei ole                                     
                              ollut aina, vaan on joskus                                    
                              saanut kansalaisuuden                                         
--------------------------------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 13}: Esimerkkien \ref{ee_hessu} ja \ref{ee_grazhdanstvo} pragmaattiset olettamat ja väittämät \normalfont \vspace{0.6cm}

Kuten taulukosta nähdään, niin esimerkissä \ref{ee_hessu} kuin esimerkissä \ref{ee_grazhdanstvo} 
ajanilmauksen referentti on osa pragmaattista väittämää. Vastaavalla tavalla voitaisiin
analysoida myös esimerkit \ref{ee_akateeeminen}, \ref{ee_valtuusto}, \ref{ee_rimljane} ja \ref{ee_vsredu}.
Kaikissa näissä tapauksissa ajanilmauksen voisi nähdä osana *fokaalista S4-konstruktiota*,
jonka esitän tarkemmin seuraavassa matriisissa:




\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[frame,pad=4pt,rule=0.1pt]{

ds   act+ \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{


syn \[ cat & V \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]


\vspace{1.5em}
}}}\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]



prag \[ df & Foc \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}




 \noindent \headingfont \small \textbf{Matriisi 20}: Fokaalinen konstruktio \normalfont \vspace{0.6cm}

\todo[inline]{Jotenkin tuo esille kakkosluvussa se ajatus, että 
silloin, kun ei ole mielekästä määritellä, diskurssistatusta
ei merkitä ja jos taas df ei ole oleellinen, sitä ei merkitä.
Kyse kun on kuitenkin referenttien välisistä suhteista ym.
vai??

venäjän fokaaliset konstr. alt.loc - arvolla?

}

\todo[inline]{VSO-huomautus}

Olennaista matriisissa 20 on se, että se esittää koko ajanilmausta
edeltävän proposition olevan diskurssistatukseltaan aktiivinen.
Taulukon  esimerkkien osalta voitaisiin yleistäen todeta,
että propositiot [Hessulla on tapana kuljettaa mökkiläisiä]
ja [puhuja on joskus saanut venäjän kansalaisuuden] ovat vastaanottajan tiedossa, ja 
ajanilmauksen tehtävänä on täydentää tätä tietoa ajallisella informaatiolla.

Vaikka kaikki edellä esitetyt esimerkit ovat olleet nominaalisia,
myös adverbit voivat esiintyä osana fokaalista konstruktiota. Tästä ovat
esimerkkejä virkkeet \ref{ee_saratov} ja \ref{ee_lautakunta}, jotka kumpikin edustavat
aineistoryhmää F3c:




\ex.\label{ee_saratov} \exfont \small Саратов, к сожалению, радует своих жителей чрезвычайно редко. (Araneum
Russicum: fn-volga.ru)






\ex.\label{ee_lautakunta} \exfont \small Erityisesti alkuaikoina lautakunta muutti ministeriön päätöstä erittäin
harvoin. (FiPress: Helsingin Sanomat)





\vspace{0.4cm} 

\noindent
\todo[inline]{Mieti, pitäisikö enemmän puhua "suomen ja venäjän" / "venäjän" fokaalisesta konstruktiosta ym.}




Syy siihen, miksi adverbit kaiken kaikkiaan ovat nominaalisia ajanilmauksia
epätodennäköisempiä sijoittumaan S4-asemaan, on kuitenkin siinä, että tietyt
adverbiset ryhmät ovat luonteeltaan lähtökohtaisesti ei--fokaalisia. 
Kuten kuvio 43 osoittaa,
selkein tällainen ryhmä ovat presuppositionaaliset adverbit *jo/уже* ja
*vielä/ещё* eli aineistoryhmät P1 ja P2. Esimerkiksi P1-ryhmän tapauksessa
S4-sijaintiin osuu
 2 124 suomenkielisestä
tapauksesta 60
ja 2 534 
venäjänkielisestä tapauksesta  tasan 2.

Muita lähes täysin S4-sijaintia välttäviä adverbisiä aineistoryhmiä
ovat molemmissa kielissä ryhmät L7c -- jonka alhaiseen S4-todennäköisyyteen
pureuduttiin edellä osiossa \ref{l7l8-erot-kesk} -- sekä F3e (ks. osio
\ref{f3a-f3b-f3c-f3d-f3e}). Venäjässä, jossa S4-sijainti on kaikkiaan
harvinaisempi, on yhteensä 8 sellaista
adverbista aineistoryhmää, joissa S4-sijainnin osuus on alle kaksi prosenttia.
Jo mainittujen lisäksi näitä ovat E5b (*вдруг*), F3a (*часто*), F3b (*всегда*)
ja F3d (*иногда*). Sen, että suomessa näiden ryhmien S4-osuudet ovat selkeästi
suurempia, voi nähdä viittaavan a) siihen, että venäjässä juuri fokaalinen konstruktio
on selkein syy käyttää S4-asemaa, mutta suomessa S4-konstruktioiden kirjo on laajempi
ja b) siihen, että adverbien taipumus painottua juuri yhteen tiettyyn asemaan
(venäjässä S2, suomessa S3) on venäjässä voimakkaampi.
Jälkimmäisestä tendenssistä on esimerkkinä  virke
\ref{ee_tietoyht}, joka sisältää objektin jälkeisen *usein*-adverbin:




\ex.\label{ee_tietoyht} \exfont \small Poliitikot pitävät tietoyhteiskuntaa liian usein vain tekniikka-asiana.
(FiPress: Turun Sanomat)





\vspace{0.4cm} 

\noindent
Informaatiorakenteen kannalta esimerkistä \ref{ee_tietoyht} havaitaan, että niin
poliitikot kuin heidän suhtautumisensa tietoyhteiskuntaan voidaan nähdä
asioina, jotka oletettavasti jo ovat lukijan mielessä ja siten osa
pragmaattista olettamaa. Se, että suhtautumisena on juuri tietoyhteiskunnan
pitäminen tekniikka-asiana, on pragmaattisen väittämän osa-aluetta ja siten on
loogista, että siihen pyritään viittaamaan aivan lauseen lopussa. 
Kuten edellä luvussa \ref{s2s3-pien} todettiin, verbinjälkeinen asema on
kuitenkin suomessa altis esimerkiksi kontrastiivisille tulkinnoille, jos siihen
kuuluu useampia konstituentteja [@manninen2003, 226]. Jos *liian usein*
-adverbiaalilla halutaan vain ilmaista proposition [poliitikot pitävät
tietoyhteiskuntaa tekniikka-asiana] taajuus, on sille selkein sijainti
lauseessa koko pragmaattiseen väittämään kuuluvan aineksen jälkeen, kuten
esimerkissä \ref{ee_tietoyht}. Tällaista ajanilmauksen sisältävää
konstruktiota, jossa verbin valenssiin kuuluu useampi argumentti,
voitaisiin kuvata seuraavan kaltaisella matriisilla:




\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[frame,pad=4pt,rule=0.1pt]{

ds   act+ \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{


syn \[ cat & V \\
val & \#1,\#2 \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

\#1   \\[5pt]


gf obj \\[5pt]


\vspace{1.5em}
}}}\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

\#2   \\[5pt]



prag \[ df & foc \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}




 \noindent \headingfont \small \textbf{Matriisi 21}: Monivalenssinen S4-konstruktio \normalfont \vspace{0.6cm}


Matriisi 21 esittää tilannetta, jossa verbin valenssiin kuuluu
objektin lisäksi jokin toinen (tässä tarkemmin määrittelemätön) argumentti (indeksi #2),
jonka diskurssifunktiona on fokus. Tällaisten fokuskonstituenttien on erittäin
todennäköistä (kielestä riippumatta) sijoittua koko lauseen viimeiseksi. Jos
ensimmäinen argumentti (indeksi #1) taas liittyy osaksi diskurssistatukseltaan
aktiivista propositiota, on puolestaan todennäköistä, että se sijaitsee heti
verbin jäljessä. Koska suomessa S2 on kieliopillisesti käytöltään rajoittunut
asema, jäävät ajanilmauksen sijaintivaihtoehdoiksi toisaalta sijainti objektin jäljessä (eli
tämän tutkimuksen viitekehyksessä S4) mutta kuitenkin ennen varsinaista
fokuksen funktiota toteuttavaa verbin argumenttia,[^advoma] toisaalta alkusijainti,
joka kuitenkin tässä tutkimuksessa aiemmin (ks. esim. luku \ref{f-funktiot})
tehtyjen havaintojen valossa johtaisi suomessa affektiiviseen tulkintaan.

[^advoma]: Adverbiaalin oman diskurssifunktion määrittely on tässäkin kohdassa
hankalaa, ja koska se ei ole tässä käsitellyn konstruktion kannalta oleellista, 
jätän sen ilmaisematta. Esimerkin \ref{ee_tietoyht} osalta ajanilmaus olisi nähdäkseni
melko luontevaa tulkita sekin osaksi fokusta. 


Koska venäjässä tyypillinen adverbisijainti on ennen verbiä, 
matriisissa 21 kuvatun kaltaisia tilanteita
ei juuri pääse syntymään: verbillä voi hyvin olla useita argumentteja ja niiden
järjestys voi määräytyä informaatiorakenteen periaatteiden mukaisesti, 
mutta adverbinen ajanilmaus sijoittuu standardinomaisesti S2-asemaan, eikä
pääse edes potentiaalisesti vaikuttamaan jonkin argumentin tulkitsemiseen
esimerkiksi kontrastiivisesti. Tämän takia ne melko harvat venäjän adverbiryhmiin
liittyvät S4-tapaukset, joita aineistossa on, ovat useimmiten tilanteita, joissa
itse adverbi todella toteuttaa selvästi juuri fokuksen funktiota, kuten
seuraavassa:




\ex.\label{ee_meshal} \exfont \small -\/- Ты мешал мне слишком часто и слишком долго. (RuPress: Комсомольская
правда)





\vspace{0.4cm} 

\noindent
Esimerkissä \ref{ee_meshal} on kyseessä tavallinen matriisissa 20 
kuvattu fokaalinen konstruktio.

### Teelinen semanttinen funktio

Edellä tehty yleiskatsaus aineistoissa havaittaviin samansuuntaisiin
vaikutuksiin osoitti, että nominaalisten ilmausten lisäksi 
teelinen semanttinen funktio eli aineistoryhmä E1c (ks. osio \ref{e1a-e1b-e1c}) erottuu muista
siinä, että se näyttäisi kielestä riippumatta kasvattavan loppusijainnin todennäköisyyttä, ja vieläpä
erittäin voimakkaasti.
Esimerkit \ref{ee_postitoimilupa} ja \ref{ee_skladskoe} kuvaavat tyypillisiä lauseenloppuisia E1c-tapauksia:




\ex.\label{ee_postitoimilupa} \exfont \small Valtioneuvosto myönsi pääkaupunkiseutua koskevan postitoimiluvan
yritykselle kolmeksi vuodeksi maaliskuussa 1997. (FiPress: Kaleva)






\ex.\label{ee_skladskoe} \exfont \small Мы отдали это складское помещение в аренду на 18 лет с подписанием
инвестиционного соглашения. (RuPress: Новый регион 2)





\vspace{0.4cm} 

\noindent
\todo[inline]{Jotain verbigrafiikkaa?}

Suomenkielinen esimerkki \ref{ee_postitoimilupa} on siinä suhteessa
mielenkiintoinen, että se sisältää itse asiassa kaksi ajanilmausta: teelisen *kolmeksi vuodeksi*
ja simultaanisen *maaliskuussa 1997*. Informaatiorakenteen kannalta 
jälkimmäinen ajanilmaus on tulkittavissa osaksi fokaalista konstruktiota, mutta samaa ei 
mitenkään yksiselitteisesti voi sanoa edellisestä ajanilmauksesta. Vaikuttaisikin siltä, että 
niin suomessa kuin venäjässä E1c-ryhmän ajanilmaukset määrittävät harvoin koko
lausetta niin kuin esimerkin \ref{ee_postitoimilupa} jälkimmäinen ajanilmaus, vaan
ovat selkeämmin sidoksissa nimenomaan tiettyihin verbeihin tai jopa
substantiiveihin.
Asia käy hyvin ilmi, kun tarkastellaan E1c-aineistojen  yleisimpiä pääverbejä, jotka on koottu
taulukkoon 14:

\FloatBarrier

\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
headverb & Freq & osuus\\
\midrule
tuomita & 301 & 14.24\\
saada & 193 & 9.13\\
valita & 121 & 5.72\\
antaa & 62 & 2.93\\
lomauttaa & 61 & 2.89\\
viedä & 58 & 2.74\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
headverb & Freq & osuus\\
\midrule
получить & 43 & 3.56\\
продлить & 43 & 3.56\\
пережить & 24 & 1.99\\
оставить & 22 & 1.82\\
взять & 19 & 1.57\\
давать & 19 & 1.57\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 14}: Yleisimmät E1c-aineiston pääverbit suomessa ja venäjässä \normalfont \vspace{0.6cm}

\FloatBarrier

Taulukon 14 mukaan E1c-aineiston S4-tapauksista
peräti 14,24 % esiintyy *tuomita*-verbin määritteenä. Venäjässä
mikään yksittäinen verbi ei samalla tavalla hallitse koko E1c-aineistoa, mutta 
taulukossa esiintyvä verbit ovat yhtä kaikki hyvin samantyyppisiä. Suurimman osan E1c-tapauksista
voi kummassakin kielessä nähdä väljästi oman *teelisen ajanilmauskonstruktionsa* ilmentyminä.
Konstruktio on kuvattu matriisiin  :




\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{


syn \[ cat & V \\
\] \\[5pt]


sem tuomita,saada,antaa,valita... \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem telic \\[5pt]


\vspace{1.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 22}: Teelinen konstruktio. \normalfont \vspace{0.6cm}


\todo[inline]{Venäjässä myös prefiksit ym...}

Teeliset ilmaukset ovat kaiken kaikkiaan melko homogeeninen ja selvästi omaksi joukokseen
erottuva ryhmänsä, jonka edustajat -- kuten taulukosta 14 
havaitaan -- ovat melko tiukasti sidoksissa tiettyihin verbilekseemeihin.
Teelisten ilmausten painottuminen loppusijaintiin käy puolestaan selvästi
ilmi taulukosta 15, jossa on esitetty kunkin sijainnin
suhteelliset osuudet suomen ja venäjän E1c-ryhmissä:



|   |S1   |S2/S3 |S4    |
|:--|:----|:-----|:-----|
|fi |0,28 |11,73 |87,98 |
|ru |4,63 |28,70 |66,67 |

\noindent \headingfont \small \textbf{Taulukko 15}: E1c-ryhmän jakautuminen eri sijainteihin \normalfont \vspace{0.6cm} 

Taulukossa nähtävä venäjän S4-osuus, 66,67 %, on itse asiassa
suurempi kuin missään muussa aineistoryhmässä. Kaiken kaikkiaan teelisistä ilmauksista
voidaan todeta, että niiden sijoittumisen ymmärtäminen on kohtalaisen mutkatonta eikä
juuri pidä sisällään kieltenvälisiä eroja.


## Eriäviä vaikutuksia {#s4-eri}

Aloitan kielen ja muiden selittävien muuttujien välisten
loppusijaintiin liittyvien interaktioiden tarkastelun edellisten lukujen tapaan
semanttisista funktiosta, joihin liittyvät interaktiot on esitetty kuviossa 
47. Kuten tähänkin asti, kuvio esittää
semanttisten funktioiden vaikutuksen suomessa suhteessa venäjään:


![\noindent \headingfont \small \textbf{Kuvio 47}: Semanttisten funktioiden ja kielen interaktio loppusijainnissa \normalfont](figure/langfuncts4-1.pdf)

Selvin kuviosta 47 tehtävä havainto koskee duratiivista
semanttista funktiota, jonka yhteisvaikutus suomen kielen kanssa on muita
kuviossa esitettyjä vaikutuksia positiivisempi. Seuraavaksi voimakkaampia
vaikutuksia ovat tapaa ilmaiseviin ja presuppositionaalisiin ilmauksiin liittyvät, joskin
etenkin jälkimmäiseen liittyy huomattava määrä epävarmuutta. Myös F-funktiolla ja
sekventiaalis--duratiivisilla ilmauksilla suomen kieli näyttäisi kasvattavan S4-todennäköisyyttä
verrattuna venäjään.

Edellä luvussa\ref{muutf-s2s3-kasv} käsiteltiin jo venäjän kohdalla havaittavaa
sekventiaalisten ilmausten taipumusta sijoittua tavallista useammin
loppusijaintiin. Tämä taipumus -- jonka edellä selitettiin olevan seurausta
ennen kaikkea venäjän L6a-aineistoryhmän leksikaalisista erityispiirteistä 
-- näyttäytyy kuviossa 47 
suomen S4-aseman todennäköisyyttä pienentävänä tekijänä. Sekventiaalisten
ohella simultaaniset ilmaukset muodostavat toisen semanttisen funktion,
jonka kohdalla keskisijainnin todennäköisyys suomessa korostuu ja sitä kautta
loppusijainnin todennäköisyys venäjään nähden pienenee.
Näiden lisäksi on vielä mainittava ennen muuta kehyksiset resultatiiviset
ilmaukset, joiden todettiin luvussa \ref{kehres-s1} kasvattavan suomen
S1-todennäköisyyttä: S4-aseman suhteen vaikutus on päinvastainen.


Morfologisesta rakenteesta tehtiin kuvion 44 
perusteella jo se koko aineistoa koskeva havainto, että nominaaliset
ilmaukset ovat yleisesti ottaen todennäköisempiä S4-sijoittujia kuin
adverbiset. Tämän tulkittiin edellä olevan seurausta ennen kaikkea siitä,
että nominaaliset ajanilmaukset ovat tyypillisempiä fokaalisen
konstruktion ilmentäjiä kuin adverbiset. Jos katsotaan kielen ja
morfologisen rakenteen välistä interaktiota, havaitaan kuitenkin myös koko
lailla selvä suomeen liittyvä vaikutus, jonka mukaan deiktiset adverbit
kasvattavat S4-todennäköisyyttä verrattuna venäjään. Tämä havainto
käy ilmi kuviosta 48:

![\noindent \headingfont \small \textbf{Kuvio 48}: Morfologisen rakenteen vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/langmorphs4-1.pdf)

Deiktisten ajanilmausten suhdetta sivuttiin edellä luvussa
\ref{simultaaninen-funktio} keskisijainnin
yhteydessä, kun pohdittiin syytä simultaanisen funktion ja ennen muuta L1-aineistoryhmien 
S2/S3-todennäköisyyttä kasvattavaan vaikutukseen suomessa. Alempana osiossa
\ref{deiktiset-adverbit-ja-positionaalisuus} samantyyppisten syiden 
havaitaan vaikuttavan myös loppusijainnin venäjää suurempaan suosioon.


Jo tarkasteltujen selittävien muuttujien lisäksi myös positionaalisuudella ja
numeraalin läsnäololla voidaan nähdä jonkinasteista kielestä riippuvaa
vaikutusta loppusijainnin todennäköisyyteen. Vaikutuksen suunta käy ilmi
kuvioista 49 ja 50:

![\noindent \headingfont \small \textbf{Kuvio 49}: Positionaalisuuden vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/pos.lang.s4-1.pdf)


![\noindent \headingfont \small \textbf{Kuvio 50}: Numeraalin läsnäolon vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/langisnumerics4-1.pdf)

Kuvion 49 perusteella voidaan sanoa, että suomessa
ajanilmauksen S4-todennäköisyys kasvaa, jos kyseessä on positionaalinen ilmaus.
Vastaavasti kuvio 50  viittaa siihen, että mikäli
lauseessa on jokin luvussa \ref{kehres-s1} esitetyn määritelmän täyttävä numeraali, 
ei S4 ole suomessa yhtä todennäköinen kuin muut sijainnit, joiden edellä
havaittiin kasvattavan ennen kaikkea alkusijainnin mutta myös jonkin verran keskisijainnin
todennäköisyyttä. Positionaalisuudesta tehty havainto on yhteydessä deiktisistä 
adverbeistä kuvion 48 perusteella tehtyyn havaintoon,
ja näitä kahta käsitelläänkin tarkemmin samassa yhteydessä, osiossa \ref{deiktiset-adverbit-ja-positionaalisuus}.
Sitä ennen tarkastelen kuitenkin semanttisiin funktioihin liittyviä suomen
ja venäjän välisiä eroja, ennen muuta duratiivisia ilmauksia.


### Duratiivinen funktio

Duratiivisen funktion painottuminen suomenkielisessä aineistossa S4-sijaintiin
on suorastaan silmiinpistävää: kaikkiaan 5 700
duratiivisesta tapauksesta S4-asemaan sijoittuu suomessa 
 3 596
eli peräti 
63,09 
prosenttia. Venäjänkielisessä aineistossa, joka kokonaisuutena on suomenkielistä aineistoa pienempi, on
kuitenkin selvästi enemmän duratiivisia ilmauksia -- kaikkiaan 7 197 kappaletta --
joista S4-asemaan sijoittuu vain 493
eli 6,85
prosenttia.[^durselitys]
Duratiiviset ilmaukset muodostavat siis ensi katsomalta suomessa ja venäjässä radikaalisti erilaiset aineistot. 
Duratiivisen funktion S4-osuuksien vertautuminen muiden funktioiden vastaaviin osuuksiin näkyy kuviossa
51, jossa katkoviivalla on merkitty kummankin kielen eri semanttisten funktioiden 
keskimääräinen S4-osuus ja pylväillä duratiivisen funktion S4-osuus.


[^durselitys]: venäjän duratiivisen aineiston koko selittyy...


![\noindent \headingfont \small \textbf{Kuvio 51}: Duratiivinen funktio ja S4-osuus \normalfont](figure/durvert-1.pdf)

Kuviosta 51 nähdään paitsi visuaalisesti kielten välinen
suuri ero S4-osuuksissa sinänsä, myös se, että venäjässä duratiivinen funktio
sisältää itse asiassa muihin funktioihin nähden keskimääräistä vähemmän
S4-tapauksia.

Duratiivisen funktion poikkeuksellisen voimakasta vaikutusta on syytä tarkastella lähemmin
vertaamalla keskenään eri duratiivisia aineistoryhmiä, joita ovat
 E1a, E2a, E2b, E6a, E6b ja E6c (ks. osiot \ref{e1a-e1b-e1c}--\ref{e6a-e6b-e6c}).
Kuvio 52 
näyttää sen, missä määrin aineistoryhmien koot vaihtelevat suomen ja venäjän välillä sekä sen,
minkä verran S4-tapausten osuus kussakin aineistoryhmässä vaihtelee. Lisäksi
kuvioon on merkitty adverbiset aineistoryhmät lihavoinnilla ja nominaaliset
kursiivilla.


![\noindent \headingfont \small \textbf{Kuvio 52}: Duratiivisten aineistoryhmien koko. Palkeissa oleva katkoviiva kertoo S4-tapausten määrän (S4-tapausten osuus on katkoviivan vasemmalle puolelle jäävä alue). Adverbiset aineistoryhmät on lihavoitu, nominaaliset taas kursivoitu. \normalfont](figure/dgsizes3c-1.pdf)

Kuvio 52 osoittaa ensinnäkin sen, että
adverbiset E6-aineistoryhmät ovat venäjässä suurempia kuin suomessa.
E6b-aineistoryhmässä venäjänkielinen aineisto sisältää
 3 373 tapausta, suomenkielinen taas
 2 549 tapausta; E6a taas sisältää
 2 007 venäjän- ja 
 1 003 suomenkielistä tapausta.
Mainittujen adverbisten ryhmien lisäksi venäjänkielinen aineisto on
suomenkielistä laajempi myös E2b-aineiston kohdalla
(ilmaukset *koko viikon* ja *всю неделю*). Muissa kuvion 52 
nominaalisissa aineistoryhmissä sen sijaan suomenkielisiä tapauksia on  -- kuten koko tutkimusaineiston tasolla --
venäjää enemmän.

Vaikka eri aineistoryhmien koot tarkasteltavissa kielissä vaihtelevat, havainto
toisaalta suomen duratiivisten ilmausten painottumisesta S4-asemaan ja
toisaalta venäjän S4-aseman suhteellisesta harvinaisuudesta
pysyy samanlaisena kaikissa kuvion 52  
aineistoryhmissä. Yleiskuvan tästä suhteellisten osuuksien välisestä erosta voi
saada katsomalla kuvioon piirrettyjä katkoviivoja, jotka osoittavat sen, kuinka
monta S4-tapausta kussakin osa-aineistossa on. Esimerkiksi venäjän
 3 373 E6b-tapauksesta S4-asemaan sijoittuu ainoastaan 
 169 \todo[inline]{Janko-viittauksia}
ja 2 007 E6a-tapauksestakin vain 
 123, kun taas suomen E6b-aineistossa 
(yhteensä 2 549 tapausta)
S4-lauseita on 1 199 ja E6a-aineistossa (yhteensä 1003 tapausta)
 632. Erot ovat samansuuntaiset myös kaikissa nominaalisissa aineistoryhmissä, joista suurin, E1a,
sisältää 73 / 577 venäjän- ja 
980 / 1 123 suomenkielistä S4-tapausta.

Duratiivisten ilmausten painottuminen S4-asemaan suomessa on lähtökohtaisesti
helppo ymmärtää, sillä ainakin päällisin puolin vaikuttaisi siltä, että
duratiiviset ilmaukset ovat tyypillisesti osa viestin pragmaattista väittämää
ja diskurssifunktioltaan fokaalisia. Näin on muun muassa seuraavissa suomen
E1a-aineiston esimerkeissä:




\ex.\label{ee_norman} \exfont \small Norman teki biografiaa päätoimisesti \emph{kaksi vuotta} ja haastatteli
kirjaansa varten suuren joukon Lennonin lähipiirin henkilöitä. (Araneum
Finnicum: beatles.fi)






\ex.\label{ee_komiteamiettii} \exfont \small Ensiksi komitea miettii sitä \emph{kaksi vuotta}. (Fipress: Länsi-Savo)






\ex.\label{ee_jkoski} \exfont \small Nevalainen on hoitanut suuren paperitehtaan kainalossa elävän
Jämsänkosken ympäristövirkaa \emph{pian yhdeksän vuotta}. (FiPress:
Kaleva)





\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_norman} -- \ref{ee_jkoski} ajanilmaukset on luontevasti
analysoitavissa osaksi sitä pragmaattisen väittämän osaa, joka erottaa
väittämän ja olettaman toisistaan (vrt. luku \ref{fokus}). Duratiivisten
ilmaisujen luontainen fokaalisuus ei kuitenkaan ole millään tavalla suomelle
spesifi ominaisuus, vaan pikemminkin universaali piirre, joka on aina läsnä,
kun kuvataan jonkin tapahtuman kestoa [@tobecited]. Itse asiassa esimerkiksi 
Janko [-@janko2001, 255--256] mainitsee venäjän *давно*-sanasta (aineistoryhmä E6b), että niin
kutsuttujen *yleisesti toteavien* verbien yhteydessä [ks. esim. shmelev 2000,
??] *давно*-sanan diskurssifunktio on aina reema. Ylipäätään *давно* ei Jankon mukaan
koskaan esiinny *täysiverisenä*, vaan \todo[inline]{mainitse padutsheva} korkeintaan
painottomana teemana (ks. myös luku \ref{topiikin-ja-fokuksen-tarkempi-erittely}).
Esimerkit \ref{ee_smarkom} -- \ref{ee_brits} kuvaavat S4-sijoittuvia fokaalisia *давно*-tapauksia:




\ex.\label{ee_smarkom} \exfont \small -\/- Мы с Марком Рудинштейном ведем переговоры уже \emph{давно.}
(RuPress: Комсомольская правда)






\ex.\label{ee_gollandskij} \exfont \small Голландский полковник Геррит Ветерхолт обхаживал меня уже \emph{давно.}
(Araneum Russicum: left.ru)






\ex.\label{ee_brits} \exfont \small Британские ученые исследуют трудные дни уже \emph{давно.} (Araneum
Russicum: 66.ru)





\vspace{0.4cm} 

\noindent
Esimerkkien \ref{ee_smarkom} -- \ref{ee_brits} tapaan myös nominaaliset duratiiviset aineistoryhmät
sisältävät samalla tavoin fokaalisia S4-tapauksia kuin edellä annetut suomenkieliset
esimerkit. Seuraavat virkkeet kuuluvat venäjän E1a-ryhmään:




\ex.\label{ee_naprasno} \exfont \small Мы ждали ее несколько дней, но напрасно. (Araneum Russicum: wmos.ru)






\ex.\label{ee_presledoval} \exfont \small Кстати, его фотограф, которому очень нравилась моя музыка, преследовал
меня восемь дней. (RuPress: Комсомольская правда)






\ex.\label{ee_zemljak} \exfont \small Наш земляк пишет ее уже несколько лет. (RuPress: Комсомольская правда)





\vspace{0.4cm} 

\noindent
Kuvion 52 perusteella vaikuttaisi kuitenkin siltä,
että suomen ja venäjän välinen ero on suurimmillaan juuri nominaalisissa
aineistoryhmissä, joissa suomen S4-osuudet ovat kaikissa tapauksissa yli 70
prosenttia ja venäjän vastaavat osuudet alle 15 prosenttia.
Suomen E1a-aineistossa S4-aseman ulkopuolelle sijoittuu kaikkiaan vain
 143 tapausta 
 1 123:sta, ja toisaalta venäjän
vastaavassa aineistossa S4-asemaan sijoittuu vain 
 73  tapausta 
 577:stä. 




Kun suomen E1a-aineiston kaikkia keskisijaintitapauksia tutkii tarkemmin,
havaitaan, että ne voidaan jakaa neljään ryhmään. Ensinnäkin tapausten joukossa
on yhteensä 32 lausetta, joista todellisuudessa puuttuu
ajanilmauksesta erillinen objekti tai joissa koneellinen analyysi on tehnyt
virheen. Esimerkki \ref{ee_urut} kuvaa tällaista tapausta:




\ex.\label{ee_urut} \exfont \small Kallion urkujen suunnittelu ja rakentaminen on vienyt \emph{neljä
vuotta} ja viisi miljoonaa. (Fipress: Länsi-Savo)





\vspace{0.4cm} 

\noindent
Toiseksi, 25 tapauksessa kyse on rakenteeltaan esimerkkejä \ref{ee_annelisauli}
ja \ref{ee_artohalonen} muistuttavista lauseista, jotka voisi lukea osaksi omaa *duratiivi + paikka* -konstruktiotaan:




\ex.\label{ee_annelisauli} \exfont \small Anneli Sauli opiskeli aikoinaan kolme vuotta Berliinissä puhetekniikkaa,
ja käyttää edelleen ääniharjoittelussa saksalaista metodia. (FiPress:
Karjalainen)






\ex.\label{ee_artohalonen} \exfont \small Arto Halonen on hoitanut nyt kolme vuotta Pohjois-Karjalassa
läänintaiteilijan virkaa. (FiPress: Kaleva)





\vspace{0.4cm} 

\noindent
Duratiivi + paikka -konstruktio voidaan kuvata seuraavasti:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{


syn \[ cat & V \\
\] \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


funct dur \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem paikka \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]



prag \[ ds & act- \\
df & foc \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 23}: Duratiivi + paikka -konstruktio. \normalfont \vspace{0.6cm}

Duratiivi + paikka -konstruktion erikoistapaus ovat lauseet, joissa lisäksi on
lauseenalkuinen lokalisoiva ajanilmaus kuten esimerkissä \ref{ee_punkassa}:




\ex.\label{ee_punkassa} \exfont \small Armeija-aikana hän odotti kaksi viikkoa erikoispunkassa varusteita.
(FiPress: Kaleva)





\vspace{0.4cm} 

\noindent
Kolmanneksi suomen E1a-aineiston keskisijaintiin sijoittuvista tapauksista voidaan erottaa
ne lauseet, joissa kyse on S2-sijoittuneesta relatiivilauseen ajanilmauksesta
kuten esimerkissä \ref{ee_peggylipton}:




\ex.\label{ee_peggylipton} \exfont \small Norma Jenningsia esittää Peggy Lipton, joka \emph{viisi vuotta} näytteli
yhtä pääosaa suosikkisarjassa "The Mod Squad". (FiPress: Demari)





\vspace{0.4cm} 

\noindent
Lopuksi voidaan todeta, että edellisistä ryhmistä erottuvat lisäksi seuraavat tapaukset:




\ex.\label{ee_juhlauhreja} \exfont \small Ja he söivät \emph{seitsemän päivää} juhlauhreja ja uhrasivat
yhteysuhreja ja ylistivät Herraa, isiensä Jumalaa. (Araneum Finnicum:
evl.fi)






\ex.\label{ee_oppilaat} \exfont \small Oppilaat juhlivat \emph{kaksi päivää} ikimuistoista opettajaansa.
(FiPress: Iltalehti)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_juhlauhreja} ja \ref{ee_oppilaat} -- joista ensimmäinen on lainaus
Raamatusta -- edustavat siitä harvinaista duratiivisten ilmausten
käyttötapaa, että ne on itse asiassa tulkittavissa diskurssifunktioltaan
ennemmin topikaalisiksi kuin fokaalisiksi. Kummassakaan virkkeessä kirjoittajan tavoitteena
ei ole ilmoittaa lukijalle jonkin lukijan tajunnassa jo edustetun tapahtuman kestoa. Pikemminkin 
molempien esimerkkien kirjoittajien voi nähdä kertovan jotakin siitä, miten lauseiden subjektit
ovat viettäneet ajanjakson X (*seitsemän / kaksi päivää*). Nimitän tällaista 
rakennetta *topikaaliseksi duratiiviseksi konstruktioksi*, josta esitän seuraavanlaisen
formaalin kuvauksen:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{4.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{


syn \[ cat & V \\
\] \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & top,aff \\
\] \\[5pt]


funct dur \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]


\vspace{4.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 \noindent \headingfont \small \textbf{Matriisi 24}: Topikaalinen duratiivinen konstruktio \normalfont \vspace{0.6cm}

Huomattakoon, että esimerkkien \ref{ee_juhlauhreja} -- \ref{ee_oppilaat} tapauksissa syy
siihen, että kirjoittaja viestii tietoa siitä, miten lauseiden subjektit ovat
jonkin ajan viettäneet, on tämän ajanviettotavan tai käytetyn ajan määrän
epätavallisuudessa. Tässä mielessä ilmaukset vertautuvat edellä esimerkiksi 
luvussa   \ref{s1-f1a}
käsiteltyihin affektiivista S1-konstruktiota edustaviin tapauksiin kuten esimerkkeihin
\ref{ee_kuolemalta} ja \ref{ee_synti}. Myös näissä tapauksissa huomattiin, että esimerkit
olivat usein peräisin tyyliltään hyvin spesifeistä uskonnollisista
konteksteista. Luvussa \ref{sekventiaalis-duratiivinen-ja-duratiivinen-funktio}
todettiin suomen lauseenalkuisista duratiivisista ilmauksista, että
niihin useimmiten liittyy affektin diskurssifunktio.

Ainakin erityisen vähän keskisijaintitapauksia sisältävän E1a-aineistoryhmän perusteella
voidaan siis sanoa, että syyt suomen duratiivisten ilmausten  sijoittumiselle muuhun kuin S4-asemaan
ovat koko lailla rajatut. Venäjän E1a-aineistoa tutkittaessa taas havaitaan, että
se on käytöltään selvästi laajempi. Olennaisin ero suomen ja venäjän E1a-aineistoissa
vaikuttaisikin olevan se, että venäjässä keskisijaintiin sijoittuu tapauksia, joiden
on melko suoraviivaista tulkita ilmentävän fokaalisen konstruktion S2-versiota. 
Kuvaan fokaalista S2-konstruktiota seuraavalla matriisilla:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

\#1   \\[5pt]


gf subj \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]



prag \[ df & focus \\
\] \\[5pt]


sem aika \\[5pt]


\vspace{0.3em}
}}\minibox[frame,pad=4pt,rule=0.1pt]{

\#1     \\

ds   act+ \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{0.3em}
}}}}%
\end{avm}

\normalsize

\vspace{0.4cm}




\noindent \headingfont \small \textbf{Matriisi 25}: Fokaalinen S2-konstruktio. \normalfont \vspace{0.6cm} 

Matriisissa 25 verbi ja sen täydennys
sekä subjekti viittaavat matriisien 19 ja
20 tapaan johonkin 
sellaiseen propositioon, joka viestin vastaanottajalla oletetaan
diskurssistatukseltaan aktiiviseksi. Tätä yhteenkuuluvuutta on merkitty
indeksillä #1. Ajanilmauksen diskurssifunktioksi on, aivan kuten 
edellä käsitellyissä fokaalisen konstruktion S1- ja S4-versioissa, 
merkitty fokus.

Venäjänkielisessä aineistossa tavattavista fokaalisista S2-konstruktioista
esimerkkejä ovat muun muassa seuraavat tapaukset:




\ex.\label{ee_dobroserdov} \exfont \small Добросердов \emph{почти семь лет} командовал дивизией. (Araneum
Russicum: delostalina.ru)






\ex.\label{ee_palomniki} \exfont \small Дарья Васильевна Спевякина \emph{вот уже семь лет} возит людей в
паломнические поездки. (Araneum Russicum: naslednick.ru)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_dobroserdov} on informaatiorakenteeltaan esimerkkiä \ref{ee_palomniki} selvempi: lauseen subjektiin
viitataan pelkällä sukunimellä, joten kirjoittaja katsoo sen oletettavasti lukijan mielessä aktiiviseksi, 
ja myös командовать-verbin täydennys *дивизией* esiintyy lauseessa ilman minkäänlaisia selittäviä määritteitä, niin että on
syytä olettaa koko proposition [D. johti divisioonaa] olevan osa pragmaattista väittämää. Ajanilmauksen
tehtäväksi jää sen ilmaisemisen, kuinka kauan tämä lukijan tiedossa oletettavasti oleva asiaintila
on ollut voimassa. Esimerkissä \ref{ee_palomniki} sen sijaan
niin lauseen subjekti kuin tästä kertova propositiokin vaikuttavat ensi silmäyksellä
aktivoimattomilta tiedoilta. Esimerkin laajempi konteksti kuitenkin osoittaa, ettei
asia ole näin:

\begin{quote}%
Нет, наверное, в России сейчас ни одного православного, который бы ни
разу ни участвовал в паломничестве или не мечтал о нём. Спокон веку люди
на Руси ко святым местам ходили. Но традиция прервалась, как надо –
забыли, а как правильно – не знаем. Вот и решили обратиться к знающему
человеку. Дарья Васильевна Спевякина вот уже семь лет возит людей в
паломнические поездки.

http://www.naslednick.ru/archive/rubric/rubric\_531.html
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkin \ref{ee_palomniki} laajemmasta kontekstista käy ilmi, että kyseessä on ortodoksisessa
aikakausjulkaisussa ilmestynyt pyhiinvaellusta käsittelevä artikkeli, jonka ingressissä esimerkki \ref{ee_palomniki}
esiintyy. Ennen varsinaista esimerkkiä ingressissä on esitelty lukijalle, että jutussa tullaan käsittelemään
pyhiinvaelluksia ja virke *Вот и решили обратиться к знающему человеку* esittelee esimerkin \ref{ee_palomniki} subjektin.
Varsinaisen esimerkkivirkkeen voi täten nähdä keskittyvän siihen, että edellisessä lauseessa
esitelty *pyhiinvaelluksista paljon tietävä henkilö* määritellään tarkemmin: kerrotaan
kyseisen henkilön nimi ja se, kuinka laaja hänen kokemuksensa asian parista itse asiassa on. Tässä mielessä
kyseessä todellakin on fokaalinen konstruktio, jossa ajanilmaus on osa pragmaattista väittämää.

Esimerkkiä \ref{ee_palomniki} muistuttaa myös esimerkki \ref{ee_vplenu}, jossa myös
tarkempi konteksti paljastaa, että kyseessä on lukijan tajunnassa oletettavasti
aktiivisena oleva tapahtuma, josta nyt kerrotaan tarkempia tietoja:




\ex.\label{ee_vplenu} \exfont \small Преступники \emph{шесть дней} держали 20-летнего парня в плену, требуя с
его отца выкуп в три миллиона евро. (RuPress: Комсомольская правда)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_vplenu} laajempi konteksti on seuraavanlainen:

\begin{quote}%
Российские богачи вывозят детей за границу, чтобы их не похитили дома

Почему даже выпускник Школы КГБ мультимиллионер Евгений Касперский не
смог уберечь сына от бандитов

«Все закончилось. ФСБ, Петровка и служба безопасности «Лаборатории»
сделали невозможное. Я очень приятно потрясен их профессионализмом.
Злодеи арестованы и дают показания. Всем работать!» - эту запись в
интернет-дневнике знаменитый программист Евгений Касперский оставил
после освобождения своего младшего сына Вани, похищенного в Москве 19
апреля. Преступники шесть дней держали 20-летнего парня в плену, требуя
с его отца выкуп в три миллиона евро.

https://www.kursk.kp.ru/daily/25677.3/836220/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Laajempi konteksti osoittaa, että esimerkki \ref{ee_vplenu} on osa kidnappauksista
kertovaa artikkelia, jossa ennen varsinaista esimerkkivirkettä on
mainittu, että Jevgeni Kasperskyn poika oli ollut kidnapattuna mutta päässyt
vapaaksi. Esimerkki \ref{ee_vplenu} tarkentaa, kuinka kauan kidnappaus oli kestänyt
ja minkälaisia vaatimuksia pojan kidnappaajilla oli ollut. 

Kolmas samanlainen tapaus on esimerkissä \ref{ee_baletom}:




\ex.\label{ee_baletom} \exfont \small Приняли нас обеих, и я несколько лет занималась балетом, несмотря на то,
что была довольно худой и не очень складной дылдой, как и все подростки
в этом возрасте. (RuPress: Комсомольская правда)





\vspace{0.4cm} 

\noindent
Myös tämän esimerkin laajempi konteksti osoittaa, että baletin harrastaminen on lukijan
tajunnassa aktiivisena oleva ajatus:

\begin{quote}%
У меня была подружка - очень «танцевальная» девочка из интеллигентной
семьи. Ее родителям посоветовали привести дочку на просмотр в балетную
студию - ну и я пошла с ней за компанию. Приняли нас обеих, и я
несколько лет занималась балетом, несмотря на то, что была довольно
худой и не очень складной дылдой, как и все подростки в этом возрасте.

https://www.kompravda.eu/daily/25665/827046/
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkissä \ref{ee_baletom} kirjoittajan tavoitteena ei selvästikään ole välittää lukijalle 
tekstin subjektista, laulaja Jelena Presnjakovasta, propositiota [Presnjakova harrasti muutaman vuoden balettia],
vaan pikemminkin kertoa, että Presnjakovan viimeistään yhdyslauseen ensimmäisessä
lauseessa ('meidät molemmat valittiin') jo implikoitu balettiharrastus kesti muutaman vuoden.
Tämä huomio on oleellinen, kun mietitään syitä sille, miksi suomen duratiiviset ilmaukset
painottuvat keskisijainnin sijasta loppusijaintiin. Esimerkin \ref{ee_baletom} suomenkielisenä käännöksenä voisi
nimittäin hyvin olla virke \ref{ee_baletfi1}, mutta laajempi konteksti huomioon ottaen virke \ref{ee_baletfi2}
olisi vähintäänkin omituinen:




\ex.\label{ee_baletfi1} \exfont \small Meidät molemmat hyväksyttiin, ja minä harrastinkin balettia muutaman
vuoden, vaikka.. .






\ex.\label{ee_baletfi2} \exfont \small Meidät molemmat hyväksyttiin, ja minä harrastinkin muutaman vuoden
balettia, vaikka.. .





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_baletfi2} omituisuus johtuu siitä, että keskisijaintiin sijoitettu ajanilmaus
aiheuttaa lauseen tulkitsemisen niin, että kirjoittaja olettaa baletin harrastamisen olevan 
lukijan tajunnassa aktivoimaton. Jos koko edeltävä kappale on kuitenkin puhuttu nimenomaan baletin harrastamisesta,
esimerkki \ref{ee_baletfi2} saa viestin vastaanottajan epäilemään, että kirjoittajalta puuttuu normaalissa vietinnässä
oleellinen kyky sovittaa sanomansa siihen, mitä kuulija oletettavasti tietää ja mitä ei [vrt. @hilpert2014, 111]. \todo[inline]{TODO eri kohta, itse asiassa}
Jos baletin harrastaminen on, kuten esimerkin \ref{ee_baletom} tapauksessa, diskurssistatukseltaan aktiivinen,
on loogisin paikka duratiiviselle ajanilmaukselle suomessa lauseen loppu. Tosin on todettava, että puhutussa
diskurssissa oikealla intonaatiolla voitaisiin epäilemättä saada aikaan ajatus baletin harrastamisen aktivoimattomuudesta vaikka
ajanilmaus sijaitsisikin S4-asemassa ja myös päinvastoin S3-sijoittuneesta ajanilmauksesta huolimatta baletti voitaisiin
prosodisin keinoin esittää aktivoituneena. Kirjoitetussa tekstissä S4 on kuitenkin se asema, joka oletuksena
tuo lukijalle esimerkin \ref{ee_baletom} kontekstissa vaadittavan ajatuksen baletin harrastamisen aktivoituneisuudesta.

\todo[inline]{Laajenna näkemystä adverbisiin ja muihin dur.ryhmiin}

E1a-ryhmän pohjalta tehtyjen havaintojen perusteella voidaan siis päätellä,
että oleellinen ero suomen ja venäjän välillä on siinä, miten venäjä käyttää
myös keskisijaintia fokaalisen konstruktion toteuttamiseen, kun taas suomessa
S4-sijainti on useimmiten ainoa vaihtoehto.
Edellä osiossa \ref{loppus_np} havaittiin, että S2-sijainti -- venäjän
standardiasema etenkin adverbeille -- on jossain määrin monikäyttöisempi
kuin suomen standardiasema, S3. Osiossa \ref{loppus_np} havaitut verbinjälkeiseen
asemaan liittyvät monitulkintaisuudet ja S2-aseman laajempikäyttöisyys ovat
epäilemättä taustalla myös kielten välillä duratiivisten ilmausten kohdalla
nähtävissä suurissa eroissa.

### Muut suomen S4-todennäköisyyttä kasvattavat semanttiset funktiot {#muutf-s4}

Kuviosta 47 nähtävät kielen ja semanttiseen funktion
väliset interaktiovaikutukset ovat selvästi muita suurempia duratiivisen
funktion kohdalla. Duratiivisen funktion ohella edellä nostettiin esille
suomen S4-todennäköisyyttä nostavina toisaalta tapaa ilmaiseva ja
presuppositionaalinen, toisaalta sekventiaalis--duratiivinen ja taajuutta
ilmaiseva funktio. Luon seuraavassa lyhyen katsauksen näiden vaikutusten
taustalla oleviin tekijöihin.



Kuten edellä mainittiin, tapaa ilmaisevaan funktioon ja loppusijainnin käyttöön
suomessa liittyviä erityispiirteitä käsiteltiin pääasiassa keskisijainnin
yhteydessä osiossa \ref{s2s3-pien}. Presuppositionaalisten ilmausten taas
todettiin luvussa \ref{presup-sama-keski} olevan kielestä riippumatta erittäin
voimakkaasti keskisijaintiin painottuneita, niin että kummassakin kielessä
keskisijaintiin sijoittuu yli 90 prosenttia kaikista presuppositionaalisista
tapauksista. Suomessa on kuitenkin hivenen 
enemmän (3,82 % / 155) myös 
presuppositionaalisia S4-tapauksia
kuin venäjässä (0,30% / 9 kpl).
Ero on jäljitettävissä seuraavankaltaisiin suomenkielisen aineiston
*jo*-tapauksiin:




\ex.\label{ee_nimeni} \exfont \small Tiesin, että hän tiesi nimeni jo. (Araneum Finnicum:
fi.taikakoulu.wikia.com)






\ex.\label{ee_starttasi} \exfont \small Hallitus starttasi oman partiosyksynsä jo ja odottaa innolla tulevaa
kautta. (Araneum Finnicum: karka.partio.net)





\vspace{0.4cm} 

\noindent
Suomen lauseenloppuiset *vielä*-tapaukset sekä käytännössä
kaikki venäjän lauseenloppuiset presuppositionaaliset tapaukset ylipäätään
osoittautuivat aineistosta suodattumatta jääneiksi ei--temporaalisiksi
käyttötilanteiksi, kuten seuraavat esimerkit:




\ex.\label{ee_satonsa} \exfont \small Parhaan laadun tuottajat rajoittavat satonsa vielä sallittuja määriä
vähäisemmiksi. (Araneum Finnicum: peda.net)






\ex.\label{ee_tsheka} \exfont \small Итак, главным правилом для увеличения среднего чека является активное
предложение клиентам чего-то еще. (Araneum Russicum: kaprise-moscow.ru)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_starttasi} antaa viitteitä siitä, että  suomessa on tietyissä
tilanteissa mahdollista käyttää *jo*-sanaa itsenäisesti fokaalisen
S4-konstruktion ajanilmauksen paikalla: esimerkin *jo*-sanan tilalle olisi
helppo kuvitella jokin lokalisoiva simultaaninen ajanilmaus, esimerkiksi
*Hallitus starttasi oman partiosyksynsä viime maanantaina*. *Jo*-sanan tapauksessa
konstruktion välittämäksi informaatioksi muodostuu lopulta se, että
starttaaminen on tapahtunut jossain kohti menneisyyttä. Myös tässä ajanilmauksen
S4-sijainnin voi nähdä olevan seurausta siitä, että  ratkaisulla vältetään
S3-sijainnista mahdollisesti seuraavat monitulkintaisuudet kuten lauseen tulkitseminen
eri aloitettavien asioiden vertailuksi. 

Sekventiaalis--duratiivisista ilmauksista todettiin keskisijainnin tarkastelun yhteydessä
luvussa  \ref{muutf-s2s3-kasv}, että koska alkusijainti on venäjässä näillä ilmauksilla
niin paljon suomea tavallisempi, nostaa kieli-muuttujan "suomi"-arvo sekä keski- että
loppusijainnin todennäköisyyttä. Sekventiaalis--duratiivisia keskisijaintitapauksia
tarkasteltaessa havaittiin, että osa 
suomen ja venäjän välisistä eroista selittyy sillä, että suomessa sekventiaalis--duratiivisiin
ilmauksiin liittyvät johdantokonstruktiot toteutuvat lähinnä S3-asemassa, siinä missä
venäjässä myös S1 on tavallinen. S4-sijaintia tutkittaessa on ensinnäkin todettava, että
niin suomessa kuin venäjässä sekventiaalis--duratiiviset ilmaukset sijoittuvat
siihen selkeästi keskimääräistä useammin. Tämä ilmi paitsi kuviosta 43 ,
myös taulukosta 16, jossa on esitetty suurimman
sekventiaalis--duratiivisen aineistoryhmän, L9d:n, jakautuminen eri sijainteihin:



|   |S1    |S2/S3 |S4    |
|:--|:-----|:-----|:-----|
|fi |2,69  |16,65 |80,67 |
|ru |26,88 |34,03 |39,09 |

\noindent \headingfont \small \textbf{Taulukko 16}: Suomen ja venäjän L9d-ryhmän jakautuminen eri sijainteihin \normalfont \vspace{0.6cm} 

S4-sijainnin suosio sinänsä on helppo selittää fokaalisen konstruktion tavallisuudella 
juuri L9d-aineistossa. Esimerkki \ref{ee_akateeeminen} kuvasi edellä tyypillistä suomen L9d-aineiston
fokaalista S4-tapausta, ja on syytä olettaa, että suurin osa niin kummankin kielen
S4-tapauksista on vastaavia fokaalisia tapauksia. Venäjänkielisestä aineistosta
voidaan löytää muun muassa seuraavat esimerkit:




\ex.\label{ee_upravljaju} \exfont \small Сам я управляю транспортными средствами с 1969 года. (Araneum Russicum:
my-ledimir.ru)






\ex.\label{ee_rukovodil} \exfont \small Он руководил страной с 1999 года, то есть он потратил более одной пятой
части своей жизни за рулем России. (Araneum Russicum: rusinform.ru)






\ex.\label{ee_tska} \exfont \small Баскетбольный клуб ЦСКА ведет свою историю с 1924 года, когда в
Центральном доме Красной Армии ( ЦДКА) была создана секция баскетбола.
(RuPress: РИА Новости)






\ex.\label{ee_patriot} \exfont \small "Патриот" ведет свою историю с 1925 года. (Araneum Russicum:
patriot-izdat.ru)






\ex.\label{ee_beton} \exfont \small В нашей стране исследование технологии производства ячеистого бетона в
едет свое начало с 1928 года. (Araneum Russicum: s-t-group.ru)






\ex.\label{ee_skautskij} \exfont \small Это большой международный скаутский праздник, традиция проведения
которого берет свое начало с 1932 года. (Araneum Russicum: kairblog.ru)





\vspace{0.4cm} 

\noindent

Esimerkit \ref{ee_tska} -- \ref{ee_skautskij} havainnollistavat venäjässä
tavallista *вести свою историю/начало с N года* -konstruktiota. Vastaavia rakenteita
kaikkiaan 394 venäjän
L9d-aineiston S4-tapauksesta on yhteensä
  77.
Tämänkin konstruktion yleisyys kuvastaa omalta osaltaan niitä käyttö- ja
merkityseroja, joita suomen ja venäjän L9d-ryhmien ilmausten välillä on. Olennaista
kuitenkin on ennen kaikkea se, että S4 on venäjän sekventiaalis--duratiivisilla
ilmauksilla yleinen juuri osana fokaalista konstruktiota.

Suomen ja venäjän S4-aineistojen välillä onkin mielenkiintoinen ero siinä,
että suomessa on eräitä loppusijaintitapauksia, joissa ajanilmauksella ei selvästikään
ole fokuksen diskurssifunktio. Näihin kuuluvat muun muassa seuraavat:




\ex.\label{ee_lannenpuhelin} \exfont \small Lännen Puhelin on maksanut osinkoja omistajilleen vuodesta 1999 lähtien
yhteensä 39 miljoonaa euroa, joka on 572 euroa osakkeelta. (Araneum
Finnicum: koulutushakemisto.com)






\ex.\label{ee_erasmus} \exfont \small Yli kolme miljoonaa opiskelijaa on saanut Erasmus-apurahaa vuodesta 1987
lähtien. (Araneum Finnicum: cimo.fi)






\ex.\label{ee_norsktipping} \exfont \small Tällä tavoin ja päivän hintatasoon sovitettuna, Norsk Tipping on
lahjoittanut jopa 85 miljardia Norjan kruunua hyvään tarkoitukseen
vuodesta 1948 lähtien. (Araneum Finnicum: casinolehti.com)





\vspace{0.4cm} 

\noindent

Jos vaikkapa esimerkkiä \ref{ee_erasmus} ajattelisi tavanomaisena fokaalisen konstruktion
edustajana, olisi tulkittava, että koko lauseessa esitettäisiin lukijan tajunnassa
aktiiviseksi oletettu propositio [yli kolme miljoonaa opiskelijaa on saanut Erasmus-apurahaa]
ja tästä täydennettäisiin ajallinen lisätieto, nimittäin se, että (mahdollisesti samojen
opiskelijoiden) rahojensaantiprosessi on jatkunut vuodesta 1987. Tällainen tulkinta
on kuitenkin mahdoton: se edellyttäisi muun muassa sen, että subjektina olisi jokin aktiiviseen
referenttiin viittaava konstituentti, kuten lauseessa *Virtanen on saanut
Erasmus-apurahaa vuodesta 1987 lähtien.* Esimerkki \ref{ee_erasmus} samoin kuin muut kaksi
esimerkkiä onkin tulkittava niin, että kussakin tapauksessa oleellisin osa
pragmaattista väittämää on nimenomaan se määrä, josta kirjoittaja viestii.
Tapaukset ovatkin tulkittavissa määrää painottavan konstruktion S4-versioksi, joka voidaan
muodollisesti kuvata seuraavasti:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


val määrä,foc \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika,rajattu ajanjakso \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



\noindent \headingfont \small \textbf{Matriisi 26}: Määrää painottavan konstruktion S4-versio \normalfont \vspace{0.6cm} 

Vastaavasta S4-konstruktiosta venäjänkielisessä
aineistossa -- jossa S4-sijoituneen sekventiaalis--duratiivisen ajanilmauksen
ja sen lisäksi numeraalin sisältäviä tapauksia on kaikkiaan vain
 14
 kappaletta -- on viitteenä ainoastaan seuraava esimerkki:




\ex.\label{ee_subjekty} \exfont \small 42 субъекта РФ получат эту возможность уже с июля этого года, а
федеральные ведомства уже в апреле. (Araneum Russicum: ocenkababenko.ru)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_subjekty} on kuitenkin siinä mielessä erilainen kuin edellä
esitetyt suomenkieliset tapaukset, että siinä ajanilmausta määrittää
presuppositionaalinen *уже*. Lauseen pragmaattinen olettama pitää sisällään
ajatuksen *tästä mahdollisuudesta* sekä oletettavasti myös sen, että mahdollisuus
koskee erilaisia hallinnollisia yksikköjä. Pragmaattiseen väittämään kuuluu ajatus
siitä, että tietyillä yksiköillä mahdollisuus tulee ajankohtaiseksi aikana T1, eräillä
muilla taas aikana T2. Ajanilmauksilla on siis lopulta fokaalinen diskurssifunktio
toisin kuin edellä luetelluissa suomenkielisissä esimerkeissä.


Tarkastelen vielä lyhyesti F-funktiota ilmaisevia tapauksia, joilla kuvion
47 mukaan on pieni suomen S4-todennäköisyyttä
kasvattava vaikutus. Jos katsotaan, millaisia S4-osuudet ovat eri F-funktiota edustavissa
aineistoryhmissä ja verrataan näitä lukemia koko kummankin kielen S4-osuuteen
koko aineiston tasolla, saadaan kuvion 53 mukainen
jakauma (vrt. S1-aineisto ja kuvio 24 edellä
luvussa \ref{f-funktiot}):


![\noindent \headingfont \small \textbf{Kuvio 53}: Erot F-funktiota edustavien aineistoryhmien S4-osuuksissa \normalfont](figure/ferojas4-1.pdf)

Kuviosta nähdään, että suomen ja venäjän F-ryhmät käyttäytyvät melko lailla samansuuntaisesti
verrattuna koko aineiston tason S4-osuuksiin: ryhmät, jotka suomessa ovat keskimääräistä
tavallisempia S4-asemassa, ovat sitä myös venäjässä, ja sama koskee myös
keskimääräistä harvemmin S4:ään sijoittuvia aineistoryhmiä.

[KESKEN]

### Suomen S4-todennäköisyyttä pienentävät semanttiset funktiot {#fi-s4-pien}

Kuvion 47 perusteella kehyksinen resultatiivinen,
sekventiaalinen ja likimääräinen funktio pienentävät suomen S4-todennäköisyyttä venäjään
verrattuna selkeästi sekä näiden lisäksi simultaaninen ja varsinainen resultatiivinen
ekstensio jonkin verran. Kuvio  esittää kolmen selvimmin
kieliä erottavaan funktioon kuuluvien aineistoryhmien S4-osuudet verrattuna koko aineiston
keskiarvoon:

![](figure/unnamed-chunk-16-1.pdf)

Tässä esitetyistä ryhmistä sekventiaalinen L6b vaikuttaisi noudattavan
koko lailla aineiston yleistä linjaa suomen ja venäjän välisissä S4-eroissa:
koko aineiston tasolla suomen S4-sijainnin suhteellinen osuus
on 3,87-kertainen venäjään verrattuna, ja L6b-ryhmän kohdalla vastaava
lukema on 3,49. Muissa kuviossa esitetyissä ryhmissä 
luvut ovat kuitenkin tavallista pienempiä: kehyksistä resultatiivisuutta edustavassa
E3a-ryhmässä 1,98, sekventiaalisessa L6a-ryhmässä 1,67,
likimääräisessä LM1-ryhmässä 1,21 ja toisessa kehyksisessä ryhmässä, E3b:ssä,
ainoastaan 1,16. Toisin sanoen, vaikka kaikissa tapauksissa suomen S4 on venäjän S4:ää
yleisempi, ovat erot pienempiä kuin koko aineiston tasolla, minkä
takia myös kuviossa 47 kehyksinen resultatiivisuus,
likimääräisyys ja sekventiaalisuus vähentävät S4-todennäköisyyttä suomessa
verrattuna venäjään.

Kehyksistä resultatiivisuutta käsiteltiin aiemmin alkusijainnin yhteydessä (osio \ref{kehres-s1}),
jolloin tutkittiin syitä sille, miksi nämä ilmaukset suomessa painottuvat 
tavallista useammin lauseen alkuun. Selkeimpänä syynä pidettiin 
määrää painottavaa S1-konstruktiota (matriisi 9 ).
Kuvion  mukaan toinen kehyksisen resultatiivisen
ekstension aineistoryhmistä, E3b, on venäjässä ja suomessa melkein yhtä yleinen
S4-sijoittuja. Esimerkit \ref{ee_rahapelit} -- \ref{ee_slyshim} valottavat lähemmin tässä
ryhmässä esiintyviä kummankin kielen S4-tapauksia:






\ex.\label{ee_rahapelit} \exfont \small Kuitenkin vain 5, 5 prosenttia osavaltion 18-24-vuotiaista pelasi
rahapelejä netissä viime vuoden aikana. (Araneum Finnicum: pokeri.info)





\vspace{0.4cm} 

\noindent



\ex.\label{ee_zhiljo} \exfont \small По оценкам представителей Минобороны, по состоянию на январь 2012 года в
очереди за квартирами стояли 82 тысячи военнослужащих, из них 49 тысяч
семей уже получили жилье в течение прошлого года. (Araneum Russicum:
spb.gdeetotdom.ru)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_rahapelit} ja \ref{ee_zhiljo} osoittavat, että paitsi määrää
painottava S1-konstruktio, myös vastaava
S4-konstruktio on kehyksisillä resultatiivisilla ilmauksilla käytössä
molemmissa kielissä. Huomionarvoista kyllä, varsinaisia fokaalisia
E3b-tapauksia kummankaan kielen S4-aineistoista ei juurikaan ole löydettävissä.
Sen sijaan tyypilliset S3-tapaukset niin suomessa kuin venäjässäkin 
muistuttavat seuraavia lauseita:




\ex.\label{ee_pontevasti} \exfont \small Hän on itse ajanut asiaa pontevasti kolmen viime vuoden aikana ja on
tyytyväinen siitä, että päätös tehtiin ennen kuin hän kesällä siirtyy
kulttuurineuvokseksi Ruotsin Washingtonin-suurlähetystöön. (Fipress:
Länsi-Savo)






\ex.\label{ee_formanova} \exfont \small Formanova on lähestynyt tasaisesti maailman kärkeä viiden viime vuoden
aikana. (Fipress: Länsi-Savo)






\ex.\label{ee_novatek} \exfont \small НОВАТЭК постепенно наращивает дивиденды в течение последних шести лет.
(RuPress: РБК Дейли)






\ex.\label{ee_lazernyje} \exfont \small Лазерные проигрыватели, микрокомпьютеры, текстовые редакторы,
микроволновые печи, видеокамеры, магнитофоны, автомобильные кондиционеры
существенно изменили характер нашей работы и досуга в течение последних
двадцати лет. (Araneum Russicum: economics.wideworld.ru)






\ex.\label{ee_palomnikov} \exfont \small В связи с ожидаемым наплывом паломников из-за рубежа власти Саудовской
Аравии уже оповестили жителей королевства, что те из них, кто уже
совершал хадж в течение последних пяти лет, в этом году право на
паломничество не получат. (RuPress: РИА Новости)






\ex.\label{ee_slyshim} \exfont \small Мы слышим их от Израиля в течение последних восьми лет. (Araneum
Russicum: irn.ved.gov.ru)





\vspace{0.4cm} 

\noindent
Lauseissa \ref{ee_pontevasti} -- \ref{ee_slyshim} ajanilmauksen diskurssifunktio on oikeastaan
ennemmin topikaalinen kuin fokaalinen. Nimitänkin näiden esimerkkien ilmentämää
konstruktiota *topikaaliseksi S4-konstruktioksi*, joka on tarkemmin kuvattu
matriisissa 27 (vrt. ei--fokaalinen konstruktio alempana 
matriisissa 28).



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & topic \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}




\noindent \headingfont \small \textbf{Matriisi 27}: Topikaalinen S4-konstruktio. \normalfont \vspace{0.6cm}

Edellisen esimerkkiryppään lauseissa voi todella ajatella, että ajanilmauksella
on selkeä referentti (esimerkiksi viisi edellistä vuotta esimerkissä \ref{ee_formanova}), 
josta kirjoittaja kertoo jonkin proposition. Usein lauseissa on toinenkin topiikki,
subjekti, josta yhtä lailla kerrotaan (esimerkin \ref{ee_formanova} voi nähdä kertovan
sekä Formanovasta että viidestä viimeksi kuluneesta vuodesta). Esimerkissä \ref{ee_lazernyje}
kuitenkin kyse on ennemminkin kokonaan uudesta tapahtumasta, joka 
lukijalle kerrotaan, jolloin ainoa lukijalle jo tuttu ankkuri on ajanilmauksen
*в течение последних двадцати лет* referentti aikajanalla.

E3a- ja E3b-aineistojen referentiaalisuuteen liittyvä ero näkyykin mielenkiintoisella
tavalla siinä, että lähtökohtaisesti referentiaaliset ja menneisyyteen
viittaavat E3b-ilmaukset  sijoittuvat
kummassakin kielessä S4-asemaan koko lailla yhtä usein, kun taas ei--referentiaalisissa
ja usein tulevaisuuteen tai nykyisyyteen viittaavissa E3a-tapauksissa suomen S4-asema on lähes
kaksi kertaa niin suosittu kuin venäjän S4-asema (ks. kuvio  ). 
Toisin kuin E3b-lauseissa, E3a-aineiston S4-tapauksista suuri osa on fokaalisia, kuten
seuraavat:




\ex.\label{ee_likvidnaja} \exfont \small Всякая ликвидная квартира сегодня находит жильца в течение недели.
(RuPress: Комсомольская правда)






\ex.\label{ee_hakemus} \exfont \small Hakemus rahojen saamiseksi on toimitettava puolen vuoden kuluessa eli
viimeistään tammikuun alkupuolella. (FiPress: Helsingin Sanomat)





\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_likvidnaja} ja \ref{ee_hakemus} ajanilmaus välittää nimenomaan pragmaattiseen
väittämään kuuluvan, lukijalta puuttuvan tiedon jonkin tapahtuman tai asiaintilan
voimassaolon kestosta. Suomenkielisen aineiston esimerkit \ref{ee_pohjanmereen} ja \ref{ee_tatte}
edustavat kuitenkin E3a-aineiston ei--fokaalisia S4-tapauksia, jollaisia
venäjän E3a-ryhmästä ei juuri ole löydettävissä:




\ex.\label{ee_pohjanmereen} \exfont \small Kymmenen osallistujan Pohjanmeri - kokous kielsi perjantaina kaikkien
ympäristömyrkkyjen upottamisen Pohjanmereen 25 vuoden kuluessa.
(FiPress: Karjalainen)






\ex.\label{ee_tatte} \exfont \small Laulua varten Tatte kokosi valituksenaiheita tamperelaisilta
tieteentekijöiltä kevään kuluessa. ( Araneum Finnicum: tatte.fi)





\vspace{0.4cm} 

\noindent
Toisin kuin edellä esitetyissä E3b-esimerkeissä, esimerkeissä \ref{ee_pohjanmereen} ja \ref{ee_tatte} on 
vaikea nähdä ajanilmauksia diskurssifunktioltaan selkeästi topikaalisina. Pikemminkin kyse on 
seuraavassa osiossa tarkemmin käsiteltävästä ei--fokaalisesti konstruktiosta 
(ks. matriisi 28 ), jonka käytöllä on paljon selitysvoimaa pohdittaessa
suomen ja venäjän välillä tavallisesti havaittavaan eroon S4-osuuksissa. Kuten 
kuvio  ja 
esimerkit \ref{ee_pontevasti} -- \ref{ee_slyshim} osoittavat, E3b-aineistossa suomen ja venäjän
S4-asemia käytetään kuitenkin hyvin pitkälle samantyyppisiin konstruktioihin, minkä tähden
tässä aineistoryhmässä kielten välinen ero on tavallista pienempi ja kehyksiset resultatiiviset
ilmaukset ylipäänsä nostavat venäjän S4-todennäköisyyttä.

E3b-ryhmän jälkeen toiseksi pienimmät erot suomen ja venäjän välillä havaittiin
kuvion  perusteella likimääräisessä LM1-ryhmässä.
Luvussa \ref{likim-s1} todettiin likimääräisten ilmausten muodostavan hyvin
pienen osa-aineiston (yhteensä 134 suomen-
ja158 venäjänkielistä tapausta), joka
eroaa myös leksikaaliselta merkitykseltään kielten välillä jonkin verran.
Kummassakin kielessä likimääräiset ilmaukset ovat keskimääräistä tavallisempia
S4-sijoittujia, mutta erityisen suuri ero keskimääräiseen on venäjässä, jossa
 50 % likimääräisistä ilmauksista saa
asemakseen S4:n. Venäjässä, jossa S4 on kauttaaltaan suomea harvinaisempi, likimääräisten
ilmausten voi tulkita korostuvan sen tähden, että ne etenkin 
venäjän *около * + genetiivi -rakenteissa ovat tavallista useammin
osana fokaalista konstruktiota, kuten seuraavassa esimerkissä:




\ex.\label{ee_roditeli} \exfont \small Родители Быстрова покинули свадьбу около двух часов ночи. (RuPress:
Комсомольская правда)





\vspace{0.4cm} 

\noindent
Suomessakin likimääräisten ilmausten funktiot S4-asemassa ovat koko lailla rajoittuneet fokaaliseen
konstruktioon, jota myös seuraava esimerkki ilmentää:




\ex.\label{ee_kankkunen} \exfont \small Tommi Mäkinen, Juha Kankkunen ja kumppanit ylittävät Kangasalantien
puoli kahden t ietämillä. (FiPress: Kangasalan Sanomat)





\vspace{0.4cm} 

\noindent
Kehyksisen resultatiivisen ja likimääräisen funktion ohella kuviossa  
tarkasteltiin sekventiaalisia ajanilmauksia, jotka -- aivan kuten muutkin 
tässä käsitellyt semanttiset funktiot -- ovat siitä erityisiä, että
niiden havaittiin luvussa \ref{ajanilmaukset-ja-alkusijainti}
kasvattavan suomen S1-sijaintia suhteessa venäjään. Käytännössä 
relevantti S4-sijainnin kannalta on ryhmä L6a, jonka S4-osuus suomessa
on vain hivenen yli koko aineiston keskiarvon, mutta venäjässä 
taas koko aineiston keskiarvoon nähden enemmän kuin kaksinkertainen.
Kuten likimääräiset tapaukset, myös sekventiaaliset tapaukset 
ovat luonteeltaan sellaisia, että ne esiintyvät todennäköisesti
osana fokaalista konstruktiota, jota edustavat muun muassa
seuraavat tapaukset:




\ex.\label{ee_dostavim} \exfont \small Мы доставим их в магазин через несколько дней. (Araneum Russicum:
freska-auto.ru)






\ex.\label{ee_betset} \exfont \small Elementtiasentajat aloittavat hommat kahden viikon päästä ja Betset
tekee viimeiset elementit valmiiksi tällä viikolla. (Araneum Finnicum:
uuteenkotiin.wordpress.com)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_dostavim} ja \ref{ee_betset} ovat samanlaisia fokaalisen konstruktion
ilmentymiä kuin esimerkit \ref{ee_roditeli} ja \ref{ee_kankkunen} yllä.
L6a-aineistossa suomen ja venäjän välillä havaittava LM1- ja E3b-aineistoja
suuremman eron voi nähdä olevan peräisin esimerkin \ref{ee_soininvaara} ja \ref{ee_baltia}
kaltaisista tapauksista, joita suomenkielinen aineisto sisältää
ja joissa lauseen lopussa on paitsi ajan, myös paikanilmaus. Esimerkissä 
\ref{ee_baltia} ajanilmauksen voi oikeastaan ajatella määrittävän ennemmin 
paikanilmausta kuin itse lauseessa ilmaistavaa propositiota tai lauseen
pääverbiä:




\ex.\label{ee_soininvaara} \exfont \small Soininvaara aikoo tiettävästi ottaa asian esille parin viikon päästä
sosiaalipoliittisessa ministerityöryhmässä. (Fipress: Länsi-Savo)






\ex.\label{ee_baltia} \exfont \small Baltian maat ovat myös saaneet kutsun EU:n seuraavaan huippukokoukseen
parin viikon päästä Cannesissa. (Fipress: Länsi-Savo)





\vspace{0.4cm} 

\noindent
Kuviossa 47 tässä käsiteltyjen kolmen funktion
lisäksi suomen S4-todennäköisyyttä pienentävinä semanttisina funktioina
esiintyvät simultaaninen ja varsinainen resultatiivinen  funktio, joskin 
etenkin jälkimmäisen kohdalla suomen negatiivinen S4-vaikutus on pieni
ja jossain määrin epävarma. Simultaanisia ajanilmauksia -- joiden
on jo edellä monesti todettu muodostavan koko aineiston suurimman
ja heterogeenisimmän funktion -- koskenee kuitenkin koko lailla sama havainto
kuin sekventiaalisia ja likimääräisiä: simultaanisissa ilmauksissa
on paljon esimerkkien \ref{ee_arbour} ja \ref{ee_kposadke} kaltaisia
fokaalisia tapauksia




\ex.\label{ee_arbour} \exfont \small Edellisen kerran Jugoslavia esti Arbourin pääsyn Kosovoon viime
maaliskuussa. (FiPress: Keskisuomalainen)






\ex.\label{ee_kposadke} \exfont \small Подготовка к посадке деревьев началась ещё на прошлой неделе. (Araneum
Russicum: prirodasibiri.ru)





\vspace{0.4cm} 

\noindent
Simultaanisen ryhmän sisällä havaitaan kuitenkin merkittäviäkin 
aineistoryhmäkohtaisia eroja, joihin deiktisten ja positionaalisten
ilmausten osalta perehdytään tarkemmin seuraavassa.



### Deiktiset adverbit ja positionaalisuus

\todo[inline]{Sido yleiseen tilastolliseen malliin ja kuvioihin edellä}

Monet edellä käsitellyistä suomen ja venäjän loppusijaintia koskevista
eroista ovat liittyneet loppu- ja keskisijainnin väliseen
suhteeseen. Niin morfologisen rakenteen kuin
positionaalisuuden osalta loppusijainti vertautuu kuitenkin erityisen selvästi alkusijaintiin.
Kuvio 54 esittää
deiktisten adverbien ja positionaalisten ilmausten jakautumisen eri sijainteihin.
Deiktiset adverbit on esitetty ylä- ja positionaaliset ilmaukset alarivillä;
sarakkeissa ovat eri sijainnit, niin että vertailun kannalta oleellisimmat eli
S1- ja S4-sijainnit on tummennettu.

![\noindent \headingfont \small \textbf{Kuvio 54}: Deiktisten adverbien ja positionaalisten ilmausten suhteellinen jakautuminen eri sijainteihin \normalfont](figure/dposplot-1.pdf)

Tutkimuksen yleisen tilastollisen mallin perusteella (ks. kuviot 16, 
17, 48 ja 49)
on mahdollista päätellä, että deiktiset adverbit ja positionaaliset ilmaukset kasvattavat suomessa
S4-aseman todennäköisyyttä venäjään verrattuna. Tämä näkyy etenkin kuvion 54 
positionaalisia ilmauksia kuvaavalla alarivillä. Alarivissä keskellä näkyvä
keskisijaintiin osuvien positionaalisten ilmausten määrä on suomessa ja venäjässä kutakuinkin samanlainen,
mutta vasemman reunan (S1-asema) ja oikean reunan (S4-asema) kielikohtaiset pylväät ovat
käytännössä toistensa peilikuvia: venäjässä noin 40 prosenttia positionaalisista ilmauksista
sijoittuu S1-asemaan, suomessa taas S4-asemaan. Vastaava peilikuvamainen symmetria näkyy myös kuvion 
54 deiktisiä adverbejä kuvaavalla ylärivillä, joskin
positionaalisia ilmauksia epätasaisimmin, sillä kielten välillä on selvemmin eroja myös keskisijainnin käytössä. 
Toisaalta  voidaan todeta, että positionaalisilla ilmauksilla venäjän S1-aseman osuus 
on 5,62-kertainen suomeen
ja suomen S4-aseman osuus tasan 4,00-kertainen 
venäjään verrattuna, mutta deiktisillä adverbeillä vastaavat luvut 
ovat 4,75 (venäjän S1 verrattuna suomen S1:een)
ja peräti 8,00 (suomen S4 verrattuna venäjän S4:ään).

\todo[inline]{TODO: viittaa aineistolukuun}

Oikeastaan positionaalisia ilmauksia ja deiktisiä adverbeja tutkittaessa
voidaan nostaa esiin kaksi huomiota herättävää seikkaa: ensinnäkin S4-sijainnin
silmiinpistävä harvinaisuus venäjän deiktisillä adverbeillä ja toiseksi
S4-sijainnin huomattava yleisyys suomen positionaalisilla ilmauksilla.
Koko venäjän valtavassa, 19 028 lausetta
kattavassa deiktisten adverbien aineistossa on ainoastaan 
  445 S4-tapausta.
Tarkemmin luokiteltuna tapaukset jakautuvat aineistoryhmittäin seuraavan
taulukon mukaisesti (vertailun vuoksi taulukossa on esitetty myös vastaavat
suomenkieliset luvut):


--------------------------------------------------------------------------------------------------------------
&nbsp;   L1a            L1b            L1c            L5c           L8a            L8b           L8c          
-------- -------------- -------------- -------------- ------------- -------------- ------------- -------------
fi       743 (19,46%)   629 (21,59%)   234 (33,33%)   372 (8,45%)   578 (26,44%)   179 (9,25%)   144 (8,29%)  

ru       91 (1,86%)     66 (1,54%)     87 (6,75%)     83 (2,58%)    58 (2,74%)     13 (0,95%)    47 (2,56%)   
--------------------------------------------------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 17}: S4-tapaukset deiktisiä adverbejä edustavissa aineistoryhmissä \normalfont \vspace{0.6cm}

Taulukossa 17  huomiota herättävät edellä (ks. luku \ref{l1-erot-kesk})
tarkasti käsitellyt  L1-aineiston ilmaukset, joissa kaikissa suomi käyttää
S4-sijaintia hyvinkin laajasti (L1c-ryhmässä peräti 33,33 %) mutta
joissa venäjänkielisiä tapauksia on niin vähän, että niitä voidaan tarkastella käytännössä yksitellen.
Seuraavat virkkeet ovat esimerkkejä venäjän L1b- ja L1c-aineistojen S4-tapauksista:




\ex.\label{ee_kokis} \exfont \small Наши инвестиции в российскую экономику оправдывают себя уже сегодня.
(Araneum Russicum: coca-colarussia.ru)






\ex.\label{ee_banju} \exfont \small При заявке в нашу компанию, мы доставим готовую деревянную баню уже
завтра. (Araneum Russicum: metaldom-stroy.ru)






\ex.\label{ee_otvetzavtra} \exfont \small Я дам тебе ответ завтра, - ответил он, чуть поджав губы. (Araneum
Russicum: fan.lib.ru)






\ex.\label{ee_zhdetnas} \exfont \small Не можем мы знать что ждет нас завтра и какие события настигнут каждую
минуту. (Araneum Russicum: moving.ru)





\vspace{0.4cm} 

\noindent
Esimerkit \ref{ee_kokis} -- \ref{ee_otvetzavtra} vertautuvat informaatiorakenteeltaan
edellä käsiteltyihin duratiivisiin esimerkkeihin
\ref{ee_palomniki} -- \ref{ee_baletom}, jotka tulkittiin fokaalisen konstruktion edustajiksi.
Esimerkeissä  \ref{ee_kokis} ajatus investointien oikeutuksesta samoin kuin esimerkissä \ref{ee_banju}
ajatus puisen saunan valmistamisesta ja esimerkissä \ref{ee_otvetzavtra} vastauksen antamisesta
ovat kaikki diskurssireferenttejä, joiden kirjoittaja olettaa olevan kuulijan
tajunnassa aktiivisena. Ajanilmauksen tehtävänä luetelluissa
esimerkeissä on välittää lukijalle ajallista, lokalisoivaa tietoa näistä propositioista, ja 
ajanilmauksen sijaitseminen sekä verbin että objektin jäljessä korostaa
oletusta näiden ilmaiseman proposition aktiivisuudesta.

On kuitenkin huomattava, että kuten duratiivisten ilmausten kohdalla, myös 
deiktisiä adverbejä tarkasteltaessa vastaavia diskurssifunktioita toteutaan
venäjänkielisessä aineistossa usein myös keskisijainnilla kuten esimerkissä \ref{ee_aromaty} ja
toisaalta alkusijainnilla kuten esimerkissä \ref{ee_trjuki}:




\ex.\label{ee_aromaty} \exfont \small Волшебные ароматы Востока уже завтра могут заполнить ваш дом, неся вам
радость, свет и вдохновение. (Araneum Russicum: aromati-vostoka.ru)






\ex.\label{ee_trjuki} \exfont \small Уже завтра зрители увидят невероятные трюки главных артистов. (Araneum
Russicum: lotosgtrk.ru)





\vspace{0.4cm} 

\noindent
\todo[inline]{Huomioita siitä, että zavtralle fok.konstr.luontevin }

Deiktisistä adverbeistä voidaan siis lähtökohtaisesti todeta, että
yksi syy venäjän S4-sijainnin harvinaisuudelle suhteessa suomeen on se, että
-- niin kuin duratiivisten ilmausten kohdalla jo havaittiin -- fokaalinen konstruktio
on venäjässä tavallinen myös muissa sijainneissa. 

Positionaalisten ilmausten tapauksessa S4 on venäjässä kauttaaltaan
tavallisempi kuin deiktisten adverbien kohdalla, joskin eri aineistoryhmien
välillä on myös jonkin verran vaihtelua.
Taulukko 18 esittää positionaalisten aineistoryhmien S4-tapausten
tarkat määrät ja suhteelliset osuudet:



----------------------------------------------------------------------
&nbsp;   F1b            L2a             L2b             L4a           
-------- -------------- --------------- --------------- --------------
fi       613 (68,19%)   1507 (33,65%)   1496 (44,16%)   466 (33,41%)  

ru       49 (36,84%)    278 (10,05%)    123 (9,41%)     19 (3,69%)    
----------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 18}: S4-tapaukset positionaalisissa aineistoryhmissä \normalfont \vspace{0.6cm}

Taulukossa 18 merkillepantavia ovat toisaalta F1b-ryhmän (ks. osio \ref{s1-f1b})
huomattavan korkea suhteellinen osuus sekä toisaalta L4a-ryhmän (osio \ref{l4a-l4b})
poikkeuksellisen pieni S4-osuus. L4a-aineisto kattaa venäjässä vain kourallisen S4-tapauksia, joista
esimerkkejä ovat virkkeet \ref{ee_blatter} ja \ref{ee_bespokoit}:




\ex.\label{ee_blatter} \exfont \small Президент ФИФА Блаттер поздравил меня вечером. (RuPress: Комсомольская
правда)






\ex.\label{ee_bespokoit} \exfont \small Сравнительно интенсивное движение автомобилей побеспокоит вас только
утром и вечером-\/- (Araneum Russicum: v-nova.ru)





\vspace{0.4cm} 

\noindent
Huomionarvoista on, ettei venäjän L4a-aineiston vähien S4-tapausten joukosta
ole löydettävissä käytännössä yhtään fokaalisen konstruktion edustajaa. Esimerkissä \ref{ee_blatter}
on kyse tapauksesta, jossa ajanilmauksen diskurssifunktio on puhtaan kehyksinen: kirjoittaja
ei oletettavasti pyri ilmoittamaan sitä, milloin Blatter onnitteli tätä vaan itse tapahtuman, proposition
[Blatter onnitteli minua], josta sitten tarkennetaan, että tämä tapahtui illalla.
Kenties lähimpänä fokaalista konstruktiota on esimerkki \ref{ee_bespokoit}, jossa siinäkin on pantava merkille, 
että *утром и вечером* viittaavat aamuihin ja iltoihin yleensä, eivät johonkin konkreettiseen,
aikajanalta erotettavissa olevaan referenttiin. Tämä on suomenkielisen aineiston valossa
odottamatonta, sillä yksi syy suomen suureen S4-tapausten määrään vaikuttaisi olevan
juuri tämänkaltaisissa fokaalisissa konstruktioissa, joita edustavat muun muassa
seuraavat esimerkit:




\ex.\label{ee_netanyahu} \exfont \small Pääministerin kanslia ilmoitti myöhemmin päivällä, että Netanyahu antaa
vaaleja koskevan lausunnon illalla. (Fipress: Länsi-Savo)






\ex.\label{ee_satamat} \exfont \small Satamien pitäisi aloittaa toimintansa taas aamulla. (Araneum Finnicum:
m.iltalehti.fi)






\ex.\label{ee_seijamake} \exfont \small Seija, Make, Matti ja Jonttu olivat grillanneet viimeiset neljä
makkaraansa illalla. (Araneum Finnicum: mahti.pp.fi)






\ex.\label{ee_vihreapaatos} \exfont \small Vihreä liitto teki päätöksensä illalla. (FiPress: Karjalainen)





\vspace{0.4cm} 

\noindent
Osittain myös L4a-aineistossa fokaalisten tapausten puuttumista voi selittää sillä, että 
ajanilmaus voi venäjässä saada fokuksen diskurssifunktion keski- tai alkusijaintien
avulla, kuten seuraavissa esimerkeissä:




\ex.\label{ee_tolkoutrom} \exfont \small О случившемся тут же сообщили супруге, которая только утром навестила
мужа. (RuPress: РИА Новости)







\exg.  Ожидается, что уже вечером он возобновит работу. (Araneum Russicum:
medpulse.ru)
\label{ee_vozobnovit}  
 Odotetaan etä jo illalla hän jatkaa työ-AKK
\





\ex.\label{ee_povtorvstretshy} \exfont \small Желающие могут вечером повторить встречу заката. (Araneum Russicum:
ukok-tour.ru)





\vspace{0.4cm} 

\noindent



Esimerkit \ref{ee_tolkoutrom} -- \ref{ee_povtorvstretshy} eivät ole ehkä yhtä selkeästi
fokaalisia kuin vaikkapa esimerkki \ref{ee_netanyahu} edellä, mutta niissä yhtä
kaikki kuvataan jotakin kuulijan tajunnassa aktiiviseksi oletettavaa
propositiota ([puoliso kertoi asiasta x miehelleen],[subjekti jatkaa
työtään],[auringonlaskun kohtaaminen]) toisin kuin esimerkiksi virkkeessä
\ref{ee_blatter} edellä. On itse asiassa todettava, että esimerkin \ref{ee_vozobnovit}
kaltaisia *уже + утром/вечером*- tai esimerkin \ref{ee_tolkoutrom} kaltaisista
*только + утром/вечером* -tapauksia on venäjänkielisessä aineistossa kaikkiaan
  20, mutta näistä vain yksi sijoittuu S4-asemaan, siinä missä
suomen 57 vastaavan *jo/vasta + aamulla/illalla*-tapauksen osalta
S4 on tavallisin sijainti (25 tapausta). Tästä
huolimatta vastaavien tapausten selitysvoimaa voi tuskin pitää riittävänä, kun
mietitään esimerkiksi venäjänkielisten L4a- ja L2-ryhmien välistä sisäistä eroa
S4-osuuksissa. Yksi mahdollinen lisäselitys olisi olettaa, että kyse ei venäjän
L4a-ryhmän osalta ole vain siitä, että fokaalisia S4-tapauksia on suomea
vähemmän, vaan siitä, että fokaalisia tapauksia on *ylipäätään* vähemmän. Tätä
oletusta tukee se, että verrattaessa venäjän positionaalisia ryhmiä keskenään
L4a erottuu selvästi muista, mutta vastaavaa ryhmien välistä eroa ei havaita
suomessa, jossa L4a-ryhmän S4-osuus on kutakuinkin yhtä suuri kuin
L2a-ryhmällä. On siis mahdollista, että venäjän *утром*- ja *вечером*-sanoihin
liittyvät diskurssifunktiot ovat lähtökohtaisesti useammin topikaalisia (ja
siten painottuneita lauseen alkupuolelle) kuten esimerkissä \ref{ee_legkimutrom}:




\ex.\label{ee_legkimutrom} \exfont \small Этим легким и летним утром я покинула мягкую постель, и начала
собираться в гости. (Araneum Russicum: rbp-place.ru)





\vspace{0.4cm} 

\noindent
Jos suomessa pyrkisi lisäämään aamuun tai iltaan viittaavaan ajanilmaukseen esimerkin \ref{ee_legkimutrom} tapaisia
adjektiivimääritteitä, olisi käytettävä tämän tutkimuksen aineistoon otettujen adessiivimuotojen (*aamulla* ja *illalla*)
sijaan essiivimuotoja *aamuna* ja *iltana*, mikä saattaa osaltaan selittää sitä, että suomessa lauseenloppuinen
asema on suhteessa niin paljon suositumpi kuin venäjässä. 

\todo[inline]{Tähän kohtaan vielä vertailu L4b-ryhmään}

Venäjän ja suomen L4a-ryhmät sisältävät siis jossain määrin jo lähtökohtaisesti
erilaisen jakauman ajanilmausten eri diskurssifunktioita. Jo todetun lisäksi on
syytä muistaa edellä osiossa \ref{l4a-l4b} mainittu huomio siitä, että venäjän *утром* ja
*вечером* ovat siinä määrin vakiintuneita ilmauksia, ettei niitä kieliopeissa
ole tapana luokitella *утро*- ja *вечер*-substantiivien taivutusmuodoiksi vaan
omiksi lekseemeikseen. Tämän voisi nähdä yhtenä mahdollisena adverbeille
tyypillisen keskisijainnin osuutta kasvattavana tekijänä, joskin on todettava,
että L4a-aineistossa suomen ja venäjän välinen ero muodostuu ennen kaikkea
erosta S1- ja S4-sijaintien välillä.


L4a-ryhmän matalan S4-osuuden lisäksi taulukosta 18 
havaitaan, että F1b-ryhmässä S4-sijainnin todennäköisyys on huomattavan suuri
kielestä riippumatta. F1b-ryhmää edustava esimerkki (\ref{ee_hessu}) esitettiin jo aivan luvun alussa
yhtenä tyypillisenä fokaalisen konstruktion ilmentymänä, ja epäilemättä juuri fokaalisten
tapausten suuri määrä selittää F1b-ryhmän painottumista lauseen loppuun. Myös L2-ryhmissä 
venäjän S4 on selvästi tavallisempi kuin esimerkiksi deiktisillä adverbeillä
(ks. taulukko 17 ), mutta tästä huolimatta ero
suomeen on hyvin selvä. L2-ryhmiä analysoitaessa onkin syytä palata luvussa
\ref{s1-sim-pos} tehtyihin L2-ryhmiä ja alkusijaintia koskeneisiin huomioihin.

Luvussa \ref{s1-sim-pos} havaittiin, että suomen ja venäjän L2-ryhmien
alkusijaintitapaukset eroavat toisistaan ennen kaikkea siinä, että
venäjässä toisin kuin suomessa L2-ajanilmaukset sijaitsevat usein lauseen alussa
ilman määritteitä. Suomen S1-tapauksien todettiin
useimmiten olevan joko alatopiikkikonstruktion, kontrastiivisen konstruktion,
äkkiä-konstruktion tai affektiivisen konstruktion edustajia sekä joskus lokaaleita
johdantokonstruktioita, kun
taas venäjässä myös globaalit johdantokonstruktiot todettiin tavallisiksi. Esimerkkejä tällaisista
tapauksista olivat L2a-ryhmän virkkeet \ref{ee_livija} ja \ref{ee_wto}, jotka esitän tässä uudelleen
numeroilla \ref{ee_livija2} ja \ref{ee_wto2}:




\ex.\label{ee_livija2} \exfont \small В среду Госдума на очередном пленарном заседании рассмотрит ситуацию в
Ливии. (RuPress: Новый регион 2 2011)






\ex.\label{ee_wto2} \exfont \small Во вторник в штаб-квартире ВТО в Женеве президент Украины Виктор Ющенко
и генеральный директор организации Паскаль Лами подписали протокол о
присоединении Украины к Всемирной торговой организации. (RuPress: РИА
Новости 2008)





\vspace{0.4cm} 

\noindent
Mielenkiintoista kyllä, vaikka suomen L2a-aineistosta ei löytynyt juuri
lainkaan esimerkkien \ref{ee_livija2} -- \ref{ee_wto2} kaltaisia S1-tapauksia
(poikkeuksena katso esimerkki \ref{ee_uusitalo}), on vastaavien S4-tapausten esiin
kaivaminen helppoa, kuten virkkeet \ref{ee_euministerit} ja \ref{ee_hyvinkaaseura}
osoittavat:




\ex.\label{ee_euministerit} \exfont \small EU:n liikenneministerit käsittelevät kuljettajien työaikoja
kokouksessaan \emph{keskiviikkona}. (FiPress: Hämeen sanomat 1999)






\ex.\label{ee_hyvinkaaseura} \exfont \small Hyvinkään Maatalousseura luovutti käytetyn kuivurin virolaisille
\emph{torstaina} maaseutukeskuksessa Järvenpäässä. (FiPress: Hyvinkään
sanomat 1994)





\vspace{0.4cm} 

\noindent
Esimerkkien \ref{ee_euministerit} ja \ref{ee_livija2} yhtäläisyys on silmiinpistävää: kummassakin on kyse aktivoimattoman
subjektin jonkinlaisessa kokouksessa suorittamasta toiminnasta, jonkin kysymyksen käsittelemisestä. Vaikka esimerkin 
\ref{ee_euministerit} laajempaa kontekstia ei ole saatavilla, voidaan olettaa, että se kuitenkin eroaa
esimerkistä \ref{ee_livija2} siinä, että kyseessä tuskin on koko tekstin ensimmäinen virke. Yhtä kaikki,
vaikuttaisi siltä, että suomessa esimerkkien \ref{ee_livija2} -- \ref{ee_wto2} kaltaisia viestintätehtäviä toteutetaan
ennemmin loppu- kuin alkusijainnilla. 

Suomen ja venäjän alku- ja loppusijaintien välinen symmetria L2-ryhmissä on nähtävissä laajemminkin
kuin vain yksittäisten esimerkkien tasolla. Luvussa \ref{s1-sim-pos} tarkasteltiin ajanilmausten kollokaatteja
ja havaittiin, että venäjässä oli suomeen nähden huomattavan paljon enemmän tapauksia, joissa ajanilmaus 
sijaitsi koko lauseen ensimmäisenä konstituenttina. Jos taas tutkitaan
S4-asemaan sijoittuvien L2a-tapausten oikeanpuoleisia kollokaatteja, saadaan taulukon 19 





\FloatBarrier 

\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
--- & 912 & 60.52\\
ja & 79 & 5.24\\
kun & 32 & 2.12\\
mutta & 21 & 1.39\\
jolloin & 11 & 0.73\\
\addlinespace
helsingin & 8 & 0.53\\
aamupäivällä & 7 & 0.46\\
helsingissä & 7 & 0.46\\
torstaina & 7 & 0.46\\
joten & 5 & 0.33\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
--- & 39 & 14.03\\
в & 34 & 12.23\\
на & 16 & 5.76\\
и & 10 & 3.60\\
вечером & 6 & 2.16\\
\addlinespace
то & 6 & 2.16\\
а & 5 & 1.80\\
10 & 4 & 1.44\\
16 & 4 & 1.44\\
17 & 4 & 1.44\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 19}: L2a-aineistoryhmän yleisimmät oikeanpuoleiset kollokaatit suomessa ja venäjässä \normalfont \vspace{0.6cm} 

 


 \FloatBarrier 

L2b-ryhmän oikeanpuoleiset kollokaatit on puolestaan esitetty taulukossa 20:


 \FloatBarrier 

\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
--- & 888 & 59.36\\
ja & 135 & 9.02\\
kun & 34 & 2.27\\
mutta & 27 & 1.80\\
jolloin & 22 & 1.47\\
\addlinespace
tai & 7 & 0.47\\
eikä & 6 & 0.40\\
joten & 6 & 0.40\\
viime & 5 & 0.33\\
koska & 4 & 0.27\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
и & 16 & 13.01\\
прошлого & 15 & 12.20\\
когда & 10 & 8.13\\
в & 7 & 5.69\\
этого & 7 & 5.69\\
\addlinespace
а & 6 & 4.88\\
но & 5 & 4.07\\
на & 4 & 3.25\\
--- & 3 & 2.44\\
не & 3 & 2.44\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 20}: L2b-aineistoryhmän oikeanpuoleiset kollokaatit suomessa ja venäjässä \normalfont \vspace{0.6cm} 
\FloatBarrier 

\FloatBarrier

\todo[inline]{TODO: log-likelihood tms. }

Taulukot 19 ja 20
osoittavat muun muassa sen, että kummassakin kielessä on melko tavallista, että
S4-sijoittunutta L2-ajanilmausta seuraa rinnastuskonjunktio tai L2b-aineiston
tapauksessa myös kun-lause. Kielten välillä on kuitenkin selvä ero siinä,
kuinka suuri osuus on tapauksilla, joissa ajanilmaus on koko virkkeen
viimeisenä eli ilman kollokaattia -- ero on peilikuva siitä, mitä S1-sijainnin yhteydessä
havaittiin vasemmanpuoleisista kollokaateista.[^ll_l2] Lisäksi vaikuttaisi
ainakin L2a-ryhmän osalta siltä, että venäjä sisältää enemmän tapauksia, joissa
ajanilmauksella kuvataan lopulta tarkempaa hetkeä kuin viikonpäivää ylipäätään.
Tällaisia ovat toisaalta *вечером* ('illalla') -tapaukset, toisaalta tapaukset,
joissa ajanilmauksen ensimmäisenä oikeanpuoleisena kollokaattina on jokin
numero ja toisaalta suuri osa tapauksista, joissa ajanilmausta seuraa
в-prepositio. Virkkeet \ref{ee_mintorg} -- \ref{ee_uvidjatsvet} ovat esimerkkejä näistä: 

[^ll_l2]: (TODO -- Bayesiläisittäin?) Jos vertaa kollokaatittomia tapauksia suomessa ja venäjässä,
saadaan esimerkiksi log-likelihood-arvo  92,576.




\ex.\label{ee_mintorg} \exfont \small Министерство торговли опубликует данные по дефициту внешней торговли за
октябрь во вторник в 13.30 по Гринвичу. (Araneum Russicum:
markets-today.ru)






\ex.\label{ee_kazan} \exfont \small Известие потрясло Казань во вторник в середине дня. (RuPress:
Комсомольская правда)






\ex.\label{ee_poslednijraz} \exfont \small Последний раз я видела Людмилу во вторник 24 октября 2000 года. (Araneum
Russicum: eparhia.karelia.ru)






\ex.\label{ee_uvidjatsvet} \exfont \small Основным событием недели для австралийского доллара станут данные по
рынку труда, которые увидят свет в четверг 14 марта в 04.30 МСК.
(Araneum Russicum: fx.ru)





\vspace{0.4cm} 

\noindent
Esimerkkien \ref{ee_mintorg} -- \ref{ee_uvidjatsvet} kaltaiset tapaukset ovat kaikki
selkeitä fokaalisen konstruktion edustajia. Suomenkielisissä L2-aineistoissa
havaittava suuri kollokaatittomien tapausten määrä viittaa kuitenkin siihen, että
suomessa on vähemmän eksplisiittiseen ajankohtaan
viittaavia ja siten potentiaalisesti juuri tästä ajankohdasta lukijaa
informoivia tapauksia. Tämä havainto antaa syytä olettaa,
että syy suomen loppusijainnin venäjää suurempaan yleisyyteen saattaisi olla
siinä, että loppusijaintia käytetään suomessa venäjää enemmän myös muihin
viestintätehtäviin kuin fokuksen ilmaisemiseen. Edellä esitetyt esimerkit
\ref{ee_euministerit} ja \ref{ee_hyvinkaaseura} antavat viitteitä näiden
viestintätehtävien luonteesta, ja itse asiassa myös deiktisten adverbien
joukosta löytyy paljon vastaanvantyyppisiä tapauksia, kuten esimerkit
\ref{ee_kiinapuhe} -- \ref{ee_vaalipiiri} osoittavat:

\todo[inline]{huom: @tomlin2014, 84 mainitsee, että sentence adverbials sijaitsevat
yleensä joko alussa tai lopussa. jotenkin pitäisi tämä perinteinen
lauseadverbiaali-käsite saada tässä mainittua. Ehkä selitettyä lisäksi, että
tämä ei ole satunnaista, vaan voidaan esittää tiettyjä erityisen tyypillisiä
konstruktioita. }




\ex.\label{ee_kiinapuhe} \exfont \small Pääministeri Zhu Rongji piti Kiinan kansantasavallan 50 -
vuotisjuhlallisuuksiin liittyen puheen Pekingissä eilen. (FiPress:
Kaleva)






\ex.\label{ee_virrankoski} \exfont \small Kyösti Virrankoski tukijoukkoineen jakoi tietoa ja mielipiteitään EU:sta
Jyväskylän kävelykadulla eilen. (FiPress: Keskisuomalainen)






\ex.\label{ee_uusitielaki} \exfont \small Hallitusneuvos Kalevi Perko kertoi uudesta tielaista Tiet taajamassa
-\/- seminaarissa keskiviikkona. (FiPress: Aamulehti)






\ex.\label{ee_europaues} \exfont \small Ja Europaeukset, Tämä suku vietti sukuyhdistyksensä 30-vuotisjuhlaa
Haapavedellä äskettäin. (Araneum Finnicum: europaeus.info)






\ex.\label{ee_vaalipiiri} \exfont \small Hänhän vaihtoi vaalipiiriäkin tässä hiljattain. (FiPress: Turun Sanomat)





\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_euministerit} -- \ref{ee_hyvinkaaseura} ja \ref{ee_kiinapuhe} --
\ref{ee_europaues} on merkillepantavaa, että niissä kaikissa lauseenloppuista
ajanilmausta edeltää paikanilmaus. Esimerkissä \ref{ee_vaalipiiri} ei varsinaisesti
ilmaista paikkaa, vaan siinä ajanilmausta edeltävällä inessiivimuotoisella
pronominilla on ainoastaan diskursiivinen funktio -- voisi jopa tulkita, että
syynä *tässä*-sanan käytölle on juuri kirjoittajan tai puhujan pyrkimys käyttää
lauseenloppuista lokalisoivaa paikka + aika -konstruktiota, vaikka varsinaista
tarvetta paikan ilmaisemiselle ei ole [@tobecited]. Paikan ilmaiseminen ei
kuitenkaan vaikuta olevan vastaavissa konstruktioissa välttämätöntä, kuten
esimerkit \ref{ee_presidenttipelasti} -- \ref{ee_samppa} (sekä venäjänkielisen aineiston
\ref{ee_blatter} edellä) osoittavat:

\todo[inline]{mahd. visk § 1317
tsekkaa myös, miten venäjässä, onko käytössä *тут недавно* -ilmaisua

tsekkaa Huumon väikkärin vika artikkeli: ei-temoraalisten tila-adverbiaalien temporaalisista tulkinnoista

humo s.189 eteenpäin

TODO: harkitse alaotsikoita tähän lukuun

huomauta, että kuukausilla harvinaisempaa.

}





\ex.\label{ee_presidenttipelasti} \exfont \small -\/- Presidentti pelasti minun päiväni tänään. (FiPress: Demari)






\ex.\label{ee_luka} \exfont \small Luka saavutti ensimmäisen kouluratsastuskilpailuvoittonsa tänään!
(Araneum Finnicum: taikaponi.net)






\ex.\label{ee_samppa} \exfont \small Samppa teki ihan täydellistä salaattia meille eilen. (Araneum Finnicum:
miatanh.blogspot.com)





\vspace{0.4cm} 

\noindent
Esimerkeissä \ref{ee_presidenttipelasti} -- \ref{ee_samppa} ajanilmausten tulkitseminen
ei--fokaalisiksi on jossain määrin mutkikkaampaa kuin edellä esitettyjen
paikanilmauksen sisältävien esimerkkien.
Tarkastelenkin seuraavassa esimerkkien \ref{ee_presidenttipelasti} -- \ref{ee_samppa} informaatiorakennetta
tarkemmin jakamalla niihin liittyvät diskurssireferentit osaksi pragmaattista
väittämää ja olettamaa käyttämällä hyväksi taulukkoa 1, johon
on vertailun vuoksi sisällytetty myös selkeän fokaalinen esimerkki \ref{ee_poslednijraz} sekä edellä
esitetyistä paikanilmauksen sisältävistä tapauksista \ref{ee_virrankoski}:

\todo[inline]{todo: ei määrittää substantiivia Kaiken kukkuraksi Teija Sarajärvi kutsui minut ja Lauran aamupalalle huomenna. (Araneum Finnicum: turisti-info.fi)}

\todo[inline]{todo: lisää esimerkkien numerot taulukkoon }


---------------------------------------------------------------------------------------------------
lause                             olettama                         väittämä                        
--------------------------------- -------------------------------- --------------------------------
*\ref{ee_poslednijraz}: Последний      Subjekti ei ole nähnyt L:aa      24.10.2000 = h                  
раз я видела Людмилу во           vähään aikaan. On olemassa                                       
вторник 24 октября 2000 года.*    hetki, jolloin subjekti                                          
                                  viimeksi näki L:n (h)                                            

*\ref{ee_virrankoski}: Kyösti          Mahdollisesti: lehdessä          On olemassa henkilö nimeltä     
Virrankoski tukijoukkoineen       esitetystä kuvasta näkyvä        K.V.. K.V. on jakanut tietoa    
jakoi tietoa ja mielipiteitään    informaatio                      ja mielipiteitä. Jakaminen      
EU:sta Jyväskylän                                                  tapahtui edeltävänä päivänä     
kävelykadulla eilen.*                                                                              

*(\ref{ee_presidenttipelasti})         Lukija tietää                    Presidentti pelasti puhujan     
Presidentti pelasti minun         *minä*-pronominin referentin     päivän. Pelastettu päivä oli    
päiväni tänään.*                  (puhujan). Lukija tietää         se, joka puhehetkellä on ollut  
                                  presidentin                      meneillään                      

*(\ref{ee_luka}) Luka saavutti         Lukija tietää Lukan. Lukija      Luka on saavuttanut             
ensimmäisen                       tietää, että Luka osallistuu     ensimmäisen voittonsa           
kouluratsastuskilpailuvoittonsa   kilpailuihin                     kouluratsastuskilpailussa.      
tänään!*                                                           Voitto saavutettiin sinä        
                                                                   päivänä, joka viestin           
                                                                   lähetyshetkellä oli meneillään  

*\ref{ee_samppa}: Samppa teki ihan     Lukija tietää, kuka on Samppa.   Samppa teki täydellistä         
täydellistä salaattia meille      Lukija tietää, että lauseen      salaattia. Salaatin tekeminen   
eilen.*                           kirjoittaja kuuluu pronominin    tapahtui eilen                  
                                  *meille* identifioimaan                                          
                                  joukkoon                                                         
---------------------------------------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 1}: Esimerkkien \ref{ee_poslednijraz}, \ref{ee_virrankoski} sekä \ref{ee_presidenttipelasti} -- \ref{ee_samppa} pragmaattiset olettamat ja väittämät \normalfont \vspace{0.6cm}


Kuten taulukosta havaitaan, esimerkeissä \ref{ee_presidenttipelasti} -- \ref{ee_samppa} ja \ref{ee_virrankoski}
ennen kaikkea pragmaattiset väittämät ovat erilaisia kuin edellä esitetyissä
fokaalisissa esimerkeissä. Fokaalisissa ajanilmaustapauksissa tyypillistä on,
että pragmaattinen olettama pitää sisällään paljon informaatiota verrattuna
pragmaattiseen väittämään, johon tavallisesti kuuluu esimerkin \ref{ee_poslednijraz}
kaltaisissa lokalisoivissa tapauksissa ainoastaan tieto ajanilmauksen kuvaaman
aikajanan pisteen ja väittämässä esitettyjen propositioiden suhteesta (yllä
kuvattu *24.10.2000 = h*). Esimerkkien \ref{ee_presidenttipelasti} -- \ref{ee_samppa}
samoin kuin esimerkkien \ref{ee_kiinapuhe} -- \ref{ee_vaalipiiri} tapauksissa taas
pragmaattiset väittämät ovat laajempia ja kattavat aina myös jotain
muuta kuin ajanilmaukseen liittyvää tietoa -- vieläpä niin, että muu tieto on
ensisijaista. Pragmaattiset olettamat taas eroavat fokaalisten esimerkkien ja esimerkkien
\ref{ee_presidenttipelasti} -- \ref{ee_samppa} sekä \ref{ee_virrankoski} välillä siinä,
että niiden sisältämä tieto on luonteeltaan erilaista: edellisten tapauksessa
tieto on ennen kaikkea propositionaalista [@yokoyama1986, 11], jälkimmäisten
tapauksessa taas useimmiten referentiaalista (mts. 9).

Pragmaattisen olettaman piiriin kuuluvan tiedon luonne ei--fokaalisissa
esimerkeissä käy hyvin ilmi, kun tutkitaan esimerkin \ref{ee_luka}
esiintymisyhteyttä (http://taikaponi.net/veera/ponipallero/luka.html,
tarkistettu 29.9.2017). Esimerkin kontekstina on kokonainen internetsivu, joka on
omistettu yhdelle tietylle Luka-nimiselle ponille. Sivulla selvitetään Lukan
taustoja, kerrotaan yleistietoja ja tilastoja siitä, minkälaisiin kilpailuihin
hevonen on osallistunut. Sivulla on myös kaksi merkintää sisältävä
*Päiväkirja*-niminen osio, jonka ensimmäinen merkintä alkaa esimerkillä
\ref{ee_luka} ja toinen kertoo ponin syntymästä. Tämä konteksti huomioon ottaen on
selvää, että kirjoittaja olettaa lukijalla olevan paljon referentiaalista
tietoa Lukasta ja että Luka sekä monenlainen siihen liittyvä tieto kuten se,
että poni osallistuu erilaisiin kilpailuihin, on lukijan tajunnassa aktiivista.
Ei ole kuitenkaan mielekästä ajatella, että propositio [Luka saavuttaa
ensimmäisen kouluratsastuskilpailuvoittonsa] olisi aktiivinen ja että
esimerkissä \ref{ee_luka} vain lokalisoitaisiin tämä jo aktiivisena ollut
propositio. Pikemminkin koko propositio esitellään virkkeessä \ref{ee_luka} uutena
ja se on osa pragmaattista väittämää. 

Esimerkki \ref{ee_virrankoski} poikkeaa esimerkistä \ref{ee_luka} siinä, että sen
kirjoittajan voitaisiin tulkita olettavan paljon vähemmän: voisi ajatella, ettei
pragmaattiseen olettamaan kuulu edes esimerkin \ref{ee_luka} kaltaista
referentiaalista tietoa, vaan että käytännössä kaikki esitettävä tieto on
diskurssistatukseltaan aktivoimatonta ja osa pragmaattista väittämää. Tässä
mielessä kyseessä olisi hyvin pitkälle venäjässä tyypillisten
globaalien S1-johdantokonstruktioiden kaltainen rakenne. Esimerkki \ref{ee_virrankoski}
sekä monet muut vastaavat tapaukset ovat suomen lehdistöaineistossa kuitenkin
siitä erityisiä, että niiden voi usein päätellä esiintyvän lehdissä
kuvateksteinä. 

Kuvatekstejä lingvistiseltä kannalta tutkinut Elina Heikkilä
[-@heikkila2006kuvan, 189] toteaa, että kuvatekstien laatimisessa on tavallista
pitää yleisohjeena, että lukijalle annetaan sellaista informaatiota, jota itse
kuvasta ei näy. Heikkilän mukaan (mts. 77) kuvatekstien oletetaan sisällyttävän
vastaukset mahdollisimman moneen kysymyksistä *kuka, mitä, missä, milloin,
miksi ja miten*. Nämä seikat tukevat ajatusta siitä, että kuvatekstit ovat
usein konteksteja, joissa suurin osa esitettävästä tiedosta kuuluu
pragmaattiseen väittämään. Voisikin ajatella, että suomenkielisessä aineistossa
esiintyvät monet kuvatekstit ovat hyvä ikkuna siihen, mihin järjestykseen
informaatio suomessa on luontevaa pakata, kun tehtävänä on välittää suuri
joukko kokonaan uutta ja aktivoimatonta tietoa samassa lauseessa. 
Esimerkin \ref{ee_virrankoski} kaltainen asioiden esittämisjärjestys vaikuttaa
Heikkilän (mts. 77--78) lainaamien suomalaisten kirjoittamisen oppaiden
perusteella suorastaan normilta. Esimerkiksi Antero Okkosen [-@okkonen1980toimittajan]
opas kehottaa *aloittamaan pääasialla ja
välttämään määreitä tekstin alussa*. Lauri Kotilaisen [-@kotilainen2003lehti]
oppaassa puolestaan todetaan, että kuvatekstin tulisi kertoa, mitä kuva esittää,
keitä kuvassa on ja *ehkä myös, milloin kuva on otettu ja missä, jos asia ei
muuten käy ilmi*. 

Palataan vielä taulukon 1 esimerkkeihin.
Virkkeestä \ref{ee_samppa} on todettava, että kysymys ajanilmauksen kuulumisesta
pragmaattisen väittämän tai olettaman piiriin on sen kohdalla muita tapauksia
monitulkintaisempi. Esimerkeissä \ref{ee_presidenttipelasti} ja \ref{ee_luka}
ajanilmauksen roolin voi nähdä melko selvästi kehyksisenä (vertaa luku
\ref{diskurssifunktioiden-tarkempi-erittely}): lauseet *Presidentti pelasti
minun päiväni!* ja *Luka saavutti ensimmäisen kouluratsastuskilpailuvoittonsa!*
voisi hyvin kuvitella myös yksittäisiksi huudahduksiksi ilman ajanilmausta.
Esimerkissä \ref{ee_samppa} ajanilmaus kuitenkin vaikuttaisi jossain määrin
topikaalisemmalta: blogityyppisessä päiväkirjamaisessa kontekstissa
[@tobecited] on luonnollista olettaa, että kirjoittaja kertoo paitsi itsestään
ja läheisistä ihmisistä, myös kuluneista päivistään sinänsä. Tässä mielessä
ajanilmaus on lauseen \ref{ee_samppa} kannalta oleellisempi kuin esimerkiksi
seuraavassa lauseessa:




\ex.\label{ee_aloitus} \exfont \small Myös minä voin tehdä tästä uuden aloituksen huomenna. (Araneum Finnicum:
teosofit.ning.com)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_aloitus} voisi nähdä edustavan ajanilmauksen topikaalisuuden
toista ääripäätä, jossa ajanilmaus on maksimaalisen irrallinen muusta lauseesta.

Vaikka edellä esitellyt lauseet siis eroavat joiltain osin informaatiorakenteen tasolla ja siinä,
onko mukana paikanilmausta vai ei, voi niiden taustalla silti nähdä oman konstruktionsa, jota
sen yleisimmällä tasolla nimitän  yksinkertaisesti *ei--fokaaliseksi S4-konstruktioksi*. 
Konstruktion tarkempi kuvaus
on annettu seuraavassa matriisissa:




\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[frame,pad=4pt,rule=0.1pt]{

df   foc \\
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf obj \\[5pt]


\vspace{0.3em}
}}}\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & -focus \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}




\noindent \headingfont \small \textbf{Matriisi 28}: Ei--fokaalinen S4-konstruktio \normalfont \vspace{0.6cm}

Ei--fokaalisesta S4-konstruktiosta periytyviksi voidaan erikseen olettaa
toisaalta topikaalinen (vrt. esimerkki \ref{ee_samppa}) ja kehyksinen (esimerkki
\ref{ee_aloitus}) sekä toisaalta myös paikan lokalisaattorin (mm. esimerkit
\ref{ee_kiinapuhe} -- \ref{ee_europaues}) sisältävät variantit. Yhteistä näille kaikille
on se, että konstruktio sisältää edellä kuvatun kaltaisesti vain vähän
sellaisia elementtejä, jotka ovat osa pragmaattista väittämää ja
diskurssistatukseltaan aktiivisia -- lauseen fokus liittyy aina
ajanilmausta edeltävään propositioon, kun taas itse ajanilmauksella
on aina jokin muu diskurssifunktio kuin fokus. Tähän informaatiorakenteelliseen
ominaislaatuun liittyen on havainnollista verrata ei--fokaalista
ajanilmauskonstruktiota eräisiin niistä paikanilmauksen sisältävistä
tapauksista, joita Tuomas Huumo on tutkinut väitöskirjassaan. Huumo
[-@huumo1997, 162--163] esittää muun muassa seuraavan lauseen, jonka lopussa on
paikkaa ilmaiseva adverbiaali:




\ex.\label{ee_huumobussi_loppu} \exfont \small Elmeri näki Anselmin bussissa





\vspace{0.4cm} 

\noindent
Huumon tulkinnan mukaan lauseessa \ref{ee_huumobussi_loppu} adverbiaalin
määrityskohde on rajattu, niin että se viittaa lähinnä lauseen objektin
referentin, Anselmin, sijaintiin. *Bussissa* on tällöin Anselmi-sanan
määrite -- se, mitä Elmeri on nähnyt, ei toisin sanoen ole "Anselmi"
vaan "Anselmi bussissa". Toinen mahdollinen tulkinta on
fokaalinen konstruktio, jossa puhuja tai kirjoittaja olettaa koko proposition
[Elmeri näki Anselmin] aktiiviseksi kuulijan tai lukijan tajunnassa ja
spesifioi, että Anselmi sijaitsi proposition tapahtumahetkellä bussissa.

Jos kuitenkin siirrytään tarkastelemaan 
vastaavia lauseenloppuisen *ajanilmauksen* sisältäviä lauseita,
tulkintakenttä muuttuu hieman, kuten keksitty kontekstiton esimerkki
\ref{ee_huumobussi_loppu_eilen} osoittaa:




\ex.\label{ee_huumobussi_loppu_eilen} \exfont \small Elmeri näki Anselmin eilen.





\vspace{0.4cm} 

\noindent
Esimerkille \ref{ee_huumobussi_loppu_eilen} voidaan erottaa kaksi loogista
tulkintaa: sen voi nähdä joko fokaalisen konstruktion edustajana, jolloin
propositio [Elmeri näki Anselmin] on diskurssistatukseltaan aktiivinen samoin
kuin alkuperäisen paikanilmauksen sisältävän tapauksen jälkimmäisessä
tulkinnassa. Toinen tulkinta taas on sovittaa esimerkki ei--fokaalisen
konstruktion sisään olettamalla koko propositio täysin uudeksi,
aktivoimattomaksi informaatioksi, niin että viestijän tavoitteena on kertoa
fakta siitä, että [Elmeri näki Anselmin] on tapahtunut ja että tämän lisäksi
koko tapahtumaa määrittää adverbi eilen.  \todo[inline]{Esittele sem.scope terminä}

\todo[inline]{maininta VSO:sta?}

On olennaista lisäksi huomata, että kun ajanilmaus toimii ei--fokaalisessa konstruktiossa,
on myös sen viittauskohde tavallisesti diskurssistatukseltaan aktiivinen,
niin kuin matriisi 28 määrittelee. Kuvittele seuraava esimerkki 
lausuttavaksi toisaalta heinäkuussa, toisaalta vaikkapa joulukuun kahdeksantena:




\ex.\label{ee_huumobussi_loppu_itsp} \exfont \small Elmeri näki Anselmin itsenäisyyspäivänä.





\vspace{0.4cm} 

\noindent
Jos lausumisajaksi oletetaan heinäkuu, on hyvin todennäköisesti kyse
fokaalisesta konstruktiosta: puhuja tai kirjoittaja on luultavasti esittämässä
kertomusta, jonka diskurssitopiikki on Elmeri ja jossa propositio [Elmeri näki
Anselmin] oletetaan osaksi kuulijan tai lukijan aktiivista tajuntaa. Toisaalta
jos lausumishetki on Suomen itsenäisyyspäivän (6.12.) välittömässä
läheisyydessä, ei--fokaalinen konstruktio on luontevampi tulkintakehys:
propositio [Elmeri näki Anselmin] on viestin vastaanottajalle täysin uutta
tietoa, joka ankkuroidaan oletettavasti tunnistettavissa olevaan referenttiin
itsenäisyyspäivä\todo[inline]{TODO: referenttinotaatio}. Voisi itse asiassa ajatella,
että aktiivinen diskurssistatus eräällä tavalla lisensoi\todo[inline]{fix} ei--fokaalisen
konstruktion,[^huumotemp] joskin myös poikkeustapauksia
on löydettävissä (ks. esimerkki \ref{ee_aloitus}).


[^huumotemp]: kerro alaviitteessä Lontoossa-esdimerkin (180)
temporaalisuudesta. JA viittaa mahdollisesti kokoavaan kfa-lukuun.

\todo[inline]{Vielä joku maininta aika + paikka -yhdistelmistä sekä ehkä}

#### Tilastollinen malli ei--fokaalisen konstruktion yleisyydestä suomessa ja venäjässä

Palaan nyt tämän tutkimuksen kannalta oleellisimpaan kysymykseen eli siihen,
missä määrin ei--fokaalisen konstruktion esiintyminen on spesifiä suomelle ja
missä määrin sen voi nähdä selittävän aineistossa havaittavia eroja
positionaalisten ilmausten ja deiktisten adverbien S4-osuuksissa. Kysymyksen
tarkastelemiseksi keskityn seuraavassa niihin L-funktiota edustaviin
tapauksiin, joissa lauseen finiittiverbi esiintyy menneen ajan aikamuodossa ja
jotka sisältävät ajanilmauksen välittömässä läheisyydessä
paikanilmauksen.\todo[inline]{Lisää perusteluita} Esitetty rajaus jättää siis
tarkastelun ulkopuolelle tulevaisuuteen viittaavan aineistoryhmän L1c sekä
F-funktiota edustavan F1b:n. Lisäksi analyysistä suljetaan pois merkitykseltään
usein ambivalentit sanat *nyt*, *nykyisin*, *pian* sekä *сейчас*, *теперь* ja
*скоро*, jotka tietyissä tapauksissa voivat esiintyä menneessä ajassa mutta
joiden kohdalla ei--fokaalisen S4-konstruktion esiintyminen on epätodennäköistä.




Edellä kuvatulla tavalla rajattu aineisto sisältää aineistoryhmät L1a, L1b, L4a
ja L5c. Nämä kattavat
yhteensä 14 744 suomen-
ja 12 226
venäjänkielistä tapausta. Nämä tapaukset on seuraavassa taulukossa jaettu
kolmeen kategoriaan: niihin, joiden välittömässä läheisyydessä ei ole
paikanilmausta (*none*), niihin, joissa paikanilmaus sijaitsee välittömästi
ajanilmauksen jälkeen (*next*) ja niihin, joissa ajanilmaus sijaitsee heti ennen
paikanilmausta (*previous*).


-----------------------------------
&nbsp;   next   none     previous  
-------- ------ -------- ----------
fi       953    13 163   628       

ru       565    11 553   108       
-----------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 21}: Valikoidut deiktiset ja positionaaliset aineistoryhmät ja paikanilmauksen läsnäolo \normalfont \vspace{0.6cm}

\todo[inline]{TODO: selitä tarkennuksia, ja kuvaa sitä, että venäjässä tosi vähän S4, ja
näistä tarkennusten jälkeen moni osoittautuu vääräksi.


Myös hyvä osoitus S4-fokaalisuudesta venäjässä: Джексон потерял сознание в своем доме в Лос-Анджелесе в четверг, около 21.30 мск. (Araneum Russicum: majkldzhekson.ru)

}

Taulukosta havaitaan ensinnäkin, että kummassakin kielessä valtaosan muodostavat
tapaukset, joissa on pelkkä ajanilmaus ja että toiseksi eniten on ajanilmaus +
paikanilmaus -tapauksia. Jos katsotaan, miten paikanilmauksen läsnäolo vaikuttaa
loppusijainnin yleisyyteen, havaitaan melko selkeä trendi, joka on esitetty
loppusijainnin suhteelliset osuudet kokoavassa kuviossa 55:

![\noindent \headingfont \small \textbf{Kuvio 55}: Paikanilmauksen läsnäolo ja S4-aseman yleisyys L1a-, L1b-, L4a- ja L5c-aineistoryhmien menneen ajan tapauksissa \normalfont](figure/locative_neighbour.s4-1.pdf)

Kuvio 55 osoittaa suorastaan eksponentiaalisen kasvun
suomenkielisten L1a-, L1b-, L4a- ja L5c-ryhmien S4-osuuksissa: tapauksissa,
joissa ajanilmauksen vieressä ei ole paikanilmausta, S4-asemaan osuu ainoastaan
noin viidennes, tapauksissa, joissa ajanilmausta seuraa paikanilmaus vajaat 40
prosenttia ja tapauksissa, joissa paikanilmaus edeltää ajanilmausta yli 70
prosenttia. Huomionarvoista kyllä, myös venäjässä jälkimmäisin vaihtoehto
näyttäisi lisäävän S4-aseman yleisyyttä.

Vaikuttaisi siis siltä, että mikäli ajan- ja paikanilmauksen sijaitsemista
menneeseen aikaan sijoittuvassa S4-lauseessa vierekkäin  voidaan pitää edes jossain
määrin tarkkana ei--fokaalisen S4-konstruktion indikaattorina, ei--fokaalinen
konstruktio on suomessa
tulkittavissa yhdeksi merkittäväksi syyksi ajanilmauksen
sijoittamiselle lauseen loppuun. Toisaalta erosta suhteessa 
venäjään ei kuviosta 55 saatavan informaation
perusteella voi vielä tehdä kovin varmoja johtopäätöksiä, sillä ainakin
jossain määrin paikanilmauksella voidaan nähdä vastaava S4-aseman
todennäköisyyttä kasvattava vaikutus myös venäjässä. Ei--fokaalisen
S4-konstruktion havaitsemista voidaan kuitenkin tarkentaa ottamalla huomioon
edellä käsiteltyjen esimerkkien perusteella esitetty arvio siitä, että nämä
konstruktiot ovat suomessa erityisen tyypillisiä lehdistökielelle. Yhdistämällä
tämä havainto ja tieto paikanilmauksen läheisyydestä voidaan rakentaa
tilastollinen malli, jonka perusteella ei--fokaalisen S4-konstruktion
vaikutuksesta S4-sijaintiin voidaan esittää kieltenvälisiä vertailuja.

Tässä käytettävää tilastollista mallia varten aineistoon luotiin muuttuja,
jolle annettiin kuusi eri lähdekorpuksen ja paikanilmauksen läsnäolon
yhdistävää arvoa: *araneum none*, *araneum next* ja *araneum previous* sekä
vastaavat lehdistökorpusta koskevat arvot *press none*, *press next* ja *press
previous*.[^nonfocbugs] Näiden avulla mitattiin kielen, lähdekorpuksen ja paikanilmauksen
läsnäolon yhteisvaikutusta. Kuvio 56  esittää 
mallin eri muuttujien selitysvoiman totuttuun tapaan keskihajontojen avulla:

[^nonfocbugs]: Mallin taustalla oleva bugs-koodi saatavilla BitBucket-versionhallinnasta 
osoitteesta https://tinyurl.com/ybnvm2ka (tarkistettu 15.3.2018)


![\noindent \headingfont \small \textbf{Kuvio 56}: Ei--fokaalista konstruktiota arvioivan tilastollisen mallin keskihajonnat \normalfont](figure/nonfoc.std-1.pdf)

Keskihajontoja tarkastelemalla voidaan todeta, että kielen ja
korpustyyppi+paikanilmaksen läsnäolo -muuttujan välillä todella on selvästi
nollasta erottuva interaktio, joskin luonnollisesti eniten selitysvoimaa on
pelkällä kielellä yksinään. Olennaista tässä tarkasteltavan ilmiön kannalta on
kuitenkin se, millaisena tämä interaktio näyttäytyy suhteessa S4-sijaintiin.
Nämä vaikutukset on esitetty kuviossa 57:

![\noindent \headingfont \small \textbf{Kuvio 57}: Paikanilmauksen läsnäolon, lähdekorpuksen ja kielen yhteisvaikutus ei--fokaalista konstruktiota arvioivassa tilastollisessa mallissa \normalfont](figure/nonfoc_interactplot-1.pdf)

Kuviosta 57 on nähtävissä, että edellä esitettyjen
oletusten mukaisesti suomen S4-todennäköisyys todella kasvaa venäjään verrattuna, jos
lause sekä a) edustaa lehdistöaineistoa että b) sisältää joko ajanilmausta seuraavan tai
sitä edeltävän paikanilmauksen. Kuviossa näkyvien palkkien pituus osoittaa, että vaikutuksen
tarkkaan voimakkuuteen liittyy kohtalaisen paljon epävarmuutta, mutta sen suunta on 
yhtä kaikki selvä. Jossain määrin epävarmasti voidaan lisäksi todeta, että vaikutus
on todennäköisesti voimakkaampi nimenomaan niissä tapauksissa, joissa
paikanilmaus edeltää ajanilmausta. Kaikki tämä tukee ajatusta siitä, että suomen
ja venäjän välillä erityisesti deiktisten ja positionaalisten ilmausten osalta
havaittava ero S4-aseman yleisyydessä on jossain määrin selitettävissä
ei--fokaalisen S4-konstruktion tavanomaisuudella suomenkielisessä aineistossa.


Osasyy siihen, että kuviosta 57  tehtäviin 
havaintoihin liittyy epävarmuutta, saattaa olla siinä, että edellä esitettyjen
tyypillisten ei--fokaalisten S4-tapausten rinnalla suomessa vaikuttaisivat
olevan tavallisia myös rakenteeltaan lähes identtiset S3-tapaukset kuten
esimerkki \ref{ee_bclinton}




\ex.\label{ee_bclinton} \exfont \small Yhdysvaltain presidentti Bill Clinton tapasi \emph{torstaina} Moskovassa
useiden venäläispuolueiden johtoa. (FiPress: Demari)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_bclinton} osoittaa toisaalta myös sen, miksi juuri välittömästi
paikanilmauksen jäljessä sijaitseva ajanilmaus vaikuttaa voimakkaammin
S4-sijainnin todennäköisyyden kasvuun. Jos esimerkissä \ref{ee_bclinton} kääntäisi
ajan- ja paikanilmausten järjestyksen (*Moskovassa torstaina*), olisi ainakin
oman intuitioni mukaan lähes välttämätöntä siirtää molemmat konstituentit
lauseen loppuun -- joskin lauseen lopussa myös paikka + aika -järjestys olisi
täysin mahdollinen.  Tämä saattaa liittyä siihen, että sijainti juuri verbin 
jäljessä on tavallisesti loogisin sille ilmaukselle, jonka referentti on 
diskurssistatukseltaan aktiivisin ja siten uutuusarvoltaan vähäisin [@tobecited]: jos 
*Moskovassa*-sanan sijasta paikanilmauksen asemassa olisi esimerkiksi *täällä*-pronomini, 
olisi luontevin järjestys aika + paikka -järjestyksen sijasta paikka + aika:




\ex.\label{ee_bclinton2} \exfont \small Yhdysvaltain presidentti Bill Clinton tapasi täällä \emph{torstaina}
useiden venäläispuolueiden johtoa. (FiPress: Demari)





\vspace{0.4cm} 

\noindent
Katsotaan vertailun vuoksi vielä lähdekorpuksen ja paikanilmauksen läsnäolon
vaikutusta S1-sijainnin todennäköisyyteen. Suomen ja venäjän väliset erot on
esitetty kuviossa 58, jossa -- kuten tämän
tutkimuksen muissa vastaavissa kielen vaikutusta mittaavissa kuvioissa -- erot
on ilmaistu käyttämällä suomea vertailun lähtökohtana:

![\noindent \headingfont \small \textbf{Kuvio 58}: Paikanilmauksen läsnäolon, lähdekorpuksen ja kielen yhteisvaikutus ei--fokaalista konstruktiota arvioivassa tilastollisessa mallissa \normalfont](figure/nonfoc_interactplots1-1.pdf)

Kuvio 58 osoittaa, että S1-todennäköisyys
kasvaa venäjässä suomeen verrattuna eniten  tapauksissa,
joissa lauseen alussa on ajanilmausta seuraava paikanilmaus ja jotka ovat osa
lehdistökorpusta.[^suuntaselitys] Tällaisia ovat muun muassa seuraavat
virkkeet:

[^suuntaselitys]: Kuten todettu, kuvio 58 esittää muun
tutkimuksen tavoin vertailun muodossa *suomessa verrattuna venäjään*. Se, että
jokin vaikutus on näin esitettynä selvästi negatiivisen puolella, tarkoittaa
aivan samaa kuin että sama vaikutus olisi saman verran positiivisen puolella,
jos vertailun lähtökohdaksi otetaan venäjä.




\ex.\label{ee_vtsherajushenko} \exfont \small Вчера в Киеве президент Украины Виктор Ющенко дал пресс-конференцию,
которую многие расценили как парад победы. (RuPress: РБК Daily)






\ex.\label{ee_ranoutrom} \exfont \small Рано утром в Москве полицейские в ходе погони застрелили 29-летнего
водителя. (RuPress: РИА Новости)






\ex.\label{ee_plenarnom} \exfont \small Сегодня на пленарном заседании Государственной думы депутаты приняли
решение пригласить на «закрытый»-\/- (RuPress: Новый регион 2)





\vspace{0.4cm} 

\noindent


Esimerkkejä \ref{ee_vtsherajushenko} -- \ref{ee_plenarnom} voidaan monessa tapauksessa
pitää edellä esitettyjen suomen ei--fokaalisten S4-esimerkkien tavallisimpina
vastineina, vaikkakin -- kuten muun muassa esimerkki \ref{ee_blatter} edellä
osoittaa -- myös ei--fokaaliset konstruktiot ovat venäjässä käytössä. On lisäksi
pidettävä mielessä se mahdollisuus, että esimerkiksi kuvatekstejä saattaa
suomenkielisessä lehtiaineistossa olla suhteessa enemmän kuin vastaavassa
venäjänkielisessä aineistossa, mikä voi hyvinkin vaikuttaa ajanilmausloppuisten
lauseiden määrään. Koska suurimmassa osassa suomenkielistä lehdistöaineistoa
laajemman kontekstin hakeminen ei ole mahdollista, on kuvatekstien todellista
osuutta vaikea arvioida. Aineistossa on kuitenkin monia sellaisia ei--fokaalista
konstruktiota edustavia esimerkkejä, jotka hyvin todennäköisesti ovat peräisin
lehtijutun varsinaisesta tekstistä, kuten esimerkki \ref{ee_hameensahko}:




\ex.\label{ee_hameensahko} \exfont \small Hämeen sähkön osakkeen hinta jatkoi nousuaan Helsingin pörssissä eilen.
(FiPress: Aamulehti)





\vspace{0.4cm} 

\noindent
Virkkeessä \ref{ee_hameensahko} on vaikea kuvitella, että Hämeen sähkön osakkeen
hinnan nouseminen olisi jotain sellaista, mitä havainnollistetaan erillisellä
kuvatekstin sisältävällä kuvalla. Seuraavien esimerkkien konteksti on
puolestaan tarkistettu erikseen digitoimattomista alkuperäislähteistä:




\ex.\label{ee_purkamista} \exfont \small Elokuvatuottaja Ilkka A. Liikanen ehdotti koko elokuvasäätiön purkamista
Vieraskynä-palstalla eilen. (FiPress: Helsingin Sanomat)






\ex.\label{ee_vanki} \exfont \small Vanki sytytti tulipalon sellissään Riihimäen keskusvankilassa tiistaina.
(FiPress: Aamulehti)





\vspace{0.4cm} 

\noindent
Esimerkki \ref{ee_purkamista} osoittautui Helsingin Sanomien kulttuurisivujen *Keskustelua*-nimisen
palstan kirjoitukseksi, jossa itse esimerkissä esiintyvä virke on osa tekstissä olevaa ingressimäistä,
tummemmalla painettua johdantokappaletta:

\begin{quote}%
Talous on elokuvan ongelma, ei hallinto

Elokuvatuottaja Ilkka A. Liikanen ehdotti koko elokuvasäätiön purkamista
Vieraskynä-palstalla eilen. Suomen elokuvasäätiön uusi tuotantojohtaja
Erkki Astala vastaa hänelle.

Onko suomalaisella elokuva-alalla erityinen hallinnollinen ongelma?
Tämän lehden palstoilla pitkään käydystä keskustelusta voisi niin
päätellä: vaatimuksia uudesta hallintomallista on heitetty ilmaan ja
nykyistä ammuttu alas, viimeksi Ilkka A. Liikasen kirjoituksessa 18. 12.

HS 19.12.1995
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Esimerkki \ref{ee_vanki} esiintyy osana pientä uutista, joka on kokonaisuudessaan esitetty seuraavassa:

\begin{quote}%
Vanki sytytti sellinsä Riihimäellä

Vanki sytytti tulipalon sellissään Riihimäen keskusvankilassa tiistaina.
Vankilan henkilökunta ehti ajoissa hätiin ja sai vedettyä miehen turvaan
sekä sammutettua palon jauhesammuttimella. Palokunta tarkisti sellin ja
sen läheiset tilat sekä suoritti savutuuletuksen. Vanki sai lievän
savumyrkytyksen.

Aamulehti 26.10.1995
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Tarkempien kontekstien valossa tutkittuna esimerkit \ref{ee_purkamista} ja \ref{ee_vanki}
ovat silmiinpistävän samanlaisia kuin aiemmin tarkastellut venäjän
johdantokonstruktiotapaukset (kuten luvun \ref{simultaaninen-funktio} esimerkit
\ref{ee_livija} ja \ref{ee_autoru}). Oleellisin ja ilmeisin yhtäläisyys on, että niin
kuin edellisissä luvuissa tarkastellut johdantokonstruktiotapaukset, myös
esimerkit \ref{ee_purkamista} ja \ref{ee_autoru} ovat koko varsinaisen tekstin
ensimmäisiä virkkeitä ja siten nimenomaan johdantoja, jotka esittelevät tekstin
varsinaiset diskurssitopiikit. Kuten johdantokonstruktioissa, tässä
tarkastelluissa ei--fokaalisssa esimerkeissä subjektit eivät ole
diskurssistatukseltaan aktiivisia, vaan esitellään lukijalle ajanilmauksen
sisältävässä virkkeessä.

Kaiken kaikkiaan voidaan sanoa, että vaikka esimerkkien \ref{ee_vtsherajushenko} --
\ref{ee_plenarnom} kaltaiset tapaukset ovat ainakin jossain määrin mahdollisia
suomessa ja esimerkkien \ref{ee_hameensahko} -- \ref{ee_vanki} kaltaiset tapaukset
venäjässä, antaa tässä tarkasteltu tilastollinen malli selvät viitteet siitä,
että esimerkeissä \ref{ee_vtsherajushenko} -- \ref{ee_plenarnom} käytetty
viestintästrategia on paljon tyypillisempi venäjälle ja esimerkeissä
\ref{ee_hameensahko} -- \ref{ee_vanki} käytetty strategia suomelle. Venäjän
johdantokonstruktioiden ja suomen ei--fokaalisten konstruktioiden rooliin
toistensa mahdollisina ekvivalentteina perehdytään tarkemmin seuraavassa,
lukujen \ref{ajanilmaukset-ja-alkusijainti} --
\ref{ajanilmaukset-ja-loppusijainti} havainnot kokoavassa luvussa.






Kokoava kontrastiivinen analyysi
================================



Luvussa \ref{funktionaalinen} mainittiin Chestermanin
[-@chesterman1998contrastive, 53] kontrastiivisen funktionaalisen
analyysimetodin ytimessä oleva ajatus siitä, että kieltenvälisen vertailun --
kontrastiivisen analyysin -- tavoitteena on tuottaa ja testata falsifioitavissa
olevia hypoteeseja jonkin ilmiön samanlaisuudesta kahden kielen välillä.
Luvuissa  \ref{ajanilmaukset-ja-alkusijainti} -- \ref{ajanilmaukset-ja-loppusijainti}
on tarkasteltu suomen ja venäjän alku, keski- ja loppusijainneissa
havaittavia yhtäläisyyksiä ja eroja, mutta varsinaiset hypoteesit kielten eri
ajanilmauskonstruktioiden vastaavuudesta ovat vielä esittämättä. Toteutan 
näiden hypoteesien esittämisen ja testaamisen tässä luvussa, jonka tarkoituksena
on samalla toimia edellisten lukujen havainnot kokoavana tiivistelmänä siitä, mitä
erilaisia ajanilmauskonstruktioita suomessa ja venäjässä havaittiin olevan 
ja miten esitetyt konstruktiot suhtautuvat toisiinsa.

Verrattaessa eri ilmiöiden samanlaisuutta olennaisessa asemassa ovat kriteerit, joiden
avulla samanlaisuus määritellään. Koska tämän tutkimuksen kontekstissa vertailun
kohteena eivät ole esimerkiksi yksittäiset sanat vaan kokonainen syntaksin ja
informaatiorakenteen tason ilmiö (ajanilmausten sijainti), ei kriteerien määrittäminen 
ole kovin suoraviivaista. Yleistäen voidaan kuitenkin sanoa, että katson tämän
tutkimuksen kontekstissa jotkin kaksi ajanilmauksen sisältävää lausetta (tai
laajempaa kielellistä kokonaisuutta) samanlaisiksi, jos

1. ne ovat yhtä tavallisia tai harvinaisia.

2. ne ovat tyyliltään ja käyttöyhteydeltään samanlaisia. 

3. ne toteuttavat samanlaisia viestinnällisiä tehtäviä

Esitän ensimmäisenä, kaikkein epämääräisimpänä hypoteesina 
suomen ja venäjän ajanilmausten samanlaisuudesta, että tässä tarkastellut kielet
ovat samanlaisia sijaintien tasolla:

\noindent \headingfont \small \textbf{Hypoteesi 1}: suomen S1-, S2-, S3- ja S4-sijainnit 
        ovat samanlaisia kuin venäjän vastaavat sijainnit \normalfont \vspace{0.6cm} 

Hypoteesin  kannalta empiirisesti
ongelmallisia ovat jo luvussa \ref{sij-yleisk} esitetyt eri sijaintien
suhteelliset osuudet kaikista ajanilmaustapauksista  -- ennen muuta se, ettei
suomessa juuri lainkaan käytetä S2-sijaintia ja että venäjässäkin S3-sijainti
on harvinainen (vrt. kriteeri 1). Koska jo tutkimuskirjallisuuden perusteella oli selvää, että
suomessa S3-sijaintia käytetään pitkälti venäjän S2-sijainnin tavoin, voidaan
hypoteesi tarkentaa seuraavaan muotoon -- tähdentämällä että *keskisijainti*
viittaa sekä S2- että S3-asemiin:


\noindent \headingfont \small \textbf{Hypoteesi 2}: Suomen alku- keski- ja loppusijainnit
        ovat samanlaisia kuin venäjän vastaavat sijainnit. \normalfont \vspace{0.6cm} 

Edelleen jo luvussa \ref{sij-yleisk} esitetyt karkeat prosenttiluvut kuitenkin
osoittavat, ettei myöskään hypoteesi 2 ole
erityisen uskottava, sillä suomen alkusijainti osoittautui huomattavasti
venäjän alkusijaintia harvinaisemmaksi ja toisaalta venäjän loppusijainti
samalla tavoin suomen loppusijaintia harvinaisemmaksi. Lähtökohtaisesti voidaan
siis olettaa, että kielten välillä on eroja siinä, miten niissä käytetään
ainakin alku- ja loppusijainteja. Lisäksi, vaikka keskisijainnin suhteellinen
osuus on kummassakin tarkasteltavassa kielessä melko samanlainen, ei tämä vielä
takaa, että keskisijaintiin osuvat konstruktiot olisivat keskenään samanlaisia
(kriteerit 2 ja 3) -- päinvastoin, edellisessä kolmessa luvussa käytetty tilastollinen malli osoitti,
että kielten välillä on eroja siinä, miten keskisijaintia käytetään.
Hypoteesi onkin tarkennettava sijaintikohtaisesta ennemminkin
konstruktiokohtaiseksi ja pilkottava useiksi yksittäisiksi hypoteeseiksi, jotka 
voidaan yleisellä tasolla esittää muodossa


\noindent \headingfont \small \textbf{Hypoteesi 3}: Suomen kielen konstruktiot F1, F2, F..n
    ovat samanlaisia kuin venäjän kielen konstruktiot R1, R2, R...n. \normalfont \vspace{0.6cm} 

Hypoteesin 3 testaamiseksi seuraavaan taulukkoon
on koottu kaikki edellisessä kolmessa luvussa käsitellyt ajanilmauskonstruktiot:


|Konstruktion nimi                     |Sijainti |Tutkimuksen alaluku                          |Matriisi |
|:-------------------------------------|:--------|:--------------------------------------------|:--------|
|Äkkiä-konstruktio                     |S1       |\ref{varsinainen-resultatiivinen-ekstensio}  |5        |
|Affektiivinen konstruktio             |S1       |\ref{varsinainen-resultatiivinen-ekstensio}  |6        |
|Kontrastiivinen konstruktio           |S1       |\ref{ei--deiktiset-adverbit}                 |7        |
|Alatopiikkikonstruktio                |S1       |\ref{likim-s1}                               |8        |
|Ajallinen alatopiikkikonstruktio      |S1       |\ref{s1-sim-pos}                             |12       |
|Määrää painottava S1-konstruktio      |S1       |\ref{likim-s1}                               |9        |
|Määrää painottava S4-konstruktio      |S4       |\ref{muutf-s4}                               |26       |
|Sekventiaalinen konstruktio           |S1       |\ref{sekv-s1}                                |10       |
|Topikaalinen konstruktio              |S1       |\ref{sekv-s1}                                |11       |
|Topikaalinen S4-konstruktio.          |S4       |\ref{fi-s4-pien}                             |27       |
|Globaali johdantokonstruktio          |S1       |\ref{s1-sim-pos}                             |13       |
|Globaali johdantokonstruktio          |S2/S3    |\ref{l1-erot-kesk}                           |18       |
|Lokaali johdantokonstruktio           |S1       |\ref{s1-sim-pos}                             |14       |
|Lokaali johdantokonstruktio           |S2/S3    |\ref{l1-erot-kesk}                           |         |
|Raportoiva konstruktio.               |S1       |\ref{s1-l3}                                  |15       |
|Ilmoituksen aloittava konstruktio     |S1       |\ref{s1-l3}                                  |16       |
|Fokaalinen konstruktio                |S1       |\ref{l7l8-erot-kesk}                         |19       |
|Fokaalinen konstruktio                |S4       |\ref{loppus_np}                              |20       |
|Fokaalinen S2-konstruktio.            |S2       |\ref{duratiivinen-funktio}                   |25       |
|Adverbinen konstruktio                |S2/S3    |\ref{morf-sama-keski}                        |17       |
|Monivalenssinen S4-konstruktio        |S4       |\ref{loppus_np}                              |21       |
|Teelinen konstruktio.                 |S4       |\ref{teelinen-semanttinen-funktio}           |22       |
|Duratiivi + paikka -konstruktio.      |S3       |\ref{duratiivinen-funktio}                   |23       |
|Topikaalinen duratiivinen konstruktio |S3       |\ref{duratiivinen-funktio}                   |24       |
|Ei-fokaalinen S4-konstruktio          |S4       |\ref{deiktiset-adverbit-ja-positionaalisuus} |28       |

\noindent \headingfont \small \textbf{Taulukko 22}: Tutkimuksessa käsitellyt konstruktiot \normalfont \vspace{0.6cm} 

Taulukon 22 ei ole tarkoitus olla tyhjentävä esitys
kaikista konstruktioista, joissa suomen ja venäjän ajanilmaukset myönteisissä
SVO-lauseissa esiintyvät. Pikemminkin taulukkoon on koottu ne edellisessä kolmessa
luvussa käsitellyt konstruktiot, jotka nousivat esille, kun kieliä vertailtiin
luvussa \ref{tilastollinenmalli} rakennetun tilastollisen mallin perusteella.


Taulukossa 22 esitetyistä konstruktioista voidaan ensinnäkin
todeta, että edellisissä luvuissa tehdyn analyysin perusteella ainakin äkkiä-konstruktio,
kontrastiivinen konstruktio, topikaalinen  S1-konstruktio ja topikaalinen S4-konstruktio,
sekventiaalinen konstruktio, adverbinen konstruktio, teelinen konstruktio sekä
fokaalinen S4-konstruktio ovat hyvin pitkälti
samanlaisina käytössä yhtä lailla suomessa kuin venäjässä. Näiden konstruktioiden 
frekvensseissä ei havaittu suuria eroja, samoin niiden käyttöyhteydet ja -ympäristöt
olivat edellisessä kolmessa luvussa käsitelltyissä tapauksissa samantyyppisiä.
Näiden konstruktioiden osalta
hypoteesia 1  voidaan siis tarkentaa muotoon:


\noindent \headingfont \small \textbf{Hypoteesi 4}: Suomen kielen äkkiä-konstruktio,
        kontrastiivinen konstruktio, topikaalinen  S1- ja S4-konstruktio,
        sekventiaalinen konstruktio, adverbinen, teelinen konstruktio sekä fokaalinen
        S4-konstruktio ovat samanlaisia kuin venäjän kielen samannimisinä
        käsitellyt konstruktiot. \normalfont \vspace{0.6cm} 

Hypoteesi 4 ei luonnollisesti ole lopullinen
tai maksimaalisen tarkka: voi hyvin olla, että kustakin edellä luetellusta konstruktiosta
on löydettävissä myös sellaisia käyttötilanteita, joissa kielet eivät toimi samalla tavoin tai
jossa konstruktioiden merkitys on eri. Lisäksi esimerkiksi venäjän
äkkiä-konstruktioista voitaisiin hyvin tarkentaa, että lauseen verbin tulee edustaa
perfektiivistä aspektia. Tämän tutkimuksen puitteissa havaittu konstruktiotason
vastaavuus on kuitenkin riittävä pysähtymispiste hypoteesin tarkennusprosessissa.
Olennaisempaa kuin tarkentaa hypoteesia 4, on tutkia
lähemmin niitä konstruktioita, joiden osalta suomen ja venäjän 
samanlaisuus ei ole yhtä ilmeistä. 


Konstruktiot, joiden samanlaisuus ei ole yksiselitteistä
------------------------------------------------------

Hypoteesin 4 jälkeen taulukossa
22 listatuista konstruktioista jäljelle jäävät
affektiivinen konstruktio, alatopiikkikonstruktio ja ajallinen
alatopiikkikonstruktio, määrää painottavat konstruktiot, johdantokonstruktion eri variantit, raportoiva konstruktio,
ilmoituksen aloittava konstruktio, fokaaliset S1- ja S2-konstruktiot,
monivalenssinen S4-konstruktio, duratiivi + paikka -konstruktio, topikaalinen duratiivinen
konstruktio sekä ei--fokaalinen konstruktio. Käsittelen näitä seuraavassa
ryhmiteltyinä toisiinsa liittyviksi kokonaisuuksiksi.

### Affektiivinen konstruktio, duratiiviset ja fokaaliset konstruktiot

Kuten luvun \ref{ajanilmaukset-ja-alkusijainti}
aluksi mainittiin, affektiivisuuden linkittyminen lauseenalkuiseen asemaan sinänsä
on laajempikin  kuin vain suomea ja venäjää koskeva kielellinen ilmiö. Suomen ja venäjän välillä
kuitenkin vaikuttaisi olevan eroja siinä, minkälaisissa tilanteissa ja kuinka usein 
lauseenalkuinen ajanilmaus on tulkittavissa affektiiviseksi ja miten tavallista 
ja odotuksenmukaista affektiivinen tyyli on. Affektiivisen konstruktion osalta hypoteesi
*suomen affektiivinen konstruktio =[^onmerkki] venäjän affektiivinen
konstruktio* ei siis sinällään vielä ole riittävän tarkka. Esimerkiksi seuraavien
edellä käsiteltyjen esimerkkien (alun perin \ref{ee_marihuanu} ja \ref{ee_neko}) osalta
voidaan todeta, että niiden kaltaiset tapaukset ovat venäjänkielisessä
aineistossa tavallisia, mutta suomenkielisessä aineistossa rajoittuneet
lähinnä uskonnollisiin tai muulla tavalla kapeisiin tunnepitoisiin
konteksteihin:

[^onmerkki]: Käytän =-merkkiä tässä ilmaisemaan *samanlaisuutta*, en
yhtäläisyyttä.





\ex.\label{ee_marihuanu2} \exfont \small Восемь лет я курила марихуану, из них пять лет марихуану выращенную
гидропоническим способом, которая имеет огромное негативное влияние на
весь организм человека. (Araneum Russicum: marinagribanova.com)






\ex.\label{ee_neko2} \exfont \small \emph{Каждый день} мы помогаем различным компаниям разрешать задачи в
различных областях: экспертизы, оценки, и сопутствующие им. (Araneum
Russicum: neko-business.ru)





\vspace{0.4cm} 

\noindent

Monissa esimerkkien \ref{ee_marihuanu2} ja \ref{ee_neko2} kaltaisissa tapauksissa onkin
todettava, että venäjän affektiivisen S1-konstruktion kanssa samanlaisissa
tilanteissa käytettäisiin todennäköisesti tavallisia fokaalisia S3- tai
S4-konstruktioita. Ylipäätään affektiivisuus on ominaisuus, jonka
voi nähdä liittyvän johonkin toiseen, esimerkiksi fokuksen tai kontrastin
diskurssifunktioon [vrt. Jankon *модифицирцющие значения*, @tobecited], ja siinä mielessä
esimerkkien \ref{ee_marihuanu2} ja \ref{ee_neko2} kaltaiset tapaukset voisi mieltää myös 
fokaalisiksi S1-konstruktioiksi. Venäjänkielisestä aineistosta löydettiin
kuitenkin myös sellaisia fokaalisia S1-tapauksia, joissa affektiivisuus vaikuttaisi
olevan melko lievää (kuten esimerkissä \ref{ee_trjuki}, tässä numerolla \ref{ee_trjuki2}) sekä
toisaalta tapauksia, joihin affektiivisuutta ei liity juuri lainkaan (esimerkki
\ref{ee_udmurtii}, tässä numerolla \ref{ee_udmurtii2}).




\ex.\label{ee_trjuki2} \exfont \small Уже завтра зрители увидят невероятные трюки главных артистов. (Araneum
Russicum: lotosgtrk.ru)






\ex.\label{ee_udmurtii2} \exfont \small Именно тогда Правительство Удмуртии будет рассматривать проект заявки.
(RuPress: Комсомольская правда)





\vspace{0.4cm} 

\noindent
Voidaankin todeta, että venäjässä lauseenalkuisen, jollain tavalla korosteisen
ajanilmauksen käyttöala on laajempi kuin suomessa. Tavallisempia ovat myös koko
lailla neutraalit fokuksen diskurssifunktiossa toimivat lauseenalkuiset
ajanilmaukset. Näissä tapauksissa hypoteesia konstruktioiden samanlaisuudesta
on tarkennettava toteamalla, että vaikka tietyissä tapauksissa suomen ja
venäjän affektiiviset konstruktiot muistuttavat toisiaan, 
näin ei ole aina, vaan muun muassa esimerkkien \ref{ee_marihuanu2} -- \ref{ee_udmurtii2}
kaltaisissa tilanteissa suomessa käyttötilanteiltaan samanlaisimpia ovat
lähtökohtaisesti neutraalit fokaaliset S3- ja S4-konstruktiot.

Kokonaan oman ryhmänsä muodostavat ne venäjän F1a-aineistossa tavalliset
tapaukset, joissa lauseenalkuista taajuuden ajanilmausta käytetään esittelemään
yrityksen toimintaa. Tähän ryhmään kuului esimerkki \ref{ee_bldene} edellä (tässä \ref{ee_bldene2}):




\ex.\label{ee_bldene2} \exfont \small \emph{Каждый месяц} мы проводим благотворительный день. (Araneum
Russicum: organicshopaz.ru)





\vspace{0.4cm} 

\noindent
Esimerkin \ref{ee_bldene2} kaltaisia ensimmäistä persoonaa hyödyntäviä F1a-tapauksia
ei suomenkielisestä aineistosta ollut juuri löydettävissä, vaan ensimmäisen
persoonan tapaukset olivat useimmiten tulkittavista selkeän affektiivisiksi.


Duratiiviseen funktioon havaittiin esimerkkien \ref{ee_marihuanu2} ja \ref{ee_neko2} 
edustamien tapausten lisäksi edellisissä kolmessa luvussa liittyvän
muitakin suomea ja venäjää erottavia tekijöitä. 
Taulukossa 22 mainittiin S3-asemaan liittyvät
duratiivi + paikka -konstruktio sekä topikaalinen duratiivinen konstruktio,
joiden voi kummankin nähdä olevan suomelle spesifejä siinä mielessä, että
niiden käyttö on linkittynyt juuri S3-asemaan eikä niinkään venäjän tavalliseen
keskisijaintiin, S2:een (ks. esimerkit @ee_annelisauli2 ja \ref{ee_juhlauhreja} edellä).


### Alatopiikkikonstruktiot, johdantokonstruktiot ja ei--fokaalinen konstruktio

Niin tavallisia kuin ajallisia alatopiikkikonstruktioita sinänsä havaittiin
sekä suomen- että venäjänkielisessä aineistossa melko runsaasti. Suomen ja venäjän välillä
vaikuttaisi kuitenkin olevan eroa siinä, miten herkästi esimerkiksi
positionaaliset (ks. luku \ref{s1-sim-pos}) tai päiviin viittaavat deiktiset
ajanilmaukset (ks. luvut \ref{l1-erot-kesk} ja \ref{deiktiset-adverbit-ja-positionaalisuus}) 
tulevat tulkituksi alatopikaalisiksi. Toisin sanoen, samanlaisuudesta
esitettävä hypoteesi alatopiikkitapausten kohdalla on tarkennettava 
muotoon *venäjän alatopiikkikonstruktio on päällisin puolin samanlainen kuin
suomen alatopiikkikonstruktio, mutta on tilanteita, jotka 
tulevat tulkituksi alatopiikkikonstruktion edustajiksi todennäköisemmin suomessa*.

Hypoteesin tarkennus tulee ilmi erityisesti tapauksissa, jotka venäjässä 
edustavat globaalin tai johdantokonstruktion S1-versiota. Tästä hyvinä esimerkkeinä
käyvät muun muassa virkkeet \ref{ee_pfizer} ja \ref{ee_autoru2}, tässä uudestaan numeroilla
\ref{ee_pfizer2} ja \ref{ee_autoru2}




\ex.\label{ee_pfizer2} \exfont \small \emph{В 1996 году} Pfizer использовала антибиотик для лечения вспышки
менингита в нигерийской провинции Кано, после чего умерли 11 детей и
десятки других остались парализованными.






\ex.\label{ee_autoru2} \exfont \small \emph{В 1996 году} вы зарегистрировали домен Auto.ru.





\vspace{0.4cm} 

\noindent
Koska -- kuten muun muassa luvussa \ref{s1-sim-pos} annetut esimerkit
\ref{ee_citylehti} ja \ref{ee_uusitalo} osoittavat -- niin lokaali kuin globaali 
johdantokonstruktio ovat suomessakin jossain määrin käytössä, ei ole
poissuljettua, etteivätkö esimerkkien \ref{ee_pfizer2} ja \ref{ee_autoru2}
sanasanaiset suomenkieliset käännökset
*vuonna 1996 Pfizer käytti...* / *vuonna 1996 te rekisteröitte...* voisi
tulla tulkituiksi, kuten venäjässä, johdantokonstruktioina. Suomen
lauseenalkuisten esimerkkien lähempi tarkastelu edellisessä kolmessa luvussa
kuitenkin osoitti, että johdantokonstruktiot ovat suomessa harvinaisia, kun taas
alatopiikkikonstruktiot melkeinpä tavallisin syy S1-sijainnille ylipäätään.
Tämän takia ainakin kirjoitetussa tekstissä lukija oletettavasti yrittää ensin
sovittaa vastaavia lauseita alatopiikkikonstruktioon -- etenkin tässä esitetyissä
lokaaleissa johdantokonstruktiotapauksissa, joissa lauseen subjekti näyttäytyy
diskurssistatukseltaan aktiivisena.


Johdantokonstruktioista sinänsä on jatkettava, että vaikka kummankin
johdantokonstruktion S1-versiota havaittiin suomenkielisessäkin aineistossa, ja
siten hypoteesi *venäjän S1-johdantokonstruktiot ovat samanlaisia kuin suomen
S1-johdantokonstruktiot* pitää osittain paikkansa, on huomattavan paljon
tavallisempaa, että suomessa vastaavia ajanilmauksen diskurssifunktioita
ilmaistaan, kuten osiossa  \ref{l1-erot-kesk} 
 havaittiin, 
alkusijainnin asemesta keskisijainnilla. Keskisijaintiin liittyvistä
johdantokonstruktioista puolestaan
voidaan todeta, että ne ovat venäjässä selkeästi tavallisempia kuin
S1-johdantokonstruktiot suomessa. Näin ollen johdantokonstruktioita koskeva
samanlaisuushypoteesi on lopulta melko monimutkainen: kummassakin kielessä
ovat käytössä periaatteessa kaikki eri johdantokonstruktion versiot, mutta
suomessa venäjän S1-konstruktiota vastaa yleisimmin S3-konstruktio, venäjässä suomen
S3-konstruktiota taas yhtä lailla S2- kuin S1-konstruktio, joskin jälkimmäinen oletettavasti
useammin.

Johdantokonstruktioihin liittyy lisäksi läheisesti myös suomen ei--fokaalinen
S4-konstruktio. Luvussa \ref{deiktiset-adverbit-ja-positionaalisuus} tehtyjen havaintojen mukaan
ei--fokaalinen konstruktio esiintyy monissa sellaisissa käyttöyhteyksissä (kuten
esimerkki \ref{ee_vanki}, tässä \ref{ee_vanki2}), joissa
venäjänkielisessä aineistossa havaitaan lauseenalkuinen ajan- ja paikanilmaus
(muun muassa esimerkki \ref{ee_vtsherajushenko}, tässä \ref{ee_vtsherajushenko2}):




\ex.\label{ee_vanki2} \exfont \small Vanki sytytti tulipalon sellissään Riihimäen keskusvankilassa tiistaina.
(FiPress: Aamulehti)






\ex.\label{ee_vtsherajushenko2} \exfont \small Вчера в Киеве президент Украины Виктор Ющенко дал пресс-конференцию,
которую многие расценили как парад победы. (RuPress: РБК Daily)





\vspace{0.4cm} 

\noindent
Esimerkkien \ref{ee_vanki2} ja \ref{ee_vtsherajushenko2} välinen yhteys liittyy
nimenomaan globaaleihin, tekstin alkupuolella esiintyviin
johdantokonstruktioihin, joissa esitellään tekstin kannalta keskeisiä toimijoita
ja tapahtumia ja jotka Lambrechtin [-@lambrecht1996, 124] jaottelussa
voitaisiin monin paikoin luokitella myös tapahtumista raportoiviksi rakenteiksi.
Juuri ei--fokaalinen konstruktio vaikuttaisi suomessa tyypilliseltä tavalta
toteuttaa tällaista diskurssifunktiota, vaikkakin myös S3-johdantokonstruktiot
ovat aineistossa melko käytetty strategia.


### Muut konstruktiot

Oleellisimmat suomen ja venäjän välillä havaittavat erot ajanilmauksen sijainnissa
liittyvät edellisissä alaluvuissa käsiteltyihin johdantokonstruktioihin, fokaalisiin
konstruktioihin sekä affektiiviseen konstruktioon. Lisäksi on tietysti pidettävä mielessä
kieliopillinen S2-sijainnin käyttöä koskeva ero, johon monet edellä mainituista 
eroista esimerkiksi fokaalisen konstruktion käytössä itse asiassa liittyvät
(vrt. alaluku \ref{loppus_np}). Tutkimuksen kuluessa kiinnitettiin huomiota
myös koko joukkoon muita konstruktioita, joiden suhteen suomella ja venäjällä
havaittiin tiettyjä kielikohtaisia erityispiirteitä.

Ensinnäkin, määrää painottavat konstruktiot vaikuttivat suomessa erityisen voimakkaasti 
lauseenalkuisen sijainnin suosioon. Vaikka venäjänkielisestä aineistosta oli
löydettävissä hyvin pitkälle samanlaisia konstruktioita, voidaan ainakin tähän tutkimukseen
valikoitujen ilmausten osalta sanoa, että suomessa määrää painottava konstruktio on
jossain määrin ilmeisempi tulkintakehys monille lauseenalkuisille -- joskin myös S3-sijoittuneilla
-- ajanilmaustapauksille kuin venäjässä.

Toiseksi, S1-sijainnissa havaittiin muutamia ennen kaikkea venäjälle
tyypillisiä, käyttötilanteiltaan melko spesifejä rakenteita, joita ovat
raportoiva konstruktio ja ilmoituksen aloittava konstruktio. Näitä konstruktioita
ilmensivät tässä uudelleen numeroilla \ref{ee_pistolet2} (laajennetun kontekstin
kera) ja \ref{ee_galereja2} esitetyt esimerkit \ref{ee_pistolet} ja \ref{ee_galereja}:




\ex.\label{ee_pistolet2} \exfont \small В Петропавловске-Камчатском ночью было совершено нападение на ведущего
камчатской радиостанции "Радио-3" Геннадия Шеренговского. \emph{В два
часа ночи} вооруженный пневматическим пистолетом пьяный гражданин
выломал входную дверь в редакции "Радио-3", находящуюся в здании Дома
прессы, и проник в студию. (Araneum Russicum: gdf.ru)






\ex.\label{ee_galereja2} \exfont \small 7 марта \emph{в 17 часов} Художественная галерея приглашает на концерт
инструментальной и вокальной музыки Смоленского гитарного ансамбля под
руководством Заслуженного работника культуры, доцента Виктора Федоровича
Павлюченкова. (Araneum Russicum: smolensk-museum.ru)





\vspace{0.4cm} 

\noindent
Kuten luvussa \ref{s1-l3} todettiin, vastaavat konstruktiot ovat epäilemättä
käytössä myös suomessa, mutta tutkimusaineistossa tällaiset tapaukset ovat
verrattain harvinaisia. Esimerkin \ref{ee_galereja2} kaltaisia ilmoituksen
aloituksia voisi ainakin kuvitella toteutettavan laajemmin esimerkiksi
fokaalisella S4-konstruktiolla. 

Esimerkin \ref{ee_pistolet2} tekee suomen kontekstissa epätavalliseksi kokonaan
uutena diskurssireferenttinä esiteltävä radiostudioon tunkeutunut mies.
Suomessa esimerkin varsinaisen ajanilmauksen sisältävän virkkeen sijaan 
tekstin avausvirkkeen jälkeisen lauseen voisi suurella todennäköisyydellä
ajatella ankkuroituvan jollain tavalla joko tekstin alussa mainittuun radioasemaan
tai hyökkäyksen kohteeksi joutuneeseen juontajaan. Suomenkielinen teksti saattaisi
kulkea esimerkiksi seuraavalla hypoteettisella tavalla:

\begin{quote}%
Alueella X tapahtui viime yönä välikohtaus, jonka kohteeksi joutui
paikallisradion juontaja Y. Radion studioon tunkeutui kello kahdelta
yöllä humalainen mies, joka mursi studion oven ja...
%
\end{quote}

 \vspace{0.4cm} 
\noindent 
Toisin sanoen suomenkielisessä kontekstissa esimerkissä \ref{ee_pistolet2} mainittu
mies olisi luontevinta erikseen esitellä osaksi  diskurssia. Tekstin
toinen virke tulisi todennäköisimmin ankkuroiduksi jo esiteltyyn tietoon joko
tapahtumaan liittyvän paikan tai toimijoiden kautta. Varsinaisessa
venäjänkielisessä esimerkissä vaikuttaisi kuitenkin siltä, että edellisessä
lauseessa muodostettu ajallinen konteksti (*ночью*-sanan referenttinä oleva
edeltävä yö) riittää ankkuriksi, jota vasten uutta tietoa -- esimerkiksi
hyökkääjä uutena toimijana -- voidaan esitellä. Tässä mielessä
raportoiva konstruktio liittyy läheisesti johdantokonstruktioihin, joiden harvinaisuus
suomessa on lopulta selitettävissä samanlaisella erolla siinä, miten tavallista
ajanilmauksia on käyttää ankkureina, joita vasten uutta tietoa esitellään.

Ilmoituksen aloittavan ja etenkin raportoivan konstruktion tapauksissa on
vaikea määritellä suoraan konstruktiota, joka todennäköisimmin toteuttaisi
samanlaista viestintätehtävää suomessa. Pikemminkin on todettava, että että
mainittujen konstruktioiden vastineet vaihtelevat kontekstikohtaisesti, niin
että joissain tapauksissa suomessa olisi todennäköisempää käyttää esimerkiksi
alatopiikkirakennetta joissain taas esimerkiksi jotakin sekventiaalisen
konstruktion ilmentymää. Näiden konstruktioiden osalta samanlaisuushypoteesi
onkin selvin esittää muodossa *venäjän kielen ilmoituksen aloittaville ja
raportoiville konstruktioille ei ole
yksittäisiä, muita selkeämmin samanlaisia suomenkielisiä konstruktioita.* 

Venäjälle tyypillisten konstruktioiden lisäksi vielä käsittelemättömistä
konstruktioista voidaan ottaa esille suomen monivalenssinen konstruktio, jota
tarkasteltiin luvussa \ref{loppus_np}. Johtuen tämän alaluvun alussa mainituista
suomen ja venäjän keskisijaintia koskevista kieliopillisista eroista monivalenssista
konstruktiota voidaan pitää koko lailla pelkästään suomessa havaittavana
ilmiönä. Konstruktiosta annettiin edellä esimerkki \ref{ee_tietoyht}, joka tässä
on toistettu numerolla \ref{ee_tietoyht2}:




\ex.\label{ee_tietoyht2} \exfont \small Poliitikot pitävät tietoyhteiskuntaa liian usein vain tekniikka-asiana.
(FiPress: Turun Sanomat)





\vspace{0.4cm} 

\noindent
Toisin kuin venäjän raportoivan ja ilmoituksen aloittava konstruktion
tapauksessa, suomen monivalenssiselle konstruktiolle on melko yksinkertaista
esittää venäjänkielisiä vastineita: luonnollisesti venäjässä samanlaisia 
ovat tavalliset adverbiset konstruktiot, joita  -- kuten luvussa \ref{loppus_np}
todettiin -- eivät koske ne rajoitteet, jotka suomessa johtavat S4-aseman 
käyttämiseen tavallisen keskisijainnin (S3) sijasta. Toisaalta voidaan myös esittää,
että koska venäjässä affektiivinen konstruktio on käytöltään
tavallisempi, myös nämä voidaan lukea yhtälön *suomen monivalenssinen 
konstruktio = * oikealle puolelle. Toisaalta molempien kielten eri konstruktiot
muodostavat tässä yhteydessä kaikkea muuta kuin selvärajaisen ja suoraviivaisen
samanlaisuuksien verkoston, sillä myös suomessa ero etenkin esimerkin \ref{ee_tietoyht2}
kaltaisten S4-asemaan liittyvien *liian* + taajuuden ajanilmaus -tapausten ja affektiivisten 
S1-tapausten välillä on lopulta melko pieni.


Koonti
------

Tässä yhteenvetoluvussa käsitellyt suomen ja venäjän eri
ajanilmauskonstruktioiden samankaltaisuudet ovat kaiken kaikkiaan parhaiten
kuvattavissa ennemmin tiheästi linkittyneenä verkostona kuin yksittäisinä,
toisistaan erillisinä vastaavuussuhteina. Kuvio 59 
pyrkii tiivistämään edellä käsitellyt konstruktioiden väliset suhteet
tällaiseksi verkoksi. Kuviossa suomen kielen konstruktiot on kuvattu
tummemmalla, venäjän kielen vaaleammalla värillä. Oma tulkintani kunkin suhteen
voimakkuudesta on esitetty eri konstruktioiden välisten viivojen paksuutena:
mitä paksumpi viiva, sitä voimakkaammasta yhteydestä on kyse.


![\noindent \headingfont \small \textbf{Kuvio 59}: Suomen ja venäjän ajanilmauskonstruktioiden väliset yhteydet verkkona. \normalfont](figure/nwvis-1.pdf)


Kuviossa 59  esitettyjen konstruktioryppäiden
etäisyys toisistaan tai niiden sijainti kuvassa ei ole merkityksellinen.
Ajatuksena on yksinkertaisesti kuvata sitä, miten eri konstruktioiden 
ja laajemmalla tasolla sijaintien samanlaisuus on monimutkaisempi asia 
kuin vain yhden konkreettisen vastineparin esittäminen.
Kuviossa on muista erottuvia yksittäisiä konstruktioita -- venäjän raportoiva
ja ilmoituksen aloittava konstruktio -- selkeitä konstruktiopareja, kuten
venäjän ja suomen kontrastiiviset konstruktiot sekä toisaalta laajempia ryppäitä,
ennen muuta johdantokonstruktioihin liittyvä rypäs sekä toisaalta affektisen ja fokaalisen
konstruktion ympärille rakentuva rypäs.







Päätelmiä
===============



Odottaa...

\todo[inline]{

- Take into account SOV -- ivl 
  - ehkä oma pikku lukunsa loppusijainnin jälkeen...?
- get some references for "weightiness of S4 in Russian" an add to
  "loppusijainti"
- note: изредка
- look at rusgram.ru (instead of russkaja grammatika)

}


Odottaa...

\todo[inline]{
Ei poisteta hypoteeseja sijaintikohtaisista luvuista, mutta
siirretään päätelmät ja koonti tänne?

- check: Russian word order patterns 2015 

- Pitäisikö itse asiassa olla 
- CFA:n mukaisten hypoteesien lopulliset muodot 
- Konstruktioiden lista (vai sittenkin jo edellisen luvun lopussa?)
- (HUOM!) "Voiko tämä kaikki nyt sitten olla yleistettävissä muihin kuin SVO-lauseisiin?"
- 


}


Lähteet
=======
