---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
fontsize: 11.5pt
geometry: twoside, b5paper, layout=b5paper, left=2cm, right=2cm, top=2cm, bottom=3cm, bindingoffset=0.5cm
indent: true
subparagraph: yes
output:
    html_document:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
    pdf_document: 
      latex_engine: xelatex
      toc: true
      toc_depth: 6
      number_sections: true
      keep_tex: true
      includes:
          in_header: preamble.tex
---


Ajanilmaukset, konstruktiot ja informaatiorakenne {#aimkonstr}
==============================


Edellä osiossa \ref{taksn2} mainittiin Johanna Viimarannan väitöstutkimus,
jossa selvitettiin semanttiselta kannalta sitä, *minkälaisia asioita*
ajanilmauksilla viestitään. Viimarannan tutkimuksessa tarkasteltiin sitä,
minkälaisin kielenaineksin suomalaiset ja venäläiset puhujat ilmaisevat aikaa
ja minkälaisiin viestintätarpeisiin temporaaliset käsitteet suomessa ja
venäjässä liittyvät [@viimaranta2006talking, 321]. Tuloksena oli, että tarpeet
ja keinot ovat yllättävänkin samanlaisia. Kuten johdantoluvussa totesin, tässä
tutkimuksessa luotava näkökulma ajanilmauksiin on ennemmin syntaktinen kuin
semanttinen: ratkaisevana pyrkimyksenä on tutkia, minkä takia ajanilmaukset
sijaitsevat juuri tietyssä kohden lausetta. 

Niin suomen kuin venäjän sanajärjestystä on perinteisesti pidetty "vapaana"
siinä mielessä, että lauseen konstituenteilla on huomattava määrä eri
variaatioita, jotka eivät vaikuta sen *kieliopillisuuteen* [vrt. @puskas2000,
41]. Generatiivisessa perinteessä suomen ja venäjän kaltaisista kielistä
voidaan käyttää termiä *discourse configurational*, joka Kissin [-@kiss1995, 5]
mukaan merkitsee ennen kaikkea sitä, etteivät lauseen \todo[inline]{virkkeen?}
tärkeimpiä konstituentteja ole syntaktisesti määritellyt subjekti ja
predikaatti vaan informaation järjestämiseen liittyvät *topiikki* ja *fokus*. 
Topiikin ja fokuksen määritelmiin palataan alempana, osiossa
\ref{informaatiorakenne}, mutta tässä kohtaa on oleellista kiinnittää huomiota
siihen, että suomessa ja venäjässä sanajärjestys on prosodian[^prosodia] ohella toinen
niistä keinosta, joiden avulla puhuja tai kirjoittaja määrittelee, mikä rooli
jollakin lauseen osalla on informaation välittämisessä. Tästä seuraa, että
etsittäessä syitä ajanilmauksen sijaintiin on ennen kaikkea vastattava
kysymykseen siitä, minkälaisia erilaisia tehtäviä ajanilmauksilla voi
olla viestinnässä ja viestintään liittyvässä informaationkulussa.

[^prosodia]: Kun puhutaan sanajärjestyksestä ja prosodiasta informaation välittämisen
keinoina, on paikallaan muistuttaa, että nyt käsillä olevan tutkimuksen
varsinaisena aineistona -- ja siten myös tutkimuskohteena -- on nimenomaan
kirjoitettu kieli. Olen kuitenkin eri mieltä kuin esimerkiksi Susanna Shore
[-@shore2008] siitä, ettei kirjoitettuja tekstejä tutkittaessa olisi tarpeen
tehdä oletuksia kirjoittajan ja vastaanottajan tiedontiloista ja siten tulkita
tekstin lukemista yhtenä keskustelun muotona. Oma käsitykseni on lähempänä
Yokoyaman [-@yokoyama1986, 144] ajatusta, jonka mukaan kirjoitetun tekstin
lukeminen on kirjoittajan ja lukijan välinen viestintätilanne samassa mielessä
kuin tavallinen keskustelu on kahden suullisesti ja reaaliaikaisesti viestivän
osallistujan välinen viestintätilanne (vertaa myös Shoren itsensä mainitsema
@halliday2013, 59--90). Janko [-@janko2001, 14] puolestaan
toteaa, että ääneen luettaessa lukija osallistuu merkityksen rakentamiseen
yhdessä tekijän kanssa yrittäessään kuvitella kirjoittajan tarkoittamat
prosodiset piirteet. Itse olen valmis menemään askeleen pidemmälle ja
esittämään, että vaikka tekstiä ei ääneen luettaisikaan, sille on *kuviteltava*
tietyt prosodiset piirteet -- voi hyvin olla, että eri lukijat (kirjoittaja itse
mukaan lukien) kuvittelevat eri tavoin, mutta yhtä kaikki tekstin merkityksen
rakentamiseksi sitä ei voi lukea ikään kuin kirjallisessa tyhjiössä, vailla
minkäänlaista, edes kuviteltua, prosodiaa. 

\todo[inline]{TARKISTA hallidayn painoksesta, että sama kohta kuin Shorella}

Tutkiessani sitä, minkälaisia tehtäviä ajanilmauksilla on viestinnässä (ja sitä
kautta syitä ajanilmausten eri sijainteihin), lähestymistapani on tarkastella
ajanilmauksia osana *konstruktioita*, joilla toteutetaan erilaisia
viestinnällisiä ja tiedonvälityksellisiä tavoitteita.  Konstruktioiden sisällä
ajanilmausten ja muiden elementtien sijaintia säätelee ennen kaikkea
*informaatiorakenne*, jonka piiriin edellä mainitut topiikin ja fokuksen
käsitteetkin kuuluvat. Paitsi yksittäisten konstruktioiden informaatiorakenteesta, usein
on mielekästä puhua myös kokonaisen diskurssin tai tekstin informaatiorakenteesta, jolloin 
on perinteisesti käytetty esimerkiksi *tekstistrategian* [
@virtanen1992discourse, 42] tai systeemis-funktionaalisessa
paradigmassa *teemankulun* [@shore2008] käsitteitä. Esittelen seuraavassa tarkemmin
ensin konstruktion käsitteen ja tämän jälkeen informaatiorakenteeseen liittyvät
*diskurssistatuksen* ja *diskurssifunktioiden* käsitteet.[^konstr_tekst]

[^konstr_tekst]: Huomautus tekstistä konstruktiona?

Ajanilmaukset osana konstruktioita {#vsvtk}
----------------------------------------------

Aloitan konstruktion käsitteen tarkastelemisen tutkimalla seuraavia 
ajanilmauksen sisältäviä lauseita:




\ex.\label{ee_pros1} \small Kokous kestää \emph{tunnin}.






\ex.\label{ee_syn1} \small Папа \emph{часто} играет в бадминтон.






\ex.\label{ee_oikeustiede} \small Eräs tuttavani pääsi \emph{viime vuonna} opiskelemaan oikeustiedettä.



Luvussa \ref{ajanilmausten-kolme-taksonomiaa} esitettyjen taksonomioiden perusteella
voidaan todeta, että esimerkit \ref{ee_pros1} -- \ref{ee_oikeustiede} edustavat kukin
yhtä ajanilmausten semanttisten funktioiden pääluokkaa, esimerkki \ref{ee_pros1}
E-, esimerkki \ref{ee_syn1} F- ja esimerkki \ref{ee_oikeustiede} L-funktiota. Lauseita
voisi hyvin verrata esimerkkeihin \ref{ee_pros2} -- \ref{ee_tiusanen}:




\ex.\label{ee_pros2} \small Sessio jatkuu \emph{puoli kolmeen}.






\ex.\label{ee_syn2} \small Я \emph{редко} смотрю телевизор.






\ex.\label{ee_tiusanen} \small Kunnallisvaltuuston puheenjohtaja Tiusanen kertoi \emph{eilen} uusista
leikkauksista.



Esimerkissä \ref{ee_pros1} on selvästikin jotain samaa kuin esimerkissä \ref{ee_pros2}
Samoin esimerkit \ref{ee_syn1} ja \ref{ee_syn2} sekä
\ref{ee_oikeustiede} ja \ref{ee_tiusanen} muistuttavat toisiaan. Voisi ajatella, että
mainitut esimerkkiparit ovat ikään kuin samalla muotilla tuotettuja yksilöitä,
samanlaisessa merkitsijä--merkitty-suhteessa kuin vaikkapa sananmuodot
*kirjoitan* ja *kirjoitat* (jotka merkitsevät samaa lekseemiä) tai morfit /ом/
ja /ой/ (jotka merkitsevät samaa morfeemia). 

Edellä mainitussa kahtiajaossa on tietysti kyse kielitieteen kannalta
perustavanlaatuisesta, tavallisesti Ferdinand de Saussureen jäljitettävästä
ajattelutavasta, jonka mukaan kieli jakautuu lukuisiin
merkitsijä/merkitty-pareihin kuten allofoni--foneemi, morfi--morfeemi tai
sane--sana [ks. esim. @mccabe2011, 6]. Yksi *konstruktiokieliopin* [esim. @fillmore1988regularity,
@kay1999, @goldberg1995constructions, @goldberg1995constructions, @hoffmann2013oxford]
keskeisimpiä ajatuksia onkin, että
samaa jaottelua voi laajentaa kuvaamaan myös esimerkkien \ref{ee_pros1} -- \ref{ee_syn2}
kaltaisia tapauksia, jolloin kunkin yllä esitetyn esimerkkiparin edustamana
merkittynä on kussakin tapauksessa oma  *konstruktionsa*. Itse asiassa 
myös kategoriat, joihin tavallisesti on viitattu sellaisilla käsitteillä kuin *lekseemi*,
tai *morfeemi*, ovat lopulta samassa mielessä konstruktioita kuin esimerkkien \ref{ee_pros1}
ja \ref{ee_pros2} taustalla oleva rakenne: molemmat kuvaavat kielenkäyttäjän
muistiin on tallentuneita kielen yksiköitä, eikä konstruktiokieliopin piirissä
jaottelu esimerkiksi sanoihin ja lauseisiin ole oleellinen [@ostman2004, 12].

Konstruktioajattelua voidaan siis pitää pohjimmiltaan melko radikaalinakin
irtautumisena koko 1900-luvun loppupuolta hallinneesta generatiivisesta
ajattelutavasta, jota John Taylor [-@taylor2012, 42] luonnehtii karrikoiden
*sanakirja + kielioppi* -malliksi: generatiivisessa (chomskylaisessa)
perinteessä kieli nähdään koostuvan toisaalta joukosta opittuja sanoja sekä
toisaalta joukosta sääntöjä, joiden perusteella sanoista voidaan muodostaa
rajaton määrä erilaisia lauseita. Kontrastina tähän konstruktiokielioppi lähtee
ajatuksesta, jonka mukaan kieli on konstruktioiden muodostama verkko
("konstruktikko"[^tikko]), tallentunut käyttäjän muistiin joukkona toisiinsa
linkittyneitä itsenäisiä yksiköitä. 

\todo[inline]{tosin adjunktiasia, ks. boas 2010 ym.}

[^tikko]: Englanninkielisessä kirjallisuudessa termi *constructicon* on
yleisessä käytössä [ks. esim. @hilpert2014], mutta suomesta vastaava termi vaikuttaisi koko
lailla puuttuvan -- ainoa löytämäni maininta on Petri Jääskeläisen väitöskirja
[-@jaaskelainen2004instrumentatiivisuus, 16].

Kun edellä kuvattua ajatusta soveltaa kysymykseen ajanilmauksen sijainnista,
tämän tutkimuksen kohteeksi tarkentuu sen selvittäminen, minkälaisia
ajanilmauksen sisältäviä konstruktiota -- vakiintuneita malleja tai skeemoja --
suomen- ja venäjänkielisillä viestijöillä mahdollisesti on käytössään. Jaakko
Leino [-@leino2003, 72] huomauttaa, ettei edellä kuvattu ajattelutapa ole
fennistisessä perinteessä välttämättä erityisen vieras tai uusi, vaan
esimerkiksi *ilmaustyypin* ja jopa *lausetyypin* [esim. @hakulinenkarlsson1979]
käsitteet ovat pohjimmiltaan sukua ajatukselle konstruktioista. Eräässä
mielessä tässä tutkimuksessa tarkasteltavia *ajanilmauskonstruktioita* voisikin
varovasti ajatella perinteisemmän fennistisen terminologian mukaisesti omina,
erillisinä lausetyyppeinään siinä missä eksistentiaali- tai kokijalauseetkin.

Konstruktioiden esittämiseen on tavallisesti käytetty niin kutsuttuja
attribuutti--arvo-matriiseja, joissa niin konstruktioon kuuluvien konstituenttien
kuin konstruktioiden itsensä ominaisuuksia kuvataan ikään kuin muuttujina, jotka
saavat erilaisia arvoja [vrt. @leino2003, 64].
Matriisit ovat perusrakenteeltaan laatikkokuvioita, joissa vierekkäisten
ja sisäkkäisten laatikoiden avulla pystytään kuvaamaan konstituenttien välisiä riippuvuussuhteita ja
järjestystä [@ostman2004, 25]. Kussakin laatikossa kuvattavan informaation 
luonne ja määrä voi vaihdella paljonkin ja on luonnollisesti sovellettavissa sen mukaan,
mikä kulloinkin on tutkijan tarkkailun kohteena [@leino2003, 79].
Koska tässä tutkimuksessa tarkasteltavien erilaisten syntaktisten rakenteiden 
variaatio on pitkälti teknisistä ja aineistonhankinnallisista syistä
rajattu minimiin (ks. osio \ref{taks4}), suurin osa esittämistäni
attribuutti-arvo-matriiseista muistuttaa seuraavaa yksinkertaista esimerkkiä:


\noindent



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & act+ \\
df & top \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


funct freq \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]


\vspace{3.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 **Matriisi 1: ** Esimerkki attribuutti--arvo-matriisista \vspace{0.6cm}

\todo[inline]{huom: tässähän ei aina ole objekteista kyse }

Matriisi 1 voisi olla kuvaus esimerkkien \ref{ee_syn1} ja
\ref{ee_syn2} ilmentämästä konstruktiosta, jossa ajanilmaus sijaitsee subjektin ja
verbin välissä. Matriisi koostuu ulommasta laatikosta ja tämän sisällä olevasta
neljästä vierekkäisestä laatikosta, joista kukin kuvaa esimerkkien \ref{ee_syn1} ja
\ref{ee_syn2} edustaman konstruktion konstituentteja. Sisemmillä laatikoilla on
vaihteleva määrä attribuutteja -- tarkoituksenani on
pyrkiä välttämään redundanssia, niin että vain arvot, jotka ovat kulloinkin
tarkasteltavan konstruktion kannalta relevantteja, ilmoitetaan. Esimerkiksi
viimeiseen laatikkoon on kirjattu ainoastaan *gf*-attribuutti arvolla *compl*,
joka kertoo, että kyseinen konstituentti on verbin täydennys; tätä edeltävässä laatikossa
esitetty lauseen verbi on puolestaan merkitty kirjaamalla *cat*-attribuutin
(leksikaalinen kategoria) arvoksi V, muttei tarkempia, esimerkiksi
semantiikasta kertovia ominaisuuksia. Sen sijaan semanttisia ominaisuuksia on
kirjattu ajanilmaukselle, joka lisäksi sisältää semanttisen funktion
ilmaisevan *funct*-attribuutin. Vielä on paikallaan lisätä, että attribuutit
voidaan ryhmitellä kimpuiksi, kuten ensimmäisessä laatikossa, jossa
*prag*-attribuutti koostuu useammasta piirteestä. Nämä piirteet kuvaavat
konstituenttiin liittyviä pragmaattisia ominaisuuksia, ennen muuta seuraavassa
alaluvussa käsiteltävää informaatiorakennetta. 

Palaan konstruktioiden käyttämiseen nimenomaan erilaisten sanajärjestyksien
kuvaajina ja toisaalta kontrastiivisen tutkimuksen välineenä sekä aineistona
olevien lauseiden suppeaan syntaktiseen variaatioon tarkemmin tutkimuksen
myöhemmissä luvuissa, joissa täydennetään matriisissa
1 esitettyä konstruktionotaatiota. Tässä vaiheessa on syytä kuitenkin 
sivuta vielä kysymystä konstruktioiden välisistä linkeistä.

Kuten edellä todettiin, konstruktiot ovat monin tavoin sidoksissa toisiinsa,
niin ettei kieltä nähdä vain listana erilaisia kielenkäytön skeemoja, vaan
erilaisia linkkejä vilisevänä verkkona. 
Keskeisin konstruktioita yhdistävä konsepti on
*periminen* (inheritance): konstruktioiden voi nähdä edustavan eri
abstraktiotasoja, niin että spesifimmät, alemman tason konstruktiot perivät
piirteitä yleisluontoisemmilta ylemmän tason konstruktioilta [@hilpert2014, 66].
Esimerkiksi matriisissa 1 kuvatun konstruktion 
voisi ajatella perivän ominaisuuksia kaikkein yleisimmällä tasolla *verbi +
täydennys* -konstruktiosta. 
Konstruktiokieliopin piirissä on perinteisesti listattu ennen kaikkea
Goldbergin [-@goldberg1995constructions] tekemään jaotteluun nojaten neljä
erilaista perintälinkkiä: toteutumalinkki, polysemialinkki, metaforisen
laajentuman linkki ja osarakennelinkki [suomenkielisistä nimistä ks.
@leino2003, 83]. Tämän tutkimuksen kannalta erilaisten linkkien tarkempi
erottelu ei kuitenkaan ole tarkoituksenmukaista, vaan olennaista on itse  ajatus ominaisuuksien 
perimisestä ja jakamisesta perivien konstruktioiden kesken.

\todo[inline]{Tieto siitä, miten vaihtoehtoiset sijainnit koodataan, vasta myöhemmin
("progressive revelation") 
ajattele myös sitä, että linkkien kuvaaminen voisi ratkaista järjestysongelmaa...
}


\todo[inline]{
    Konstruktiot ja linkit: Selitä, että totta kai jokainen kompleksisempi syntaktinen
    konstruktio koostuu subpart-linkeillä yhdistellyistä pienemmistä 
    osasista (Hilpert 71), mutta tämän tutkimuksen tarkoitusten kannalta ei ole syytä
    keskittyä kuvaamaan niitä

}


Ajanilmaukset ja informaatiorakenne {#informaatiorakenne}
-------------------

Informaatiorakenteen tutkimisessa on nimensä mukaisesti kyse sen
tarkastelemisesta, miten tekstissä välitetään tietoa. Tätä tiedon jäsentämistä
on pidetty morfologian ja syntaksin ohella kolmantena luonnollisen kielen
peruskomponenttina [@lambrecht1996, ??]. Informaatiorakenteen tutkimuksen
kannalta merkittäviä ja etenkin slavistisessa perinteessä käytettyjä ovat niin
sanotun Prahan koulukunnan tutkimukset ([@tobecited] Danes, Firbas, Matesius),
joiden perua ovat muun muassa klassiset käsitteet teema ja reema. \todo[inline]{Princen
maininta?} Kuten
esimerkiksi Leino [-@jleino2013, 319] toteaa, informaatiorakenne on
kuitenkin ollut merkittävä tutkimuskohde myös konstruktiokieliopin piirissä,
missä yhteydessä käsiteparin teema--reema sijasta on tavattu käyttää termejä
topiikki ja fokus.

Yksinkertaistetusti voidaan sanoa, että informaatiorakenteessa oleellista on sen
erittely, mikä tieto on vanhaa tai annettua ja mikä uutta. Jo varhain
informaatiorakenteen tutkijat ovat kuitenkin tunnistaneet, etteivät uutuus ja
annettuus sinänsä ole riittävän tarkkoja käsitteitä kuvaavaan erityyppistä
informaatiota. Esimerkiksi Jeanette Gundel [-@gundel1999topic, 4], toteaakin,
että jokin lauseen elementti voi olla uusi, vanha tai jotain siltä väliltä
ainakin kahdella tasolla: ensinnäkin, jonkin konstituentin -- kuten pronominin
*hän* tai lausekkeen *eräs tamperelainen nainen* -- viittauskohde voi olla
viestijöille tuttu tai tuntematon sekä lisäksi joko aktiivisen huomion kohteena
tai syvemmällä tajunnassa; toiseksi, jokin tämän viittauskohteen ominaisuus tai
viittauskohteeseen liittyvä tapahtuma voi olla viestijöiden tiedossa tai heille
tuntematon. Nimitän tässä ensimmäisenkaltaista informaatiota *diskurssistatukseksi*
ja jälkimmäistä *diskurssifunktioiksi*.[^vilkunadf] Jälkimmäinen termi on
kuitenkin tässä tutkimuksessa myös laajemmassa käytössä kuin ainoastaan 
määrittelemässä tiedossa olemista tai olemattomuutta (vrt. osio \ref{muut-df}).

[^vilkunadf]: Kattavana katsauksena informaatiorakenteen värikkääseen
terminologiaan ks. esim. [@kruijff2003discourse, 254]


### Diskurssistatus

Edellä mainittiin ohimennen ajatus siitä, ettei kirjoitettua tekstiä
tutkittaessa tarvitsisi tehdä oletuksia viestijöiden (Shoren termein)
*tiedontiloista*. Esitin, että kirjoitettua tekstiä kuitenkin voidaan hyvinkin
käsitellä ilmiönä, joka muistuttaa keskustelua: toisin sanoen tilanteena, jossa
viestijä viestii jotakin viestin vastaanottajalle. Oma näkemykseni on, että
viestin rakentumiseen todella vaikuttaa se, minkälaisia oletuksia kirjoittaja
tekee lukijan tiedontiloista. Tämä on erityisen ilmeistä Internet-viestinnässä,
jota suuri osa tämän tutkimuksen aineistoa edustaa [@tobecited]. Kaiken
kaikkaan on kuitenkin korostettava, että vaikka pidän perusasetelmaa (viestijä
ja viestin vastaanottaja) puhutussa ja kirjoitetussa diskurssissa samanlaisena,
en väitä, etteikö puhutun ja kirjoitetun viestintätilanteen välillä olisi myös
merkittäviä eroja.

Olga Yokoyama [-@yokoyama1986] on vienyt ajatuksen tiedontilojen tulkitsemisesta
erityisen pitkälle esittelemällä *tietojoukon* (*set of knowledge* /
*когнитивное множество*)[^yokkaannos] käsitteen.  Yokoyaman mukaan (mts. 3)
jokainen viestintätilanne vaatii vähintään neljä eri komponenttia: viestijät A
ja B sekä joukot, jotka kuvaavat viestijöiden *tämänhetkisen huomion kohdetta*
(*matter of current concern*, *ven. käännös*): $C_a$ ja $C_b$. A ja B voidaan
määrittää tarkemmin tarkoittamaan ei viestijöitä itseään vaan heidän hallussaan
pitämää tietomäärää ylipäänsä. Jotta viestintä ylipäätään olisi mahdollista,
joukkojen A ja B on *leikattava toisensa*: on oltava tiettyjä asioita
(propositioita, referentiaalista tietoa muun muassa eri henkilöistä, yhteinen kieli), jotka
ovat sekä A:n että B:n tiedossa. Tätä leikkauskohtaa, *intersektiota*, voidaan
merkitä joukko-opin notaatiota lainaten $A \cap B$.

[^yokkaannos]: Yokoyaman teos *Discourse and word order* ilmestyi
venäjänkielisenä käännöksenä nimellä xxx vuonna yyy.

$A \cap B$-intersektiota oleellisempi on kuitenkin intersektio $C_a \cap C_b$
eli kohta, jossa tämänhetkisen huomion kohteena olevat asiat leikkaavat.
Tavallisesti tähän joukkoon kuuluvat aina vähintään deiktiset elementit (ks.
osio \ref{x}) eli esimerkiksi keskustelun osapuolet (minä, sinä) ja -- tämän
tutkimuksen kannalta oleellisesti -- nykyhetki ja siihen viittaavat ilmaukset
kuten *nyt*, *сейчас* ja *скоро*. Yokoyama esittää, että viestijät tekevät
jatkuvasti oletuksia siitä, missä tietojoukossa jokin elementti sijaitsee, ja
erityisesti venäjän (kuten myös suomen) kaltaisissa kielissä nämä oletukset
ilmaistaan sanajärjestyksen avulla. Hän antaa (mts. 218) muun muassa seuraavan
yksinkertaisen esimerkin venäjästä (Yokoyamalla numero 30):





\exg. \small Я сейчас уезжаю в Бостон
\label{ee_yokboston}  
 minä nyt lähteä PREP Boston-AKK
\


Yokoyaman analyysin mukaan esimerkissä \ref{ee_yokboston} *Я* ja *сейчас* edustavat
tietoa, joka on kummankin viestijän tämänhetkisen huomion kohteena eli osa sekä
$C_a$- että $C_b$ joukkoa ja sitä kautta $C_a \cap C_b$-intersektiota. *Boston*
puolestaan on osa viestin lähettäjän tämänhetkistä huomiota. Mitä tulee viestin
vastaanottajaan, lähettäjä olettaa että a) *Boston* ei ole viestin lähettämisen
hetkellä vastaanottajan huomion kohteena  ja b) vastaanottaja kuitenkin
ylipäätään tuntee sanan *Boston* viittauskohteen, eli se on osa B-joukkoa,
joskaan ei $C_b$-joukkoa -- tarkemmin ilmaistuna Boston-sanan referentti kuuluu 
joukkoon $C_a \cap (B-C_b)$. 

Yokoyaman mukaan esimerkin \ref{ee_yokboston} kaltaisissa tapauksissa
(konstruktioissa) venäjänkielinen viesti rakennetaan yksinkertaistetusti siten,
että $C_a \cap C_b$-joukkoon kuuluva informaatio sijoitetaan ennen verbiä ja
$C_a \cap (B-C_b)$-joukkoon kuuluva verbin jälkeen. Toisin sanoen, sijoittamalla
ilmauksen tiettyyn kohtaan lausetta puhuja tekee "oletuksen vastaanottajan
tiedontilasta" eli siitä, kuuluuko elementti ylipäätään vastaanottajan hallussa
pitämään tietoon ja siitä, kuinka lähellä vastaanottajan tajunnan pintaa
elementti sijaitsee.

Vaikka Yokoyaman tietojoukkoteoria ei ole erityisen käytetty ja se on ainakin
slavistipiireissä saanut kritiikkiäkin (Anton Zimmerling, henkilökohtainen
tiedonanto), on sen keskeinen ajatus tajunnan eri tasoista ja tiedontiloista
tehtävistä oletuksista kiinnostavan lähellä sitä, mitä monet laajemmin tunnetut
pragmaatikot ovat esittäneet. Vastaavia ajatuksia voidaan havaita esimerkiksi
Knud Lambrechtilla, joka Leinon [-@jleino2013, 319] mukaan on
konstruktiokieliopin piirissä eniten informaatiorakennetta käsitellyt tutkija.

Lambrechtilla idea jonkin tiedon sijaitsemisesta joko viestijän hallussa
ylipäätään tai nykyisen huomion kohteena olevaksi tiedoksi tarkentuu ajatukseksi siitä, kuinka
lähellä tajunnan pintaa jokin tieto on ja kuinka mahdollista tieto on toisaalta
tunnistaa, toisaalta tuoda lähemmäs pintatasoa. Tämä taas ilmenee
konkreettisesti *tunnistettavuuden* (identifiability) ja *aktivoituneisuuden*
(activation) käsitteissä, jotka Lambrecht on edelleen kehittänyt Chafen 
[-@chafe1976; -@chafe1987; esim. -@chafe1994discourse] pohjalta.

Tunnistettavissa oleva tieto -- tai tarkemmin jokin viittauskohde, (diskurssi)referentti --
on Lambrechtin [-@lambrecht1996, 87] ja Chafen [-@chafe1976, 39] mukaan
sellainen, josta virkkeen lausumishetkellä on olemassa representaatio niin
lausujan kuin vastaanottajankin mielessä. Kuulija pystyy oletettavasti
tunnistamaan (identifioimaan) jonkin ilmauksen referentin silloin, kun
keskustelijoiden jakamassa keskusteluavaruudessa (Yokoyaman termein
tietojoukossa $A \cap B$) on jokin yksittäinen elementti, johon ilmaus voi
viitata. Jos elementti on tunnistettavissa, voidaan kysyä, kuinka lähellä
viestijöiden tajunnan pintaa se sijaitsee, jolloin puhutaan aktivoituneisuudesta
[@lambrecht1996, 93--94]. Jos elementti on hyvin lähellä tajunnan pintaa (vertaa
Yokoyaman $C_a$- ja $C_b$-joukot), sen on Lambrechtin (mts. 76, 165) termein
*aktivoitu*; hieman kauempana tajunnan pinnasta olevia elementtejä voidaan nimittää
*käytettävissä oleviksi* ja tunnistettavissa olevia mutta kaukana tajunnan
pinnasta sijaitsevia *käyttämättömiksi*.

Nyt esiteltyjen käsitteiden avulla *diskurssistatus* voidaan alustavasti
määritellä seuraavasti:\linebreak

\noindent 

\noindent 
**Määritelmä 1: ** Jotta elementillä ylipäätään voi olla diskurssistatus,
    elementin on oltava referentiaalinen eli sillä on oltava viittauspiste. Jos
    elementillä on viittauspiste, voidaan kysyä, onko viittauspiste sellainen,
    jonka kaikki viestintätilanteen osapuolet voivat tunnistaa. Jos elementti
    voidaan tunnistaa, se on joko käyttämätön, käytettävissä oleva tai
    aktiivinen. Jos taas elementti ei kuulu viestijöiden jakamaan tietoon, se
    on -- Princen [-@prince1981, ??] termein -- täysin uusi (brand new).\linebreak 

Oman tutkimukseni kannalta on kuitenkin useimmiten riittävä yksinkertaistaa määritelmässä
1 esitettyä käsiteverkkoa, ja keskittyä kahteen olennaiseen
erotteluun: toisaalta siihen, onko jonkin tekstissä esiintyvän kielellisen yksikön
viittauskohde eli diskurssireferentti esitetty täysin uutena vai
tunnistettavana ja toisaalta siihen, onko tunnistettavana esitetty
viittauspiste esitetty aktiivisena vai ei. Käytän ajanilmauskonstruktioita kuvatessani
tähän liittyen merkintää *ds ident+* kuvaamaan niitä referenttejä, joiden
on kuvattavassa konstruktiossa tultava esitetyiksi tunnistettavissa olevina sekä
merkintää *ds act+* kuvaamaan niitä referenttejä, joiden on tultava esitetyiksi
aktiivisina. Selvennän näitä käsitteitä seuraavassa esimerkkien \ref{ee_eduskunta_teoria} -- \ref{ee_libya_teoria}
avulla.




\ex.\label{ee_eduskunta_teoria} \small Eduskunta käsittelee keskiviikkona täysistunnossaan Libyan tilannetta.



Esimerkki \ref{ee_eduskunta_teoria} on keksitty suomenkielinen virke, jota käsitellään
tarkemmin varsinaisen tutkimusaineiston analyysin yhteydessä luvussa \ref{s1-sim-pos} ja
joka on kuviteltu kirjoitetuksi osana uutistekstiä keväällä 2011.
Esimerkistä voidaan ensinnäkin erotella, että siinä on kaksi kielellistä elementtiä,
joille on helppo määrittää viittauskohde -- toisin sanoen kaksi selkeän referentiaalista 
elementtiä. Näitä ovat toisaalta *eduskunta* (jonka referenttiä, Suomen 
200-jäsenistä parlamenttia, merkitsen  *r1*), toisaalta *Libyan
tilannetta* (jonka referenttiä, Libyassa helmikuussa 2011 alkanutta aseellista
konfliktia, merkitsen *r2*). 

Esimerkissä \ref{ee_eduskunta_teoria} ei ole ainuttakaan täysin uutena lukijalle esiteltävää
($A - B$ -tietojokkoon kuuluvaa) elementtiä. Sekä r1 että r2 ovat sen sijaan tunnistettavissa.
Jos esimerkin \ref{ee_eduskunta_teoria} esittäisi osiossa \ref{vsvtk} esiteltyjen
attribuutti--arvo-matriisien avulla, notaatio olisi tässä tutkimuksessa
seuraavanlainen:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & ident+ \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem paikka \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf compl \\[5pt]



prag \[ ds & ident+ \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 **Matriisi 2: ** Esimerkki \ref{ee_eduskunta_teoria} attribuutti--arvo-matriisina \vspace{0.6cm}

Matriisissa 2 lauseen subjektin ja objektin
diskurssistatuksiksi on merkitty yllä kuvatun konvention mukaisesti *ds ident+*.
Huomattakoon, että myös kahden adverbiaalin, *keskiviikkona* ja *täysistunnossa*
voi ajatella viittaavan konkreettiseen, tunnistettavissa olevaan diskurssireferenttiin:
tapahtumaan *eduskunnan keskiviikkoinen täysistunto*. Vastaaviin adverbiaaleihin liittyvien
diskurssireferenttien ja sitä kautta diskurssistatusten määrittely on kuitenkin
usein huomattavasti hämärämpää kuin esimerkin \ref{ee_eduskunta_teoria} subjektiin ja objektiin
liittyvien viittauskohteiden, joilla luvuissa \ref{ajanilmaukset-ja-alkusijainti} --
\ref{ajanilmaukset-ja-loppusijainti} havaitaan usein olevan myös huomattavasti
enemmän selitysvoimaa pohdittaessa ajanilmauksen saamaa sijaintia. Itse ajanilmauksen
diskurssistatukseen liittyvät määrittelyongelmat tulevat esille esimerkiksi
seuraavassa, esimerkin \ref{ee_eduskunta_teoria} aloittamaa diskurssia jatkavassa
lauseessa:




\ex.\label{ee_eduskunta_teoria2} \small Se on viime aikoina kärjistynyt entisestään.



Ajanilmauksella *viime aikoina* voidaan nähdä referenttinä
epämääräinen jakso aikajanalta (vrt. luku \ref{viittauskohteen-tarkkuus}). Kuten luvussa
\ref{selittuxe4vuxe4t-muuttujat} todetaan, tämä ei loppujen lopuksi kuitenkaan ole ajanilmauksen sijainnista
tehtävien päätelmien kannalta erityisen selitysvoimainen ratkaisu, vaan oleellisempaa
on ylipäätään se, että kyseessä on lokalisoivaa funktiota edustava ilmaus. Tämän vuoksi
luvuissa \ref{ajanilmaukset-ja-alkusijainti} -- \ref{kokoava-kontrastiivinen-analyysi}
käsiteltävissä ajanilmauskonstruktioissa ei juurikaan oteta kantaan ajanilmausten
omaan diskurssistatukseen, vaikka sen määrittely alaluvussa \ref{taksn3} kuvatulla
teoreettisella tasolla olisikin mahdollista.

Esimerkki \ref{ee_eduskunta_teoria2} eroaa esimerkistä \ref{ee_eduskunta_teoria} myös siltä osin, että
siinä on vain yksi kielellinen elementti, jolle on mahdollista yksiselitteisesti
määrittää viittauskohde. Tämä viittauskohde on sama kuin toinen esimerkin \ref{ee_eduskunta_teoria} 
sisältämistä referenteistä, Libyassa vallitseva konfliktitilanne (r2). R2-referentin 
diskurssistatus on kuitenkin muuttunut, niin ettei se ole enää vain tunnistettavissa
vaan aktiivinen. Lambrechtin [-@lambrecht1996, ??] mukaan juuri pronominin käyttäminen
substantiivilausekkeen sijasta on kielestä riippumatta tyypillisimpiä keinoja
ilmaista jonkin diskurssireferentin aktiivisuutta. R2-referentin aktiivisuus esimerkissä
\ref{ee_eduskunta_teoria2} kuvattaisiin konstruktionotaatiossa seuraavasti:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]



prag \[ ds & act+ \\
\] \\[5pt]


\vspace{0.3em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{2.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]


\vspace{1.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


\vspace{2.5em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 **Matriisi 3: ** Esimerkki \ref{ee_eduskunta_teoria2} attribuutti--arvo-matriisina \vspace{0.6cm}


Matriisissa 3 subjektin diskurssistatukseksi
on merkitty *act+*, eikä *ident+* kuten matriisissa 2.

Määritelmässä 1 mainituista diskurssistatukseen liittyvistä
käsitteistä on vielä selvittämättä, mitä tässä tutkimuksessa ymmärretään 
niillä diskurssireferenteillä, jotka eivät ole aktiivisia eivätkä edes tunnistettavia,
vaan täysin uusia. Esimerkkinä tällaisista viittauskohteista voidaan tarkastella 
vaikka seuraavaa Libyan vuoden 2011 konfliktista kertovaan Wikipedia-artikkeliin
kuuluvaa virkettä:[^libyavirke]





\ex.\label{ee_libya_teoria} \small Maan entinen oikeusministeri Mustafa Mohamed Abud Ajleil kertoi
lauantaina 26. helmikuuta, että hän pyrkii muodostamaan väliaikaisen
hallituksen.



[^libyavirke]: https://fi.wikipedia.org/wiki/Libyan\_sis%C3%A4llissota#Tapahtumat, tarkistettu 5.4.2018

Esimerkin \ref{ee_libya_teoria} subjektina oleva substantiivilauseke *Maan entinen
oikeusministeri Mustafa Mohammed Abud Ajleil* (r3) eroaa edellä käsitellyistä
esimerkkien \ref{ee_eduskunta_teoria} ja \ref{ee_eduskunta_teoria2} r1- ja
r2-referenteistä siinä, ettei se ole sen paremmin aktiivinen kuin
tunnistettavissakaan. Kuten määritelmässä 
todettiin, tällöin kyse on täysin uudesta viittauskohteesta. R3-referentistä
voidaan kuitenkin sanoa, että se esitellään lukijalle suhteuttamalla se
johonkin, minkä lukija voi tunnistaa (kyseessä on puheenaiheena olevan maan
entinen oikeusministeri). Lambrecht [-@lambrecht1996, 86, 167]
lainaa vastaavia tapauksia varten Princeltä [-@prince1981, 236] käsitteen
*brand new anchored* (täysin uusi, mutta *ankkuroitu*). Prince itse käytti alun
perin esimerkkeinä englannin lauseita *I got on a bus yesterday* ja *A guy I
work with says he knows your sister*, joista ensimmäisessä *a bus*-lausekkeen
referentin hän analysoi täysin uudeksi ja ankkuroimattomaksi ja jälkimmäisessä
*a guy I work with*-lausekkeen referentin taas täysin uudeksi mutta
ankkuroiduksi. Viittaan tässä tutkimuksessa diskurssistatukseltaan täysin
uusiin mutta ankkuroituihin diskurssireferentteihin merkinnällä *ds anch+*.


### Diskurssifunktio

Käytän termiä *diskurssifunktio* lähtökohtaisesti kuvaamaan
informaatiorakenteen perinteisiä ydinkäsitteitä *topiikki* ja *fokus* (teema ja
reema). Termin  yksi tarkoitus on tässä suhteuttaa toisiinsa Gundelin
[-@gundel1999topic, 125] mainitsemat informaatiorakenteen kaksi osatekijää,
joista toisen muodostavat diskurssifunktiot (*suhteellinen* merkitys Gundelin
termein), toisen edellä käsitelty diskurssistatus (*referentiaalinen* merkitys
Gundelin termein).[^termiero] Laajennan kuitenkin vielä käsitettä myös varsinaisen
informaatiorakenteen tutkimuksesta tekstintutkimuksen suuntaan, niin että
topiikin tai fokuksen lisäksi etenkin tutkimuksessa tarkasteltavien ajanilmauksilla
voi olla myös eräitä muita diskurssifunktioita, joita käsitellään alaluvussa
\ref{muut-df}. Tämä on sikäli olennainen lisäys, että ajanilmausten määritteleminen
juuri topiikiksi tai fokukseksi ei useinkaan ole yksiselitteistä, ja juuri 
topiikista ja fokuksesta poikkeavat diskurssifunktiot ovatkin ajanilmauksen 
sijaintia mietittäessä selitysvoimaisempia.


[^termiero]: Huomattakoon, että käyttämäni diskurssifunktion käsite eroaa esimerkiksi
Vilkunan [-@vilkuna1989, 38] tai Virtasen [-@virtanen1992discourse, 44]
käyttämistä *discourse function* -termeistä. Sen sijaan tässä käytetty diskurssifunktion
käsite on lähellä Dikin [-@dik1989, 266] termiä *pragmatic function*.

#### Topiikki

Lambrechtin [-@lambrecht1996, 118] pohjalta voidaan määritellä, että *topiikin*
diskurssifunktion täyttää lauseessa[^lausediscl] se elementti, josta kyseinen
lause  kertoo tai josta lauseessa on kyse. Kuten Vilkuna [-@vilkuna1989, 79]
huomauttaa, informaatiorakenteen tutkimuksessa vastaavan määritelmän
kilpailijana on perinteisesti pidetty määritelmää, jonka mukaan topiikki (tai
teema) on se osa lauseesta, joka edustaa vanhaa tai annettua (*old/given*)
informaatiota.
*Uutuus* ja *annettuus* ovat nähdäkseni kuitenkin itsessään liian
yksinkertaistavia käsitteitä. Yhdistänkin niiden merkityspiiriin kuuluvat ilmiöt
tässä ennemmin diskurssistatukseen kuin diskurssifunktioihin. Lambrecht
[-@lambrecht1996, 117; vrt. myös @guijarro2001, 104] korostaa lisäksi eroa siihen, että topiikki
määriteltäisiin lineaarisesti niin, että mikä tahansa on virkkeessä ensimmäisenä
elementtinä, on topiikki. Vastaava määritelmä olisi erityisen ongelmallinen
tämän tutkimuksen kannalta: koska tarkoituksena on muun muassa pyrkiä
ymmärtämään eri syitä ajanilmauksen lauseenalkuiseen sijaintiin, olisi hyödytön
kehäpäätelmä määrittää *a priori*, että kaikkien lauseenalkuisten ajanilmausten
diskurssifunktio on topiikki.

[^lausediscl]:  Puhun tässä yksinkertaisuuden vuoksi *lauseista*, vaikka
kyse saattaa yhtä hyvin olla kompleksisemmista rakenteista kuten yhdyslauseista,
tai -- kuten alempana nähdään -- laajemmistakin tekstikokonaisuuksista.

Koska tutkimuksen kohteena ovat suomalaiset ja  venäläiset ajanilmaukset, on
syytä kiinnittää erityistä huomiota venäläisen tutkimustradition tapaan
määrittää topiikki tai topiikinkaltainen diskurssifunktio. Venäläinen
informaatiorakenteen tutkimus nojaa voimakkaasti Prahan koulukunnan
tutkimustuloksiin, ja merkittävimpänä nimenomaan venäjän kieltä käsittelevänä
kontribuutiona pidetään yleisesti Irina Kovtunovan [-@kovtunova1976] tutkimusta
[@zimmerling2013, 262-263]. Kovtunovan määritelmän mukaan teema[^teematop] on
*lauseen lähtökohta* ja *aihe* [-@kovtunova1976, 6-7]. Janko [-@janko2001, 23,
25] määrittelee teeman suhteessa tämän vastakohtaan, reemaan, niin että teema on
*viestin ei-toteava osa*, joka muodostaa *lähtökohdan* viestille. 

\todo[inline]{Tähän väliin King + d-neutral?}

[^teematop]: käytän toistaiseksi käsitettä teema venäläisestä
tutkimuksesta puhuttaessa

Oleellinen kysymys suhteessa topiikin käsitteeseen on, minkälaiset elementit
voivat toimia tai tyypillisesti toimivat topiikin funktiossa. Ensinnäkin on
tyypillistä, että lauseen subjekti on topiikki [@lambrecht1996, 131]. Voidaan
sanoa, että subjekti on eräänlainen oletuskandidaatti[^dt] topiikin funktioon.
Esimerkiksi lauseen \ref{ee_subtop} Миша on selkeästi referentti, josta lause kertoo:




\ex.\label{ee_subtop} \small Миша играет на скрипке



[^dt]: vrt. käsite *DT* Vilkunalla [-@vilkuna1989, 41]

Subjektit eivät kuitenkaan ole ainoita mahdollisia topiikkeja, eikä se, että
lauseessa on subjekti, automaattisesti tarkoita, että subjekti saisi topiikin
funktion. Tämän tutkimuksen kannalta tärkeää on huomata, että myös adverbiaalit
voivat toimia topiikin funktiossa. Topiikkina ei voi silti esiintyä mikä
tahansa lauseen elementti, vaan funktioon liittyy olennaisia rajoitteita, joita
on hyvä havainnollistaa edellä käsitellyn diskurssistatuksen avulla.

Ensinnäkin, topiikiksi kelpaa vain elementti, jolla
ylipäätään on jokin diskurssistatus --  toisin sanoen elementin
on oltava luonteeltaan referentiaalinen. On vaikea esittää lauseen kertovan
*jostakin*, jos potentiaalisella topiikilla ei voi olla viittauskohdetta
viestintätilanteen osapuolten mielessä. Tarkemmin sanottuna lauseet eivät ylipäätään
kerro jostakin sanasta tai konstituentista, vaan juuri *referentistä*, johon
konstituentti viittaa [@lambrecht1996, 161].

\todo[inline]{ Lisäksi... [@lambrecht1996, 150] }

[^DT]: Vilkunan [@vilkuna1989] analyysissä T-kentällä (ks. osio 4.x) on
esimerkiksi oma alalajinsa, DT, joka viittaa ennen kaikkea
juuri subjekteihin.

Toiseksi, topiikiksi kelpaavuuden voi nähdä asteittaisena ominaisuutena, niin
että todennäköisyys toimia topiikin funktiossa kasvaa sitä suuremmaksi, mitä
lähempänä tajunnan pintaa viestin lähettäjä olettaa entiteetin vastaanottajalla
sijaitsevan [-@lambrecht1996, 165]. Kaikkein todennäköisimpiä topiikkeja ovat
tämän oletuksen mukaan diskurssistatukseltaan aktivoidut entiteetit, hivenen
vähemmän todennäköisiä käytettävissä olevat ja edelleen vähemmän todennäköisiä
käyttämättömät entiteetit. Tunnistamattomat eli täysin uudet elementit ovat
topikaalisuusasteikon alapäässä niin, että täysin uudet ankkuroidut ilmaukset
 ovat kuitenkin todennäköisempiä topiikkeja kuin täysin
uudet ankkuroimattomat. 


#### Fokus

Fokuksen diskurssifunktio on perinteisesti ollut toinen osapuoli käsiteparissa,
jonka toisena osapuolena on topiikki tai teema. Esimerkiksi Kovtunova
[@kovtunova1976, 6, 39] määrittelee, että lauseet voidaan jakaa kahteen osaan,
joista ensimmäisen tehtävänä on toimia lähtökohtana (teema) ja toisen *kertoa
jotakin ensimmäisestä, toisin sanoen vastata kysymykseen, joka esitetään suhteessa
ensimmäiseen* (kursivointi omani). Gundel [-@gundel1999focus, 295] käyttää tällä
tavalla määritellystä fokuksesta termiä *semanttinen fokus*. Hänen mukaansa
semanttinen fokus on se osa lausetta, joka vastaa (implisiittiseen tai
eksplisiittiseen) lausetta koskevaan kysymykseen siinä kontekstissa, jossa lausetta
käytetään.

Kovtunovalta saatava määritelmä on sikäli puutteellinen, ettei kaikissa
tapauksissa Lambrechtin [-@lambrecht1996, 206] mukaan voida erottaa "ensimmäistä
osaa", mistä seuraa, ettei fokusta aina voi määrittää suhteessa topiikkiin tai
teemaan.[^topiktonesim] On myös tärkeä huomata, ettei
*uutuuden* käsite automaattisesti nivoudu fokuksen käsitteeseen. Esimerkiksi
Gundelin [-@gundel1999topic, 126] mukaan jokin entiteetti voi olla
viestintätilanteen osapuolille tuttu tai annettu, mutta silti toimia fokuksen
funktiossa [@gundel1999topic, 126].

[^topiktonesim]: Yksi Lambrechtin (mts. 177) antama esimerkki topiikittomista
lauseista ovat viestintätilanteen aloittavat *esittelylauseet* (*presentational
sentences*) tyyppiä *olipa kerran prinssi*. 

Jotta tässä käytettävä Lambrechtin oma määritelmä fokuksesta kävisi ymmärrettäväksi, on syytä
tarkastella kahta hänen teoriansa kannalta olennaista käsitettä, pragmaattista
olettamaa  (*pragmatic presupposition*) ja *pragmaattista väittämää* (*pragmatic
assertion*). Lambrecht selventää käsitteiden sisältöä seuraavalla
esimerkillä [-@lambrecht1996, 52]:




\ex.\label{ee_lambfinally} \small I finally met the woman who moved in downstairs



Lambrechtin (mts. 54) mukaan henkilö, joka esittää lauseen \ref{ee_lambfinally}, olettaa
vastaajan tietävän, että a) joku on muuttanut alakertaan, b) tuo joku on
sukupuoleltaan nainen ja c) viestin lähettäjän olisi olettanut tapaavan kyseisen
henkilön jo aiemmin. Tämä informaatio muodostaa esimerkin \ref{ee_lambfinally}
kontekstissa pragmaattisen olettaman. Pragmaattinen väittämä puolestaan on
Lambrechtin (mts. 52) mukaan *se lauseen ilmaisema propositio, joka viestin
vastaanottajan oletetaan tietävän sen jälkeen, kun tämä on vastaanottanut
viestin*, esimerkissä \ref{ee_lambfinally} se, että alakertaan on muuttanut joku
nainen joka viestin lähettäjän olisi jo pitänyt tavata sekä se, että viestin
lähettäjä on nyt tavannut kyseisen henkilön.

Lambrecht [-@lambrecht1996, 213] esittää fokuksen määritelmäksi, että kyseessä on 
 *se (semanttinen) osa propositiota, jonka suhteen pragmaattinen
väittämä eroaa pragmaattisesta olettamasta"*. Jos esimerkissä
\ref{ee_lambfinally} pragmaattinen väittämä kattaa tiedot (vastaanottajan oletetaan
tietävän että) [joku nainen on muuttanut
alakertaan; viestin lähettäjä on tavannut naisen; tapaamisen olisi pitänyt
tapahtua jo aiemmin] ja pragmaattinen olettama
näiden lisäksi tiedon [joku nainen on muuttanut alakertaan], on fokus silloin se, että viestin
lähettäjä on tavannut naisen. Huomaa, että tällä tavoin määriteltynä fokus on
todella nimenomaan oletettujen tiedontilojen suhde: se ei
määräydy syntaktisesti tietyn lauseaseman mukaan, eikä siitä toisaalta voida
sanoa, että jokin tietty sana tai lauseke on fokus, vaan pikemminkin fokus on
viestinjälkeisin tiedon suhde viestiä edeltävään.

Palaan tässä kohtaa diskurssistatuksen käsitettä määriteltäessä käytettyyn
esimerkkiin \ref{ee_eduskunta_teoria2}, joka esitetään uudestaan numerolla
\ref{ee_eduskunta_teoria2b}:




\ex.\label{ee_eduskunta_teoria2b} \small Se on viime aikoina kärjistynyt entisestään.



Esimerkissä \ref{ee_eduskunta_teoria2b} -- jos ajatellaan, että tätä ennen on esitetty
esimerkki \ref{ee_eduskunta_teoria} -- pragmaattiseksi olettamaksi voitaisiin määritellä 
esimerkiksi seuraavat väitteet
[Suomessa on olemassa eduskunnaksi kutsuttu elin, joka käsittelee erilaisia kysymyksiä;
Eduskunta aikoo käsitellä Libyan tilannetta; Libyan tilanne on negatiivinen (siellä on konflikti)]. 
Esimerkin \ref{ee_eduskunta_teoria2b} pragmaattinen väittämä lisää tähän väittämien listaan
tiedon siitä, että Libyan tilanne on muuttunut vielä negatiivisemmaksi.

Selvitän lopuksi ajanilmausten esiintymistä fokuksena tarkastelemalla vielä yhtä
vuoteen 2011 liittyvää keksittyä virkettä \ref{ee_libyakesti}:




\ex.\label{ee_libyakesti} \small Libyan sisällissota kesti kahdeksan kuukautta.



Esimerkistä \ref{ee_libyakesti} voidaan todeta, että ajanilmaus *kahdeksan kuukautta*
on vähintäänkin osa fokuskenttää [vrt. Lambrechtin  käsite *focus domain* -@lambrecht1996, x]
tai *fokaalinen*. Konstruktionotaationa esimerkki \ref{ee_libyakesti} esitettäisiin
tässä tutkimuksessa seuraavasti:



\vspace{0.4cm}

\scriptsize 

\begin{avm}%
\minibox[frame,pad=4pt,rule=0.1pt]{
\minibox[pad=4pt,rule=0pt]{

\minibox[frame,pad=4pt,rule=0.1pt]{

gf subj \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

cat V \\[5pt]


\vspace{3.5em}
}
\minibox[frame,pad=4pt,rule=0.1pt]{

gf adv \\[5pt]


sem aika \\[5pt]



prag \[ df & foc \\
\] \\[5pt]


\vspace{0.3em}
}}}%
\end{avm}

\normalsize

\vspace{0.4cm}



 **Matriisi 4: ** Esimerkki \ref{ee_libyakesti} attribuutti--arvo-matriisina \vspace{0.6cm}

Matriisista 4 on tarkoituksella jätetty pois muut
kuin ajanilmauksen sijainnin kannalta olennaiset diskurssifunktioihin ja
-statuksiin liittyvät merkinnät. Koska ajanilmauksen sijainti on parhaiten
selitettävissä sen fokaalisuudella (vrt. alaluku \ref{loppus_np} ja *fokaalisen
konstruktion* määrittely), on sen diskurssifunktioksi (df) merkitty fokaalista
konstituenttia tarkoittava *foc*. 


#### Ajanilmauksen topikaalisuuteen liittyviä määrittelyongelmia

Kuten edellä on jo useammassa yhteydessä käynyt ilmi, en näe mielekkääksi
pyrkiä sovittamaan ajanilmauksia aina joko topiikin tai fokuksen
diskurssifunktioihin. Erityisen paljon määrittelyongelmia liittyy topiikin
käsitteeseen.

Topiikki määriteltiin edellä yksinkertaistaen siksi, mistä jokin lause kertoo.
Kuten Shore [-@shore2008, 39] huomauttaa, puheenaiheena oleminen on lopulta siinä
mielessä petollinen määritelmä, että oikeasta kulmasta katsottuna lauseen voi
nähdä kertovan melkeinpä mistä tahansa siinä mainitusta. Esimerkiksi lauseen
\ref{ee_eduskunta_teoria2b} voi periaatteessa tulkita kertovan ajanilmauksen *viime aikoina*
referentistä -- siitä aikajanan segmentistä, johon ajanilmaus viittaa, mutta
ajanilmauksen kutsuminen lauseen topiikiksi vaikuttaisi silti jossain määrin 
ongelmalliselta ja epämääräiseltä.

Avuksi tähän määrittelyn epämääräisyyteen voidaan ottaa Lambrechtin
[-@lambrecht1996, 119] ajatus siitä, että myös topikaalisuus tulisi ymmärtää
asteittaisena ilmiönä, niin että jokin propositio *p* kertoo jossain määrin
referentistä *a*, jossain määrin -- mahdollisesti enemmän -- referentistä *b*,
ja yhdessä ja samassa lauseessa voi siis olla useampia topiikkeja. Myös
esimerkin \ref{ee_eduskunta_teoria2b} *viime aikoina* -ajanilmauksen voi nähdä
jossain määrin *topikaalisena*, sikäli kuin tällaisella analyysilla on
jonkinlaista lisäarvoa pohdittaessa syitä ajanilmauksen sijainnille. Selkeimmin
lauseen topiikkina on kuitenkin nimenomaan Libyan tilanne, josta kerrotaan
tiettyä ajallista taustaa vasten. 

Ajatus useasta topiikista (teemasta) on keskeinen myös Jankolla, joka esittää
seuraavan esimerkin: [-@janko2001, 78][^alunperinp]

[^alunperinp]: Alun perin esimerkki peräisin Paduchevalta 19xx, joka on
poiminut sen Tshehovin novellista X.




\ex.\label{ee_grob} \small На другой день в двенадцатом часу гробовщик и его дочери вышли из
калитки.



Jankon analyysin mukaan virkkeessä \ref{ee_grob} sekä *на другой день*, *в двенадцатом
часу вечера* että *гробовщик и его дочери* ovat samanlaisia, "täysiverisiä"
(*полноценные*), teemoja. Jos mietitään mainittujen topiikkien
informaatiorakenteellista painoarvoa, niiden väliltä todella on vaikea löytää eroja:
esimerkki \ref{ee_grob} tuntuu kertovan yhtä paljon ja samansuuruisella painotuksella
niin seuraavasta päivästä, kahdennestatoista tunnista kuin arkuntekijästä ja tämän
tyttäristäkin. Vastaavissa tapauksissa on toisaalta usein ollut tapana puhua
myös erityyppisistä topiikeista tai teemoista. Esimerkiksi Shore
[-@shore2008, 45] käyttää nimitystä *orientoiva sivuteema* ja havainnollistaa 
käsitettä seuraavalla lauseella:




\ex.\label{ee_shore45} \small Viime vuosikymmenellä me suomalaiset kulutimme ennätysmäärän ulkomaisia
tuotteita.



Esimerkissä \ref{ee_shore45} Shore analysoi ajanilmauksen *viime vuosikymmenellä*
(orientoivaksi) sivuteemaksi ja subjektin *me suomalaiset* varsinaiseksi
teemaksi. Samantyyppiseen analyysiin viittaavat myös esimerkiksi Chafen
[-@chafe1976] käyttämä nimitys *scene-setting topics* [vrt. @le2003time ja
*frame-setting topics*] sekä venäjänkielisessä tutkimusperinteessä tavallinen
termi *lokalisaattori* (*локализатор*) [ks. esim  @suleimanova2015, 210], joka
kuitenkin luonnollisesti rajoittuu lokalisoiviin ilmauksiin. Kaikkien mainittujen 
käsitteiden ongelmana tämän tutkimuksen kannalta on, että niiden määrittely
tuntuu olevan väkisinkin jo valmiiksi sidoksissa niiden sijaintiin: *sivuteema*
on jotakin, joka ilmaistaan *varsinaista teemaa* edeltävällä konstituentilla.
Samaten *scene-setting topic* viittaa juuri lauseen alkuun [@lambrecht1996,
??]. 

Esimerkin \ref{ee_shore45} kaltaisista lokalisoivista ilmauksista on myös
tavallisesti huomautettu, että ne ovat syntaktisesti irrallaan muusta lauseesta
ja liittyvät siihen ennen kaikkea semanttisesti [@shi2000topic, 388]. Ne voi
tulkita verbin mutta toisaalta myös koko lauseen määritteiksi: tällaisen
syntaktisen määritelmän nojalla erityisesti fennistiikassa on ollut tapana
nimittää esimerkin \ref{ee_shore45} kaltaisia ajanilmauksia *kehysadverbiaaleiksi*
[ks. esim. @hakulinenkarlsson1979, xx, @visk, xx].  Kovtunovalla
[-@kovtunova1976, 61] puolestaan vastaava nimitys on *adverbiaalideterminantti*
(обстоятельственный детерминант). Oma 
lähestymistapani  on kuitenkin olla ottamatta 
kantaa ajanilmauksen syntaktisen aseman hienojakoisempaan kuvaukseen tai
topiikkien jaotteluun esimerkiksi erilaisiksi sivuteemoiksi. Sen sijaan 
näen jossain määrin hyödylliseksi toisentyyppisen, topiikkien suhdetta
ylemmän tason tekstuaalisiin kokonaisuuksiin kuvaavan jaottelun,
jonka esitän lyhyesti seuraavassa.

#### Eri tason topiikkeja

Edellä mainittiin, että lausetason lisäksi informaatiorakennetta on mahdollista
tarkastella ylemmällä, esimerkiksi kokonaisten tekstien tasolla. Toteutan
ylemmän tason tarkastelun hyödyntämällä Simon Dikin [-@dik1989] tapaa
jaotella topiikit useampaan eri alaluokkaan riippuen siitä, mikä rooli niillä
on sen laajemman tekstikokonaisuuden kannalta, jonka osina ne esiintyvät.
[esimerkkinä jaottelun kontrastiivisesta sovellutuksesta ks. @hildalgo2010].

Dik (mts. 267) lähtee liikkeelle siitä, että minkä tahansa diskurssin
(kirjoitetun tekstin, luennon, keskustelun ym.) voi aina nähdä *kertovan
jostakin*. Jos tätä vertaa edellä esitettyyn topiikin määritelmään, voidaan
perustellusti tulkita, että myös kokonaiset diskurssit toteuttavat topiikin
funktiota. Näitä ylätason topiikkeja Dik nimittää *diskurssitopiikeiksi*.
Diskurssitopiikeista on syytä huomata, että niitä voi olla monentasoisia ja ne voivat
olla hierarkkisessa suhteessa keskenään: esimerkiksi tämä kappale kokonaisuudessaan
on kertonut diskurssitopiikista, mutta samalla se on osa informaatiorakenteesta kertovaa
osiota ajanilmausten sijaintia käsittelevässä väitöskirjassa.

Diskurssitopiikin lisäksi Dik määrittelee erilaisiksi topiikin lajeiksi *uudet
topiikit* (new topic), *esitellyt topiikit* (given topic), *alatopiikit*
(subtopic) ja *palautetut topiikit* (resumed topic). Ajatus annetuista ja
palautetuista topiikeista palvelee osin edellä kuvattuun diskurssistatukseen
liittyvän käsitteistön kanssa päällekkäisiä tarkoitusperiä, mutta erityisesti
uuden topiikin ja alatopiikin käsitteet ovat myös tämän tutkimuksen kannalta
selitysvoimaisia konsepteja. Käytän niistä jatkossa termejä 
*esittelytopiikki* ja *alatopiikki* [vrt. @sandberg19]. 

Tekstin luominen voidaan Dikin mallissa nähdä prosessina, jossa viestijä
esittelee uusia diskurssitopiikkeja ja toisaalta pyrkii pitämään jo esitellyt
topiikit käytettävissä (vrt. diskurssistatus ja käytettävissä oleminen edellä)
viittaamalla niihin eri tavoin -- tätä prosessia on Dikin (mts. 271) mukaan
nimitetty muun muassa käsitteellä *topiikkiketju* [ks. @givon1983topic]. 
Topiikkiketjut rakentuvat tavallisesti yksinkertaisimmillaan niin, että 
ketjun alussa uusi diskurssitopiikki tuodaan esittelytopiikkina
mukaan tekstiin, minkä jälkeen siihen voidaan joko viitata
jo esiteltynä topiikkina tai siitä voidaan esimerkiksi lohkaista uusi 
alatopiikki. Topiikkiketjun ajatusta voidaan havainnollistaa seuraavalla
analyysitapaa soveltaneen Jesus Guijarron [-@guijarro2001, 110]
esittämällä esimerkillä (tässä lyhennettynä):




\ex.\label{ee_guijarro} \small ...Crete (1) has a fascinating history... Being the largest of the Greek
islands, its landscape (2) changes at every road bend... ...whaterver
your idea of a good time, Crete’s (3) got something just for you!



Guijarron analyysin mukaan katkelman ensimmäinen *Crete* (1) on esittelytopiikki.
Tässä numerolla 2 merkitty *landscape* edustaa Kreetan saaren esittelyn myötä 
diskurssistatukseltaan semiaktiiviseksi tullutta entiteettiä (ks. 
\ref{diskurssistatus} edellä), jota voidaan käyttää alatopiikin roolissa.
Kohdassa 3 on puolestaan kyseessä esitelty topiikki:
kirjoittaja viittaa kohteeseen, joka on kerran jo tuotu tekstiin ja  siten sijaitsee 
oletettavasti lähellä lukijan tajunnan pintaa eikä vaadi erityistä lauseasemaa
tai tarkentavia määreitä.

Tutkimuksen luvussa \ref{likim-s1} havaitaan, että alatopiikit ovat tärkeässä
roolissa, kun mietitään etenkin lauseenalkuisen ajanilmauksen diskurssifunktioita
tekstissä. Tässäkin yhteydessä on otettava huomioon luvussa \ref{ajanilm-kasite}
esitetty ajatus siitä, että aika ja temporaalisuus hahmotetaan usein paikan 
ja spatiaalisuuden tarjoaman mallin mukaan. Vastaavalla tavalla kuin esimerkissä \ref{ee_guijarro}
kirjoittaja rakentaa tekstiään pilkkomalla käsiteltävää aihetta erilaisiin maantieteellisiin
palasiin, on tavallista, että jokin diskurssitopiikki kuvataan
erilaisten ajallisten segmenttien kautta. Esimerkiksi jonkun tekstin aiheena olevan
henkilön elämää voitaisiin käsitellä jaottelemalla se relevantteihin ajanjaksoihin kuten
lapsuus, nuoruus ja aikuisuus [vrt. @vilkuna1989, 92].

Esittelytopiikeista on paikallaan huomata, että niitä voidaan toteuttaa
lukuisilla eri strategioilla. Monet uuden topiikin
esittelytavat ovat enemmän tai vähemmän universaaleja, mutta esimerkiksi
venäjässä tyypilliset verbialkuiset rakenteet kuten Yokoyaman [-@yokoyama1986,
217] esimerkki *К вам пришла Галина Петровна* [vrt. myös @kovtunova1976, 68]
eivät tulisi kysymykseen suomessa. Joskus
kirjoittaja voi käyttää erityisiä metakielellisiä rakenteita kuten *Kerron nyt X:stä*, 
mutta usein uuden topiikin esitteleminen kuitenkin tapahtuu hienovaraisemmin,
esimerkiksi tiettyjen ilmestymistä merkitsevien verbien avulla [@dik1989, 268 vrt. myös
@vilkuna1989, 165; @janko2001, 161]. 

Dik (mts. 269) mainitsee, että
esittelytopiikit sijaitsevat tyypillisesti kielestä riippumatta lauseen
loppupuolella. Ei kuitenkaan ole lainkaan harvinaista, että esittelytopiikkina
on esimerkiksi lauseen alussa oleva subjekti. Esimerkiksi Lambrecht
[-@lambrecht1996, 143, 177] käyttää uuden topiikin diskurssiin tuovista
rakenteista nimitystä *esittelykonstruktio* (*presentational construction*),
jollaisista ja antaa esimerkkinä yhtä lailla satukirjan aloittavan *once there
was a wizard* kuin arkisessa puhetilanteessa käytettävän *JOHN called* (isot
kirjaimet Lambrechtin tapa osoittaa prosodista korostamista). 
Esittelykonstruktiosta irrallinen ilmiö ovat tavallisesti *topikalisoiduiksi*
kutsutut tapaukset, joissa jokin elementti tuodaan topiikille 
tyypilliseen asemaan lauseen alkuun [@lambrecht1996, 147; @vilkuna1989, 91].


#### Muut diskurssifunktiot {#muut-df}

Informaatiorakenteen tutkimuksessa on usein tapana erotella paitsi topikiin ja
fokuksen diskurssifunktiot, myös suorittaa tarkempia jaotteluita esimerkiksi
sen perusteella, onko kyseessä *kontrastiivinen* fokus tai topiikki.
Esimerkiksi Jekaterina Janko, joka tarkastelee informaatiorakennetta puheaktien
käsitteen kautta, erottelee toisistaan *puheakteja muodostavat funktiot* kuten
fokus ja *puheakteja muokkaavat funktiot*, joita ovat muun muassa kontrasti,
vahvistus ja emfaasi [-@janko2001, 46]. 

Jankon (mts. 47) mukaan kontrastin käsitteelle on olennaista ajatus siitä,
että jostakin vaihtoehtojen joukosta valitaan tai osoitetaan oikeaksi yksi
tietty vaihtoehto. Gundell [-@gundel1999focus, 294] puolestaan määrittelee, että
kontrastin tapauksessa viestin lähettäjä ajattelee, ettei viestin vastaanottaja
ole keskittänyt huomiotaan siihen entiteettiin, jossa huomion lähettäjän
mielestä pitäisi olla. Janko antaa muun muassa seuraavan esimerkin:




\ex.\label{ee_vasjamasha} \small Вася пришел (а не Маша)



Esimerkissä \ref{ee_vasjamasha} olennaista on, että Vasja on erotettu muusta
lauseesta intonaation keinoin. Lauseesta on helppo määrittää, että siinä
kuvataan kontrasti *Vasja*- ja *Maša*-sanojen referenttien välillä. Voisi
määritellä, että *Vasja* on lauseen kontrastiivinen fokus.

Informaatiorakennetta käsittelevässä kirjallisuudessa ei kuitenkaan ole aina
yksiselitteistä, katsotaanko kontrastin kohteena oleva entiteetti
yläkäsitteeltään topiikiksi vai fokukseksi. Esimerkiksi Gundell esittää edellä
mainitun määritelmänsä yhteydessä seuraavan esimerkin:




\ex.\label{ee_gundellrob} \small To ROB you can complain about anything having to do with the PROGRAM,
and to CHRISTINE you can address all the OTHER complaints.



Gundellin mukaan esimerkissä \ref{ee_gundellrob} on kyse
puhtaan kontrastiivisesta *fokuksesta.* Toisaalta 
esimerkiksi Carola Féry ja Manfred Krifka [-@fery2009information, 127] kuvaavat seuraavaa
esimerkkiä kontrastiiviseksi *topiikiksi*:




\ex.\label{ee_fery8} \small A: What are your sisters playing? B: My YOUNGER sister plays the VIOLIN,
and my OLDER sister, the FLUTE.



Esimerkeistä \ref{ee_gundellrob} ja \ref{ee_fery8} toinen on siis analysoitu kontrastiiviseksi
fokukseksi, toinen kontrastiiviseksi topiikiksi, vaikka päällisin puolin
kumpikin kuvaa melko samanlaista viestintätilannetta. Itse
en näe tämän tutkimuksen kannalta tarkoituksenmukaiseksi tehdä
mitään ehdotonta erottelua kontrastiivisten topiikkien ja kontrastiivisen fokusten välillä, ja
katsonkin kontrastin yksinkertaisesti omaksi diskurssifunktiokseen -- yhdeksi mahdolliseksi
diskurssifunktio-attribuutin arvoksi siinä missä topiikin ja fokuksenkin.

Kontrastin ohella toinen topiikin ja fokuksen lisäksi käyttämäni diskurssifunktio
on *affekti*, jolla viittaan Isoa suomen kielioppia (VISK §1707) mukaillen siihen, miten *puhuja 
osoittaa suhtautumistaan tai asennoitumistaan puheenalaiseen asiaan tai
puhekumppaniinsa*. Esimerkiksi Yokoyaman [-@yokoyama1986, 256] mukaan suhtautumisella viestittävään
asiaan tai viestissä mainittaviin referentteihin on ainakin venäjässä
vaikutusta myös syntaksin tasolle ja siihen, mihin kohtaa lausetta
elementtejä sijoitetaan. Yokoyama viittaa ilmiöön *empatian* käsitteellä
[Alun perin ks. @kuno1977empathy]. 

\todo[inline]{@besnier1990 ? }

Topiikin, fokuksen, kontrastin ja affektin lisäksi ajanilmuksen diskurssifunktioksi
määritellään joissain tapauksissa pelkästään *muu*. Tälläinen tapaus
koskee esimerkiksi seuraavan varsinaiseen tutkimusaineistoon
kuuluvan virkettä, jota käsitellään tarkemmin luvussa \ref{varsinainen-resultatiivinen-ekstensio}.




\ex.\label{ee_rutiinit_teoria} \small Arjen rutiinit rikkoutuvat ja \emph{äkkiä} elämä saa odottamattoman
käänteen.



Esimerkissä \ref{ee_rutiinit_teoria} ei ole tarkoituksenmukaista puhua *äkkiä*-sanan
diskurssifunktiosta siinä mielessä, että sen voisi jollain tasolla katsoa
olevan virkkeessä fokuksena tai topiikkina tai toisaalta kontrastin tai
affektin ilmaisimena. Tästä huolimatta sillä on
selkeä viestinnällinen tehtävä: se auttaa luomaan ajatuksen
siirtymisestä staattisesta tilasta kohti jotakin muutosta ja toimii
eräänlaisena konnektorina tai "segmentaatiomarkkerina" ['segmentation marker', vrt. @bestgen1995, 387].

Luvussa  \ref{morf-sama-keski} tarkasteltavassa esimerkissä \ref{ee_atempnyt_teoria}
nyt-sanalla puolestaan on lähes tyystin ei-ajallinen diskurssipartikkelin funktio:




\ex.\label{ee_atempnyt_teoria} \small Jos joku nyt vetää herneen siitä nenään, että blogin pitäjä sattuu
pitämään jostakin/ jonkun mielipiteestä, niin ehkä kannattaisi sitten
seurata jotain blogia, jonka kirjoittajalla ei ole mielipiteitä.



\todo[inline]{Esimerkki konstruktionotaatiosta vasta/jo tänne?}

Myös esimerkin \ref{ee_atempnyt_teoria} kaltaiset tapaukset esitetään
konstruktioita kuvaavissa piirrematriiseissa *muu*-merkinnällä.