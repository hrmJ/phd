---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
fontsize: 11.5pt
geometry: twoside, b5paper, layout=b5paper, left=2cm, right=2cm, top=2cm, bottom=3cm, bindingoffset=0.5cm
indent: true
subparagraph: yes
title: 'Ajanilmausten sijainnit suomessa ja venäjässä: konstruktiotason kontrastiivinen näkökulma'
output:
    html_document2:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
---
<style>
	                    .outerbox{
	                        border: 1px solid black;
	                        display: inline-block;
	                        padding: 5px;
	                    }
	
	                    .cxg{
	                        font-size:80%;
	                    }
	
	                    .outerbox &gt; .statusline{
	                        padding-left:5px;
	                    }
	
	                    .containerbox-outer{
	                        display:inline-block;
	                    }
	                    .containerbox{
	                        display:flex;
	                        padding:0.2em;
	                        align-items:flex-end;
	                    }
	
	                    .innerbox{
	                        display:flex;
	                    }
	
	                    .node {
	                        border: 1px solid black;
	                        margin: 0.2em;
	                        padding: 0.2em;
	                    }
	
	                    .node:last-child {
	                        margin-right: 0em;
	                    }
	
	
	                    .featureline {
	                        display: flex;
	                    }
	
	                    .fleft {
	                        margin-right: 10px;
	                    }
	
	                    .matrixfeat{
	                        border-left:1px solid black;
	                        border-right:1px solid black;
	                        border-radius:5px;
	                        padding:0.3em;
	                    }
	
	
	                    .dashed{
	                        border: 1px dashed black;
	                    }
	
	                    .quote p {
	                        margin-bottom: 1em;
	                    }
	
	                    .quote {
	                        margin-left: 3em;
	                        font-family: serif;
	                        padding: 0.1em 0.1em 0.4em 2em;
	                        background: whitesmoke;
	                        font-size: 90%;
	                        line-height: 130%;
	                        color: #4e4e4e;
	                    }
	
	                    .toc-content {
	                        text-align: justify;
	                        max-width: 700px;
	                    }
	
	                    .toc-content h1,.toc-content h2, .toc-content h3, .toc-content h4{
	                        text-align: left;
	                    }
	
	                    .example li{
	                        font-size:0.8em;
	                    }
	
	                    .example li + li{
	                        margin-top:0.5em;
	                    }
	
	                    /* HACK kable-taulukoille rinta rinnan */
	                    .kable_wrapper td table {
	                        margin-right: 5em;
	                        margin-bottom: 2em;
	                    }
	
	                    </style>



Ajanilmaukset tutkimusaineistossa
=================================


Tutkimusaineistot
-----------------

Edellä luvussa [4](ajanilmaukset_kontrastiivisena.html#kontr)
painotettiin Chestermanin [-@chesterman1998contrastive:
57--58] näkemystä, jonka  mukaan oleellista (funktionaalisessa)
kontrastiivisessa analyysissa on, että hypoteeseja voidaan testata empiirisellä
aineistolla. Kenties tavallisin empiirisen aineiston muoto niin kieli- kuin
käännöstieteissä ovat jo pitkään olleet erilaiset sähköiset tekstikokoelmat eli
korpukset, joiden voidaan jopa väittää mullistaneen sen, miten kieltä tutkitaan
[@mikhailovcooper, 16]. Kuten jo Fried & Östman [-@ostman2004, 24] toteavat 
[tuoreempana katsauksena ks. @gries2013data], korpusaineistojen
käyttö on ollut erityisen keskeisellä sijalla konstruktiokieliopin piirissä
tehtävässä tutkimuksessa.[^usgbsed]

[^usgbsed]: Äärimmilleen vietynä (laajoilla) korpusaineistoilla
tehtävät päätelmät ovat olleet käyttöpohjaisessa kieliopissa 
[ks. esim. @barlow2000usage; @bybee2006usage]. 

Tehtäessä nimenomaan vertailevaa tutkimusta korpusten hankkimiseen ja käyttöön
liittyy tiettyjä erityispiirteitä. Kahta kieltä vertailtaessa on tavallisesti
turvauduttu joko *rinnakkaisiin* (*parallel*) tai *verrannollisiin*
(*comparable*) korpuksiin [@mikhailovcooper, 5]. Edelliset Mikhailov ja Cooper (mp.)
määrittelevät lyhyesti korpuksiksi, jotka sisältävät vähintään kaksi versiota 
samasta[^samat] tekstistä [termistä *parallel corpora* ks. @teubert1996]; jälkimmäisistä taas esimerkiksi Stig Johansson
[-@johansson2007, 9] tiivistää, että ne 
koostuvat erillisistä kahden- tai useammankielisistä alkuperäisteksteistä, jotka vastaavat
toisiaan tiettyjen kriteerien kuten genren tai julkaisuajankohdan osalta.

[^samat]: Kysymys tekstien samuudesta on luonnollisesti mutkikas, ks. [@chesterman1998contrastive,?]

Tässä tutkimuksessa keskitytään käyttämään kahta verrannollista kokonaisuutta, joista toinen --
Internet-tekstien verrannollinen korpus -- on alun perin verrannolliseksi koottu
ja toinen -- lehtitekstien verrannollinen korpus -- sisältää kaksi täysin
riippumatonta aineistolähdettä. Viittaan näihin kaikkiin verrannollisina korpuksina,
joskin verrannollisuus on tässä yhteydessä ymmärrettävä jossain määrin väljästi.

Syynä käytettävien korpusten väljään verrannollisuuteen on aineistojen
tavoitellussa koossa. Luonnollisesti voisi argumentoida, että mitä tiukempien
kriteerien perusteella tutkittavat tekstimassat valitaan, sitä paremmin
verrannollinen tuloksesta tulee -- jos palataan edellisessä luvussa pohdittuun
samanlaisuuden käsitteeseen, voidaan todeta, että jonkin hyvin kapeasti
määritellyn kentän sisällä samanlaisuuden aste on väistämättä korkea
[@chesterman1998contrastive, 11]. Tiukasti rajatun aineiston ongelma on
kuitenkin jo määritelmällisesti itse rajauksessa, sillä kapean rajauksen väistämättömänä
seurauksena mahdollisen tutkittavan tekstiaineksen määrä pienenee.
<!--Korpusten kokoamisessa onkin puhuttu balansoiduista korpuksista jne.--> 
Tämän tutkimuksen kannalta aineiston on kuitenkin oltava laaja ainakin
seuraavista syistä:

1. Vaikka ajanilmaukset sinänsä ovat melko frekventti ilmiö, pyrkimykseni on
   tarkastella mahdollisimman monia (luvussa 1 määritellyillä tavoilla)
   *erilaisia* ilmauksia. 
2. Edellisessä luvussa määritelty syntaktinen rajaus (myönteiset SVO-lauseet) ei
   ole mahdollinen, ellei aineistoa ole paljon.
3. Koska tavoitteena on tehdä kvantitatiivisia päätelmiä, ei riitä, että
   tutkittavana olisi vain joitakin yksittäisiä tapauksia, vaan mitä enemmän
   aineistoa saadaan, sitä varmempia päätelmiä pystytään tekemään ja sitä
   hienojakoisempien ilmiöiden vaikutuksen tarkastelu mahdollistuu (ks. osio 5.1).

Vaatimus aineiston laajuudesta oli myös tärkein yksittäinen tekijä, jonka perusteella
tutkimuksessa päädyttiin nimenomaan verrannollisiin eikä rinnakkaisiin
korpuksiin.[^ksgradu] Lisäksi verrannollisiin aineistoihin keskittyminen 
auttaa välttämään ne ongelmat, joita syntyy, kun käännöksiä
käytetään kieltenvälisten vertailun pohjana -- muun muassa 
von Waldenfelsin [-@waldenfels2012, 279] mainitsemat käännetyn kielen erityispiirteet[^ksuomeksi]
ja ekvivalenssin osittaisuuden (*partial equivalence*).

[^ksuomeksi]: Ilmiöstä suomen osalta katso [@ksuom].
[^ksgradu]:Puhtaasti rinnakkaisteksteihin perustuvana käsittelynä samasta
aiheesta katso kuitenkin [@gradu].

### Verrannollisten korpusten rakenne {#aineistot-vrn}

Tarpeeksi laajoja aineistoja etsittäessä luonnollinen lähestymiskulma on hakea
tekstimassoja Internetistä, jonka sisältämän aineiston määrä on käytännössä
loputon [ks. esim. @kilgarriff2003introduction, @kanerva2014syntactic].
Internet-aineiston käyttöön liittyy kuitenkin monia ongelmia. Internet-korpusten
ongelmia sanakirjojen kokoamisen kannalta tutkineet Tarp ja Fuertes-Oliveira
[-@tarp, 278] listaavat muun muassa seuraavat, myös yleisemmällä tasolla pätevät
vaikeudet:

- Tekstien laatua ja alkuperää ei pystytä kontrolloimaan
- Tekstit voivat olla koneellisesti tuotettuja
- Tekstit voivat olla peräisin henkilöltä, jonka kielitaito on puutteellinen

Lisäksi merkittävä ongelma on tekstien heterogeenisyys: genren määritteleminen
Internet-aineistojen tapauksessa on usein ongelmallista ja esimerkiksi pelkkä
"Internet-teksti" on lajityypin määritteenä miltei absurdi.[^klusteriviite]
Kaikki puutteet ja haitat huomioidenkin Internetin tarjoama laajuus itsessään on
nähdäkseni silti riittävän suuri hyöty, jotta Internet-aineistojen käyttöä
voidaan harkita. Tässä on loppujen lopuksi kyse ilmiöistä, jota
informaatiotutkimuksessa kutsutaan termeillä *precision* ja *recall* --
vakiintuneiden suomennosten puuttuessa käytän tässä nimityksiä *tarkkuus* ja
*kattavuus*. Encyclopedia of Machine Learning -käsikirjan [-@encyclopmachine,
781] määritelmää soveltaen esitän nämä käsitteet seuraavanalaisina suhteina:


\vspace{0.3cm}

\noindent \emph{Kattavuus} = kaikki tutkittavan ilmiön kannalta olennaiset esiintymät, jotka
tutkija onnistuu erottamaan aineistosta / kaikki tutkittavan ilmiön kannalta
olennaiset esiintymät, jotka aineisto sisältää

\vspace{0.3cm}

\noindent \emph{Tarkkuus} = kaikki tutkittavan ilmiön kannalta olennaiset esiintymät, jotka
tutkija onnistuu erottamaan aineistosta / kaikki esiintymät, jotka tutkija
erottaa aineistosta

\vspace{0.3cm}

[^klusteriviite]: Internet-korpusten genreistys on kuitenkin mahdollista.
Mielenkiintoisena lähestymistapana ks. esim. Laippala ja kumppanit x.

Voidaan argumentoida, että tiukasti rajattu, genreltään ja ajankohdaltaan hyvin
samanlaisia tekstejä sisältävä verrannollinen kokonaisuus saavuttaisi hyvän
tarkkuuden, mutta auttamattomasti liian pienen kattavuuden. Väljempi rajaus
puolestaan varmistaa hyvän kattavuuden: mukana on paljon epämääräistä ainesta
(karkeammin ilmaistuna "roskaa" / engl. *noise*), mutta toisaalta tarpeeksi suuren
koon voi nähdä aiheuttavan sen, että marginaalisten tekstien vaikutus
kokonaiskuvaan jää pieneksi. Tutkimukseen valittiin kaksi erityyppistä
verrannollista kokonaisuutta -- Internet-aineistojen lisäksi lehtitekstit --
juuri sen takia, että tällöin voidaan selkeämmin erottaa tapaukset, joissa on
kyse ennemmin Internet-aineiston ongelmallisuuden aiheuttamasta anomaliasta kuin
suomeen tai venäjään todella liittyvästä piirteestä tai ominaisuudesta.

Tutkimukseen valituissa Internet-aineistoissa on se hyvä puoli, että ne on alun
perin koottu vertailevaa käyttöä varten. Valitut Internet-aineistot ovat osa
Vladimir Benkon [-@benko2014] kokoamaa Aranea-projektia[^araneaurl], joka kattaa noin
kaksikymmentä erikielistä korpusta, jotka tekijöiden mukaan ovat verrannollisia
seuraavista kolmesta syystä (mts. 247):

- Korpukset on kerätty Internetistä suurin piirtein samaan aikaan
- Korpuksia koottaessa on pyritty siihen, että kukin korpus sisältäisi saman
  sekoituksen erilaisia Internetissä tavattavia tekstityyppejä, genrejä ja rekistereitä
- Korpukset ovat samankokoisia

Toinen Benkon mainitsema verrannollisuuden aspekti vaatii selvennystä:
todellisuudessa Aranea-korpusten tiedonkeruuprosessiin ei varsinaisesti ole
kuulunut vaihetta, jossa valittujen tekstien genrejä olisi pyritty jollakin
tavalla lajittelemaan tai karsimaan. Tekstimassat Aranea-korpuksiin on kerätty
hyödyntämällä BootCAT-nimistä sovellusta, joka toimii siten, että hakukoneeseen
(tässä tapauksessa Google) syötetään tiettyjen avainsanojen satunnaisia yhdistelmiä, ja
hakutuloksien perusteella saatujen sivujen osoitteet tallennetaan
jatkokäsittelyä varten [@baroni2004bootcat, 2]. Aranea-korpusten lähtökohtana
toimivat avainsanalistat on kunkin korpuksen osalta valittu samoista[^samadiscl]
asiakirjoista: Ihmisoikeuksien julistuksesta, Johanneksen evankeliumin
ensimmäisestä luvusta sekä *polkupyörä*- ja *rakkaus*-sanojen
Wikipedia-artikkeleista [@benko2014, 249]. Korpukset ovat siis samanrakenteisia
tässä hyvin löyhässä mielessä: niiden *siemeninä* on käytetty 
sanoja, jotka ovat peräisin oletettavasti samankaltaisista tekstilähteistä.

[^samadiscl]: Sana *sama* ei tässä kohden toki ole korrekti, vaan tarkkaan
ottaen mainittujen tekstien "samuus" on hyvin erilaista eikä missään nimessä
tarkoita identiteetin yhtenevyyttä.

Tarkkaan ottaen tässä tutkimuksessa hyödynnettävät Internet-korpukset ovat
Araneum Finnicum (Aranea-projektin suomenkielisistä aineistoista koostuva korpus) ja 
Araneum Russicum (Aranea-projektin venäjänkielisistä aineistoista koostuva
korpus). Molemmista korpuksista käytetään Maius-versioita eli suurempia, noin
miljardin saneen kokoisia korpuksia[^maiusminus]. Finnicum- ja
Russicum-korpusten koosta kertovat tilastot on esitetty 
taulukossa 1:


-------------------------------------------------------
&nbsp;            Araneum Finnicum   Araneum Russicum  
----------------- ------------------ ------------------
Saneet            1 200 000 100      1 200 000 258     

sanat             816 931 276        859 319 823       

virkkeet          88 733 910         71 616 173        

kappaleet         33 218 052         29 510 308        

lähdeasiakirjat   2 279 618          1 826 514         
-------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 1}: Suomen ja venäjän Araneum-korpusten koko \normalfont \vspace{0.6cm}

[^araneaurl]: http://ucts.uniba.sk/aranea_about/, viitattu 21.5.2018
[^maiusminus]: Aranea-korpuksista on saatavilla erikseen Minus-versiot (noin
sanetta) ja suuremmat Maius-versiot (noin miljardi sanetta). Maius-versioiden
käyttö edellyttää rekisteröitymistä.



Taulukosta 1 havaitaan konkreettisesti, että vaikka
korpuksia rakennettaessa on pyritty likimain samaan kokoon, tämä ei ole aivan
toteutunut. "Sama koko" on selvästikin tarkoittanut sitä, että sanemäärät ovat
yhteneviä. Eri kielten vertailu sanemäärien avulla on kuitenkin kielten
rakenteellisten erilaisuuksien takia ongelmallista  [vrt. @harme17, 36]. Suomea ja venäjää verrattaessa
on otettava huomioon ainakin [@tobecited]

1. yhdistämisen tavallisuus suomen sananmuodostuskeinona
2. suffiksien ja liitepartikkeleiden suuri määrä suomessa
3. prepositioiden tavallisuus venäjässä

Mainitut kolme seikkaa vaikuttavat siihen, että jos jokin teksti sisältää
esimerkiksi 1000 sanaa venäjässä, on siinä oletettavasti vähemmän informaatiota
(todellista "pituutta") kuin suomenkielisessä 1000-sanaisessa tekstissä.
Yksinkertaistavana indikaattorina erosta voidaan tarkastella edellä mainittuja
ihmisoikeuksien julistusta ja Johanneksen evankeliumia. Ihmisoikeuksien
julistuksen suomenkielinen käännös sisältää 1 246 ja
venäjänkielinen 1 563 sanetta; koko Johanneksen evankeliumin
suomenkielinen käännös sisältää 14 101 ja venäjänkielinen 
15 161 sanetta.[^johstats] 

[^johstats]: Laskettuna suomessa vuoden 1992 käännöksestä ja venäjässä vuoden
2003 Радостная весть -käännöksestä.

Kun otetaan huomioon suomen ja venäjän väliset rakenteelliset erot, on
todettava, että Araneum Finnicum on suurempi korpus kuin Araneum Russicum. Tämä
oletus vahvistuu, kun tutkitaan taulukkoa 1 muiden
ominaisuuksien kuin sanemäärien osalta. Suomenkielinen aineisto sisältää suurin
piirtein yhtä monta sanetta mutta peräti 
17 117 737 virkettä
enemmän kuin venäjänkielinen. Lukuihin ei voi suhtautua täysin varauksetta,
sillä korpukset eivät tarjoa tarkkaa tietoa tavasta, jolla määrät on laskettu,
ja vaikka laskelmat olisivatkin tarkkoja, ei virkemääräkään ole pituusmittana
ongelmaton. 

Tässä tutkimuksessa hyödynnettävien Internet-aineistojen yksi merkittävä etu
tutkittaessa sanajärjestykseen ja informaatiorakenteeseen liittyviä kysymyksiä
on siinä, että ne tarjoavat helpomman pääsyn yksittäisten esimerkkien laajempaan kontekstiin. 
Ensinnäkin, koska aineistot ovat alun perinkin sijainneet vapaasti saatavilla
Internetissä, ne on voitu sisällyttää Araneum-korpuksiin siten, että 
hakutuloksia on mahdollista tarkastella suoraan yhtä virkettä laajempina
konteksteina. Toiseksi hakutulokset sisältävät suoran linkin itse 
alkuperäistekstiin, niin että mikäli teksti on tutkimuksen tekohetkellä
yhä olemassa alkuperäisellä palvelimellaan, on sen tarkasteleminen 
alkuperäisessä julkaisuympäristössä nopeaa ja helppoa.

Internet-aineiston vastapainona käytän aineistoja, joita nimitän
lehtiaineistoiksi, vaikka kyseessä eivät sinänsä ole puhtaasti sanoma- ja
aikakauslehdistä koostuvat tekstikokoelmat, vaan joukkoon mahtuu myös
esimerkiksi Internetissä julkaistuja uutistoimistojen tekstejä. Oleellista
näissä vastapainoksi valituissa korpuksissa on, että on, että siinä missä
Aranea-korpukset edustavat jäsentämätöntä ja heterogeenistä tekstimassaa, tässä
lehtikorpuksina käsitellyt aineistot edustavat ammattimaisesti tuotettuja
tekstejä, joiden alkuperä ja lajityyppi ovat hyvin tiedossa [Internet-korpuksen ja
perinteisemmän korpuksen eroista venäjän tapauksessa ks. @kutuzov2015comparing].

Suomenkielinen lehtitekstiaineisto koostettiin kahdesta Kielipankin
(www.kielipankki.fi) aineistokokonaisuuksiin kuuluvasta korpuksesta: Suomen
kielen tekstikokoelmasta (http://urn.fi/urn:nbn:fi:lb-201403268; vastedes: SKT) sekä
Kansalliskirjaston sanoma- ja aikakauslehtikokoelmasta
(http://urn.fi/urn:nbn:fi:lb-201405276; vastedes: KLK).[^viitatutkp]
KLK-korpus kattaa materiaalia aina 1770-luvulta asti, mutta koska tavoitteenani
ei ole tehdä diakronisen tason havaintoja, korpuksesta erotettiin käyttöön
ainoastaan vuonna 1990 ilmestyneet ja sitä tuoreemmat aineistot. KLK:sta valittu
otos koostuu pääasiassa Länsi-Savo-lehden arkistoista[^klktark]; SKT:n
tekstilähteet ovat moninaisemmat, joskin siitä on rajattu pois eräät koko
SKT:hen kuuluvat kirja-aineistot, niin että tässä käytetty SKT koostuu
sanomalehdistä (mm. Helsingin sanomat, Aamulehti, Turun sanomat)
ja aikakauslehdistä (mm. Tekniikan maailma, Suomen kuvalehti).[^skttark]

[^klktark]: Tarkka listaus KLK-aineiston lehdistä saatavissa
https://www.kielipankki.fi/wp-content/uploads/klk-lehdet-fi.pdf (tarkistettu
25.4.2017)

[^skttark]: Tarkka listaus SKT-aineiston lehdistä saatavissa
 https://kitwiki.csc.fi/twiki/bin/view/FinCLARIN/KielipankkiAineistotFtc (tarkistettu 25.4.2017)

[^viitatutkp]: URL-osoitteet tarkistettu 25.4.2017

Venäjänkielinen lehtitekstiaineisto on peräisin venäjän kansalliskorpukseen
(www.ruscorpora.ru) kuuluvasta lehdistökorpuksesta. Lehdistökorpuksen kuvaus on
saatavilla osoitteessa http://ruscorpora.ru/corpora-structure.html (tarkistettu
25.4.2017), jossa todetaan, että korpus koostuu 2000-luvulla julkaistuista
teksteistä -- paitsi painetuista lehdistä kuten Izvestija, Sovetski sport ja
Komsomolskaja pravda, myös uutistoimistojen, kuten RIA novosti, tarjoamista
sähköisistä materiaaleista. Kansalliskorpuksen lehdistökorpuksen aineisto
otettiin kokonaisuudessaan mukaan tutkimukseen.

Viittaan jatkossa suomen- ja venäjänkielisiin lehdistökorpuksiin lyhenteillä
FiPress ja RuPress. Kuten annetuista kuvauksista käy ilmi, FiPress koostuu
vanhemmasta materiaalista kuin RuPress: edellinen kattaa pääosin 1990-luvulla,
jälkimmäinen taas 2000-luvulla julkaistuja tekstejä. Koska tutkimuskohteena ovat
syntaksin eivätkä esimerkiksi sanaston tason ilmiöt, en näe ajankohtaeroa
erityisen haitallisena aineistojen vertailukelpoisuuden kannalta. Myös
lehdistökorpuksista on kuitenkin todettava, että suomenkielinen aineisto on
venäjänkielistä laajempi: Siinä missä RuPress-korpus kattaa 16 669 748 virkettä
ja 228 521 421 sanetta, kattaa FiPress-korpus 27 622 211 virkettä ja noin 294
460 000 sanetta.[^kokoinfot]

[^kokoinfot]: Informaatio korpusten koista on saatu suomen osalta
Korp-käyttöliittymän kautta (korp.csc.fi) ja venäjän osalta lehdistökorpuksen
hakutulossivulta. (tarkistettu 25.4.2017)

Lehtiaineistojen huono puoli verrattuna Internet-aineistoihin on siinä, että
laajemman kontekstin saaminen esimerkkeihin on usein huomattavasti vaikeampaa,
sillä Lehdistöaineistojen käyttöoikeudet eivät mahdollista laajempien
kontekstien tarkastelua suoraan korpuskäyttöliittymien kautta. Pääosin melko
tuoreista julkaisuista koostuva venäjänkielinen alkuperäisaineisto on tästä
huolimatta monissa tapauksissa saatavilla digitaalisesti lehtien
Internet-sivuilta, mutta koska suomenkielinen lehtikorpus koostuu uusimmillaankin
vuonna 2000 julkaistusta materiaalista, on laajempien kontekstien saamiseksi
turvauduttava lähinnä kirjastojen mikrofilmikokoelmiin.

Lehtiteksteistä on vielä huomautettava, että aivan samaan tapaan kuin 
käännösten on esitetty edustavan omaa kielimuotoaan, käännöskieltä (*translationese*),
myös lehtitekstit voi nähdä lehdistökielen (*journalese*) edustajina. Gries [-@gries2009, 7]
korostaa tämän seikan huomioimisen tärkeyttä: on tiedostettava, että
lehtitekstejä tutkimalla saadaan informaatiota ennen kaikkea siitä melko
spesifistä rekisteristä ja tyylistä, jota ne edustavat. 
Tutkimuksen Internet-aineistojen roolina on kompensoida -- siinä määrin kuin ne itse
eivät ole lehdistötekstejä -- tätä erityispiirrettä. 


### Tutkittavien ajanilmaustapausten erottaminen verrannollisista korpuksista

Edellä kuvattujen neljän korpuksen rooli tutkimuksen kannalta on ollut toimia
eräänlaisena työstämättömänä raaka-aineena -- lähtökohtana, jonka pohjalta
varsinainen tutkimusaineisto on jalostettu. Jalostamisella tarkoitan tässä
yhteydessä karkeasti ottaen sitä, että korpuksista erotettiin tutkittavaksi
sellaiset ajanilmauksen sisältävät virkkeet, jotka noudattavat osiossa [4.2](ajanilmaukset_kontrastiivisena.html#taks4)
määriteltyä syntaktista rajausta. Tarkoituksena ei ollut tutkia kaikkia
mahdollisia ajanilmauksia, vaan ennemminkin valita laaja joukko erilaisia
ajanilmausryhmiä mahdollisimman hyvin edustavia yksittäisiä
esimerkki-ilmauksia. Käsiteltävien ajanilmausten laaja-alaisuus pyrittiin
takaamaan valitsemalla tarkasteltavat esimerkki-ilmaukset siten, että ne
edustaisivat mahdollisimman monia eri haaroja luvussa
[2.2](ajan_ilmaisemisen_monet_muodot.html#ajanilmausten-kolme-taksonomiaa) esitetyistä taksonomioista.

Esimerkki-ilmauksia valittaessa ensimmäinen lähtökohta olivat osiossa [2.2.2](ajan_ilmaisemisen_monet_muodot.html#taksn2)
määritellyt semanttiset funktiot, niin että mukaan pyrittiin ottamaan niin
L-funktiota, E-funktiota kuin F-funktiota edustavia ilmauksia. Edelleen eri
semanttisten funktioiden sisältä pyrittiin poimimaan kunkin funktion eri
alalajeja edustavia ilmauksia, niin että mukaan tulisi paitsi *simultaanista*
(kuten *viime viikolla*), myös esimerkiksi *sekventiaalista* (kuten *sodan
jälkeen*) L-funktiota edustavia tapauksia. Lisäksi esimerkki-ilmausten
valinnassa pyrittiin huomioimaan osiossa [2.2.1](ajan_ilmaisemisen_monet_muodot.html#taksn1) mainitut eri muodostustavat, niin
että aineisto käsittäisi paitsi kalendaarisia (kuten *maanantaina*), myös
adverbisiä (*usein*) ja aikaa indefiniittisesti kvantifioivia (*vähän aikaa*)
ilmauksia. Valintaprosessin lopputuloksena saatiin 
50 *aineistoryhmää*,
jotka on tarkemmin esitelty alla osioissa [5.2.3.1](ajanilmaukset_tutkimusaineistossa.html#l1a-l1b-l1c)--[5.2.6.2](ajanilmaukset_tutkimusaineistossa.html#lm1).
Osa valituista aineistoryhmistä
koostuu vain yhdestä yksittäisestä ilmauksesta: esimerkiksi ryhmään L6b
kuuluvat suomen ilmaus *sodan jälkeen* sekä venäjän ilmaus *после войны*.
Jotkin ryhmistä taas kattavat useampia samankaltaisia ilmauksia, kuten ryhmä
F1a, joka koostuu suomen ilmauksista *joka päivä*, *joka viikko*, *joka
kuukausi* ja *joka vuosi* sekä venäjän ilmauksista *каждый день*, *каждую
неделю*, *каждый месяц*  ja *каждый год*.

#### Ensimmäinen vaihe

Varsinaisen tutkimusaineiston kokoamisen **ensimmäinen vaihe** oli suorittaa
jokaisen aineistoryhmän osalta konkordanssihaut [ks. esim. @mikhailovcooper,
48] kaikista neljästä tutkimuskorpuksesta. Tämä vaihe oli toisten
aineistoryhmien kohdalla -- erityisesti merkitykseltään yksiselitteisten
adverbien kuten *tavallisesti*/*обычно* -- yksinkertainen, toisten kohdalla --
esimerkiksi ryhmä E1a eli ilmaukset tyyppiä *kaksi tuntia* tai *kolme vuotta*
-- taas monimutkaisempi. Konkordanssihaut muodostettiin korpuskäyttöliittymän
niin salliessa hyödyntämällä erityisiä kyselykieliä -- FiPress-korpuksen
tapauksessa CQP-kieltä [@evert2010ims] ja Araneum-korpusten tapauksessa
CQL-kieltä [@jakubicek2010fast].[^nkrjahuom] Alla on esimerkki CQP-kyselystä,
jota käytettiin L9a-aineistoryhmän suomenkielisten ilmausten konkordanssien
hakemiseksi:

    [word="vuonna" %c]
    [word="199."]
    [msd != ".*AgPcp.*" 
        & msd != ".*CASE_(G|I|A|P|E|T|K).*PCP_PrfPrc.*" 
        & msd != ".*PCP_PrsPrc.*"]

Ajatuksena tässä kuvatun kaltaisissa kyselyissä on, että tulosten joukosta
suodatetaan pois mahdollisimman suuri määrä tapauksia, joita ei haluta
tarkastella. Yllä olevassa esimerkissä kolmas hakasulkein erotettu kokonaisuus
pyrkii karsimaan tuloksista pois ajanilmausta seuraavat partisiippimuodot, jotta
esimerkiksi sellaiset tapaukset kuin *vuonna 1994 perustettu yhtiö* eivät
sisältyisi hakutuloksiin. 

[^nkrjahuom]: Venäjän kansalliskorpus ei tue kyselykieliä, mutta tarjoaa
graafisen käyttöliittymän, jonka avulla päästään lähes samoihin tuloksiin. Myös
Aranea-korpukset ja Korp-käyttöliittymä sisältävät graafiset vaihtoehdot
kyselyjen tekemiselle.


<!--


TODO: MAINITSE 20000 kappaleen rajoituksesta.

-->

#### Toinen vaihe

Vaikka jo konkordanssihaun yhteydessä pyrittiin kohtalaiseen hakutarkkuuteen,
varsinainen tarkempi suodattaminen tapahtui tutkimuksen **toisessa vaiheessa**,
jossa saatujen konkordanssilistojen perusteella muodostettiin ikään kuin uudet
neljä korpusta. Nämä neljä jalostettua korpusta -- joita kutsun myös myös tämän
vaiheen jälkeen nimillä FiPress, RuPress, Araneum Finnicum ja Araneum Russicum
-- eivät enää käsittäneet satoja miljoonia saneita, vaan ennemminkin kymmeniä
tuhansia konteksteja, joita kaikkia yhdistää se, että ne sisälsivät vähintään
yhden alaluvussa [5.2](ajanilmaukset_tutkimusaineistossa.html#aryhmat) määriteltävän ajanilmauksen. 

Oleellinen seikka tutkimuksen toisessa vaiheessa muodostetuista jalostetuista
korpuksista on se, että jalostettuja korpuksia muodostettaessa kaikki
ensimmäisessä vaiheessa mukaan suodatetut kontekstit annotoitiin koneellisesti.[^annotsel]
Kaikki tekstit sisälsivät alun perinkin vähintään morfologisen annotoinnin --
toisin sanoen teksteihin oli jo alun perin lisätty koneellisesti saatu
informaatio esimerkiksi kunkin nominin suvusta, luvusta ja sijamuodosta. Tutkimuksen toisen vaiheen
korpuksia muodostettaessa kaikki kontekstit kuitenkin annotoitiin uudelleen,
niin että lopputuloksena kaikki suomenkieliset aineistot noudattivat omaa
yhteneväistä annotointikaavaansa ja kaikki venäjänkieliset omaansa.

[^annotsel]: Annotoinnilla viitataan tässä yhteydessä karkeasti ottaen siihen,
että tekstien sisältämiin sanoihin merkitään niiden ominaisuuksia. Tarkemmin
kirjallisissa korpuksissa yleensä käytettävistä erilaisista annotoinneista ks.
[@gries2017linguistic, 383-385].

Morfologisen analyysin lisäksi tutkittaville konteksteille tehtiin myös
syntaktinen analyysi eli aineistot jäsennettiin dependenssijäsentimellä. Niin
suomen kuin venäjän tapauksessa hyödynnettiin vapaasti saatavilla olevia
ohjelmistoja, suomenkielisen aineistojen osalta Turun yliopistossa kehitettyä
jäsennintä [@haverinen2013tdt][^fdeplink]  ja venäjänkielisen aineistojen osalta
Serge Sharoffin ja Joakim Nivren MALT-ohjelmistoa (http://maltparser.org)
hyödyntävää jäsennintä [@sharoffnivre]. Koska dependenssijäsentimet eivät voi
toimia ilman morfologista analyysiä, molempiin jäsentimiin sisältyvät 
sanojen lemmatisoinnista ja morfologisesta annotoinnista vastaavat komponentit,
joita luonnollisesti myös hyödynsin -- toisin sanoen, vaikka kaikki tekstit oli
jo alkuperäisissä korpuksissa morfologisesti annotoitu, tutkimuksen toiseen vaiheeseen 
otetut kontekstit annotoitiin uudestaan niillä työkaluilla, joita käytetyt
dependenssijäsentimet hyödyntävät.

[^fdeplink]: Jäsentimen lataaminen ja asennusohjeet:
http://turkunlp.github.io/Finnish-dep-parser/ (tarkistettu 26.4.2017)

Kahden kielen vertailu koneellisesti annotoitujen aineistojen perusteella on
lähtökohtaisesti ongelmallista, sillä eri jäsentimet toimivat usein jossain määrin eri
periaatteiden mukaisesti ja tuottavat myös eri käsitteistöön nojaavia
lopputuloksia. Yhtenä kiinnostavana ratkaisuna ongelmaan voidaan nähdä Universal
Dependencies -projekti (http://universaldependencies.org)[^udtark], jonka tarkoituksena
on kehittää yli kielirajojen sovellettavia annotaatiomalleja: standardoidut
merkinnät sanaluokasta, morfologisista ominaisuuksista, syntaktisista suhteista
ja periaatteista, joiden mukaan syntaktisia rakenteita analysoidaan
[@luotolahti2015towards, 211]. Tutkimusaineistoa kerättäessä suomi oli yksi
projektissa täysipainoisesti mukana olevista kielistä, mutta venäjään
sovellettavia työkaluja ei vielä ollut saatavilla, minkä vuoksi 
projektin tuloksia ei varsinaisesti ole hyödynnetty tässä tutkimuksessa. On
kuitenkin todettava, että tässä käytetty suomen kielen jäsentimen
annotaatiomalli noudattaa Universal dependecies 1.0 -standardia. Lause @ee_depex
antaa esimerkin siitä, miltä suomen kielen jäsentimen tuottama annotointi
näyttää. Lauseessa @ee_depex_ru  puolestaan on esimerkki venäjän jäsentimen
tuottamasta lopputuloksesta.

(@ee_depex) 

\scriptsize

\begin{dependency}
\begin{deptext}
Venäjä \& aikoo \& huomenna \& pyytää \& valuuttarahastolta \& 4 \& miljardin \& dollarin \& lainaa.  \\
\end{deptext}

\depedge{4}{1}{nsubj}
\depedge{4}{2}{aux}
\depedge{4}{3}{advmod}
\depedge{4}{5}{nmod}
\depedge{7}{6}{compound}
\depedge{8}{7}{nummod}
\depedge{9}{8}{nmod:poss}
\depedge{4}{9}{dobj}
\end{dependency}


\normalsize



(@ee_depex_ru) 

\scriptsize

\begin{dependency}
\begin{deptext}
Я \& завтра \& собрался \& подводить \& очередные \& промежуточные \& итоги \& конкурса.  \\
\end{deptext}

\depedge{3}{1}{предик}
\depedge{3}{2}{обст}
\depedge{3}{4}{1-компл}
\depedge{7}{5}{опред}
\depedge{7}{6}{опред}
\depedge{4}{7}{1-компл}
\depedge{7}{8}{квазиагент}
\end{dependency}




\normalsize

[^udtark]: Tarkistettu 26.4.2017

#### Kolmas vaihe

Koneellista syntaksin analyysia hyödynnettiin siten, että sen avulla
tutkimuksen **kolmanteen vaiheeseen** suodatettiin mukaan ainoastaan lauseet,
jotka täyttivät seuraavat ehdot (vrt. osio [4.2](ajanilmaukset_kontrastiivisena.html#taks4)):

- Ehto 1: Lauseessa on oltava (nominatiivimuotoinen) subjekti, finiittiverbi ja
  objekti
- Ehto 2: Subjektin, verbin ja  objektin keskinäinen järjestys on oltava
  SVO.
- Ehto 3: Ajanilmauksen on esiinnyttävä itsenäisenä, suoraan
  finiittiverbistä riippuvana lauseen elementtinä. Lisäksi on
  pystyttävä toteamaan, että myös subjekti ja objekti ovat kyseisen verbin dependenttejä.


Ehdon 2 tarkastelu koneellisen analyysin perusteella oli melko suoraviivaista:
kun lauseesta oli eroteltu subjekti, objekti, verbi ja ajanilmaus ja varmistettu
että subjekti, objekti ja ajanilmaus ovat verbin dependenttejä, voitiin niiden
järjestys määrittää yksinkertaisesti vertaamalla jäsentimen kullekin sanalle
antamia järjestysnumeroita (esimerkissä @ee_depex sanalla *Venäjä* on
järjestysnumero 1, sanalla *aikoo* numero 2 ja niin edelleen). 

Ehtoihin 1 ja 3 liittyy enemmän hankaluuksia, joista
ensimmäinen koskee lauseen objektin tunnistamista. Kuten esimerkeistä @ee_depex
ja @ee_depex_ru havaitaan, jäsentimet merkitsevät jokaiselle analysoitavan
virkkeen sanalle *dependenssiroolin*. Subjektin määritteleminen tällaisen
dependenssiroolin perusteella on yksinkertaista: suomen jäsennin merkitsee
nominatiivisubjekteille dependenssiroolin *nsubj*, venäjänkielinen puolestaan
*предик*. Esimerkissä @ee_depex siis subjektiksi saadaan siis substantiivi *Venäjä*,
esimerkissä @ee_depex_ru pronomini *я*.

Objektin määritteleminen on kuitenkin yksi niistä seikoista, jonka osalta
käytetyt jäsentimet eroavat toisistaan. Suomen jäsennin sisältää koko lailla
selkeän dependenssiroolin *dobj*, mutta venäjän jäsentimellä vastaavaa
kategoriaa ei ole. Sen sijaan venäjän jäsennin erottaa viisi eri tasoista
luokkaa erilaisille verbin (tai esimerkiksi substantiivin)
täydennyksille:[^termhuomtayd] *1-kompl*, *2-kompl*, *3-kompl*, *4-kompl* ja
*5-kompl* [tämän merkintätavan juurista ks. @Melchuk1988]. Tästä erosta johtuen
olen tarkoituksella ymmärtänyt objektin käsitteen venäjän osalta jossain
määrin suomea laajemmin. Venäjänkielisessä aineistossa "objekteiksi" on luettu
kaikki sellaiset *1-kompl*-dependenssiroolin saavat sanat jotka

a)  eivät ole jäsentimen analyysin mukaan adpositioita, verbejä, konjuktioita,
  partikkeleita tai adverbejä
b) riippuvat (eräin poikkeuksin) suoraan samasta finiittiverbistä kuin ajanilmaus ja subjektikin

[^termhuomtayd]: Tähän huomio terminologiasta: täydennys / argumentti / yms.

Tämä tarkoittaa, että mukaan otettiin paitsi esimerkissä @ee_depex_ru *1-kompl*-roolin saanut 
puhtaan akkusatiivimuotoinen[^nomkalt] sana *итоги*, myös esimerkin @ee_pokupkatovara *покупкой*:


(@ee_pokupkatovara)


\scriptsize
\begin{dependency}
\begin{deptext}
Я \& давно \& занимался \& покупкой \& различного \& товара \& в \& заграничных \& магазинах \& через \& интернет \\
\end{deptext}

\depedge{3}{1}{предик}
\depedge{3}{2}{обст}
\depedge{3}{4}{1-компл}
\depedge{6}{5}{опред}
\depedge{4}{6}{1-компл}
\depedge{3}{7}{обст}
\depedge{9}{8}{опред}
\depedge{7}{9}{предл}
\depedge{9}{10}{атриб}
\depedge{10}{11}{предл}
\end{dependency}
\normalsize


On kuitenkin huomattava, että esimerkissä @ee_pokupkatovara myös sana *товара*
on analysoitu roolilla *1-kompl*. Tätä sanaa ei kuitenkin sellaisenaan laskettaisi
objektiksi, koska se ei ole finiittiverbin vaan substantiivin dependentti. 

Kysymys *1-kompl*-merkinnällä annotoidun sanan pääsanasta vaatii vielä
toisenkin tarkennuksen, joka liittyy edellä esitettyyn ehtoon 3.
Esimerkissä @ee_depex_ru sana *итоги* ei nimittäin riipu samasta
finiittiverbistä kuin ajanilmaus *завтра*. Itse asiassa *итоги* ei riipu
finiittiverbistä lainkaan, vaan infiniittimuodosta *подводить*. Näissä
tapauksissa lauseiden suodatussääntöjä tarkennettiin siten, että jos
ajanilmauksen pääverbin dependettinä 

a) ei ole objektia mutta 
b) on infinitiivimuotoinen verbi, jolla puolestaan on objekti (kuten esimerkissä @ee_depex_ru)

infinitiivimuotoisen verbin objekti katsottiin lauseen objektiksi, vaikka sillä
oli eri pääsana kuin ajanilmauksella ja vaikka pääsana oli infiniittinen. Tämä
tarkennus koskee vain venäjänkielisiä lauseita, sillä kuten
esimerkistä @ee_depex havaitaan, suomen jäsennin analysoi joka tapauksessa niin
subjektin, objektin kuin ajanilmauksen riippuvaksi samasta verbistä. Suomen
jäsentimeen liittyy kuitenkin toinen tähdennys: kuten esimerkistä @ee_depex
nähdään, apuverbitapauksissa ajanilmauksen, subjektin ja objektin *kaikkien*
pääverbiksi on analysoitu infinitiivimuoto, jonka dependentiksi myös lauseen
finiittiverbi on luettu. Näissä tapauksissa toimittiin siten, että mikäli     

a) ajanilmaus on infinitiivimuotoisen verbin dependentti ja
b) saman infinitiivimuotoisen verbin dependenttinä on finiittiverbi

lause otettiin mukaan tutkimukseen.

Ehdosta 3 on vielä todettava, että sen avulla pyrittiin karsimaan pois
tapaukset, joissa ajanilmaus on esimerkiksi partisiippimuodon dependentti,
kuten lauseessa *Eilen järjestetyn tapahtuman osallistujat olivat kotoisin
kymmenestä eri maasta* (vrt. myös edellä esitelty CQL-lauseke, jolla pyrittiin
samaan päämäärään).

[^nomkalt]: joskin tässä tapauksessa akkusatiivi on nominatiivin kaltainen

<!--

TODO: githubiin python-pohjaiset suodattamismenetelmät.

-->

#### Erottamisprosessin tarkkuuden arviointia

<!-- PARSERITESTI -->

Edellä lueteltujen kolmen vaiheen tarkoituksena oli tuottaa
mahdollisimman vertailukelpoiset suomen- ja venäjänkieliset aineistot
sellaisista myönteisistä SVO-lauseista, joissa ajanilmaus on pääverbin
suora dependentti. Aineiston lopullisena muotona ovat seuraavassa
luvussa tarkemmin käsiteltävät  49
aineistoryhmää, jotka yhteenlaskettuna kattavat 
86607 suomen- 
ja 73194 venäjänkielistä tapausta.

Tutkimusaineistoa kerätessä pyrittiin tuottamaan niin suuri määrä
esimerkkitapauksia, että niiden käsittely automaattisen jäsennyksen keinoin ja
hyödyntäminen tilastollisissa päätelmissä olisi mielekästä. On kuitenkin
kysyttävä, kuinka suuressa osassa tapauksia lopulliseen tutkimusaineistoon
päätynyt materiaali on tullut oikein analysoiduksi -- toisin sanoen, kuinka
suurella todennäköisyydellä esimerkiksi S3-sijaintiin määritelty
L1a-aineistoryhmän tapaus todella on S3-sijoittunut L1a-aineistoryhmän tapaus.

Tutkimusaineiston keräämisprosessi oli altis ennen muuta kahdenlaisaisille
annotointivirheille. Ensinnäkin, on mahdollista, että aineistoryhmien sisältö
poikkeaa halutusta esimerkiksi siten, että aineistoon valikoitunut ajanilmaus
on ominaisuuksiltaan vääränlainen tai ei lainkaan ole esimerkiksi adverbiaali.
Esimerkiksi etsittäessä suomen duratiiviseen ekstensioon viittaavaa ilmausta
*tunnin* (kuten lauseessa *Aatos pelasi tunnin jalkapalloa*) aineistoon on
voinut päätyä lause *Opettaja kannatti tunnin siirtämistä seuraavaan päivään*,
jossa sananmuotoa *tunnin* on käytetty konkreettiseen referenttiin viittaavana
*kannattaa*-verbin objektina eikä niinkään ajanilmauksena. Huomattakoon
kuitenkin, että etenkin monilla adverbisillä ajanilmauksilla on sekä
temporaalisia että ei--temporaalisia merkityksiä, joiden automaattinen erottelu
ei tavallisesti ole mahdollista -- esimerkiksi *silloin*-sanaa käytetään usein
ilmaisemaan ennemmin kausaalisuutta kuin ajankohtaa (vrt. esimerkit osiossa
[8.2.2.3](keskisijainti.html#l7l8-erot-kesk)). Tällaisia ei--ajallisia merkityksiä ei ole pyritty
tietoisesti karsimaan pois eikä niiden ilmenemistä aineistossa katsota
virheeksi.

Toinen potentiaalinen virhelähde on, että automaattiseen analyysiin käytetyt
jäsentimet ovat analysoineet jonkin lauseen syntaktisen rakenteen väärin.
Tällaiset virheet ovat odotuksenmukaisia, sillä jäsenninten tarkkuuden ei
oletetakaan olevan sataa prosenttia. Jäsenninten tarkkuuden mittaamiseen
voidaan käyttää muun muassa LAS-mittaria (*labeled attachment score*), joka
tarkoittaa karkeasti ottaen oikein annotoitujen[^oikan] saneiden suhdetta
kaikkiin saneisiin. Tällä mittarilla mitattuna suomenkielisen aineiston
analysointiin käytetty jäsennin on (tässä käytettyä varhaisemmalla versiolla)
saavuttanut tarkkuuden  81 % [@haverinen2013tdt, 529] ja venäjän jäsennin
tarkkuuden 82,3% [@sharoffnivre, 8]. On kuitenkin otettava
huomioon, että tähän tutkimukseen kerättävän datan kannalta jäsentimiltä
vaaditaan vain tietyntyyppisten, vaiheessa 1 määriteltyjen
ajanilmauksen sisältävien lauseiden oikein tunnistamista, ja niitä käytetään
suodattamaan vain SVO-lauseita, mikä saattaa vaikuttaa positiivisesti 
tarkkuuteen. Toisin sanoen, LAS-mittarista poiketen tämän tutkimuksen kannalta onnistunut jäsennys
ei vaadi jokaisen, vaan ainoastaan tiettyjen kriittisten (predikaattiverbi,
ajanilmaus, subjekti, objekti) saneen onnistunutta analysointia. Lisäksi
tavoitteena on ennemminkin suuri tarkkuus kuin suuri saanti: oleellista on, että
mukaan tulevat tapaukset ovat mahdollisimman pitkälle oikein analysoituja. Se,
että tutkimuksen ulkopuolelle jää tapauksia toki heikentää saantia, mutta toisaalta
edellä esitetyt luvut osoittavat, että aineistoa on silti runsaasti.

[^oikan]: Oikein annotoidulla tarkoitetaan sanetta, jonka dependenssirooli ja
pääsana on annotoitu oikein.

Tutkimukseen päätyneessä aineistossa oletettiin siis olevan jonkin verran
virheitä, mahdollisesti kuitenkin vähemmän kuin mitä syntaktisten jäsenninten
LAS-lukemat antaisivat ymmärtää. Todellisen tarkkuuden arvioimiseksi valmiista
tutkimusaineistosta otettiin yhteensä
 1000 
 (500 kpl / kieli)
lausetta kattanut satunnaisotanta. Jokainen satunnaisotannan esimerkki käytiin
läpi siten, että kustakin esimerkistä arvioitiin, kelpasiko se kuvaamansa
kategorian edustajaksi -- toisin sanoen, voitiinko esimerkiksi S3-sijaintiin
määriteltyä L1a-aineistoryhmän tapausta todella pitää S3-sijoittuneena L1a-tapauksena.
Analyysin lopputulos on esitetty taulukossa \@ref(tab: acctest).

<!--

TODO: kyssärit

-->


|       |oikein |väärin |tarkkuus |
|:------|:------|:------|:--------|
|suomi  |454    |46     |90,8 %   |
|venäjä |453    |47     |90,6 %   |

Taulukosta \@ref(tab: acctest) käy ilmi, että niin suomen kuin venäjänkin
osalta oikein analysoitujen tapausten osuus on melko korkea, eikä kielten
välillä itse asiassa ole käytännössä minkäänlaista eroa. Tutkimusaineistoon
liittyy siis jonkin verran epätarkkuutta, mutta epätarkkuuden osuus 
on odotuksenmukainen eikä nähdäkseni nyt käsillä olevan kokoisessa aineistossa
häiritsevä. Epätarkkuus voidaan nähdä kaikkeen empiiriseen tutkimukseen 
liittyvänä mittausvirheenä, jonka olemassaolo erityisesti tässä
tutkimuksessa käytettävässä tilastollisen päättelyn muodossa, bayesiläisissä
menetelmissä (ks. osio [6.1](tilastollinen_malli.html#bayesiläinen-päättely-tilastollisen-analyysin-lähtökohtana)),
on ymmärrettävissä päättelyprosessiin luonnollisesti kuuluvana epävarmuutena.
Aineistossa olevat epätarkkuudet tulevat luvuissa
[7](alkusijainti.html#ajanilmaukset-ja-alkusijainti) -- [9](loppusijainti.html#ajanilmaukset-ja-loppusijainti) 
esille lähinnä käsiteltäessä erityisen harvinaisia kategorioita, joihin
päätyneet tapaukset ovat paikoin selitettävissä jäsentimeen tai ilmauksia
suodattaneeseen hakulausekkeeseen liittyvinä virheinä.


Aineistoryhmät {#aryhmat}
--------------

Tutkimusaineiston perusyksiköksi muodostuivat siis edellä kuvatun prosessin
perusteella aineistoryhmiksi kutsumani ilmausjoukot, joiden tarkoituksena on mahdollisimman
monipuolisesti edustaa suomen- ja venäjänkielisissä teksteissä esiintyviä
ajanilmauksia. Tässä osiossa kukin näistä aineistoryhmistä ja niihin johtaneet
valintakriteerit käydään läpi.

Käytin edellä kokonaisen luvun ajanilmausten luokitteluun kolmesta eri näkökulmasta,
jotta tutkimukseen osattaisiin valita mahdollisimman monia erilaisia
ajanilmauksia ja jotta mahdollisimman moni ajanilmaustyyppi tulisi
tutkimuksessa edustetuksi. Taksonomioissa 1--3 on luonnollisesti pyritty
erittelemään ilmausjoukkoja, jotka ovat edustettuna niin suomessa kuin
venäjässäkin. Kun luokitteluiden pohjalta ryhdyttiin rakentamaan
konkreettisista, korpuksesta haettavissa olevista sanoista  muodostuvia
aineistoryhmiä, törmättiin myös jonkin verran rajoituksiin siinä, minkä ilmausten
käyttäminen on mahdollista. Näitä rajoituksia on selvennetty
alempana kunkin aineistoryhmän kohdalla tarkemmin. Lisäksi osiossa [4.2](ajanilmaukset_kontrastiivisena.html#taks4)
mainitut syntaktiset rajoitukset (muun muassa se, ettei aineistoon valittu lainkaan
kieltolauseita) kaventavat  mahdollisten ilmausten joukkoa.

<!--

Viimaranta-viite edelliseen kpl?

-->

Onkin syytä korostaa, ettei tarkoitukseni ole kuvata kaikkia mahdollisia
ajanilmauksia eikä välttämättä edes edustavaa otosta, vaan ennen
kaikkea vain *suurta joukkoa erilaisia ilmauksia*. Tässä kuvattavien
aineistoryhmien roolina on tuottaa luvussa [6](tilastollinen_malli.html#tilastollinenmalli)
muodostettavaa tilastollista mallia varten riittävä joukko toisistaan poikkeavia
tapauksia, joilla on riittävä määrä sellaisia ominaisuuksia, joiden
avulla suomen ja venäjän välillä havaittavien erojen taustalla olevista
syistä voidaan saada vihjeitä. Edelleen, tarkoitukseni ei ole aukottomasti 
selittää kaikkia suomen ja venäjän välillä mahdollisesti olevia eroja 
ja yhtäläisyyksiä vaan pikemminkin löytää yleisemmän tason suuntaviivat sille,
minkätyyppiset erot ovat tavallisia ja mille osa-alueille erot tavallisesti
sijoittuvat.

### Aineistoryhmien muodostusprosessi

Edellä taksonomiassa 1  (osio [2.2.1](ajan_ilmaisemisen_monet_muodot.html#taksn1), ks. erityisesti kuvio 
) lähdettiin liikkeelle ajanilmausten
jakamisesta (potentiaalisesti) kalendaarisiin ja ei--kalendaarisiin
ilmauksiin. Kalendaaristen ilmausten sisällä tehtiin jako ensinnäkin
positionaalisuuden mukaan, niin että aineistosta eroteltiin
positionaaliset (kuten *maanantai*) ja ei--positionaaliset ilmaukset
(tarkemmasta määritelmästä ks. osio [2.2.1.1](ajan_ilmaisemisen_monet_muodot.html#pot-kal))). Toiseksi
kalendaariset ilmaukset jaettiin alkuperän mukaan, jolloin saatiin
neljä eri joukkoa: luonnon sykleihin perustuneet
*vuosi*,*kuukausi*,*päivä*, näistä edelleen johdetut kuten *viikko*,
juhlapäiviin perustuvat kuten *joulun jälkeen* sekä puhehetkeä
ympäröivien päivien omat nimitykset *eilen*,*tänään* ja *huomenna*, joihin alla
viitataaan lyhyesti termillä *deiktiset päivännimet*.

Ei--kalendaariset ilmaukset jaoteltiin tarkemmin indefiniittisesti
aikaa kvantifioiviin tapauksiin kuten *vähän aikaa* tai *hiljattain*
(tarkemmasta määritelmästä ks. [2.2.1.2](ajan_ilmaisemisen_monet_muodot.html#ei-kalendaariset-ajanilmaukset)),
adpositiotapauksiin, adverbi- ja pronominitapauksiin (esim. *joskus*)
sekä tämän tutkimuksen ulkopuolelle jääviin kompleksisempiin
rakenteisiin. 

Ensimmäinen askel mahdollisimman kattavan ajanilmausjoukon
muodostamisessa oli saada mukaan ilmauksia kaikista yllä luetelluista
taksonomian 1 mukaisista alajoukoista. Toinen askel oli tutkia
taksonomian 2 mukaisia semanttisia funktioita ja näiden alajoukkoja ja
pyrkiä löytämään edustajia jokaiselle
kuviossa  esitetyn puukuvaimen
haaralle. 

Semanttiset funktiot jaettiin edellä kolmeen pääluokkaan, L-, F- ja
E-funktioihin, joista etenkin L- ja E-funktiot voitiin edelleen jakaa
moniin alaluokkiin. F-funktiosta osiossa
[2.2.2.2](ajan_ilmaisemisen_monet_muodot.html#taajuutta-ilmaiseva-semanttinen-funktio) esitetty alajako erosi
sikäli L- ja E-funktioiden tarkemmista luokista, että luetellut
alaryhmät eivät niinkään ole tarkempia semanttisia ominaisuuksia vaan
ennemmin muodostustapaan liittyviä. Tästä johtuen F-funktion alaluokat
on otettu huomioon aineistoryhmiä muodostettaessa aineiston paremman kattavuuden
ja monimuotoisuuden saavuttamiseksi, mutta tutkimuksen
varsinaisessa tilastollisessa mallissa (ks. luku [6](tilastollinen_malli.html#tilastollinenmalli))
F-funktioita käsitellään yhtenä kategoriana. 

F-funktion mainittuja alaluokkia olivat toisaalta *joka*-sanan,
toisaalta *kerta*-sanan sisältävät ilmaukset, viikonpäiviin liittyvät
ilmaukset tyyppiä *maanantaisin* sekä adverbi-ilmaukset, jotka
jaoteltiin erikseen taajuutta korostaviin (kuten *usein*), harvuutta
korostaviin tai neutraaleihin (*joskus*,*harvoin*) sekä epämääräisiin
(*yleensä*). L-funktio puolestaan jaettiin kehyksisiin (kuten
*viime viikolla*), ja punktuaalisiin simultaanisiin (*kello
kahdelta*), sekventiaalisiin (*joulun jälkeen*),
sekventiaalis--duratiivisiin (*tammikuusta asti*) ja ajallista
etäisyyttä ilmaiseviin (*kuukauden päästä*) tapauksiin. E-funktion
alaluokkia olivat myonteistä (*tunnin*) ja kielteistä (*kahteen
tuntiin*) duratiivista ekstensiota ilmaisevat , varsinaista (*kolmessa
kuukaudessa*) ja kehyksistä (*viime vuoden aikana*) resultatiivista
ekstensiota ilmaisevat  sekä teeliset (*kolmeksi vuodeksi*), aikaa ja
tapaa ilmaisevat (*nopeasti*) ja metatason ekstensiota ilmaisevat
(*kilpailu siirrettiin keskiviikkoon*).

Edellä mainituista luokista tutkimuksen ulkopuolelle jätettiin luvussa
[4.2](ajanilmaukset_kontrastiivisena.html#taks4) esitetyn syntaktisen rajauksen perusteella
kieltoduratiivit sekä toisaalta metatason ekstensio, jota edustavien
ilmausten katsottiin luonteeltaan olevan sikäli muista erottuvia ja 
toisaalta käytöltään rajattuja, ettei niiden mukaan ottamista nähty
mielekkäänä. Kolmen pääfunktion lisäksi mukaan tutkimukseen otettiin
presuppositionaalinen ja likimääräinen funktio, joita ei erikseen
jaettu alaluokkiin. Huomattakoon, että likimääräistä funktiota
edustavat ilmaukset (kuten *viiden maissa*) voidaan toisella tasolla
tulkita myös simultaanisiksi, mutta niitä käsitellään tilastollisessa
mallissa omana luokkanaan. 

Kolmannen taksonomian (osio [2.2.3](ajan_ilmaisemisen_monet_muodot.html#taksn3)) huomioon ottaminen aineistoryhmissä
osoittautui hankalammaksi kuin kahden ensimmäisen ennen kaikkea siitä
syystä, ettei referentiaalisuuden määritteleminen sekä referentiaalisten ja
ei--referentiaalisten tapausten erotteleminen korpusaineistosta haettaviksi
kyselyiksi ole yksiselitteistä. Referentiaalisuutta pyrittiin kuitenkin saamaan aineistossa
edustetuksi toisaalta muodostamalla referentiaalinen/ei--referentiaalinen-pareja
kuten oletettavasti ei--referentiaalinen *viikon* ja oletettavasti
referentiaalinen *koko viikon*, toisaalta ottamalla tutkimukseen
mukaan puhehetkeen viittaavia (deiktisiä) adverbejä kuten *eilen*,
*nyt* ja *äskettäin* ja lisäksi vielä erikseen anaforisesti 
ankkuroivia ilmauksia kuten *siitä asti*. Tilastollisen mallin
kannalta ensiksi mainitun kaltaisilla pareilla ei juuri havaittu selitysvoimaa,
joten referentiaalisuutta tarkasteltiin lopulta deiktisten adverbien
ja anaforisten ilmausten kategorioiden avulla.

Edellä kuvatun taksonomioita 1--3 mallintavan prosessin myötä
tutkimukseen valikoitui lopulta kaikkiaan 49
aineistoryhmää. Viittaamisen helpottamiseksi ryhmille annettiin
lyhyet koodit, jotka muodostettiin liittämällä taksonomian 2
mukaista pääryhmää kuvaavaan isoon kirjaimeen (*L*,*F*, *E*, *LM* tai *P*) 
numero. Ryhmät, joilla on jokin selkeä yhdistävä tekijä (esimerkiksi
*vuosi*- tai *год*-sanan sisältävät L-funktiota edustavat ryhmät L9a ja L9b),
liitetiin saman iso kirjain + numero -koodin alle ja erotettiin
toisistaan pienaakkosin. Ryhmien tarkat kuvaukset on esitetty
seuraavassa osiossa, mutta eri taksonomioimen realisoituminen
konkreettisiksi aineistoryhmiksi on tiivistetty jo tässä kohden
alla olevaan kuvioon  \@ref(fig: groupmeta).

Kuvion \@ref(fig: groupmeta) tarkoitus on mallintaa sitä,
miten hyvin aineistoryhmien valinnassa on onnistuttu poimimaan
tutkimukseen mukaan ilmauksia kaikista taksonomioiden 1--3
alaluokista sekä toisaalta sitä, mitkä osa-alueet  ovat muita selkeämmin
painottuneita. Kuviossa $x$-akselille on kuvattu taksonomian 2
mukaiset semanttiset funktiot siten, kuin ne luvun
[6](tilastollinen_malli.html#tilastollinenmalli) tilastollisessa mallissa on otettu huomioon
ja $y$-akselille puolestaan taksonomian 1 mukaiset alaluokat. Lisäksi
eri ryhmien suhtautuminen kolmanteen taksonomiaan
(referentiaalisuuteen) on esitetty merkinnöillä \*, \*\* ja \*\*\*.
Kuvion $y$-akseli on sävytetty siten, että tummempitaustaisessa yläosassa
ovat kalendaariset, valkotaustaisessa alaosassa taas ei--kalendaariset
ilmaukset. Kunkin ryhmän nimen koko on suhteessa siihen, kuinka monta 
tapausta ryhmään kuuluu.


<!--

TODO: miten kuvan saa hoidettua poikittain?

??

[^vktarkkuus]: Referentiaalisuudesta on vielä todettava, että osiossa
[2.2.3.2](ajan_ilmaisemisen_monet_muodot.html#viittauskohteen-tarkkuus) käsitelty viittauskohteen tarkkuuden kategorian
(tarkat kuten *maanantaina kello 3* ja epämääräiset kuten *hiljattain*)
molemmat vaihtoehdot ovat edustettuina aineistoryhmissä x ja y.
MYÖS anaforisuus (!!).

-->


![plot of chunk groupmeta](figure/groupmeta-1.svg)

Huomattakoon, että aineistoryhmien nimien sijaintiin
koordinaatistossa on kuviossa \@ref(fig:groupmeta) lisätty satunnaista
hajontaa sen tähden, että ne eivät peittäisi toisiaan --
todellisuudessa esimerkiksi L1b-ryhmä (*tänään*/*сегодня*-sanat) ei
sijaitse simultaanisuuden ja sekventiaalisuuden välimaastossa, vaan on
nimenomaan simultaaninen. Kuviossa on pyritty noudattamaan samaa
järjestystä kuin edellä esitetyissä puukuvaimissa, minkä takia
ylimpänä ovat positionaaliset, lähellä alaosaa taas adverbiset
ei--kalendaariset ilmaukset.

Kuviosta \@ref(fig:groupmeta) käy ilmi, että aineisto on jossain määrin
painottunut L-funktiota edustaviin ja ennen kaikkea kalendaarisiin ilmauksiin.
Tämä on sikäli luonnollinen painotus, että kuten jo aiemmin mainittiin,
lokalisoiva funktio vaikuttaisi esimerkiksi pro gradu -työssäni tehtyjen
havaintojen perusteella [@gradulyhyt, xx] selvästi yleisimmältä ja F-funktio
puolestaan harvinaisimmalta. Lisäksi on todettava, että esimerkiksi
positionaalisten ilmausten ja deiktisten päivännimien luonteeseen kuuluu, että
niitä voidaan käyttää käytännössä ainoastaan lokalisoivassa funktiossa
(poislukien ilmaukset tyyppiä *maanantaisin* eli taajuutta ilmaiseva
aineistoryhmä F1b). Toinen huomionarvoinen havainto on, että F-funktioon
liittyy muita funktioita enemmän adverbejä. Lisäksi on todettava
(vrt. osio [2.2.3.1](ajan_ilmaisemisen_monet_muodot.html#referentiaalisuuden-indikaattoreita)), että myös
referentiaalisuus on linkittynyt ennen muuta lokalisoivaan funktioon, tämän
sisällä vielä tarkemmin ottaen simultaaniseen alaluokkaan.

Kuten tässä osiossa on jo useasti korostettu, tutkimusaineiston muodostamisessa
tärkeimpänä päämäränä oli koota sellainen joukko ilmauksia, joka mahdollisimman
monimuotoisesti edustaisi erilaisia luvun [2](ajan_ilmaisemisen_monet_muodot.html#aimm) alussa annettujen
määritelmien mukaisia ajanilmauksia. Monimuotoisuus on pyritty varmistamaan
ottamalla systemaattisesti mukaan ilmauksia niin  monesta taksonomioiden 1--3
eri alaluokasta kuin mahdollista. Päätettäessä niitä konkreettisia sanoja ja
lausekkeita, jotka kunkin aineistoryhmän muodostavat, ei kuitenkaan ole
noudatettu tiettyä eksaktia valintajärjestelmää, vaan valittu oman intuition
perusteella ilmauksia, jotka täyttävät seuraavat kriteerit:

1. *Ilmaukselle on löydettävissä riittävän samanlaiset ja korpuksista haettavissa
   olevat edustajat kummastakin tutkittavasta kielestä*.
2. *Ilmaus on frekvenssiltään riittävän yleinen.* Riittävä frekvenssi on
   määritelty tutkimalla, kuinka paljon ilmauksia aineistoryhmään jää, kun
   sanat on haettu lähdekorpuksista ja suodatettu osiossa
   [5.1.2](ajanilmaukset_tutkimusaineistossa.html#tutkittavien-ajanilmaustapausten-erottaminen-verrannollisista-korpuksista)
   kuvattujen vaiheiden mukaisesti. Tapauksille, joissa lopulliseen tutkimusaineistoon
   valikoituneita ilmauksia on jäänyt alle sata, on pyritty löytämään
   frekventimpiä, samalla tavalla taksonomioihin 1--3 suhtautuvia vaihtoehtoja, mutta
   joissakin tilanteissa (selkeimpänä esimerkkinä suomen *siitä asti*-ilmaukset, joista
   tarkemmin alla osiossa [5.2.3.7](ajanilmaukset_tutkimusaineistossa.html#l7a-l7b-l7c-l7d)) on jouduttu tyytymään
   ainoastaan muutamaan kymmeneen esiintymään. Tutkimukseen ei siis ole pyritty
   järjestelmällisesti valikoimaan kaikkein frekventeimpiä kustakin ajanilmaustyypistä,
   vaan ennen kaikkea saamaan kustakin tyypistä ylipäätään edustaja, toki mieluummin
   tavallinen kuin harvinainen.
3. Valintaprosessissa painotettiin myös sitä, mitkä ilmaukset ovat saaneet huomiota
   aiemmassa tutkimuskirjallisuudessa. Yksi selkeä esimerkki ovat venäjän *теперь*-
   ja *сейчас*-sanat, joita koskevaa kirjallisuutta on tarkemmin lueteltu osiossa
   [8.2.2.3](keskisijainti.html#l7l8-erot-kesk).


Aineistoryhmät on luokiteltu seuraavassa kolmen semanttisten funktioiden yläluokkien
mukaan siten, että osio [5.2.3](ajanilmaukset_tutkimusaineistossa.html#lokalisoiva-funktio) kattaa L-funktiota, osio
[5.2.5](ajanilmaukset_tutkimusaineistossa.html#taajuutta-ilmaiseva-funktio) F-funktiota, osio 
[5.2.4](ajanilmaukset_tutkimusaineistossa.html#ekstensiota-ilmaiseva-funktio) E-funktiota sekä 
osio [5.2.6](ajanilmaukset_tutkimusaineistossa.html#muut-funktiot) muita semanttisia funktioita edustavat tapaukset.


### L-funktiota edustavat aineistoryhmät

Niin kuin kuviosta \@ref(fig:groupmeta) ja osiosta
[2.2.2.1](ajan_ilmaisemisen_monet_muodot.html#lokalisoiva-semanttinen-funktio) on käynyt ilmi, L-funktio 
on semanttisista funktioista yleisin  ja aineistossa 
laajimmin edustettuna: sitä edustaa kaikkiaan
 23 
ryhmää. Nämä valikoitiin siten, että mukaan pyrittiin saamaan 
edustajia toisaalta kustakin lokalisoivan funktion alaluokasta,
toisaalta mahdollisimman monesta taksonomioiden 1 ja 3 kategoriasta.

Jos tarkastellaan taksonomian 1 mukaisia kalendaarisia ilmauksia, voidaan
todeta, että deiktisiä päivännimiä edustavat alla lueteltavista tapauksista
ryhmät L1a--L1c, positionaalisia ilmauksia ryhmät L2a--b ja L4a, suoraan
luonnon sykleihin perustuvia ilmauksia ryhmät L5a, L9a ja L9b ja  näistä edelleen
johdettuja taas ryhmät L3, L4b ja L5b.  Ei--kalendaariset ilmaukset ovat edustettuina siten,
että ryhmät L5c, L7d ja L7b edustavat aikaa indefiniittisesti kvantifioivia, ryhmät
L6a, L6b  ja L7c (kummassakin kielessä) adpositioilmauksia ja ryhmät L8a--c
adverbi- ja pronomini-ilmauksia.

Taksonomian 3 kannalta tarkasteltuna tutkimusta varten rakennetut L-funktiota
edustavat aineistoryhmät jakautuvat niin, että ryhmät L7a--L7c edustavat
anaforisesti ankkuroivia, ryhmät L1a--c, L5a--c ja  L8a--c puhehetkeen
ankkuroivia ja ryhmät L2a--b, L3, L9d muulla tavoin referentiaalisia
ajanilmauksia.

<!--

TODO: deiktisyys viime viikolla ja viime vuonna -tapauksissa??


-->

Ryhmiin liittyvät tarkemmat hakukriteerit sekä kunkin ryhmän kielikokohtaiset
kokonaismäärät on esitetty seuraavissa alaluvuissa.

<!--

Mitä jos toteuttaisi huvikseen pikku hakukoneen aineistoryhmistä jonnekin
serverille?

-->

#### L1a, L1b, L1c


| Ryhmän nimi   | suomi            | venäjä             |
| ------------- | ---------------- | ------------------ |
| L1a           | eilen            | вчера              |
| L1b           | tänään           | сегодня            |
| L1c           | huomenna         | завтра             |

L1-ryhmät edustavat kolmea tavallisinta deiktistä päivännimeä.
Korpuksista etsittävien ilmausten valitseminen L1-ryhmiin oli
suoraviivaista, koska niin suomessa, venäjässä kuin oletettavasti
paljon laajemmallakin tasolla nämä ilmaukset ovat selkesti
rajattavissa yksittäisiin adverbilekseemeihin. Tarkemmalta
semanttiselta funktioltaan kaikki ryhmät voidaan luokitella
simultaanisen funktion edustajiksi. 
Taksonomian 3 kannalta voidaan todeta, että kaikki tässä mainitut
ilmaukset ovat puhehetkeen ankkuroivia referentiaalisia ilmauksia,
mikä on kuviossa \@ref(fig:groupmeta) osoitettu \*\*-merkinnällä.

L1a--L1c-ryhmien ilmaisuja etsittäessä aineistosta pyrittiin hakemaan
ainoastaan itsenäisesti aikaa ilmaisevat tapaukset. Tämä tarkoittaa,
että esimerkiksi ilmaukset *eilen viideltä, eilen aamulla* ja *eilen
kello kaksi* on jätetty korpushakujen ulkopuolelle.

Kunkin ryhmän suhtautuminen taksonomioihin 1--3 on listattu taulukkoon
\@ref(tab:l1meta). Taulukon viimeiset sarakkeet kertovat, kuinka monta suomen-
ja kuinka monta venäjänkielistä tapausta tutkimukseen kaikkiaan päätyi
lauseiden suodattamisen jälkeen (vrt. tutkimusvaiheiden kuvaus osiossa
[5.1.2](ajanilmaukset_tutkimusaineistossa.html#tutkittavien-ajanilmaustapausten-erottaminen-verrannollisista-korpuksista)).


|Ryhmä |Takson.1           |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:------------------|:--------|:--------|----:|----:|:--------------------|
|L1a   |deikt. päivännimet |L/sim    |deikt.   | 3818| 4898|deict.ADV            |
|L1b   |deikt. päivännimet |L/sim    |deikt.   | 2913| 4296|deict.ADV            |
|L1c   |deikt. päivännimet |L/sim    |deikt.   |  702| 1289|deict.ADV            |

<!--

Ehkä vielä jotain juttua

-->

#### L2a, L2b

| Ryhmän nimi | suomi                                 | venäjä                           | 
|-------------| ------------------------------------- | -------------------------------- | 
| L2a         | tiistaina, keskiviikkona, torstaina   | во вторник, в среду, в четверг   | 
| L2b         | maaliskuussa, lokakuussa              | в марте, в октябре               | 

Ryhmät L2a ja L2b valittiin edustamaan kahta selkeimmin positionaalista
ilmausjoukkoa, viikonpäiviä ja kuukausia. L2-ryhmät ovat lähtökohtaisesti
referentiaalisia ilmaisuja, joskin se, miten ne aikajanalle ankkuroidaan,
vaihtelee. Toisaalta esimerkiksi *tiistaina* saattaa kytkeytyä puhe- tai
kirjoitushetkeen, jos puhujan tai kirjoittajan tarkoituksena on viitata tätä
hetkeä lähimpään (joko menneeseen tai tulevaan) tiistaipäivään. Toisaalta
kyseessä voi olla jonkin aiemmassa diskurssissa määritellyn viikon tiistai.

Myös näiden ryhmien tapauksessa hakutuloksista pyrittiin karsimaan pois
epäitsenäiset tapaukset kuten *tiistaina aamulla*. Huomattakoon, että niin kuin
yllä olevasta taulukosta käy ilmi, mukaan ei valittu kaikkia mahdollisia viikonpäiviä
ja kuukausia, vaan ainoastaan yllä luetellut kolme päivää ja kaksi kuukautta. Tämä
rajoitus tehtiin, koska niin viikonpäivä kuin kuukausia edustavia L-funktiotapauksia oli
korpuksissa niin paljon, että osiossa [5.1.2](ajanilmaukset_tutkimusaineistossa.html#tutkittavien-ajanilmaustapausten-erottaminen-verrannollisista-korpuksista)
mainittu 20 000 korpusesiintymän raja rikkoutui nopeasti. Rajaamalla mukaan vain joitakin
viikonpäivä ja kuukausia aineistoryhmistä saatiin suhteessa sopivamman kokoiset
kuin jos mukaan olisi otettu kaikki mahdolliset viikonpäivät tai varsinkaan kuukaudet.
Ryhmien tarkemmat ominaisuudet ja tällä tavoin saadut lopulliset, suodatetut
esiintymämäärät on lueteltu taulukossa \@ref(tab:l2meta).

[^nomhuom]: Lasken tässä nominaalisiksi yhtä lailla suomen pelkästä
sijamuodossa taivutetusta nominista kuin venäjän prepositiosta ja nominista
koostuvat tapaukset erotukseksi adverbisistä tapauksista.


|Ryhmä |Takson.1 |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------|:--------|:--------|----:|----:|:--------------------|
|L2a   |pos      |L/sim    |muu      | 4479| 2766|NP                   |
|L2b   |pos      |L/sim    |muu      | 3388| 1307|NP                   |

#### L3


| Ryhmän nimi   | suomi                                    | venäjä                                         |
| ------------- | ---------------------------------------- | ---------------------------------------------- |
| L3            | kahdelta, kolmelta ... kahdeltatoista    | в два часа, в три часа... в двенадцать часов   |

Ryhmä L3 sisältää tasatuntisiin kellonaikoihin viittaavat ilmaisut. Nämäkin
ovat luonteeltaan simultaanisia, mutta toisin kuin edelliset ryhmät,
ne viittaavat selkeän punktuaalisesti ajalliseen viitepisteeseensä eivätkä kehyksisesti
koko tiettyyn ajanjaksoon. Samalla ilmaukset eivät ankkuroi referenttiään
suhteessa puhehetkeen, vaan ennemminkin suoraan absoluuttiselle aikajanalle.
Kolme pistettä yllä olevassa taulukossa merkitsee sitä, että mukana ovat kaikki
luvut kahden ja kahdentoista väliltä (yksi on jätetty pois sen takia, että
venäjässä kello yhteen viitataan tavallisesti ilman numeroa, pelkästään sanalla
*час*, 'tunti').

<!--

Punktuaalisuus tarkemmin vielä käsittelemättä...
Yleisemmälläkin tasolla ajatus frekvensseistä on vielä ilmaisematta.
JONNEKKIN SUORA TUNNUSTUS, että intuitiolla on ollut osanssa, mutta etten katso sen haittaavan.

-->

Hakutuloksista on karsittu ilmaukset tyyppiä *kahdelta aamulla, kahdelta illalla*.
Tarkemmat tiedot on esitetty taulukossa \@ref(tab:l3meta).


|Ryhmä |Takson.1        |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:---------------|:--------|:--------|----:|----:|:--------------------|
|L3    |edell. johdetut |L/sim    |muu      |  307|  476|NP                   |

#### L4a, L4b


| Ryhmän nimi   | suomi                          | venäjä                                   |
| ------------- | ------------------------------ | ---------------------------------------- |
| L4a           | aamulla, illalla               | утром, вечером                           |
| L4b           | yhtenä aamuna/päivänä/iltana   | однажды утром / вечером / днём           |

Ryhmä L4a on samantapainen kuin L1-ryhmät siinä, että kyseessä ovat
(tavallisesti) puhehetkeen ankkuroidut simultaaniset ilmaukset, jotka lokalisoivat
tapahtumia kehyksisesti tietyn ajanjakson sisään. Lisäksi L4a on L2-ryhmien ohella
yksi tutkimusaineiston positionaalisista aineistoryhmistä, sikäli kuin aamu ja ilta
nähdään päivää jaottelevina ajanyksiköinä.[^apip]
Ryhmä L4b on puolestaan valittu tutkimukseen mahdollisena esimerkkinä ajallisesti
ankkuroimattomasta (ei-referentiaalisesta) ilmauksesta. 
 Ilmausten morfologisesta
rakenteesta on syytä huomauttaa, että venäjän *утром* ja *вечером* on tapana
analysoida leksikaalistuneiksi adverbeiksi, vaikka ne onkin alun perin muodostettu instrumentaalin
sijapäätteellä substantiiveista утро ja вечер [@akademitsheskaja, 398].
Kieltenvälisen vertailun helpottamiseksi tulkitsen tästä huolimatta myös venäjän L4-ilmaukset
nominaalisiksi, mitä puoltaa sekin, että aineistossa on myös selvästi ei--leksikaalistuneita
tapauksia kuten luvussa [9.2.4](loppusijainti.html#deiktiset-adverbit-ja-positionaalisuus) annettu esimerkki
@ee_legkimutrom osoittaa.

[^apip]: Tarkemmin päivän jaottelusta suomessa ja venäjässä ks. @harme17.

Myös ryhmän L4a ilmauksissa on pyritty itsenäisyyteen karsimalla esiintymät
tyyppiä *aamulla viideltä* ja *eilen aamulla*. Ryhmien tarkemmat tiedot on
esitetty taulukossa \@ref(tab:l4meta). Huomattakoon, että L4b:tä ei ole merkitty
taksonomian 1 luokittelussa positionaaliseksi, koska 


|Ryhmä |Takson.1        |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:---------------|:--------|:--------|----:|----:|:--------------------|
|L4a   |pos             |L/sim    |deikt.   | 1395|  515|NP                   |
|L4b   |edell. johdetut |L/sim    |ei       |  352|   48|NP                   |


#### L5a, L5b, L5c {#l5a-l5b}

<!--  

- Epäkohta: suomen monikot! 
- Mahdollinen ratkaisu: poista R:ssä.

TODO: taksonomian 3 merkintä ja morf.rak - merkintä

-->


| Ryhmän nimi   | suomi                | venäjä                            |
|---------------|----------------------|-----------------------------------|
| L5a           | viime vuonna         | в прошлом году                    |
| L5b           | viime viikolla       | на прошлой неделе, в прошлом году |
| L5c           | hiljattain/äskettäin | недавно                           |

Myös L5-ryhmät edustavat simultaanista L-funktiota, ja kaikki voidaan
tarkemmin jaotella kehyksisesti lokalisoiviksi, joskin ryhmän L5a voi
joissain tapauksissa tulkita myös punktuaaliseksi. A- ja b-ryhmien
ajallinen ekstensio on moniin edellisiin ryhmiin verrattuna melko
laaja, c-ryhmän osalta taas poikkeuksellinen epämääräinen. C-ryhmä on
morfologiselta rakenteeltaan ei--kalendaarinen, aikaa
indefiniittisesti kvantifioiva adverbi, muut ovat kalendaarisia ryhmiä
ja morfologiselta rakenteeltaan nominaalisia. Taksonomian 3 kannalta
kaikki ovat referentiaalisia, puhehetkeen ankkuroivia ilmauksia.
Ryhmien yksityiskohdat on tiivistetty taulukkoon \@ref(tab:l5meta)


|Ryhmä |Takson.1        |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:---------------|:--------|:--------|----:|----:|:--------------------|
|L5b   |edell. johdetut |L/sim    |deikt.   | 1007|  831|NP                   |
|L5c   |aikaa kvant.    |L/sim    |deikt.   | 4400| 3216|deict.ADV            |
|L5a   |luonnon sykl.   |L/sim    |muu      | 3701| 2229|NP                   |


#### L6a, L6b

| Ryhmän nimi | suomi                                                   | venäjä                                    |
|-------------| ------------------------------------------------------- | ----------------------------------------- |
| L6a         | kahden,kolmen... päivän/viikon/vuoden kuluttua/päästä   | через/спустя два,три... дней/недель/лет   |
| L6b         | sodan jälkeen                                           | после войны                               |

Ryhmä L6a edustaa tarkemmin jaoteltuna taksonomiassa 2 ajallisen
etäisyyden funktiota, ryhmä L6b puolestaan sekventiaalista funktiota.
Taksonomian 3 kannalta L6a on luonteeltaan referentiaalinen ja se voi
olla ankkuroitu joko puhehetkeen tai ajalliseen topiikkiin (TT). L6b
on myös referentiaalinen, mutta ankkuroituu eri tavalla: suhteessa
objektiivisen aikajanan pisteeseen. Tämäntyyppistä ajanilmausta
pohdittaessa päädyttiin lopulta ilmauksiin *sodan jälkeen* ja *после
войны* sillä perusteella, että niistä oli kummassakin kielessä
mahdollista löytää melko paljon esiintymiä ja toisaalta ilmausten
käytön voidaan olettaa olevan koko lailla samanlaista. Se, mihin
konkreettiseen sotaan kummassakin kielessä kulloinkin viittataan,
luultavasti vaihtelee jonkin verran, mutta oleellista on, että
ilmausta käytettäessä oletetaan kuulijan tai lukijan pystyvän
identifioimaan tämän ajallisen viitepisteen. Taksonomian 1 kannalta L6a-ryhmä on
kalendaarinen, L6b taas ei--kalendaarinen. 

<!--

TÄHÄN kohtaan mukaan joulun jälkeen -aineistoryhmä ja 
juttua sota/joulu-ryhmien samanlaisuudesta sekä siitä,
miksi "ennen" ei oikeastaan ollut vaihtoehto.

Jotain juttua myös L6a ja L6b -suhteesta tai muutoin eroteltava toisistaan..

-->

Ryhmien tarkemmat ominaisuudet ovat
seuraavanlaiset:


|Ryhmä |Takson.1 |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------|:--------|:--------|----:|----:|:--------------------|
|L6a   |adpos    |L/dist   |muu      |  422| 1364|NP                   |
|L6b   |adpos    |L/sekv   |muu      |  741|  155|NP                   |

#### L7a, L7b, L7c, L7d

| Ryhmän nimi | suomi                                                       | venäjä                            |
|-------------| ----------------------------------------------------------- | --------------------------------- |
| L7a         | siitä asti/lähtien/pitäen                                   | с тех пор                         |
| L7b         | siihen aikaan                                               | в то время                        |
| L7c         | silloin                                                     | тогда                             |
| L7d         | sellaisena hetkenä/hetkinä, sellaisella hetkellä/hetkillä   | в такой момент, в такие моменты   |

<!--
varmista, että anaforisuutta sinänsä käsitelty jossain, vaikka ref ei oma muuttujansa
-->

L7-ryhmät sisältävät monta taksonomian 1 perusteella indefiniittisesti aikaa
kvantifioivaksi luokiteltavaa ilmausta. L7a on L-funktioltaan
sekventiaalis-duratiivinen, muut kolme ryhmää simultaanisia. Jos mietitään
simultaanisten ilmausten joukon sisäistä sisäistä jaottelua punktuaalisiin ja
kehyksisiin, ryhmistä L7b, L7c ja L7d vain L7b viittaa selkeästi ajanjaksoon
eikä yhteen pisteeseen. L7c voi puolestaan viitata niin yksittäiseen hetkeen kuin
laajempaankin ajanjaksoon.

L7-ryhmät ovat erityisiä nimenomaan referentiaalisuutensa osalta. Ryhmien
L7a-L7c viitepistettä voi pitää anaforisena, edeltävässä diskurssissa
perustettuna. L7d-ryhmän mukaan ottamisella taas pyrittiin saamaan esimerkkejä
ankkuroimattomista, ei-referentiaalisista ajanilmauksista. Kuten taulukosta 
\@ref(tab:l7meta) havaitaan, L7d-ilmauksia ei lähdekorpuksista löytynyt kovin paljon.
Ryhmä otettiin mukaan edustamaan ei--referentiaalisia simultaanisia 
ilmauksia, jotka ovat jo lähtökohtaisesti harvinaisuus.


<!--

juttua siitä jonnekink, miten ADV-luokka tilastollisessa mallissa edustaa
myös eräitä usein pronomineiksi luokiteltuja ilmauksia.

Jonnekin ehkä myös perusteluja siitä, miksi taks1 ei ole operationalisoitu
tilastolliseen malliin: olisi hassua olettaa, että sillä on merkitystä,
onko kyseessä

-->

L7d-ryhmän harvinaisuus oli siis odotettavaa, mutta yllättävää kyllä myös
L7a-ryhmä osoittautui etenkin suomessa erittäin vähälukuiseksi. Tarkkaa
syytä sille, miksi tapauksia löytyi niin niukasti, ei lopulta saatu selville. Esimerkiksi
Araneum Finnicum -korpuksen koko hakutulos kyselylle *siitä asti* tuottaa
ainoastaan 4 025 tulosta, siinä missä vaikkapa *silloin*-sana tuottaa 492
747 hakutulosta. Suomen kielen lehdistökorpuksesta haettaessa tuloksia taas
*siitä asti* -ilmaukselle saadaan ainoastaan 243. Kun tuloksista suodatetaan
osiossa
[5.1.2](ajanilmaukset_tutkimusaineistossa.html#tutkittavien-ajanilmaustapausten-erottaminen-verrannollisista-korpuksista)
asettettujen kriteerien mukaisesti, jäljelle jäävien ilmausten määrä jää
auttamatta pieneksi. 


|Ryhmä |Takson.1       |Takson.2   |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------------|:----------|:--------|----:|----:|:--------------------|
|L7a   |muu ei-kalend. |L/sekv-dur |anafor.  |   31|  752|NP                   |
|L7c   |adv/pron       |L/sim      |anafor.  | 1535| 1828|ADV                  |
|L7b   |aikaa kvant.   |L/sim      |anafor.  |  517|  718|NP                   |
|L7d   |aikaa kvant.   |L/sim      |ei       |   46|  138|NP                   |

#### L8a, L8b, L8c


| Ryhmän nimi   | suomi              | venäjä   |
| ------------- | ------------------ | -------- |
| L8a           | pian               | скоро    |
| L8b           | nykyisin/nykyään   | теперь   |
| L8c           | nyt                | сейчас   |

L8-aineistoryhmät koostuvat lyhyistä puhehetkeen ankkuroituvista ja tarkemmalta
semanttiselta funktioltaan simultaanisista adverbeista. Jako L8b- ja
L8c-ryhmien välillä on tehty venäjän tähden, sillä теперь- ja сейчас-sanoilla
on tunnetusti erityislaatuinen työnjako (ks. luku [8.2.2.3](keskisijainti.html#l7l8-erot-kesk)).
L8-ilmausten simultaanisuus on pääosin kehyksistä, joskin
rymä L8c voi lokalisoida tapahtumia myös punktuaalisesti. 

L8a-ryhmä päätettiin lisätä tutkimukseen mukaan sen tähden, että tässä
luetellun kaltaiset lyhyet deiktiset, simultaaniset adverbit ovat kummassakin
kielessä hyvin tavallisia, mutta toisalta L8b- ja erityisesti venäjässä myös
L8c-ryhmiin liittyy paljon leksikaalisia erityispiirteitä. L8a-ryhmän tarkoitus
on tuoda tämän tyyppiset deiktiset adverbit monipuolisemmin edustetuksi. 

L8-ilmausten yleisyys näkyy myös taulukossa \@ref(tab:l8meta), joka osoittaa
tässä esiteltyjen aineistoryhmien kokonaismääräksi 
11 186.

<!--

TODO: testaa tilastollista mallia ilman L8a:ta??

Juttua siitä, että punktuaalisuusjaottelua ei juuri siksi otettu mukaan
varsinaiseen tilastolliseen malliin, että sitä on vaikea mitata aineistoryhmien 
tasolla: olisi määriteltävä ennemminkin tapauskohtaisesti...
Otetaan kyllä huomioon tarkemmissa analyyseissa....

-->


|Ryhmä |Takson.1 |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------|:--------|:--------|----:|----:|:--------------------|
|L8b   |adv/pron |L/sim    |deikt.   | 1935| 1375|deict.ADV            |
|L8a   |adv/pron |L/sim    |deikt.   | 2186| 2117|deict.ADV            |
|L8c   |adv/pron |L/sim    |deikt.   | 1736| 1837|deict.ADV            |

#### L9a, L9b, L9c, L9d


| Ryhmän nimi   | suomi                                               | venäjä                                       | tarkempi sem. funktio      |
| ------------- | --------------------------------------------------- | -------------------------------------------- | -------------------------  |
| L9a           | vuonna 1990...1999                                  | в 1990...1999 году                           | simultaaninen, kehyksinen  |
| L9b           | vuodesta 1900...1999                                | с 1900...1999 года                           | sekventiaalis-duratiivinen |

<!--

Myös L9c pois...? AINIIN eikun se oli jo alun perin jäänyt pois?!


Punktuaalisuuden lisähaaste: ei koske vain sim vaan muitakin..

-->

L9-ryhmät sisältävät vuosiin ankkuroituja ajanilmauksia. Ryhmä L9b on
sekventiaalis-duratiivinen, L9a taas simultaaninen ja esiintyy tarkemmin
määriteltynä sekä kehyksisesti että punktuaalisesti lokalisoivana,
mahdollisesti useammin kuitenkin jälkimmäisenä versiona. Molempia ryhmiä
yhdistää se, että taksonomian 3 kannalta niiden voi nähdä ankkuroituvan
absoluuttiselle aikajanalle. 

Ryhmässä L9b tutkittavien tapausten ulkopuolelle pyrittiin tarkoituksella
jättämään ilmaukset, joissa myös ajanjakson loppupää on ilmaistu (vuodesta 1900
vuoteen 1910). Mukaan otettiin kuitenkin luvussa esitetyn esimerkin
@ee_spereryvom kaltaiset venäjänkieliset tapaukset, joissa ajanjakson
loppupiste on selkeästi oma erillinen ilmauksensa. Suomessa mukaan on otettu
niin postposition *asti* tai *lähtien* sisältävät ilmaisut kuin ilmaisut ilman
postpositiota. 

L9-ryhmien tarkemmat ominaisuudet on lueteltu taulukossa \@ref(tab:l9meta).


|Ryhmä |Takson.1      |Takson.2   |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:-------------|:----------|:--------|----:|----:|:--------------------|
|L9d   |luonnon sykl. |L/sekv-dur |muu      | 3947| 1008|NP                   |
|L9a   |luonnon sykl. |L/sim      |muu      | 3691| 2695|NP                   |

### F-funktiota edustavat aineistoryhmät

F-funktiota edustaa tutkimusaineistossa 
 10 aineistoryhmää. Näistä
taksonomian 1 kannalta potentiaalisesti kalendaarisia ilmauksia ovat ainoastaan
ryhmät F1a ja F1b, joista edellinen kuuluu luonnon sykleihin perustuvista
ilmauksista edelleen johdettujen tapausten alaluokkaan, jälkimmäinen taas
positionaalisten ilmausten alaluokkaan. Suurin osa F-funktion edustajista on
siis ei--kalendaarisia, ja tähän joukkoon voidaan laskea myös
*kerta*/*раз*-sanoille perustuva F2a-ryhmä, joka on morfologiselta
rakenteeltaan nominaalinen eikä sellaisenaan osu luontevasti oikeastaan
mihinkään taksonomian 1 mukaisista luokista. Kuten osiossa
[2.2.2.2](ajan_ilmaisemisen_monet_muodot.html#taajuutta-ilmaiseva-semanttinen-funktio) esitettiin, nämä ilmaukset
muodostavat kuitenkin oman selkeän luokkansa F-funktion sisällä. Ryhmät F3a--e
sekä F4 puolestaan ovat adverbisiä ei--kalendaarisia ilmauksia.


F-funktiota edustavien ajanilmausten määrittely taksonomian 3 suhteen ei ole
aivan yksiselitteistä: lähtökohtaisesti F-funktion edustajat  ovat
ei--referentiaalisia, joskin F2a-ryhmän *maanantaisin*-tyyppisillä ilmauksilla
voidaan tietyissä yhteyksissä tulkita olevan viittauskohde tai tarkemmin ottaen
useita viittauskohteita aikajanalla.


#### F1a, F1b

| Ryhmän nimi | suomi                              | venäjä                          | 
|-------------| ---------------------------------- | ------------------------------  | 
| F1a         | joka päivä/viikko/kuukausi/vuosi   | каждый день/неделю/месяц/год    | 
| F1b         | maanantaisin...sunnuntaisin        | по понедельникам...воскресеньям | 

F1a-ryhmässä huomionarvoista on se, että kyseessä on L2a-, L2b- ja L4a-ryhmien
tavoin positionaalinen aineistoryhmä. Koska *maanantaisin*- ja *по
понедельникам* -tyyppiset ilmaukset ovat aineistossa huomattavasti
harvinaisempia kuin L2a-ryhmän viikonpäivätapaukset, otettiin F1b-ryhmään
mukaan kaikki seitsemän viikonpäivää (vrt. osio [5.2.3.2](ajanilmaukset_tutkimusaineistossa.html#l2a-l2b) edellä).
Taksonomian 1 kannalta F1-ryhmät edustavat aineiston
ainuita kalendaarisia F-funktiotapauksia. Ryhmien tarkemmat ominaisuudet
on listattu taulukkoon \@ref(tab: f1meta).


|Ryhmä |Takson.1      |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:-------------|:--------|:--------|----:|----:|:--------------------|
|F1a   |luonnon sykl. |F/freq   |ei       | 2077| 2405|NP                   |
|F1b   |pos           |F/freq   |ei       |  899|  133|NP                   |


#### F2a, F2b

| Ryhmän nimi | suomi                              | venäjä                         |
|-------------| ---------------------------------- | ------------------------------ |
| F2a         | kaksi....kymmenen kertaa           | два...десять раз               |
| F2b         | kahdesti, kolmesti                 | дважды, трижды                 |

Ei--kalendaarisista F2-ryhmistä ensimmäinen pitää sisällään nominaalisia
tapauksia ja toinen adverbejä, ja merkityksen kannalta kyseessä ovat
käytännössä synonyymiset ryhmät. Nämä kaksi vaihtoehtoista tapaa ilmaista
numeerista taajuutta ovat hyvin samanlaisina käytössä niin suomessa kuin venäjässäkin, joten oli
loogista ottaa molemmat mukaan omina aineistoryhminään. Taksonomian 3 kannalta
voidaan todeta, ettei näillä ilmauksilla ei ole referenttejä.

Tässä tarkasteltavista tapauksista jätettiin lopulta pois yhteen toistumiskertaan
viittaavat ilmaukset, ennen muuta suomen *kerran* ja venäjän *однажды*. Nämä olisivat
muodostaneet mielenkiintoisen, semanttiselta funktioltaan ambivalentin (kontekstista
riippuen joko F- tai L-funktiota ilmaisevan) tapauksen. Ilmausten hakeminen ja
suodattaminen lähdekorpuksista osoittautui kuitenkin lopulta liian haastavaksi, joten
näiden tapausten mukaan ottamisesta luovuttiin. *Kerran-* ja *однажды*-tapauksiin
viitataan kuitenkin lyhyesti osiossa [8.1.1](keskisijainti.html#morf-sama-keski).

F2-ryhmien tarkemmat ominaisuudet on lueteltu taulukkoon \@ref{f2meta}.


|Ryhmä |Takson.1       |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------------|:--------|:--------|----:|----:|:--------------------|
|F2b   |adv/pron       |F/freq   |ei       | 1833| 1658|ADV                  |
|F2a   |muu ei-kalend. |F/freq   |ei       | 1257|  991|NP                   |

#### F3a, F3b, F3c, F3d, F3e

| Ryhmän nimi | suomi                              | venäjä                         | 
|-------------| ---------------------------------- | ------------------------------ | 
| F3a         | usein                              | часто                          | 
| F3b         | aina                               | всегда                         | 
| F3c         | harvoin                            | редко                          | 
| F3d         | joskus                             | иногда                         | 
| F3e         | tavallisesti, yleensä              | обычно                         | 

F3-ryhmät ovat joukko ei--kalendaarisia adverbejä, joista F3a-b
korostavat yleisyyttä, F3c harvuutta, F3d on neutraali ja F3e
alaryhmältään epämääräistä taajuutta ilmaiseva. Taksonomian 3 kannalta
voidaan todeta, ettei yksikään F3-aineistoryhmistä ole
referentiaalinen. 

F3a--F3e-ryhmien muodostaminen oli sikäli suoraviivaista, että
molemmissa tutkimuksen kielissä ovat
käytössä koko lailla vastaavat ilmaukset. Kummassakin kielessä on
lisäksi olemassa yllä mainituista adverbeistä johdettuja,
merkitykseltään hivenen erilaisia tapauksia kuten 
useuteen viittaava venäjän *зачастую* sekä venäjän ja suomen harvuuteen
viitaavat *изредка* ja *harvakseltaan*. Tutkimukseen otettiin 
kuitenkin mukaan vain selkeimmin vertailtavissa olevat ja
frekventeimmät perustapaukset, jotka ovatkin jo sellaisinaan
tutkimuksen suurimpien aineistoryhmien joukossa.
Tarkat frekvenssit näkyvät taulukossa \@ref(tab: f3meta).

<!--

TODO: takson. 2 taulukossa f-ilmausten osalta!

-->


|Ryhmä |Takson.1 |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------|:--------|:--------|----:|----:|:--------------------|
|F3e   |adv/pron |F/freq   |ei       | 2564| 1867|ADV                  |
|F3c   |adv/pron |F/freq   |ei       | 1845| 1868|ADV                  |
|F3d   |adv/pron |F/freq   |ei       | 1756| 1667|ADV                  |
|F3a   |adv/pron |F/freq   |ei       | 3159| 2508|ADV                  |
|F3b   |adv/pron |F/freq   |ei       | 2101| 3356|ADV                  |


#### F4

| Ryhmän nimi | suomi                              | venäjä                         |
|-------------| ---------------------------------- | ------------------------------ |
| F4          | ajoittain                          | временами                      |

F4-ryhmä edustaa  taksonomian 1 kannalta aikaa indefiniittisesti
kvantifioivia F-funktiotapauksia. Merkitykseltään se on lähellä ryhmää
F3d ja myös se on aineistossa tulkittu morfologiselta rakenteeltaan
adverbiryhmäksi, joskin tässä tapauksessa leksikaalistuneen ilmauksen
pohjalla oleva nomini ja siihen liittyvät sijapäätteet ovat
kummassakin kielessä yhä hyvin näkyvissä. Kuten F-ryhmät yleensä,
tässäkin tapauksessa kyseessä on taksonomian 3 kannalta
ei--referentiaalinen ilmausjoukko. 


|Ryhmä |Takson.1     |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:------------|:--------|:--------|----:|----:|:--------------------|
|F4    |aikaa kvant. |F/freq   |ei       | 2501|  725|ADV                  |


### E-funktiota edustavat aineistoryhmät

Ekstensiota ilmaisevia aineistoryhmiä kerättiin tutkimusaineistoon
kaikkiaan 14
kappaletta. Koska ekstension ilmaiseminen on läheisesti sidoksissa
kuluneen tai kuluvan ajan mittaamiseen, E-aineistoryhmistä suuri osa
liittyy taksonomiassa 1 potentiaalisesti kalendaarisiin, lunnossa havaittavista
sykleistä suoraan tai välillisesti johdettuihin ajanyksiköihin.
Tällaisia ryhmiä ovat E1a--c, E2a--b ja E3a--b. Ei--kalendaarisista
ilmauksista E4-, E5a- ja E6c-ryhmät ovat aikaa indefiniittisesti
kvantifioivia, E5b-, E6a-, E6b- ja E7-ryhmät puolestaan adverbi-ilmauksia.

Taksonomian 3 osalta voidaan todeta, että ryhmät E2b ja E3b edustavat
edellä osiossa [5.2.1](ajanilmaukset_tutkimusaineistossa.html#aineistoryhmien-muodostusprosessi) mainittua
pyrkimystä saada tutkimukseen mukaan monipuolisesti erilaisia
referentiaalisia ilmauksia -- myös muita kuin tyypillisiä lokalisoivia
tapauksia. Mainittujen ryhmien muilta ominaisuuksiltaan samantyyppiset
parit E2a ja E3a samoin kuin kaikki muut E-funktiota edustavat ryhmät
ovat taksonomian 3 mukaisessa luokittelussa ei--referentiaalisia.

<!--
TODO: pitäisikö taksonomiasta 3 olla oma pieni taksonomioiden 1 ja 2
kaltainen puukuvansa?
-->


#### E1a, E1b, E1c

<!-- venäjän несколько ym. lisäksi минута >> poista R:ssä? -->

| Ryhmän nimi | suomi                                                                 | venäjä                                        | 
|-------------|-----------------------------------------------------------------------|-----------------------------------------------|
| E1a         | kaksi...kymmenen päivää/vuotta/viikkoa/tuntia                         | два...десять дней, лет, недель, часов         | 
| E1b         | kahdessa...kymmenessä päivässä/vuodessa/viikossa/tunnissa             | за два...десять дней, лет, недель, часов      | 
| E1c         | kahdeksi...kymmeneksi kuukaudeksi/viikoksi/päiväksi/vuodeksi/tunniksi | на два...десять месяцев/недель/дней/лет/часов | 

E1-ryhmien ilmaukset sisältävät lukusanan ja kalendaarisen
ajanyksikön. E1a edustaa duratiivista, E1b varsinaista resultatiivista
(erotuksena kehyksisestä resultatiivisesta kuten E3-ryhmät alla) ja
E1c teelistä E-funktiota. Kuten E-funktiolle on tavallista, näistä
yksikään ei ole referentiaalinen ilmaus.

Erityisesti E1a-ilmausten etsiminen aineistosta oli haastavaa, sillä numero +
ajanyksikkö -yhdistelmää haettaessa tarkkuus on väistämättä huono. Selkein
hakutuloksista ohitettu joukko olivat ne venäjän tapaukset, joissa
etsityn ilmauksen jäljessä oli tai niitä edelsi adpositio (esimerkiksi *назад*,
*спустя* tai *через*). Lopulliset määrät varsinaiseen aineistoon päätyneistä
ilmauksista on totuttuun tapaan koottu alla olevaan taulukkoon:



|Ryhmä |Takson.1      |Takson.2   |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:-------------|:----------|:--------|----:|----:|:--------------------|
|E1c   |luonnon sykl. |E/telic    |ei       | 2114| 1209|NP                   |
|E1b   |luonnon sykl. |E/res.vars |ei       | 2596| 1262|NP                   |
|E1a   |luonnon sykl. |E/dur      |ei       | 1123|  577|NP                   |

<!--

Huom: saanti + tarkkuus vakiintuneina suomenkielisinä termeinä: korjaa
koko tutkimuksen laajuisesti

-->

<!--
Lisää käytetyistä tarkkuutta parantavista rajoituksista
Pääverbeistä karsittu pois дать, получить
-->


#### E2a, E2b

| Ryhmän nimi | suomi              | venäjä              |
|-------------| ------------------ | ------------------- |
| E2a         | viikon             | неделю              |
| E2b         | koko viikon        | всю неделю          |

Kuten tämän osion johdannossa mainittiin, E2-ryhmät muodostavat
taksonomian 3 kannalta ei--referentiaalinen--referentiaalin-parin: siinä missä E2a
ei viittaa mihinkään tiettyyn jaksoon aikajanalla, ankkuroi
E2b-ilmauksen attribuutti ilmauksen vähintäänkin puhujan
subjektiiviselle aikajanalla, jossain mielessä myös absoluuttiselle,
ajatellen että kyseessä on joka tapauksessa ollut kaikille ihmisille
sama, tietty viikko. Taksonomian 1 kannalta kyseessä
ovat kalendaariset, luonnon sykleistä edelleen johdetut ilmaukset ja
tarkempi taksonomian 2 mukainen alaluokka on duratiivinen E-funktio. 

Ajanyksikkönä *viikko* valikoitui näihin ryhmiin, koska venäjän
*неделя* on feminiinisukuinen ja sitä kautta sen akkusatiivimuodot on
helppo erottaa aineistosta. Kaiken kaikkiaan myös E2-ryhmien
ilmaisujen kohdalla korpuhakujen tarkkuus osoittautui tavallista
heikommaksi. Suomessa tarkkuutta on parannettu poistamalla ilmausta
seuraavat *ajan, mittaan, jälkeen* ym., venäjässä taas ennen kaikkea
poistamalla adpositio *назад*. Samaten ryhmää E2a haettaessa oli
luonnollisesti poistettava ryhmään E2b kuuluvat tapaukset sekä muut
referentiaalisuutta implikoivat attribuutit kuten *viime/ensi*.
Molemmat ryhmät ovat morfologiselta rakenteeltaan nominaalisia, ja muut
tarkemmat ominaisuudet on listattu taulukkoon \@ref(tab: e2meta).



|Ryhmä |Takson.1        |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:---------------|:--------|:--------|----:|----:|:--------------------|
|E2b   |edell. johdetut |E/dur    |muu      |  121|  615|NP                   |
|E2a   |edell. johdetut |E/dur    |ei       |  186|  122|NP                   |

#### E3a, E3b {#e3a-e3b}

| Ryhmän nimi | suomi                                                                | venäjä                                                                     |
|-------------| -------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| E3a         | vuoden/viikon/kuukauden/kevään...talven kuluessa                     | в течение годя/месяца/недели/весны...зимы                                  |
| E3b         | viime/koko/sen/tuon vuoden/viikon/kuukauden/kevään...talven aikana   | в течение этого/того/прошлого/последнего годя/месяца/недели/весны...зимы   |

E3-ryhmiin on koottu kehyksisiä resultatiivisen ekstension ilmauksia
eli jonkin ajanjakson mittaiselle ajalle sijoitettavia tapahtumia.
Koska nämä ilmaukset ovat melko harvinaisia, ajanyksiköinä on käytetty
mahdollisimman laajaa eri kalendaaristen ilmausten joukkoa. E3a ei ole
referentiaalinen, mutta E3b-tapauksissa attribuutit ankkuroivat
ilmauksen joko puhujan subjektiiviselle aikajanalle tai suhteessa
puhehetkeen, niin että E3-ilmaukset muodostavat E2-ryhmien kaltaisen
referentiaalinen--ei--referentiaalinen-parin. Suomessa ryhmää E3a
varten on käytetty *aikana*-adposition sijaan adpositiota *kuluessa*,
millä on pyritty parantamaan E3a-ryhmän hakutulosten tarkkuutta:
tuloksena on varmemmin ei-referentiaalisia ilmauksia kuin mitä
*aikana*-sanan avulla olisi saatu. Morfologiselta rakenteeltaan
E3-ryhmät ovat nominaalisia. Yksityiskohdat on lueteltu taulukkoon
\@ref(tab: e3meta).



|Ryhmä |Takson.1      |Takson.2  |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:-------------|:---------|:--------|----:|----:|:--------------------|
|E3a   |luonnon sykl. |E/res.keh |ei       |  916| 1164|NP                   |
|E3b   |luonnon sykl. |E/res.keh |muu      | 1286|  324|NP                   |


#### E4

| Ryhmän nimi | suomi        | venäjä       |
|-------------|--------------|--------------|
| E4          | siinä ajassa | за это время |

E4-ryhmä käsittää yhden taksonomian 1 kannalta aikaa
indefiiniittisesti kvantifioivaksi luokiteltavan ilmauksen.
Taksonomian 2 kannalta ryhmä ilmaisee varsinaista resultatiivista ekstensiota, mutta sen
taksonomian 3 mukainen referentiaalisuus ei ole selkeästi
määriteltävissä. Voisi ajatella, että *siinä* ja *это* ankkuroivat ilmauksen
tiettyyn, keskustelussa aiemmin muodostettuun ajalliseen referenttiin, vaikka
toisaalta ne voivat myös vain viitata tiettyyn aiemmin mainittuun
mittayksikköön. Kummankinkieliset ilmaukset ovat nominaalisia.
Kyseessä on niin suomessa kuin venäjässä kohtalaisen harvinainen
aineistoryhmä, niin kuin taulukosta \@ref(tab: e4meta) käy ilmi:



|Ryhmä |Takson.1     |Takson.2   |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:------------|:----------|:--------|----:|----:|:--------------------|
|E4    |aikaa kvant. |E/res.vars |ei       |  117|  173|NP                   |

#### E5a, E5b

| Ryhmän nimi | suomi              | venäjä                      | 
|-------------| ------------------ | -------------------         | 
| E5a         | hetkessä           | в/во/за мгновенье/мгновение | 
| E5b         | äkkiä, yhtäkkiä    | вдруг                       | 

E5-ryhmät edustavat  taksonomiassa 2 lähtökohtaisesti varsinaista
resultatiivista ekstensiota, joskin kummassakaan tapauksessa --
etenkään E5b-ryhmän kohdalla -- tämä ei läheskään aina ole
yksiselitteistä: kuten luvussa
[7.1.1](alkusijainti.html#varsinainen-resultatiivinen-ekstensio) todetaan, E5b-ryhmän
sanoilla on etenkin venäjässä usein jopa ei-temporaalinen
diskurssipartikkelin funktio. 

E5a-ryhmä on aineistossa merkittävämpi
suomen kuin venäjän kannalta, sillä suomessa *hetkessä*-ilmaukset ovat
verrattain yleisiä. E5b-ilmaukset ovat (temporaalisesti käytettynä)
merkityksensä puolesta lähellä E5a-ilmauksia, mutta ilmentävät tiettyä
punktuaalista luonnetta. Taksonomian 3 kannalta ryhmät ovat
ei--referentiaalisia; morfologisen rakenteen osalta voidaan todeta,
että E5a-ryhmä on aineistossa analysoitu nominaaliseksi, E5b-ryhmä
taas adverbiseksi. 

Kuten taulukko \@ref(tab: e5meta) osoittaa,
venäjässä tähän valikoidut ilmaukset ovat melko harvinaisia johtuen
kielten välillä olevasta, luvussa [2.2.1.2](ajan_ilmaisemisen_monet_muodot.html#ei-kalendaariset-ajanilmaukset)
mainitusta erosta *hetki*-sanan käytössä: venäjässä monet suomen
*hetkessä*-sanalla kuvattavat ilmiöt tulevat ennemmin ilmaistuksi joko
E5b-ryhmän ilmauksilla tai esimerkiksi *минута*-sanaa käyttämällä.
Jälkimmäisten tapausten vertailukelpoinen hakeminen korpusaineistoista
olisi kuitenkin ollut vaikeaa ja haun tarkkuus olisi jäänyt heikoksi.




|Ryhmä |Takson.1     |Takson.2   |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:------------|:----------|:--------|----:|----:|:--------------------|
|E5b   |adv/pron     |E/res.vars |ei       | 1445| 2768|ADV                  |
|E5a   |aikaa kvant. |E/res.vars |ei       | 1574|  111|NP                   |

#### E6a, E6b, E6c

| Ryhmän nimi   | suomi                                     | venäjä                         |
|---------------|-------------------------------------------|--------------------------------|
| E6a           | kauan                                     | долго                          |
| E6b           | pitkään aikaan, pitkiin aikoihin, pitkään | давно                          |
| E6c           | jonkin aikaa                              | какое-то время, некоторе время |


E6-ryhmän ilmaukset edustavat kaikki taksonomiassa 2 duratiivista E-funktiota.
Venäjässä E6a ja E6b ovat taksonomian 1 kannalta ei--kalendaarisia ja 
venäjässä puhtaasti adverbejä, joskin suomessa E6b voidaan
muodostaa paitsi adverbisesti, myös *aika*-sanan avulla. Suomen
E6b-ilmausten konkordanssihakujen tuloksia olikin siistittävä
tarkkuuden varmistamiseksi: lukuun ottamatta *aika*-sanaa E6b-ryhmästä
karsittiin pois kaikki sellaiset tapaukset, joissa *pitkään* olisi
syntaktisessa analyysissa luokiteltu jonkin substantiivin
dependentiksi. Taksonomian 3 osalta voidaan todeta, että E6-ryhmien
ilmauksilla ei lähtökohtaisesti ole viittauspisteitä, joskin
E6c-ilmausten voi nähdä sisältävän jonkinlaisen vihjeen käsiteltävänä
olevan ajanjakson esittämisestä tiettynä, eroteltavissa olevana
aikajanan lohkona.

E6-ryhmien koosta on huomautettava, että vaikka koko aineistossa
havaittava yleinen tendenssi on, että suomenkielisiä tapauksia on
enemmän, on lopulliseen tutkimusaineistoon päätyneiden
venäjänkielisten E6a- ja E6b-tapausten määrä huomattavasti suomea
huomattavasti yleisempiä. Tämä käy ilmi taulukosta \@ref(tab: e6meta):



|Ryhmä |Takson.1     |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:------------|:--------|:--------|----:|----:|:--------------------|
|E6c   |aikaa kvant. |E/dur    |ei       |  718|  503|NP                   |
|E6a   |adv/pron     |E/dur    |ei       | 1003| 2007|ADV                  |
|E6b   |adv/pron     |E/dur    |ei       | 2549| 3373|ADV                  |

<!--

TODO: ryhmien järjestys näissä alataulukoissa.
Mietintään: määrien ilmoittaminen jaoteltuna lehdistöön ja
internet-aineistoon?

-->

#### E7

| Ryhmän nimi |  suomi    | venäjä   |
|-------------|-----------|----------|
| E7          |  nopeasti | быстро   |

Ryhmään E7 kuuluvat ajanilmaukset määriteltiin luvussa
[2.2.2.3](ajan_ilmaisemisen_monet_muodot.html#ekstensiota-ilmaiseva-semanttinen-funktio) taksonomian 2 kannalta
aikaa ja tapaa ilmaiseviksi, sillä niiden ajallisuus on jossain määrin
tulkinnanvaraista. Nämä ilmaukset toimivatkin hyvänä rajatapauksena,
jonka avulla voidaan tarkastella ajanilmausten sijainnin suhdetta
tavanilmausten sijaintiin. Luonnollisesti kyseessä ovat morfologiselta
rakenteeltaan adverbiset, taksonomian 1 kannalta
ei--kalendaariset ja taksonomian 3 kannalta ei--referentiaaliset
ilmaukset. Taulukko \@ref(tab: e7meta) puolestaan osoittaa, että
ilmausten hakeminen aineistosta oli yksinkertaista ja lopulliseen
tutkimusaineistoon päätyneiden tapausten määrä suuri.



|Ryhmä |Takson.1 |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------|:--------|:--------|----:|----:|:--------------------|
|E7    |adv/pron |E/tapa   |ei       | 3424| 2816|ADV                  |

### Muita funktiota edustavat aineistoryhmät

L-, E- ja F-ajanilmausten lisäksi tutkimusaineistoon on kerätty pieni edustus
myös edellä presuppositionaaliseksi ja likimääräiseksi määritellyistä
semanttisista funktioista. Näistä presuppositionaaliset tapaukset
(ryhmäkoodien ensimmäinen kirjain *P*) ovat taksonomian 1 kannalta
ei--kalendaarisia, tähän valitut likimääräiset tapaukset (ryhmäkoodin alussa *LM*
erotuksena lokalisoivaan funktioon viittaavasta *L*:stä) taas
kalendaarisia, tarkemmin ottaen luonnon sykleistä edelleen johdettuja
(kellonaikoja).

#### P1, P2

| Ryhmän nimi | suomi   | venäjä   |
|-------------| ------- | -------- |
| P1          | jo      | уже      |
| P2          | vielä   | еще      |

Presuppositionaalista funktiota edustavat aineistossa suomen *jo*- ja
*vielä*- sekä venäjän *уже*- ja *еще*-sanat, jotka luonnollisesti ovat
morfologiselta rakenteeltaan adverbejä. Kummassakin kielessä näihin
sanoihin liittyy ymmärrettävästi paljon tapauksia, joiden
tulkitseminen ajallisiksi on usein ongelmallisia. Lähtökohtaisesti
aineistoa on suodatettu niin, että mukaan on otettu ainoastaan
itsenäiset tapaukset, ei siis esimerkiksi ilmausta *jo eilen*.


|Ryhmä |Takson.1 |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:--------|:--------|:--------|----:|----:|:--------------------|
|P2    |adv/pron |P/presup |ei       | 1936|  442|ADV                  |
|P1    |adv/pron |P/presup |ei       | 2124| 2534|ADV                  |


#### LM1

| suomi                                               | venäjä                      |
| --------------------------------------------------- | --------------------------- |
| viiden...kahdentoista maissa/tienoilla/hujakoilla   | около двух...двенадцати     |

Likimääräisyyttä edustavaan aineistoryhmään valittiin kummastakin
kielestä ilmaukset, joissa likimääräisyys saadaan aikaan adposition
avulla. Venäjästä tarkastelun kohteeksi otettiin *около*-prepositio,
suomesta joukko postpositioita. Venäjänkielistä aineistoa oli
suodatettava siten, ettei mukaan tule E-funktiota ilmaisevia tapauksia
kuten *он играл на скрипке около двух часов* ('hän soitti viulua noin
kaksi tuntia'). Kuten taulukosta \@ref(tab: lmmeta) käy ilmi,
LM1-ryhmä on kummassakin kielessä kohtalaisen harvinainen.



|Ryhmä |Takson.1        |Takson.2 |Takson.3 | n/fi| n/ru|Morfologinen rakenne |
|:-----|:---------------|:--------|:--------|----:|----:|:--------------------|
|LM1   |edell. johdetut |L/likim  |muu      |  134|  158|NP                   |


Yleiskatsaus sijainneista tutkimusaineiston perusteella {#sij-yleisk}
-------------------------------------------------------

<!--

TÄNNE SOV-testi? Minne parseritesti?

-->

Luon luvun  [5](ajanilmaukset_tutkimusaineistossa.html#ajanilmaukset-tutkimusaineistossa) lopuksi alustavan
katsauksen siihen, miten ajanilmausten neljä sijaintia suomen- ja
venäjänkielisessä tutkimusaineistossa jakautuvat.
Jos tarkastellaan kaikkia aineistoryhmiä yhdessä ja tutkitaan ainoastaan
suhteellisia osuuksia, saadaan taulukon 2 mukainen
prosentuaalinen jakauma:

<!--Mukaan myös absoluuttiset luvut?-->


---------------------------------------
&nbsp;   S1      S2      S3     S4     
-------- ------- ------- ------ -------
fi       11.31   5.57    47.6   35.52  

ru       36.79   49.13   4.89   9.19   
---------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 2}: Yleiskuva prosentuaalisista jakaumista \normalfont \vspace{0.6cm}

Taulukko 2 antaa karkean yleiskuvan kielten välisestä
erosta. Oletusten mukaisesti sijainnit S1 ja S2 ovat yleisimmät venäjässä, ja
sijainti S3 on yleisin suomessa. Kontrasti tutkittavien kielten välillä on
kuitenkin yllättävänkin selvä. Suomi ja venäjä näyttävät ajanilmausten
sijoittumisen valossa suorastaan toistensa peilikuvilta: venäjässä painottuvat
sijainnit ennen verbiä, suomessa sijainnit verbin jälkeen. Jo taulukon
2 tarkastelu osoittaa, että venäjän ja suomen välillä todella
on selvä systemaattinen ero siinä, miten ajanilmaukset lauseessa sijoittuvat.
Ero on jyrkempi kuin pelkästään tutkimuskirjallisuuden tai intuition
perusteella saattoi olettaa ja ulottuu paitsi ennalta oletettavissa olleisiin
S2- ja S3-sijainteihin, myös muihin.

<!--T.kys.ollenkaan?-->

Ehkä yllättävin havainto koskee eroa S1-sijainnin osuudessa. Kuvio
1 esittää S1-sijainnin suhteellisen yleisyyden eri
aineistoryhmien välillä:

![\noindent \headingfont \small \textbf{Kuvio 1}: S1-sijainnin suhteellinen yleisyys aineistoryhmissä \normalfont](figure/boxplots1-1.svg)

Kuvion 1 perusteella S1 on kauttaaltaan yleisempi sijainti
venäjässä kuin suomessa -- suurimmassa osassa venäjänkielisiä aineistoryhmiä S1-sijainnin osuus
asettuu suurin piirtein 20 ja 50 prosentin välille, kun taas  suomessa valtaosa
ryhmistä asettuu 5 ja 20 prosentin välille. Havainnosta tekee yllättävän se,
ettei pelkän tutkimuskirjallisuuden tai intuition perusteella nouse yhtä
selkeää ajatusta siitä, mikä S1-sijainteja voisi tarkastelluissa kielissä
erottaa. Päinvastoin, tutkimuskirjallisuuden pohjalta oli syytä olettaa, että
S1-sijoittuneen ajanilmauksen sisältävät konstruktiot (S1-konstruktiot) ovat
yhtä lailla tavallisia niin venäjässä kuin suomessa. Esimerkiksi Hakulinen ym. [-@hkv1980, 123]  
toteavat oman kvantitatiivisen tutkimuksensa yhteydessä, että suomessa ajan adverbiaalit ovat
esimerkiksi paikkaan verrattuna selvemmin "kasaantuneet alkuasemaan". 

