---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
fontsize: 11.5pt
geometry: twoside, b5paper, layout=b5paper, left=2cm, right=2cm, top=2cm, bottom=3cm, bindingoffset=0.5cm
indent: true
subparagraph: yes
output:
    html_document:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
    pdf_document: 
      latex_engine: xelatex
      toc: true
      toc_depth: 6
      number_sections: true
      keep_tex: true
      includes:
          in_header: preamble.tex
---
<style>
	                    .outerbox{
	                        border: 1px solid black;
	                        display: inline-block;
	                        padding: 5px;
	                    }
	
	                    .cxg{
	                        font-size:80%;
	                    }
	
	                    .outerbox &gt; .statusline{
	                        padding-left:5px;
	                    }
	
	                    .containerbox-outer{
	                        display:inline-block;
	                    }
	                    .containerbox{
	                        display:flex;
	                        padding:0.2em;
	                        align-items:flex-end;
	                    }
	
	                    .innerbox{
	                        display:flex;
	                    }
	
	                    .node {
	                        border: 1px solid black;
	                        margin: 0.2em;
	                        padding: 0.2em;
	                    }
	
	                    .node:last-child {
	                        margin-right: 0em;
	                    }
	
	
	                    .featureline {
	                        display: flex;
	                    }
	
	                    .fleft {
	                        margin-right: 10px;
	                    }
	
	                    .matrixfeat{
	                        border-left:1px solid black;
	                        border-right:1px solid black;
	                        border-radius:5px;
	                        padding:0.3em;
	                    }
	
	
	                    .dashed{
	                        border: 1px dashed black;
	                    }
	
	                    .quote p {
	                        margin-bottom: 1em;
	                    }
	
	                    .quote {
	                        margin-left: 3em;
	                        font-family: serif;
	                        padding: 0.1em 0.1em 0.4em 2em;
	                        background: whitesmoke;
	                        font-size: 90%;
	                        line-height: 130%;
	                        color: #4e4e4e;
	                    }
	
	                    .toc-content {
	                        text-align: justify;
	                        max-width: 700px;
	                    }
	
	                    .toc-content h1,.toc-content h2, .toc-content h3, .toc-content h4{
	                        text-align: left;
	                    }
	
	                    .example li{
	                        font-size:0.8em;
	                    }
	
	                    .example li + li{
	                        margin-top:0.5em;
	                    }
	
	                    /* HACK kable-taulukoille rinta rinnan */
	                    .kable_wrapper td table {
	                        margin-right: 5em;
	                        margin-bottom: 2em;
	                    }
	
	                    </style>



Ajanilmaukset ja loppusijainti
=============================


Tarkasteltaessa keskisijaintia kävi ilmi, että suurin osa ajanilmauksen
sijainnin kannalta selitysvoimaisista konstruktioista esiteltiin jo
alkusijainnin yhteydessä luvussa [7](alkusijainti.html#ajanilmaukset-ja-alkusijainti).
Keskisijainti vaikuttaisi monessa mielessä olevan melko neutraali -- monin
paikoin siinä havaittavat erot ovat tulkittavissa seurauksiksi
venäjän taipumuksesta käyttää alkusijaintia. Lähes peilikuvamaisesti voidaan
kuitenkin myös havaita, että suomessa vaikuttaisi olevan voimakkaampi taipumus
käyttää loppusijaintia kuin venäjässä.

## Samansuuntaisia S4-vaikutuksia {#s4-samans}

Luvussa [5.3](ajanilmaukset_tutkimusaineistossa.html#sij-yleisk) esitettiin, että
loppusijaintiin sijoittuu kaikkiaan 
 86 737 tutkimusaineiston
suomenkielisestä lauseesta 
 30 811
eli 35,52 prosenttia  
ja kaikkiaan 73 302 tutkimusaineiston 
venäjänkielisestä lauseesta
 6 734 
 eli 9,19
prosenttia. Havaittavat erot muistuttavat alkusijainnin kohdalla havaittuja,
joskin tässä tapauksessa venäjä on kieli, jossa tarkastelun kohteena oleva
sijainti on harvinainen.


Kuten alku- ja keskisijainnin tapauksessa, lähden myös loppusijainnin kohdalla liikkeelle koko
aineiston tasolla havaittavista vaikutuksista. Semanttisen funktion osalta
koko aineiston tasolla havaittavat yleiset vaikutukset on 
kerätty kuvioon 43.


![\noindent \headingfont \small \textbf{Kuvio 43}: Semanttisten funktioiden vaikutus loppusijaintiin \normalfont](figure/functs4-1.svg)

Kuvio 43 osoittaa, että teelisellä semanttisella
funktiolla on voimakkain koko aineiston tasolla S4-sijaintia kasvattava
vaikutus. Ymmärrettävää kyllä, presuppositionaalinen funktio taas näyttäisi
kielestä riippumatta pienentävän loppusijainnin todennäköisyyttä. Lisäksi
vaikuttaisi siltä, että ainakin tapaa ilmaiseva, sekventiaalis--duratiivinen sekä
mahdollisesti duratiivinen funktio
kasvattavat ja simultaaninen, sekventiaalinen ja resultatiiviset funktiot
vähentävät loppusijainnin todennäköisyyttä
koko aineiston tasolla. Jos kuviota 43 kuitenkin vertaa
kuvioon 47 alempana, on todettava, että ainoastaan
teelisen funktion vaikutus on käytännössä kielestä riippumaton.

Morfologinen rakenne osoittaa loppusijainnin osalta yhden selvästi koko
aineistolle yhteisen tendenssin, joka näkyy kuviossa 44:
muut kuin adverbiset ilmaukset ovat lähtökohtaisesti todennäköisempiä S4-sijoittujia.


![\noindent \headingfont \small \textbf{Kuvio 44}: Morfologisen rakenteen vaikutus loppusijaintiin \normalfont](figure/morphs4-1.svg)

Kuviosta 44 tehtävä havainto voidaan vahvistaa
kielen ja morfologisen rakenteen välistä interaktiota esittävästä kuviosta
 48, jonka mukaan nominaalisuudella ja kielellä
kyllä on yhteisvaikutus (suomessa nominaalisuuden S4-todennäköisyyttä
kasvattava vaikutus on venäjää pienempi), mutta se ei ole kovin suuri.
Nominaalisuuden vaikutuksiin koko aineiston tasolla pureudutaan
tarkemmin alaluvussa [9.1.1](loppusijainti.html#loppus_np).


Kuten tähänkin asti, positionaalisuuden vaikutukset ovat melko pieniä. Verrattuna alku- ja keskisijaintiin
on kuitenkin todettava, että loppusijainnissa on muita sijainteja vähemmän havaittavissa
koko aineistolle yhteistä tendenssiä, mikä näkyy myös kuviosta 45.


![\noindent \headingfont \small \textbf{Kuvio 45}: Positionaalisuuden vaikutus loppusijaintiin \normalfont](figure/poss4-1.svg)


Myös numeraalin läsnäololla koko aineiston tasolla havaittavat vaikutukset
ovat pieniä, kuten seuraava kuvio osoittaa:


![\noindent \headingfont \small \textbf{Kuvio 46}: Numeraalin läsnäolon vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/isnumerics4-1.svg)

Tarkastelen nyt lähemmin yllä esitetyn yleiskatsauksen perusteella selvimmiltä näyttäviä
koko aineiston tasolla havaittavia vaikutuksia. Lähden liikkeelle nominaalisuuden S4-
vaikutuksesta, minkä jälkeen tarkastelen lyhyesti semanttisten funktioiden tasolla
havaittavia koko aineiston laajuisia vaikutuksia.

### Nominaalisuus ja loppusijainti {#loppus_np}

Nominaalisuuden kielestä riippumattomalle linkittymiselle loppusijaintiin
voidaan ainakin lähtökohtaisesti esittää selvä informaatiorakenteellinen selitys, joka liittyy
siihen, että lauseen loppu on niin suomessa, venäjässä kuin yleisemmälläkin tasolla 
oletusasema fokuksen diskurssifunktiota toteuttaville elementeille [@tobecited]. 
Lisäksi voidaan väittää, että monille adverbeille
fokus on epätyypillinen tai jopa lähtökohtaisesti mahdoton diskurssifunktio, mistä
seuraa, että ne esiintyvät harvemmin lauseen lopussa. 

Esimerkit @ee_hessu -- @ee_vsredu kuvaavat tyypillisiä suomalaisia ja venäläisiä
fokaalisia ajanilmauksia:

(@ee_hessu) Aikaisempien kesien tapaan Hessu hakee mökkeilijät Reposalmen
rannasta tiistaisin, torstaisin ja perjantaisin klo 10. (Araneum Finnicum:
mieto.fi)
(@ee_akateeeminen) Akateeminen Kirjakauppa on jakanut suosituimman
esikoisteoksen palkinnon vuodesta 1990 lähtien. (Araneum Finnicum:
stockmanngroup.fi)
(@ee_valtuusto) Mikkelin kaupungin valtuusto hyväksyi esityksen
järjestyssäännöstä kuudes toukokuuta vuonna 1996. (Fipress: Länsi -Savo)
(@ee_grazhdanstvo) Я получил российское гражданство в 1998 году. (Araneum
Russicum: korrespondent.net) 
(@ee_rimljane) Богатые римляне посещали баню два раза в день. (Araneum
Russicum: forest-stroy.ru)
(@ee_vsredu) Объединенный ВС начал работу в среду. (Araneum Russicum: rapsinews.ru)

Luvussa [3.2.2](ajanilmaukset_osana_diskurssia.html#diskurssifunktio) esitetyn Lambrechtin [-@lambrecht1996, 213]
määritelmän mukaan fokus on se osa propositiota, jonka suhteen pragmaattinen
väittämä eroaa pragmaattisesta olettamasta. Otan yllä esitetystä fokaalisten
aineistoesimerkkien listasta lähempään tarkasteluun virkkeet @ee_hessu ja
@ee_grazhdanstvo. Niihin liittyvät pragmaattiset olettamat ja väittämät on
esitetty taulukossa 13:


--------------------------------------------------------------------------------------------
lause                         olettama                       väittämä                       
----------------------------- ------------------------------ -------------------------------
*Aikaisempien kesien tapaan   Lukija tietää erisnimen        Mökkiläisten hakeminen         
Hessu hakee mökkeilijät       *Hessu* referentin. Hessulla   tapahtuu tiistaisin,           
Reposalmen rannasta           on tapana kuljettaa            torstaisin ja perjantaisin     
tiistaisin, torstaisin ja     mökkeilijöitä                  kello 10. Mökkiläiset haetaan  
perjantaisin klo 10.*                                        Reposalmen rannasta            

*Я получил российское         lukija tietää pronominin *Я*   pronominin *я* referentistä    
гражданство в 1998 году.*     referentin. pronominin *Я*     tuli venäjän kansalainen       
                              referentti on venäjän          vuonna 1998                    
                              kansalainen, mutta ei ole                                     
                              ollut aina, vaan on joskus                                    
                              saanut kansalaisuuden                                         
--------------------------------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 13}: Esimerkkien @ee_hessu ja @ee_grazhdanstvo pragmaattiset olettamat ja väittämät \normalfont \vspace{0.6cm}

Kuten taulukosta nähdään, niin esimerkissä @ee_hessu kuin esimerkissä @ee_grazhdanstvo 
ajanilmauksen referentti on osa pragmaattista väittämää. Vastaavalla tavalla voitaisiin
analysoida myös esimerkit @ee_akateeeminen, @ee_valtuusto, @ee_rimljane ja @ee_vsredu.
Kaikissa näissä tapauksissa ajanilmauksen voisi nähdä osana *fokaalista S4-konstruktiota*,
jonka esitän tarkemmin seuraavassa matriisissa:


<div class="outerbox cxg"><div class="containerbox"><div class="containerbox"><div class="outerbox"><div class="statusline"><div class="featureline"><div class="fleft">ds</div><div>act+</div></div></div><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">syn</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">cat</div><div>V</div></div></div></div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>obj</div></div></div></div></div></div><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">prag</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">df</div><div>Foc</div></div></div></div></div></div></div></div></div>


 \noindent \headingfont \small \textbf{Matriisi 20}: Fokaalinen konstruktio \normalfont \vspace{0.6cm}

<!--Jotenkin tuo esille kakkosluvussa se ajatus, että 
silloin, kun ei ole mielekästä määritellä, diskurssistatusta
ei merkitä ja jos taas df ei ole oleellinen, sitä ei merkitä.
Kyse kun on kuitenkin referenttien välisistä suhteista ym.
vai??

venäjän fokaaliset konstr. alt.loc - arvolla?

-->

<!--VSO-huomautus-->

Olennaista matriisissa 20 on se, että se esittää koko ajanilmausta
edeltävän proposition olevan diskurssistatukseltaan aktiivinen.
Taulukon  esimerkkien osalta voitaisiin yleistäen todeta,
että propositiot [Hessulla on tapana kuljettaa mökkiläisiä]
ja [puhuja on joskus saanut venäjän kansalaisuuden] ovat vastaanottajan tiedossa, ja 
ajanilmauksen tehtävänä on täydentää tätä tietoa ajallisella informaatiolla.

Vaikka kaikki edellä esitetyt esimerkit ovat olleet nominaalisia,
myös adverbit voivat esiintyä osana fokaalista konstruktiota. Tästä ovat
esimerkkejä virkkeet @ee_saratov ja @ee_lautakunta, jotka kumpikin edustavat
aineistoryhmää F3c:

(@ee_saratov) Саратов, к сожалению, радует своих жителей чрезвычайно редко. (Araneum Russicum: fn-volga.ru)
(@ee_lautakunta) Erityisesti alkuaikoina lautakunta muutti ministeriön päätöstä erittäin harvoin. (FiPress: Helsingin Sanomat)

<!--Mieti, pitäisikö enemmän puhua "suomen ja venäjän" / "venäjän" fokaalisesta konstruktiosta ym.-->




Syy siihen, miksi adverbit kaiken kaikkiaan ovat nominaalisia ajanilmauksia
epätodennäköisempiä sijoittumaan S4-asemaan, on kuitenkin siinä, että tietyt
adverbiset ryhmät ovat luonteeltaan lähtökohtaisesti ei--fokaalisia. 
Kuten kuvio 43 osoittaa,
selkein tällainen ryhmä ovat presuppositionaaliset adverbit *jo/уже* ja
*vielä/ещё* eli aineistoryhmät P1 ja P2. Esimerkiksi P1-ryhmän tapauksessa
S4-sijaintiin osuu
 2 124 suomenkielisestä
tapauksesta 60
ja 2 534 
venäjänkielisestä tapauksesta  tasan 2.

Muita lähes täysin S4-sijaintia välttäviä adverbisiä aineistoryhmiä
ovat molemmissa kielissä ryhmät L7c -- jonka alhaiseen S4-todennäköisyyteen
pureuduttiin edellä osiossa [8.2.2.3](keskisijainti.html#l7l8-erot-kesk) -- sekä F3e (ks. osio
[5.2.3.3](ajanilmaukset_tutkimusaineistossa.html#f3a-f3b-f3c-f3d-f3e)). Venäjässä, jossa S4-sijainti on kaikkiaan
harvinaisempi, on yhteensä 8 sellaista
adverbista aineistoryhmää, joissa S4-sijainnin osuus on alle kaksi prosenttia.
Jo mainittujen lisäksi näitä ovat E5b (*вдруг*), F3a (*часто*), F3b (*всегда*)
ja F3d (*иногда*). Sen, että suomessa näiden ryhmien S4-osuudet ovat selkeästi
suurempia, voi nähdä viittaavan a) siihen, että venäjässä juuri fokaalinen konstruktio
on selkein syy käyttää S4-asemaa, mutta suomessa S4-konstruktioiden kirjo on laajempi
ja b) siihen, että adverbien taipumus painottua juuri yhteen tiettyyn asemaan
(venäjässä S2, suomessa S3) on venäjässä voimakkaampi.
Jälkimmäisestä tendenssistä on esimerkkinä  virke
@ee_tietoyht, joka sisältää objektin jälkeisen *usein*-adverbin:

(@ee_tietoyht) Poliitikot pitävät tietoyhteiskuntaa liian usein vain
tekniikka-asiana. (FiPress: Turun Sanomat)

Informaatiorakenteen kannalta esimerkistä @ee_tietoyht havaitaan, että niin
poliitikot kuin heidän suhtautumisensa tietoyhteiskuntaan voidaan nähdä
asioina, jotka oletettavasti jo ovat lukijan mielessä ja siten osa
pragmaattista olettamaa. Se, että suhtautumisena on juuri tietoyhteiskunnan
pitäminen tekniikka-asiana, on pragmaattisen väittämän osa-aluetta ja siten on
loogista, että siihen pyritään viittaamaan aivan lauseen lopussa. 
Kuten edellä luvussa [8.2.1](keskisijainti.html#s2s3-pien) todettiin, verbinjälkeinen asema on
kuitenkin suomessa altis esimerkiksi kontrastiivisille tulkinnoille, jos siihen
kuuluu useampia konstituentteja [@manninen2003, 226]. Jos *liian usein*
-adverbiaalilla halutaan vain ilmaista proposition [poliitikot pitävät
tietoyhteiskuntaa tekniikka-asiana] taajuus, on sille selkein sijainti
lauseessa koko pragmaattiseen väittämään kuuluvan aineksen jälkeen, kuten
esimerkissä @ee_tietoyht. Tällaista ajanilmauksen sisältävää
konstruktiota, jossa verbin valenssiin kuuluu useampi argumentti,
voitaisiin kuvata seuraavan kaltaisella matriisilla:


<div class="outerbox cxg"><div class="containerbox"><div class="containerbox"><div class="outerbox"><div class="statusline"><div class="featureline"><div class="fleft">ds</div><div>act+</div></div></div><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">syn</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">cat</div><div>V</div></div><div class="featureline"><div class="fleft">val</div><div>#1,#2</div></div></div></div></div></div><div class="node"><div class="featureline"><div class="fleft">#1</div><div> </div></div><div class="featureline"><div class="fleft">gf</div><div>obj</div></div></div></div></div></div><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>aika</div></div></div><div class="node"><div class="featureline"><div class="fleft">#2</div><div> </div></div><div class="featureline"><div class="fleft">prag</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">df</div><div>foc</div></div></div></div></div></div></div></div></div>


 \noindent \headingfont \small \textbf{Matriisi 21}: Monivalenssinen S4-konstruktio \normalfont \vspace{0.6cm}


Matriisi 21 esittää tilannetta, jossa verbin valenssiin kuuluu
objektin lisäksi jokin toinen (tässä tarkemmin määrittelemätön) argumentti (indeksi #2),
jonka diskurssifunktiona on fokus. Tällaisten fokuskonstituenttien on erittäin
todennäköistä (kielestä riippumatta) sijoittua koko lauseen viimeiseksi. Jos
ensimmäinen argumentti (indeksi #1) taas liittyy osaksi diskurssistatukseltaan
aktiivista propositiota, on puolestaan todennäköistä, että se sijaitsee heti
verbin jäljessä. Koska suomessa S2 on kieliopillisesti käytöltään rajoittunut
asema, jäävät ajanilmauksen sijaintivaihtoehdoiksi toisaalta sijainti objektin jäljessä (eli
tämän tutkimuksen viitekehyksessä S4) mutta kuitenkin ennen varsinaista
fokuksen funktiota toteuttavaa verbin argumenttia,[^advoma] toisaalta alkusijainti,
joka kuitenkin tässä tutkimuksessa aiemmin (ks. esim. luku [7.2.2.4](alkusijainti.html#f-funktiot))
tehtyjen havaintojen valossa johtaisi suomessa affektiiviseen tulkintaan.

[^advoma]: Adverbiaalin oman diskurssifunktion määrittely on tässäkin kohdassa
hankalaa, ja koska se ei ole tässä käsitellyn konstruktion kannalta oleellista, 
jätän sen ilmaisematta. Esimerkin @ee_tietoyht osalta ajanilmaus olisi nähdäkseni
melko luontevaa tulkita sekin osaksi fokusta. 


Koska venäjässä tyypillinen adverbisijainti on ennen verbiä, 
matriisissa 21 kuvatun kaltaisia tilanteita
ei juuri pääse syntymään: verbillä voi hyvin olla useita argumentteja ja niiden
järjestys voi määräytyä informaatiorakenteen periaatteiden mukaisesti, 
mutta adverbinen ajanilmaus sijoittuu standardinomaisesti S2-asemaan, eikä
pääse edes potentiaalisesti vaikuttamaan jonkin argumentin tulkitsemiseen
esimerkiksi kontrastiivisesti. Tämän takia ne melko harvat venäjän adverbiryhmiin
liittyvät S4-tapaukset, joita aineistossa on, ovat useimmiten tilanteita, joissa
itse adverbi todella toteuttaa selvästi juuri fokuksen funktiota, kuten
seuraavassa:

(@ee_meshal)-- Ты мешал мне слишком часто и слишком долго. (RuPress: Комсомольская правда)

Esimerkissä @ee_meshal on kyseessä tavallinen matriisissa 20 
kuvattu fokaalinen konstruktio.

### Teelinen semanttinen funktio

Edellä tehty yleiskatsaus aineistoissa havaittaviin samansuuntaisiin
vaikutuksiin osoitti, että nominaalisten ilmausten lisäksi 
teelinen semanttinen funktio eli aineistoryhmä E1c (ks. osio [5.2.2.1](ajanilmaukset_tutkimusaineistossa.html#e1a-e1b-e1c)) erottuu muista
siinä, että se näyttäisi kielestä riippumatta kasvattavan loppusijainnin todennäköisyyttä, ja vieläpä
erittäin voimakkaasti.
Esimerkit @ee_postitoimilupa ja @ee_skladskoe kuvaavat tyypillisiä lauseenloppuisia E1c-tapauksia:

(@ee_postitoimilupa) Valtioneuvosto myönsi pääkaupunkiseutua koskevan
postitoimiluvan yritykselle kolmeksi vuodeksi maaliskuussa 1997. (FiPress:
Kaleva)
(@ee_skladskoe) Мы отдали это складское помещение в аренду на 18 лет с
подписанием инвестиционного соглашения. (RuPress: Новый регион 2)

<!--Jotain verbigrafiikkaa?-->

Suomenkielinen esimerkki @ee_postitoimilupa on siinä suhteessa
mielenkiintoinen, että se sisältää itse asiassa kaksi ajanilmausta: teelisen *kolmeksi vuodeksi*
ja simultaanisen *maaliskuussa 1997*. Informaatiorakenteen kannalta 
jälkimmäinen ajanilmaus on tulkittavissa osaksi fokaalista konstruktiota, mutta samaa ei 
mitenkään yksiselitteisesti voi sanoa edellisestä ajanilmauksesta. Vaikuttaisikin siltä, että 
niin suomessa kuin venäjässä E1c-ryhmän ajanilmaukset määrittävät harvoin koko
lausetta niin kuin esimerkin @ee_postitoimilupa jälkimmäinen ajanilmaus, vaan
ovat selkeämmin sidoksissa nimenomaan tiettyihin verbeihin tai jopa
substantiiveihin.
Asia käy hyvin ilmi, kun tarkastellaan E1c-aineistojen  yleisimpiä pääverbejä, jotka on koottu
taulukkoon 14:

\FloatBarrier

\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
headverb & Freq & osuus\\
\midrule
tuomita & 301 & 14.24\\
saada & 193 & 9.13\\
valita & 121 & 5.72\\
antaa & 62 & 2.93\\
lomauttaa & 61 & 2.89\\
viedä & 58 & 2.74\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
headverb & Freq & osuus\\
\midrule
получить & 43 & 3.56\\
продлить & 43 & 3.56\\
пережить & 24 & 1.99\\
оставить & 22 & 1.82\\
взять & 19 & 1.57\\
давать & 19 & 1.57\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 14}: Yleisimmät E1c-aineiston pääverbit suomessa ja venäjässä \normalfont \vspace{0.6cm}

\FloatBarrier

Taulukon 14 mukaan E1c-aineiston S4-tapauksista
peräti 14,24 % esiintyy *tuomita*-verbin määritteenä. Venäjässä
mikään yksittäinen verbi ei samalla tavalla hallitse koko E1c-aineistoa, mutta 
taulukossa esiintyvä verbit ovat yhtä kaikki hyvin samantyyppisiä. Suurimman osan E1c-tapauksista
voi kummassakin kielessä nähdä väljästi oman *teelisen ajanilmauskonstruktionsa* ilmentyminä.
Konstruktio on kuvattu matriisiin  :


<div class="outerbox cxg"><div class="containerbox"><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">syn</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">cat</div><div>V</div></div></div></div></div><div class="featureline"><div class="fleft">sem</div><div>tuomita,saada,antaa,valita...</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>obj</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>telic</div></div></div></div></div></div>

 \noindent \headingfont \small \textbf{Matriisi 22}: Teelinen konstruktio. \normalfont \vspace{0.6cm}


<!--Venäjässä myös prefiksit ym...-->

Teeliset ilmaukset ovat kaiken kaikkiaan melko homogeeninen ja selvästi omaksi joukokseen
erottuva ryhmänsä, jonka edustajat -- kuten taulukosta 14 
havaitaan -- ovat melko tiukasti sidoksissa tiettyihin verbilekseemeihin.
Teelisten ilmausten painottuminen loppusijaintiin käy puolestaan selvästi
ilmi taulukosta 15, jossa on esitetty kunkin sijainnin
suhteelliset osuudet suomen ja venäjän E1c-ryhmissä:



|   |S1   |S2/S3 |S4    |
|:--|:----|:-----|:-----|
|fi |0,28 |11,73 |87,98 |
|ru |4,63 |28,70 |66,67 |

\noindent \headingfont \small \textbf{Taulukko 15}: E1c-ryhmän jakautuminen eri sijainteihin \normalfont \vspace{0.6cm} 

Taulukossa nähtävä venäjän S4-osuus, 66,67 %, on itse asiassa
suurempi kuin missään muussa aineistoryhmässä. Kaiken kaikkiaan teelisistä ilmauksista
voidaan todeta, että niiden sijoittumisen ymmärtäminen on kohtalaisen mutkatonta eikä
juuri pidä sisällään kieltenvälisiä eroja.


## Eriäviä vaikutuksia {#s4-eri}

Aloitan kielen ja muiden selittävien muuttujien välisten
loppusijaintiin liittyvien interaktioiden tarkastelun edellisten lukujen tapaan
semanttisista funktiosta, joihin liittyvät interaktiot on esitetty kuviossa 
47. Kuten tähänkin asti, kuvio esittää
semanttisten funktioiden vaikutuksen suomessa suhteessa venäjään:


![\noindent \headingfont \small \textbf{Kuvio 47}: Semanttisten funktioiden ja kielen interaktio loppusijainnissa \normalfont](figure/langfuncts4-1.svg)

Selvin kuviosta 47 tehtävä havainto koskee duratiivista
semanttista funktiota, jonka yhteisvaikutus suomen kielen kanssa on muita
kuviossa esitettyjä vaikutuksia positiivisempi. Seuraavaksi voimakkaampia
vaikutuksia ovat tapaa ilmaiseviin ja presuppositionaalisiin ilmauksiin liittyvät, joskin
etenkin jälkimmäiseen liittyy huomattava määrä epävarmuutta. Myös F-funktiolla ja
sekventiaalis--duratiivisilla ilmauksilla suomen kieli näyttäisi kasvattavan S4-todennäköisyyttä
verrattuna venäjään.

Edellä luvussa[8.2.2.4](keskisijainti.html#muutf-s2s3-kasv) käsiteltiin jo venäjän kohdalla havaittavaa
sekventiaalisten ilmausten taipumusta sijoittua tavallista useammin
loppusijaintiin. Tämä taipumus -- jonka edellä selitettiin olevan seurausta
ennen kaikkea venäjän L6a-aineistoryhmän leksikaalisista erityispiirteistä 
-- näyttäytyy kuviossa 47 
suomen S4-aseman todennäköisyyttä pienentävänä tekijänä. Sekventiaalisten
ohella simultaaniset ilmaukset muodostavat toisen semanttisen funktion,
jonka kohdalla keskisijainnin todennäköisyys suomessa korostuu ja sitä kautta
loppusijainnin todennäköisyys venäjään nähden pienenee.
Näiden lisäksi on vielä mainittava ennen muuta kehyksiset resultatiiviset
ilmaukset, joiden todettiin luvussa [7.2.1.3](alkusijainti.html#kehres-s1) kasvattavan suomen
S1-todennäköisyyttä: S4-aseman suhteen vaikutus on päinvastainen.


Morfologisesta rakenteesta tehtiin kuvion 44 
perusteella jo se koko aineistoa koskeva havainto, että nominaaliset
ilmaukset ovat yleisesti ottaen todennäköisempiä S4-sijoittujia kuin
adverbiset. Tämän tulkittiin edellä olevan seurausta ennen kaikkea siitä,
että nominaaliset ajanilmaukset ovat tyypillisempiä fokaalisen
konstruktion ilmentäjiä kuin adverbiset. Jos katsotaan kielen ja
morfologisen rakenteen välistä interaktiota, havaitaan kuitenkin myös koko
lailla selvä suomeen liittyvä vaikutus, jonka mukaan deiktiset adverbit
kasvattavat S4-todennäköisyyttä verrattuna venäjään. Tämä havainto
käy ilmi kuviosta 48:

![\noindent \headingfont \small \textbf{Kuvio 48}: Morfologisen rakenteen vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/langmorphs4-1.svg)

Deiktisten ajanilmausten suhdetta sivuttiin edellä luvussa
[7.2.2.3](alkusijainti.html#simultaaninen-funktio) keskisijainnin
yhteydessä, kun pohdittiin syytä simultaanisen funktion ja ennen muuta L1-aineistoryhmien 
S2/S3-todennäköisyyttä kasvattavaan vaikutukseen suomessa. Alempana osiossa
[9.2.4](loppusijainti.html#deiktiset-adverbit-ja-positionaalisuus) samantyyppisten syiden 
havaitaan vaikuttavan myös loppusijainnin venäjää suurempaan suosioon.


Jo tarkasteltujen selittävien muuttujien lisäksi myös positionaalisuudella ja
numeraalin läsnäololla voidaan nähdä jonkinasteista kielestä riippuvaa
vaikutusta loppusijainnin todennäköisyyteen. Vaikutuksen suunta käy ilmi
kuvioista 49 ja 50:

![\noindent \headingfont \small \textbf{Kuvio 49}: Positionaalisuuden vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/pos.lang.s4-1.svg)


![\noindent \headingfont \small \textbf{Kuvio 50}: Numeraalin läsnäolon vaikutus loppusijaintiin suomessa verrattuna venäjään \normalfont](figure/langisnumerics4-1.svg)

Kuvion 49 perusteella voidaan sanoa, että suomessa
ajanilmauksen S4-todennäköisyys kasvaa, jos kyseessä on positionaalinen ilmaus.
Vastaavasti kuvio 50  viittaa siihen, että mikäli
lauseessa on jokin luvussa [7.2.1.3](alkusijainti.html#kehres-s1) esitetyn määritelmän täyttävä numeraali, 
ei S4 ole suomessa yhtä todennäköinen kuin muut sijainnit, joiden edellä
havaittiin kasvattavan ennen kaikkea alkusijainnin mutta myös jonkin verran keskisijainnin
todennäköisyyttä. Positionaalisuudesta tehty havainto on yhteydessä deiktisistä 
adverbeistä kuvion 48 perusteella tehtyyn havaintoon,
ja näitä kahta käsitelläänkin tarkemmin samassa yhteydessä, osiossa [9.2.4](loppusijainti.html#deiktiset-adverbit-ja-positionaalisuus).
Sitä ennen tarkastelen kuitenkin semanttisiin funktioihin liittyviä suomen
ja venäjän välisiä eroja, ennen muuta duratiivisia ilmauksia.


### Duratiivinen funktio

Duratiivisen funktion painottuminen suomenkielisessä aineistossa S4-sijaintiin
on suorastaan silmiinpistävää: kaikkiaan 5 700
duratiivisesta tapauksesta S4-asemaan sijoittuu suomessa 
 3 596
eli peräti 
63,09 
prosenttia. Venäjänkielisessä aineistossa, joka kokonaisuutena on suomenkielistä aineistoa pienempi, on
kuitenkin selvästi enemmän duratiivisia ilmauksia -- kaikkiaan 7 197 kappaletta --
joista S4-asemaan sijoittuu vain 493
eli 6,85
prosenttia.[^durselitys]
Duratiiviset ilmaukset muodostavat siis ensi katsomalta suomessa ja venäjässä radikaalisti erilaiset aineistot. 
Duratiivisen funktion S4-osuuksien vertautuminen muiden funktioiden vastaaviin osuuksiin näkyy kuviossa
51, jossa katkoviivalla on merkitty kummankin kielen eri semanttisten funktioiden 
keskimääräinen S4-osuus ja pylväillä duratiivisen funktion S4-osuus.


[^durselitys]: venäjän duratiivisen aineiston koko selittyy...


![\noindent \headingfont \small \textbf{Kuvio 51}: Duratiivinen funktio ja S4-osuus \normalfont](figure/durvert-1.svg)

Kuviosta 51 nähdään paitsi visuaalisesti kielten välinen
suuri ero S4-osuuksissa sinänsä, myös se, että venäjässä duratiivinen funktio
sisältää itse asiassa muihin funktioihin nähden keskimääräistä vähemmän
S4-tapauksia.

Duratiivisen funktion poikkeuksellisen voimakasta vaikutusta on syytä tarkastella lähemmin
vertaamalla keskenään eri duratiivisia aineistoryhmiä, joita ovat
 E1a, E2a, E2b, E6a, E6b ja E6c (ks. osiot [5.2.2.1](ajanilmaukset_tutkimusaineistossa.html#e1a-e1b-e1c)--[5.2.2.6](ajanilmaukset_tutkimusaineistossa.html#e6a-e6b-e6c)).
Kuvio 52 
näyttää sen, missä määrin aineistoryhmien koot vaihtelevat suomen ja venäjän välillä sekä sen,
minkä verran S4-tapausten osuus kussakin aineistoryhmässä vaihtelee. Lisäksi
kuvioon on merkitty adverbiset aineistoryhmät lihavoinnilla ja nominaaliset
kursiivilla.


![\noindent \headingfont \small \textbf{Kuvio 52}: Duratiivisten aineistoryhmien koko. Palkeissa oleva katkoviiva kertoo S4-tapausten määrän (S4-tapausten osuus on katkoviivan vasemmalle puolelle jäävä alue). Adverbiset aineistoryhmät on lihavoitu, nominaaliset taas kursivoitu. \normalfont](figure/dgsizes3c-1.svg)

Kuvio 52 osoittaa ensinnäkin sen, että
adverbiset E6-aineistoryhmät ovat venäjässä suurempia kuin suomessa.
E6b-aineistoryhmässä venäjänkielinen aineisto sisältää
 3 373 tapausta, suomenkielinen taas
 2 549 tapausta; E6a taas sisältää
 2 007 venäjän- ja 
 1 003 suomenkielistä tapausta.
Mainittujen adverbisten ryhmien lisäksi venäjänkielinen aineisto on
suomenkielistä laajempi myös E2b-aineiston kohdalla
(ilmaukset *koko viikon* ja *всю неделю*). Muissa kuvion 52 
nominaalisissa aineistoryhmissä sen sijaan suomenkielisiä tapauksia on  -- kuten koko tutkimusaineiston tasolla --
venäjää enemmän.

Vaikka eri aineistoryhmien koot tarkasteltavissa kielissä vaihtelevat, havainto
toisaalta suomen duratiivisten ilmausten painottumisesta S4-asemaan ja
toisaalta venäjän S4-aseman suhteellisesta harvinaisuudesta
pysyy samanlaisena kaikissa kuvion 52  
aineistoryhmissä. Yleiskuvan tästä suhteellisten osuuksien välisestä erosta voi
saada katsomalla kuvioon piirrettyjä katkoviivoja, jotka osoittavat sen, kuinka
monta S4-tapausta kussakin osa-aineistossa on. Esimerkiksi venäjän
 3 373 E6b-tapauksesta S4-asemaan sijoittuu ainoastaan 
 169 <!--Janko-viittauksia-->
ja 2 007 E6a-tapauksestakin vain 
 123, kun taas suomen E6b-aineistossa 
(yhteensä 2 549 tapausta)
S4-lauseita on 1 199 ja E6a-aineistossa (yhteensä 1003 tapausta)
 632. Erot ovat samansuuntaiset myös kaikissa nominaalisissa aineistoryhmissä, joista suurin, E1a,
sisältää 73 / 577 venäjän- ja 
980 / 1 123 suomenkielistä S4-tapausta.

Duratiivisten ilmausten painottuminen S4-asemaan suomessa on lähtökohtaisesti
helppo ymmärtää, sillä ainakin päällisin puolin vaikuttaisi siltä, että
duratiiviset ilmaukset ovat tyypillisesti osa viestin pragmaattista väittämää
ja diskurssifunktioltaan fokaalisia. Näin on muun muassa seuraavissa suomen
E1a-aineiston esimerkeissä:

(@ee_norman) Norman teki biografiaa päätoimisesti *kaksi vuotta* ja haastatteli kirjaansa varten suuren joukon Lennonin lähipiirin henkilöitä. (Araneum Finnicum: beatles.fi)
(@ee_komiteamiettii) Ensiksi komitea miettii sitä *kaksi vuotta*. (Fipress: Länsi-Savo)
(@ee_jkoski) Nevalainen on hoitanut suuren paperitehtaan kainalossa elävän Jämsänkosken ympäristövirkaa *pian yhdeksän vuotta*. (FiPress: Kaleva)

Esimerkeissä @ee_norman -- @ee_jkoski ajanilmaukset on luontevasti
analysoitavissa osaksi sitä pragmaattisen väittämän osaa, joka erottaa
väittämän ja olettaman toisistaan (vrt. luku [3.2.2.2](ajanilmaukset_osana_diskurssia.html#fokus)). Duratiivisten
ilmaisujen luontainen fokaalisuus ei kuitenkaan ole millään tavalla suomelle
spesifi ominaisuus, vaan pikemminkin universaali piirre, joka on aina läsnä,
kun kuvataan jonkin tapahtuman kestoa [@tobecited]. Itse asiassa esimerkiksi 
Janko [-@janko2001, 255--256] mainitsee venäjän *давно*-sanasta (aineistoryhmä E6b), että niin
kutsuttujen *yleisesti toteavien* verbien yhteydessä [ks. esim. shmelev 2000,
??] *давно*-sanan diskurssifunktio on aina reema. Ylipäätään *давно* ei Jankon mukaan
koskaan esiinny *täysiverisenä*, vaan <!--mainitse padutsheva--> korkeintaan
painottomana teemana (ks. myös luku [](paatelmat.html#topiikin-ja-fokuksen-tarkempi-erittely)).
Esimerkit @ee_smarkom -- @ee_brits kuvaavat S4-sijoittuvia fokaalisia *давно*-tapauksia:

(@ee_smarkom) -- Мы с Марком Рудинштейном ведем переговоры уже *давно.* (RuPress: Комсомольская правда)
(@ee_gollandskij) Голландский полковник Геррит Ветерхолт обхаживал меня уже *давно.* (Araneum Russicum: left.ru)
(@ee_brits) Британские ученые исследуют трудные дни уже *давно.* (Araneum Russicum: 66.ru)

Esimerkkien @ee_smarkom -- @ee_brits tapaan myös nominaaliset duratiiviset aineistoryhmät
sisältävät samalla tavoin fokaalisia S4-tapauksia kuin edellä annetut suomenkieliset
esimerkit. Seuraavat virkkeet kuuluvat venäjän E1a-ryhmään:

(@ee_naprasno) Мы ждали ее несколько дней, но напрасно. (Araneum Russicum: wmos.ru)
(@ee_presledoval) Кстати, его фотограф, которому очень нравилась моя музыка, преследовал меня восемь дней. (RuPress: Комсомольская правда)
(@ee_zemljak) Наш земляк пишет ее уже несколько лет. (RuPress: Комсомольская правда)

Kuvion 52 perusteella vaikuttaisi kuitenkin siltä,
että suomen ja venäjän välinen ero on suurimmillaan juuri nominaalisissa
aineistoryhmissä, joissa suomen S4-osuudet ovat kaikissa tapauksissa yli 70
prosenttia ja venäjän vastaavat osuudet alle 15 prosenttia.
Suomen E1a-aineistossa S4-aseman ulkopuolelle sijoittuu kaikkiaan vain
 143 tapausta 
 1 123:sta, ja toisaalta venäjän
vastaavassa aineistossa S4-asemaan sijoittuu vain 
 73  tapausta 
 577:stä. 




Kun suomen E1a-aineiston kaikkia keskisijaintitapauksia tutkii tarkemmin,
havaitaan, että ne voidaan jakaa neljään ryhmään. Ensinnäkin tapausten joukossa
on yhteensä 32 lausetta, joista todellisuudessa puuttuu
ajanilmauksesta erillinen objekti tai joissa koneellinen analyysi on tehnyt
virheen. Esimerkki @ee_urut kuvaa tällaista tapausta:

(@ee_urut) Kallion urkujen suunnittelu ja rakentaminen on vienyt *neljä vuotta* ja viisi miljoonaa. (Fipress: Länsi-Savo)

Toiseksi, 25 tapauksessa kyse on rakenteeltaan esimerkkejä @ee_annelisauli
ja @ee_artohalonen muistuttavista lauseista, jotka voisi lukea osaksi omaa *duratiivi + paikka* -konstruktiotaan:

(@ee_annelisauli) Anneli Sauli opiskeli aikoinaan kolme vuotta Berliinissä puhetekniikkaa, ja käyttää edelleen ääniharjoittelussa saksalaista metodia. (FiPress: Karjalainen)
(@ee_artohalonen) Arto Halonen on hoitanut nyt kolme vuotta Pohjois-Karjalassa läänintaiteilijan virkaa. (FiPress: Kaleva)

Duratiivi + paikka -konstruktio voidaan kuvata seuraavasti:

<div class="outerbox cxg"><div class="containerbox"><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">syn</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">cat</div><div>V</div></div></div></div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>aika</div></div><div class="featureline"><div class="fleft">funct</div><div>dur</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>paikka</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>obj</div></div><div class="featureline"><div class="fleft">prag</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">ds</div><div>act-</div></div><div class="featureline"><div class="fleft">df</div><div>foc</div></div></div></div></div></div></div></div></div>

 \noindent \headingfont \small \textbf{Matriisi 23}: Duratiivi + paikka -konstruktio. \normalfont \vspace{0.6cm}

Duratiivi + paikka -konstruktion erikoistapaus ovat lauseet, joissa lisäksi on
lauseenalkuinen lokalisoiva ajanilmaus kuten esimerkissä @ee_punkassa:

(@ee_punkassa) Armeija-aikana hän odotti kaksi viikkoa erikoispunkassa varusteita. (FiPress: Kaleva)

Kolmanneksi suomen E1a-aineiston keskisijaintiin sijoittuvista tapauksista voidaan erottaa
ne lauseet, joissa kyse on S2-sijoittuneesta relatiivilauseen ajanilmauksesta
kuten esimerkissä @ee_peggylipton:

(@ee_peggylipton) Norma Jenningsia esittää Peggy Lipton, joka *viisi vuotta* näytteli yhtä pääosaa suosikkisarjassa \"The Mod Squad\". (FiPress: Demari)

Lopuksi voidaan todeta, että edellisistä ryhmistä erottuvat lisäksi seuraavat tapaukset:

(@ee_juhlauhreja) Ja he söivät *seitsemän päivää* juhlauhreja ja uhrasivat yhteysuhreja ja ylistivät Herraa, isiensä Jumalaa. (Araneum Finnicum: evl.fi)
(@ee_oppilaat) Oppilaat juhlivat *kaksi päivää* ikimuistoista opettajaansa. (FiPress: Iltalehti)

Esimerkit @ee_juhlauhreja ja @ee_oppilaat -- joista ensimmäinen on lainaus
Raamatusta -- edustavat siitä harvinaista duratiivisten ilmausten
käyttötapaa, että ne on itse asiassa tulkittavissa diskurssifunktioltaan
ennemmin topikaalisiksi kuin fokaalisiksi. Kummassakaan virkkeessä kirjoittajan tavoitteena
ei ole ilmoittaa lukijalle jonkin lukijan tajunnassa jo edustetun tapahtuman kestoa. Pikemminkin 
molempien esimerkkien kirjoittajien voi nähdä kertovan jotakin siitä, miten lauseiden subjektit
ovat viettäneet ajanjakson X (*seitsemän / kaksi päivää*). Nimitän tällaista 
rakennetta *topikaaliseksi duratiiviseksi konstruktioksi*, josta esitän seuraavanlaisen
formaalin kuvauksen:

<div class="outerbox cxg"><div class="containerbox"><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">syn</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">cat</div><div>V</div></div></div></div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>aika</div></div><div class="featureline"><div class="fleft">prag</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">df</div><div>top,aff</div></div></div></div></div><div class="featureline"><div class="fleft">funct</div><div>dur</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>obj</div></div></div></div></div></div>

 \noindent \headingfont \small \textbf{Matriisi 24}: Topikaalinen duratiivinen konstruktio \normalfont \vspace{0.6cm}

Huomattakoon, että esimerkkien @ee_juhlauhreja -- @ee_oppilaat tapauksissa syy
siihen, että kirjoittaja viestii tietoa siitä, miten lauseiden subjektit ovat
jonkin ajan viettäneet, on tämän ajanviettotavan tai käytetyn ajan määrän
epätavallisuudessa. Tässä mielessä ilmaukset vertautuvat edellä esimerkiksi 
luvussa   [7.2.2.4.1](alkusijainti.html#s1-f1a)
käsiteltyihin affektiivista S1-konstruktiota edustaviin tapauksiin kuten esimerkkeihin
@ee_kuolemalta ja @ee_synti. Myös näissä tapauksissa huomattiin, että esimerkit
olivat usein peräisin tyyliltään hyvin spesifeistä uskonnollisista
konteksteista. Luvussa [7.2.2.5](alkusijainti.html#sekventiaalis-duratiivinen-ja-duratiivinen-funktio)
todettiin suomen lauseenalkuisista duratiivisista ilmauksista, että
niihin useimmiten liittyy affektin diskurssifunktio.

Ainakin erityisen vähän keskisijaintitapauksia sisältävän E1a-aineistoryhmän perusteella
voidaan siis sanoa, että syyt suomen duratiivisten ilmausten  sijoittumiselle muuhun kuin S4-asemaan
ovat koko lailla rajatut. Venäjän E1a-aineistoa tutkittaessa taas havaitaan, että
se on käytöltään selvästi laajempi. Olennaisin ero suomen ja venäjän E1a-aineistoissa
vaikuttaisikin olevan se, että venäjässä keskisijaintiin sijoittuu tapauksia, joiden
on melko suoraviivaista tulkita ilmentävän fokaalisen konstruktion S2-versiota. 
Kuvaan fokaalista S2-konstruktiota seuraavalla matriisilla:

<div class="outerbox cxg"><div class="containerbox"><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">#1</div><div> </div></div><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">prag</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">df</div><div>focus</div></div></div></div></div><div class="featureline"><div class="fleft">sem</div><div>aika</div></div></div></div><div class="containerbox"><div class="outerbox"><div class="statusline"><div class="featureline"><div class="fleft">#1</div><div> </div></div></div><div class="statusline"><div class="featureline"><div class="fleft">ds</div><div>act+</div></div></div><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">cat</div><div>V</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>compl</div></div></div></div></div></div></div></div>


\noindent \headingfont \small \textbf{Matriisi 25}: Fokaalinen S2-konstruktio. \normalfont \vspace{0.6cm} 

Matriisissa 25 verbi ja sen täydennys
sekä subjekti viittaavat matriisien 19 ja
20 tapaan johonkin 
sellaiseen propositioon, joka viestin vastaanottajalla oletetaan
diskurssistatukseltaan aktiiviseksi. Tätä yhteenkuuluvuutta on merkitty
indeksillä #1. Ajanilmauksen diskurssifunktioksi on, aivan kuten 
edellä käsitellyissä fokaalisen konstruktion S1- ja S4-versioissa, 
merkitty fokus.

Venäjänkielisessä aineistossa tavattavista fokaalisista S2-konstruktioista
esimerkkejä ovat muun muassa seuraavat tapaukset:

(@ee_dobroserdov) Добросердов *почти семь лет* командовал дивизией. (Araneum Russicum: delostalina.ru)
(@ee_palomniki) Дарья Васильевна Спевякина *вот уже семь лет* возит людей в паломнические поездки. (Araneum Russicum: naslednick.ru)

Esimerkki @ee_dobroserdov on informaatiorakenteeltaan esimerkkiä @ee_palomniki selvempi: lauseen subjektiin
viitataan pelkällä sukunimellä, joten kirjoittaja katsoo sen oletettavasti lukijan mielessä aktiiviseksi, 
ja myös командовать-verbin täydennys *дивизией* esiintyy lauseessa ilman minkäänlaisia selittäviä määritteitä, niin että on
syytä olettaa koko proposition [D. johti divisioonaa] olevan osa pragmaattista väittämää. Ajanilmauksen
tehtäväksi jää sen ilmaisemisen, kuinka kauan tämä lukijan tiedossa oletettavasti oleva asiaintila
on ollut voimassa. Esimerkissä @ee_palomniki sen sijaan
niin lauseen subjekti kuin tästä kertova propositiokin vaikuttavat ensi silmäyksellä
aktivoimattomilta tiedoilta. Esimerkin laajempi konteksti kuitenkin osoittaa, ettei
asia ole näin:

<div class="quote"><p> Нет, наверное, в России сейчас ни одного православного, который бы ни разу ни участвовал в паломничестве или не мечтал о нём. Спокон веку люди на Руси ко святым местам ходили. Но традиция прервалась, как надо – забыли, а как правильно – не знаем. Вот и решили обратиться к знающему человеку. Дарья Васильевна Спевякина вот уже семь лет возит людей в паломнические поездки.</p><p> http://www.naslednick.ru/archive/rubric/rubric\_531.html</p></div>

Esimerkin @ee_palomniki laajemmasta kontekstista käy ilmi, että kyseessä on ortodoksisessa
aikakausjulkaisussa ilmestynyt pyhiinvaellusta käsittelevä artikkeli, jonka ingressissä esimerkki @ee_palomniki
esiintyy. Ennen varsinaista esimerkkiä ingressissä on esitelty lukijalle, että jutussa tullaan käsittelemään
pyhiinvaelluksia ja virke *Вот и решили обратиться к знающему человеку* esittelee esimerkin @ee_palomniki subjektin.
Varsinaisen esimerkkivirkkeen voi täten nähdä keskittyvän siihen, että edellisessä lauseessa
esitelty *pyhiinvaelluksista paljon tietävä henkilö* määritellään tarkemmin: kerrotaan
kyseisen henkilön nimi ja se, kuinka laaja hänen kokemuksensa asian parista itse asiassa on. Tässä mielessä
kyseessä todellakin on fokaalinen konstruktio, jossa ajanilmaus on osa pragmaattista väittämää.

Esimerkkiä @ee_palomniki muistuttaa myös esimerkki @ee_vplenu, jossa myös
tarkempi konteksti paljastaa, että kyseessä on lukijan tajunnassa oletettavasti
aktiivisena oleva tapahtuma, josta nyt kerrotaan tarkempia tietoja:

(@ee_vplenu) Преступники *шесть дней* держали 20-летнего парня в плену, требуя с его отца выкуп в три миллиона евро. (RuPress: Комсомольская правда)

Esimerkin @ee_vplenu laajempi konteksti on seuraavanlainen:

<div class="quote"><p>  Российские богачи вывозят детей за границу, чтобы их не похитили дома</p><p> Почему даже выпускник Школы КГБ мультимиллионер Евгений Касперский не смог уберечь сына от бандитов</p><p> «Все закончилось. ФСБ, Петровка и служба безопасности «Лаборатории» сделали невозможное. Я очень приятно потрясен их профессионализмом. Злодеи арестованы и дают показания. Всем работать!» - эту запись в интернет-дневнике знаменитый программист Евгений Касперский оставил после освобождения своего младшего сына Вани, похищенного в Москве 19 апреля. Преступники шесть дней держали 20-летнего парня в плену, требуя с его отца выкуп в три миллиона евро.</p><p>  https://www.kursk.kp.ru/daily/25677.3/836220/</p></div>

Laajempi konteksti osoittaa, että esimerkki @ee_vplenu on osa kidnappauksista
kertovaa artikkelia, jossa ennen varsinaista esimerkkivirkettä on
mainittu, että Jevgeni Kasperskyn poika oli ollut kidnapattuna mutta päässyt
vapaaksi. Esimerkki @ee_vplenu tarkentaa, kuinka kauan kidnappaus oli kestänyt
ja minkälaisia vaatimuksia pojan kidnappaajilla oli ollut. 

Kolmas samanlainen tapaus on esimerkissä @ee_baletom:

(@ee_baletom) Приняли нас обеих, и я несколько лет занималась балетом, несмотря
на то, что была довольно худой и не очень складной дылдой, как и все подростки
в этом возрасте. (RuPress: Комсомольская правда)

Myös tämän esimerkin laajempi konteksti osoittaa, että baletin harrastaminen on lukijan
tajunnassa aktiivisena oleva ajatus:

<div class="quote"><p> У меня была подружка - очень «танцевальная» девочка из интеллигентной семьи. Ее родителям посоветовали привести дочку на просмотр в балетную студию - ну и я пошла с ней за компанию. Приняли нас обеих, и я несколько лет занималась балетом, несмотря на то, что была довольно худой и не очень складной дылдой, как и все подростки в этом возрасте.</p><p> https://www.kompravda.eu/daily/25665/827046/</p></div>

Esimerkissä @ee_baletom kirjoittajan tavoitteena ei selvästikään ole välittää lukijalle 
tekstin subjektista, laulaja Jelena Presnjakovasta, propositiota [Presnjakova harrasti muutaman vuoden balettia],
vaan pikemminkin kertoa, että Presnjakovan viimeistään yhdyslauseen ensimmäisessä
lauseessa ('meidät molemmat valittiin') jo implikoitu balettiharrastus kesti muutaman vuoden.
Tämä huomio on oleellinen, kun mietitään syitä sille, miksi suomen duratiiviset ilmaukset
painottuvat keskisijainnin sijasta loppusijaintiin. Esimerkin @ee_baletom suomenkielisenä käännöksenä voisi
nimittäin hyvin olla virke @ee_baletfi1, mutta laajempi konteksti huomioon ottaen virke @ee_baletfi2
olisi vähintäänkin omituinen:

(@ee_baletfi1) Meidät molemmat hyväksyttiin, ja minä harrastinkin balettia muutaman vuoden, vaikka.. .
(@ee_baletfi2) Meidät molemmat hyväksyttiin, ja minä harrastinkin muutaman vuoden balettia, vaikka.. .

Esimerkin @ee_baletfi2 omituisuus johtuu siitä, että keskisijaintiin sijoitettu ajanilmaus
aiheuttaa lauseen tulkitsemisen niin, että kirjoittaja olettaa baletin harrastamisen olevan 
lukijan tajunnassa aktivoimaton. Jos koko edeltävä kappale on kuitenkin puhuttu nimenomaan baletin harrastamisesta,
esimerkki @ee_baletfi2 saa viestin vastaanottajan epäilemään, että kirjoittajalta puuttuu normaalissa vietinnässä
oleellinen kyky sovittaa sanomansa siihen, mitä kuulija oletettavasti tietää ja mitä ei [vrt. @hilpert2014, 111]. <!--TODO eri kohta, itse asiassa-->
Jos baletin harrastaminen on, kuten esimerkin @ee_baletom tapauksessa, diskurssistatukseltaan aktiivinen,
on loogisin paikka duratiiviselle ajanilmaukselle suomessa lauseen loppu. Tosin on todettava, että puhutussa
diskurssissa oikealla intonaatiolla voitaisiin epäilemättä saada aikaan ajatus baletin harrastamisen aktivoimattomuudesta vaikka
ajanilmaus sijaitsisikin S4-asemassa ja myös päinvastoin S3-sijoittuneesta ajanilmauksesta huolimatta baletti voitaisiin
prosodisin keinoin esittää aktivoituneena. Kirjoitetussa tekstissä S4 on kuitenkin se asema, joka oletuksena
tuo lukijalle esimerkin @ee_baletom kontekstissa vaadittavan ajatuksen baletin harrastamisen aktivoituneisuudesta.

<!--Laajenna näkemystä adverbisiin ja muihin dur.ryhmiin-->

E1a-ryhmän pohjalta tehtyjen havaintojen perusteella voidaan siis päätellä,
että oleellinen ero suomen ja venäjän välillä on siinä, miten venäjä käyttää
myös keskisijaintia fokaalisen konstruktion toteuttamiseen, kun taas suomessa
S4-sijainti on useimmiten ainoa vaihtoehto.
Edellä osiossa [9.1.1](loppusijainti.html#loppus_np) havaittiin, että S2-sijainti -- venäjän
standardiasema etenkin adverbeille -- on jossain määrin monikäyttöisempi
kuin suomen standardiasema, S3. Osiossa [9.1.1](loppusijainti.html#loppus_np) havaitut verbinjälkeiseen
asemaan liittyvät monitulkintaisuudet ja S2-aseman laajempikäyttöisyys ovat
epäilemättä taustalla myös kielten välillä duratiivisten ilmausten kohdalla
nähtävissä suurissa eroissa.

### Muut suomen S4-todennäköisyyttä kasvattavat semanttiset funktiot {#muutf-s4}

Kuviosta 47 nähtävät kielen ja semanttiseen funktion
väliset interaktiovaikutukset ovat selvästi muita suurempia duratiivisen
funktion kohdalla. Duratiivisen funktion ohella edellä nostettiin esille
suomen S4-todennäköisyyttä nostavina toisaalta tapaa ilmaiseva ja
presuppositionaalinen, toisaalta sekventiaalis--duratiivinen ja taajuutta
ilmaiseva funktio. Luon seuraavassa lyhyen katsauksen näiden vaikutusten
taustalla oleviin tekijöihin.



Kuten edellä mainittiin, tapaa ilmaisevaan funktioon ja loppusijainnin käyttöön
suomessa liittyviä erityispiirteitä käsiteltiin pääasiassa keskisijainnin
yhteydessä osiossa [8.2.1](keskisijainti.html#s2s3-pien). Presuppositionaalisten ilmausten taas
todettiin luvussa [8.1.2](keskisijainti.html#presup-sama-keski) olevan kielestä riippumatta erittäin
voimakkaasti keskisijaintiin painottuneita, niin että kummassakin kielessä
keskisijaintiin sijoittuu yli 90 prosenttia kaikista presuppositionaalisista
tapauksista. Suomessa on kuitenkin hivenen 
enemmän (3,82 % / 155) myös 
presuppositionaalisia S4-tapauksia
kuin venäjässä (0,30% / 9 kpl).
Ero on jäljitettävissä seuraavankaltaisiin suomenkielisen aineiston
*jo*-tapauksiin:

(@ee_nimeni) Tiesin, että hän tiesi nimeni jo. (Araneum Finnicum: fi.taikakoulu.wikia.com)
(@ee_starttasi) Hallitus starttasi oman partiosyksynsä jo ja odottaa innolla tulevaa kautta. (Araneum Finnicum: karka.partio.net)

Suomen lauseenloppuiset *vielä*-tapaukset sekä käytännössä
kaikki venäjän lauseenloppuiset presuppositionaaliset tapaukset ylipäätään
osoittautuivat aineistosta suodattumatta jääneiksi ei--temporaalisiksi
käyttötilanteiksi, kuten seuraavat esimerkit:

(@ee_satonsa) Parhaan laadun tuottajat rajoittavat satonsa vielä sallittuja määriä vähäisemmiksi. (Araneum Finnicum: peda.net)
(@ee_tsheka) Итак, главным правилом для увеличения среднего чека является активное предложение клиентам чего-то еще. (Araneum Russicum: kaprise-moscow.ru)

Esimerkki @ee_starttasi antaa viitteitä siitä, että  suomessa on tietyissä
tilanteissa mahdollista käyttää *jo*-sanaa itsenäisesti fokaalisen
S4-konstruktion ajanilmauksen paikalla: esimerkin *jo*-sanan tilalle olisi
helppo kuvitella jokin lokalisoiva simultaaninen ajanilmaus, esimerkiksi
*Hallitus starttasi oman partiosyksynsä viime maanantaina*. *Jo*-sanan tapauksessa
konstruktion välittämäksi informaatioksi muodostuu lopulta se, että
starttaaminen on tapahtunut jossain kohti menneisyyttä. Myös tässä ajanilmauksen
S4-sijainnin voi nähdä olevan seurausta siitä, että  ratkaisulla vältetään
S3-sijainnista mahdollisesti seuraavat monitulkintaisuudet kuten lauseen tulkitseminen
eri aloitettavien asioiden vertailuksi. 

Sekventiaalis--duratiivisista ilmauksista todettiin keskisijainnin tarkastelun yhteydessä
luvussa  [8.2.2.4](keskisijainti.html#muutf-s2s3-kasv), että koska alkusijainti on venäjässä näillä ilmauksilla
niin paljon suomea tavallisempi, nostaa kieli-muuttujan "suomi"-arvo sekä keski- että
loppusijainnin todennäköisyyttä. Sekventiaalis--duratiivisia keskisijaintitapauksia
tarkasteltaessa havaittiin, että osa 
suomen ja venäjän välisistä eroista selittyy sillä, että suomessa sekventiaalis--duratiivisiin
ilmauksiin liittyvät johdantokonstruktiot toteutuvat lähinnä S3-asemassa, siinä missä
venäjässä myös S1 on tavallinen. S4-sijaintia tutkittaessa on ensinnäkin todettava, että
niin suomessa kuin venäjässä sekventiaalis--duratiiviset ilmaukset sijoittuvat
siihen selkeästi keskimääräistä useammin. Tämä ilmi paitsi kuviosta 43 ,
myös taulukosta 16, jossa on esitetty suurimman
sekventiaalis--duratiivisen aineistoryhmän, L9d:n, jakautuminen eri sijainteihin:



|   |S1    |S2/S3 |S4    |
|:--|:-----|:-----|:-----|
|fi |2,69  |16,65 |80,67 |
|ru |26,88 |34,03 |39,09 |

\noindent \headingfont \small \textbf{Taulukko 16}: Suomen ja venäjän L9d-ryhmän jakautuminen eri sijainteihin \normalfont \vspace{0.6cm} 

S4-sijainnin suosio sinänsä on helppo selittää fokaalisen konstruktion tavallisuudella 
juuri L9d-aineistossa. Esimerkki @ee_akateeeminen kuvasi edellä tyypillistä suomen L9d-aineiston
fokaalista S4-tapausta, ja on syytä olettaa, että suurin osa niin kummankin kielen
S4-tapauksista on vastaavia fokaalisia tapauksia. Venäjänkielisestä aineistosta
voidaan löytää muun muassa seuraavat esimerkit:

(@ee_upravljaju) Сам я управляю транспортными средствами с 1969 года. (Araneum Russicum: my-ledimir.ru)
(@ee_rukovodil) Он руководил страной с 1999 года, то есть он потратил более одной пятой части своей жизни за рулем России. (Araneum Russicum: rusinform.ru)
(@ee_tska) Баскетбольный клуб ЦСКА ведет свою историю с 1924 года, когда в Центральном доме Красной Армии ( ЦДКА) была создана секция баскетбола. (RuPress: РИА Новости)
(@ee_patriot)"Патриот" ведет свою историю с 1925 года. (Araneum Russicum: patriot-izdat.ru)
(@ee_beton) В нашей стране исследование технологии производства ячеистого бетона в
едет свое начало с 1928 года. (Araneum Russicum: s-t-group.ru)
(@ee_skautskij) Это большой международный скаутский праздник, традиция проведения которого берет свое начало с 1932 года. (Araneum Russicum: kairblog.ru)


Esimerkit @ee_tska -- @ee_skautskij havainnollistavat venäjässä
tavallista *вести свою историю/начало с N года* -konstruktiota. Vastaavia rakenteita
kaikkiaan 394 venäjän
L9d-aineiston S4-tapauksesta on yhteensä
  77.
Tämänkin konstruktion yleisyys kuvastaa omalta osaltaan niitä käyttö- ja
merkityseroja, joita suomen ja venäjän L9d-ryhmien ilmausten välillä on. Olennaista
kuitenkin on ennen kaikkea se, että S4 on venäjän sekventiaalis--duratiivisilla
ilmauksilla yleinen juuri osana fokaalista konstruktiota.

Suomen ja venäjän S4-aineistojen välillä onkin mielenkiintoinen ero siinä,
että suomessa on eräitä loppusijaintitapauksia, joissa ajanilmauksella ei selvästikään
ole fokuksen diskurssifunktio. Näihin kuuluvat muun muassa seuraavat:

(@ee_lannenpuhelin) Lännen Puhelin on maksanut osinkoja omistajilleen vuodesta
1999 lähtien yhteensä 39 miljoonaa euroa, joka on 572 euroa osakkeelta.
(Araneum Finnicum: koulutushakemisto.com)
(@ee_erasmus) Yli kolme miljoonaa opiskelijaa on saanut Erasmus-apurahaa
vuodesta 1987 lähtien. (Araneum Finnicum: cimo.fi)
(@ee_norsktipping) Tällä tavoin ja päivän hintatasoon sovitettuna, Norsk
Tipping on lahjoittanut jopa 85 miljardia Norjan kruunua hyvään tarkoitukseen
vuodesta 1948 lähtien. (Araneum Finnicum: casinolehti.com)


Jos vaikkapa esimerkkiä @ee_erasmus ajattelisi tavanomaisena fokaalisen konstruktion
edustajana, olisi tulkittava, että koko lauseessa esitettäisiin lukijan tajunnassa
aktiiviseksi oletettu propositio [yli kolme miljoonaa opiskelijaa on saanut Erasmus-apurahaa]
ja tästä täydennettäisiin ajallinen lisätieto, nimittäin se, että (mahdollisesti samojen
opiskelijoiden) rahojensaantiprosessi on jatkunut vuodesta 1987. Tällainen tulkinta
on kuitenkin mahdoton: se edellyttäisi muun muassa sen, että subjektina olisi jokin aktiiviseen
referenttiin viittaava konstituentti, kuten lauseessa *Virtanen on saanut
Erasmus-apurahaa vuodesta 1987 lähtien.* Esimerkki @ee_erasmus samoin kuin muut kaksi
esimerkkiä onkin tulkittava niin, että kussakin tapauksessa oleellisin osa
pragmaattista väittämää on nimenomaan se määrä, josta kirjoittaja viestii.
Tapaukset ovatkin tulkittavissa määrää painottavan konstruktion S4-versioksi, joka voidaan
muodollisesti kuvata seuraavasti:

<div class="outerbox cxg"><div class="containerbox"><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">cat</div><div>V</div></div><div class="featureline"><div class="fleft">val</div><div>määrä,foc</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>obj</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>aika,rajattu ajanjakso</div></div></div></div></div></div>

\noindent \headingfont \small \textbf{Matriisi 26}: Määrää painottavan konstruktion S4-versio \normalfont \vspace{0.6cm} 

Vastaavasta S4-konstruktiosta venäjänkielisessä
aineistossa -- jossa S4-sijoituneen sekventiaalis--duratiivisen ajanilmauksen
ja sen lisäksi numeraalin sisältäviä tapauksia on kaikkiaan vain
 14
 kappaletta -- on viitteenä ainoastaan seuraava esimerkki:

(@ee_subjekty) 42 субъекта РФ получат эту возможность уже с июля этого года, а
федеральные ведомства уже в апреле. (Araneum Russicum: ocenkababenko.ru)

Esimerkki @ee_subjekty on kuitenkin siinä mielessä erilainen kuin edellä
esitetyt suomenkieliset tapaukset, että siinä ajanilmausta määrittää
presuppositionaalinen *уже*. Lauseen pragmaattinen olettama pitää sisällään
ajatuksen *tästä mahdollisuudesta* sekä oletettavasti myös sen, että mahdollisuus
koskee erilaisia hallinnollisia yksikköjä. Pragmaattiseen väittämään kuuluu ajatus
siitä, että tietyillä yksiköillä mahdollisuus tulee ajankohtaiseksi aikana T1, eräillä
muilla taas aikana T2. Ajanilmauksilla on siis lopulta fokaalinen diskurssifunktio
toisin kuin edellä luetelluissa suomenkielisissä esimerkeissä.


Tarkastelen vielä lyhyesti F-funktiota ilmaisevia tapauksia, joilla kuvion
47 mukaan on pieni suomen S4-todennäköisyyttä
kasvattava vaikutus. Jos katsotaan, millaisia S4-osuudet ovat eri F-funktiota edustavissa
aineistoryhmissä ja verrataan näitä lukemia koko kummankin kielen S4-osuuteen
koko aineiston tasolla, saadaan kuvion 53 mukainen
jakauma (vrt. S1-aineisto ja kuvio 24 edellä
luvussa [7.2.2.4](alkusijainti.html#f-funktiot)):


![\noindent \headingfont \small \textbf{Kuvio 53}: Erot F-funktiota edustavien aineistoryhmien S4-osuuksissa \normalfont](figure/ferojas4-1.svg)

Kuviosta nähdään, että suomen ja venäjän F-ryhmät käyttäytyvät melko lailla samansuuntaisesti
verrattuna koko aineiston tason S4-osuuksiin: ryhmät, jotka suomessa ovat keskimääräistä
tavallisempia S4-asemassa, ovat sitä myös venäjässä, ja sama koskee myös
keskimääräistä harvemmin S4:ään sijoittuvia aineistoryhmiä.

[KESKEN]

### Suomen S4-todennäköisyyttä pienentävät semanttiset funktiot {#fi-s4-pien}

Kuvion 47 perusteella kehyksinen resultatiivinen,
sekventiaalinen ja likimääräinen funktio pienentävät suomen S4-todennäköisyyttä venäjään
verrattuna selkeästi sekä näiden lisäksi simultaaninen ja varsinainen resultatiivinen
ekstensio jonkin verran. Kuvio  esittää kolmen selvimmin
kieliä erottavaan funktioon kuuluvien aineistoryhmien S4-osuudet verrattuna koko aineiston
keskiarvoon:

![](figure/unnamed-chunk-3-1.svg)

Tässä esitetyistä ryhmistä sekventiaalinen L6b vaikuttaisi noudattavan
koko lailla aineiston yleistä linjaa suomen ja venäjän välisissä S4-eroissa:
koko aineiston tasolla suomen S4-sijainnin suhteellinen osuus
on 3,87-kertainen venäjään verrattuna, ja L6b-ryhmän kohdalla vastaava
lukema on 3,49. Muissa kuviossa esitetyissä ryhmissä 
luvut ovat kuitenkin tavallista pienempiä: kehyksistä resultatiivisuutta edustavassa
E3a-ryhmässä 1,98, sekventiaalisessa L6a-ryhmässä 1,67,
likimääräisessä LM1-ryhmässä 1,21 ja toisessa kehyksisessä ryhmässä, E3b:ssä,
ainoastaan 1,16. Toisin sanoen, vaikka kaikissa tapauksissa suomen S4 on venäjän S4:ää
yleisempi, ovat erot pienempiä kuin koko aineiston tasolla, minkä
takia myös kuviossa 47 kehyksinen resultatiivisuus,
likimääräisyys ja sekventiaalisuus vähentävät S4-todennäköisyyttä suomessa
verrattuna venäjään.

Kehyksistä resultatiivisuutta käsiteltiin aiemmin alkusijainnin yhteydessä (osio [7.2.1.3](alkusijainti.html#kehres-s1)),
jolloin tutkittiin syitä sille, miksi nämä ilmaukset suomessa painottuvat 
tavallista useammin lauseen alkuun. Selkeimpänä syynä pidettiin 
määrää painottavaa S1-konstruktiota (matriisi 9 ).
Kuvion  mukaan toinen kehyksisen resultatiivisen
ekstension aineistoryhmistä, E3b, on venäjässä ja suomessa melkein yhtä yleinen
S4-sijoittuja. Esimerkit @ee_rahapelit -- @ee_slyshim valottavat lähemmin tässä
ryhmässä esiintyviä kummankin kielen S4-tapauksia:



(@ee_rahapelit) Kuitenkin vain 5, 5 prosenttia osavaltion 18-24-vuotiaista
pelasi rahapelejä netissä viime vuoden aikana. (Araneum Finnicum: pokeri.info)

(@ee_zhiljo) По оценкам представителей Минобороны, по состоянию на январь 2012
года в очереди за квартирами стояли 82 тысячи военнослужащих, из них 49 тысяч
семей уже получили жилье в течение прошлого года. (Araneum Russicum:
spb.gdeetotdom.ru)

Esimerkit @ee_rahapelit ja @ee_zhiljo osoittavat, että paitsi määrää
painottava S1-konstruktio, myös vastaava
S4-konstruktio on kehyksisillä resultatiivisilla ilmauksilla käytössä
molemmissa kielissä. Huomionarvoista kyllä, varsinaisia fokaalisia
E3b-tapauksia kummankaan kielen S4-aineistoista ei juurikaan ole löydettävissä.
Sen sijaan tyypilliset S3-tapaukset niin suomessa kuin venäjässäkin 
muistuttavat seuraavia lauseita:

(@ee_pontevasti) Hän on itse ajanut asiaa pontevasti kolmen viime vuoden aikana
ja on tyytyväinen siitä, että päätös tehtiin ennen kuin hän kesällä siirtyy
kulttuurineuvokseksi Ruotsin Washingtonin-suurlähetystöön. (Fipress:
Länsi-Savo)
(@ee_formanova) Formanova on lähestynyt tasaisesti maailman kärkeä viiden viime
vuoden aikana. (Fipress: Länsi-Savo)
(@ee_novatek) НОВАТЭК постепенно наращивает дивиденды в течение последних шести
лет. (RuPress: РБК Дейли)
(@ee_lazernyje) Лазерные проигрыватели, микрокомпьютеры, текстовые редакторы, микроволновые
печи, видеокамеры, магнитофоны, автомобильные кондиционеры существенно изменили
характер нашей работы и досуга в течение последних двадцати лет. (Araneum
Russicum: economics.wideworld.ru)
(@ee_palomnikov) В связи с ожидаемым наплывом паломников из-за рубежа власти
Саудовской Аравии уже оповестили жителей королевства, что те из них, кто уже
совершал хадж в течение последних пяти лет, в этом году право на паломничество
не получат. (RuPress: РИА Новости)
(@ee_slyshim) Мы слышим их от Израиля в течение последних восьми лет. (Araneum
Russicum: irn.ved.gov.ru) 

Lauseissa @ee_pontevasti -- @ee_slyshim ajanilmauksen diskurssifunktio on oikeastaan
ennemmin topikaalinen kuin fokaalinen. Nimitänkin näiden esimerkkien ilmentämää
konstruktiota *topikaaliseksi S4-konstruktioksi*, joka on tarkemmin kuvattu
matriisissa 27 (vrt. ei--fokaalinen konstruktio alempana 
matriisissa 28).

<div class="outerbox cxg"><div class="containerbox"><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">cat</div><div>V</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>obj</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>aika</div></div><div class="featureline"><div class="fleft">prag</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">df</div><div>topic</div></div></div></div></div></div></div></div></div>


\noindent \headingfont \small \textbf{Matriisi 27}: Topikaalinen S4-konstruktio. \normalfont \vspace{0.6cm}

Edellisen esimerkkiryppään lauseissa voi todella ajatella, että ajanilmauksella
on selkeä referentti (esimerkiksi viisi edellistä vuotta esimerkissä @ee_formanova), 
josta kirjoittaja kertoo jonkin proposition. Usein lauseissa on toinenkin topiikki,
subjekti, josta yhtä lailla kerrotaan (esimerkin @ee_formanova voi nähdä kertovan
sekä Formanovasta että viidestä viimeksi kuluneesta vuodesta). Esimerkissä @ee_lazernyje
kuitenkin kyse on ennemminkin kokonaan uudesta tapahtumasta, joka 
lukijalle kerrotaan, jolloin ainoa lukijalle jo tuttu ankkuri on ajanilmauksen
*в течение последних двадцати лет* referentti aikajanalla.

E3a- ja E3b-aineistojen referentiaalisuuteen liittyvä ero näkyykin mielenkiintoisella
tavalla siinä, että lähtökohtaisesti referentiaaliset ja menneisyyteen
viittaavat E3b-ilmaukset  sijoittuvat
kummassakin kielessä S4-asemaan koko lailla yhtä usein, kun taas ei--referentiaalisissa
ja usein tulevaisuuteen tai nykyisyyteen viittaavissa E3a-tapauksissa suomen S4-asema on lähes
kaksi kertaa niin suosittu kuin venäjän S4-asema (ks. kuvio  ). 
Toisin kuin E3b-lauseissa, E3a-aineiston S4-tapauksista suuri osa on fokaalisia, kuten
seuraavat:

(@ee_likvidnaja) Всякая ликвидная квартира сегодня находит жильца в течение недели. (RuPress: Комсомольская правда)
(@ee_hakemus) Hakemus rahojen saamiseksi on toimitettava puolen vuoden kuluessa
eli viimeistään tammikuun alkupuolella. (FiPress: Helsingin Sanomat)

Esimerkeissä @ee_likvidnaja ja @ee_hakemus ajanilmaus välittää nimenomaan pragmaattiseen
väittämään kuuluvan, lukijalta puuttuvan tiedon jonkin tapahtuman tai asiaintilan
voimassaolon kestosta. Suomenkielisen aineiston esimerkit @ee_pohjanmereen ja @ee_tatte
edustavat kuitenkin E3a-aineiston ei--fokaalisia S4-tapauksia, jollaisia
venäjän E3a-ryhmästä ei juuri ole löydettävissä:

(@ee_pohjanmereen) Kymmenen osallistujan Pohjanmeri - kokous kielsi perjantaina
kaikkien ympäristömyrkkyjen upottamisen Pohjanmereen 25 vuoden kuluessa. (FiPress: Karjalainen)
(@ee_tatte) Laulua varten Tatte kokosi valituksenaiheita tamperelaisilta tieteentekijöiltä kevään kuluessa. (
Araneum Finnicum: tatte.fi)

Toisin kuin edellä esitetyissä E3b-esimerkeissä, esimerkeissä @ee_pohjanmereen ja @ee_tatte on 
vaikea nähdä ajanilmauksia diskurssifunktioltaan selkeästi topikaalisina. Pikemminkin kyse on 
seuraavassa osiossa tarkemmin käsiteltävästä ei--fokaalisesti konstruktiosta 
(ks. matriisi 28 ), jonka käytöllä on paljon selitysvoimaa pohdittaessa
suomen ja venäjän välillä tavallisesti havaittavaan eroon S4-osuuksissa. Kuten 
kuvio  ja 
esimerkit @ee_pontevasti -- @ee_slyshim osoittavat, E3b-aineistossa suomen ja venäjän
S4-asemia käytetään kuitenkin hyvin pitkälle samantyyppisiin konstruktioihin, minkä tähden
tässä aineistoryhmässä kielten välinen ero on tavallista pienempi ja kehyksiset resultatiiviset
ilmaukset ylipäänsä nostavat venäjän S4-todennäköisyyttä.

E3b-ryhmän jälkeen toiseksi pienimmät erot suomen ja venäjän välillä havaittiin
kuvion  perusteella likimääräisessä LM1-ryhmässä.
Luvussa [7.2.1.2](alkusijainti.html#likim-s1) todettiin likimääräisten ilmausten muodostavan hyvin
pienen osa-aineiston (yhteensä 134 suomen-
ja158 venäjänkielistä tapausta), joka
eroaa myös leksikaaliselta merkitykseltään kielten välillä jonkin verran.
Kummassakin kielessä likimääräiset ilmaukset ovat keskimääräistä tavallisempia
S4-sijoittujia, mutta erityisen suuri ero keskimääräiseen on venäjässä, jossa
 50 % likimääräisistä ilmauksista saa
asemakseen S4:n. Venäjässä, jossa S4 on kauttaaltaan suomea harvinaisempi, likimääräisten
ilmausten voi tulkita korostuvan sen tähden, että ne etenkin 
venäjän *около * + genetiivi -rakenteissa ovat tavallista useammin
osana fokaalista konstruktiota, kuten seuraavassa esimerkissä:

(@ee_roditeli) Родители Быстрова покинули свадьбу около двух часов ночи. (RuPress: Комсомольская правда)

Suomessakin likimääräisten ilmausten funktiot S4-asemassa ovat koko lailla rajoittuneet fokaaliseen
konstruktioon, jota myös seuraava esimerkki ilmentää:

(@ee_kankkunen) Tommi Mäkinen, Juha Kankkunen ja kumppanit ylittävät Kangasalantien puoli kahden t
ietämillä. (FiPress: Kangasalan Sanomat)

Kehyksisen resultatiivisen ja likimääräisen funktion ohella kuviossa  
tarkasteltiin sekventiaalisia ajanilmauksia, jotka -- aivan kuten muutkin 
tässä käsitellyt semanttiset funktiot -- ovat siitä erityisiä, että
niiden havaittiin luvussa [7](alkusijainti.html#ajanilmaukset-ja-alkusijainti)
kasvattavan suomen S1-sijaintia suhteessa venäjään. Käytännössä 
relevantti S4-sijainnin kannalta on ryhmä L6a, jonka S4-osuus suomessa
on vain hivenen yli koko aineiston keskiarvon, mutta venäjässä 
taas koko aineiston keskiarvoon nähden enemmän kuin kaksinkertainen.
Kuten likimääräiset tapaukset, myös sekventiaaliset tapaukset 
ovat luonteeltaan sellaisia, että ne esiintyvät todennäköisesti
osana fokaalista konstruktiota, jota edustavat muun muassa
seuraavat tapaukset:

(@ee_dostavim) Мы доставим их в магазин через несколько дней. (Araneum Russicum: freska-auto.ru)
(@ee_betset) Elementtiasentajat aloittavat hommat kahden viikon päästä ja
Betset tekee viimeiset elementit valmiiksi tällä viikolla. (Araneum Finnicum:
uuteenkotiin.wordpress.com)

Esimerkit @ee_dostavim ja @ee_betset ovat samanlaisia fokaalisen konstruktion
ilmentymiä kuin esimerkit @ee_roditeli ja @ee_donrosa yllä.
L6a-aineistossa suomen ja venäjän välillä havaittava LM1- ja E3b-aineistoja
suuremman eron voi nähdä olevan peräisin esimerkin @ee_soininvaara ja @ee_baltia
kaltaisista tapauksista, joita suomenkielinen aineisto sisältää
ja joissa lauseen lopussa on paitsi ajan, myös paikanilmaus. Esimerkissä 
@ee_baltia ajanilmauksen voi oikeastaan ajatella määrittävän ennemmin 
paikanilmausta kuin itse lauseessa ilmaistavaa propositiota tai lauseen
pääverbiä:

(@ee_soininvaara) Soininvaara aikoo tiettävästi ottaa asian esille parin viikon
päästä sosiaalipoliittisessa ministerityöryhmässä. (Fipress: Länsi-Savo)
(@ee_baltia) Baltian maat ovat myös saaneet kutsun EU:n seuraavaan huippukokoukseen parin
viikon päästä Cannesissa. (Fipress: Länsi-Savo)

Kuviossa 47 tässä käsiteltyjen kolmen funktion
lisäksi suomen S4-todennäköisyyttä pienentävinä semanttisina funktioina
esiintyvät simultaaninen ja varsinainen resultatiivinen  funktio, joskin 
etenkin jälkimmäisen kohdalla suomen negatiivinen S4-vaikutus on pieni
ja jossain määrin epävarma. Simultaanisia ajanilmauksia -- joiden
on jo edellä monesti todettu muodostavan koko aineiston suurimman
ja heterogeenisimmän funktion -- koskenee kuitenkin koko lailla sama havainto
kuin sekventiaalisia ja likimääräisiä: simultaanisissa ilmauksissa
on paljon esimerkkien @ee_arbour ja @ee_kposadke kaltaisia
fokaalisia tapauksia

(@ee_arbour) Edellisen kerran Jugoslavia esti Arbourin pääsyn Kosovoon viime maaliskuussa. (FiPress: Keskisuomalainen)
(@ee_kposadke) Подготовка к посадке деревьев началась ещё на прошлой неделе. (Araneum Russicum: prirodasibiri.ru)

Simultaanisen ryhmän sisällä havaitaan kuitenkin merkittäviäkin 
aineistoryhmäkohtaisia eroja, joihin deiktisten ja positionaalisten
ilmausten osalta perehdytään tarkemmin seuraavassa.



### Deiktiset adverbit ja positionaalisuus

<!--Sido yleiseen tilastolliseen malliin ja kuvioihin edellä-->

Monet edellä käsitellyistä suomen ja venäjän loppusijaintia koskevista
eroista ovat liittyneet loppu- ja keskisijainnin väliseen
suhteeseen. Niin morfologisen rakenteen kuin
positionaalisuuden osalta loppusijainti vertautuu kuitenkin erityisen selvästi alkusijaintiin.
Kuvio 54 esittää
deiktisten adverbien ja positionaalisten ilmausten jakautumisen eri sijainteihin.
Deiktiset adverbit on esitetty ylä- ja positionaaliset ilmaukset alarivillä;
sarakkeissa ovat eri sijainnit, niin että vertailun kannalta oleellisimmat eli
S1- ja S4-sijainnit on tummennettu.

![\noindent \headingfont \small \textbf{Kuvio 54}: Deiktisten adverbien ja positionaalisten ilmausten suhteellinen jakautuminen eri sijainteihin \normalfont](figure/dposplot-1.svg)

Tutkimuksen yleisen tilastollisen mallin perusteella (ks. kuviot 16, 
17, 48 ja 49)
on mahdollista päätellä, että deiktiset adverbit ja positionaaliset ilmaukset kasvattavat suomessa
S4-aseman todennäköisyyttä venäjään verrattuna. Tämä näkyy etenkin kuvion 54 
positionaalisia ilmauksia kuvaavalla alarivillä. Alarivissä keskellä näkyvä
keskisijaintiin osuvien positionaalisten ilmausten määrä on suomessa ja venäjässä kutakuinkin samanlainen,
mutta vasemman reunan (S1-asema) ja oikean reunan (S4-asema) kielikohtaiset pylväät ovat
käytännössä toistensa peilikuvia: venäjässä noin 40 prosenttia positionaalisista ilmauksista
sijoittuu S1-asemaan, suomessa taas S4-asemaan. Vastaava peilikuvamainen symmetria näkyy myös kuvion 
54 deiktisiä adverbejä kuvaavalla ylärivillä, joskin
positionaalisia ilmauksia epätasaisimmin, sillä kielten välillä on selvemmin eroja myös keskisijainnin käytössä. 
Toisaalta  voidaan todeta, että positionaalisilla ilmauksilla venäjän S1-aseman osuus 
on 5,62-kertainen suomeen
ja suomen S4-aseman osuus tasan 4,00-kertainen 
venäjään verrattuna, mutta deiktisillä adverbeillä vastaavat luvut 
ovat 4,75 (venäjän S1 verrattuna suomen S1:een)
ja peräti 8,00 (suomen S4 verrattuna venäjän S4:ään).

<!--TODO: viittaa aineistolukuun-->

Oikeastaan positionaalisia ilmauksia ja deiktisiä adverbeja tutkittaessa
voidaan nostaa esiin kaksi huomiota herättävää seikkaa: ensinnäkin S4-sijainnin
silmiinpistävä harvinaisuus venäjän deiktisillä adverbeillä ja toiseksi
S4-sijainnin huomattava yleisyys suomen positionaalisilla ilmauksilla.
Koko venäjän valtavassa, 19 028 lausetta
kattavassa deiktisten adverbien aineistossa on ainoastaan 
  445 S4-tapausta.
Tarkemmin luokiteltuna tapaukset jakautuvat aineistoryhmittäin seuraavan
taulukon mukaisesti (vertailun vuoksi taulukossa on esitetty myös vastaavat
suomenkieliset luvut):


--------------------------------------------------------------------------------------------------------------
&nbsp;   L1a            L1b            L1c            L5c           L8a            L8b           L8c          
-------- -------------- -------------- -------------- ------------- -------------- ------------- -------------
fi       743 (19,46%)   629 (21,59%)   234 (33,33%)   372 (8,45%)   578 (26,44%)   179 (9,25%)   144 (8,29%)  

ru       91 (1,86%)     66 (1,54%)     87 (6,75%)     83 (2,58%)    58 (2,74%)     13 (0,95%)    47 (2,56%)   
--------------------------------------------------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 17}: S4-tapaukset deiktisiä adverbejä edustavissa aineistoryhmissä \normalfont \vspace{0.6cm}

Taulukossa 17  huomiota herättävät edellä (ks. luku [8.2.2.1](keskisijainti.html#l1-erot-kesk))
tarkasti käsitellyt  L1-aineiston ilmaukset, joissa kaikissa suomi käyttää
S4-sijaintia hyvinkin laajasti (L1c-ryhmässä peräti 33,33 %) mutta
joissa venäjänkielisiä tapauksia on niin vähän, että niitä voidaan tarkastella käytännössä yksitellen.
Seuraavat virkkeet ovat esimerkkejä venäjän L1b- ja L1c-aineistojen S4-tapauksista:

(@ee_kokis) Наши инвестиции в российскую экономику оправдывают себя уже сегодня. (Araneum Russicum: coca-colarussia.ru)
(@ee_banju) При заявке в нашу компанию, мы доставим готовую деревянную баню уже завтра. (Araneum Russicum: metaldom-stroy.ru)
(@ee_otvetzavtra) Я дам тебе ответ завтра, - ответил он, чуть поджав губы. (Araneum Russicum: fan.lib.ru)
(@ee_zhdetnas) Не можем мы знать что ждет нас завтра и какие события настигнут каждую минуту. (Araneum Russicum: moving.ru)

Esimerkit @ee_kokis -- @ee_otvetzavtra vertautuvat informaatiorakenteeltaan
edellä käsiteltyihin duratiivisiin esimerkkeihin
@ee_palomniki -- @ee_baletom, jotka tulkittiin fokaalisen konstruktion edustajiksi.
Esimerkeissä  @ee_kokis ajatus investointien oikeutuksesta samoin kuin esimerkissä @ee_banju
ajatus puisen saunan valmistamisesta ja esimerkissä @ee_otvetzavtra vastauksen antamisesta
ovat kaikki diskurssireferenttejä, joiden kirjoittaja olettaa olevan kuulijan
tajunnassa aktiivisena. Ajanilmauksen tehtävänä luetelluissa
esimerkeissä on välittää lukijalle ajallista, lokalisoivaa tietoa näistä propositioista, ja 
ajanilmauksen sijaitseminen sekä verbin että objektin jäljessä korostaa
oletusta näiden ilmaiseman proposition aktiivisuudesta.

On kuitenkin huomattava, että kuten duratiivisten ilmausten kohdalla, myös 
deiktisiä adverbejä tarkasteltaessa vastaavia diskurssifunktioita toteutaan
venäjänkielisessä aineistossa usein myös keskisijainnilla kuten esimerkissä @ee_aromaty ja
toisaalta alkusijainnilla kuten esimerkissä @ee_trjuki:

(@ee_aromaty) Волшебные ароматы Востока уже завтра могут заполнить ваш дом, неся вам радость, свет и вдохновение. (Araneum Russicum: aromati-vostoka.ru)
(@ee_trjuki) Уже завтра зрители увидят невероятные трюки главных артистов. (Araneum Russicum: lotosgtrk.ru)

<!--Huomioita siitä, että zavtralle fok.konstr.luontevin -->

Deiktisistä adverbeistä voidaan siis lähtökohtaisesti todeta, että
yksi syy venäjän S4-sijainnin harvinaisuudelle suhteessa suomeen on se, että
-- niin kuin duratiivisten ilmausten kohdalla jo havaittiin -- fokaalinen konstruktio
on venäjässä tavallinen myös muissa sijainneissa. 

Positionaalisten ilmausten tapauksessa S4 on venäjässä kauttaaltaan
tavallisempi kuin deiktisten adverbien kohdalla, joskin eri aineistoryhmien
välillä on myös jonkin verran vaihtelua.
Taulukko 18 esittää positionaalisten aineistoryhmien S4-tapausten
tarkat määrät ja suhteelliset osuudet:



----------------------------------------------------------------------
&nbsp;   F1b            L2a             L2b             L4a           
-------- -------------- --------------- --------------- --------------
fi       613 (68,19%)   1507 (33,65%)   1496 (44,16%)   466 (33,41%)  

ru       49 (36,84%)    278 (10,05%)    123 (9,41%)     19 (3,69%)    
----------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 18}: S4-tapaukset positionaalisissa aineistoryhmissä \normalfont \vspace{0.6cm}

Taulukossa 18 merkillepantavia ovat toisaalta F1b-ryhmän (ks. osio [7.2.2.4.2](alkusijainti.html#s1-f1b))
huomattavan korkea suhteellinen osuus sekä toisaalta L4a-ryhmän (osio [5.2.1.4](ajanilmaukset_tutkimusaineistossa.html#l4a-l4b))
poikkeuksellisen pieni S4-osuus. L4a-aineisto kattaa venäjässä vain kourallisen S4-tapauksia, joista
esimerkkejä ovat virkkeet @ee_blatter ja @ee_bespokoit:

(@ee_blatter) Президент ФИФА Блаттер поздравил меня вечером. (RuPress: Комсомольская правда)
(@ee_bespokoit) Сравнительно интенсивное движение автомобилей побеспокоит вас только утром и вечером\-\-  (Araneum Russicum: v-nova.ru)

Huomionarvoista on, ettei venäjän L4a-aineiston vähien S4-tapausten joukosta
ole löydettävissä käytännössä yhtään fokaalisen konstruktion edustajaa. Esimerkissä @ee_blatter
on kyse tapauksesta, jossa ajanilmauksen diskurssifunktio on puhtaan kehyksinen: kirjoittaja
ei oletettavasti pyri ilmoittamaan sitä, milloin Blatter onnitteli tätä vaan itse tapahtuman, proposition
[Blatter onnitteli minua], josta sitten tarkennetaan, että tämä tapahtui illalla.
Kenties lähimpänä fokaalista konstruktiota on esimerkki @ee_bespokoit, jossa siinäkin on pantava merkille, 
että *утром и вечером* viittaavat aamuihin ja iltoihin yleensä, eivät johonkin konkreettiseen,
aikajanalta erotettavissa olevaan referenttiin. Tämä on suomenkielisen aineiston valossa
odottamatonta, sillä yksi syy suomen suureen S4-tapausten määrään vaikuttaisi olevan
juuri tämänkaltaisissa fokaalisissa konstruktioissa, joita edustavat muun muassa
seuraavat esimerkit:

(@ee_netanyahu) Pääministerin kanslia ilmoitti myöhemmin päivällä, että Netanyahu antaa vaaleja koskevan lausunnon illalla. (Fipress: Länsi-Savo)
(@ee_satamat) Satamien pitäisi aloittaa toimintansa taas aamulla. (Araneum Finnicum: m.iltalehti.fi)
(@ee_seijamake) Seija, Make, Matti ja Jonttu olivat grillanneet viimeiset neljä makkaraansa illalla. (Araneum Finnicum: mahti.pp.fi)
(@ee_vihreapaatos) Vihreä liitto teki päätöksensä illalla. (FiPress: Karjalainen)

Osittain myös L4a-aineistossa fokaalisten tapausten puuttumista voi selittää sillä, että 
ajanilmaus voi venäjässä saada fokuksen diskurssifunktion keski- tai alkusijaintien
avulla, kuten seuraavissa esimerkeissä:

(@ee_tolkoutrom) О случившемся тут же сообщили супруге, которая только утром навестила мужа. (RuPress: РИА Новости)
(@ee_vozobnovit) Ожидается, что уже вечером он возобновит работу. (Araneum Russicum: medpulse.ru)
(@ee_povtorvstretshy) Желающие могут вечером повторить встречу заката. (Araneum Russicum: ukok-tour.ru)




Esimerkit @ee_tolkoutrom -- @ee_povtorvstretshy eivät ole ehkä yhtä selkeästi
fokaalisia kuin vaikkapa esimerkki @ee_netanyahu edellä, mutta niissä yhtä
kaikki kuvataan jotakin kuulijan tajunnassa aktiiviseksi oletettavaa
propositiota ([puoliso kertoi asiasta x miehelleen],[subjekti jatkaa
työtään],[auringonlaskun kohtaaminen]) toisin kuin esimerkiksi virkkeessä
@ee_blatter edellä. On itse asiassa todettava, että esimerkin @ee_vozobnovit
kaltaisia *уже + утром/вечером*- tai esimerkin @ee_tolkoutrom kaltaisista
*только + утром/вечером* -tapauksia on venäjänkielisessä aineistossa kaikkiaan
  20, mutta näistä vain yksi sijoittuu S4-asemaan, siinä missä
suomen 57 vastaavan *jo/vasta + aamulla/illalla*-tapauksen osalta
S4 on tavallisin sijainti (25 tapausta). Tästä
huolimatta vastaavien tapausten selitysvoimaa voi tuskin pitää riittävänä, kun
mietitään esimerkiksi venäjänkielisten L4a- ja L2-ryhmien välistä sisäistä eroa
S4-osuuksissa. Yksi mahdollinen lisäselitys olisi olettaa, että kyse ei venäjän
L4a-ryhmän osalta ole vain siitä, että fokaalisia S4-tapauksia on suomea
vähemmän, vaan siitä, että fokaalisia tapauksia on *ylipäätään* vähemmän. Tätä
oletusta tukee se, että verrattaessa venäjän positionaalisia ryhmiä keskenään
L4a erottuu selvästi muista, mutta vastaavaa ryhmien välistä eroa ei havaita
suomessa, jossa L4a-ryhmän S4-osuus on kutakuinkin yhtä suuri kuin
L2a-ryhmällä. On siis mahdollista, että venäjän *утром*- ja *вечером*-sanoihin
liittyvät diskurssifunktiot ovat lähtökohtaisesti useammin topikaalisia (ja
siten painottuneita lauseen alkupuolelle) kuten esimerkissä @ee_legkimutrom:

(@ee_legkimutrom) Этим легким и летним утром я покинула мягкую постель, и начала собираться в гости. (Araneum Russicum: rbp-place.ru)

Jos suomessa pyrkisi lisäämään aamuun tai iltaan viittaavaan ajanilmaukseen esimerkin @ee_legkimutrom tapaisia
adjektiivimääritteitä, olisi käytettävä tämän tutkimuksen aineistoon otettujen adessiivimuotojen (*aamulla* ja *illalla*)
sijaan essiivimuotoja *aamuna* ja *iltana*, mikä saattaa osaltaan selittää sitä, että suomessa lauseenloppuinen
asema on suhteessa niin paljon suositumpi kuin venäjässä. 

<!--Tähän kohtaan vielä vertailu L4b-ryhmään-->

Venäjän ja suomen L4a-ryhmät sisältävät siis jossain määrin jo lähtökohtaisesti
erilaisen jakauman ajanilmausten eri diskurssifunktioita. Jo todetun lisäksi on
syytä muistaa edellä osiossa [5.2.1.4](ajanilmaukset_tutkimusaineistossa.html#l4a-l4b) mainittu huomio siitä, että venäjän *утром* ja
*вечером* ovat siinä määrin vakiintuneita ilmauksia, ettei niitä kieliopeissa
ole tapana luokitella *утро*- ja *вечер*-substantiivien taivutusmuodoiksi vaan
omiksi lekseemeikseen. Tämän voisi nähdä yhtenä mahdollisena adverbeille
tyypillisen keskisijainnin osuutta kasvattavana tekijänä, joskin on todettava,
että L4a-aineistossa suomen ja venäjän välinen ero muodostuu ennen kaikkea
erosta S1- ja S4-sijaintien välillä.


L4a-ryhmän matalan S4-osuuden lisäksi taulukosta 18 
havaitaan, että F1b-ryhmässä S4-sijainnin todennäköisyys on huomattavan suuri
kielestä riippumatta. F1b-ryhmää edustava esimerkki (@ee_hessu) esitettiin jo aivan luvun alussa
yhtenä tyypillisenä fokaalisen konstruktion ilmentymänä, ja epäilemättä juuri fokaalisten
tapausten suuri määrä selittää F1b-ryhmän painottumista lauseen loppuun. Myös L2-ryhmissä 
venäjän S4 on selvästi tavallisempi kuin esimerkiksi deiktisillä adverbeillä
(ks. taulukko 17 ), mutta tästä huolimatta ero
suomeen on hyvin selvä. L2-ryhmiä analysoitaessa onkin syytä palata luvussa
[7.2.2.1](alkusijainti.html#s1-sim-pos) tehtyihin L2-ryhmiä ja alkusijaintia koskeneisiin huomioihin.

Luvussa [7.2.2.1](alkusijainti.html#s1-sim-pos) havaittiin, että suomen ja venäjän L2-ryhmien
alkusijaintitapaukset eroavat toisistaan ennen kaikkea siinä, että
venäjässä toisin kuin suomessa L2-ajanilmaukset sijaitsevat usein lauseen alussa
ilman määritteitä. Suomen S1-tapauksien todettiin
useimmiten olevan joko alatopiikkikonstruktion, kontrastiivisen konstruktion,
äkkiä-konstruktion tai affektiivisen konstruktion edustajia sekä joskus lokaaleita
johdantokonstruktioita, kun
taas venäjässä myös globaalit johdantokonstruktiot todettiin tavallisiksi. Esimerkkejä tällaisista
tapauksista olivat L2a-ryhmän virkkeet @ee_livija ja @ee_wto, jotka esitän tässä uudelleen
numeroilla @ee_livija2 ja @ee_wto2:

(@ee_livija2) В среду Госдума на очередном пленарном заседании рассмотрит ситуацию в Ливии. (RuPress:  Новый регион 2 2011)
(@ee_wto2) Во вторник в штаб-квартире ВТО в Женеве президент Украины Виктор Ющенко и генеральный директор организации Паскаль Лами подписали протокол о присоединении Украины к Всемирной торговой организации. (RuPress: РИА Новости 2008) 

Mielenkiintoista kyllä, vaikka suomen L2a-aineistosta ei löytynyt juuri
lainkaan esimerkkien @ee_livija2 -- @ee_wto2 kaltaisia S1-tapauksia
(poikkeuksena katso esimerkki @ee_uusitalo), on vastaavien S4-tapausten esiin
kaivaminen helppoa, kuten virkkeet @ee_euministerit ja @ee_hyvinkaaseura
osoittavat:

(@ee_euministerit) EU:n liikenneministerit käsittelevät kuljettajien työaikoja kokouksessaan *keskiviikkona*. (FiPress: Hämeen sanomat 1999)
(@ee_hyvinkaaseura) Hyvinkään Maatalousseura luovutti käytetyn kuivurin virolaisille *torstaina* maaseutukeskuksessa Järvenpäässä. (FiPress: Hyvinkään sanomat 1994)

Esimerkkien @ee_euministerit ja @ee_livija2 yhtäläisyys on silmiinpistävää: kummassakin on kyse aktivoimattoman
subjektin jonkinlaisessa kokouksessa suorittamasta toiminnasta, jonkin kysymyksen käsittelemisestä. Vaikka esimerkin 
@ee_euministerit laajempaa kontekstia ei ole saatavilla, voidaan olettaa, että se kuitenkin eroaa
esimerkistä @ee_livija2 siinä, että kyseessä tuskin on koko tekstin ensimmäinen virke. Yhtä kaikki,
vaikuttaisi siltä, että suomessa esimerkkien @ee_livija2 -- @ee_wto2 kaltaisia viestintätehtäviä toteutetaan
ennemmin loppu- kuin alkusijainnilla. 

Suomen ja venäjän alku- ja loppusijaintien välinen symmetria L2-ryhmissä on nähtävissä laajemminkin
kuin vain yksittäisten esimerkkien tasolla. Luvussa [7.2.2.1](alkusijainti.html#s1-sim-pos) tarkasteltiin ajanilmausten kollokaatteja
ja havaittiin, että venäjässä oli suomeen nähden huomattavan paljon enemmän tapauksia, joissa ajanilmaus 
sijaitsi koko lauseen ensimmäisenä konstituenttina. Jos taas tutkitaan
S4-asemaan sijoittuvien L2-tapausten oikeanpuoleisia kollokaatteja, saadaan taulukoiden 19 
ja 20 mukaiset tulokset:




\FloatBarrier

\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
--- & 912 & 60.52\\
ja & 79 & 5.24\\
kun & 32 & 2.12\\
mutta & 21 & 1.39\\
jolloin & 11 & 0.73\\
\addlinespace
helsingin & 8 & 0.53\\
aamupäivällä & 7 & 0.46\\
helsingissä & 7 & 0.46\\
torstaina & 7 & 0.46\\
joten & 5 & 0.33\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
--- & 39 & 14.03\\
в & 34 & 12.23\\
на & 16 & 5.76\\
и & 10 & 3.60\\
вечером & 6 & 2.16\\
\addlinespace
то & 6 & 2.16\\
а & 5 & 1.80\\
10 & 4 & 1.44\\
16 & 4 & 1.44\\
17 & 4 & 1.44\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 19}: L2a-aineistoryhmän yleisimmät oikeanpuoleiset kollokaatit suomessa ja venäjässä \normalfont \vspace{0.6cm} 
\begin{table}[!htb]
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
--- & 888 & 59.36\\
ja & 135 & 9.02\\
kun & 34 & 2.27\\
mutta & 27 & 1.80\\
jolloin & 22 & 1.47\\
\addlinespace
tai & 7 & 0.47\\
eikä & 6 & 0.40\\
joten & 6 & 0.40\\
viime & 5 & 0.33\\
koska & 4 & 0.27\\
\bottomrule
\end{tabular} \end{minipage}%
    \begin{minipage}{.5\linewidth}
      \centering 
\begin{tabular}{lrr}
\toprule
Kollokaatti & Freq & Osuus\\
\midrule
и & 16 & 13.01\\
прошлого & 15 & 12.20\\
когда & 10 & 8.13\\
в & 7 & 5.69\\
этого & 7 & 5.69\\
\addlinespace
а & 6 & 4.88\\
но & 5 & 4.07\\
на & 4 & 3.25\\
--- & 3 & 2.44\\
не & 3 & 2.44\\
\bottomrule
\end{tabular} \end{minipage} 
\end{table}
 
\noindent \headingfont \small \textbf{Taulukko 20}: L2b-aineistoryhmän oikeanpuoleiset kollokaatit suomessa ja venäjässä \normalfont \vspace{0.6cm} 

\FloatBarrier

<!--TODO: log-likelihood tms. -->

Taulukot 19 ja 20
osoittavat muun muassa sen, että kummassakin kielessä on melko tavallista, että
S4-sijoittunutta L2-ajanilmausta seuraa rinnastuskonjunktio tai L2b-aineiston
tapauksessa myös kun-lause. Kielten välillä on kuitenkin selvä ero siinä,
kuinka suuri osuus on tapauksilla, joissa ajanilmaus on koko virkkeen
viimeisenä eli ilman kollokaattia -- ero on peilikuva siitä, mitä S1-sijainnin yhteydessä
havaittiin vasemmanpuoleisista kollokaateista.[^ll_l2] Lisäksi vaikuttaisi
ainakin L2a-ryhmän osalta siltä, että venäjä sisältää enemmän tapauksia, joissa
ajanilmauksella kuvataan lopulta tarkempaa hetkeä kuin viikonpäivää ylipäätään.
Tällaisia ovat toisaalta *вечером* ('illalla') -tapaukset, toisaalta tapaukset,
joissa ajanilmauksen ensimmäisenä oikeanpuoleisena kollokaattina on jokin
numero ja toisaalta suuri osa tapauksista, joissa ajanilmausta seuraa
в-prepositio. Virkkeet @ee_mintorg -- @ee_uvidjatsvet ovat esimerkkejä näistä: 

[^ll_l2]: (TODO -- Bayesiläisittäin?) Jos vertaa kollokaatittomia tapauksia suomessa ja venäjässä,
saadaan esimerkiksi log-likelihood-arvo  92,576.

(@ee_mintorg) Министерство торговли опубликует данные по дефициту внешней
торговли за октябрь во вторник в 13.30 по Гринвичу. (Araneum Russicum: markets-today.ru)
(@ee_kazan) Известие потрясло Казань во вторник в середине дня. (RuPress: Комсомольская правда)
(@ee_poslednijraz) Последний раз я видела Людмилу во вторник 24 октября 2000 года. (Araneum Russicum: eparhia.karelia.ru)
(@ee_uvidjatsvet) Основным событием недели для австралийского доллара станут данные по рынку труда, которые увидят свет в четверг 14 марта в 04.30 МСК. (Araneum Russicum: fx.ru)

Esimerkkien @ee_mintorg -- @ee_uvidjatsvet kaltaiset tapaukset ovat kaikki
selkeitä fokaalisen konstruktion edustajia. Suomenkielisissä L2-aineistoissa
havaittava suuri kollokaatittomien tapausten määrä viittaa kuitenkin siihen, että
suomessa on vähemmän eksplisiittiseen ajankohtaan
viittaavia ja siten potentiaalisesti juuri tästä ajankohdasta lukijaa
informoivia tapauksia. Tämä havainto antaa syytä olettaa,
että syy suomen loppusijainnin venäjää suurempaan yleisyyteen saattaisi olla
siinä, että loppusijaintia käytetään suomessa venäjää enemmän myös muihin
viestintätehtäviin kuin fokuksen ilmaisemiseen. Edellä esitetyt esimerkit
@ee_euministerit ja @ee_hyvinkaaseura antavat viitteitä näiden
viestintätehtävien luonteesta, ja itse asiassa myös deiktisten adverbien
joukosta löytyy paljon vastaanvantyyppisiä tapauksia, kuten esimerkit
@ee_kiinapuhe -- @ee_vaalipiiri osoittavat:

<!--huom: @tomlin2014, 84 mainitsee, että sentence adverbials sijaitsevat
yleensä joko alussa tai lopussa. jotenkin pitäisi tämä perinteinen
lauseadverbiaali-käsite saada tässä mainittua. Ehkä selitettyä lisäksi, että
tämä ei ole satunnaista, vaan voidaan esittää tiettyjä erityisen tyypillisiä
konstruktioita. -->

(@ee_kiinapuhe) Pääministeri Zhu Rongji piti Kiinan kansantasavallan 50 - vuotisjuhlallisuuksiin liittyen puheen Pekingissä eilen. (FiPress: Kaleva)
(@ee_virrankoski) Kyösti Virrankoski tukijoukkoineen jakoi tietoa ja mielipiteitään EU:sta Jyväskylän kävelykadulla eilen. (FiPress: Keskisuomalainen)
(@ee_uusitielaki) Hallitusneuvos Kalevi Perko kertoi uudesta tielaista Tiet taajamassa -- seminaarissa keskiviikkona. (FiPress: Aamulehti)
(@ee_europaues) Ja Europaeukset, Tämä suku vietti sukuyhdistyksensä 30-vuotisjuhlaa Haapavedellä äskettäin. (Araneum Finnicum: europaeus.info)
(@ee_vaalipiiri) Hänhän vaihtoi vaalipiiriäkin tässä hiljattain. (FiPress: Turun Sanomat)

Esimerkeissä @ee_euministerit -- @ee_hyvinkaaseura ja @ee_kiinapuhe --
@ee_europaues on merkillepantavaa, että niissä kaikissa lauseenloppuista
ajanilmausta edeltää paikanilmaus. Esimerkissä @ee_vaalipiiri ei varsinaisesti
ilmaista paikkaa, vaan siinä ajanilmausta edeltävällä inessiivimuotoisella
pronominilla on ainoastaan diskursiivinen funktio -- voisi jopa tulkita, että
syynä *tässä*-sanan käytölle on juuri kirjoittajan tai puhujan pyrkimys käyttää
lauseenloppuista lokalisoivaa paikka + aika -konstruktiota, vaikka varsinaista
tarvetta paikan ilmaisemiselle ei ole [@tobecited]. Paikan ilmaiseminen ei
kuitenkaan vaikuta olevan vastaavissa konstruktioissa välttämätöntä, kuten
esimerkit @ee_presidenttipelasti -- @ee_samppa (sekä venäjänkielisen aineiston
@ee_blatter edellä) osoittavat:

<!--mahd. visk § 1317
tsekkaa myös, miten venäjässä, onko käytössä *тут недавно* -ilmaisua

tsekkaa Huumon väikkärin vika artikkeli: ei-temoraalisten tila-adverbiaalien temporaalisista tulkinnoista

humo s.189 eteenpäin

TODO: harkitse alaotsikoita tähän lukuun

huomauta, että kuukausilla harvinaisempaa.

-->


(@ee_presidenttipelasti) -- Presidentti pelasti minun päiväni tänään. (FiPress: Demari)
(@ee_luka) Luka saavutti ensimmäisen kouluratsastuskilpailuvoittonsa tänään! (Araneum Finnicum: taikaponi.net)
(@ee_samppa) Samppa teki ihan täydellistä salaattia meille eilen. (Araneum Finnicum: miatanh.blogspot.com)

Esimerkeissä @ee_presidenttipelasti -- @ee_samppa ajanilmausten tulkitseminen
ei--fokaalisiksi on jossain määrin mutkikkaampaa kuin edellä esitettyjen
paikanilmauksen sisältävien esimerkkien.
Tarkastelenkin seuraavassa esimerkkien @ee_presidenttipelasti -- @ee_samppa informaatiorakennetta
tarkemmin jakamalla niihin liittyvät diskurssireferentit osaksi pragmaattista
väittämää ja olettamaa käyttämällä hyväksi taulukkoa 1, johon
on vertailun vuoksi sisällytetty myös selkeän fokaalinen esimerkki @ee_poslednijraz sekä edellä
esitetyistä paikanilmauksen sisältävistä tapauksista @ee_virrankoski:

<!--todo: ei määrittää substantiivia Kaiken kukkuraksi Teija Sarajärvi kutsui minut ja Lauran aamupalalle huomenna. (Araneum Finnicum: turisti-info.fi)-->

<!--todo: lisää esimerkkien numerot taulukkoon -->


---------------------------------------------------------------------------------------------------
lause                             olettama                         väittämä                        
--------------------------------- -------------------------------- --------------------------------
*@ee_poslednijraz: Последний      Subjekti ei ole nähnyt L:aa      24.10.2000 = h                  
раз я видела Людмилу во           vähään aikaan. On olemassa                                       
вторник 24 октября 2000 года.*    hetki, jolloin subjekti                                          
                                  viimeksi näki L:n (h)                                            

*@ee_virrankoski: Kyösti          Mahdollisesti: lehdessä          On olemassa henkilö nimeltä     
Virrankoski tukijoukkoineen       esitetystä kuvasta näkyvä        K.V.. K.V. on jakanut tietoa    
jakoi tietoa ja mielipiteitään    informaatio                      ja mielipiteitä. Jakaminen      
EU:sta Jyväskylän                                                  tapahtui edeltävänä päivänä     
kävelykadulla eilen.*                                                                              

*(@ee_presidenttipelasti)         Lukija tietää                    Presidentti pelasti puhujan     
Presidentti pelasti minun         *minä*-pronominin referentin     päivän. Pelastettu päivä oli    
päiväni tänään.*                  (puhujan). Lukija tietää         se, joka puhehetkellä on ollut  
                                  presidentin                      meneillään                      

*(@ee_luka) Luka saavutti         Lukija tietää Lukan. Lukija      Luka on saavuttanut             
ensimmäisen                       tietää, että Luka osallistuu     ensimmäisen voittonsa           
kouluratsastuskilpailuvoittonsa   kilpailuihin                     kouluratsastuskilpailussa.      
tänään!*                                                           Voitto saavutettiin sinä        
                                                                   päivänä, joka viestin           
                                                                   lähetyshetkellä oli meneillään  

*@ee_samppa: Samppa teki ihan     Lukija tietää, kuka on Samppa.   Samppa teki täydellistä         
täydellistä salaattia meille      Lukija tietää, että lauseen      salaattia. Salaatin tekeminen   
eilen.*                           kirjoittaja kuuluu pronominin    tapahtui eilen                  
                                  *meille* identifioimaan                                          
                                  joukkoon                                                         
---------------------------------------------------------------------------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 1}: Esimerkkien @ee_poslednijraz, @ee_virrankoski sekä @ee_presidenttipelasti -- @ee_samppa pragmaattiset olettamat ja väittämät \normalfont \vspace{0.6cm}


Kuten taulukosta havaitaan, esimerkeissä @ee_presidenttipelasti -- @ee_samppa ja @ee_virrankoski
ennen kaikkea pragmaattiset väittämät ovat erilaisia kuin edellä esitetyissä
fokaalisissa esimerkeissä. Fokaalisissa ajanilmaustapauksissa tyypillistä on,
että pragmaattinen olettama pitää sisällään paljon informaatiota verrattuna
pragmaattiseen väittämään, johon tavallisesti kuuluu esimerkin @ee_poslednijraz
kaltaisissa lokalisoivissa tapauksissa ainoastaan tieto ajanilmauksen kuvaaman
aikajanan pisteen ja väittämässä esitettyjen propositioiden suhteesta (yllä
kuvattu *24.10.2000 = h*). Esimerkkien @ee_presidenttipelasti -- @ee_samppa
samoin kuin esimerkkien @ee_kiinapuhe -- @ee_vaalipiiri tapauksissa taas
pragmaattiset väittämät ovat laajempia ja kattavat aina myös jotain
muuta kuin ajanilmaukseen liittyvää tietoa -- vieläpä niin, että muu tieto on
ensisijaista. Pragmaattiset olettamat taas eroavat fokaalisten esimerkkien ja esimerkkien
@ee_presidenttipelasti -- @ee_samppa sekä @ee_virrankoski välillä siinä,
että niiden sisältämä tieto on luonteeltaan erilaista: edellisten tapauksessa
tieto on ennen kaikkea propositionaalista [@yokoyama1986, 11], jälkimmäisten
tapauksessa taas useimmiten referentiaalista (mts. 9).

Pragmaattisen olettaman piiriin kuuluvan tiedon luonne ei--fokaalisissa
esimerkeissä käy hyvin ilmi, kun tutkitaan esimerkin @ee_luka
esiintymisyhteyttä (http://taikaponi.net/veera/ponipallero/luka.html,
tarkistettu 29.9.2017). Esimerkin kontekstina on kokonainen internetsivu, joka on
omistettu yhdelle tietylle Luka-nimiselle ponille. Sivulla selvitetään Lukan
taustoja, kerrotaan yleistietoja ja tilastoja siitä, minkälaisiin kilpailuihin
hevonen on osallistunut. Sivulla on myös kaksi merkintää sisältävä
*Päiväkirja*-niminen osio, jonka ensimmäinen merkintä alkaa esimerkillä
@ee_luka ja toinen kertoo ponin syntymästä. Tämä konteksti huomioon ottaen on
selvää, että kirjoittaja olettaa lukijalla olevan paljon referentiaalista
tietoa Lukasta ja että Luka sekä monenlainen siihen liittyvä tieto kuten se,
että poni osallistuu erilaisiin kilpailuihin, on lukijan tajunnassa aktiivista.
Ei ole kuitenkaan mielekästä ajatella, että propositio [Luka saavuttaa
ensimmäisen kouluratsastuskilpailuvoittonsa] olisi aktiivinen ja että
esimerkissä @ee_luka vain lokalisoitaisiin tämä jo aktiivisena ollut
propositio. Pikemminkin koko propositio esitellään virkkeessä @ee_luka uutena
ja se on osa pragmaattista väittämää. 

Esimerkki @ee_virrankoski poikkeaa esimerkistä @ee_luka siinä, että sen
kirjoittajan voitaisiin tulkita olettavan paljon vähemmän: voisi ajatella, ettei
pragmaattiseen olettamaan kuulu edes esimerkin @ee_luka kaltaista
referentiaalista tietoa, vaan että käytännössä kaikki esitettävä tieto on
diskurssistatukseltaan aktivoimatonta ja osa pragmaattista väittämää. Tässä
mielessä kyseessä olisi hyvin pitkälle venäjässä tyypillisten
globaalien S1-johdantokonstruktioiden kaltainen rakenne. Esimerkki @ee_virrankoski
sekä monet muut vastaavat tapaukset ovat suomen lehdistöaineistossa kuitenkin
siitä erityisiä, että niiden voi usein päätellä esiintyvän lehdissä
kuvateksteinä. 

Kuvatekstejä lingvistiseltä kannalta tutkinut Elina Heikkilä
[-@heikkila2006kuvan, 189] toteaa, että kuvatekstien laatimisessa on tavallista
pitää yleisohjeena, että lukijalle annetaan sellaista informaatiota, jota itse
kuvasta ei näy. Heikkilän mukaan (mts. 77) kuvatekstien oletetaan sisällyttävän
vastaukset mahdollisimman moneen kysymyksistä *kuka, mitä, missä, milloin,
miksi ja miten*. Nämä seikat tukevat ajatusta siitä, että kuvatekstit ovat
usein konteksteja, joissa suurin osa esitettävästä tiedosta kuuluu
pragmaattiseen väittämään. Voisikin ajatella, että suomenkielisessä aineistossa
esiintyvät monet kuvatekstit ovat hyvä ikkuna siihen, mihin järjestykseen
informaatio suomessa on luontevaa pakata, kun tehtävänä on välittää suuri
joukko kokonaan uutta ja aktivoimatonta tietoa samassa lauseessa. 
Esimerkin @ee_virrankoski kaltainen asioiden esittämisjärjestys vaikuttaa
Heikkilän (mts. 77--78) lainaamien suomalaisten kirjoittamisen oppaiden
perusteella suorastaan normilta. Esimerkiksi Antero Okkosen [-@okkonen1980toimittajan]
opas kehottaa *aloittamaan pääasialla ja
välttämään määreitä tekstin alussa*. Lauri Kotilaisen [-@kotilainen2003lehti]
oppaassa puolestaan todetaan, että kuvatekstin tulisi kertoa, mitä kuva esittää,
keitä kuvassa on ja *ehkä myös, milloin kuva on otettu ja missä, jos asia ei
muuten käy ilmi*. 

Palataan vielä taulukon 1 esimerkkeihin.
Virkkeestä @ee_samppa on todettava, että kysymys ajanilmauksen kuulumisesta
pragmaattisen väittämän tai olettaman piiriin on sen kohdalla muita tapauksia
monitulkintaisempi. Esimerkeissä @ee_presidenttipelasti ja @ee_luka
ajanilmauksen roolin voi nähdä melko selvästi kehyksisenä (vertaa luku
[](paatelmat.html#diskurssifunktioiden-tarkempi-erittely)): lauseet *Presidentti pelasti
minun päiväni!* ja *Luka saavutti ensimmäisen kouluratsastuskilpailuvoittonsa!*
voisi hyvin kuvitella myös yksittäisiksi huudahduksiksi ilman ajanilmausta.
Esimerkissä @ee_samppa ajanilmaus kuitenkin vaikuttaisi jossain määrin
topikaalisemmalta: blogityyppisessä päiväkirjamaisessa kontekstissa
[@tobecited] on luonnollista olettaa, että kirjoittaja kertoo paitsi itsestään
ja läheisistä ihmisistä, myös kuluneista päivistään sinänsä. Tässä mielessä
ajanilmaus on lauseen @ee_samppa kannalta oleellisempi kuin esimerkiksi
seuraavassa lauseessa:

(@ee_aloitus) Myös minä voin tehdä tästä uuden aloituksen huomenna. (Araneum Finnicum: teosofit.ning.com)

Esimerkin @ee_aloitus voisi nähdä edustavan ajanilmauksen topikaalisuuden
toista ääripäätä, jossa ajanilmaus on maksimaalisen irrallinen muusta lauseesta.

Vaikka edellä esitellyt lauseet siis eroavat joiltain osin informaatiorakenteen tasolla ja siinä,
onko mukana paikanilmausta vai ei, voi niiden taustalla silti nähdä oman konstruktionsa, jota
sen yleisimmällä tasolla nimitän  yksinkertaisesti *ei--fokaaliseksi S4-konstruktioksi*. 
Konstruktion tarkempi kuvaus
on annettu seuraavassa matriisissa:


<div class="outerbox cxg"><div class="containerbox"><div class="containerbox"><div class="outerbox"><div class="statusline"><div class="featureline"><div class="fleft">df</div><div>foc</div></div></div><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>subj</div></div></div><div class="node"><div class="featureline"><div class="fleft">cat</div><div>V</div></div></div><div class="node"><div class="featureline"><div class="fleft">gf</div><div>obj</div></div></div></div></div></div><div class="innerbox"><div class="node"><div class="featureline"><div class="fleft">gf</div><div>adv</div></div><div class="featureline"><div class="fleft">sem</div><div>aika</div></div><div class="featureline"><div class="fleft">prag</div><div><div class="matrixfeat"><div class="featureline"><div class="fleft">df</div><div>-focus</div></div></div></div></div></div></div></div></div>


\noindent \headingfont \small \textbf{Matriisi 28}: Ei--fokaalinen S4-konstruktio \normalfont \vspace{0.6cm}

Ei--fokaalisesta S4-konstruktiosta periytyviksi voidaan erikseen olettaa
toisaalta topikaalinen (vrt. esimerkki @ee_samppa) ja kehyksinen (esimerkki
@ee_aloitus) sekä toisaalta myös paikan lokalisaattorin (mm. esimerkit
@ee_kiinapuhe -- @ee_europaues) sisältävät variantit. Yhteistä näille kaikille
on se, että konstruktio sisältää edellä kuvatun kaltaisesti vain vähän
sellaisia elementtejä, jotka ovat osa pragmaattista väittämää ja
diskurssistatukseltaan aktiivisia -- lauseen fokus liittyy aina
ajanilmausta edeltävään propositioon, kun taas itse ajanilmauksella
on aina jokin muu diskurssifunktio kuin fokus. Tähän informaatiorakenteelliseen
ominaislaatuun liittyen on havainnollista verrata ei--fokaalista
ajanilmauskonstruktiota eräisiin niistä paikanilmauksen sisältävistä
tapauksista, joita Tuomas Huumo on tutkinut väitöskirjassaan. Huumo
[-@huumo1997, 162--163] esittää muun muassa seuraavan lauseen, jonka lopussa on
paikkaa ilmaiseva adverbiaali:

(@ee_huumobussi_loppu) Elmeri näki Anselmin bussissa 

Huumon tulkinnan mukaan lauseessa @ee_huumobussi_loppu adverbiaalin
määrityskohde on rajattu, niin että se viittaa lähinnä lauseen objektin
referentin, Anselmin, sijaintiin. *Bussissa* on tällöin Anselmi-sanan
määrite -- se, mitä Elmeri on nähnyt, ei toisin sanoen ole "Anselmi"
vaan "Anselmi bussissa". Toinen mahdollinen tulkinta on
fokaalinen konstruktio, jossa puhuja tai kirjoittaja olettaa koko proposition
[Elmeri näki Anselmin] aktiiviseksi kuulijan tai lukijan tajunnassa ja
spesifioi, että Anselmi sijaitsi proposition tapahtumahetkellä bussissa.

Jos kuitenkin siirrytään tarkastelemaan 
vastaavia lauseenloppuisen *ajanilmauksen* sisältäviä lauseita,
tulkintakenttä muuttuu hieman, kuten keksitty kontekstiton esimerkki
@ee_huumobussi_loppu_eilen osoittaa:

(@ee_huumobussi_loppu_eilen) Elmeri näki Anselmin eilen.

Esimerkille @ee_huumobussi_loppu_eilen voidaan erottaa kaksi loogista
tulkintaa: sen voi nähdä joko fokaalisen konstruktion edustajana, jolloin
propositio [Elmeri näki Anselmin] on diskurssistatukseltaan aktiivinen samoin
kuin alkuperäisen paikanilmauksen sisältävän tapauksen jälkimmäisessä
tulkinnassa. Toinen tulkinta taas on sovittaa esimerkki ei--fokaalisen
konstruktion sisään olettamalla koko propositio täysin uudeksi,
aktivoimattomaksi informaatioksi, niin että viestijän tavoitteena on kertoa
fakta siitä, että [Elmeri näki Anselmin] on tapahtunut ja että tämän lisäksi
koko tapahtumaa määrittää adverbi eilen.  <!--Esittele sem.scope terminä-->

<!--maininta VSO:sta?-->

On olennaista lisäksi huomata, että kun ajanilmaus toimii ei--fokaalisessa konstruktiossa,
on myös sen viittauskohde tavallisesti diskurssistatukseltaan aktiivinen,
niin kuin matriisi 28 määrittelee. Kuvittele seuraava esimerkki 
lausuttavaksi toisaalta heinäkuussa, toisaalta vaikkapa joulukuun kahdeksantena:

(@ee_huumobussi_loppu_itsp) Elmeri näki Anselmin itsenäisyyspäivänä.

Jos lausumisajaksi oletetaan heinäkuu, on hyvin todennäköisesti kyse
fokaalisesta konstruktiosta: puhuja tai kirjoittaja on luultavasti esittämässä
kertomusta, jonka diskurssitopiikki on Elmeri ja jossa propositio [Elmeri näki
Anselmin] oletetaan osaksi kuulijan tai lukijan aktiivista tajuntaa. Toisaalta
jos lausumishetki on Suomen itsenäisyyspäivän (6.12.) välittömässä
läheisyydessä, ei--fokaalinen konstruktio on luontevampi tulkintakehys:
propositio [Elmeri näki Anselmin] on viestin vastaanottajalle täysin uutta
tietoa, joka ankkuroidaan oletettavasti tunnistettavissa olevaan referenttiin
itsenäisyyspäivä<!--TODO: referenttinotaatio-->. Voisi itse asiassa ajatella,
että aktiivinen diskurssistatus eräällä tavalla lisensoi<!--fix--> ei--fokaalisen
konstruktion,[^huumotemp] joskin myös poikkeustapauksia
on löydettävissä (ks. esimerkki @ee_aloitus).


[^huumotemp]: kerro alaviitteessä Lontoossa-esdimerkin (180)
temporaalisuudesta. JA viittaa mahdollisesti kokoavaan kfa-lukuun.

<!--Vielä joku maininta aika + paikka -yhdistelmistä sekä ehkä-->

#### Tilastollinen malli ei--fokaalisen konstruktion yleisyydestä suomessa ja venäjässä

Palaan nyt tämän tutkimuksen kannalta oleellisimpaan kysymykseen eli siihen,
missä määrin ei--fokaalisen konstruktion esiintyminen on spesifiä suomelle ja
missä määrin sen voi nähdä selittävän aineistossa havaittavia eroja
positionaalisten ilmausten ja deiktisten adverbien S4-osuuksissa. Kysymyksen
tarkastelemiseksi keskityn seuraavassa niihin L-funktiota edustaviin
tapauksiin, joissa lauseen finiittiverbi esiintyy menneen ajan aikamuodossa ja
jotka sisältävät ajanilmauksen välittömässä läheisyydessä
paikanilmauksen.<!--Lisää perusteluita--> Esitetty rajaus jättää siis
tarkastelun ulkopuolelle tulevaisuuteen viittaavan aineistoryhmän L1c sekä
F-funktiota edustavan F1b:n. Lisäksi analyysistä suljetaan pois merkitykseltään
usein ambivalentit sanat *nyt*, *nykyisin*, *pian* sekä *сейчас*, *теперь* ja
*скоро*, jotka tietyissä tapauksissa voivat esiintyä menneessä ajassa mutta
joiden kohdalla ei--fokaalisen S4-konstruktion esiintyminen on epätodennäköistä.




Edellä kuvatulla tavalla rajattu aineisto sisältää aineistoryhmät L1a, L1b, L4a
ja L5c. Nämä kattavat
yhteensä 14 744 suomen-
ja 12 226
venäjänkielistä tapausta. Nämä tapaukset on seuraavassa taulukossa jaettu
kolmeen kategoriaan: niihin, joiden välittömässä läheisyydessä ei ole
paikanilmausta (*none*), niihin, joissa paikanilmaus sijaitsee välittömästi
ajanilmauksen jälkeen (*next*) ja niihin, joissa ajanilmaus sijaitsee heti ennen
paikanilmausta (*previous*).


-----------------------------------
&nbsp;   next   none     previous  
-------- ------ -------- ----------
fi       953    13 163   628       

ru       565    11 553   108       
-----------------------------------

Table: \noindent \headingfont \small \textbf{Taulukko 21}: Valikoidut deiktiset ja positionaaliset aineistoryhmät ja paikanilmauksen läsnäolo \normalfont \vspace{0.6cm}

<!--TODO: selitä tarkennuksia, ja kuvaa sitä, että venäjässä tosi vähän S4, ja
näistä tarkennusten jälkeen moni osoittautuu vääräksi.


Myös hyvä osoitus S4-fokaalisuudesta venäjässä: Джексон потерял сознание в своем доме в Лос-Анджелесе в четверг, около 21.30 мск. (Araneum Russicum: majkldzhekson.ru)

-->

Taulukosta havaitaan ensinnäkin, että kummassakin kielessä valtaosan muodostavat
tapaukset, joissa on pelkkä ajanilmaus ja että toiseksi eniten on ajanilmaus +
paikanilmaus -tapauksia. Jos katsotaan, miten paikanilmauksen läsnäolo vaikuttaa
loppusijainnin yleisyyteen, havaitaan melko selkeä trendi, joka on esitetty
loppusijainnin suhteelliset osuudet kokoavassa kuviossa 55:

![\noindent \headingfont \small \textbf{Kuvio 55}: Paikanilmauksen läsnäolo ja S4-aseman yleisyys L1a-, L1b-, L4a- ja L5c-aineistoryhmien menneen ajan tapauksissa \normalfont](figure/locative_neighbour.s4-1.svg)

Kuvio 55 osoittaa suorastaan eksponentiaalisen kasvun
suomenkielisten L1a-, L1b-, L4a- ja L5c-ryhmien S4-osuuksissa: tapauksissa,
joissa ajanilmauksen vieressä ei ole paikanilmausta, S4-asemaan osuu ainoastaan
noin viidennes, tapauksissa, joissa ajanilmausta seuraa paikanilmaus vajaat 40
prosenttia ja tapauksissa, joissa paikanilmaus edeltää ajanilmausta yli 70
prosenttia. Huomionarvoista kyllä, myös venäjässä jälkimmäisin vaihtoehto
näyttäisi lisäävän S4-aseman yleisyyttä.

Vaikuttaisi siis siltä, että mikäli ajan- ja paikanilmauksen sijaitsemista
menneeseen aikaan sijoittuvassa S4-lauseessa vierekkäin  voidaan pitää edes jossain
määrin tarkkana ei--fokaalisen S4-konstruktion indikaattorina, ei--fokaalinen
konstruktio on suomessa
tulkittavissa yhdeksi merkittäväksi syyksi ajanilmauksen
sijoittamiselle lauseen loppuun. Toisaalta erosta suhteessa 
venäjään ei kuviosta 55 saatavan informaation
perusteella voi vielä tehdä kovin varmoja johtopäätöksiä, sillä ainakin
jossain määrin paikanilmauksella voidaan nähdä vastaava S4-aseman
todennäköisyyttä kasvattava vaikutus myös venäjässä. Ei--fokaalisen
S4-konstruktion havaitsemista voidaan kuitenkin tarkentaa ottamalla huomioon
edellä käsiteltyjen esimerkkien perusteella esitetty arvio siitä, että nämä
konstruktiot ovat suomessa erityisen tyypillisiä lehdistökielelle. Yhdistämällä
tämä havainto ja tieto paikanilmauksen läheisyydestä voidaan rakentaa
tilastollinen malli, jonka perusteella ei--fokaalisen S4-konstruktion
vaikutuksesta S4-sijaintiin voidaan esittää kieltenvälisiä vertailuja.

Tässä käytettävää tilastollista mallia varten aineistoon luotiin muuttuja,
jolle annettiin kuusi eri lähdekorpuksen ja paikanilmauksen läsnäolon
yhdistävää arvoa: *araneum none*, *araneum next* ja *araneum previous* sekä
vastaavat lehdistökorpusta koskevat arvot *press none*, *press next* ja *press
previous*.[^nonfocbugs] Näiden avulla mitattiin kielen, lähdekorpuksen ja paikanilmauksen
läsnäolon yhteisvaikutusta. Kuvio 56  esittää 
mallin eri muuttujien selitysvoiman totuttuun tapaan keskihajontojen avulla:

[^nonfocbugs]: Mallin taustalla oleva bugs-koodi saatavilla BitBucket-versionhallinnasta 
osoitteesta https://tinyurl.com/ybnvm2ka (tarkistettu 15.3.2018)



```
## Ladataan tallennetua dataa Bayes-malliin nimeltä nonfocal
```

![\noindent \headingfont \small \textbf{Kuvio 56}: Ei--fokaalista konstruktiota arvioivan tilastollisen mallin keskihajonnat \normalfont](figure/nonfoc.std-1.svg)

Keskihajontoja tarkastelemalla voidaan todeta, että kielen ja
korpustyyppi+paikanilmaksen läsnäolo -muuttujan välillä todella on selvästi
nollasta erottuva interaktio, joskin luonnollisesti eniten selitysvoimaa on
pelkällä kielellä yksinään. Olennaista tässä tarkasteltavan ilmiön kannalta on
kuitenkin se, millaisena tämä interaktio näyttäytyy suhteessa S4-sijaintiin.
Nämä vaikutukset on esitetty kuviossa 57:

![\noindent \headingfont \small \textbf{Kuvio 57}: Paikanilmauksen läsnäolon, lähdekorpuksen ja kielen yhteisvaikutus ei--fokaalista konstruktiota arvioivassa tilastollisessa mallissa \normalfont](figure/nonfoc_interactplot-1.svg)

Kuviosta 57 on nähtävissä, että edellä esitettyjen
oletusten mukaisesti suomen S4-todennäköisyys todella kasvaa venäjään verrattuna, jos
lause sekä a) edustaa lehdistöaineistoa että b) sisältää joko ajanilmausta seuraavan tai
sitä edeltävän paikanilmauksen. Kuviossa näkyvien palkkien pituus osoittaa, että vaikutuksen
tarkkaan voimakkuuteen liittyy kohtalaisen paljon epävarmuutta, mutta sen suunta on 
yhtä kaikki selvä. Jossain määrin epävarmasti voidaan lisäksi todeta, että vaikutus
on todennäköisesti voimakkaampi nimenomaan niissä tapauksissa, joissa
paikanilmaus edeltää ajanilmausta. Kaikki tämä tukee ajatusta siitä, että suomen
ja venäjän välillä erityisesti deiktisten ja positionaalisten ilmausten osalta
havaittava ero S4-aseman yleisyydessä on jossain määrin selitettävissä
ei--fokaalisen S4-konstruktion tavanomaisuudella suomenkielisessä aineistossa.


Osasyy siihen, että kuviosta 57  tehtäviin 
havaintoihin liittyy epävarmuutta, saattaa olla siinä, että edellä esitettyjen
tyypillisten ei--fokaalisten S4-tapausten rinnalla suomessa vaikuttaisivat
olevan tavallisia myös rakenteeltaan lähes identtiset S3-tapaukset kuten
esimerkki @ee_bclinton

(@ee_bclinton) Yhdysvaltain presidentti Bill Clinton tapasi *torstaina*
Moskovassa useiden venäläispuolueiden johtoa. (FiPress: Demari)

Esimerkki @ee_bclinton osoittaa toisaalta myös sen, miksi juuri välittömästi
paikanilmauksen jäljessä sijaitseva ajanilmaus vaikuttaa voimakkaammin
S4-sijainnin todennäköisyyden kasvuun. Jos esimerkissä @ee_bclinton kääntäisi
ajan- ja paikanilmausten järjestyksen (*Moskovassa torstaina*), olisi ainakin
oman intuitioni mukaan lähes välttämätöntä siirtää molemmat konstituentit
lauseen loppuun -- joskin lauseen lopussa myös paikka + aika -järjestys olisi
täysin mahdollinen.  Tämä saattaa liittyä siihen, että sijainti juuri verbin 
jäljessä on tavallisesti loogisin sille ilmaukselle, jonka referentti on 
diskurssistatukseltaan aktiivisin ja siten uutuusarvoltaan vähäisin [@tobecited]: jos 
*Moskovassa*-sanan sijasta paikanilmauksen asemassa olisi esimerkiksi *täällä*-pronomini, 
olisi luontevin järjestys aika + paikka -järjestyksen sijasta paikka + aika:

(@ee_bclinton2) Yhdysvaltain presidentti Bill Clinton tapasi täällä *torstaina*  useiden venäläispuolueiden johtoa. (FiPress: Demari)

Katsotaan vertailun vuoksi vielä lähdekorpuksen ja paikanilmauksen läsnäolon
vaikutusta S1-sijainnin todennäköisyyteen. Suomen ja venäjän väliset erot on
esitetty kuviossa 58, jossa -- kuten tämän
tutkimuksen muissa vastaavissa kielen vaikutusta mittaavissa kuvioissa -- erot
on ilmaistu käyttämällä suomea vertailun lähtökohtana:

![\noindent \headingfont \small \textbf{Kuvio 58}: Paikanilmauksen läsnäolon, lähdekorpuksen ja kielen yhteisvaikutus ei--fokaalista konstruktiota arvioivassa tilastollisessa mallissa \normalfont](figure/nonfoc_interactplots1-1.svg)

Kuvio 58 osoittaa, että S1-todennäköisyys
kasvaa venäjässä suomeen verrattuna eniten  tapauksissa,
joissa lauseen alussa on ajanilmausta seuraava paikanilmaus ja jotka ovat osa
lehdistökorpusta.[^suuntaselitys] Tällaisia ovat muun muassa seuraavat
virkkeet:

[^suuntaselitys]: Kuten todettu, kuvio 58 esittää muun
tutkimuksen tavoin vertailun muodossa *suomessa verrattuna venäjään*. Se, että
jokin vaikutus on näin esitettynä selvästi negatiivisen puolella, tarkoittaa
aivan samaa kuin että sama vaikutus olisi saman verran positiivisen puolella,
jos vertailun lähtökohdaksi otetaan venäjä.

(@ee_vtsherajushenko) Вчера в Киеве президент Украины Виктор Ющенко дал пресс-конференцию, которую многие расценили как парад победы. (RuPress: РБК Daily)
(@ee_ranoutrom) Рано утром в Москве полицейские в ходе погони застрелили 29-летнего водителя. (RuPress: РИА Новости)
(@ee_plenarnom) Сегодня на пленарном заседании Государственной думы депутаты приняли решение пригласить на «закрытый»\-\- (RuPress: Новый регион 2)



Esimerkkejä @ee_vtsherajushenko -- @ee_plenarnom voidaan monessa tapauksessa
pitää edellä esitettyjen suomen ei--fokaalisten S4-esimerkkien tavallisimpina
vastineina, vaikkakin -- kuten muun muassa esimerkki @ee_blatter edellä
osoittaa -- myös ei--fokaaliset konstruktiot ovat venäjässä käytössä. On lisäksi
pidettävä mielessä se mahdollisuus, että esimerkiksi kuvatekstejä saattaa
suomenkielisessä lehtiaineistossa olla suhteessa enemmän kuin vastaavassa
venäjänkielisessä aineistossa, mikä voi hyvinkin vaikuttaa ajanilmausloppuisten
lauseiden määrään. Koska suurimmassa osassa suomenkielistä lehdistöaineistoa
laajemman kontekstin hakeminen ei ole mahdollista, on kuvatekstien todellista
osuutta vaikea arvioida. Aineistossa on kuitenkin monia sellaisia ei--fokaalista
konstruktiota edustavia esimerkkejä, jotka hyvin todennäköisesti ovat peräisin
lehtijutun varsinaisesta tekstistä, kuten esimerkki @ee_hameensahko:

(@ee_hameensahko) Hämeen sähkön osakkeen hinta jatkoi nousuaan Helsingin pörssissä eilen. (FiPress: Aamulehti)

Virkkeessä @ee_hameensahko on vaikea kuvitella, että Hämeen sähkön osakkeen
hinnan nouseminen olisi jotain sellaista, mitä havainnollistetaan erillisellä
kuvatekstin sisältävällä kuvalla. Seuraavien esimerkkien konteksti on
puolestaan tarkistettu erikseen digitoimattomista alkuperäislähteistä:

(@ee_purkamista) Elokuvatuottaja Ilkka A. Liikanen ehdotti koko elokuvasäätiön purkamista Vieraskynä-palstalla eilen. (FiPress: Helsingin Sanomat)
(@ee_vanki) Vanki sytytti tulipalon sellissään Riihimäen keskusvankilassa tiistaina. (FiPress: Aamulehti)

Esimerkki @ee_purkamista osoittautui Helsingin Sanomien kulttuurisivujen *Keskustelua*-nimisen
palstan kirjoitukseksi, jossa itse esimerkissä esiintyvä virke on osa tekstissä olevaa ingressimäistä,
tummemmalla painettua johdantokappaletta:

<div class="quote"><p> Talous on elokuvan ongelma, ei hallinto </p><p> Elokuvatuottaja Ilkka A. Liikanen ehdotti koko elokuvasäätiön purkamista Vieraskynä-palstalla eilen. Suomen elokuvasäätiön uusi tuotantojohtaja Erkki Astala vastaa hänelle. </p><p> Onko suomalaisella elokuva-alalla erityinen hallinnollinen ongelma? Tämän lehden palstoilla pitkään käydystä keskustelusta voisi niin päätellä: vaatimuksia uudesta hallintomallista on heitetty ilmaan ja nykyistä ammuttu alas, viimeksi Ilkka A. Liikasen kirjoituksessa 18. 12.</p><p> HS 19.12.1995 </p></div>


Esimerkki @ee_vanki esiintyy osana pientä uutista, joka on kokonaisuudessaan esitetty seuraavassa:

<div class="quote"><p>  Vanki sytytti sellinsä Riihimäellä</p><p> Vanki sytytti tulipalon sellissään Riihimäen keskusvankilassa tiistaina. Vankilan henkilökunta ehti ajoissa hätiin ja sai vedettyä miehen turvaan sekä sammutettua palon jauhesammuttimella. Palokunta tarkisti sellin ja sen läheiset tilat sekä suoritti savutuuletuksen. Vanki sai lievän savumyrkytyksen. </p><p> Aamulehti 26.10.1995</p><p></p></div>

Tarkempien kontekstien valossa tutkittuna esimerkit @ee_purkamista ja @ee_vanki
ovat silmiinpistävän samanlaisia kuin aiemmin tarkastellut venäjän
johdantokonstruktiotapaukset (kuten luvun [7.2.2.3](alkusijainti.html#simultaaninen-funktio) esimerkit
@ee_livija ja @ee_autoru). Oleellisin ja ilmeisin yhtäläisyys on, että niin
kuin edellisissä luvuissa tarkastellut johdantokonstruktiotapaukset, myös
esimerkit @ee_purkamista ja @ee_autoru ovat koko varsinaisen tekstin
ensimmäisiä virkkeitä ja siten nimenomaan johdantoja, jotka esittelevät tekstin
varsinaiset diskurssitopiikit. Kuten johdantokonstruktioissa, tässä
tarkastelluissa ei--fokaalisssa esimerkeissä subjektit eivät ole
diskurssistatukseltaan aktiivisia, vaan esitellään lukijalle ajanilmauksen
sisältävässä virkkeessä.

Kaiken kaikkiaan voidaan sanoa, että vaikka esimerkkien @ee_vtsherajushenko --
@ee_plenarnom kaltaiset tapaukset ovat ainakin jossain määrin mahdollisia
suomessa ja esimerkkien @ee_hameensahko -- @ee_vanki kaltaiset tapaukset
venäjässä, antaa tässä tarkasteltu tilastollinen malli selvät viitteet siitä,
että esimerkeissä @ee_vtsherajushenko -- @ee_plenarnom käytetty
viestintästrategia on paljon tyypillisempi venäjälle ja esimerkeissä
@ee_hameensahko -- @ee_vanki käytetty strategia suomelle. Venäjän
johdantokonstruktioiden ja suomen ei--fokaalisten konstruktioiden rooliin
toistensa mahdollisina ekvivalentteina perehdytään tarkemmin seuraavassa,
lukujen [7](alkusijainti.html#ajanilmaukset-ja-alkusijainti) --
[9](loppusijainti.html#ajanilmaukset-ja-loppusijainti) havainnot kokoavassa luvussa.



