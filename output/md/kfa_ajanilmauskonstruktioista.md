---
bibliography: lahteet.bib
csl: utaltl.csl
smart: true
output:
    html_document:
        toc: true
        toc_float: true
        toc_depth: 6
        number_sections: true
    pdf_document: 
      latex_engine: xelatex
      toc: true
      number_sections: true
      keep_tex: true
      includes:
          in_header: preamble.tex
---


Kokoava kontrastiivinen analyysi
================================


Luvussa \ref{funktionaalinen} mainittiin Chestermanin
[-@chesterman1998contrastive, 53] kontrastiivisen funktionaalisen
analyysimetodin ytimessä oleva ajatus siitä, että kieltenvälisen vertailun --
kontrastiivisen analyysin -- tavoitteena on tuottaa ja testata falsifioitavissa
olevia hypoteeseja jonkin ilmiön samanlaisuudesta kahden kielen välillä.
Luvuissa  \ref{ajanilmaukset-ja-alkusijainti} -- \ref{ajanilmaukset-ja-loppusijainti}
on tarkasteltu suomen ja venäjän alku, keski- ja loppusijainneissa
havaittavia yhtäläisyyksiä ja eroja, mutta varsinaiset hypoteesit kielten eri
ajanilmauskonstruktioiden vastaavuudesta ovat vielä esittämättä. Toteutan 
näiden hypoteesien esittämisen ja testaamisen tässä luvussa, jonka tarkoituksena
on samalla toimia edellisten lukujen havainnot kokoavana tiivistelmänä siitä, mitä
erilaisia ajanilmauskonstruktioita suomessa ja venäjässä havaittiin olevan 
ja miten esitetyt konstruktiot suhtautuvat toisiinsa.

Esitän ensimmäisenä, kaikkein epämääräisimpänä hypoteesina 
suomen ja venäjän ajanilmausten samanlaisuudesta, että tässä tarkastellut kielet
ovat samanlaisia sijaintien tasolla:

**Hypoteesi 1: ** suomen S1-sijaintia käytetään
samalla tavoin kuin venäjän S1-sijaintia, suomen S2-sijaintia samalla tavoin
kuin venäjän S2-sijaintia, suomen S3-sijaintia samalla tavoin kuin venäjän S3-sijaintia
ja suomen S4-sijaintia samalla tavoin kuin
venäjän S4-sijaintia 

Hypoteesin  kannalta empiirisesti
ongelmallisia ovat jo luvussa \ref{sij-yleisk} esitetyt eri sijaintien
suhteelliset osuudet kaikista ajanilmaustapauksista -- ennen muuta se, ettei
suomessa juuri lainkaan käytetä S2-sijaintia ja että venäjässäkin S3-sijainti
on harvinainen. Koska jo tutkimuskirjallisuuden perusteella oli selvää, että
suomessa S3-sijaintia käytetään pitkälti venäjän S2-sijainnin tavoin, voidaaan
hypoteesi tarkentaa seuraavaan muotoon -- tähdentämällä että *keskisijainti*
viittaa sekä S2- että S3-asemiin:


**Hypoteesi 2: ** Suomen alkusijaintia käytetään
samalla tavoin kuin venäjän alkusijaintia, suomen keskijaintia samalla tavoin
kuin venäjän keskisijaintia ja suomen loppusijaintia samalla tavoin kuin
venäjän loppusijaintia. 

Edelleen jo luvussa \ref{sij-yleisk} esitetyt karkeat prosenttiluvut kuitenkin
osoittavat, ettei myöskään hypoteesi 2 ole
erityisen uskottava, sillä suomen alkusijainti osoittautui huomattavasti
venäjän alkusijaintia harvinaisemmaksi ja toisaalta venäjän loppusijainti
samalla tavoin suomen loppusijaintia harvinaisemmaksi. Lähtökohtaisesti voidaan
siis olettaa, että kielten välillä on eroja siinä, miten niissä käytetään
ainakin alku- ja loppusijainteja. Lisäksi, vaikka keskisijainnin suhteellinen
osuus on kummassakin tarkasteltavassa kielessä melko samanlainen, ei tämä vielä
takaa, että keskisijaintiin osuvat konstruktiot olisivat keskenään samanlaisia --
päinvastoin, edellisessä kulmessa luvussa käytetty tilastollinen malli osoitti,
että kielten välillä on eroja siinä, miten keskisijaintia käytetään.
Hypoteesi onkin tarkennettava sijaintikohtaisesta ennemminkin
konstruktiokohtaiseksi ja pilkottava useiksi yksittäisiksi hypoteeseiksi, jotka 
voidaan yleisellä tasolla esittää muodossa


**Hypoteesi 3: ** Suomen kielen konstruktiot F1, F2, F2..n
    ovat samanlaisia kuin venäjän kielen konstruktiot R1, R2, R...n. 

Hypoteesin 3 testaamiseksi seuraavaan taulukkoon
on koottu kaikki edellisessä kolmessa luvussa käsitellyt ajanilmauskonstruktiot:


|Konstruktion nimi                     |Sijainti |Tutkimuksen alaluku                          |Matriisi |
|:-------------------------------------|:--------|:--------------------------------------------|:--------|
|Äkkiä-konstruktio                     |S1       |\ref{varsinainen-resultatiivinen-ekstensio}  |         |
|Affektiivinen konstruktio             |S1       |\ref{varsinainen-resultatiivinen-ekstensio}  |         |
|Kontrastiivinen konstruktio           |S1       |\ref{ei--deiktiset-adverbit}                 |         |
|Alatopiikkikonstruktio                |S1       |\ref{likim-s1}                               |         |
|Ajallinen alatopiikkikonstruktio      |S1       |\ref{s1-sim-pos}                             |         |
|Määrää painottava S1-konstruktio      |S1       |\ref{likim-s1}                               |         |
|Määrää painottava S4-konstruktio      |S4       |\ref{muutf-s4}                               |         |
|Sekventiaalinen konstruktio           |S1       |\ref{sekv-s1}                                |         |
|Topikaalinen konstruktio              |S1       |\ref{sekv-s1}                                |         |
|Topikaalinen S4-konstruktio.          |S4       |\ref{fi-s4-pien}                             |         |
|Globaali johdantokonstruktio          |S1       |\ref{s1-sim-pos}                             |         |
|Globaali johdantokonstruktio          |S2/S3    |\ref{l1-erot-kesk}                           |         |
|Lokaali johdantokonstruktio           |S1       |\ref{s1-sim-pos}                             |         |
|Lokaali johdantokonstruktio           |S2/S3    |\ref{l1-erot-kesk}                           |         |
|Raportoiva konstruktio.               |S1       |\ref{s1-l3}                                  |         |
|Ilmoituksen aloittava konstruktio     |S1       |\ref{s1-l3}                                  |         |
|Fokaalinen konstruktio                |S1       |\ref{l7l8-erot-kesk}                         |         |
|Fokaalinen konstruktio                |S4       |\ref{loppus_np}                              |         |
|Fokaalinen S2-konstruktio.            |S2       |\ref{duratiivinen-funktio}                   |         |
|Adverbinen konstruktio                |S2/S3    |\ref{morf-sama-keski}                        |         |
|Monivalenssinen S4-konstruktio        |S4       |\ref{loppus_np}                              |         |
|Teelinen konstruktio.                 |S4       |\ref{teelinen-semanttinen-funktio}           |         |
|Duratiivi + paikka -konstruktio.      |S3       |\ref{duratiivinen-funktio}                   |         |
|Topikaalinen duratiivinen konstruktio |S3       |\ref{duratiivinen-funktio}                   |         |
|Ei-fokaalinen S4-konstruktio          |S4       |\ref{deiktiset-adverbit-ja-positionaalisuus} |         |

**Taulukko 1: ** Tutkimuksessa käsitellyt konstruktiot 

Taulukon 1 ei ole tarkoitus olla tyhjentävä esitys
kaikista konstruktioista, joissa suomen ja venäjän ajanilmaukset myönteisissä
SVO-lauseissa esiintyvät. Pikemminkin taulukkoon on koottu ne edellisessä kolmessa
luvussa käsitellyt konstruktiot, jotka nousivat esille, kun kieliä vertailtiin
luvussa \ref{tilastollinenmalli} rakennetun tilastollisen mallin perusteella.


Taulukossa 1 esitetyistä konstruktioista voidaan ensinnäkin
todeta, että edellisissä luvuissa tehdyn analyysin perusteella ainakin äkkiä-konstruktio,
kontrastiivinen konstruktio, topikaalinen  S1-konstruktio,
sekventiaalinen konstruktio, adverbinen konstruktio, teelinen konstruktio sekä
fokaalinen S4-konstruktio ovat hyvin pitkälti
samanlaisina käytössä yhtä lailla suomessa kuin venäjässä. Näiden konstruktioiden osalta
hypoteesia 1  voidaan siis tarkentaa muotoon:


**Hypoteesi 4: ** Suomen kielen äkkiä-konstruktio,
        kontrastiivinen konstruktio, topikaalinen  S1-konstruktio,
        sekventiaalinen konstruktio, adverbinen, teelinen konstruktio sekä fokaalinen
        S4-konstruktio ovat samanlaisia kuin venäjän kielen samannimisinä
        käsitellyt konstruktiot. 

Hypoteesi 4 ei luonnollisesti ole lopullinen
tai maksimaalisen tarkka: voi hyvin olla, että kustakin edellä luetellusta konstruktiosta
on löydettävissä monia käyttötilanteita, joissa kielet eivät toimi samalla tavoin tai
jossa konstruktioiden merkitys on eri ja niin edelleen. Lisäksi esimerkiksi venäjän
äkkiä-konstruktioista voitaisiin hyvin tarkentaa, että lauseen verbin tulee edustaa
perfektiivistä aspektia. Tämän tutkimuksen puitteissa havaittu konstruktiotason
vastaavuus on kuitenkin riittävä pysähtymispiste hypoteesin tarkennusprosessissa.
Olennaisempaa kuin tarkentaa hypoteesia 4, on tutkia
lähemmin niitä konstruktioita, joiden osalta suomen ja venäjän 
samanlaisuus ei ole yhtä ilmeistä. 


Konstruktiot, joiden vastaavuus ei ole yksiselitteistä
------------------------------------------------------

Hypoteesin 4 jälkeen taulukossa
1 listatuista konstruktioista jäljelle jäävät
affektiivinen konstruktio, alatopiikkikonstruktio ja ajallinen
alatopiikkikonstruktio, määrää painottavat konstruktiot, topikaalinen
S4-konstruktio, johdantokonstruktion eri variantit, raportoiva konstruktio,
ilmoituksen aloittava konstruktio, fokaaliset S1- ja S2-konstruktiot,
monivalenssinen S4-konstruktio, duratiivi + paikka -konstruktio, topikaalinen duratiivinen
konstruktio sekä ei--fokaalinen konstruktio. Käsittelen näitä seuraavassa
ryhmiteltyinä toisiinsa liittyviksi kokonaisuuksiksi.

### Affektiivinen konstruktio, duratiiviset ja fokaaliset konstruktiot

Kuten luvun \ref{ajanilmaukset-ja-alkusijainti}
aluksi mainittiin, affektiivisuuden linkittyminen lauseenalkuiseen asemaan sinänsä
on laajempikin  kuin vain suomea ja venäjää koskeva kielellinen ilmiö. Suomen ja venäjän välillä
kuitenkin vaikuttaisi olevan eroja siinä, minkälaisissa tilanteissa ja kuinka usein 
lauseenalkuinen ajanilmaus on tulkittavissa affektiiviseksi ja miten tavallista 
ja odotuksenmukaista affektiivinen tyyli on. Affektiivisen konstruktion osalta hypoteesi
*suomen affektiivinen konstruktio =[^onmerkki] venäjän affektiivinen
konstruktio* ei siis sinällään vielä ole riittävän tarkka. Esimerkiksi seuraavien
edellä käsiteltyjen esimerkkien (alun perin @ee_marihuanu ja @ee_neko) osalta
voidaan todeta, että niiden kaltaiset tapaukset ovat venäjänkielisessä
aineistossa tavallisia, mutta suomenkielisessä aineistossa rajoittuneet
lähinnä uskonnollisiin tai muulla tavalla kapeisiin tunnepitoisiin
konteksteihin:

[^onmerkki]: Käytän =-merkkiä tässä ilmaisemaan *samanlaisuutta*, en
yhtäläisyyttä.





\ex.\label{ee_marihuanu2} \small Восемь лет я курила марихуану, из них пять лет марихуану выращенную
гидропоническим способом, которая имеет огромное негативное влияние на
весь организм человека. (Araneum Russicum: marinagribanova.com)






\ex.\label{ee_neko2} \small \emph{Каждый день} мы помогаем различным компаниям разрешать задачи в
различных областях: экспертизы, оценки, и сопутствующие им. (Araneum
Russicum: neko-business.ru)




Monissa esimerkkien \ref{ee_marihuanu2} ja \ref{ee_neko2} kaltaisissa tapauksissa onkin
todettava, että venäjän affektiivisen S1-konstruktion kanssa samanlaisissa
tilanteissa käytettäisiin todennäköisesti tavallisia fokaalisia S3- tai
S4-konstruktioita. Ylipäätään affektiivisuus on ominaisuus, jonka
voi nähdä liittyvän johonkin toiseen, esimerkiksi fokuksen tai kontrastin
diskurssifunktioon (vrt. Jankon *модифицирцющие значения*), ja siinä mielessä
esimerkkien \ref{ee_marihuanu2} ja \ref{ee_neko2} kaltaiset tapaukset voisi mieltää myös 
fokaalisiksi S1-konstruktioiksi. Venäjänkielisestä aineistosta löydettiin
kuitenkin myös sellaisia fokaalisia S1-tapauksia, joissa affektiivisuus vaikuttaisi
olevan melko lievää (kuten esimerkissa \ref{ee_trjuki}, tässä numerolla @ee_trjuki2) sekä
toisaalta tapauksia, joihin affektiivisuutta ei liity juuri lainkaan (esimerkki
@ee_udmurtii, tässä numerolla \ref{ee_udmurtii2}).




\ex.\label{ee_trjuki} \small Уже завтра зрители увидят невероятные трюки главных артистов. (Araneum
Russicum: lotosgtrk.ru)






\ex.\label{ee_udmurtii2} \small Именно тогда Правительство Удмуртии будет рассматривать проект заявки.
(RuPress: Комсомольская правда)



Voidaankin todeta, että venäjässä lauseenalkuisen, jollain tavalla korosteisen
ajanilmauksen käyttöala on laajempi kuin suomessa. Tavallisempia ovat myös koko
lailla neutraalit fokuksen diskurssifunktiossa toimivat lauseenalkuiset
ajanilmaukset. Näissä tapauksissa hypoteesia konstruktioiden samanlaisuudesta
on tarkennettava toteamalla, että vaikka tietyissä tapauksissa suomen ja
venäjän affektiiviset konstruktiot muistuttavat toisiaan, 
näin ei ole aina, vaan muun muassa esimerkkien \ref{ee_marihuanu2} -- \ref{ee_udmurtii2}
kaltaisissa tilanteissa suomessa käyttötilanteiltaan samanlaisimpia ovat
lähtökohtaisesti neutraalit fokaaliset S3- ja S4-konstruktiot.

Esimerkkien \ref{ee_marihuanu2} ja \ref{ee_neko2} edustamien tapausten lisäksi
duratiiviseen funktioon havaittiin edellisissä kolmessa luvussa liittyvän
muitakin suomea ja venäjää erottavia tekijöitä. 
Taulukossa 1 mainittiin S3-asemaan liittyvät
duratiivi + paikka -konstruktio sekä topikaalinen duratiivinen konstruktio,
joiden voi kummankin nähdä olevan suomelle spesifejä siinä mielessä, että
niiden käyttö on linkittynyt juuri S3-asemaan eikä niinkään venäjän tavalliseen
keskisijaintiin, S2:een (ks. esimerkit @ee_annelisauli2 ja @ee_juhlauhreja edellä).


### Alatopiikkikonstruktiot, johdantokonstruktiot ja ei--fokaalinen konstuktio

Niin tavallisia kuin ajallisia alatopiikkikonstruktioita sinänsä havaittiin
sekä suomen- että venäjänkielisessä aineistossa melko runsaasti. Suomen ja venäjän välillä
vaikuttaisi kuitenkin olevan eroa siinä, miten herkästi esimerkiksi
positionaaliset (ks. luku \ref{s1-sim-pos}) tai päiviin viittaavat deiktiset
ajanilmaukset (ks. luvut \ref{l1-erot-kesk} ja \ref{deiktiset-adverbit-ja-positionaalisuus}) 
tulevat tulkituksi alatopikaalisiksi. Toisin sanoen, samanlaisuudesta
esitettävä hypoteesi alatopiikkitapausten kohdalla on tarkennettava 
muotoon *venäjän alatopiikkikonstruktio on päällisin puolin samanlainen kuin
suomen alatopiikkikonstruktio, mutta on tilanteita, jotka 
tulevat tulkituksi alatopiikikonstruktion edustajiksi todennäköisemmin suomessa*.

Hypoteesin tarkennus tulee ilmi erityisesti tapauksissa, jotka venäjässä 
edustavat globaalin tai johdantokonstruktion S1-versiota. Tästä hyvinä esimerkkeinä
käyvät muun muassa virkkeet @ee_pfizer ja \ref{ee_autoru2}, tässä uudestaan numeroilla
\ref{ee_pfizer2} ja \ref{ee_autoru2}




\ex.\label{ee_pfizer2} \small \emph{В 1996 году} Pfizer использовала антибиотик для лечения вспышки
менингита в нигерийской провинции Кано, после чего умерли 11 детей и
десятки других остались парализованными.






\ex.\label{ee_autoru2} \small \emph{В 1996 году} вы зарегистрировали домен Auto.ru.



Koska -- kuten muun muassa luvussa \ref{s1-sim-pos} annetut esimerkit
@ee_citylehti ja @ee_uusitalo osoittavat -- niin lokaali kuin globaali 
johdantokonstruktio ovat suomessakin jossain määrin käytössä, ei ole
poissuljettua, etteivätkö esimerkkien \ref{ee_pfizer2} ja \ref{ee_autoru2}
sanasanaiset suomenkieliset käännökset
*vuonna 1996 Pfizer käytti...* / *vuonna 1996 te rekisteröitte...* voisi
tulla tulkituiksi, kuten venäjässä, johdantokonstruktioina. Suomen
lauseenalkuisten esimerkkien lähempi tarkastelu edellisessä kolmessa luvussa
kuitenkin osoitti, että johdantokonstruktiot ovat suomessa harvinaisia, kun taas
alatopiikkikonstruktiot melkeinpä tavallisin syy S1-sijainnille ylipäätään.
Tämän takia ainakin kirjoitetussa tekstissä lukija oletettavasti yrittää ensin
sovittaa vastaavia lauseita alatopiikkikonstruktioon -- etenkin tässä esitetyissä
lokaaleissa johdantokonstruktiotapauksissa, joissa lauseen subjekti näyttäytyy
diskurssistatukseltaan aktiivisena.


Johdantokonstruktioista sinänsä on jatkettava, että vaikka kummankin
johdantokonstruktion S1-versiota havaittiin suomenkielisessäkin aineistossa, ja
siten hypoteesi *venäjän S1-johdantokonstruktiot ovat samanlaisia kuin suomen
S1-johdantokonstruktiot* pitää osittain paikkansa, on huomattavan paljon
tavallisempaa, että suomessa vastaavia ajanilmauksen diskurssifunktioita
ilmaistaan, kuten osiossa  \ref{l1-erot-kesk} 
 havaittiin, 
alkusijainnin asemesta keskisijainnilla. Keskisijaintiin liittyvistä
johdantokonstruktioista puolestaan
voidaan todeta, että ne ovat venäjässä selkeästi tavallisempia kuin
S1-johdantokonstruktiot suomessa. Näin ollen johdantokonstruktioita koskeva
samanlaisuushypoteesi on lopulta melko monimutkainen: kummassakin kielessä
ovat käytössä periaatteessa kaikki eri johdantokonstruktion versiot, mutta
suomessa venäjän S1-konstruktiota vastaa yleisimmin S3-konstruktio, venäjässä suomen
S3-konstruktiota taas yhtä lailla S2- kuin S1-konstruktio, joskin jälkimmäinen oletettavasti
useammin.

Johdantokonstruktioihin liittyy lisäksi läheisesti myös suomen ei--fokaalinen
S4-konstruktio. Luvussa \ref{deiktiset-adverbit-ja-positionaalisuus} tehtyjen havaintojen mukaan
ei--fokaalinen konstruktio esiintyy monissa sellaisisa käyttöyhteyksissä (kuten
esimerkki @ee_vanki, tässä \ref{ee_vanki2}), joissa
venäjänkielisessä aineistossa havaitaan lauseenalkuinen ajan- ja paikanilmaus
(muun muassa esimerkki @ee_vtsherajushenko, tässsä \ref{ee_vtsherajushenko2}):




\ex.\label{ee_vanki2} \small Vanki sytytti tulipalon sellissään Riihimäen keskusvankilassa tiistaina.
(FiPress: Aamulehti)






\ex.\label{ee_vtsherajushenko2} \small Вчера в Киеве президент Украины Виктор Ющенко дал пресс-конференцию,
которую многие расценили как парад победы. (RuPress: РБК Daily)



Esimerkkien \ref{ee_vanki2} ja \ref{ee_vtsherajushenko2} välinen yhteys liittyy
nimenomaan globaaleihin, tekstin alkupuolella esiintyviin
johdantokonstruktioihin, joissa esitellään tekstin kannalta keskeisiä toimijoita
ja tapahtumia ja jotka Lambrechtin [-@lambrecht1996, 124] jaottelussa
voitaisiin monin paikoin luokitella myös tapahtumista raportoiviksi rakenteiksi.
Juuri ei--fokaalinen konstruktio vaikuttaisi suomessa tyypilliseltä tavalta
toteuttaa tällaista diskurssifunktiota, vaikkakin myös S3-johdantokonstruktiot
ovat aineistossa melko käytetty strategia.


### Muut konstruktiot

Oleellisimmat suomen ja venäjän välillä havaittavat erot ajanilmauksen sijainnissa
liittyvät edellisissä alauluvuissa käsiteltyihin johdantokonstruktioihin, fokaalisiin
konstruktioihin sekä affektiiviseen konstruktioon. Lisäksi on tietysti pidettävä mielessä
kieliopillinen S2-sijainnin käyttöä koskeva ero, johon monet edellä mainituista 
eroista esimerkiksi fokaalisen konstruktion käytössä itse asiassa liittyvät
(vrt. alaluku \ref{loppus_np}). Tutkimuksen kuluessa kiinnitettiin huomiota
myös koko joukkoon muita konstruktioita, joiden suhteen suomella ja venäjällä
havaittiin tiettyjä kielikohtaisia erityispiirteitä.

Ensinnäkin, määrää painottavat konstruktiot vaikuttivat suomessa erityisen voimakkaasti 
lauseenalkuisen sijainnin suosioon. Vaikka venäjänkielisestä aineistosta oli
löydettävissä hyvin pitkälle samanlaisia konstruktioita, voidaan ainakin tähän tutkimukseen
valikoitujen ilmausten osalta sanoa, että suomessa määrää painottava konstruktio on
jossain määrin ilmeisempi tulkintakehys monille lauseenalkuisille -- joskin myös S3-sijoittuneilla
-- ajanilmaustapauksille kuin venäjässä.

Toiseksi, S1-sijainissa havaittiin muutamia ennen kaikkea venäjälle
tyypillisiä, käyttötilanteiltaan melko spesifejä rakenteita, joita ovat
raportoiva konstruktio ja ilmoituksen aloittava konstruktio. Näitä konstruktioita
ilmensivät tässä uudelleen numeroilla \ref{ee_pistolet2} (laajennetun kontekstin
kera) ja \ref{ee_galereja2} esitetyt esimerkit @ee_pistolet ja @ee_galereja:




\ex.\label{ee_pistolet2} \small В Петропавловске-Камчатском ночью было совершено нападение на ведущего
камчатской радиостанции "Радио-3" Геннадия Шеренговского. \emph{В два
часа ночи} вооруженный пневматическим пистолетом пьяный гражданин
выломал входную дверь в редакции "Радио-3", находящуюся в здании Дома
прессы, и проник в студию. (Araneum Russicum: gdf.ru)






\ex.\label{ee_galereja2} \small 7 марта \emph{в 17 часов} Художественная галерея приглашает на концерт
инструментальной и вокальной музыки Смоленского гитарного ансамбля под
руководством Заслуженного работника культуры, доцента Виктора Федоровича
Павлюченкова. (Araneum Russicum: smolensk-museum.ru)



Kuten luvussa \ref{s1-l3} todettiin, vastaavat konstruktiot ovat epäilemättä
käytössä myös suomessa, mutta tutkimusaineistossa tällaiset tapaukset ovat
verrattain harvinaisia. Esimerkin \ref{ee_galereja2} kaltaisia ilmoituksen
aloituksia voisi ainakin kuvitella toteutettavan laajemmin esimerkiksi
fokaalisella S4-konstruktiolla. 

Esimerkin \ref{ee_pistolet2} tekee suomen kontekstissa epätavalliseksi kokonaan
uutena diskurssireferenttinä esiteltävä radiostudioon tunkeutunut mies.
Suomessa esimerkin varsinaisen ajanilmauksen sisältävän virkkeen sijaan 
tekstin avausvirkkeen jälkeisen lauseen voisi suurella todennäköisyydellä
ajatella ankkuroituvan jollain tavalla joko tekstin alussa mainittuun radioasemaan
tai hyökkäyksen kohteeksi joutuneeseen juontajaan. Suomenkielinen teksti saattaisi
kulkea esimerkiksi seuraavalla hypoteettisella tavalla:

\begin{quote}
Alueella X tapahtui viime yönä välikohtaus, jonka kohteeksi joutui
paikallisradion juontaja Y. Radion studioon tunkeutui kello kahdelta yöllä humalainen
mies, joka mursi studion oven ja...
\end{quote}

Toisin sanoen suomenkielisessä kontekstissa esimerkissä \ref{ee_pistolet2} mainittu
mies olisi luontevinta erikseen esitellä osaksi  diskurssia. Tekstin
toinen virke tulisi todennäköisimmin ankkuroiduksi jo esiteltyyn tietoon joko
tapahtumaan liittyvän paikan tai toimijoiden kautta. Varsinaisessa
venäjänkielisessä esimerkissä vaikuttaisi kuitenkin siltä, että edellisessä
lauseessa muodostettu ajallinen konteksti (*ночью*-sanan referenttinä oleva
edeltävä yö) riittää ankkuriksi, jota vasten uutta tietoa -- esimerkiksi
hyökkääjä uutena toimijana -- voidaan esitellä. Tässä mielessä
raportoiva konstruktio liittyy läheisesti johdantokonstruktioihin, joiden harvinaisuus
suomessa on lopulta selitettävissä samanlaisella erolla siinä, miten tavallista
ajanilmauksia on käyttää ankkureina, joita vasten uutta tietoa esitellään.

Ilmoituksen aloittavan ja etenkin raportoivan konstruktion tapauksissa on
vaikea määritellä suoraan konstruktiota, joka todennäköisimmin toteuttaisi
samanlaista viestintätehtävää suomessa. Pikemminkin on todettava, että että
mainittujen konstruktioiden vastineet vaihtelevat konstekstikohtaisesti, niin
että joissain tapauksissa suomessa olisi todennäköisempää käyttää esimerkiksi
alatopiikkirakennetta joissain taas esimerkiksi jotakin sekventiaalisen
konstruktion ilmentymää. Näiden konstruktioiden osalta samanlaisuushypoteesi
onkin selvin esittää muodossa *venäjän kielen ilmoituksen aloittaville ja
raportoiville konstruktioille ei ole
yksittäisiä, muita selkeämmin samanlaisia suomenkielisiä konstruktioita.* 

Venäjälle tyypillisten konstruktioiden lisäksi vielä käsittelemättömistä
konstruktioista voidaan ottaa esille suomen monivalenssinen konstruktio, jota
tarkasteltiin luvussa \ref{loppus_np}. Johtuen tämän alaluvun alussa mainituista
suomen ja venäjän keskisijaintia koskevista kieliopillisista eroista monivalenssista
konstruktiota voidaan pitää koko lailla pelkästään suomessa havaittavana
ilmiönä. Konstruktiosta annettiin edellä esimerkki @ee_tietoyht, joka tässä
on toistettu numerolla \ref{ee_tietoyht2}:




\ex.\label{ee_tietoyht2} \small Poliitikot pitävät tietoyhteiskuntaa liian usein vain tekniikka-asiana.
(FiPress: Turun Sanomat)



Toisin kuin venäjän raportoivan ja ilmoituksen aloittava konstruktion
tapauksessa, suomen monivalenssiselle konstruktiolle on melko yksinkertaista
esittää venäjänkielisiä vastineita: luonnollisesti venäjässä samanlaisia 
ovat tavalliset adverbiset konstruktiot, joita  -- kuten luvussa \ref{loppus_np}
todettiin -- eivät koske ne rajoitteet, jotka suomessa johtavat S4-aseman 
käyttämiseen tavallisen keskisijainnin (S3) sijasta. Toisaalta voidaan myös esittää,
että koska venäjässä affektiivinen konstruktio on käytöltään
tavallisempi, myös nämä voidaan lukea yhtälön *suomen monivalenssinen 
konstruktio = * oikealle puolelle. Toisaalta molempien kielten eri konstruktiot
muodostavat tässä yhteydessä kaikkea muuta kuin selvärajaisen ja suoraviivaisen
samanlaisuuksien verkoston, sillä myös suomessa ero etenkin esimerkin \ref{ee_tietoyht2}
kaltaisten S4-asemaan liittyvien *liian* + taajuuden ajanilmaus -tapausten ja affektiivisten 
S1-tapausten välillä on lopulta melko pieni.


Koonti
------

Tässä yhteenvetoluvussa käsitellyt suomen ja venäjän eri
ajanilmauskonstruktioiden samankaltaisuudet ovat kaiken kaikkiaan parhaiten
kuvattavissa ennemmin tiheästi linkittyneenä verkostona kuin yksittäisinä,
toisistaan erillisinä vastaavuussuhteina. Kuvio 1 
pyrkiikin tiivistämään edellä käsitellyt konstruktioiden väliset suhteet
tällaiseksi verkoksi. Kuviossa suomen kielen konstruktiot on kuvattu
tummemmalla, venäjän kielen vaaleammalla värillä. Oma tulkintani kunkin suhteen
voimakkuudesta on esitetty eri konstruktioiden välisten viivojen paksuutena:
mitä paksumpi viiva, sitä voimakkaammasta yhteydestä on kyse.


![**Kuvio 1: ** Suomen ja venäjän ajanilmauskonstruktioiden väliset yhteydet verkkona.](figure/nwvis-1.pdf)


Kuviossa 1  esitettyjen konstruktioryppäiden
etäisyys toisistaan tai niiden sijainti kuvassa ei ole merkityksellinen.
Ajatuksena on yksinkertaisesti kuvata sitä, miten eri konstruktioiden 
ja laajemmalla tasolla sijaintien samanlaisuus on monimutkaisempi asia 
kuin vain yhden konkreettisen vastineparin esittäminen.
Kuviossa on muista erottuvia yksittäisiä konstruktioita -- venäjän raportoiva
ja ilmoituksen aloittava konstruktio -- selkeitä konstruktiopareja, kuten
venäjän ja suomen kontrastiiviset konstruktiot sekä toisaalta laajempia ryppäitä,
ennen muuta johdantokonstruktioihin liittyvä rypäs sekä toisaalta affektisen ja fokaalisen
konstruktion ympärille rakentuva rypäs.





