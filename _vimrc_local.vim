set nowrap
set nospell
set foldcolumn=0

abbr rrr \@ref(

inoremap  tabref <Esc>:call Maketabref('tab')<cr>
inoremap  figref <Esc>:call Maketabref('fig')<cr>ff_
inoremap  defref <Esc>:call Maketabref('def')<cr>
inoremap  asref <Esc>:call Maketabref('as')<cr>
inoremap  hhref <Esc>:call Maketabref('hyp')<cr>
inoremap  mmref <Esc>:call Maketabref('mat')<cr>


inoremap  kaavaref <Esc>:call Maketabref('kaavaRef')<cr>
inoremap rrl <Esc>:call InsertHere("`r `")<cr>i
inoremap rrb <Esc>:call InsertHere("```{r, echo=FALSE}\n```")<cr>jo<Esc>2ko
inoremap emem (@ee_)<Esc>i
inoremap colred <span class='r'></span><Esc>F>a
map <c-r>r "xdi<span class='r'></span><Esc>F>"xp
map <c-g><c-g> vf-eh "oy :!echo "<C-r>o">>~/.config/enchant/fi.dic<CR><CR>
map <c-c> :sp chaplist.otl<CR>:set nowrap<CR>zM<c-w>5-:so ../chapcommands.vim<CR>
inoremap <c-q>h <C-R>=CustomComplete('hyp')<CR>
inoremap <c-q>m <C-R>=CustomComplete('matrix')<CR>
noremap reptoru :s/fi/ru/g<CR>
noremap reptofi :s/ru/fi/g<CR>

map <leader>gi 0f`2lvt`\ss


map <c-r>oj  <Esc>:call Output()<cr><c-w>j<Esc>"bpI<CR><Esc><c-w>k
map <c-r>ol  <Esc>:call Output()<cr><c-w>l<Esc>"bpI<CR><Esc><c-w>h


function! Output()
    let @b = "Output('" . expand("%") . "')"
endfunction

func! CustomComplete(type)
    let b:position = col('.')
    if a:type == 'hyp' 
      let b:list = readfile('/home/juho_harme/phdmanuscript/configfiles/hyp.txt')
    elseif a:type == 'matrix'
      let b:list = readfile('/home/juho_harme/phdmanuscript/configfiles/matrix.txt')
    endif
    let b:matches = []
    for item in b:list
        call add(b:matches,item)
    endfor
    call complete(b:position, b:matches)
    return ''
endfunc


function! Maketabref2(type)
    let a:trefstart = "`r ref('" . a:type . "','"
    let a:trefend = "')`"
    let instext = " " . a:trefstart . a:trefend
    execute ":normal i" . instext
    execute "normal hh"
    startinsert 
endfunction


function! Maketabref(type)
    let a:trefstart = "`r Ref('" . a:type . "', '"
    let a:trefend = "')`"
    let instext = " " . a:trefstart . a:trefend
    execute ":normal i" . instext
    execute "normal hh"
    startinsert 
endfunction


function! InsertHere(instext)
    let @a = a:instext
    execute "normal \"apa"
endfunction


"Translitterointi funktiona, joka poistaa turhat rivivälit

	"nmap <silent> <space>t :set opfunc=Translit_en<CR>g@
	"vmap <silent> <space>t :<C-U>call Translit_en(visualmode(), 1)<CR>
	nmap <silent> <space>t :set opfunc=Translit<CR>g@
	vmap <silent> <space>t :<C-U>call Translit(visualmode(), 1)<CR>

	function! Translit(type, ...)
	  let sel_save = &selection
	  let &selection = "inclusive"
	  let reg_save = @@

	  if a:0  " Invoked from Visual mode, use '< and '> marks.
	    silent exe "normal! `<" . a:type . "`>\"ad"
	  elseif a:type == 'line'
	    silent exe "normal! '[V']\"ad"
	  elseif a:type == 'block'
	    silent exe "normal! `[\<C-V>`]\"ad"
	  else
	    silent exe "normal! `[v`]\"ad"
	  endif
      "Itse komennot:
      let com = "php /home/juho_harme/phdmanuscript/configfiles/scripts/translit.php '" . @a ."'"
      let @b = system(com)
      let @b=substitute(strtrans(@b),'\^@',' ','g')
      execute "normal \"bp"

	endfunction




	function! Translit_en(type, ...)
	  let sel_save = &selection
	  let &selection = "inclusive"
	  let reg_save = @@

	  if a:0  " Invoked from Visual mode, use '< and '> marks.
	    silent exe "normal! `<" . a:type . "`>\"ad"
	  elseif a:type == 'line'
	    silent exe "normal! '[V']\"ad"
	  elseif a:type == 'block'
	    silent exe "normal! `[\<C-V>`]\"ad"
	  else
	    silent exe "normal! `[v`]\"ad"
	  endif
      "Itse komennot:
      let com = "echo '" . @a ."' |  translit -t 'GOST 7.79 RUS'"
      let @b = system(com)
      let @b=substitute(strtrans(@b),'\^@',' ','g')
      execute "normal \"bp"

	endfunction




"Translitterointi funktiona, joka poistaa turhat rivivälit

	nmap <silent> <space>t :set opfunc=Translit<CR>g@
	vmap <silent> <space>t :<C-U>call Translit(visualmode(), 1)<CR>

	function! Translit(type, ...)
	  let sel_save = &selection
	  let &selection = "inclusive"
	  let reg_save = @@

	  if a:0  " Invoked from Visual mode, use '< and '> marks.
	    silent exe "normal! `<" . a:type . "`>\"ad"
	  elseif a:type == 'line'
	    silent exe "normal! '[V']\"ad"
	  elseif a:type == 'block'
	    silent exe "normal! `[\<C-V>`]\"ad"
	  else
	    silent exe "normal! `[v`]\"ad"
	  endif
      "Itse komennot:
      let com = "php /home/juho_harme/Dropbox/VK/skriptit/php/translit_commandline.php '" . @a ."'"
      let @b = system(com)
      let @b=substitute(strtrans(@b),'\^@',' ','g')
      execute "normal \"bp"

	endfunction




	function! Translit_en(type, ...)
	  let sel_save = &selection
	  let &selection = "inclusive"
	  let reg_save = @@

	  if a:0  " Invoked from Visual mode, use '< and '> marks.
	    silent exe "normal! `<" . a:type . "`>\"ad"
	  elseif a:type == 'line'
	    silent exe "normal! '[V']\"ad"
	  elseif a:type == 'block'
	    silent exe "normal! `[\<C-V>`]\"ad"
	  else
	    silent exe "normal! `[v`]\"ad"
	  endif
      "Itse komennot:
      let com = "echo '" . @a ."' |  translit -t 'GOST 7.79 RUS'"
      let @b = system(com)
      let @b=substitute(strtrans(@b),'\^@',' ','g')
      execute "normal \"bp"

	endfunction

"
