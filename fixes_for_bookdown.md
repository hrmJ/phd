# Korjaa viittaukset kuvioihin:

```

%s/`r Ref('fig', \?'ff_\([\.a-öA-Ö_0-9_-]\+\)')`/\\@ref(fig:\1)/gc

```

# Korjaa viittaukset lukuihin ja osioihin

```

%s/\\ref{\([a-öA-Ö0-9_-]\+\)}/\\@ref(\1)/gc

```

# Korjaa viittaukset matriiseihin

```

%s/`r Ref('mat', \?'mm_\([a-öA-Ö0-9_]\+\)')`/\\@ref(mat:\1)/gc
%s/`r Ref('mat', \?'\([a-öA-Ö0-9_]\+\)')`/\\@ref(mat:\1)/gc

```


# Korjaa viittaukset taulukoihin

```

%s/`r Ref('tab', \?'tt_\([a-öA-Ö0-9_]\+\)')`/\\@ref(tab:\1)/gc

```


# Korjaa esimerkit

## Yksiriviset

```
%s/^(@ee_\([a-öA-Ö_0-9x-]\+\))\([^(]\+\)(\(Araneum\|FiPress\|RuPress\|Fipress\|Rupress\)\(.*\)/(@) \2 (\3\4 {#ex:\1}/gc
```

## Moniriviset

```

%s/^(@ee_\([a-öA-Ö_-]\+\))\([^(]\+\)\n\([^(]\+\n\)\{1,\}\([^(]\+\)(\(Araneum\|RuPress\|FiPress\)\(.*\)/(@) \2\3\4 (\5\6 {#ex:\1}/gc

%s/^(@)\(.*\)\n\([^(]\+(\)\(Araneum\|RuPress\|FiPress\)\(.*\)/(@)\1 \2\3\4/gc


```

## viittaukset

```
%s/^@ee_\([a-öA-Ö0-9_]\+\)/@ex:\1/gc
%s/@ee_\([a-zA-Z]\+\)/@ex:\1/gc

```




# Korjaa viittaukset hypoteeseihin

```

%s/`r Ref('hyp', \?'hh_\([a-öA-Ö0-9_]\+\)')`/\\@ref(hyp:\1)/gc

```

