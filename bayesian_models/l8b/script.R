#!/usr/bin/env Rscript

library(runjags)
library(coda)
library(mcmcplots)

aineisto <- readRDS('/home/juho_harme/data/modeldata/l8b.rds')

for(varname in c('lang','clausestatus')){
    aineisto[[varname]] <- as.factor(aineisto[[varname]])
}

observations <- xtabs(~ lang + clausestatus + location3, data=aineisto)
totals <- xtabs(~ lang + clausestatus, data=aineisto)

dataList <- list(observations=observations, totals=totals, Nlang = length(unique(aineisto$lang)), Nclausestatus = length(unique(aineisto$clausestatus)), Nlocation3 = length(unique(aineisto$location3)))

monitor <- c('lang', 'clausestatus', 'std.lang', 'std.clausestatus', 'lang.clausestatus', 'std.lang.clausestatus')

RunJagsModel <- run.jags(data=dataList, monitor=monitor, model='/home/juho_harme/data/model_specifications/l8b/model.bugs', burnin=5000.0, thin=1.0, adapt=10000.0, n.chains=2.0, method='parallel', sample=50000.0)

post <- as.mcmc.list(RunJagsModel)
saveRDS(post,"/home/juho_harme/data/modeldata/l8b_post.rds")
mcmcplot(post,dir="/home/juho_harme/phdmanuscript/output/mcmc_diagnostics/l8b")
summary(post)