---
title: 'Ajanilmausten sijainnit suomessa ja venäjässä: konstruktiotason kontrastiivinen näkökulma'
---


On the locations of time adverbials in Finnish and Russian: a construction-based contrastive approach.

250 pages, in Finnish

**Keywords** Contrastive linguistics, corpora, word order, construction grammar, time adverbials

<!--

Ajanilmausten sijainnit suomessa ja venäjässä: konstruktiotason kontrastiivinen näkökulma

-->


Abstract
========


Motivation and theoretical background
--------------------------------------

Finnish and Russian are languages traditionally considered to have a free or --
in generativistic terms -- discourse configurational word order. In both
languages the positioning of adverbials in general and time adverbials in
particular is especially flexible. This flexibility poses a challenge both from
an L2 learner's and a translator's perspective: coming from a Finnish
background it is often difficult to use time adverbials idiomatically in
Russian and vice versa. The challenge is made greater by the fact that there is
very little learning material or teacher's guides available on the subject.
This study takes a construction grammar based contrastive approach on the
issue, aiming to provide translators, language learners and teachers with
concrete examples on what the essential differences between Finnish and Russian
are with regard to the placement of time adverbials. <!-- The study operates on the
notions of information structure as defined in Knud Lambrecht's 1994 work. -->

Research data
----------------

The study examines `r d %>% pull(group) %>% unique %>% length`
different groups of Finnish and Russian time adverbials, the
selection of which was based on three qualities: the etymology,
semantic function and referential status of the adverbial. 
Syntactically, the focus is on Finnish and Russian affirmative SVO
sentences, in the context of which the possible locations
of the adverbial were divided into four distinct categories: 
L1 (before the subject, the verb and the object), L2 (between the
subject and the verb), L3 (between the verb and the object) and
L4 (after the subject, the verb and the object). The data of 
the study consists of two pairs of large comparable corpora: 
A Finnish and a Russian newspaper corpus and a Finnish and a
Russian corpus of internet texts. The corpora
were queried for the expressions included in the 
`r d %>% pull(group) %>% unique %>% length` groups and the
concordances retrieved by the queries were further annotated
using dependency parsers. As a result, a data set of 
 `r d %>% filter(lang=="fi") %>% nrow %>% format(., big.mark=" ")`
Finnish 
and `r d %>% filter(lang=="ru") %>% nrow %>% format(., big.mark=" ")`
Russian sentences was formed.

```{r, echo=FALSE}
counts <- round(prop.table(xtabs(~lang+location,d),1)*100,2)
```

Reseach methods and main findings
--------------------------------

The collected data shows, first of all, that there really is a
notable difference between Finnish and Russian in the frequencies
of the four locations defined. Besides the 
grammatically expected difference in the usage of L2 and L3 (the
former is rare in Finnish, the latter in Russian), the data
indicates that the sentence-initial position is more frequent in
Russian (`r counts["ru","S1"]` % of all the cases compared to `r counts["fi","S1"]` % in Finnish)
and, conversely, the sentence-final more frequent in Finnish
(`r counts["fi","S4"]` % of all the cases compared to `r counts["ru","S4"]` % in Russian).
To account for the reasons behind these observations, a
(bayesian) categorical regression model was built with the
location of the adverbial as the dependent variable.
The model was used to identify the use cases of a time adverbial
that are, on the other hand, common for both of the studied
languages, and, on the other hand,  more typical for Russian than
for Finnish and vice versa. The properties of these use cases --
most importantly, the roles of the constituents in the examples from 
the point of view of information structure -- were summarized as
constructions. 

The final results of the study are presented as a network of
constructions, indicating which Finnish and Russian use cases of
time adverbials are similar and to what extent. In many cases
there is a correspondence between a Russian construction with a
sentence-initial  and a Finnish construction with a
sentence-final or L3-located adverbial. One clear example of
these are sentences classified in the study as introductory time
adverbial constructions, which are constructions usually used at
the beginning of a text to introduce, for instance, the central
event a news item is reporting about.

Conclusions
-----------

By analyzing time adverbials as parts of constructions  rather
than as an independent syntactic category and by using a
data-driven, explicitly contrastive approach the study intends to
raise discussion on the role and methods of teaching grammar for
students in translation studies. It is suggested that a
corpus-based methodology and a theoretical framework based on
construction grammar would provide a fruitful combination in
university-level second language teaching aimed specifically at
future translators and interpreters working within a specific
language pair. Although the study operates mainly on a rather
practical and applied level of contrastive linguistics, the
findings give grounds for further research to be conducted 
from a more theoretical, typological perspective.







